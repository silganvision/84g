#if !defined(AFX_SETUPSTLIGHTFILTER_H__0ECAA95E_6822_4174_8449_D1921E8CE1BE__INCLUDED_)
#define AFX_SETUPSTLIGHTFILTER_H__0ECAA95E_6822_4174_8449_D1921E8CE1BE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// setupStLightFilter.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// setupStLightFilter dialog

class setupStLightFilter : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
	// Construction
public:
	void UpdateDisplay();
	void UpdateValues();
	setupStLightFilter(CWnd* pParent = NULL);   // standard constructor
	CBottleApp *theapp;
	CMainFrame *pframe;
	int resSen;
	bool stepSlow;


// Dialog Data
	//{{AFX_DATA(setupStLightFilter)
	enum { IDD = IDD_STLIGHTFILTER };
	CButton	m_step;
	CString	m_senFilterLigh;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(setupStLightFilter)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(setupStLightFilter)
	virtual void OnOK();
	afx_msg void OnUseimgst1();
	afx_msg void OnUseimgst2();
	afx_msg void OnUseimgst3();
	afx_msg void OnUseimgst4();
	afx_msg void OnUseimgst5();
	afx_msg void OnUseimgst6();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnStep();
	afx_msg void OnSenlightup();
	afx_msg void OnSenlightdw();
	afx_msg void OnUseimgst7();
	afx_msg void OnUseimgst8();
	afx_msg void OnUseimgst9();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETUPSTLIGHTFILTER_H__0ECAA95E_6822_4174_8449_D1921E8CE1BE__INCLUDED_)
