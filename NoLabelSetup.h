#if !defined(AFX_NOLABELSETUP_H__E6109EA8_48A1_4B1B_9450_BFDB31B3587A__INCLUDED_)
#define AFX_NOLABELSETUP_H__E6109EA8_48A1_4B1B_9450_BFDB31B3587A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NoLabelSetup.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// NoLabelSetup dialog

class NoLabelSetup : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
	// Construction
public:
	void UpdateValues();
	void UpdateDisplay();
	NoLabelSetup(CWnd* pParent = NULL);   // standard constructor
	CBottleApp *theapp;
	CMainFrame* pframe;
	int res;
	int resOff;

	int WidthCam;
	int HeightCam;

// Dialog Data
	//{{AFX_DATA(NoLabelSetup)
	enum { IDD = IDD_NOLABELSETUP };
	CButton	m_fine;
	CString	m_editnloffset;
	CString	m_labelSen;
	CString	m_editnloffsetX1;
	CString	m_editnloffsetX2;
	CString	m_editnloffsetX3;
	CString	m_editnloffsetX4;
	CString	m_editnloffsetW;
	CString	m_editnloffsetH;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(NoLabelSetup)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(NoLabelSetup)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnFinemod();
	afx_msg void OnNlup();
	afx_msg void OnNldn();
	afx_msg void OnNolabelsenup();
	afx_msg void OnNolabelsendw();
	afx_msg void OnUseimg1();
	afx_msg void OnUseimg2();
	afx_msg void OnUseimg3();
	afx_msg void OnUseimg4();
	afx_msg void OnUseimg5();
	afx_msg void OnUseimg6();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnOffsetxup1();
	afx_msg void OnOffsetxdw1();
	afx_msg void OnOffsetxup2();
	afx_msg void OnOffsetxup3();
	afx_msg void OnOffsetxup4();
	afx_msg void OnOffsetxdw2();
	afx_msg void OnOffsetxdw3();
	afx_msg void OnOffsetxdw4();
	afx_msg void OnWidthup();
	afx_msg void OnWidthdown();
	afx_msg void OnHeightup();
	afx_msg void OnHeightdown();
	afx_msg void OnUseimg7();
	afx_msg void OnUseimg8();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NOLABELSETUP_H__E6109EA8_48A1_4B1B_9450_BFDB31B3587A__INCLUDED_)
