#if !defined(AFX_PICRECAL_H__588278F3_4ABC_48F5_BFA0_56479BD0D7CB__INCLUDED_)
#define AFX_PICRECAL_H__588278F3_4ABC_48F5_BFA0_56479BD0D7CB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PicRecal.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// PicRecal dialog

class PicRecal : public CDialog
{
	friend class CMainFrame;
	friend class CBottleApp;
	friend class XAxisView;
	// Construction
public:
	void UpdateValues();
	void UpdateDisplay();
	PicRecal(CWnd* pParent = NULL);   // standard constructor
	
	CMainFrame	*pframe;
	CBottleApp	*theapp;
	XAxisView	*m_pxaxis;

	int count;
	int reasnofailur;

	int w[8];	int h[8];

	CComboBox *m_pcombo;

	BYTE *imgBuf3RGB;
	BYTE *imgBuf3RGB_cam2;
	BYTE *imgBuf3RGB_cam3;
	BYTE *imgBuf3RGB_cam4;
	BYTE *imgBuf3RGB_cam5;

	BYTE* arr_strng_addrs[6];


// Dialog Data
	//{{AFX_DATA(PicRecal)
	enum { IDD = IDD_PICTURE };
	CString	m_countpics;
	CString	m_edit1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PicRecal)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PicRecal)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	virtual void OnOK();
	afx_msg void OnReset();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnSelection();
	afx_msg void OnShowC5();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PICRECAL_H__588278F3_4ABC_48F5_BFA0_56479BD0D7CB__INCLUDED_)
