#if !defined(AFX_MIDDLESETUP_H__AD28629B_A792_41A2_9D1A_26A38A2C86E0__INCLUDED_)
#define AFX_MIDDLESETUP_H__AD28629B_A792_41A2_9D1A_26A38A2C86E0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MiddleSetup.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// MiddleSetup dialog

class MiddleSetup : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
	// Construction
public:
	void UpdateValues();
	void UpdateDisplay();
	MiddleSetup(CWnd* pParent = NULL);   // standard constructor
	CBottleApp *theapp;
	CMainFrame* pframe;

	int camHeight;
	int camWidth;

	int res;
	int resSen;

// Dialog Data
	//{{AFX_DATA(MiddleSetup)
	enum { IDD = IDD_MIDDLESETUP };
	CButton	m_fineMode;
	CString	m_middleW1;
	CString	m_middleW2;
	CString	m_middleW3;
	CString	m_middleW4;
	CString	m_middleY1;
	CString	m_middleY2;
	CString	m_middleY3;
	CString	m_middleY4;
	CString	m_senMidd;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(MiddleSetup)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(MiddleSetup)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnRefisleftc1();
	afx_msg void OnRefisleftc2();
	afx_msg void OnRefisleftc3();
	afx_msg void OnRefisleftc4();
	afx_msg void OnYrefup1();
	afx_msg void OnYrefdw1();
	afx_msg void OnYrefup2();
	afx_msg void OnYrefup3();
	afx_msg void OnYrefup4();
	afx_msg void OnYrefdw2();
	afx_msg void OnYrefdw3();
	afx_msg void OnYrefdw4();
	afx_msg void OnWidthup1();
	afx_msg void OnWidthdw1();
	afx_msg void OnWidthup2();
	afx_msg void OnWidthup3();
	afx_msg void OnWidthup4();
	afx_msg void OnWidthdw2();
	afx_msg void OnWidthdw3();
	afx_msg void OnWidthdw4();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnTeachmiddles();
	afx_msg void OnSenmiddup();
	afx_msg void OnSenmidddw();
	afx_msg void OnFinemod();
	afx_msg void OnUseimg1();
	afx_msg void OnUseimg2();
	afx_msg void OnUseimg3();
	afx_msg void OnUseimg4();
	afx_msg void OnUseimg5();
	afx_msg void OnUseimg6();
	afx_msg void OnUseimg7();
	afx_msg void OnUseimg8();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MIDDLESETUP_H__AD28629B_A792_41A2_9D1A_26A38A2C86E0__INCLUDED_)
