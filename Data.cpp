// Data.cpp : implementation file
//

#include "stdafx.h"
#include "Bottle.h"
#include "Data.h"
#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Data dialog


Data::Data(CWnd* pParent /*=NULL*/)
	: CDialog(Data::IDD, pParent)
{
	//{{AFX_DATA_INIT(Data)
	m_r1 = _T("");
	m_r10 = _T("");
	m_r2 = _T("");
	m_r3 = _T("");
	m_r4 = _T("");
	m_r5 = _T("");
	m_r6 = _T("");
	m_r7 = _T("");
	m_r8 = _T("");
	m_r9 = _T("");
	m_r15 = _T("");
	m_24hrtime = _T("");
	m_r11 = _T("");
	m_out0 = _T("");
	m_out1 = _T("");
	m_out10 = _T("");
	m_out11 = _T("");
	m_out12 = _T("");
	m_out13 = _T("");
	m_out14 = _T("");
	m_out15 = _T("");
	m_out16 = _T("");
	m_out2 = _T("");
	m_out3 = _T("");
	m_out4 = _T("");
	m_out5 = _T("");
	m_out6 = _T("");
	m_out7 = _T("");
	m_out8 = _T("");
	m_out9 = _T("");
	m_out17 = _T("");
	m_outh0 = _T("");
	m_outh1 = _T("");
	m_outh2 = _T("");
	m_outh3 = _T("");
	m_outh4 = _T("");
	m_outh5 = _T("");
	m_outh6 = _T("");
	m_outh7 = _T("");
	m_outhm1 = _T("");
	m_outhm2 = _T("");
	m_outhm3 = _T("");
	m_outhm4 = _T("");
	m_outhm5 = _T("");
	m_outhm6 = _T("");
	m_outhm7 = _T("");
	m_outm1 = _T("");
	m_outm10 = _T("");
	m_outm11 = _T("");
	m_outm12 = _T("");
	m_outm13 = _T("");
	m_outm14 = _T("");
	m_outm15 = _T("");
	m_outm16 = _T("");
	m_outm17 = _T("");
	m_outm2 = _T("");
	m_outm3 = _T("");
	m_outm4 = _T("");
	m_outm5 = _T("");
	m_outm6 = _T("");
	m_outm7 = _T("");
	m_outm8 = _T("");
	m_outm9 = _T("");
	m_r12 = _T("");
	m_out18 = _T("");
	m_out19 = _T("");
	m_outm18 = _T("");
	m_outm19 = _T("");
	m_insp1 = _T("");
	m_insp2 = _T("");
	m_insp3 = _T("");
	m_insp4 = _T("");
	m_insp5 = _T("");
	m_insp6 = _T("");
	m_insp7 = _T("");
	m_insp8 = _T("");
	m_insp9 = _T("");
	m_insp10 = _T("");
	//}}AFX_DATA_INIT
}


void Data::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Data)
	DDX_Text(pDX, IDC_R1, m_r1);
	DDX_Text(pDX, IDC_R10, m_r10);
	DDX_Text(pDX, IDC_R2, m_r2);
	DDX_Text(pDX, IDC_R3, m_r3);
	DDX_Text(pDX, IDC_R4, m_r4);
	DDX_Text(pDX, IDC_R5, m_r5);
	DDX_Text(pDX, IDC_R6, m_r6);
	DDX_Text(pDX, IDC_R7, m_r7);
	DDX_Text(pDX, IDC_R8, m_r8);
	DDX_Text(pDX, IDC_R9, m_r9);
	DDX_Text(pDX, IDC_R15, m_r15);
	DDX_Text(pDX, IDC_24HRTIME, m_24hrtime);
	DDX_Text(pDX, IDC_R11, m_r11);
	DDX_Text(pDX, IDC_OUT0, m_out0);
	DDX_Text(pDX, IDC_OUT1, m_out1);
	DDX_Text(pDX, IDC_OUT10, m_out10);
	DDX_Text(pDX, IDC_OUT11, m_out11);
	DDX_Text(pDX, IDC_OUT12, m_out12);
	DDX_Text(pDX, IDC_OUT13, m_out13);
	DDX_Text(pDX, IDC_OUT14, m_out14);
	DDX_Text(pDX, IDC_OUT15, m_out15);
	DDX_Text(pDX, IDC_OUT16, m_out16);
	DDX_Text(pDX, IDC_OUT2, m_out2);
	DDX_Text(pDX, IDC_OUT3, m_out3);
	DDX_Text(pDX, IDC_OUT4, m_out4);
	DDX_Text(pDX, IDC_OUT5, m_out5);
	DDX_Text(pDX, IDC_OUT6, m_out6);
	DDX_Text(pDX, IDC_OUT7, m_out7);
	DDX_Text(pDX, IDC_OUT8, m_out8);
	DDX_Text(pDX, IDC_OUT9, m_out9);
	DDX_Text(pDX, IDC_OUT17, m_out17);
	DDX_Text(pDX, IDC_OUTH0, m_outh0);
	DDX_Text(pDX, IDC_OUTH1, m_outh1);
	DDX_Text(pDX, IDC_OUTH2, m_outh2);
	DDX_Text(pDX, IDC_OUTH3, m_outh3);
	DDX_Text(pDX, IDC_OUTH4, m_outh4);
	DDX_Text(pDX, IDC_OUTH5, m_outh5);
	DDX_Text(pDX, IDC_OUTH6, m_outh6);
	DDX_Text(pDX, IDC_OUTH7, m_outh7);
	DDX_Text(pDX, IDC_OUTHM1, m_outhm1);
	DDX_Text(pDX, IDC_OUTHM2, m_outhm2);
	DDX_Text(pDX, IDC_OUTHM3, m_outhm3);
	DDX_Text(pDX, IDC_OUTHM4, m_outhm4);
	DDX_Text(pDX, IDC_OUTHM5, m_outhm5);
	DDX_Text(pDX, IDC_OUTHM6, m_outhm6);
	DDX_Text(pDX, IDC_OUTHM7, m_outhm7);
	DDX_Text(pDX, IDC_OUTM1, m_outm1);
	DDX_Text(pDX, IDC_OUTM10, m_outm10);
	DDX_Text(pDX, IDC_OUTM11, m_outm11);
	DDX_Text(pDX, IDC_OUTM12, m_outm12);
	DDX_Text(pDX, IDC_OUTM13, m_outm13);
	DDX_Text(pDX, IDC_OUTM14, m_outm14);
	DDX_Text(pDX, IDC_OUTM15, m_outm15);
	DDX_Text(pDX, IDC_OUTM16, m_outm16);
	DDX_Text(pDX, IDC_OUTM17, m_outm17);
	DDX_Text(pDX, IDC_OUTM2, m_outm2);
	DDX_Text(pDX, IDC_OUTM3, m_outm3);
	DDX_Text(pDX, IDC_OUTM4, m_outm4);
	DDX_Text(pDX, IDC_OUTM5, m_outm5);
	DDX_Text(pDX, IDC_OUTM6, m_outm6);
	DDX_Text(pDX, IDC_OUTM7, m_outm7);
	DDX_Text(pDX, IDC_OUTM8, m_outm8);
	DDX_Text(pDX, IDC_OUTM9, m_outm9);
	DDX_Text(pDX, IDC_R12, m_r12);
	DDX_Text(pDX, IDC_OUT18, m_out18);
	DDX_Text(pDX, IDC_OUT19, m_out19);
	DDX_Text(pDX, IDC_OUTM18, m_outm18);
	DDX_Text(pDX, IDC_OUTM19, m_outm19);
	DDX_Text(pDX, IDC_INSP1, m_insp1);
	DDX_Text(pDX, IDC_INSP2, m_insp2);
	DDX_Text(pDX, IDC_INSP3, m_insp3);
	DDX_Text(pDX, IDC_INSP4, m_insp4);
	DDX_Text(pDX, IDC_INSP5, m_insp5);
	DDX_Text(pDX, IDC_INSP6, m_insp6);
	DDX_Text(pDX, IDC_INSP7, m_insp7);
	DDX_Text(pDX, IDC_INSP8, m_insp8);
	DDX_Text(pDX, IDC_INSP9, m_insp9);
	DDX_Text(pDX, IDC_INSP10, m_insp10);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Data, CDialog)
	//{{AFX_MSG_MAP(Data)
	ON_BN_CLICKED(IDPRINT, OnPrint)
	ON_BN_CLICKED(IDC_BUTTONEARLY, OnButtonearly)
	ON_BN_CLICKED(IDC_BUTTONLATE, OnButtonlate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Data message handlers

BOOL Data::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pdata=this;
	theapp=(CBottleApp*)AfxGetApp();

	m_insp1.Format("I1 %s",	theapp->inspctn[1].name);
	m_insp2.Format("I2 %s",	theapp->inspctn[2].name);
	m_insp3.Format("I3 %s",	theapp->inspctn[3].name);
	m_insp4.Format("I4 %s",	theapp->inspctn[4].name);
	m_insp5.Format("I5 %s",	theapp->inspctn[5].name);
	m_insp6.Format("I6 %s",	theapp->inspctn[6].name);
	m_insp7.Format("I7 %s",	theapp->inspctn[7].name);
	m_insp8.Format("I8 %s",	theapp->inspctn[8].name);
	m_insp9.Format("I9 %s",	theapp->inspctn[9].name);
	m_insp10.Format("I10 %s",theapp->inspctn[10].name);
//	m_insp11.Format("I10 %s",theapp->inspctn[11].name);

	m_r1.Format("%3i",		pframe->m_pxaxis->E1Cnt);	//CapHeight
	m_r2.Format(" %3i",		pframe->m_pxaxis->E2Cnt);	//SportCap OR MidCap
	m_r3.Format(" %3i",		pframe->m_pxaxis->E3Cnt);	//CapColor
	m_r4.Format(" %3i",		pframe->m_pxaxis->E4Cnt);	//Neck Integrity
	m_r5.Format(" %3i",		pframe->m_pxaxis->E5Cnt);	//Height
	m_r6.Format(" %3i",		pframe->m_pxaxis->E6Cnt);	//Seam
	m_r7.Format(" %3i",		pframe->m_pxaxis->E7Cnt);	//Label Integrity
	m_r8.Format(" %3i",		pframe->m_pxaxis->E8Cnt);	//No Label
	m_r9.Format(" %3i",		pframe->m_pxaxis->E9Cnt);	//Splice
	m_r10.Format(" %3i",	pframe->m_pxaxis->E10Cnt);	//Tear Label
	m_r11.Format(" %3i",	pframe->m_pxaxis->E11Cnt);	//Waist

	m_r11.Format("%7i",	theapp->Total);
	m_r12.Format(" %3i",0);								//Neck pframe->m_pxaxis->E4Cnt
	m_r15.Format(" %7i",pframe->Rejects);

	m_24hrtime.Format("%2i",theapp->hourClock);	

	m_out0.Format("%6i",	theapp->out0);
	m_out1.Format("%6i",	theapp->out1);
	m_out2.Format("%6i",	theapp->out2);
	m_out3.Format("%6i",	theapp->out3);
	m_out4.Format("%6i",	theapp->out4);
	m_out5.Format("%6i",	theapp->out5);
	m_out6.Format("%6i",	theapp->out6);
	m_out7.Format("%6i",	theapp->out7);
	m_out8.Format("%6i",	theapp->out8);
	m_out9.Format("%6i",	theapp->out9);
	m_out10.Format("%6i",	theapp->out10);
	m_out11.Format("%6i",	theapp->out11);
	m_out12.Format("%6i",	theapp->out12);
	m_out13.Format("%6i",	theapp->out13);
	m_out14.Format("%6i",	theapp->out14);
	m_out15.Format("%6i",	theapp->out15);
	m_out16.Format("%6i",	theapp->out16);
	m_out17.Format("%6i",	theapp->out17);
	m_out18.Format("%6i",	theapp->out18);
	m_out19.Format("%6i",	theapp->out19);

	m_outm1.Format("%6i",	theapp->out1m);
	m_outm2.Format("%6i",	theapp->out2m);
	m_outm3.Format("%6i",	theapp->out3m);
	m_outm4.Format("%6i",	theapp->out4m);
	m_outm5.Format("%6i",	theapp->out5m);
	m_outm6.Format("%6i",	theapp->out6m);
	m_outm7.Format("%6i",	theapp->out7m);
	m_outm8.Format("%6i",	theapp->out8m);
	m_outm9.Format("%6i",	theapp->out9m);
	m_outm10.Format("%6i",	theapp->out10m);
	m_outm11.Format("%6i",	theapp->out11m);
	m_outm12.Format("%6i",	theapp->out12m);
	m_outm13.Format("%6i",	theapp->out13m);
	m_outm14.Format("%6i",	theapp->out14m);
	m_outm15.Format("%6i",	theapp->out15m);
	m_outm16.Format("%6i",	theapp->out16m);
	m_outm17.Format("%6i",	theapp->out17m);
	m_outm18.Format("%6i",	theapp->out18m);
	m_outm19.Format("%6i",	theapp->out19m);

	m_outh0.Format("%6i",	theapp->outh0s);
	m_outh1.Format("%6i",	theapp->outh1s);
	m_outh2.Format("%6i",	theapp->outh2s);
	m_outh3.Format("%6i",	theapp->outh3s);
	m_outh4.Format("%6i",	theapp->outh4s);
	m_outh5.Format("%6i",	theapp->outh5s);
	m_outh6.Format("%6i",	theapp->outh6s);
	m_outh7.Format("%6i",	theapp->outh7s);

	m_outhm1.Format("%6i",	theapp->outh1ms);
	m_outhm2.Format("%6i",	theapp->outh2ms);
	m_outhm3.Format("%6i",	theapp->outh3ms);
	m_outhm4.Format("%6i",	theapp->outh4ms);
	m_outhm5.Format("%6i",	theapp->outh5ms);
	m_outhm6.Format("%6i",	theapp->outh6ms);
	m_outhm7.Format("%6i",	theapp->outh7ms);

	UpdateData(false);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Data::OnPrint() 
{
	char buf[10];
	int filenum=theapp->SavedDataFile+=1;
	CTime thetime = CTime::GetCurrentTime();
	char* CurrentName="capdat";

	int mth=thetime.GetMonth();
	int day=thetime.GetDay();

	char month[2];
	char dayofweek[2];
	itoa(mth,month,10);
	itoa(day,dayofweek,10);
	
	CString x =itoa(filenum,buf,10);
	CString d = CurrentName;

	char* pFileName = "c:\\silgandata\\";//drive	
	char currentpath[60];

	strcpy(currentpath,pFileName);
	strcat(currentpath,d);
	strcat(currentpath,month);
	strcat(currentpath,dayofweek);
	strcat(currentpath,x);
	strcat(currentpath,".txt");

	DataFile.Open(currentpath, CFile::modeWrite | CFile::modeCreate);

	m_r1.Format("%3i",pframe->m_pxaxis->E1Cnt);
	m_r2.Format(" %3i",pframe->m_pxaxis->E2Cnt);
	m_r3.Format(" %3i",pframe->m_pxaxis->E3Cnt);
	m_r12.Format(" %3i",pframe->m_pxaxis->E4Cnt);
	m_r4.Format(" %3i",0);
	m_r5.Format(" %3i",pframe->m_pxaxis->E5Cnt);
	m_r6.Format(" %3i",pframe->m_pxaxis->E6Cnt);
	m_r7.Format(" %3i",pframe->m_pxaxis->E7Cnt);
	m_r8.Format(" %3i",pframe->m_pxaxis->E8Cnt);
	m_r9.Format(" %3i",pframe->m_pxaxis->E9Cnt);
	m_r10.Format(" %3i",pframe->m_pxaxis->E10Cnt);

	m_r11.Format(" %8i",theapp->Total);
	m_r15.Format(" %5i",pframe->Rejects);

	m_outh0.Format("%6i",theapp->outh0);
	m_outh1.Format("%6i",theapp->outh1);
	m_outh2.Format("%6i",theapp->outh2);
	m_outh3.Format("%6i",theapp->outh3);
	m_outh4.Format("%6i",theapp->outh4);
	m_outh5.Format("%6i",theapp->outh5);
	m_outh6.Format("%6i",theapp->outh6);
	m_outh7.Format("%6i",theapp->outh7);


	m_outhm1.Format("%6i",theapp->outh1m);
	m_outhm2.Format("%6i",theapp->outh2m);
	m_outhm3.Format("%6i",theapp->outh3m);
	m_outhm4.Format("%6i",theapp->outh4m);
	m_outhm5.Format("%6i",theapp->outh5m);
	m_outhm6.Format("%6i",theapp->outh6m);
	m_outhm7.Format("%6i",theapp->outh7m);

	DataFile.Write("CapHeight",9);
	DataFile.Write("\t",1);
	DataFile.Write(m_r1,m_r1.GetLength());
	
	DataFile.Write("\n",1);

	DataFile.Write("SportCap",8);
	DataFile.Write("\t",1);
	DataFile.Write(m_r2,m_r2.GetLength());
	
	DataFile.Write("\n",1);

	DataFile.Write("CapColor",8);
	DataFile.Write("\t",1);
	DataFile.Write(m_r3,m_r3.GetLength());
	
	DataFile.Write("\n",1);

	DataFile.Write("Neck", 4);
	DataFile.Write("\t",1);
	DataFile.Write(m_r12,m_r12.GetLength());
	
	DataFile.Write("\n",1);
	
	DataFile.Write("FillLevel", 9);
	DataFile.Write("\t",1);
	DataFile.Write(m_r4,m_r4.GetLength());
	
	DataFile.Write("\n",1);

	DataFile.Write("LabelHeight",11);
	DataFile.Write("\t",1);
	DataFile.Write(m_r5,m_r5.GetLength());
	
	DataFile.Write("\n",1);

	DataFile.Write("Rotate",6);
	DataFile.Write("\t",1);
	DataFile.Write(m_r6,m_r6.GetLength());
	
	DataFile.Write("\n",1);

	DataFile.Write("LabelIntegrity",14);
	DataFile.Write("\t",1);
	DataFile.Write(m_r7,m_r7.GetLength());
	
	DataFile.Write("\n",1);

	DataFile.Write("NoLabel",7);
	DataFile.Write("\t",1);
	DataFile.Write(m_r8,m_r8.GetLength());
	
	DataFile.Write("\n",1);

	DataFile.Write("H0",2);
	DataFile.Write("\t",1);
	DataFile.Write(m_outh0,m_outh0.GetLength());
	
	DataFile.Write("\n",1);

	DataFile.Write("H1up",4);
	DataFile.Write("\t",1);
	DataFile.Write(m_outh1,m_outh1.GetLength());
	
	DataFile.Write("\n",1);
	
	DataFile.Write("H2up",4);
	DataFile.Write("\t",1);
	DataFile.Write(m_outh2,m_outh2.GetLength());
	
	DataFile.Write("\n",1);

	DataFile.Write("H3up",4);
	DataFile.Write("\t",1);
	DataFile.Write(m_outh3,m_outh3.GetLength());
	DataFile.Write(", ",1);
	DataFile.Write("\n",1);

	DataFile.Write("H4up",4);
	DataFile.Write("\t",1);
	DataFile.Write(m_outh4,m_outh4.GetLength());
	
	DataFile.Write("\n",1);

	DataFile.Write("H5up",4);
	DataFile.Write("\t",1);
	DataFile.Write(m_outh5,m_outh5.GetLength());
	
	DataFile.Write("\n",1);

	DataFile.Write("H6up",4);
	DataFile.Write("\t",1);
	DataFile.Write(m_outh6,m_outh6.GetLength());
	DataFile.Write("\n",1);

	DataFile.Write("Hup+",4);
	DataFile.Write("\t",1);
	DataFile.Write(m_outh7,m_outh7.GetLength());
	DataFile.Write("\n",1);

	DataFile.Write("H1dn",4);
	DataFile.Write("\t",1);
	DataFile.Write(m_outhm1,m_outhm1.GetLength());
	DataFile.Write("\n",1);
	
	DataFile.Write("H2dn",4);
	DataFile.Write("\t",1);
	DataFile.Write(m_outhm2,m_outhm2.GetLength());
	DataFile.Write("\n",1);

	DataFile.Write("H3dn",4);
	DataFile.Write("\t",1);
	DataFile.Write(m_outhm3,m_outhm3.GetLength());
	DataFile.Write("\n",1);

	DataFile.Write("H4dn",4);
	DataFile.Write("\t",1);
	DataFile.Write(m_outhm4,m_outhm4.GetLength());
	DataFile.Write("\n",1);

	DataFile.Write("H5dn",4);
	DataFile.Write("\t",1);
	DataFile.Write(m_outhm5,m_outhm5.GetLength());
	DataFile.Write("\n",1);

	DataFile.Write("H6dn",4);
	DataFile.Write("\t",1);
	DataFile.Write(m_outhm6,m_outhm6.GetLength());
	DataFile.Write("\n",1);

	DataFile.Write("Hdn+",4);
	DataFile.Write("\t",1);
	DataFile.Write(m_outhm7,m_outhm7.GetLength());
	DataFile.Write("\n",1);

	DataFile.Write("R0",2);
	DataFile.Write("\t",1);
	DataFile.Write(m_out0,m_out0.GetLength());
	DataFile.Write("\n",1);

	DataFile.Write("Total",5);
	DataFile.Write("\t",1);
	DataFile.Write(m_r11,m_r11.GetLength());
	DataFile.Write("\n",1);

	DataFile.Write("Rejects",7);
	DataFile.Write("\t",1);
	DataFile.Write(m_r15,m_r15.GetLength());
	DataFile.Write("\n",1);

	DataFile.Close();
}

void Data::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

void Data::OnButtonearly() 
{
	theapp->hourClock-=1;
	if(theapp->hourClock<=1) theapp->hourClock=1;
	m_24hrtime.Format("%2i",theapp->hourClock);

	UpdateData(false);
}

void Data::OnButtonlate() 
{
	theapp->hourClock+=1;
	if(theapp->hourClock>=24) theapp->hourClock=24;
	m_24hrtime.Format("%2i",theapp->hourClock);

	UpdateData(false);
}
