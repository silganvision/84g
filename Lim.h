#if !defined(AFX_LIM_H__BECFB756_F84B_424A_8A61_4C5B16B1F18B__INCLUDED_)
#define AFX_LIM_H__BECFB756_F84B_424A_8A61_4C5B16B1F18B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Lim.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Lim dialog

class Lim : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
// Construction
public:

	Lim(CWnd* pParent = NULL);   // standard constructor
	
	void	UpdateDisplay();
	void	UpdateValues();
	void	UpdateVisibilityInspection12();
	bool	fine;
	int		AbsoluteMax;
	int		AbsoluteMax2;

	
	double	res;
	bool	Max;

	CBottleApp *theapp;
	CMainFrame *pframe;
	LPTSTR static_m_1;
	LPTSTR static_m_2;
	LPTSTR static_m_3;
	LPTSTR static_m_4;
	LPTSTR static_m_5;
	LPTSTR static_m_6;
	LPTSTR static_m_7;
	LPTSTR static_m_8;
	LPTSTR static_m_9;
	LPTSTR static_m_10;

	LPTSTR static_m_m1;
	LPTSTR static_m_m2;
	LPTSTR static_m_m3;
	LPTSTR static_m_m4;
	LPTSTR static_m_m5;
	LPTSTR static_m_m6;
	LPTSTR static_m_m7;
	LPTSTR static_m_m8;
	LPTSTR static_m_m9;
	LPTSTR static_m_m10;

	LPTSTR static_m_l1;
	LPTSTR static_m_l2;
	LPTSTR static_m_l3;
	LPTSTR static_m_l4;
	LPTSTR static_m_l5;
	LPTSTR static_m_l6;
	LPTSTR static_m_l7;
	LPTSTR static_m_l8;
	LPTSTR static_m_l9;
	LPTSTR static_m_l10;

	LPTSTR static_m_1max;
	LPTSTR static_m_2max;
	LPTSTR static_m_3max;
	LPTSTR static_m_4max;
	LPTSTR static_m_5max;
	LPTSTR static_m_6max;
	LPTSTR static_m_7max;
	LPTSTR static_m_8max;
	LPTSTR static_m_9max;
	LPTSTR static_m_10max;

	LPTSTR static_m_1min;
	LPTSTR static_m_2min;
	LPTSTR static_m_3min;
	LPTSTR static_m_4min;
	LPTSTR static_m_5min;
	LPTSTR static_m_6min;
	LPTSTR static_m_7min;
	LPTSTR static_m_8min;
	LPTSTR static_m_9min;
	LPTSTR static_m_10min;


// Dialog Data
	//{{AFX_DATA(Lim)
	enum { IDD = IDD_LIM };
	CButton	mbutt_enabl12;
	CButton	mbutt_enab11;
	CButton	m_fine;
	CButton	mbutt_enab9;
	CButton	mbutt_enab7;
	CButton	mbutt_enab6;
	CButton	mbutt_enab5;
	CButton	mbutt_enab4;
	CButton	mbutt_enab3;
	CButton	mbutt_enab2;
	CButton	mbutt_enab10;
	CButton	mbutt_enab1;
	CButton	mbutt_enab8;
	CButton	m_10up;
	CButton	m_10dn;
	CStatic	m_name4;
	CButton	m_name3;
	CButton	m_6upc;
	CButton	m_6dnc;
	CEdit	m_6c;
	CButton	m_1up;
	CButton	m_1dn;
	CString	m_1;
	CString	m_2;
	CString	m_5;
	CString	m_4;
	CString	m_3;
	CString	m_6;
	CString	m_8;
	CString	m_7;
	CString	m_m1;
	CString	m_m2;
	CString	m_m3;
	CString	m_m4;
	CString	m_m5;
	CString	m_m6;
	CString	m_m7;
	CString	m_m8;
	CString	m_l2;
	CString	m_l1;
	CString	m_l3;
	CString	m_l4;
	CString	m_l5;
	CString	m_l6;
	CString	m_l7;
	CString	m_l8;
	CString	m_1max;
	CString	m_2max;
	CString	m_3max;
	CString	m_4max;
	CString	m_5max;
	CString	m_6max;
	CString	m_7max;
	CString	m_8max;
	CString	m_1min;
	CString	m_2min;
	CString	m_3min;
	CString	m_4min;
	CString	m_5min;
	CString	m_6min;
	CString	m_7min;
	CString	m_8min;
	CString	m_10;
	CString	m_l10;
	CString	m_m10;
	CString	m_10max;
	CString	m_10min;
	CString	m_9;
	CString	m_l9;
	CString	m_m9;
	CString	m_9max;
	CString	m_9min;
	CString	m_11;
	CString	m_l11;
	CString	m_m11;
	CString	m_11min;
	CString	m_11max;
	CString	m_12;
	CString	m_l12;
	CString	m_m12;
	CString	m_12min;
	CString	m_12max;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Lim)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Lim)
	virtual BOOL OnInitDialog();
	afx_msg void On1up();
	afx_msg void On1dn();
	afx_msg void On2up();
	afx_msg void On2dn();
	afx_msg void On3up();
	afx_msg void On3dn();
	afx_msg void On4up();
	afx_msg void On4dn();
	afx_msg void On5up();
	afx_msg void On5dn();
	afx_msg void On6up();
	afx_msg void On6dn();
	afx_msg void On7up();
	afx_msg void On7dn();
	afx_msg void On8up();
	afx_msg void On8dn();
	afx_msg void OnMax();
	afx_msg void OnMin();
	afx_msg void OnFast();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void On7up2();
	afx_msg void On7dn2();
	afx_msg void On8up2();
	afx_msg void On8dn2();
	afx_msg void On10up();
	afx_msg void On10dn();
	afx_msg void On10up2();
	afx_msg void On10dn2();
	afx_msg void On9up();
	afx_msg void On9dn();
	afx_msg void OnE8();
	afx_msg void OnE1();
	afx_msg void OnE2();
	afx_msg void OnE3();
	afx_msg void OnE4();
	afx_msg void OnE5();
	afx_msg void OnE6();
	afx_msg void OnE7();
	afx_msg void OnE9();
	afx_msg void OnE10();
	afx_msg void OnFine();
	afx_msg void OnE11();
	afx_msg void On11up();
	afx_msg void On11dn();
	afx_msg void OnE12();
	afx_msg void On12up();
	afx_msg void On12dn();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LIM_H__BECFB756_F84B_424A_8A61_4C5B16B1F18B__INCLUDED_)
