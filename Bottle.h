// Bottle.h : main header file for the BOTTLE application
//

#if !defined(AFX_BOTTLE_H__0B2B9D93_1486_4214_BA97_83D87AE444E7__INCLUDED_)
#define AFX_BOTTLE_H__0B2B9D93_1486_4214_BA97_83D87AE444E7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#include <atomic>


#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif




#include "resource.h"       // main symbols




#define WM_INSPECT (WM_USER + 101)
#define WM_SAVE (WM_USER + 102)
#define WM_TRIGGER (WM_USER + 103)
#define WM_NEWJOB (WM_USER + 104)
#define WM_RELOAD (WM_USER + 105)
#define WM_SETLIGHTLEVEL (WM_USER + 106)
#define WM_UPDATEVIEW (WM_USER + 107)
#define WM_UPDATEVIEW2 (WM_USER + 108)
#define WM_UPDATEVIEW3 (WM_USER + 109)
#define WM_UPDATEVIEW4 (WM_USER + 110)

#define MAXNUMDEFPIC 500 // for 1000 pictures we need at least 2GB in memory


struct Inspctn
{
	CString name;
	int jobNum;

	int enable;
	int type;
	int type2;

	int min;	int max;
	int lmin;	int lmax;
	int lminB;	int lmaxB;
	int lminC;	int lmaxC;
};

struct JobInfo
{

	//Label Mating 

	bool showLabelMating;

	int labelMatingLeftBound;
	int labelMatingRightBound;
	int labelMatingTopBound;
	int labelMatingBottomBound;

	int labelMatingTeachColorLeftBound;
	int labelMatingTeachColorRightBound;
	int labelMatingTeachColorTopBound;
	int labelMatingTeachColorBottomBound;

	int labelMatingTaughtR;
	int labelMatingTaughtG;
	int labelMatingTaughtB;

	int labelMatingTaughtRTolerance;
	int labelMatingTaughtGTolerance;
	int labelMatingTaughtBTolerance;

	//Waist cam1
	int cam1WaistSelectionLeftBound;
	int	cam1WaistSelectionRightBound;
	int	cam1WaistSelectionTopBound;
	int	cam1WaistSelectionBottomBound;

	int cam1WaistMeasurementUpperLeftBound;
	int cam1WaistMeasurementUpperRightBound;
	int cam1WaistMeasurementUpperTopBound;
	int cam1WaistMeasurementUpperBottomBound;

	int cam1WaistMeasurementLeftBound;
	int cam1WaistMeasurementRightBound;
	int cam1WaistMeasurementTopBound;
	int cam1WaistMeasurementBottomBound;
	
	int cam1WaistEdgeThreshold;
	int cam1WaistVerticalSearchLength;
	int cam1WaistHorizontalSearchLength;

	int cam1WaistAvgWindowSize;
	int cam1WaistEdgeCoordinateDeviation;
	int	cam1WaistfillTopToBottom;

	int	cam1WaistWidthThreshold;
	int	cam1WaistWidthOverlayStartX;
	int	cam1WaistWidthOverlayEndX;
	int	cam1WaistWidthOverlayY;

//Wiast cam4
	int cam4WaistMeasurementUpperLeftBound;
	int cam4WaistMeasurementUpperRightBound;
	int cam4WaistMeasurementUpperTopBound;
	int cam4WaistMeasurementUpperBottomBound;

	int cam4WaistMeasurementLeftBound;
	int cam4WaistMeasurementRightBound;
	int cam4WaistMeasurementTopBound;
	int cam4WaistMeasurementBottomBound;
	
	int cam4WaistEdgeThreshold;
	int cam4WaistVerticalSearchLength;
	int cam4WaistHorizontalSearchLength;

	int cam4WaistAvgWindowSize;
	int cam4WaistEdgeCoordinateDeviation;
	int	cam4WaistfillTopToBottom;

	int	cam4WaistWidthThreshold;
	int	cam4WaistWidthOverlayStartX;
	int	cam4WaistWidthOverlayEndX;
	int	cam4WaistWidthOverlayY;

//Wiast cam2
	int cam2WaistSelectionLeftBound;
	int	cam2WaistSelectionRightBound;
	int	cam2WaistSelectionTopBound;
	int	cam2WaistSelectionBottomBound;

	int cam2WaistMeasurementUpperLeftBound;
	int cam2WaistMeasurementUpperRightBound;
	int cam2WaistMeasurementUpperTopBound;
	int cam2WaistMeasurementUpperBottomBound;

	int cam2WaistMeasurementLeftBound;
	int cam2WaistMeasurementRightBound;
	int cam2WaistMeasurementTopBound;
	int cam2WaistMeasurementBottomBound;
	
	int cam2WaistEdgeThreshold;
	int cam2WaistVerticalSearchLength;
	int cam2WaistHorizontalSearchLength;

	int cam2WaistAvgWindowSize;
	int cam2WaistEdgeCoordinateDeviation;
	int	cam2WaistfillTopToBottom;

	int	cam2WaistWidthThreshold;
	int	cam2WaistWidthOverlayStartX;
	int	cam2WaistWidthOverlayEndX;
	int	cam2WaistWidthOverlayY;

//Wiast cam3
	int cam3WaistMeasurementUpperLeftBound;
	int cam3WaistMeasurementUpperRightBound;
	int cam3WaistMeasurementUpperTopBound;
	int cam3WaistMeasurementUpperBottomBound;

	int cam3WaistMeasurementLeftBound;
	int cam3WaistMeasurementRightBound;
	int cam3WaistMeasurementTopBound;
	int cam3WaistMeasurementBottomBound;
	
	int cam3WaistEdgeThreshold;
	int cam3WaistVerticalSearchLength;
	int cam3WaistHorizontalSearchLength;

	int cam3WaistAvgWindowSize;
	int cam3WaistEdgeCoordinateDeviation;
	int	cam3WaistfillTopToBottom;

	int	cam3WaistWidthThreshold;
	int	cam3WaistWidthOverlayStartX;
	int	cam3WaistWidthOverlayEndX;
	int	cam3WaistWidthOverlayY;

	int waistInspectionCameraToUse;
//-----------------------------------------

	int		method5_Rt;
	int		method5_Gt;
	int		method5_Bt;
	int		method5_Ht;

	int		method5_Rb;
	int		method5_Gb;
	int		method5_Bb;
	int		method5_Hb;

	int		boxYt;
	int		boxWt;
	int		boxHt;

	int		boxYb;
	int		boxWb;
	int		boxHb;

	int		vertBarX;
	int		midBarY;
	int		neckBarY;
	int		totinsp;
	bool	taught;

	double	lightLevel1;
	double	lightLevel2;
	double	lightLevel3;
	double	lightLevel4;
	double	lightLevel5;
	
	CString jobname;

	int		motPos;
	int		profile;
	int		u2Thresh1;
	int		u2Thresh2;
	int		u2Pos;
	int		lipX;
	int		lDist;
	int		labelThresh;
	bool	active;
	bool	decCutEnable;
	int		intThresh;
	int		lip;
	int		sensitivity;
	int		enable;
	int		dark;
	
	int		notchSen;
	int		labelSen;
	int		colorTargR;
	int		colorTargG;
	int		colorTargB;

	int		colorTargR2;
	int		colorTargG2;
	int		colorTargB2;
	int		colorOffset2;
	int		lizardFilter;
	int		colorFilter;
	int		colorFilter2;
	int		type;
	
	int		noLabelOffset;
	int		noLabelOffsetX[8];
	int		noLabelOffsetW;
	int		noLabelOffsetH;

	int		noLabelSen;

	int		shiftedLabSen;
	int		EdgeLabSen;

	int		sensEdge;
	int		sensMet3;
	int		heigMet3;
	int		widtMet3;

	int		heigMet5;
	int		widtMet5;


	int		mainBodySen;
	int		prodName;
	int		bottleColor;

	bool	labelIntegrityUseMethod1;
	bool	labelIntegrityUseMethod2;
	bool	labelIntegrityUseMethod3;
	bool	labelIntegrityUseMethod4;
	bool	labelIntegrityUseMethod5;
	bool	labelIntegrityUseMethod6;

	int		rotatePos;
	bool	insideOut;
	int		neckStrength;
	int		capStrength;
	int		capStrength2;

	int		neckSen;
	int		neckPos;
	int		neckInOut;
	int		neckInOut2;

	int		orienArea;
	int		redFilter;
	int		blueFilter;
	int		greenFilter;
	bool	underLipEnable;
	int		findWhite;
	int		fillSize;
	int		fillOffset;
	int		capColorOffset;
	int		capColorSize;
	int		bottleStyle;
	int		labelColorRectX;
	int		labelColorRectY;
	int		ellipseSen;
	int		rotOffset;

	int		sportPos;
	int		sportSen;
	bool	sport;

	//Variables for Middle Setup
	bool	taughtMiddle;
	bool	RefForMiddleIsLeft[8];
	int		taughtMiddleRefY[8];
	int		taughtMiddleRefW[8];

	int		senMidd;	//Sensitivity to find middle on bottle
	int		MiddImg;	//Image to use at the time of finding middles

	int		stDarkImg;	//Image to filter Dark - Stitch image
	int		stLighImg;	//Image to filter Ligjt- Stitch image
	int		stEdgeImg;	//Image to filter Edge - Stitch image

	///////////////////

	int		ytopLimPatt;
	int		ybotLimPatt;

	int		shiftTopLim[6];
	int		shiftBotLim[6];

	int		shiftWidth;

	int		shiftHorIniLim;
	int		shiftHorEndLim;

	///////////////////
	bool	showMiddles;
	bool	findMiddles;
	bool	showEdges;

	bool	showSetupI5;
	bool	showSetupI6;
	bool	showSetupI7;
	bool	showSetupI8;
	bool	showSetupI9;
	bool	showSetupI10;
	bool	showC5inPictures;

	//////////////////////
	int		ySplice[6];
	int		wSplice;
	int		hSplice;
	int		hSpliceSmBx;
	
	int		spliceVar;
	//////////////////////

	int		middlesM[8];
	int		middlesBarX[8];
	int		middlesBarY[8];

	//Xtra Patterns
	int		tgYpattern;

	//Xtra Patterns
	int		tgXpatt[5];
	int		tgYpatt[5];

	//Method 6 patterns

	int meth6_patt1_x,
		meth6_patt1_y,
		meth6_patt2_x,
		meth6_patt2_y,
		meth6_patt3_x,
		meth6_patt3_y;

	//Method 1 pattern
	int meth1_size_xy;

	//Stitch Variables
	int		stitchY[6];
	int		stitchW;
	int		stitchH;

	bool	showStLigh;
	bool	showStDark;
	bool	showStEdges;
	bool	showStFinal;
	bool	showStbw;

	bool	useStLigh;
	bool	useStDark;
	bool	useStEdge;

	//Edge Sensitivy
	int		senEdges;

	//Stitch Sensitivities
	int		stDarkSen;
	int		stLighSen;
	int		stEdgeSen;

	////////////
	
	//** Seam Variables
	int		seamW;
	int		seamH;
	int		seamOffX;
	int		seamOffY;

	//Areas to Avoid
	int		avoidOffX;

	//Edge Transition
	int		EdgeTransition;

	//Splice look for Dark or Light
	int		SpliceLookFor;

	//Filter Selection Pattern
	int		filterSelPatt;

	//Filter Selection Pattern
	int		filterSelPattCap;

	//Filter Selection No Label
	int		filterSelNolabel;

	//Filter Selection Shifted
	int		filterSelShifted;

	//Filter Selection Splice
	int		filterSelSplice;

	bool useRing;

	/// cocked label parameters from 51R84 Andrew Gawlik 2017-07-14
	int sen_Cocked_Label; //##VK--new inspection for Cocked Label
	int MidRef_cockedLabel[5]; //##VK --YRef variable for Cocked Label Inspection

};

/////////////////////////////////////////////////////////////////////////////
// CBottleApp:
// See Bottle.cpp for the implementation of this class
//

// MACROS TO FIND MAX AND MIN
#define MAX(_a,_b) ((_a)<(_b)?(_b):(_a))
#define MIN(_a,_b) ((_a)<(_b)?(_a):(_b))
#define ABS(_a)    ((_a)<0?-(_a):(_a))


class CBottleApp : public CWinApp
{
	friend class CMainFrame;

public:

	CBottleApp();
	CMainFrame* pframeSub;

	bool alignAvailable;
	bool waistAvailable;
	bool showLargeImages;
	bool showLargeImages2;

	bool Cam1SelectedForWaist;
	bool Cam2SelectedForWaist;

	int		tempSelect;
	float	Temperature;

	bool sleeverEnable;
	bool _CheckGPIOReg;

	bool	passNotFinished;
	bool	enabPicsToShow;

	int		holdTime1;
	
	bool	saveDefects;

	int		SavedDBOffset;
	bool	SavedBottleSensor;
	int		convRatio;
	bool	labelOpen;
	int		tempLimit;
	bool	serPresent;
	int		picNum;
	bool	capEjectorInstalled;
	bool	DoNewLedge;
	bool	showGuide;
	int		Totalx;
	int		lostCam;
	int		redBal;
	int		blueBal;
	bool	resetFromPLC;

	int		out0, out1, out2, out3, out4, out5, out6, out7, out8, out9;
	int		out10,out11,out12,out13,out14,out15,out16,out17,out18,out19;

	int		out1m,out2m,out3m,out4m,out5m,out6m,out7m,out8m,out9m;
	int		out10m,out11m,out12m,out13m,out14m,out15m,out16m,out17m,out18m,out19m;

	int		outh0,outh1,outh2,outh3,outh4,outh5,outh6,outh7;
	int		outh1m,outh2m,outh3m,outh4m,outh5m,outh6m,outh7m;

	int		outh0s,outh1s,outh2s,outh3s,outh4s,outh5s,outh6s,outh7s;
	int		outh1ms,outh2ms,outh3ms,outh4ms,outh5ms,outh6ms,outh7ms;

	bool	tog;
	int		labelpos;
	int		bottlepos;
	int		boltOffset;
	int		boltOffset2;
	bool	XLightOn;
	bool	showMot;

	bool	useDiff51R85Hardware; //use different hardware for 51R85
	bool	cam5Enable;
	bool	showCam5;
	bool	securityEnable;

	int		hourClock;
	int		Rejects24;
	int		Total24;
	bool	teachLabel;
	bool	capOpen;
	bool	tearOpen;

	bool	saveOneC, saveOneC2, saveOneC3, saveOneC4, saveOneC5;
	bool	saveOneSetCol;
	bool	saveManySetCol;
	int		inspDelay;
	bool	StartDelay;

	CString SavedOPPassWord;
	CString	SavedSUPPassWord;
	CString	SavedMANPassWord;

	void ReadRegistery(int JobNum);

	struct Inspctn inspctn[18];
//	struct JobInfo jobinfo[42];

	struct JobInfo jobinfo[100];

	COLORREF savedColor2;
	COLORREF savedColor;

	bool SecOff;
	int SavedEncSim;
	int SavedPhotoeye;
	int SavedEncDur;
	int SavedEncDist;
	int SavedConRejects;
	int SavedConRejects2;
	int SavedIORejects;
	int SavedDelay;

	int sizepattBy12;
	int sizepattBy6;
	int sizepattBy3_2W;
	int sizepattBy3_2H;

	CString SavedPassWord;

	bool lock;
	
	int savedZero;
		
	int cam1Offset;
	int cam2Offset;
	int cam3Offset;
	int cam4Offset;

	int CamSer1, CamSer2, CamSer3, CamSer4, CamSer5;

	float	savedShutter;
	float	savedShutter2;
	int		tropSystem;
	bool	SavedEnableX;
	bool	enableHeartBeat;
	int		trialRadius;
	bool	rejectAllKrones;
	int		timedTrigger;
	int		camOrder;
	int		savedTotal;

	int		lightDuration;
	int		ringDuration;

	bool saveOne, saveOne2, saveOne3, saveOne4, saveOne5;

	bool	statsOpen;
	bool	commProblem;
	bool	UserLip;
	bool	DidTriggerOnce;
	int		cam1XOffset;

	int		SavedTotalJobs;
	float	bandwidth;
	float	bandwidth5;
	int		Total;

	std::atomic<int> camHeight[8];
	std::atomic<int> camWidth[8]; 
	std::atomic<int> camTop[8]; 
	std::atomic<int> camLeft[8];

	bool SavedEnable1, SavedEnable2, SavedEnable3, SavedEnable4;
	bool SavedEnable5, SavedEnable6, SavedEnable7, SavedEnable8, SavedEnable9;

	bool camsPresent;
	
	bool SavedAdvanced;
	bool SavedDownBottle;
	bool SavedCam;
	bool SavedSetup;
	bool SavedLimits;//
	bool SavedEject;//
	bool SavedModify;//
	bool SavedJob;
	bool SavedTech;
	bool SavedEjectDisable;
	int	SavedDeadBand;
	int SavedDataFile;

	std::atomic<int>	whitebalance1Red;	//Camera 1 White Balance Red Component
	std::atomic<int>	whitebalance2Red;	//Camera 2 White Balance Red Component
	std::atomic<int>	whitebalance3Red;	//Camera 3 White Balance Red Component
	std::atomic<int>	whitebalance4Red;	//Camera 2 White Balance Red Component
	std::atomic<int>	whitebalance5Red;	//Camera 3 White Balance Red Component
	std::atomic<int>	whitebalance1Blue;	//Camera 1 White Balance Blue Component
	std::atomic<int>	whitebalance2Blue;	//Camera 2 White Balance Blue Component
	std::atomic<int>	whitebalance3Blue;	//Camera 3 hite Balance Blue Component
	std::atomic<int>	whitebalance4Blue;	//Camera 2 White Balance Blue Component
	std::atomic<int>	whitebalance5Blue;	//Camera 3 hite Balance Blue Component

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBottleApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	COleTemplateServer m_server;
	//{{AFX_MSG(CBottleApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BOTTLE_H__0B2B9D93_1486_4214_BA97_83D87AE444E7__INCLUDED_)
