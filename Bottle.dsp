# Microsoft Developer Studio Project File - Name="Bottle" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Bottle - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Bottle.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Bottle.mak" CFG="Bottle - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Bottle - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Bottle - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""SurroundSCMScci""
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Bottle - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /Zi /O2 /I "C:\FlyCapture2\include" /I "C:\FlyCapture2\include\FC1" /I "C:\Projects\VisSDK\inc" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_AFXDLL" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 GoIO_DLL.lib pgrflycapture.lib pgrflycapturegui.lib hid.lib setupapi.lib /nologo /subsystem:windows /machine:I386 /libpath:"C:\FlyCapture2\lib\FC1" /libpath:"C:\Projects\VisSDK\lib"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "Bottle - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /I "C:\FlyCapture2\include" /I "C:\FlyCapture2\include\FC1" /I "C:\Projects\VisSDK\inc" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Fr /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 hid.lib setupapi.lib pgrflycapture.lib pgrflycapturegui.lib /nologo /subsystem:windows /map /debug /machine:I386 /pdbtype:sept /libpath:"C:\FlyCapture2\lib\FC1" /libpath:"C:\Projects\VisSDK\lib"
# SUBTRACT LINK32 /pdb:none /incremental:no

!ENDIF 

# Begin Target

# Name "Bottle - Win32 Release"
# Name "Bottle - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Advanced.cpp
# End Source File
# Begin Source File

SOURCE=.\Blowoff.cpp
# End Source File
# Begin Source File

SOURCE=.\Bottle.cpp
# End Source File
# Begin Source File

SOURCE=.\Bottle.rc
# End Source File
# Begin Source File

SOURCE=.\BottleDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\BottleDown.cpp
# End Source File
# Begin Source File

SOURCE=.\BottleView.cpp
# End Source File
# Begin Source File

SOURCE=.\Camera.cpp
# End Source File
# Begin Source File

SOURCE=.\Cap.cpp
# End Source File
# Begin Source File

SOURCE=.\CapSetup1.cpp
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\CockedLabel.cpp
# End Source File
# Begin Source File

SOURCE=.\Data.cpp
# End Source File
# Begin Source File

SOURCE=.\DigIO.cpp
# End Source File
# Begin Source File

SOURCE=.\Insp.cpp
# End Source File
# Begin Source File

SOURCE=.\Job.cpp
# End Source File
# Begin Source File

SOURCE=.\LabelColor.cpp
# End Source File
# Begin Source File

SOURCE=.\LabelMatingSetup.cpp
# End Source File
# Begin Source File

SOURCE=.\LabIntegSetup.cpp
# End Source File
# Begin Source File

SOURCE=.\Light.cpp
# End Source File
# Begin Source File

SOURCE=.\Lim.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MiddleSetup.cpp
# End Source File
# Begin Source File

SOURCE=.\Modify.cpp
# End Source File
# Begin Source File

SOURCE=.\Motor.cpp
# End Source File
# Begin Source File

SOURCE=.\MoveHead.cpp
# End Source File
# Begin Source File

SOURCE=.\mscomm.cpp
# End Source File
# Begin Source File

SOURCE=.\Neck.cpp
# End Source File
# Begin Source File

SOURCE=.\NoLabelSetup.cpp
# End Source File
# Begin Source File

SOURCE=.\Photoeye.cpp
# End Source File
# Begin Source File

SOURCE=.\PicRecal.cpp
# End Source File
# Begin Source File

SOURCE=.\Pics.cpp
# End Source File
# Begin Source File

SOURCE=.\Prompt.cpp
# End Source File
# Begin Source File

SOURCE=.\SaveAs.cpp
# End Source File
# Begin Source File

SOURCE=.\Security.cpp
# End Source File
# Begin Source File

SOURCE=.\Serial.cpp
# End Source File
# Begin Source File

SOURCE=.\Setup.cpp
# End Source File
# Begin Source File

SOURCE=.\setupStDarkFilter.cpp
# End Source File
# Begin Source File

SOURCE=.\setupStEdgeFilter.cpp
# End Source File
# Begin Source File

SOURCE=.\setupStLightFilter.cpp
# End Source File
# Begin Source File

SOURCE=.\ShiftedLabel.cpp
# End Source File
# Begin Source File

SOURCE=.\SleeverHeadData.cpp
# End Source File
# Begin Source File

SOURCE=.\Splice.cpp
# End Source File
# Begin Source File

SOURCE=.\SPLITTER.CPP
# End Source File
# Begin Source File

SOURCE=.\Status.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\StitchSetup.cpp
# End Source File
# Begin Source File

SOURCE=.\SysConfig.cpp
# End Source File
# Begin Source File

SOURCE=.\Tech.cpp
# End Source File
# Begin Source File

SOURCE=.\Usb.cpp
# End Source File
# Begin Source File

SOURCE=.\View.cpp
# End Source File
# Begin Source File

SOURCE=.\WaistInspection.cpp
# End Source File
# Begin Source File

SOURCE=.\WaistInspection2.cpp
# End Source File
# Begin Source File

SOURCE=.\WhiteBalance.cpp
# End Source File
# Begin Source File

SOURCE=.\XAxisView.cpp
# End Source File
# Begin Source File

SOURCE=.\YAxisView.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\Advanced.h
# End Source File
# Begin Source File

SOURCE=.\Blowoff.h
# End Source File
# Begin Source File

SOURCE=.\Bottle.h
# End Source File
# Begin Source File

SOURCE=.\BottleDoc.h
# End Source File
# Begin Source File

SOURCE=.\BottleDown.h
# End Source File
# Begin Source File

SOURCE=.\BottleView.h
# End Source File
# Begin Source File

SOURCE=.\Camera.h
# End Source File
# Begin Source File

SOURCE=.\Cap.h
# End Source File
# Begin Source File

SOURCE=.\CapSetup1.h
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.h
# End Source File
# Begin Source File

SOURCE=.\CockedLabel.h
# End Source File
# Begin Source File

SOURCE=.\Data.h
# End Source File
# Begin Source File

SOURCE=.\DigIO.h
# End Source File
# Begin Source File

SOURCE=.\Globals.h
# End Source File
# Begin Source File

SOURCE=.\Insp.h
# End Source File
# Begin Source File

SOURCE=.\Job.h
# End Source File
# Begin Source File

SOURCE=.\LabelColor.h
# End Source File
# Begin Source File

SOURCE=.\LabelMatingSetup.h
# End Source File
# Begin Source File

SOURCE=.\LabIntegSetup.h
# End Source File
# Begin Source File

SOURCE=.\Light.h
# End Source File
# Begin Source File

SOURCE=.\Lim.h
# End Source File
# Begin Source File

SOURCE=.\LimitSingleInstance.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\MiddleSetup.h
# End Source File
# Begin Source File

SOURCE=.\Modify.h
# End Source File
# Begin Source File

SOURCE=.\Motor.h
# End Source File
# Begin Source File

SOURCE=.\MoveHead.h
# End Source File
# Begin Source File

SOURCE=.\mscomm.h
# End Source File
# Begin Source File

SOURCE=.\Neck.h
# End Source File
# Begin Source File

SOURCE=.\NoLabelSetup.h
# End Source File
# Begin Source File

SOURCE=.\Photoeye.h
# End Source File
# Begin Source File

SOURCE=.\PicRecal.h
# End Source File
# Begin Source File

SOURCE=.\Pics.h
# End Source File
# Begin Source File

SOURCE=.\Prompt.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SaveAs.h
# End Source File
# Begin Source File

SOURCE=.\Security.h
# End Source File
# Begin Source File

SOURCE=.\Serial.h
# End Source File
# Begin Source File

SOURCE=.\setup.h
# End Source File
# Begin Source File

SOURCE=.\setupStDarkFilter.h
# End Source File
# Begin Source File

SOURCE=.\setupStEdgeFilter.h
# End Source File
# Begin Source File

SOURCE=.\setupStLightFilter.h
# End Source File
# Begin Source File

SOURCE=.\ShiftedLabel.h
# End Source File
# Begin Source File

SOURCE=.\SleeverHeadData.h
# End Source File
# Begin Source File

SOURCE=.\Splice.h
# End Source File
# Begin Source File

SOURCE=.\SPLITTER.H
# End Source File
# Begin Source File

SOURCE=.\Status.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\StitchSetup.h
# End Source File
# Begin Source File

SOURCE=.\SysConfig.h
# End Source File
# Begin Source File

SOURCE=.\SystemEnums.h
# End Source File
# Begin Source File

SOURCE=.\Tech.h
# End Source File
# Begin Source File

SOURCE=.\Usb.h
# End Source File
# Begin Source File

SOURCE=.\View.h
# End Source File
# Begin Source File

SOURCE=.\WaistInspection.h
# End Source File
# Begin Source File

SOURCE=.\WaistInspection2.h
# End Source File
# Begin Source File

SOURCE=.\WhiteBalance.h
# End Source File
# Begin Source File

SOURCE=.\XAxisView.h
# End Source File
# Begin Source File

SOURCE=.\YAxisView.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\binary1.bin
# End Source File
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00003.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00004.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00005.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00006.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00007.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Bottle.ico
# End Source File
# Begin Source File

SOURCE=.\res\Bottle.rc2
# End Source File
# Begin Source File

SOURCE=.\res\BottleDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\cam4dn.bmp
# End Source File
# Begin Source File

SOURCE=.\res\M6patBy3L.bin
# End Source File
# Begin Source File

SOURCE=.\res\M6patBy3L.bmp
# End Source File
# Begin Source File

SOURCE=.\mdi.ico
# End Source File
# Begin Source File

SOURCE=.\res\method3a.bmp
# End Source File
# Begin Source File

SOURCE=.\res\testbin1.bin
# End Source File
# Begin Source File

SOURCE=.\res\testbin2.bin
# End Source File
# End Group
# Begin Source File

SOURCE=.\blt.txt
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\xtra.txt
# End Source File
# End Target
# End Project
