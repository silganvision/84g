// Neck.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "Neck.h"
#include "mainfrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Neck dialog


Neck::Neck(CWnd* pParent /*=NULL*/)
	: CDialog(Neck::IDD, pParent)
{
	//{{AFX_DATA_INIT(Neck)
	m_editneckpos = _T("");
	m_editnecksen = _T("");
	m_editcappos = _T("");
	//}}AFX_DATA_INIT
}


void Neck::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Neck)
	DDX_Text(pDX, IDC_EDITNECKPOS, m_editneckpos);
	DDX_Text(pDX, IDC_EDITNECKSEN, m_editnecksen);
	DDX_Text(pDX, IDC_RADIO1, m_enable);
	DDX_Text(pDX, IDC_EDITCAPPOS, m_editcappos);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Neck, CDialog)
	//{{AFX_MSG_MAP(Neck)
	ON_BN_CLICKED(IDC_NECKUP, OnNeckup)
	ON_BN_CLICKED(IDC_NECKDN, OnNeckdn)
	ON_BN_CLICKED(IDC_NECKUP2, OnNeckup2)
	ON_BN_CLICKED(IDC_NECKDN2, OnNeckdn2)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_NECKOUT, OnNeckout)
	ON_BN_CLICKED(IDC_NECKIN, OnNeckin)
	ON_BN_CLICKED(IDC_NECKOFFUP, OnNeckoffup)
	ON_BN_CLICKED(IDC_NECKOFFDN, OnNeckoffdn)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Neck message handlers

void Neck::OnNeckup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckPos-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].neckPos<=-6) 
	{theapp->jobinfo[pframe->CurrentJobNum].neckPos=-6;}
	
	m_editcappos.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckPos);
	SetTimer(1,3000,NULL);
	UpdateData(false);	
}

void Neck::OnNeckdn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckPos+=res;
	m_editcappos.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckPos);
	SetTimer(1,3000,NULL);
	UpdateData(false);	
}

void Neck::OnNeckup2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckSen+=res;
	m_editnecksen.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckSen);
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Neck::OnNeckdn2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckSen-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].neckSen<=0) 
	{theapp->jobinfo[pframe->CurrentJobNum].neckSen=0;}

	m_editnecksen.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckSen);
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

BOOL Neck::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pneck=this;
	theapp=(CBottleApp*)AfxGetApp();

	m_editneckpos.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckInOut2);
	m_editnecksen.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckSen);
	m_editcappos.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckPos);

	if(theapp->jobinfo[pframe->CurrentJobNum].underLipEnable==false)
	{CheckDlgButton(IDC_RADIO1,1); m_enable.Format("%s","Disabled");}
	else
	{CheckDlgButton(IDC_RADIO1,0); m_enable.Format("%s","Enabled");}

	res=2;
	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Neck::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{KillTimer(1); pframe->SaveJob(pframe->CurrentJobNum);}

	CDialog::OnTimer(nIDEvent);
}


void Neck::OnRadio1() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].underLipEnable)
	{
		theapp->jobinfo[pframe->CurrentJobNum].underLipEnable=false;
		CheckDlgButton(IDC_RADIO1,1); 
		m_enable.Format("%s","Disabled");
	}
	else
	{
		theapp->jobinfo[pframe->CurrentJobNum].underLipEnable=true;
		CheckDlgButton(IDC_RADIO1,0);
		m_enable.Format("%s","Enabled");
	}

	UpdateData(false);	
}

void Neck::OnNeckout() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckInOut-=1;
	m_editneckpos.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckInOut);
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Neck::OnNeckin() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckInOut+=1;
	m_editneckpos.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckInOut);
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Neck::OnNeckoffup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckInOut2-=1;
	m_editneckpos.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckInOut2);
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Neck::OnNeckoffdn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckInOut2+=1;
	m_editneckpos.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckInOut2);
	SetTimer(1,3000,NULL);
	UpdateData(false);
}
