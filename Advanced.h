#if !defined(AFX_ADVANCED_H__9A6AC3B5_5207_4E1B_8B6D_C74AEB6C3A21__INCLUDED_)
#define AFX_ADVANCED_H__9A6AC3B5_5207_4E1B_8B6D_C74AEB6C3A21__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Advanced.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// Advanced dialog

class Advanced : public CDialog
{
// Construction
public:

	CMainFrame* pframe;
	CBottleApp* theapp;
	Advanced(CWnd* pParent = NULL);   // standard constructor
	float	localTemp;

	void UpdateDisplay();
	int motorHeight;

// Dialog Data
	//{{AFX_DATA(Advanced)
	enum { IDD = IDD_ADVANCED };
	CString	m_editheight;
	CString	m_edittemp;
	CString	m_editcurrent;
	CString	m_editconv;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Advanced)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CString m_enable;
	// Generated message map functions
	//{{AFX_MSG(Advanced)
	afx_msg void OnEncsim();
	virtual BOOL OnInitDialog();
	afx_msg void OnPlcpos();
	afx_msg void OnMore();
	afx_msg void OnLess();
	afx_msg void OnMore2();
	afx_msg void OnLess2();
	afx_msg void OnThigher();
	afx_msg void OnTlower();
	afx_msg void OnOhigher();
	afx_msg void OnOlower();
	virtual void OnOK();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSendresettoplc();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADVANCED_H__9A6AC3B5_5207_4E1B_8B6D_C74AEB6C3A21__INCLUDED_)
