#if !defined(AFX_SECURITY_H__EBA93489_7E32_461D_9F81_B9A1E6D80778__INCLUDED_)
#define AFX_SECURITY_H__EBA93489_7E32_461D_9F81_B9A1E6D80778__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Security.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Security dialog

class Security : public CDialog
{
// Construction
	friend class CMainFrame;
	friend class CBottleApp;
public:
	bool bypassTimer;
	bool Safe1;
	bool Safe2;
	bool Safe;
	CString PassWord;
	CString PassWordX;
	Security(CWnd* pParent = NULL);   // standard constructor
	CString	m_editpass12;
// Dialog Data
	//{{AFX_DATA(Security)
	enum { IDD = IDD_SECURITY };
	CEdit	m_editpass2;
	CString	m_editpass;
	CString	m_manpass;
	CString	m_oppass;
	CString	m_suppass;
	//}}AFX_DATA

	CMainFrame* pframe;
	CBottleApp *theapp;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Security)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Security)
	virtual BOOL OnInitDialog();
	afx_msg void OnB1();
	afx_msg void OnAccept();
	afx_msg void OnB2();
	afx_msg void OnB3();
	afx_msg void OnB4();
	afx_msg void OnB5();
	afx_msg void OnB6();
	afx_msg void OnB7();
	afx_msg void OnB8();
	afx_msg void OnB9();
	afx_msg void OnRsetup();
	afx_msg void OnRlimits();
	afx_msg void OnReject();
	afx_msg void OnRmodify();
	afx_msg void OnB0();
	afx_msg void OnTech();
	afx_msg void OnRjob();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnOK();
	afx_msg void OnChangeop();
	afx_msg void OnChangesup();
	afx_msg void OnChangeman();
	afx_msg void OnBypass();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SECURITY_H__EBA93489_7E32_461D_9F81_B9A1E6D80778__INCLUDED_)
