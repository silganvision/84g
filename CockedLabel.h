#if !defined(AFX_COCKEDLABEL_H__7CE8DB5A_20A5_4B62_9EAA_471FE6666C74__INCLUDED_)
#define AFX_COCKEDLABEL_H__7CE8DB5A_20A5_4B62_9EAA_471FE6666C74__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CockedLabel.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCockedLabel dialog

class CCockedLabel : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
	// Construction
public:
	CCockedLabel(CWnd* pParent = NULL);   // standard constructor
	CBottleApp *theapp;
	CMainFrame* pframe;

private:
	int res;
	int camHeight;
	int camWidth;

// Dialog Data
	//{{AFX_DATA(CCockedLabel)
	enum { IDD = IDD_COCKEDLABEL };
	CButton	m_fineMode;
	CString	m_sens;
	CString	m_yref1;
	CString	m_yref2;
	CString	m_yref3;
	CString	m_yref4;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCockedLabel)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCockedLabel)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnYrefup1();
	afx_msg void OnYrefdw1();
	afx_msg void OnYrefup2();
	afx_msg void OnYrefdw2();
	afx_msg void OnYrefup3();
	afx_msg void OnYrefdw3();
	afx_msg void OnYrefup4();
	afx_msg void OnYrefdw4();
	afx_msg void OnFinemod();
	afx_msg void OnSenmiddup();
	afx_msg void OnSenmidddw();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COCKEDLABEL_H__7CE8DB5A_20A5_4B62_9EAA_471FE6666C74__INCLUDED_)
