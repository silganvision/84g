// View.cpp : implementation file
//

#include "stdafx.h"
#include "Bottle.h"
#include "View.h"
#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// View dialog


View::View(CWnd* pParent /*=NULL*/)
	: CDialog(View::IDD, pParent)
{
	//{{AFX_DATA_INIT(View)
	m_id1 = _T("");
	m_id2 = _T("");
	m_id3 = _T("");
	m_id4 = _T("");
	m_id5 = _T("");
	m_id6 = _T("");
	m_id7 = _T("");
	m_id8 = _T("");
	m_id9 = _T("");
	m_id10 = _T("");
	m_t10 = _T("");
	m_t3 = _T("");
	m_t4 = _T("");
	m_t5 = _T("");
	m_t6 = _T("");
	m_t7 = _T("");
	m_t8 = _T("");
	m_t9 = _T("");
	m_sd1 = _T("");
	m_sd4 = _T("");
	m_sd5 = _T("");
	m_sd6 = _T("");
	m_sd7 = _T("");
	m_sd8 = _T("");
	m_insp1 = _T("");
	m_insp2 = _T("");
	m_insp3 = _T("");
	m_insp4 = _T("");
	m_insp5 = _T("");
	m_insp6 = _T("");
	m_insp7 = _T("");
	m_insp8 = _T("");
	m_sd9 = _T("");
	m_sd10 = _T("");
	m_insp9 = _T("");
	m_time1 = _T("");
	m_time10 = _T("");
	m_time2 = _T("");
	m_time3 = _T("");
	m_time4 = _T("");
	m_time5 = _T("");
	m_time6 = _T("");
	m_time7 = _T("");
	m_time8 = _T("");
	m_time9 = _T("");
	m_picNum = _T("");
	m_rate = _T("");
	m_eject = _T("");
	m_t1 = _T("");
	m_t1b = _T("");
	m_t2 = _T("");
	m_sd1b = _T("");
	m_sd2 = _T("");
	m_sd3 = _T("");
	m_insp10 = _T("");
	m_imgRead1 = _T("");
	m_imgRead2 = _T("");
	m_imgRead3 = _T("");
	m_imgRead4 = _T("");
	m_probCounter = _T("");
	m_time0C1 = _T("");
	m_time0C2 = _T("");
	m_time0C3 = _T("");
	m_time0C4 = _T("");
	m_time0C5 = _T("");
	m_LeftCap = _T("");
	m_RighCap = _T("");
	m_sameImg = _T("");
	m_imgRead5 = _T("");
	m_master = _T("");
	m_finished1 = _T("");
	m_finished2 = _T("");
	m_finished3 = _T("");
	m_finished4 = _T("");
	m_finished5 = _T("");
	m_timeFinalProc = _T("");
	m_timeVisionTotal = _T("");
	m_timeInsDelay = _T("");
	m_insp11 = _T("");
	m_id11 = _T("");
	m_t11 = _T("");
	m_sd11 = _T("");
	m_time11 = _T("");
	//}}AFX_DATA_INIT
}


void View::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(View)
	DDX_Control(pDX, IDC_TBSNAME, m_tbsname);
	DDX_Text(pDX, IDC_1, m_id1);
	DDX_Text(pDX, IDC_2, m_id2);
	DDX_Text(pDX, IDC_3, m_id3);
	DDX_Text(pDX, IDC_4, m_id4);
	DDX_Text(pDX, IDC_5, m_id5);
	DDX_Text(pDX, IDC_6, m_id6);
	DDX_Text(pDX, IDC_7, m_id7);
	DDX_Text(pDX, IDC_8, m_id8);
	DDX_Text(pDX, IDC_9, m_id9);
	DDX_Text(pDX, IDC_10, m_id10);
	DDX_Text(pDX, IDC_T10, m_t10);
	DDX_Text(pDX, IDC_T3, m_t3);
	DDX_Text(pDX, IDC_T4, m_t4);
	DDX_Text(pDX, IDC_T5, m_t5);
	DDX_Text(pDX, IDC_T6, m_t6);
	DDX_Text(pDX, IDC_T7, m_t7);
	DDX_Text(pDX, IDC_T8, m_t8);
	DDX_Text(pDX, IDC_T9, m_t9);
	DDX_Text(pDX, IDC_SD1, m_sd1);
	DDX_Text(pDX, IDC_SD4, m_sd4);
	DDX_Text(pDX, IDC_SD5, m_sd5);
	DDX_Text(pDX, IDC_SD6, m_sd6);
	DDX_Text(pDX, IDC_SD7, m_sd7);
	DDX_Text(pDX, IDC_SD8, m_sd8);
	DDX_Text(pDX, IDC_INSP1, m_insp1);
	DDX_Text(pDX, IDC_INSP2, m_insp2);
	DDX_Text(pDX, IDC_INSP3, m_insp3);
	DDX_Text(pDX, IDC_INSP4, m_insp4);
	DDX_Text(pDX, IDC_INSP5, m_insp5);
	DDX_Text(pDX, IDC_INSP6, m_insp6);
	DDX_Text(pDX, IDC_INSP7, m_insp7);
	DDX_Text(pDX, IDC_INSP8, m_insp8);
	DDX_Text(pDX, IDC_SD9, m_sd9);
	DDX_Text(pDX, IDC_SD10, m_sd10);
	DDX_Text(pDX, IDC_INSP9, m_insp9);
	DDX_Text(pDX, IDC_TIME1, m_time1);
	DDX_Text(pDX, IDC_TIME10, m_time10);
	DDX_Text(pDX, IDC_TIME2, m_time2);
	DDX_Text(pDX, IDC_TIME3, m_time3);
	DDX_Text(pDX, IDC_TIME4, m_time4);
	DDX_Text(pDX, IDC_TIME5, m_time5);
	DDX_Text(pDX, IDC_TIME6, m_time6);
	DDX_Text(pDX, IDC_TIME7, m_time7);
	DDX_Text(pDX, IDC_TIME8, m_time8);
	DDX_Text(pDX, IDC_TIME9, m_time9);
	DDX_Text(pDX, IDC_PICSTOSHOW, m_picNum);
	DDX_Text(pDX, IDC_RATE, m_rate);
	DDX_Text(pDX, IDC_EJECT, m_eject);
	DDX_Text(pDX, IDC_T1, m_t1);
	DDX_Text(pDX, IDC_T1B, m_t1b);
	DDX_Text(pDX, IDC_T2, m_t2);
	DDX_Text(pDX, IDC_SD1B, m_sd1b);
	DDX_Text(pDX, IDC_SD2, m_sd2);
	DDX_Text(pDX, IDC_SD3, m_sd3);
	DDX_Text(pDX, IDC_INSP10, m_insp10);
	DDX_Text(pDX, IDC_READY1, m_imgRead1);
	DDX_Text(pDX, IDC_READY2, m_imgRead2);
	DDX_Text(pDX, IDC_READY3, m_imgRead3);
	DDX_Text(pDX, IDC_READY4, m_imgRead4);
	DDX_Text(pDX, IDC_PROBCOUNTER, m_probCounter);
	DDX_Text(pDX, IDC_PROCTIMEC1, m_time0C1);
	DDX_Text(pDX, IDC_PROCTIMEC2, m_time0C2);
	DDX_Text(pDX, IDC_PROCTIMEC3, m_time0C3);
	DDX_Text(pDX, IDC_PROCTIMEC4, m_time0C4);
	DDX_Text(pDX, IDC_PROCTIMEC5, m_time0C5);
	DDX_Text(pDX, IDC_CAPLEFTVAL, m_LeftCap);
	DDX_Text(pDX, IDC_CAPRIGHVAL, m_RighCap);
	DDX_Text(pDX, IDC_SAMEIMG, m_sameImg);
	DDX_Text(pDX, IDC_READY5, m_imgRead5);
	DDX_Text(pDX, IDC_MASTER, m_master);
	DDX_Text(pDX, IDC_STATUS1, m_finished1);
	DDX_Text(pDX, IDC_STATUS2, m_finished2);
	DDX_Text(pDX, IDC_STATUS3, m_finished3);
	DDX_Text(pDX, IDC_STATUS4, m_finished4);
	DDX_Text(pDX, IDC_STATUS5, m_finished5);
	DDX_Text(pDX, IDC_TIMEFINAPROC, m_timeFinalProc);
	DDX_Text(pDX, IDC_TIMEVISIONTOTAL, m_timeVisionTotal);
	DDX_Text(pDX, IDC_TIMEVISIONINSPDELAY, m_timeInsDelay);
	DDX_Text(pDX, IDC_INSP12, m_insp11);
	DDX_Text(pDX, IDC_12, m_id11);
	DDX_Text(pDX, IDC_T12, m_t11);
	DDX_Text(pDX, IDC_SD12, m_sd11);
	DDX_Text(pDX, IDC_TIME12, m_time11);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(View, CDialog)
	//{{AFX_MSG_MAP(View)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_PICTOSHOWUP, OnPictoshowup)
	ON_BN_CLICKED(IDC_PICTOSHOWDOWN, OnPictoshowdown)
	ON_BN_CLICKED(IDC_ENAPICSTOSHOW, OnEnapicstoshow)
	ON_BN_CLICKED(IDC_MISSPIC1, OnMisspic1)
	ON_BN_CLICKED(IDC_MISSPIC2, OnMisspic2)
	ON_BN_CLICKED(IDC_MISSPIC3, OnMisspic3)
	ON_BN_CLICKED(IDC_MISSPIC4, OnMisspic4)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// View message handlers

BOOL View::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pview2=this;
	theapp=(CBottleApp*)AfxGetApp();

	if(!theapp->waistAvailable)
	{
		GetDlgItem(IDC_INSP12)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_12)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_T12)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SD12)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_TIME12)->ShowWindow(SW_HIDE);
	}	

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void View::UpdateDisplay()
{
	if(theapp->Total<=0 )
	{
		m_rate.Format("%3.2f",	0);
		m_eject.Format("%3.2f",	0);

		m_id1.Format("%3.2f",0);
		m_id2.Format("%3.2f",0);
 		m_id3.Format(" %3i",0);
		m_id4.Format(" %3i",0);
		m_id5.Format(" %3i",0);
		m_id6.Format(" %3i",0);
		m_id7.Format(" %3i",0);
		m_id8.Format(" %3i",0);
		m_id9.Format(" %3i",0);
		m_id10.Format(" %3i",0);
 		m_id11.Format(" %3i",0);	

		m_t1.Format(" %3i",0);
		m_t1b.Format(" %3i",0);
		m_t2.Format(" %3i",0);
		m_t3.Format(" %3i",0);
		m_t4.Format(" %3i",0);
		m_t5.Format(" %3i",0);
		m_t6.Format(" %3i",0);
		m_t7.Format(" %3i",0);
		m_t8.Format(" %3i",0);
		m_t9.Format(" %3i",0);
		m_t10.Format(" %3i",0);
		m_t11.Format(" %3i",0);

		m_sd1.Format("%3.2f",0);
		m_sd1b.Format("%3.2f",0);
		m_sd2.Format("%3.2f",0);
		m_sd3.Format("%3.2f",0);
		m_sd4.Format("%3.2f",0);
		m_sd5.Format("%3.2f",0);
		m_sd6.Format("%3.2f",0);
		m_sd7.Format("%3.2f",0);
		m_sd8.Format("%3.2f",0);
		m_sd9.Format("%3.2f",0);
		m_sd10.Format("%3.2f",0);
		m_sd11.Format("%3.2f",0);

		m_insp1.Format("%s",theapp->inspctn[1].name);
		m_insp2.Format("%s",theapp->inspctn[2].name);
		m_insp3.Format("%s",theapp->inspctn[3].name);
		m_insp4.Format("%s",theapp->inspctn[4].name);
		m_insp5.Format("%s",theapp->inspctn[5].name);
		m_insp6.Format("%s",theapp->inspctn[6].name);
		m_insp7.Format("%s",theapp->inspctn[7].name);
		m_insp8.Format("%s",theapp->inspctn[8].name);
		m_insp9.Format("%s",theapp->inspctn[9].name);
		m_insp10.Format("%s",theapp->inspctn[10].name);
		m_insp11.Format("%s",theapp->inspctn[11].name);

		m_time1.Format("%.03f msec",	0);
		m_time2.Format("%.03f msec",	0);
		m_time3.Format("%.03f msec",	0);		
		m_time4.Format("%.03f msec",	0);
		m_time5.Format("%.03f msec",	0);
		m_time6.Format("%.03f msec",	0);
		m_time7.Format("%.03f msec",	0);
		m_time8.Format("%.03f msec",	0);
		m_time9.Format("%.03f msec",	0);
		m_time10.Format("%.03f msec",	0);
		m_time11.Format("%.03f msec",	0);
	}
	else
	{
		m_rate.Format("%3.2f",	pframe->BottleRate);
		m_eject.Format("%3.2f",	pframe->EjectRate);

		m_id1.Format(" %3i",	pframe->m_pxaxis->e1Cnt);
		m_id2.Format(" %3i",	pframe->m_pxaxis->e2Cnt);
		m_id3.Format(" %3i",	pframe->m_pxaxis->e3Cnt);
		m_id4.Format(" %3i",	pframe->m_pxaxis->e4Cnt);
		m_id5.Format(" %3i",	pframe->m_pxaxis->e5Cnt);
		m_id6.Format(" %3i",	pframe->m_pxaxis->e6Cnt);
		m_id7.Format(" %3i",	pframe->m_pxaxis->e7Cnt);
		m_id8.Format(" %3i",	pframe->m_pxaxis->e8Cnt);
		m_id9.Format(" %3i",	pframe->m_pxaxis->e9Cnt);
		m_id10.Format(" %3i",	pframe->m_pxaxis->e10Cnt);
		m_id11.Format(" %3i",	pframe->m_pxaxis->e11Cnt);

		m_t1.Format("%3.2f",	pframe->m_pxaxis->LHt);
		m_t1b.Format("%3.2f",	pframe->m_pxaxis->RHt);
		m_t2.Format("%3.2f",	pframe->m_pxaxis->Dt);
		m_t3.Format("%3.2f",	pframe->m_pxaxis->Ft);
		m_t4.Format("%3.2f",	pframe->m_pxaxis->Lt);
		m_t5.Format("%3.2f",	pframe->m_pxaxis->TBt);
		m_t6.Format("%3.2f",	pframe->m_pxaxis->TB2t);
		m_t7.Format("%3.2f",	pframe->m_pxaxis->Pt);
		m_t8.Format("%3.2f",	pframe->m_pxaxis->Not);
		m_t9.Format("%3.2f",	pframe->m_pxaxis->LCt);
		m_t10.Format("%3.2f",	pframe->m_pxaxis->TLt);
		m_t11.Format("%3.2f",	pframe->m_pxaxis->Wst);

		m_sd1.Format("%3.2f",	pframe->m_pxaxis->LHsd);
		m_sd1b.Format("%3.2f",	pframe->m_pxaxis->RHsd);
 		m_sd2.Format("%3.2f",	pframe->m_pxaxis->Dsd);
		m_sd3.Format("%3.2f",	pframe->m_pxaxis->Fsd);
		m_sd4.Format("%3.2f",	pframe->m_pxaxis->Lsd);
		m_sd5.Format("%3.2f",	pframe->m_pxaxis->TBsd);
		m_sd6.Format("%3.2f",	pframe->m_pxaxis->TB2sd);
		m_sd7.Format("%3.2f",	pframe->m_pxaxis->Psd);		
		m_sd8.Format("%3.2f",	pframe->m_pxaxis->Nosd);
		m_sd9.Format("%3.2f",	pframe->m_pxaxis->LCsd);
		m_sd10.Format("%3.2f",	pframe->m_pxaxis->TLsd);
		m_sd11.Format("%3.2f",	pframe->m_pxaxis->Wssd);
		////////////
		
		m_time0C1.Format("%03.01fms",	float(pframe->m_pxaxis->timeMicroSecondC1I0) );
		m_time0C2.Format("%03.01fms",	float(pframe->m_pxaxis->timeMicroSecondC2I0) );
		m_time0C3.Format("%03.01fms",	float(pframe->m_pxaxis->timeMicroSecondC3I0) );
		m_time0C4.Format("%03.01fms",	float(pframe->m_pxaxis->timeMicroSecondC4I0) );
		m_time0C5.Format("%03.01fms",	float(pframe->m_pxaxis->timeMicroSecondC5I0) );
	
		m_time1.Format("%.03fms",	float(pframe->m_pxaxis->dMicroSecondC1I1) );
		m_time2.Format("%.03fms",	float(pframe->m_pxaxis->dMicroSecondC1I2) );
		m_time3.Format("%.03fms",	float(pframe->m_pxaxis->dMicroSecondC1I3) );		
		m_time4.Format("%.03fms",	float(pframe->m_pxaxis->dMicroSecondC1I4) );
		m_time5.Format("%.03fms",	float(pframe->m_pxaxis->dMicroSecondC1I5) );
		m_time6.Format("%.03fms",	float(pframe->m_pxaxis->dMicroSecondC1I6) );
		m_time7.Format("%.03fms",	float(pframe->m_pxaxis->dMicroSecondC1I7) );
		m_time8.Format("%.03fms",	float(pframe->m_pxaxis->dMicroSecondC1I8) );
		m_time9.Format("%.03fms",	float(pframe->m_pxaxis->dMicroSecondC1I9) );
		m_time10.Format("%.03fms",	float(pframe->m_pxaxis->dMicroSecondC1I10) );
		m_time11.Format("%.03fms",	float(pframe->m_pxaxis->dMicroSecondC1I11) );
								
		m_timeFinalProc.Format("%.03f msec",	float(pframe->m_pxaxis->spanElapsed) );
		m_timeInsDelay.Format("%.03f msec",		float(theapp->inspDelay));
		m_timeVisionTotal.Format("%.03f msec",	float(theapp->inspDelay) + float(pframe->m_pxaxis->spanElapsed) );
		
		m_imgRead1.Format("%i", pframe->countC1.load());
		m_imgRead2.Format("%i", pframe->countC2.load());
		m_imgRead3.Format("%i", pframe->countC3.load());
		m_imgRead4.Format("%i", pframe->countC4.load());
		m_imgRead5.Format("%i", pframe->countC5.load());

		m_master.Format("%i",	pframe->countC0.load());

		m_finished1.Format("%i", pframe->m_pxaxis->cameraDone[1][pframe->countC0.load()]);
		m_finished2.Format("%i", pframe->m_pxaxis->cameraDone[2][pframe->countC0.load()]);
		m_finished3.Format("%i", pframe->m_pxaxis->cameraDone[3][pframe->countC0.load()]);
		m_finished4.Format("%i", pframe->m_pxaxis->cameraDone[4][pframe->countC0.load()]);
		m_finished5.Format("%i", pframe->m_pxaxis->cameraDone[5][pframe->countC0.load()]);

		m_probCounter.Format("%i", pframe->notFinished);
		m_sameImg.Format("%i", pframe->sameImg);

		m_LeftCap.Format("%i",	pframe->m_pxaxis->capLeftVal);
		m_RighCap.Format("%i",	pframe->m_pxaxis->capRighVal);
	}

	m_picNum.Format("%i", pframe->pictoShow);

	if(theapp->enabPicsToShow){CheckDlgButton(IDC_ENAPICSTOSHOW,1);}
	else							{CheckDlgButton(IDC_ENAPICSTOSHOW,0);}

	UpdateData(false);
	InvalidateRect(NULL,false);
}

void View::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	COLORREF bluecolor = RGB(0,0,150);
	CPen GreenPen(PS_SOLID, 1, RGB(0,255,0));
	dc.SetBkMode(TRANSPARENT);
	dc.SelectObject(&GreenPen);
}

void View::OnPictoshowup() 
{
	pframe->pictoShow++;	

	if(pframe->pictoShow>=49)	{pframe->pictoShow=49;}

	UpdateDisplay();
}

void View::OnPictoshowdown() 
{
	pframe->pictoShow--;	

	if(pframe->pictoShow<=1)	{pframe->pictoShow=1;}
	
	UpdateDisplay();	
}

void View::OnEnapicstoshow() 
{
	if( theapp->enabPicsToShow)
	{CheckDlgButton(IDC_ENAPICSTOSHOW,0); theapp->enabPicsToShow=false;}
	else
	{CheckDlgButton(IDC_ENAPICSTOSHOW,1); theapp->enabPicsToShow=true;}
		
	UpdateData(false);		
}

void View::UpdateDisplay2()
{
	m_insp1.Format("%s",theapp->inspctn[1].name);
	m_insp2.Format("%s",theapp->inspctn[2].name);
	m_insp3.Format("%s",theapp->inspctn[3].name);
	m_insp4.Format("%s",theapp->inspctn[4].name);
	m_insp5.Format("%s",theapp->inspctn[5].name);
	m_insp6.Format("%s",theapp->inspctn[6].name);
	m_insp7.Format("%s",theapp->inspctn[7].name);
	m_insp8.Format("%s",theapp->inspctn[8].name);
	m_insp9.Format("%s",theapp->inspctn[9].name);
	m_insp10.Format("%s",theapp->inspctn[10].name);
	m_insp11.Format("%s",theapp->inspctn[11].name);
	m_insp12.Format("%s",theapp->inspctn[12].name);

	UpdateData(false);
	InvalidateRect(NULL,false);
}

void View::OnMisspic1() 
{
	pframe->missPic1 = true;	
}

void View::OnMisspic2() 
{
	pframe->missPic2 = true;		
}

void View::OnMisspic3() 
{
	pframe->missPic3 = true;		
}

void View::OnMisspic4() 
{
	pframe->missPic4 = true;		
}
