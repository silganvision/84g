#if !defined(AFX_SETUPSTDARKFILTER_H__50A08F98_35ED_4DD7_9E75_59FEA81C7D45__INCLUDED_)
#define AFX_SETUPSTDARKFILTER_H__50A08F98_35ED_4DD7_9E75_59FEA81C7D45__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// setupStDarkFilter.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// setupStDarkFilter dialog

class setupStDarkFilter : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
	// Construction
public:
	void UpdateDisplay();
	void UpdateValues();
	setupStDarkFilter(CWnd* pParent = NULL);   // standard constructor
	CBottleApp *theapp;
	CMainFrame *pframe;

	int resSen;
	bool stepSlow;

// Dialog Data
	//{{AFX_DATA(setupStDarkFilter)
	enum { IDD = IDD_STDARKFILTER };
	CButton	m_step;
	CString	m_senFilterDark;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(setupStDarkFilter)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(setupStDarkFilter)
	virtual void OnOK();
	afx_msg void OnUseimgst1();
	afx_msg void OnUseimgst2();
	afx_msg void OnUseimgst3();
	afx_msg void OnUseimgst4();
	afx_msg void OnUseimgst5();
	afx_msg void OnUseimgst6();
	afx_msg void OnSendarkup();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSendarkdw();
	afx_msg void OnStep();
	afx_msg void OnUseimgst7();
	afx_msg void OnUseimgst8();
	afx_msg void OnUseimgst9();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETUPSTDARKFILTER_H__50A08F98_35ED_4DD7_9E75_59FEA81C7D45__INCLUDED_)
