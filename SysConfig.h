#if !defined(AFX_SYSCONFIG_H__E58AB094_78F0_4025_848B_82BE19CC3C0C__INCLUDED_)
#define AFX_SYSCONFIG_H__E58AB094_78F0_4025_848B_82BE19CC3C0C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SysConfig.h : header file
//
#include "SystemEnums.h"

/////////////////////////////////////////////////////////////////////////////
// CSysConfig dialog

class CSysConfig : public CDialog
{
	friend class CMainFrame;
	// Construction
private:
	int machineNo;
	bool welchsEnabled;
	bool trackerEnabled;
	bool waistEnabled;
	bool alignmentEnabled;
public:
	CSysConfig(CWnd* pParent = NULL);   // standard constructor
	CMainFrame* pframe;
	
	CSysConfig(MachineType machNo, bool trackEnab, bool waistEnab, bool alignEnab, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSysConfig)
	enum { IDD = IDD_SYSCONFIG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSysConfig)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
//protected:
	public:
	// Generated message map functions
	//{{AFX_MSG(CSysConfig)

	virtual BOOL OnInitDialog();
	afx_msg void OnUpdateSelections();
	afx_msg void M84RadioSelect();
	afx_msg void M85RadioSelect();
	afx_msg void OnTrackerSelect();
	afx_msg void OnAlignmentSelect();
	afx_msg afx_msg void OnWaistSelect();
	afx_msg afx_msg void OnWelchsSelect();
	void SetParams(MachineType machNo, bool trackEnab, bool waistEnab, bool alignEnab, bool welchsEnab);
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SYSCONFIG_H__E58AB094_78F0_4025_848B_82BE19CC3C0C__INCLUDED_)
