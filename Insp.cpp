// Insp.cpp : implementation file
//

#include "stdafx.h"
#include "Bottle.h"
#include "Insp.h"
#include "xaxisview.h"
#include "mainfrm.h"
#include "bottleview.h"
#include "tech.h"
#include "security.h"
#include "serial.h"
#include "View.h"
#include "Lim.h"
#include "Modify.h"
#include "Camera.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Insp dialog
int v=0;

Insp::Insp(CWnd* pParent /*=NULL*/)
	: CDialog(Insp::IDD, pParent)
{
	//{{AFX_DATA_INIT(Insp)
	m_capmiddle = _T("");
	m_tbl = _T("");
	m_tbr = _T("");
	m_fillh = _T("");
	m_fillv = _T("");
	m_edittb2s = _T("");
	m_edittb2h = _T("");
	m_edituh = _T("");
	m_editus = _T("");
	m_edituv = _T("");
	m_editsize = _T("");
	m_edittb2 = _T("");
	m_editb2v = _T("");
	m_edittbseam = _T("");
	m_tb2vert = _T("");
	m_editperf = _T("");
	m_tgXYpatt = _T("");
	m_tgXYpatt2 = _T("");
	m_sensMet3 = _T("");
	m_topLimPatt = _T("");
	m_botLimPatt = _T("");
	m_offset1 = _T("");
	m_offset2 = _T("");
	m_offset3 = _T("");
	m_offset4 = _T("");
	m_sensEdge = _T("");
	//}}AFX_DATA_INIT
}


void Insp::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Insp)
	DDX_Control(pDX, IDC_TARGLEFT, m_tgLeft);
	DDX_Control(pDX, IDC_ALIGN, m_align);
	DDX_Control(pDX, IDC_BUTTON6, m_b2);
	DDX_Control(pDX, IDC_BUTTON5, m_b1);
	DDX_Control(pDX, IDC_NUP, m_nupsp);
	DDX_Control(pDX, IDC_NDN, m_ndnsp);
	DDX_Control(pDX, IDC_MUP, m_mupsp);
	DDX_Control(pDX, IDC_MDN, m_mdnsp);
	DDX_Text(pDX, IDC_CAPMIDDLE, m_capmiddle);
	DDX_Text(pDX, IDC_TARGPATTPOS, m_tgXYpatt);
	DDX_Text(pDX, IDC_TARGPATTPOS2, m_tgXYpatt2);
	DDX_Text(pDX, IDC_SENSMET3, m_sensMet3);
	DDX_Text(pDX, IDC_TOPLIMPATT, m_topLimPatt);
	DDX_Text(pDX, IDC_BOTLIMPATT, m_botLimPatt);
	DDX_Text(pDX, IDC_OFFSET1, m_offset1);
	DDX_Text(pDX, IDC_OFFSET2, m_offset2);
	DDX_Text(pDX, IDC_OFFSET3, m_offset3);
	DDX_Text(pDX, IDC_OFFSET4, m_offset4);
DDX_Text(pDX, IDC_FINEINS, m_fine);
	DDX_Text(pDX, IDC_SENSEDGE, m_sensEdge);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Insp, CDialog)
	//{{AFX_MSG_MAP(Insp)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_MUP, OnMup)
	ON_BN_CLICKED(IDC_MDN, OnMdn)
	ON_BN_CLICKED(IDC_NUP, OnNup)
	ON_BN_CLICKED(IDC_NDN, OnNdn)
	ON_WM_PAINT()
	ON_BN_CLICKED(ID_TRIGGER, OnTrigger)
	ON_BN_CLICKED(ID_TRIGGER3, OnTrigger3)
	ON_BN_CLICKED(IDC_INSPECT, OnInspectPushed)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_BN_CLICKED(IDC_RADIO3, OnRadio3)
	ON_BN_CLICKED(IDC_RADIO4, OnRadio4)
	ON_BN_CLICKED(IDC_CAMERAS, OnCameras)
	ON_BN_CLICKED(IDC_BUTTON5, OnButton5)
	ON_BN_CLICKED(IDC_BUTTON6, OnButton6)
	ON_BN_CLICKED(IDC_CHK1, OnChk1)
	ON_BN_CLICKED(IDC_CHK2, OnChk2)
	ON_BN_CLICKED(IDC_CHK3, OnChk3)
	ON_BN_CLICKED(IDC_FINEINS, OnFineins)
	ON_BN_CLICKED(IDC_CHK4, OnChk4)
	ON_BN_CLICKED(IDC_CHK5, OnChk5)
	ON_BN_CLICKED(IDC_CHK6, OnChk6)
	ON_BN_CLICKED(IDC_CHK7, OnChk7)
	ON_BN_CLICKED(IDC_TARGLEFT, OnTargleft)
	ON_BN_CLICKED(IDC_TARGMIDDLE, OnTargmiddle)
	ON_BN_CLICKED(IDC_TARGRIGHT, OnTargright)
	ON_BN_CLICKED(IDC_TARGPATTUP, OnTargpattup)
	ON_BN_CLICKED(IDC_TARGPATTDW, OnTargpattdw)
	ON_BN_CLICKED(IDC_TARGPATTRIGHT, OnTargpattright)
	ON_BN_CLICKED(IDC_TARGPATTLEFT, OnTargpattleft)
	ON_BN_CLICKED(IDC_TEACHTARGPATT, OnTeachtargpatt)
	ON_BN_CLICKED(IDC_LABINTUSEMETHOD2, OnLabintusemethod2)
	ON_BN_CLICKED(IDC_FILTER1, OnFilter1)
	ON_BN_CLICKED(IDC_FILTER2, OnFilter2)
	ON_BN_CLICKED(IDC_FILTER3, OnFilter3)
	ON_BN_CLICKED(IDC_FILTER4, OnFilter4)
	ON_BN_CLICKED(IDC_FILTER5, OnFilter5)
	ON_BN_CLICKED(IDC_TARGPATTUP2, OnTargpattup2)
	ON_BN_CLICKED(IDC_TARGPATTDW2, OnTargpattdw2)
	ON_BN_CLICKED(IDC_TARGPATTRIGHT2, OnTargpattright2)
	ON_BN_CLICKED(IDC_TARGPATTLEFT2, OnTargpattleft2)
	ON_BN_CLICKED(IDC_FILTER6, OnFilter6)
	ON_BN_CLICKED(IDC_CHK8, OnChk8)
	ON_BN_CLICKED(IDC_CHK9, OnChk9)
	ON_BN_CLICKED(IDC_LABINTUSEMETHOD3, OnLabintusemethod3)
	ON_BN_CLICKED(IDC_SENSMET3UP, OnSensmet3up)
	ON_BN_CLICKED(IDC_SENSMET3DW, OnSensmet3dw)
	ON_BN_CLICKED(IDC_CHK10, OnChk10)
	ON_BN_CLICKED(IDC_CHK11, OnChk11)
	ON_BN_CLICKED(IDC_TOPLIMPATTUP, OnToplimpattup)
	ON_BN_CLICKED(IDC_TOPLIMPATTDW, OnToplimpattdw)
	ON_BN_CLICKED(IDC_BOTLIMPATTUP, OnBotlimpattup)
	ON_BN_CLICKED(IDC_BOTLIMPATTDW, OnBotlimpattdw)
	ON_BN_CLICKED(IDC_LABINTUSEMETHOD4, OnLabintusemethod4)
	ON_BN_CLICKED(IDC_FILTER7, OnFilter7)
	ON_BN_CLICKED(IDC_FILTER8, OnFilter8)
	ON_BN_CLICKED(IDC_LABINTUSEMETHOD1, OnLabintusemethod1)
	ON_BN_CLICKED(IDC_LIGHTTODARK, OnLighttodark)
	ON_BN_CLICKED(IDC_DARKTOLIGHT, OnDarktolight)
	ON_BN_CLICKED(IDC_SENSEDGEUP, OnSensedgeup)
	ON_BN_CLICKED(IDC_SENSEDGEDW, OnSensedgedw)
	ON_BN_CLICKED(IDC_LABINTUSEMETHOD5, OnLabintusemethod5)
	ON_BN_CLICKED(IDC_SELBODY1, OnSelbody1)
	ON_BN_CLICKED(IDC_SELBODY2, OnSelbody2)
	ON_BN_CLICKED(IDC_SELBODY3, OnSelbody3)
	ON_BN_CLICKED(IDC_SELCAP0, OnSelcap0)
	ON_BN_CLICKED(IDC_SELCAP1, OnSelcap1)
	ON_BN_CLICKED(IDC_LABINTUSEMETHOD6, OnLabintusemethod6)
	ON_BN_CLICKED(IDC_Method6_Pattern1, OnMethod6Pattern1)
	ON_BN_CLICKED(IDC_Method6_Pattern2, OnMethod6Pattern2)
	ON_BN_CLICKED(IDC_Method6_Pattern3, OnMethod6Pattern3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Insp message handlers

BOOL Insp::OnInitDialog()
{
	CDialog::OnInitDialog();

	//	The following width and height values are rotated to reflect camera position
	WIDTH = 408;
	HEIGHT = 1024;
	heightBy3 = HEIGHT / 3;
	widthBy3 = WIDTH / 3;
	heightBy6 = HEIGHT / 6;
	widthBy6 = WIDTH / 6;
	
	pframe			=	(CMainFrame*)AfxGetMainWnd();
	pframe->m_pinsp	=	this;
	theapp			=	(CBottleApp*)AfxGetApp();

	filterSelPatt	=	theapp->jobinfo[pframe->CurrentJobNum].filterSelPatt;
	EdgeTransition	= 	theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	
	pattSel			=	Pattern1;	CheckDlgButton(IDC_TARGLEFT, 1);

	if(!theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6)
		CheckDlgButton(IDC_Method6_Pattern1,1);

	resRes12		=	1;
	resRes6			=	6;
	res				=	1;
	once			=	true;

	sizepattBy12	=	theapp->sizepattBy12;
	sizepattBy6		=	theapp->sizepattBy6;
	sizepattBy3W	=	theapp->sizepattBy3_2W;
	sizepattBy3H	=	theapp->sizepattBy3_2H;

	m_fine.Format("%s","Slow"); 
	m_b1.ShowWindow(SW_HIDE);
	m_b2.ShowWindow(SW_HIDE);

	m_align.ShowWindow(SW_HIDE);
	
	/////////////////////////////////////
	//Body style
	int bottleStyle = theapp->jobinfo[pframe->CurrentJobNum].bottleStyle;
	
	setAllCheckBoxesToFalse();

	switch(bottleStyle)
	{
		case 1:	CheckDlgButton(IDC_CHK1,1);	break;
		case 2:	CheckDlgButton(IDC_CHK2,1);	break;
		case 3:	CheckDlgButton(IDC_CHK3,1);	break;
		case 4:	CheckDlgButton(IDC_CHK4,1);	break;
		case 5:	CheckDlgButton(IDC_CHK5,1);	break;
		case 6:	CheckDlgButton(IDC_CHK6,1);	break;
		case 8:	CheckDlgButton(IDC_CHK8,1);	break;
		case 9:	CheckDlgButton(IDC_CHK9,1);	break;
		case 10:CheckDlgButton(IDC_CHK10,1);break;
		case 11:CheckDlgButton(IDC_CHK11,1);break;
	}

	if(bottleStyle==10)			{CheckDlgButton(IDC_SELBODY2,1);}	//Square
	else if(bottleStyle==3)		{CheckDlgButton(IDC_SELBODY3,1);}	//Wolverine
	else						{CheckDlgButton(IDC_SELBODY1,1);}	//Default:Cylindrical

	/////////////////////////////////////
	//Cap style
	CheckDlgButton(IDC_CHK7,0);
	CheckDlgButton(IDC_CAPSEL1,0);

	if(theapp->jobinfo[pframe->CurrentJobNum].sport==0)	{CheckDlgButton(IDC_CAPSEL1,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)	{CheckDlgButton(IDC_CHK7,1);}

	CheckDlgButton(IDC_SELCAP0,0);
	CheckDlgButton(IDC_SELCAP1,0);

	if(theapp->jobinfo[pframe->CurrentJobNum].sport==0)	{CheckDlgButton(IDC_SELCAP0,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)	{CheckDlgButton(IDC_SELCAP1,1);}

	CheckDlgButton(IDC_RADIO1,1);

	cam=1;

	////////////////////

	//int nCam;
	
	//*** Images Divided By 3
	fac = 3;

	/*
	for(int i=1; i<=4; i++)
	{
		width[i]		= theapp->camHeight[i];
		height[i]		= theapp->camWidth[i];

		fac = 3;
		widthby3[i]		= theapp->camHeight[i]/fac;
		heighby3[i]		= theapp->camWidth[i]/fac;

		fac = 6;
		widthby6[i]		= theapp->camHeight[i]/fac;
		heighby6[i]		= theapp->camWidth[i]/fac;

		byteImgCoDiv3[i]= (BYTE*)malloc(4*widthby3[i]*heighby3[i]);
	}
	*/

	for(int i=1; i<=4; i++)
	{
		byteImgCoDiv3[i]= (BYTE*)malloc(4*widthBy3*heightBy3);
	}

	byteImg1BWDiv3		= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg2BWDiv3		= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg3BWDiv3		= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg4BWDiv3		= (BYTE*)malloc( widthBy3*heightBy3);
		
	byteImg1BWDiv6		= (BYTE*)malloc( widthBy6*heightBy6);
	byteImg2BWDiv6		= (BYTE*)malloc( widthBy6*heightBy6);
	byteImg3BWDiv6		= (BYTE*)malloc( widthBy6*heightBy6);
	byteImg4BWDiv6		= (BYTE*)malloc( widthBy6*heightBy6);

	byteImg1Fil2Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg2Fil2Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg3Fil2Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg4Fil2Div3	= (BYTE*)malloc( widthBy3*heightBy3);

	byteImg1Fil2Div6	= (BYTE*)malloc( widthBy6*heightBy6);
	byteImg2Fil2Div6	= (BYTE*)malloc( widthBy6*heightBy6);
	byteImg3Fil2Div6	= (BYTE*)malloc( widthBy6*heightBy6);
	byteImg4Fil2Div6	= (BYTE*)malloc( widthBy6*heightBy6);

	byteImg1Fil3Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg2Fil3Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg3Fil3Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg4Fil3Div3	= (BYTE*)malloc( widthBy3*heightBy3);

	byteImg1Fil3Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg2Fil3Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg3Fil3Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg4Fil3Div6	= (BYTE*)malloc( widthBy3*heightBy3);

	byteImg1Fil4Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg2Fil4Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg3Fil4Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg4Fil4Div3	= (BYTE*)malloc( widthBy3*heightBy3);

	byteImg1Fil4Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg2Fil4Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg3Fil4Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg4Fil4Div6	= (BYTE*)malloc( widthBy3*heightBy3);

	byteImg1Fil5Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg2Fil5Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg3Fil5Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg4Fil5Div3	= (BYTE*)malloc( widthBy3*heightBy3);

	byteImg1Fil5Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg2Fil5Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg3Fil5Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg4Fil5Div6	= (BYTE*)malloc( widthBy3*heightBy3);

	byteImg1Fil6Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg2Fil6Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg3Fil6Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg4Fil6Div3	= (BYTE*)malloc( widthBy3*heightBy3);

	byteImg1Fil6Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg2Fil6Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg3Fil6Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg4Fil6Div6	= (BYTE*)malloc( widthBy3*heightBy3);

	byteImg1Fil7Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg2Fil7Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg3Fil7Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg4Fil7Div3	= (BYTE*)malloc( widthBy3*heightBy3);

	byteImg1Fil7Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg2Fil7Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg3Fil7Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg4Fil7Div6	= (BYTE*)malloc( widthBy3*heightBy3);

	byteImg1Fil8Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg2Fil8Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg3Fil8Div3	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg4Fil8Div3	= (BYTE*)malloc( widthBy3*heightBy3);

	byteImg1Fil8Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg2Fil8Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg3Fil8Div6	= (BYTE*)malloc( widthBy3*heightBy3);
	byteImg4Fil8Div6	= (BYTE*)malloc( widthBy3*heightBy3);


	LoadCameraImages();

	/*
	nCam = 1;//Color
	CVisRGBAByteImage	imgco1div3(widthBy3, heightBy3, 1, -1, byteImgCoDiv3[nCam]);
	imgco1div3.SetRect(0, 0, widthBy3, heightBy3);	imageCoDiv3[nCam] = imgco1div3; 
	CVisRGBAByteImage	imgcoC1div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_reduce1);
	imgcoC1div3.SetRect(0, 0, widthBy3, heightBy3);	imgcoC1div3.CopyPixelsTo(imageCoDiv3[nCam]);
	byteImgCoDiv3[nCam] = (unsigned char*)imageCoDiv3[nCam].PixelAddress(0,0,0);

	nCam = 2;//Color	Div3
	CVisRGBAByteImage	imgco2div3(widthBy3, heightBy3, 1, -1, byteImgCoDiv3[nCam]);
	imgco2div3.SetRect(0, 0, widthBy3, heightBy3);	imageCoDiv3[nCam] = imgco2div3; 
	CVisRGBAByteImage	imgcoC2div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_reduce2);
	imgcoC2div3.SetRect(0, 0, widthBy3, heightBy3);	imgcoC2div3.CopyPixelsTo(imageCoDiv3[nCam]);
	byteImgCoDiv3[nCam] = (unsigned char*)imageCoDiv3[nCam].PixelAddress(0,0,0);

	nCam = 3;//Color	Div3
	CVisRGBAByteImage	imgco3div3(widthBy3, heightBy3, 1, -1, byteImgCoDiv3[nCam]);
	imgco3div3.SetRect(0, 0, widthBy3, heightBy3);	imageCoDiv3[nCam] = imgco3div3; 
	CVisRGBAByteImage	imgcoC3div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_reduce3);
	imgcoC3div3.SetRect(0, 0, widthBy3, heightBy3);	imgcoC3div3.CopyPixelsTo(imageCoDiv3[nCam]);
	byteImgCoDiv3[nCam] = (unsigned char*)imageCoDiv3[nCam].PixelAddress(0,0,0);

	nCam = 4;//Color	Div3
	CVisRGBAByteImage	imgco4div3(widthBy3, heightBy3, 1, -1, byteImgCoDiv3[nCam]);
	imgco4div3.SetRect(0, 0, widthBy3, heightBy3);	imageCoDiv3[nCam] = imgco4div3; 
	CVisRGBAByteImage	imgcoC4div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_reduce4);
	imgcoC4div3.SetRect(0, 0, widthBy3, heightBy3);	imgcoC4div3.CopyPixelsTo(imageCoDiv3[nCam]);
	byteImgCoDiv3[nCam] = (unsigned char*)imageCoDiv3[nCam].PixelAddress(0,0,0);

	//////////////////////////////

	//BW	Div3 Filter 2
	//i = 1;

//	byteImg1BWDiv3 = static_byteImg1BWDiv3;

	//BW	Div3 Filter 1
	CVisByteImage		img1bw1div3(widthBy3, heightBy3, 1, -1, byteImg1BWDiv3);
	img1bw1div3.SetRect(0, 0, widthBy3, heightBy3);	image1BWDiv3 = img1bw1div3; 
	CVisByteImage		img1bwC1div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cambw1);
	img1bwC1div3.SetRect(0, 0, widthBy3, heightBy3);	img1bwC1div3.CopyPixelsTo(image1BWDiv3);
	byteImg1BWDiv3 = (unsigned char*)image1BWDiv3.PixelAddress(0, 0, 0);
		
	//BW	Div6
	CVisByteImage		img1bw1div6(widthBy6, heightBy6, 1, -1, byteImg1BWDiv6);
	img1bw1div6.SetRect(0, 0, widthBy6, heightBy6);	image1BWDiv6 = img1bw1div6; 
	CVisByteImage		img1bwC1div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam1bwDiv6);
	img1bwC1div6.SetRect(0, 0, widthBy6, heightBy6);	img1bwC1div6.CopyPixelsTo(image1BWDiv6);
	byteImg1BWDiv6 = (unsigned char*)image1BWDiv6.PixelAddress(0, 0, 0);

	CVisByteImage		img1fil2div3(widthBy3, heightBy3, 1, -1, byteImg1Fil2Div3);
	img1fil2div3.SetRect(0, 0, widthBy3, heightBy3);	image1Fil2Div3 = img1fil2div3; 
	CVisByteImage		img1filter2div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img1SatDiv3);
	img1filter2div3.SetRect(0, 0, widthBy3, heightBy3);	img1filter2div3.CopyPixelsTo(image1Fil2Div3);
	byteImg1Fil2Div3 = (unsigned char*)image1Fil2Div3.PixelAddress(0,0,0);

	CVisByteImage		img1fil2div6(widthBy6, heightBy6, 1, -1, byteImg1Fil2Div6);
	img1fil2div6.SetRect(0, 0, widthBy6, heightBy6);	image1Fil2Div6 = img1fil2div6; 
	CVisByteImage		img1filter2div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img1SatDiv6);
	img1filter2div6.SetRect(0, 0, widthBy6, heightBy6);	img1filter2div6.CopyPixelsTo(image1Fil2Div6);
	byteImg1Fil2Div6 = (unsigned char*)image1Fil2Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 3
	CVisByteImage		img1fil3div3(widthBy3, heightBy3, 1, -1, byteImg1Fil3Div3);
	img1fil3div3.SetRect(0, 0, widthBy3, heightBy3);	image1Fil3Div3 = img1fil3div3; 
	CVisByteImage		img1filter3div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img1MagDiv3);
	img1filter3div3.SetRect(0, 0, widthBy3, heightBy3);	img1filter3div3.CopyPixelsTo(image1Fil3Div3);
	byteImg1Fil3Div3 = (unsigned char*)image1Fil3Div3.PixelAddress(0,0,0);

	CVisByteImage		img1fil3div6(widthBy6, heightBy6, 1, -1, byteImg1Fil3Div6);
	img1fil3div6.SetRect(0, 0, widthBy6, heightBy6);	image1Fil3Div6 = img1fil3div6; 
	CVisByteImage		img1filter3div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img1MagDiv6);
	img1filter3div6.SetRect(0, 0, widthBy6, heightBy6);	img1filter3div6.CopyPixelsTo(image1Fil3Div6);
	byteImg1Fil3Div6 = (unsigned char*)image1Fil3Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 4
	CVisByteImage		img1fil4div3(widthBy3, heightBy3, 1, -1, byteImg1Fil4Div3);
	img1fil4div3.SetRect(0, 0, widthBy3, heightBy3);	image1Fil4Div3 = img1fil4div3; 
	CVisByteImage		img1filter4div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img1YelDiv3);
	img1filter4div3.SetRect(0, 0, widthBy3, heightBy3);	img1filter4div3.CopyPixelsTo(image1Fil4Div3);
	byteImg1Fil4Div3 = (unsigned char*)image1Fil4Div3.PixelAddress(0,0,0);

	CVisByteImage		img1fil4div6(widthBy6, heightBy6, 1, -1, byteImg1Fil4Div6);
	img1fil4div6.SetRect(0, 0, widthBy6, heightBy6);	image1Fil4Div6 = img1fil4div6; 
	CVisByteImage		img1filter4div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img1YelDiv6);
	img1filter4div6.SetRect(0, 0, widthBy6, heightBy6);	img1filter4div6.CopyPixelsTo(image1Fil4Div6);
	byteImg1Fil4Div6 = (unsigned char*)image1Fil4Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 5
	CVisByteImage		img1fil5div3(widthBy3, heightBy3, 1, -1, byteImg1Fil5Div3);
	img1fil5div3.SetRect(0, 0, widthBy3, heightBy3);	image1Fil5Div3 = img1fil5div3; 
	CVisByteImage		img1filter5div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img1CyaDiv3);
	img1filter5div3.SetRect(0, 0, widthBy3, heightBy3);	img1filter5div3.CopyPixelsTo(image1Fil5Div3);
	byteImg1Fil5Div3 = (unsigned char*)image1Fil5Div3.PixelAddress(0,0,0);

	CVisByteImage		img1fil5div6(widthBy6, heightBy6, 1, -1, byteImg1Fil5Div6);
	img1fil5div6.SetRect(0, 0, widthBy6, heightBy6);	image1Fil5Div6 = img1fil5div6; 
	CVisByteImage		img1filter5div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img1CyaDiv6);
	img1filter5div6.SetRect(0, 0, widthBy6, heightBy6);	img1filter5div6.CopyPixelsTo(image1Fil5Div6);
	byteImg1Fil5Div6 = (unsigned char*)image1Fil5Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 6
	CVisByteImage		img1fil6div3(widthBy3, heightBy3, 1, -1, byteImg1Fil6Div3);
	img1fil6div3.SetRect(0, 0, widthBy3, heightBy3);	image1Fil6Div3 = img1fil6div3; 
	CVisByteImage		img1filter6div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam1Rbw);
	img1filter6div3.SetRect(0, 0, widthBy3, heightBy3);	img1filter6div3.CopyPixelsTo(image1Fil6Div3);
	byteImg1Fil6Div3 = (unsigned char*)image1Fil6Div3.PixelAddress(0,0,0);

	CVisByteImage		img1fil6div6(widthBy6, heightBy6, 1, -1, byteImg1Fil6Div6);
	img1fil6div6.SetRect(0, 0, widthBy6, heightBy6);	image1Fil6Div6 = img1fil6div6; 
	CVisByteImage		img1filter6div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam1RbwDiv6);
	img1filter6div6.SetRect(0, 0, widthBy6, heightBy6);	img1filter6div6.CopyPixelsTo(image1Fil6Div6);
	byteImg1Fil6Div6 = (unsigned char*)image1Fil6Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 7
	CVisByteImage		img1fil7div3(widthBy3, heightBy3, 1, -1, byteImg1Fil7Div3);
	img1fil7div3.SetRect(0, 0, widthBy3, heightBy3);	image1Fil7Div3 = img1fil7div3; 
	CVisByteImage		img1filter7div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam1Gbw);
	img1filter7div3.SetRect(0, 0, widthBy3, heightBy3);	img1filter7div3.CopyPixelsTo(image1Fil7Div3);
	byteImg1Fil7Div3 = (unsigned char*)image1Fil7Div3.PixelAddress(0,0,0);

	CVisByteImage		img1fil7div6(widthBy6, heightBy6, 1, -1, byteImg1Fil7Div6);
	img1fil7div6.SetRect(0, 0, widthBy6, heightBy6);	image1Fil7Div6 = img1fil7div6; 
	CVisByteImage		img1filter7div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam1GbwDiv6);
	img1filter7div6.SetRect(0, 0, widthBy6, heightBy6);	img1filter7div6.CopyPixelsTo(image1Fil7Div6);
	byteImg1Fil7Div6 = (unsigned char*)image1Fil7Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 8
	CVisByteImage		img1fil8div3(widthBy3, heightBy3, 1, -1, byteImg1Fil8Div3);
	img1fil8div3.SetRect(0, 0, widthBy3, heightBy3);	image1Fil8Div3 = img1fil8div3; 
	CVisByteImage		img1filter8div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam1Bbw);
	img1filter8div3.SetRect(0, 0, widthBy3, heightBy3);	img1filter8div3.CopyPixelsTo(image1Fil8Div3);
	byteImg1Fil8Div3 = (unsigned char*)image1Fil8Div3.PixelAddress(0,0,0);

	CVisByteImage		img1fil8div6(widthBy6, heightBy6, 1, -1, byteImg1Fil8Div6);
	img1fil8div6.SetRect(0, 0, widthBy6, heightBy6);	image1Fil8Div6 = img1fil8div6; 
	CVisByteImage		img1filter8div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam1BbwDiv6);
	img1filter8div6.SetRect(0, 0, widthBy6, heightBy6);	img1filter8div6.CopyPixelsTo(image1Fil8Div6);
	byteImg1Fil8Div6 = (unsigned char*)image1Fil8Div6.PixelAddress(0,0,0);

	//i = 2;

	//BW	Div3 Filter 1
	CVisByteImage		img2bw1div3(widthBy3, heightBy3, 1, -1, byteImg2BWDiv3);
	img2bw1div3.SetRect(0, 0, widthBy3, heightBy3);	image2BWDiv3 = img2bw1div3; 
	CVisByteImage		img2bwC1div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cambw2);
	img2bwC1div3.SetRect(0, 0, widthBy3, heightBy3);	img2bwC1div3.CopyPixelsTo(image2BWDiv3);
	byteImg2BWDiv3 = (unsigned char*)image2BWDiv3.PixelAddress(0,0,0);
		
	//BW	Div6
	CVisByteImage		img2bw1div6(widthBy6, heightBy6, 1, -1, byteImg2BWDiv6);
	img2bw1div6.SetRect(0, 0, widthBy6, heightBy6);	image2BWDiv6 = img2bw1div6; 
	CVisByteImage		img2bwC1div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam2bwDiv6);
	img2bwC1div6.SetRect(0, 0, widthBy6, heightBy6);	img2bwC1div6.CopyPixelsTo(image2BWDiv6);
	byteImg2BWDiv6 = (unsigned char*)image2BWDiv6.PixelAddress(0,0,0);

	CVisByteImage		img2fil2div3(widthBy3, heightBy3, 1, -1, byteImg2Fil2Div3);
	img2fil2div3.SetRect(0, 0, widthBy3, heightBy3);	image2Fil2Div3 = img2fil2div3; 
	CVisByteImage		img2filter2div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img2SatDiv3);
	img2filter2div3.SetRect(0, 0, widthBy3, heightBy3);	img2filter2div3.CopyPixelsTo(image2Fil2Div3);
	byteImg2Fil2Div3 = (unsigned char*)image2Fil2Div3.PixelAddress(0,0,0);

	CVisByteImage		img2fil2div6(widthBy6, heightBy6, 1, -1, byteImg2Fil2Div6);
	img2fil2div6.SetRect(0, 0, widthBy6, heightBy6);	image2Fil2Div6 = img2fil2div6; 
	CVisByteImage		img2filter2div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img2SatDiv6);
	img2filter2div6.SetRect(0, 0, widthBy6, heightBy6);	img2filter2div6.CopyPixelsTo(image2Fil2Div6);
	byteImg2Fil2Div6 = (unsigned char*)image2Fil2Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 3
	CVisByteImage		img2fil3div3(widthBy3, heightBy3, 1, -1, byteImg2Fil3Div3);
	img2fil3div3.SetRect(0, 0, widthBy3, heightBy3);	image2Fil3Div3 = img2fil3div3; 
	CVisByteImage		img2filter3div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img2MagDiv3);
	img2filter3div3.SetRect(0, 0, widthBy3, heightBy3);	img2filter3div3.CopyPixelsTo(image2Fil3Div3);
	byteImg2Fil3Div3 = (unsigned char*)image2Fil3Div3.PixelAddress(0,0,0);

	CVisByteImage		img2fil3div6(widthBy6, heightBy6, 1, -1, byteImg2Fil3Div6);
	img2fil3div6.SetRect(0, 0, widthBy6, heightBy6);	image2Fil3Div6 = img2fil3div6; 
	CVisByteImage		img2filter3div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img2MagDiv6);
	img2filter3div6.SetRect(0, 0, widthBy6, heightBy6);	img2filter3div6.CopyPixelsTo(image2Fil3Div6);
	byteImg2Fil3Div6 = (unsigned char*)image2Fil3Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 4
	CVisByteImage		img2fil4div3(widthBy3, heightBy3, 1, -1, byteImg2Fil4Div3);
	img2fil4div3.SetRect(0, 0, widthBy3, heightBy3);	image2Fil4Div3 = img2fil4div3; 
	CVisByteImage		img2filter4div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img2YelDiv3);
	img2filter4div3.SetRect(0, 0, widthBy3, heightBy3);	img2filter4div3.CopyPixelsTo(image2Fil4Div3);
	byteImg2Fil4Div3 = (unsigned char*)image2Fil4Div3.PixelAddress(0,0,0);

	CVisByteImage		img2fil4div6(widthBy6, heightBy6, 1, -1, byteImg2Fil4Div6);
	img2fil4div6.SetRect(0, 0, widthBy6, heightBy6);	image2Fil4Div6 = img2fil4div6; 
	CVisByteImage		img2filter4div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img2YelDiv6);
	img2filter4div6.SetRect(0, 0, widthBy6, heightBy6);	img2filter4div6.CopyPixelsTo(image2Fil4Div6);
	byteImg2Fil4Div6 = (unsigned char*)image2Fil4Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 5
	CVisByteImage		img2fil5div3(widthBy3, heightBy3, 1, -1, byteImg2Fil5Div3);
	img2fil5div3.SetRect(0, 0, widthBy3, heightBy3);	image2Fil5Div3 = img2fil5div3; 
	CVisByteImage		img2filter5div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img2CyaDiv3);
	img2filter5div3.SetRect(0, 0, widthBy3, heightBy3);	img2filter5div3.CopyPixelsTo(image2Fil5Div3);
	byteImg2Fil5Div3 = (unsigned char*)image2Fil5Div3.PixelAddress(0,0,0);

	CVisByteImage		img2fil5div6(widthBy6, heightBy6, 1, -1, byteImg2Fil5Div6);
	img2fil5div6.SetRect(0, 0, widthBy6, heightBy6);	image2Fil5Div6 = img2fil5div6; 
	CVisByteImage		img2filter5div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img2CyaDiv6);
	img2filter5div6.SetRect(0, 0, widthBy6, heightBy6);	img2filter5div6.CopyPixelsTo(image2Fil5Div6);
	byteImg2Fil5Div6 = (unsigned char*)image2Fil5Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 6
	CVisByteImage		img2fil6div3(widthBy3, heightBy3, 1, -1, byteImg2Fil6Div3);
	img2fil6div3.SetRect(0, 0, widthBy3, heightBy3);	image2Fil6Div3 = img2fil6div3; 
	CVisByteImage		img2filter6div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam2Rbw);
	img2filter6div3.SetRect(0, 0, widthBy3, heightBy3);	img2filter6div3.CopyPixelsTo(image2Fil6Div3);
	byteImg2Fil6Div3 = (unsigned char*)image2Fil6Div3.PixelAddress(0,0,0);

	CVisByteImage		img2fil6div6(widthBy6, heightBy6, 1, -1, byteImg2Fil6Div6);
	img2fil6div6.SetRect(0, 0, widthBy6, heightBy6);	image2Fil6Div6 = img2fil6div6; 
	CVisByteImage		img2filter6div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam2RbwDiv6);
	img2filter6div6.SetRect(0, 0, widthBy6, heightBy6);	img2filter6div6.CopyPixelsTo(image2Fil6Div6);
	byteImg2Fil6Div6 = (unsigned char*)image2Fil6Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 7
	CVisByteImage		img2fil7div3(widthBy3, heightBy3, 1, -1, byteImg2Fil7Div3);
	img2fil7div3.SetRect(0, 0, widthBy3, heightBy3);	image2Fil7Div3 = img2fil7div3; 
	CVisByteImage		img2filter7div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam2Gbw);
	img2filter7div3.SetRect(0, 0, widthBy3, heightBy3);	img2filter7div3.CopyPixelsTo(image2Fil7Div3);
	byteImg2Fil7Div3 = (unsigned char*)image2Fil7Div3.PixelAddress(0,0,0);

	CVisByteImage		img2fil7div6(widthBy6, heightBy6, 1, -1, byteImg2Fil7Div6);
	img2fil7div6.SetRect(0, 0, widthBy6, heightBy6);	image2Fil7Div6 = img2fil7div6; 
	CVisByteImage		img2filter7div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam2GbwDiv6);
	img2filter7div6.SetRect(0, 0, widthBy6, heightBy6);	img2filter7div6.CopyPixelsTo(image2Fil7Div6);
	byteImg2Fil7Div6 = (unsigned char*)image2Fil7Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 8
	CVisByteImage		img2fil8div3(widthBy3, heightBy3, 1, -1, byteImg2Fil8Div3);
	img2fil8div3.SetRect(0, 0, widthBy3, heightBy3);	image2Fil8Div3 = img2fil8div3; 
	CVisByteImage		img2filter8div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam2Bbw);
	img2filter8div3.SetRect(0, 0, widthBy3, heightBy3);	img2filter8div3.CopyPixelsTo(image2Fil8Div3);
	byteImg2Fil8Div3 = (unsigned char*)image2Fil8Div3.PixelAddress(0,0,0);

	CVisByteImage		img2fil8div6(widthBy6, heightBy6, 1, -1, byteImg2Fil8Div6);
	img2fil8div6.SetRect(0, 0, widthBy6, heightBy6);	image2Fil8Div6 = img2fil8div6; 
	CVisByteImage		img2filter8div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam2BbwDiv6);
	img2filter8div6.SetRect(0, 0, widthBy6, heightBy6);	img2filter8div6.CopyPixelsTo(image2Fil8Div6);
	byteImg2Fil8Div6 = (unsigned char*)image2Fil8Div6.PixelAddress(0,0,0);

	//i = 3;

	//BW	Div3 Filter 1
	CVisByteImage		img3bw1div3(widthBy3, heightBy3, 1, -1, byteImg3BWDiv3);
	img3bw1div3.SetRect(0, 0, widthBy3, heightBy3);	image3BWDiv3 = img3bw1div3; 
	CVisByteImage		img3bwC1div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cambw3);
	img3bwC1div3.SetRect(0, 0, widthBy3, heightBy3);	img3bwC1div3.CopyPixelsTo(image3BWDiv3);
	byteImg3BWDiv3 = (unsigned char*)image3BWDiv3.PixelAddress(0,0,0);
		
	//BW	Div6
	CVisByteImage		img3bw1div6(widthBy6, heightBy6, 1, -1, byteImg3BWDiv6);
	img3bw1div6.SetRect(0, 0, widthBy6, heightBy6);	image3BWDiv6 = img3bw1div6; 
	CVisByteImage		img3bwC1div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam3bwDiv6);
	img3bwC1div6.SetRect(0, 0, widthBy6, heightBy6);	img3bwC1div6.CopyPixelsTo(image3BWDiv6);
	byteImg3BWDiv6 = (unsigned char*)image3BWDiv6.PixelAddress(0,0,0);
	
	CVisByteImage		img3fil2div3(widthBy3, heightBy3, 1, -1, byteImg3Fil2Div3);
	img3fil2div3.SetRect(0, 0, widthBy3, heightBy3);	image3Fil2Div3 = img3fil2div3; 
	CVisByteImage		img3filter2div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img3SatDiv3);
	img3filter2div3.SetRect(0, 0, widthBy3, heightBy3);	img3filter2div3.CopyPixelsTo(image3Fil2Div3);
	byteImg3Fil2Div3 = (unsigned char*)image3Fil2Div3.PixelAddress(0,0,0);

	CVisByteImage		img3fil2div6(widthBy6, heightBy6, 1, -1, byteImg3Fil2Div6);
	img3fil2div6.SetRect(0, 0, widthBy6, heightBy6);	image3Fil2Div6 = img3fil2div6; 
	CVisByteImage		img3filter2div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img3SatDiv6);
	img3filter2div6.SetRect(0, 0, widthBy6, heightBy6);	img3filter2div6.CopyPixelsTo(image3Fil2Div6);
	byteImg3Fil2Div6 = (unsigned char*)image3Fil2Div6.PixelAddress(0,0,0);

	CVisByteImage		img3fil3div3(widthBy3, heightBy3, 1, -1, byteImg3Fil3Div3);
	img3fil3div3.SetRect(0, 0, widthBy3, heightBy3);	image3Fil3Div3 = img3fil3div3; 
	CVisByteImage		img3filter3div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img3MagDiv3);
	img3filter3div3.SetRect(0, 0, widthBy3, heightBy3);	img3filter3div3.CopyPixelsTo(image3Fil3Div3);
	byteImg3Fil3Div3 = (unsigned char*)image3Fil3Div3.PixelAddress(0,0,0);

	CVisByteImage		img3fil3div6(widthBy6, heightBy6, 1, -1, byteImg3Fil3Div6);
	img3fil3div6.SetRect(0, 0, widthBy6, heightBy6);	image3Fil3Div6 = img3fil3div6; 
	CVisByteImage		img3filter3div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img3MagDiv6);
	img3filter3div6.SetRect(0, 0, widthBy6, heightBy6);	img3filter3div6.CopyPixelsTo(image3Fil3Div6);
	byteImg3Fil3Div6 = (unsigned char*)image3Fil3Div6.PixelAddress(0,0,0);
	
	//BW	Div3 Filter 4
	CVisByteImage		img3fil4div3(widthBy3, heightBy3, 1, -1, byteImg3Fil4Div3);
	img3fil4div3.SetRect(0, 0, widthBy3, heightBy3);	image3Fil4Div3 = img3fil4div3; 
	CVisByteImage		img3filter4div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img3YelDiv3);
	img3filter4div3.SetRect(0, 0, widthBy3, heightBy3);	img3filter4div3.CopyPixelsTo(image3Fil4Div3);
	byteImg3Fil4Div3 = (unsigned char*)image3Fil4Div3.PixelAddress(0,0,0);

	CVisByteImage		img3fil4div6(widthBy6, heightBy6, 1, -1, byteImg3Fil4Div6);
	img3fil4div6.SetRect(0, 0, widthBy6, heightBy6);	image3Fil4Div6 = img3fil4div6; 
	CVisByteImage		img3filter4div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img3YelDiv6);
	img3filter4div6.SetRect(0, 0, widthBy6, heightBy6);	img3filter4div6.CopyPixelsTo(image3Fil4Div6);
	byteImg3Fil4Div6 = (unsigned char*)image3Fil4Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 5
	CVisByteImage		img3fil5div3(widthBy3, heightBy3, 1, -1, byteImg3Fil5Div3);
	img3fil5div3.SetRect(0, 0, widthBy3, heightBy3);	image3Fil5Div3 = img3fil5div3; 
	CVisByteImage		img3filter5div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img3CyaDiv3);
	img3filter5div3.SetRect(0, 0, widthBy3, heightBy3);	img3filter5div3.CopyPixelsTo(image3Fil5Div3);
	byteImg3Fil5Div3 = (unsigned char*)image3Fil5Div3.PixelAddress(0,0,0);

	CVisByteImage		img3fil5div6(widthBy6, heightBy6, 1, -1, byteImg3Fil5Div6);
	img3fil5div6.SetRect(0, 0, widthBy6, heightBy6);	image3Fil5Div6 = img3fil5div6; 
	CVisByteImage		img3filter5div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img3CyaDiv6);
	img3filter5div6.SetRect(0, 0, widthBy6, heightBy6);	img3filter5div6.CopyPixelsTo(image3Fil5Div6);
	byteImg3Fil5Div6 = (unsigned char*)image3Fil5Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 6
	CVisByteImage		img3fil6div3(widthBy3, heightBy3, 1, -1, byteImg3Fil6Div3);
	img3fil6div3.SetRect(0, 0, widthBy3, heightBy3);	image3Fil6Div3 = img3fil6div3; 
	CVisByteImage		img3filter6div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam3Rbw);
	img3filter6div3.SetRect(0, 0, widthBy3, heightBy3);	img3filter6div3.CopyPixelsTo(image3Fil6Div3);
	byteImg3Fil6Div3 = (unsigned char*)image3Fil6Div3.PixelAddress(0,0,0);

	CVisByteImage		img3fil6div6(widthBy6, heightBy6, 1, -1, byteImg3Fil6Div6);
	img3fil6div6.SetRect(0, 0, widthBy6, heightBy6);	image3Fil6Div6 = img3fil6div6; 
	CVisByteImage		img3filter6div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam3RbwDiv6);
	img3filter6div6.SetRect(0, 0, widthBy6, heightBy6);	img3filter6div6.CopyPixelsTo(image3Fil6Div6);
	byteImg3Fil6Div6 = (unsigned char*)image3Fil6Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 7
	CVisByteImage		img3fil7div3(widthBy3, heightBy3, 1, -1, byteImg3Fil7Div3);
	img3fil7div3.SetRect(0, 0, widthBy3, heightBy3);	image3Fil7Div3 = img3fil7div3; 
	CVisByteImage		img3filter7div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam3Gbw);
	img3filter7div3.SetRect(0, 0, widthBy3, heightBy3);	img3filter7div3.CopyPixelsTo(image3Fil7Div3);
	byteImg3Fil7Div3 = (unsigned char*)image3Fil7Div3.PixelAddress(0,0,0);

	CVisByteImage		img3fil7div6(widthBy6, heightBy6, 1, -1, byteImg3Fil7Div6);
	img3fil7div6.SetRect(0, 0, widthBy6, heightBy6);	image3Fil7Div6 = img3fil7div6; 
	CVisByteImage		img3filter7div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam3GbwDiv6);
	img3filter7div6.SetRect(0, 0, widthBy6, heightBy6);	img3filter7div6.CopyPixelsTo(image3Fil7Div6);
	byteImg3Fil7Div6 = (unsigned char*)image3Fil7Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 8
	CVisByteImage		img3fil8div3(widthBy3, heightBy3, 1, -1, byteImg3Fil8Div3);
	img3fil8div3.SetRect(0, 0, widthBy3, heightBy3);	image3Fil8Div3 = img3fil8div3; 
	CVisByteImage		img3filter8div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam3Bbw);
	img3filter8div3.SetRect(0, 0, widthBy3, heightBy3);	img3filter8div3.CopyPixelsTo(image3Fil8Div3);
	byteImg3Fil8Div3 = (unsigned char*)image3Fil8Div3.PixelAddress(0,0,0);

	CVisByteImage		img3fil8div6(widthBy6, heightBy6, 1, -1, byteImg3Fil8Div6);
	img3fil8div6.SetRect(0, 0, widthBy6, heightBy6);	image3Fil8Div6 = img3fil8div6; 
	CVisByteImage		img3filter8div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam3BbwDiv6);
	img3filter8div6.SetRect(0, 0, widthBy6, heightBy6);	img3filter8div6.CopyPixelsTo(image3Fil8Div6);
	byteImg3Fil8Div6 = (unsigned char*)image3Fil8Div6.PixelAddress(0,0,0);

	//i = 4;

	//BW	Div3 Filter 1
	CVisByteImage		img4bw1div3(widthBy3, heightBy3, 1, -1, byteImg4BWDiv3);
	img4bw1div3.SetRect(0, 0, widthBy3, heightBy3);	image4BWDiv3 = img4bw1div3; 
	CVisByteImage		img4bwC1div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cambw4);
	img4bwC1div3.SetRect(0, 0, widthBy3, heightBy3);	img4bwC1div3.CopyPixelsTo(image4BWDiv3);
	byteImg4BWDiv3 = (unsigned char*)image4BWDiv3.PixelAddress(0,0,0);
		
	//BW	Div6
	CVisByteImage		img4bw1div6(widthBy6, heightBy6, 1, -1, byteImg4BWDiv6);
	img4bw1div6.SetRect(0, 0, widthBy6, heightBy6);	image4BWDiv6 = img4bw1div6; 
	CVisByteImage		img4bwC1div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam4bwDiv6);
	img4bwC1div6.SetRect(0, 0, widthBy6, heightBy6);	img4bwC1div6.CopyPixelsTo(image4BWDiv6);
	byteImg4BWDiv6 = (unsigned char*)image4BWDiv6.PixelAddress(0,0,0);

	CVisByteImage		img4fil2div3(widthBy3, heightBy3, 1, -1, byteImg4Fil2Div3);
	img4fil2div3.SetRect(0, 0, widthBy3, heightBy3);	image4Fil2Div3 = img4fil2div3; 
	CVisByteImage		img4filter2div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img4SatDiv3);
	img4filter2div3.SetRect(0, 0, widthBy3, heightBy3);	img4filter2div3.CopyPixelsTo(image4Fil2Div3);
	byteImg4Fil2Div3 = (unsigned char*)image4Fil2Div3.PixelAddress(0,0,0);

	CVisByteImage		img4fil2div6(widthBy6, heightBy6, 1, -1, byteImg4Fil2Div6);
	img4fil2div6.SetRect(0, 0, widthBy6, heightBy6);	image4Fil2Div6 = img4fil2div6; 
	CVisByteImage		img4filter2div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img4SatDiv6);
	img4filter2div6.SetRect(0, 0, widthBy6, heightBy6);	img4filter2div6.CopyPixelsTo(image4Fil2Div6);
	byteImg4Fil2Div6 = (unsigned char*)image4Fil2Div6.PixelAddress(0,0,0);

	CVisByteImage		img4fil3div3(widthBy3, heightBy3, 1, -1, byteImg4Fil3Div3);
	img4fil3div3.SetRect(0, 0, widthBy3, heightBy3);	image4Fil3Div3 = img4fil3div3; 
	CVisByteImage		img4filter3div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img4MagDiv3);
	img4filter3div3.SetRect(0, 0, widthBy3, heightBy3);	img4filter3div3.CopyPixelsTo(image4Fil3Div3);
	byteImg4Fil3Div3 = (unsigned char*)image4Fil3Div3.PixelAddress(0,0,0);

	CVisByteImage		img4fil3div6(widthBy6, heightBy6, 1, -1, byteImg4Fil3Div6);
	img4fil3div6.SetRect(0, 0, widthBy6, heightBy6);	image4Fil3Div6 = img4fil3div6; 
	CVisByteImage		img4filter3div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img4MagDiv6);
	img4filter3div6.SetRect(0, 0, widthBy6, heightBy6);	img4filter3div6.CopyPixelsTo(image4Fil3Div6);
	byteImg4Fil3Div6 = (unsigned char*)image4Fil3Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 4
	CVisByteImage		img4fil4div3(widthBy3, heightBy3, 1, -1, byteImg4Fil4Div3);
	img4fil4div3.SetRect(0, 0, widthBy3, heightBy3);	image4Fil4Div3 = img4fil4div3; 
	CVisByteImage		img4filter4div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img4YelDiv3);
	img4filter4div3.SetRect(0, 0, widthBy3, heightBy3);	img4filter4div3.CopyPixelsTo(image4Fil4Div3);
	byteImg4Fil4Div3 = (unsigned char*)image4Fil4Div3.PixelAddress(0,0,0);

	CVisByteImage		img4fil4div6(widthBy6, heightBy6, 1, -1, byteImg4Fil4Div6);
	img4fil4div6.SetRect(0, 0, widthBy6, heightBy6);	image4Fil4Div6 = img4fil4div6; 
	CVisByteImage		img4filter4div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img4YelDiv6);
	img4filter4div6.SetRect(0, 0, widthBy6, heightBy6);	img4filter4div6.CopyPixelsTo(image4Fil4Div6);
	byteImg4Fil4Div6 = (unsigned char*)image4Fil4Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 5
	CVisByteImage		img4fil5div3(widthBy3, heightBy3, 1, -1, byteImg4Fil5Div3);
	img4fil5div3.SetRect(0, 0, widthBy3, heightBy3);	image4Fil5Div3 = img4fil5div3; 
	CVisByteImage		img4filter5div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img4CyaDiv3);
	img4filter5div3.SetRect(0, 0, widthBy3, heightBy3);	img4filter5div3.CopyPixelsTo(image4Fil5Div3);
	byteImg4Fil5Div3 = (unsigned char*)image4Fil5Div3.PixelAddress(0,0,0);

	CVisByteImage		img4fil5div6(widthBy6, heightBy6, 1, -1, byteImg4Fil5Div6);
	img4fil5div6.SetRect(0, 0, widthBy6, heightBy6);	image4Fil5Div6 = img4fil5div6; 
	CVisByteImage		img4filter5div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img4CyaDiv6);
	img4filter5div6.SetRect(0, 0, widthBy6, heightBy6);	img4filter5div6.CopyPixelsTo(image4Fil5Div6);
	byteImg4Fil5Div6 = (unsigned char*)image4Fil5Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 6
	CVisByteImage		img4fil6div3(widthBy3, heightBy3, 1, -1, byteImg4Fil6Div3);
	img4fil6div3.SetRect(0, 0, widthBy3, heightBy3);	image4Fil6Div3 = img4fil6div3; 
	CVisByteImage		img4filter6div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam4Rbw);
	img4filter6div3.SetRect(0, 0, widthBy3, heightBy3);	img4filter6div3.CopyPixelsTo(image4Fil6Div3);
	byteImg4Fil6Div3 = (unsigned char*)image4Fil6Div3.PixelAddress(0,0,0);

	CVisByteImage		img4fil6div6(widthBy6, heightBy6, 1, -1, byteImg4Fil6Div6);
	img4fil6div6.SetRect(0, 0, widthBy6, heightBy6);	image4Fil6Div6 = img4fil6div6; 
	CVisByteImage		img4filter6div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam4RbwDiv6);
	img4filter6div6.SetRect(0, 0, widthBy6, heightBy6);	img4filter6div6.CopyPixelsTo(image4Fil6Div6);
	byteImg4Fil6Div6 = (unsigned char*)image4Fil6Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 7
	CVisByteImage		img4fil7div3(widthBy3, heightBy3, 1, -1, byteImg4Fil7Div3);
	img4fil7div3.SetRect(0, 0, widthBy3, heightBy3);	image4Fil7Div3 = img4fil7div3; 
	CVisByteImage		img4filter7div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam4Gbw);
	img4filter7div3.SetRect(0, 0, widthBy3, heightBy3);	img4filter7div3.CopyPixelsTo(image4Fil7Div3);
	byteImg4Fil7Div3 = (unsigned char*)image4Fil7Div3.PixelAddress(0,0,0);

	CVisByteImage		img4fil7div6(widthBy6, heightBy6, 1, -1, byteImg4Fil7Div6);
	img4fil7div6.SetRect(0, 0, widthBy6, heightBy6);	image4Fil7Div6 = img4fil7div6; 
	CVisByteImage		img4filter7div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam4GbwDiv6);
	img4filter7div6.SetRect(0, 0, widthBy6, heightBy6);	img4filter7div6.CopyPixelsTo(image4Fil7Div6);
	byteImg4Fil7Div6 = (unsigned char*)image4Fil7Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 8
	CVisByteImage		img4fil8div3(widthBy3, heightBy3, 1, -1, byteImg4Fil8Div3);
	img4fil8div3.SetRect(0, 0, widthBy3, heightBy3);	image4Fil8Div3 = img4fil8div3; 
	CVisByteImage		img4filter8div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam4Bbw);
	img4filter8div3.SetRect(0, 0, widthBy3, heightBy3);	img4filter8div3.CopyPixelsTo(image4Fil8Div3);
	byteImg4Fil8Div3 = (unsigned char*)image4Fil8Div3.PixelAddress(0,0,0);

	CVisByteImage		img4fil8div6(widthBy6, heightBy6, 1, -1, byteImg4Fil8Div6);
	img4fil8div6.SetRect(0, 0, widthBy6, heightBy6);	image4Fil8Div6 = img4fil8div6; 
	CVisByteImage		img4filter8div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam4BbwDiv6);
	img4filter8div6.SetRect(0, 0, widthBy6, heightBy6);	img4filter8div6.CopyPixelsTo(image4Fil8Div6);
	byteImg4Fil8Div6 = (unsigned char*)image4Fil8Div6.PixelAddress(0,0,0);

	*/

	//////////////////////////////

	int sizepattBy3W = theapp->sizepattBy3_2W;
	int sizepattBy3H = theapp->sizepattBy3_2H;

	//int pattType;
/*	for(pattType=1; pattType<=3; pattType++)
	{
		bytePattBy6[pattType]	=(BYTE*)malloc(sizepattBy6*sizepattBy6);
		bytePattBy3[pattType]	=(BYTE*)malloc(sizepattBy3W*sizepattBy3H);

		CVisByteImage	imgtemp1_6(sizepattBy6, sizepattBy6, 1, -1, bytePattBy6[pattType]);
		imgtemp1_6.SetRect(0,0,sizepattBy6, sizepattBy6);	imagePattBy6[pattType] = imgtemp1_6; 
 		CVisByteImage	imgtmp1_6(sizepattBy6, sizepattBy6, 1, -1, pframe->m_pxaxis->bytePattBy6[pattType]);
		imgtmp1_6.SetRect(0,0,sizepattBy6, sizepattBy6);	imgtmp1_6.CopyPixelsTo(imagePattBy6[pattType]);

		CVisByteImage	imgtemp2(sizepattBy3W, sizepattBy3H, 1, -1, bytePattBy3[pattType]);
		imgtemp2.SetRect(0,0,sizepattBy3W, sizepattBy3H);	imagePattBy3[pattType] = imgtemp2; 
 		CVisByteImage	imgtmp2x(sizepattBy3W, sizepattBy3H, 1, -1, pframe->m_pxaxis->bytePattBy3[pattType]);
		imgtmp2x.SetRect(0,0,sizepattBy3W, sizepattBy3H);	imgtmp2x.CopyPixelsTo(imagePattBy3[pattType]);
	}*/

	//---------------------although it loops through 3 times, only pattType=1 is used for storing the pattern taught
	/*
	for(pattType=1; pattType<=3; pattType++)
	{
		CVisByteImage	imgtemp1(sizepattBy6, sizepattBy6, 1, -1, pframe->m_pxaxis->bytePattBy6[pattType]);
		imgtemp1.SetRect(0,0,sizepattBy6, sizepattBy6);	imagePattBy6[pattType] = imgtemp1; 
 	
		CVisByteImage	imgtemp2(sizepattBy3W, sizepattBy3H, 1, -1, pframe->m_pxaxis->bytePattBy3[pattType]);
		imgtemp2.SetRect(0,0,sizepattBy3W, sizepattBy3H);	imagePattBy3[pattType] = imgtemp2; 
	}
	*/

	/* 
	//Andrew's Test 2017-09-11
	for(int pattType=0; pattType<3; pattType++)
	{
		imagePattBy6[pattType] = CVisByteImage(sizepattBy6,sizepattBy6,1,-1,pframe->m_pxaxis->bytePattBy6[pattType]);
		imagePattBy6[pattType].SetRect(0,0,sizepattBy6,sizepattBy6);

		imagePattBy3[pattType] = CVisByteImage(sizepattBy3W,sizepattBy3H,1,-1,pframe->m_pxaxis->bytePattBy3[pattType]);
		imagePattBy3[pattType].SetRect(0,0,sizepattBy3W,sizepattBy3H);
	}
	 //End of Andrew's Test 2017-09-11 
	*/
	
	CVisByteImage	imagtemp1(sizepattBy6, sizepattBy6, 1, -1, pframe->m_pxaxis->bytePattBy6_method6);
	imagtemp1.SetRect(0,0,sizepattBy6, sizepattBy6);	imagePattBy6_M6 = imagtemp1; 
 	
	CVisByteImage	imagtemp2(sizepattBy3W, sizepattBy3H, 1, -1, pframe->m_pxaxis->bytePattBy3_method6);
	imagtemp2.SetRect(0,0,sizepattBy3W, sizepattBy3H);	imagePattBy3_M6 = imagtemp2;

	CVisByteImage	imagtemp3(sizepattBy6, sizepattBy6, 1, -1, pframe->m_pxaxis->bytePattBy6_method6_num3);
	imagtemp3.SetRect(0,0,sizepattBy6, sizepattBy6);	imagePattBy6_M6_Num3 = imagtemp3; 
 	
	CVisByteImage	imagtemp4(sizepattBy3W, sizepattBy3H, 1, -1, pframe->m_pxaxis->bytePattBy3_method6_num3);
	imagtemp4.SetRect(0,0,sizepattBy3W, sizepattBy3H);	imagePattBy3_M6_Num3 = imagtemp4;

	CVisByteImage	imagtemp5(sizepattBy6, sizepattBy6, 1, -1, pframe->m_pxaxis->bytePattBy6_method6_1); //pattern 1
	imagtemp5.SetRect(0,0,sizepattBy6, sizepattBy6);	imagePattBy6_M6_1 = imagtemp5; 
 	
	CVisByteImage	imagtemp6(sizepattBy3W, sizepattBy3H, 1, -1, pframe->m_pxaxis->bytePattBy3_method6_1);
	imagtemp6.SetRect(0,0,sizepattBy3W, sizepattBy3H);	imagePattBy3_M6_1 = imagtemp6; 

	//end of commenting code
	////////////////////

	//SetWindowPos(&wndTop,10,400,780,250,SWP_SHOWWINDOW | SWP_NOSIZE );
	
	if(theapp->jobinfo[pframe->CurrentJobNum].midBarY>480 || theapp->jobinfo[pframe->CurrentJobNum].midBarY<0 )
	{theapp->jobinfo[pframe->CurrentJobNum].midBarY=0;}

	if(theapp->jobinfo[pframe->CurrentJobNum].neckBarY>480 || theapp->jobinfo[pframe->CurrentJobNum].neckBarY<0 )
	{theapp->jobinfo[pframe->CurrentJobNum].neckBarY=0;}

	if(pframe->m_pxaxis->RTopBar.x>480 || pframe->m_pxaxis->RTopBar.x<50) 
	{pframe->m_pxaxis->RTopBar.x=50;}
	
	//if(theapp->jobinfo[pframe->CurrentJobNum].vertBarX>480 || theapp->jobinfo[pframe->CurrentJobNum].vertBarX <50 ) theapp->jobinfo[pframe->CurrentJobNum].vertBarX=50;
	//if(pframe->m_pxaxis->ShowBars){CheckDlgButton(IDC_BARS,1);}else{CheckDlgButton(IDC_BARS,0);}

	pframe->InspOpen=true;

	m_capmiddle.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].midBarY);
	
	pframe->CountEnable=false;

	if(pframe->m_pxaxis->MidTop.x <=50) {pframe->m_pxaxis->MidTop.x=320;}
	
	boltPos.y=theapp->jobinfo[pframe->CurrentJobNum].tgYpattern;
	boltPos.x=50;

	CheckDlgButton(IDC_LABINTUSEMETHOD1,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod1);
	CheckDlgButton(IDC_LABINTUSEMETHOD2,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2);
	CheckDlgButton(IDC_LABINTUSEMETHOD3,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3);
	CheckDlgButton(IDC_LABINTUSEMETHOD4,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4);
	CheckDlgButton(IDC_LABINTUSEMETHOD5,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5);
	CheckDlgButton(IDC_LABINTUSEMETHOD6,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6);

	UpdateDisplay();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Insp::InitialXLocations()
{
	//SinglePoint CapLSide=pframe->m_pxaxis->FindCapSide(pframe->m_pxaxis->im_data,theapp->jobinfo[pframe->CurrentJobNum].midBarY*2,true);
	
	//theapp->jobinfo[pframe->CurrentJobNum].vertBarX=CapLSide.x+theapp->jobinfo[pframe->CurrentJobNum].lDist;
	//theapp->jobinfo[pframe->CurrentJobNum].tBandX=CapLSide.x+pframe->m_pxaxis->PerfOffset.x;

}

void Insp::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==2) 
	{
		KillTimer(2);
		
		if(theapp->serPresent)			{pframe->m_pserial->SendChar(0,410);}

		pframe->m_pview->OnlineBox(1);
		InvalidateRect(NULL,false);
		pframe->Stop=true;
	}

	if (nIDEvent==4) 
	{
		KillTimer(4);
		InvalidateRect(NULL,false);
	}
	
	
	if (nIDEvent==5) 
	{
		KillTimer(5);
		pframe->SaveJob(pframe->CurrentJobNum);

	}

	CDialog::OnTimer(nIDEvent);
}

void Insp::OnMup() 
{
	boltPos.y-=res;
	m_capmiddle.Format("%3i",boltPos.y);

	UpdateData(false);
	InvalidateRect(NULL,false);
}

void Insp::OnMdn() 
{
	boltPos.y+=res;
	m_capmiddle.Format("%3i",boltPos.y);

	UpdateData(false);
	InvalidateRect(NULL,false);
}

void Insp::OnOK() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tgYpattern=boltPos.y;

	pframe->SaveJob(pframe->CurrentJobNum);
	pframe->PhotoEyeEnable(true);
	pframe->InspOpen=false;
	pframe->Stop=false;

	//pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	CDialog::OnOK();
}

void Insp::OnClose() 
{
	pframe->CountEnable=true;
	pframe->Stop=false;
	pframe->PhotoEyeEnable(true);
		
	CDialog::OnClose();
}

void Insp::OnNup() 
{
	boltPos.x-=res;
	m_capmiddle.Format("%3i",boltPos.x);

	UpdateData(false);
	InvalidateRect(NULL,false);
}

void Insp::OnNdn() 
{
	boltPos.x+=res;
	m_capmiddle.Format("%3i",boltPos.x);

	UpdateData(false);
	InvalidateRect(NULL,false);
}

void Insp::DisplayImage(CVisImageBase& image, CDC* pDC)
{
	CVisImageBase& refimage = image;
	assert(refimage.IsValid());
	refimage.DisplayInHdc(*pDC);
}

void Insp::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	COLORREF greencolor	= RGB(0,255,0);
	COLORREF redcolor	= RGB(255,0,0);
	COLORREF bluecolor	= RGB(0,0,150);
	COLORREF lbluecolor	= RGB(100,190,250);
	COLORREF graycolor	= RGB(190,190,190);
	COLORREF blackcolor	= RGB(50,50,50);
	COLORREF yellowcolor= RGB(255,255,0);
	COLORREF pattCol;

	CPen BluePen(PS_SOLID,	1, RGB(0,0,250));
	CPen LGrayPen(PS_SOLID, 1, RGB(200,200,200));
	CPen LRedPen(PS_SOLID,	1, RGB(230,30,80));
	CPen FRedPen(PS_SOLID,	3, RGB(255,0,0));
	CPen RedDash(PS_DASH,	1, RGB(255,0,0));
	CPen BlackPen(PS_SOLID, 2, RGB(0,0,0));
	CPen GreenPen(PS_SOLID,	1, RGB(0,255,0));
	CPen LGreenPen(PS_SOLID,1, RGB(0,255,0));
	CPen YellowPen(PS_DASH,	1, RGB(255,255,0));
	CPen WhitePen(PS_SOLID,	2, RGB(255,255,255));
	CPen AquaPen(PS_SOLID,	4, RGB(0,255,255));

	//To show Pattern1 on the screen
	if(once)	{bitmapl.CreateCompatibleBitmap(&dc, 12, 12); once=false;}
	
	bitmapl.SetBitmapBits(12*12*4, pframe->m_pxaxis->im_pat);   //Displaying a 12 x 12 pattern
	CDC templ; templ.CreateCompatibleDC(0);
	BITMAP bmpl;

	CBitmap *old =templ.SelectObject(&bitmapl);
	::GetObject(bitmapl, sizeof(BITMAP), &bmpl);
	//dc.StretchBlt(325, 34, 96, 96, &templ, 0, 0, 24, 24, SRCCOPY);

	dc.StretchBlt(290, 24, 96, 96, &templ, 0, 0, 24, 24, SRCCOPY);  //###############################      Method 1 Pattern being displayed   #####################
	
	/*
	StretchBlt(290[x-coordinate (in logical units) of the upper-left corner of the dest rect.],
	 24[y-coordinate (in logical units) of the upper-left corner of the dest rect],
	 96[width (in logical units) of the dest rect], 
	 96[height (in logical units) of the dest rect],
	 &templ[source device context],
	 0[x-coordinate (in logical units) of the upper-left corner of the source rect],
	 0[y-coordinate (in logical units) of the upper-left corner of the source rect] ,
	 24[width (in logical units) of the source rectangle],
	 24[height (in logical units) of the source rectangle], SRCCOPY)
	*/

	////////////////////////////////
	//dc.SetWindowOrg(-100,-40);
	dc.SetWindowOrg(-100,-24);   //############  Displaying the 4 camera's image of the container

	if(filterSelPatt==1)   //Based on teh radio button selected from "Filter Selection"
	{
		//CVisImageBase& refimage1=imageCoDiv3[1];
		//CVisImageBase& refimage2=imageCoDiv3[2];
		//CVisImageBase& refimage3=imageCoDiv3[3];
		//CVisImageBase& refimage4=imageCoDiv3[4];

		switch(cam)  // based on the radio button selection, only display 1 camera image
		{
			case 1: DisplayImage(image1BWDiv3, &dc); break;
			case 2:	DisplayImage(image2BWDiv3, &dc); break;
			case 3:	DisplayImage(image3BWDiv3, &dc); break;
			case 4:	DisplayImage(image4BWDiv3, &dc); break;
		}
	}
	else if(filterSelPatt==2)
	{
		switch(cam)
		{
			case 1: DisplayImage(image1Fil2Div3, &dc); break;
			case 2:	DisplayImage(image2Fil2Div3, &dc); break;
			case 3:	DisplayImage(image3Fil2Div3, &dc); break;
			case 4:	DisplayImage(image4Fil2Div3, &dc); break;
		}
	}
	else if(filterSelPatt==3)
	{
		switch(cam)
		{
			case 1: DisplayImage(image1Fil3Div3, &dc); break;
			case 2:	DisplayImage(image2Fil3Div3, &dc); break;
			case 3:	DisplayImage(image3Fil3Div3, &dc); break;
			case 4:	DisplayImage(image4Fil3Div3, &dc); break;
		}
	}
	else if(filterSelPatt==4)
	{
		switch(cam)
		{
			case 1: DisplayImage(image1Fil4Div3, &dc); break;
			case 2:	DisplayImage(image2Fil4Div3, &dc); break;
			case 3:	DisplayImage(image3Fil4Div3, &dc); break;
			case 4:	DisplayImage(image4Fil4Div3, &dc); break;
		}
	}
	else if(filterSelPatt==5)
	{
		switch(cam)
		{
			case 1: DisplayImage(image1Fil5Div3, &dc); break;
			case 2:	DisplayImage(image2Fil5Div3, &dc); break;
			case 3:	DisplayImage(image3Fil5Div3, &dc); break;
			case 4:	DisplayImage(image4Fil5Div3, &dc); break;
		}
	}
	else if(filterSelPatt==6)
	{
		switch(cam)
		{
			case 1: DisplayImage(image1Fil6Div3, &dc); break;
			case 2:	DisplayImage(image2Fil6Div3, &dc); break;
			case 3:	DisplayImage(image3Fil6Div3, &dc); break;
			case 4:	DisplayImage(image4Fil6Div3, &dc); break;
		}
	}
	else if(filterSelPatt==7)
	{
		switch(cam)
		{
			case 1: DisplayImage(image1Fil7Div3, &dc); break;
			case 2:	DisplayImage(image2Fil7Div3, &dc); break;
			case 3:	DisplayImage(image3Fil7Div3, &dc); break;
			case 4:	DisplayImage(image4Fil7Div3, &dc); break;
		}
	}
	else if(filterSelPatt==8)
	{
		switch(cam)
		{
			case 1: DisplayImage(image1Fil8Div3, &dc); break;
			case 2:	DisplayImage(image2Fil8Div3, &dc); break;
			case 3:	DisplayImage(image3Fil8Div3, &dc); break;
			case 4:	DisplayImage(image4Fil8Div3, &dc); break;
		}
	}

	//*** Search Limits
	int ylim;
	dc.SelectObject(&LRedPen);
	ylim = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt;
	dc.MoveTo(0, ylim);	dc.LineTo(150,ylim);
	dc.SelectObject(&RedDash);
	ylim = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt;
	dc.MoveTo(0, ylim);	dc.LineTo(150,ylim);

	/////////////

	int x, y;
	if(chosenMethod == Method6 || chosenMethod == Method2)
	{
		x	= theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_x/3;   //Only used if Method 6 is selected
		y	= theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_y/3;
	}
	else
	{
		pattSel = Pattern1;
		x	= theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattSel]/3;  //pattSel is always 1
		y	= theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattSel]/3;
	}

	int x2	= theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_x/3;   //Only used if Method 6 is selected
	int y2	= theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_y/3;

	int x3	= theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_x/3;   //Only used if Method 6 is selected
	int y3	= theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_y/3;

	//##Drawing the Green & Red boxes on the image
	dc.Draw3dRect(boltPos.x-6,boltPos.y-6, 12, 12,greencolor,greencolor);
	dc.Draw3dRect(x-sizepattBy3W/2,y-sizepattBy3H/2, sizepattBy3W, sizepattBy3H,redcolor,redcolor);  

	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6)
	{
		dc.Draw3dRect(x2-sizepattBy3W/2,y2-sizepattBy3H/2, sizepattBy3W, sizepattBy3H,bluecolor,bluecolor);		
		dc.Draw3dRect(x3-sizepattBy3W/2,y3-sizepattBy3H/2, sizepattBy3W, sizepattBy3H,yellowcolor,yellowcolor);		
	}

	/////////////////////

	int xstart	=	-260; //-280
	int ystart	=	-220;//-255
	int xoff	=	70;//40

	if(chosenMethod == Method6 || chosenMethod == Method2)
	{
		if(pattSel == Pattern1)
		{
			x	= theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_x/6;   //Only used if Method 6 is selected
			y	= theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_y/6;
		}
		else if(pattSel == Pattern2)
		{
			x	= theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_x/6;   //Only used if Method 6 is selected
			y	= theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_y/6;
		}
		else if(pattSel == Pattern3)
		{
			x	= theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_x/6;   //Only used if Method 6 is selected
			y	= theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_y/6;
		}
		else
		{
			x	= theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattSel]/6;   //Only used if Method 6 is selected
			y	= theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattSel]/6;
		}
	}
	else
	{
		pattSel = Pattern1;
		x	 = theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattSel]/6;
		y	 = theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattSel]/6;
	}
	//x2	 = theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[2]/6;
	//y2	 = theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[2]/6;

	int szPat= sizepattBy12;
	
	if(pattSel==2)	pattCol=bluecolor;
	else if(pattSel==3)	pattCol=yellowcolor;
	else pattCol=redcolor;

	dc.SetWindowOrg(xstart-0*xoff, ystart);
	
	//##############--------------------  METHOD 2 ----------------#######################

	//#### Based on the filter selected, the smaller (By 12) image is displayed under Method 2
	//Cam 1
	if(filterSelPatt==1){DisplayImage(image1BWDiv6,&dc);}
	else if(filterSelPatt==2){DisplayImage(image1Fil2Div6,&dc);}
	else if(filterSelPatt==3){DisplayImage(image1Fil3Div6,&dc);}
	else if(filterSelPatt==4){DisplayImage(image1Fil4Div6,&dc);}
	else if(filterSelPatt==5){DisplayImage(image1Fil5Div6,&dc);}
	else if(filterSelPatt==6){DisplayImage(image1Fil6Div6,&dc);}
	else if(filterSelPatt==7){DisplayImage(image1Fil7Div6,&dc);}
	else if(filterSelPatt==8){DisplayImage(image1Fil8Div6,&dc);}

	if(cam==1) 
	{
		dc.Draw3dRect(x-szPat/2, y-szPat/2, szPat, szPat, pattCol,pattCol);
		//dc.Draw3dRect(x2-szPat/2,y2-szPat/2, szPat, szPat, pattCol,pattCol);
	}

	//////////////////
		//Cam 2
	dc.SetWindowOrg(xstart-1*xoff, ystart);

	if(filterSelPatt==1){DisplayImage(image2BWDiv6,&dc);}
	else if(filterSelPatt==2){DisplayImage(image2Fil2Div6,&dc);}
	else if(filterSelPatt==3){DisplayImage(image2Fil3Div6,&dc);}
	else if(filterSelPatt==4){DisplayImage(image2Fil4Div6,&dc);}
	else if(filterSelPatt==5){DisplayImage(image2Fil5Div6,&dc);}
	else if(filterSelPatt==6){DisplayImage(image2Fil6Div6,&dc);}
	else if(filterSelPatt==7){DisplayImage(image2Fil7Div6,&dc);}
	else if(filterSelPatt==8){DisplayImage(image2Fil8Div6,&dc);}

	if(cam==2) 
	{
		dc.Draw3dRect(x-szPat/2, y-szPat/2, szPat, szPat, pattCol,pattCol);
		//dc.Draw3dRect(x2-szPat/2,y2-szPat/2, szPat, szPat, pattCol,pattCol);
	}

	////////////////
	//Cam 3
	dc.SetWindowOrg(xstart-2*xoff, ystart);

	if(filterSelPatt==1){DisplayImage(image3BWDiv6,&dc);}
	else if(filterSelPatt==2){DisplayImage(image3Fil2Div6,&dc);}
	else if(filterSelPatt==3){DisplayImage(image3Fil3Div6,&dc);}
	else if(filterSelPatt==4){DisplayImage(image3Fil4Div6,&dc);}
	else if(filterSelPatt==5){DisplayImage(image3Fil5Div6,&dc);}
	else if(filterSelPatt==6){DisplayImage(image3Fil6Div6,&dc);}
	else if(filterSelPatt==7){DisplayImage(image3Fil7Div6,&dc);}
	else if(filterSelPatt==8){DisplayImage(image3Fil8Div6,&dc);}

	if(cam==3) 
	{
		dc.Draw3dRect(x-szPat/2, y-szPat/2, szPat, szPat, pattCol,pattCol);
	}

	////////////////////////
	//Cam 4
	dc.SetWindowOrg(xstart-3*xoff, ystart);

	if(filterSelPatt==1){DisplayImage(image4BWDiv6,&dc);}
	else if(filterSelPatt==2){DisplayImage(image4Fil2Div6,&dc);}
	else if(filterSelPatt==3){DisplayImage(image4Fil3Div6,&dc);}
	else if(filterSelPatt==4){DisplayImage(image4Fil4Div6,&dc);}
	else if(filterSelPatt==5){DisplayImage(image4Fil5Div6,&dc);}
	else if(filterSelPatt==6){DisplayImage(image4Fil6Div6,&dc);}
	else if(filterSelPatt==7){DisplayImage(image4Fil7Div6,&dc);}
	else if(filterSelPatt==8){DisplayImage(image4Fil8Div6,&dc);}

	if(cam==4) 
	{
		dc.Draw3dRect(x-szPat/2, y-szPat/2, szPat, szPat, pattCol,pattCol);
		//dc.Draw3dRect(x2-szPat/2,y2-szPat/2, szPat, szPat, pattCol,pattCol);
	}

	//###########---------- Displaying the 2 patterns for the pattern match ----------###
	//Template By12
	int xini;	int yini;
	//int szPat3W;	int szPat3H;
/*
	//Now drawing the 2 new patterns when Method 6 is slected
	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6)
	{
		yini = ystart-200;//100;

		//Template By6
		yini = ystart-192;//130;

		xini = xstart-0.75*xoff+25; //1.35*xoff; 
		dc.SetWindowOrg(xini, yini);
		DisplayImage(imagePattBy6_M6_1,&dc);			//	imagePattBy6_M6_1	OLD:	imagePattBy6[1]
		dc.Draw3dRect(-4, -4, szPat + 8, szPat + 8, redcolor, redcolor);

		//To show Pattern2 on the screen
		//Template By3
		int szPat3W= sizepattBy3W;
		int szPat3H= sizepattBy3H;

		xini = xstart-1.5*xoff+22; //0.5*xoff; 
		dc.SetWindowOrg(xini, yini);
		DisplayImage(imagePattBy3_M6_1, &dc);				//					OLD:	imagePattBy3[1]
		dc.Draw3dRect(-4, -4, szPat3W + 8, szPat3H + 8, redcolor, redcolor);

		//Template By6
		yini = ystart-240;//130;

		xini = xstart-0.75*xoff+25; //1.35*xoff; 
		dc.SetWindowOrg(xini, yini);
		DisplayImage(imagePattBy6_M6, &dc);
		dc.Draw3dRect(-4, -4, szPat + 8, szPat + 8, redcolor, redcolor);

		//To show Pattern2 on the screen
		//Template By3
		szPat3W= sizepattBy3W;
		szPat3H= sizepattBy3H;

		xini = xstart-1.5*xoff+22; //0.5*xoff; 
		dc.SetWindowOrg(xini, yini);
		DisplayImage(imagePattBy3_M6, &dc);
		dc.Draw3dRect(-4, -4, szPat3W + 8, szPat3H + 8, redcolor, redcolor);

		//###Now drawing Pattern Number 3

			//Template By6
		yini = ystart-292;//130;

		xini = xstart-0.75*xoff+25; //1.35*xoff; 
		dc.SetWindowOrg(xini, yini);
		DisplayImage(imagePattBy6_M6_Num3, &dc);
		dc.Draw3dRect(-4, -4, szPat + 8, szPat + 8, redcolor, redcolor);

		//To show Pattern2 on the screen
		//Template By3	 

		xini = xstart-1.5*xoff+22; //0.5*xoff; 
		dc.SetWindowOrg(xini, yini);
		DisplayImage(imagePattBy3_M6_Num3, &dc);
		dc.Draw3dRect(-4, -4, szPat3W + 8, szPat3H + 8, redcolor, redcolor);	
	}
	*/
	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2 || theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6)
	{
		yini = ystart-200;//100;

		//Template By6
		yini = ystart-192;//130;

		xini = xstart-0.75*xoff+25; //1.35*xoff; 
		dc.SetWindowOrg(xini, yini);
		//DisplayImage(imagePattBy6[0], &dc);
		DisplayImage(imagePattBy6_M6_1,&dc);			//	OLD:	imagePattBy6[1]
		dc.Draw3dRect(-4, -4, szPat + 8, szPat + 8, redcolor, redcolor);

		//To show Pattern2 on the screen
		//Template By3
		int szPat3W= sizepattBy3W;
		int szPat3H= sizepattBy3H;

		xini = xstart-1.5*xoff+22; //0.5*xoff; 
		dc.SetWindowOrg(xini, yini);
		DisplayImage(imagePattBy3_M6_1, &dc);				//					OLD:	imagePattBy3[1]
		//DisplayImage(imagePattBy3[0], &dc);
		dc.Draw3dRect(-4, -4, szPat3W + 8, szPat3H + 8, redcolor, redcolor);

		if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6)
		{
			//Template By6
			yini = ystart-240;//130;

			xini = xstart-0.75*xoff+25; //1.35*xoff; 
			dc.SetWindowOrg(xini, yini);
			DisplayImage(imagePattBy6_M6, &dc);
			//DisplayImage(imagePattBy6[1], &dc);
			dc.Draw3dRect(-4, -4, szPat + 8, szPat + 8, redcolor, redcolor);

			//To show Pattern2 on the screen
			//Template By3
			szPat3W= sizepattBy3W;
			szPat3H= sizepattBy3H;

			xini = xstart-1.5*xoff+22; //0.5*xoff; 
			dc.SetWindowOrg(xini, yini);
			DisplayImage(imagePattBy3_M6, &dc);
			//DisplayImage(imagePattBy3[1], &dc);
			dc.Draw3dRect(-4, -4, szPat3W + 8, szPat3H + 8, redcolor, redcolor);

			//###Now drawing Pattern Number 3

			//Template By6
			yini = ystart-292;//130;

			xini = xstart-0.75*xoff+25; //1.35*xoff; 
			dc.SetWindowOrg(xini, yini);
			DisplayImage(imagePattBy6_M6_Num3, &dc);
			//DisplayImage(imagePattBy6[2], &dc);
			dc.Draw3dRect(-4, -4, szPat + 8, szPat + 8, redcolor, redcolor);

			//To show Pattern2 on the screen
			//Template By3	 

			xini = xstart-1.5*xoff+22; //0.5*xoff; 
			dc.SetWindowOrg(xini, yini);
			DisplayImage(imagePattBy3_M6_Num3, &dc);
			//DisplayImage(imagePattBy3[2], &dc);
			dc.Draw3dRect(-4, -4, szPat3W + 8, szPat3H + 8, redcolor, redcolor);	
		}
	}/*
	else
	{
		yini = ystart-200;//100;

		//Template By6
		yini = ystart-192;//130;

		xini = xstart-0.75*xoff+25; //1.35*xoff; 
		dc.SetWindowOrg(xini, yini);
		DisplayImage(imagePattBy6[1],&dc);
		dc.Draw3dRect(-4, -4, szPat + 8, szPat + 8, redcolor, redcolor);

		//To show Pattern2 on the screen
		//Template By3
		szPat3W= sizepattBy3W;
		szPat3H= sizepattBy3H;

		xini = xstart-1.5*xoff+22; //0.5*xoff; 
		dc.SetWindowOrg(xini, yini);
		DisplayImage(imagePattBy3[1], &dc);
		dc.Draw3dRect(-4, -4, szPat3W + 8, szPat3H + 8, redcolor, redcolor);
	}*/
}

void Insp::OnTrigger() 
{
	pframe->Stop=false;
	pframe->SendMessage(WM_TRIGGER,NULL,NULL);

	LoadCameraImages();

	SetTimer(2,100,NULL);
	SetTimer(4,200,NULL);
}

void Insp::OnTrigger3() 
{
	pframe->PhotoEyeEnable(true);
	pframe->WaitNext=true;

	InitialXLocations();
	InvalidateRect(NULL,false);
	SetTimer(4,1000,NULL);
	UpdateData(false);
}

void Insp::OnInspectPushed() 
{
	if(pframe->CurrentJobNum>=1 && pframe->CurrentJobNum <=theapp->SavedTotalJobs)
	{	
		if(cam==1)
		{
			if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattern2(byteImg1BWDiv3,	boltPos);}
			else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattern2(byteImg1Fil2Div3,	boltPos);}
			else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattern2(byteImg1Fil3Div3,	boltPos); }
			else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattern2(byteImg1Fil4Div3,	boltPos); }
			else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattern2(byteImg1Fil5Div3,	boltPos); }
			else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattern2(byteImg1Fil6Div3,	boltPos); }
			else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattern2(byteImg1Fil7Div3,	boltPos); }
			else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattern2(byteImg1Fil8Div3,	boltPos); }
		}
		else if(cam==2)
		{
			if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattern2(byteImg2BWDiv3,	boltPos);}
			else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattern2(byteImg2Fil2Div3,	boltPos);}
			else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattern2(byteImg2Fil3Div3,	boltPos); }
			else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattern2(byteImg2Fil4Div3,	boltPos); }
			else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattern2(byteImg2Fil5Div3,	boltPos); }
			else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattern2(byteImg2Fil6Div3,	boltPos); }
			else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattern2(byteImg2Fil7Div3,	boltPos); }
			else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattern2(byteImg2Fil8Div3,	boltPos); }
		}
		else if(cam==3)
		{
			if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattern2(byteImg3BWDiv3,	boltPos);}
			else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattern2(byteImg3Fil2Div3,	boltPos);}
			else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattern2(byteImg3Fil3Div3,	boltPos); }
			else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattern2(byteImg3Fil4Div3,	boltPos); }
			else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattern2(byteImg3Fil5Div3,	boltPos); }
			else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattern2(byteImg3Fil6Div3,	boltPos); }
			else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattern2(byteImg3Fil7Div3,	boltPos); }
			else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattern2(byteImg3Fil8Div3,	boltPos); }
		}
		else if (cam==4)
		{
			if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattern2(byteImg4BWDiv3,	boltPos);}
			else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattern2(byteImg4Fil2Div3,	boltPos);}
			else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattern2(byteImg4Fil3Div3,	boltPos); }
			else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattern2(byteImg4Fil4Div3,	boltPos); }
			else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattern2(byteImg4Fil5Div3,	boltPos); }
			else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattern2(byteImg4Fil6Div3,	boltPos); }
			else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattern2(byteImg4Fil7Div3,	boltPos); }
			else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattern2(byteImg4Fil8Div3,	boltPos); }
		}

		pframe->m_pxaxis->colorReady=false;

/////////////////////
/*
		pframe->m_pxaxis->LearnPattern2X(byteImgBWDiv3[cam],boltPos); 
		int sizepatt2W = theapp->sizepattBy3_2W;
		int sizepatt2H = theapp->sizepattBy3_2H;

		CVisByteImage	imgtemp2(sizepatt2W, sizepatt2H, 1, -1, bytePatt2);
		imgtemp2.SetRect(0,0,sizepatt2W, sizepatt2H);	imagePatt2 = imgtemp2; 
 		CVisByteImage	imgtmp2x(sizepatt2W, sizepatt2H, 1, -1, pframe->m_pxaxis->im_mat2);
		imgtmp2x.SetRect(0,0,sizepatt2W, sizepatt2H);	imgtmp2x.CopyPixelsTo(imagePatt2);
*/
/////////////////////
	}
	else {AfxMessageBox("You must load a job first!(1)");}

	InvalidateRect(NULL,false);	
}

void Insp::OnRadio1()	{ cam=1; InvalidateRect(NULL,false); }

void Insp::OnRadio2()	{ cam=2; InvalidateRect(NULL,false); }

void Insp::OnRadio3()	{ cam=3; InvalidateRect(NULL,false); }

void Insp::OnRadio4()	{ cam=4; InvalidateRect(NULL,false); }

void Insp::OnCameras() 
{
	if(!theapp->lock)
	{
		m_b1.ShowWindow(SW_SHOW);
		m_b2.ShowWindow(SW_SHOW);
		m_align.ShowWindow(SW_SHOW);
	}
	else
	{
		Security sec;
		sec.DoModal();

		if(!theapp->lock)
		{
			m_b1.ShowWindow(SW_SHOW);
			m_b2.ShowWindow(SW_SHOW);
			m_align.ShowWindow(SW_SHOW);
		}
	}

	UpdateDisplay();
}

void Insp::OnButton5() 
{
	switch(cam)
	{
		case 1:	if(theapp->cam1Offset>0) {theapp->cam1Offset-=1;} break;
		case 2:	if(theapp->cam2Offset>0) {theapp->cam2Offset-=1;} break;
		case 3:	if(theapp->cam3Offset>0) {theapp->cam3Offset-=1;} break;
		case 4:	if(theapp->cam4Offset>0) {theapp->cam4Offset-=1;} break;
	}

	UpdateDisplay();
}

void Insp::OnButton6() 
{
	switch(cam)
	{
		case 1:	theapp->cam1Offset+=1;	break;
		case 2:	theapp->cam2Offset+=1;	break;
		case 3:	theapp->cam3Offset+=1;	break;
		case 4:	theapp->cam4Offset+=1;	break;
	}

	UpdateDisplay();
}

void Insp::OnChk1() 
{
	setAllCheckBoxesToFalse();

	theapp->jobinfo[pframe->CurrentJobNum].bottleStyle=1;
	theapp->jobinfo[pframe->CurrentJobNum].type=1;

	UpdateValues();	
}

void Insp::OnChk2() 
{
	setAllCheckBoxesToFalse();

	theapp->jobinfo[pframe->CurrentJobNum].bottleStyle=2;
	theapp->jobinfo[pframe->CurrentJobNum].type=0;

	UpdateValues();	
}

void Insp::OnChk3() 
{
	setAllCheckBoxesToFalse();
	theapp->jobinfo[pframe->CurrentJobNum].bottleStyle=3;

	UpdateValues();	
}

void Insp::OnChk4() 
{
	setAllCheckBoxesToFalse();

	theapp->jobinfo[pframe->CurrentJobNum].bottleStyle=4;
	theapp->jobinfo[pframe->CurrentJobNum].type=1;

	UpdateValues();	
}

void Insp::OnChk5() 
{
	setAllCheckBoxesToFalse();

	theapp->jobinfo[pframe->CurrentJobNum].bottleStyle=5;
	theapp->jobinfo[pframe->CurrentJobNum].type=0;

	UpdateValues();	
}

void Insp::OnChk6() 
{
	setAllCheckBoxesToFalse();

	theapp->jobinfo[pframe->CurrentJobNum].bottleStyle=6;
	theapp->jobinfo[pframe->CurrentJobNum].type=0;

	UpdateValues();	
}

void Insp::OnChk7() 
{
	int sport = theapp->jobinfo[pframe->CurrentJobNum].sport;

	if(sport==0)	{sport=1;}
	else			{sport=0;}

	theapp->jobinfo[pframe->CurrentJobNum].sport = sport > 0;
	//////////////

	UpdateValues();	
}

void Insp::OnChk8() 
{
	setAllCheckBoxesToFalse();

	theapp->jobinfo[pframe->CurrentJobNum].bottleStyle=8;
	theapp->jobinfo[pframe->CurrentJobNum].type=0;

	UpdateValues();			
}

void Insp::OnChk9() 
{
	setAllCheckBoxesToFalse();

	theapp->jobinfo[pframe->CurrentJobNum].bottleStyle=9;
	theapp->jobinfo[pframe->CurrentJobNum].type=0;

	UpdateValues();			
}

void Insp::OnChk10() 
{
	setAllCheckBoxesToFalse();

	theapp->jobinfo[pframe->CurrentJobNum].bottleStyle=10;
	theapp->jobinfo[pframe->CurrentJobNum].type=1;

	UpdateValues();		
}

void Insp::OnChk11() 
{
	setAllCheckBoxesToFalse();

	theapp->jobinfo[pframe->CurrentJobNum].bottleStyle=11;
	theapp->jobinfo[pframe->CurrentJobNum].type=1;

	UpdateValues();			
}

void Insp::OnFineins() 
{
	if(res==1)
	{m_fine.Format("%s","Fast"); CheckDlgButton(IDC_FINEINS,1); res=10; resRes12=24; resRes6=12;}
	else
	{m_fine.Format("%s","Slow"); CheckDlgButton(IDC_FINEINS,0);	res=1;	resRes12=12; resRes6=6;}

	UpdateData(false);		
}

void Insp::OnTargleft() 
{	pattSel = 1;	InvalidateRect(NULL,false);	}

void Insp::OnTargmiddle() 
{	pattSel = 2;	InvalidateRect(NULL,false);	}

void Insp::OnTargright() 
{	pattSel = 3;	InvalidateRect(NULL,false);	}

void Insp::OnTargpattup() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 || theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2)   	//In Case of vertical displacemenet, move both patterns together
	{
		CButton *m_ctlCheck = (CButton*) GetDlgItem(IDC_Method6_Pattern1);  ///For Pattern 1
		int ChkBox = m_ctlCheck->GetCheck();
		if( ChkBox == BST_CHECKED)
		{
			pattSel = Pattern1;
			theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_y-=resRes6;

			if(	theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_y<12)
			{theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_y = 12;}
		}

		CButton *m_ctlCheck2 = (CButton*) GetDlgItem(IDC_Method6_Pattern2);  //For pattern 2
		int ChkBox2 = m_ctlCheck2->GetCheck();
		if( ChkBox2 == BST_CHECKED)
		{
			pattSel = Pattern2;
			theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_y-=resRes6;
			if(	theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_y<12)
			{theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_y = 12;}
		}

		CButton *m_ctlCheck3 = (CButton*) GetDlgItem(IDC_Method6_Pattern3);  //For pattern 3
		int ChkBox3 = m_ctlCheck3->GetCheck();
		if( ChkBox3 == BST_CHECKED)
		{
			pattSel = Pattern3;
			theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_y-=resRes6;
			if(	theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_y<12)
			{theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_y = 12;}
		}
	}
	else   // if any other method except for Method 6
	{
		pattSel = Pattern1;
		theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattSel]-=resRes6;

		if(	theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattSel]<12)
		{theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattSel] = 12;}
	}

	UpdateValues();		
}

void Insp::OnTargpattdw() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 || theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2)
	{		//In Case of vertical displacemenet, move both patterns together
		CButton *m_ctlCheck = (CButton*) GetDlgItem(IDC_Method6_Pattern1);  ///For Pattern 1
		int ChkBox = m_ctlCheck->GetCheck();
		if( ChkBox == BST_CHECKED)
		{
			pattSel = Pattern1;
			theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_y+=resRes6;

			if(	theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_y+12>HEIGHT)
			{theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_y-=resRes6;}	
		}

		CButton *m_ctlCheck2 = (CButton*) GetDlgItem(IDC_Method6_Pattern2);  //For pattern 2
		int ChkBox2 = m_ctlCheck2->GetCheck();
		if( ChkBox2 == BST_CHECKED)
		{
			pattSel = Pattern2;
			theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_y+=resRes6;

			if(	theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_y+12>HEIGHT)
			{theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_y-=resRes6;}
		}

		CButton *m_ctlCheck3 = (CButton*) GetDlgItem(IDC_Method6_Pattern3);  //For pattern 3
		int ChkBox3 = m_ctlCheck3->GetCheck();
		if( ChkBox3 == BST_CHECKED)
		{
			pattSel = Pattern3;
			theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_y+=resRes6;

			if(	theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_y+12>HEIGHT)
			{theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_y-=resRes6;}
		}
	}
	else   // if any other method except for Method 6
	{
		pattSel = Pattern1;
		theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattSel]+=resRes6;

		if(	theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattSel]+12>HEIGHT)
		{theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattSel]-=resRes6;}
	}
	
	UpdateValues();		
}

void Insp::OnTargpattright() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 || theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2)
	{
		CButton *m_ctlCheck = (CButton*) GetDlgItem(IDC_Method6_Pattern1);  ///For Pattern 1
		int ChkBox = m_ctlCheck->GetCheck();
		if( ChkBox == BST_CHECKED)
		{
			pattSel = Pattern1;
			theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_x+=resRes6;

			if(	theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_x+12>WIDTH)
			{theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_x -= resRes6;}
		}

		CButton *m_ctlCheck2 = (CButton*) GetDlgItem(IDC_Method6_Pattern2);  //For pattern 2
		int ChkBox2 = m_ctlCheck2->GetCheck();
		if( ChkBox2 == BST_CHECKED)
		{
			pattSel = Pattern2;
			theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_x+=resRes6;

			if(	theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_x+12>WIDTH)
			{theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_x -= resRes6;}
		}

		CButton *m_ctlCheck3 = (CButton*) GetDlgItem(IDC_Method6_Pattern3);  //For pattern 3
		int ChkBox3 = m_ctlCheck3->GetCheck();
		if( ChkBox3 == BST_CHECKED)
		{
			pattSel = Pattern3;
			theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_x+=resRes6;

			if(	theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_x+12>WIDTH)
			{theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_x -= resRes6;}
		}
	}
	else   // if any other method except for Method 6
	{
		pattSel = Pattern1;
		theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattSel]+=resRes6;

		if(	theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattSel]+12>WIDTH)
		{theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattSel] -= resRes6;}
	}

	UpdateValues();			
}

void Insp::OnTargpattleft() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 || theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2)
	{
		CButton *m_ctlCheck = (CButton*) GetDlgItem(IDC_Method6_Pattern1);  ///For Pattern 1
		int ChkBox = m_ctlCheck->GetCheck();
		if( ChkBox == BST_CHECKED)
		{
			pattSel = Pattern1;
			theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_x-=resRes6;

			if(	theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_x+12<12)
			{theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_x = 12;}
		}

		CButton *m_ctlCheck2 = (CButton*) GetDlgItem(IDC_Method6_Pattern2);  //For pattern 2
		int ChkBox2 = m_ctlCheck2->GetCheck();
		if( ChkBox2 == BST_CHECKED)
		{
			pattSel = Pattern2;
			theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_x-=resRes6;

			if(	theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_x+12<12)
			{theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_x = 12;}
		}

		CButton *m_ctlCheck3 = (CButton*) GetDlgItem(IDC_Method6_Pattern3);  //For pattern 3
		int ChkBox3 = m_ctlCheck3->GetCheck();
		if( ChkBox3 == BST_CHECKED)
		{
			pattSel = Pattern3;
			theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_x-=resRes6;

			if(	theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_x+12<12)
			{theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_x = 12;}
		}
	}
	else   // if any other method except for Method 6
	{
		pattSel = Pattern1;
		theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattSel]-=resRes6;

		if(	theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattSel]<12)
		{theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattSel] = 12;}
	}

	UpdateValues();		
}

void Insp::UpdateDisplay()
{
	if(!theapp->lock)
	{
		GetDlgItem(IDC_TITLE1)->ShowWindow(true);
		GetDlgItem(IDC_TITLE2)->ShowWindow(true);
		GetDlgItem(IDC_TITLE3)->ShowWindow(true);
		GetDlgItem(IDC_TITLE4)->ShowWindow(true);

		m_offset1.Format("%3i", theapp->cam1Offset);
		m_offset2.Format("%3i", theapp->cam2Offset);
		m_offset3.Format("%3i", theapp->cam3Offset);
		m_offset4.Format("%3i", theapp->cam4Offset);
	}
	else
	{
		GetDlgItem(IDC_TITLE1)->ShowWindow(false);
		GetDlgItem(IDC_TITLE2)->ShowWindow(false);
		GetDlgItem(IDC_TITLE3)->ShowWindow(false);
		GetDlgItem(IDC_TITLE4)->ShowWindow(false);
	}

	m_botLimPatt.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt);
	m_topLimPatt.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt);

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	if(EdgeTransition==0){CheckDlgButton(IDC_LIGHTTODARK,1);}else{CheckDlgButton(IDC_LIGHTTODARK,0);}
	if(EdgeTransition==1){CheckDlgButton(IDC_DARKTOLIGHT,1);}else{CheckDlgButton(IDC_DARKTOLIGHT,0);}

	int filterSelPatt = theapp->jobinfo[pframe->CurrentJobNum].filterSelPatt;

	if(filterSelPatt==1){CheckDlgButton(IDC_FILTER1,1);}else{CheckDlgButton(IDC_FILTER1,0);}
	if(filterSelPatt==2){CheckDlgButton(IDC_FILTER2,1);}else{CheckDlgButton(IDC_FILTER2,0);}
	if(filterSelPatt==3){CheckDlgButton(IDC_FILTER3,1);}else{CheckDlgButton(IDC_FILTER3,0);}
	if(filterSelPatt==4){CheckDlgButton(IDC_FILTER4,1);}else{CheckDlgButton(IDC_FILTER4,0);}
	if(filterSelPatt==5){CheckDlgButton(IDC_FILTER5,1);}else{CheckDlgButton(IDC_FILTER5,0);}
	if(filterSelPatt==6){CheckDlgButton(IDC_FILTER6,1);}else{CheckDlgButton(IDC_FILTER6,0);}
	if(filterSelPatt==7){CheckDlgButton(IDC_FILTER7,1);}else{CheckDlgButton(IDC_FILTER7,0);}
	if(filterSelPatt==8){CheckDlgButton(IDC_FILTER8,1);}else{CheckDlgButton(IDC_FILTER8,0);}

	int bottleStyle=theapp->jobinfo[pframe->CurrentJobNum].bottleStyle;

	CheckDlgButton(IDC_CHK1,0);
	CheckDlgButton(IDC_CHK2,0);
	CheckDlgButton(IDC_CHK3,0);
	CheckDlgButton(IDC_CHK4,0);
	CheckDlgButton(IDC_CHK5,0);
	CheckDlgButton(IDC_CHK6,0);
	CheckDlgButton(IDC_CHK8,0);
	CheckDlgButton(IDC_CHK9,0);
	CheckDlgButton(IDC_CHK10,0);
	CheckDlgButton(IDC_CHK11,0);

	if(bottleStyle==1)	{CheckDlgButton(IDC_CHK1,1);}
	if(bottleStyle==2)	{CheckDlgButton(IDC_CHK2,1);}
	if(bottleStyle==3)	{CheckDlgButton(IDC_CHK3,1);}
	if(bottleStyle==4)	{CheckDlgButton(IDC_CHK4,1);}
	if(bottleStyle==5)	{CheckDlgButton(IDC_CHK5,1);}
	if(bottleStyle==6)	{CheckDlgButton(IDC_CHK6,1);}
	if(bottleStyle==8)	{CheckDlgButton(IDC_CHK8,1);}
	if(bottleStyle==9)	{CheckDlgButton(IDC_CHK9,1);}
	if(bottleStyle==10)	{CheckDlgButton(IDC_CHK10,1);}
	if(bottleStyle==11)	{CheckDlgButton(IDC_CHK11,1);}

	///////////////
	
	CheckDlgButton(IDC_SELBODY1,0);
	CheckDlgButton(IDC_SELBODY2,0);
	CheckDlgButton(IDC_SELBODY3,0);

	if(bottleStyle==10)			{CheckDlgButton(IDC_SELBODY2,1);}	//Square
	else if(bottleStyle==3)		{CheckDlgButton(IDC_SELBODY3,1);}	//Wolverine
	else						{CheckDlgButton(IDC_SELBODY1,1);}	//Default:Cylindrical

	//Cap selection
	CheckDlgButton(IDC_CHK7,0);
	CheckDlgButton(IDC_CAPSEL1,0);
	if(theapp->jobinfo[pframe->CurrentJobNum].sport==0)	{CheckDlgButton(IDC_CAPSEL1,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)	{CheckDlgButton(IDC_CHK7,1);}

	///////////////
	CheckDlgButton(IDC_SELCAP0,0);
	CheckDlgButton(IDC_SELCAP1,0);
	if(theapp->jobinfo[pframe->CurrentJobNum].sport==0)	{CheckDlgButton(IDC_SELCAP0,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)	{CheckDlgButton(IDC_SELCAP1,1);}

	///////////////

	m_sensMet3.Format("%3i",	theapp->jobinfo[pframe->CurrentJobNum].sensMet3);
	m_sensEdge.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].sensEdge);

	int selX = 12, selY = 12;
	if(chosenMethod == Method6 || chosenMethod == Method2)
	{
		if(pattSel == Pattern1)
		{
			selX = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_x;
			selY = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_y;
		}
		else if(pattSel == Pattern2)
		{
			selX = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_x;
			selY = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_y;
		}
		else if(pattSel == Pattern3)
		{
			selX = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_x;
			selY = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_y;
		}
		else
		{
			selX = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_x;
			selY = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_y;
		}
	}
	else
	{
		pattSel = Pattern1;
		selX = theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattSel];
		selY = theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattSel];

		//	Pattern 3?
	}

	m_tgXYpatt.Format("%i, %i", selX, selY);
	m_tgXYpatt2.Format("%i, %i", theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[2],
								theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[2]);

	///////////////////////////////////////////////////

	if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)	{theapp->inspctn[2].name="Cap Sport";}
	else												{theapp->inspctn[2].name="Cap Mid Height";}

	///////////////////////////////////////////////////

	//switch(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle)
	//{
	//	case 10:theapp->inspctn[4].name="Skewness";		break;
	//	default:theapp->inspctn[4].name="LipPattern";		break;
	//}
	

	switch(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle)
	{
		case 10:theapp->inspctn[12].name="Skewness";		break;
		default:theapp->inspctn[12].name="Alignment";		break;
	}
	
	switch(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle)
	{
		case 3: theapp->inspctn[6].name="Lbl Rotate";	break;
		case 10:theapp->inspctn[6].name="ShiftLbl";		break;
		default:theapp->inspctn[6].name="Spare";		break;
	}

	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod1)
	{CheckDlgButton(IDC_LABINTUSEMETHOD1,1); chosenMethod = Method1;}
	else {CheckDlgButton(IDC_LABINTUSEMETHOD1,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2)
	{CheckDlgButton(IDC_LABINTUSEMETHOD2,1); chosenMethod = Method2;}
	else {CheckDlgButton(IDC_LABINTUSEMETHOD2,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3)
	{CheckDlgButton(IDC_LABINTUSEMETHOD3,1); chosenMethod = Method3;}
	else {CheckDlgButton(IDC_LABINTUSEMETHOD3,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4)
	{CheckDlgButton(IDC_LABINTUSEMETHOD4,1); chosenMethod = Method4;}
	else {CheckDlgButton(IDC_LABINTUSEMETHOD4,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5)
	{CheckDlgButton(IDC_LABINTUSEMETHOD5,1); chosenMethod = Method5;}
	else {CheckDlgButton(IDC_LABINTUSEMETHOD5,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6)
	{CheckDlgButton(IDC_LABINTUSEMETHOD6,1); chosenMethod = Method6;}
	else {CheckDlgButton(IDC_LABINTUSEMETHOD6,0);}

	//
	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6)
	{
		GetDlgItem(IDC_Low2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_HIGH2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_Low3)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_Low4)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_Method6_Pattern1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_Method6_Pattern2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_Method6_Pattern3)->ShowWindow(SW_SHOW);
	}
	else														//##Hide certain controls when Method 6 is not Selected
	{
		GetDlgItem(IDC_Low2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_HIGH2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_Low3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_Low4)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_Method6_Pattern1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_Method6_Pattern2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_Method6_Pattern3)->ShowWindow(SW_HIDE);
	}

	InvalidateRect(NULL,false);
	UpdateData(false);	

	pframe->m_pview->UpdateDisplay();
	pframe->m_pview->UpdateMinor();
	pframe->m_pview2->UpdateDisplay2();
	pframe->m_plimit->UpdateDisplay();
	pframe->m_pmod->UpdateDisplay();
}

void Insp::UpdateValues()
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelPatt = filterSelPatt;

	KillTimer(5);
	SetTimer(5,500,NULL);

	UpdateDisplay();
}

void Insp::SaveTemplateX(BYTE *im_sr2, BYTE *im_sr3, int ncam, int pattsel)
{
	char* pFileName = "c:\\silgandata\\pat\\";//drive
	CString c;

	c =itoa(pframe->CurrentJobNum, buf, 10);
	
	strcpy(currentpathTxt,pFileName);	strcat(currentpathTxt,c);
	strcpy(currentpathBmp,pFileName);	strcat(currentpathBmp,c);

	strcpy(currentpathTxt2,pFileName);	strcat(currentpathTxt2,c);
	strcpy(currentpathBmp2,pFileName);	strcat(currentpathBmp2,c);

	strcpy(currentpathTxt3,pFileName);	strcat(currentpathTxt3,c);
	strcpy(currentpathBmp3,pFileName);	strcat(currentpathBmp3,c);

	///replacing if statements with switch

	//if(pattsel==1)
	//{
		//strcat(currentpathTxt2,"\\imgBy6L.txt");
	//	strcat(currentpathBmp2,"\\patBy6L.bmp");
		//strcat(currentpathTxt3,"\\imgBy3L.txt");
	//	strcat(currentpathBmp3,"\\patBy3L.bmp");
	//}
	//else if(pattsel==2)
	//{
		//strcat(currentpathTxt2,"\\imgBy6M.txt");
	//	strcat(currentpathBmp2,"\\patBy6M.bmp");
		//strcat(currentpathTxt3,"\\imgBy3M.txt");
	//	strcat(currentpathBmp3,"\\patBy3M.bmp");
	//}
	//else if(pattsel==3)
	//{
		//strcat(currentpathTxt2,"\\imgBy6R.txt");
	//	strcat(currentpathBmp2,"\\patBy6R.bmp");
		//strcat(currentpathTxt3,"\\imgBy3R.txt");
	//	strcat(currentpathBmp3,"\\patBy3R.bmp");
	//}

	switch(pattsel)
	{
		case 1:
		{
			strcat(currentpathTxt2,"\\imgBy6L.txt");
			strcat(currentpathBmp2,"\\patBy6L.bmp");

			strcat(currentpathTxt3,"\\imgBy3L.txt");
			strcat(currentpathBmp3,"\\patBy3L.bmp");
			break;
		}
		case 2:
		{
			strcat(currentpathTxt2,"\\imgBy6M.txt");
			strcat(currentpathBmp2,"\\patBy6M.bmp");

			strcat(currentpathTxt3,"\\imgBy3M.txt");
			strcat(currentpathBmp3,"\\patBy3M.bmp");
			break;
		}
		case 3:
		{
			strcat(currentpathTxt2,"\\imgBy6R.txt");
			strcat(currentpathBmp2,"\\patBy6R.bmp");

			strcat(currentpathTxt3,"\\imgBy3R.txt");
			strcat(currentpathBmp3,"\\patBy3R.bmp");
			break;
		}
		default:
		{
			strcat(currentpathTxt2,"\\imgBy6.txt");
			strcat(currentpathBmp2,"\\patBy6.bmp"); //removing last letter from filename to signify error

			strcat(currentpathTxt3,"\\imgBy3.txt");
			strcat(currentpathBmp3,"\\patBy3.bmp");
		}
	}

	int szpatt12	=	theapp->sizepattBy12;
	int szpatt6		=	theapp->sizepattBy6;
	int szpatt3W	=	theapp->sizepattBy3_2W;
	int szpatt3H	=	theapp->sizepattBy3_2H;

	//////////////////

	///comment out saving as text file
	
	ImageFile.Open(currentpathTxt2, CFile::modeWrite | CFile::modeCreate);
	
	CString szDigit2;	int digit2;
	int g;
	for(g=0; g<szpatt6*szpatt6; g++)
	{
		digit2=*(im_sr2+g);
		szDigit2.Format("%3i",digit2);
		ImageFile.Write(szDigit2,szDigit2.GetLength());
	}
	
	ImageFile.Close();

	///////////////////

	CVisByteImage image2(szpatt6,szpatt6,1,-1,im_sr2);
	imagepat2=image2;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpathBmp2;
		imagepat2.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{AfxMessageBox("The pat file could not be saved.");}// Warn the user.

	///////////////////


	///comment out saving as text file
	
	
	ImageFile.Open(currentpathTxt3, CFile::modeWrite | CFile::modeCreate);
	
	CString szDigit3;	int digit3;

	for(g=0; g<szpatt3W*szpatt3H; g++)
	{
		digit3=*(im_sr3+g);
		szDigit3.Format("%3i",digit3);
		ImageFile.Write(szDigit3,szDigit3.GetLength());
	}
	
	ImageFile.Close();
	
	///////////////////

	CVisByteImage image3(szpatt3W,szpatt3H,1,-1,im_sr3);
	imagepat3=image3;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpathBmp3;
		imagepat3.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{AfxMessageBox("The pat file could not be saved.");}// Warn the user.

	///////////////////
}

void Insp::OnTeachtargpatt() 
{
	//int pattsel = Pattern1;   //Why is "pattsel" always 1?? 
	int x1,y1;
	bool nopattern=false;

	if(pframe->CurrentJobNum>=1 && pframe->CurrentJobNum<=theapp->SavedTotalJobs)
	{
		//########New way of implementing method 6
		if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 || theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2)
		{
			switch(pattSel)
			{
				case Pattern1:
				{
					x1 = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_x;
					y1 = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_y;
					break;
				}
				case Pattern2:
				{
					x1 = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_x;
					y1 = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_y;
					break;
				}
				case Pattern3:
				{
					x1 = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_x;
					y1 = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_y;
					break;
				}
				default:
				{
					nopattern=true;
				}
			}

			if(!nopattern)
			{
				///consolidated implementation
				switch(cam)
				{
					case 1:
					{
						switch(filterSelPatt)
						{
							case 1:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg1BWDiv6,	byteImg1BWDiv3,	x1, y1, cam, pattSel);
								break;
							}
							case 2:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil2Div6,	byteImg1Fil2Div3, x1, y1, cam, pattSel);
								break;
							}
							case 3:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil3Div6,	byteImg1Fil3Div3, x1, y1, cam, pattSel);
								break;
							}
							case 4:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil4Div6,	byteImg1Fil4Div3, x1, y1, cam, pattSel);
								break;
							}
							case 5:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil5Div6,	byteImg1Fil5Div3, x1, y1, cam, pattSel);
								break;
							}
							case 6:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil6Div6,	byteImg1Fil6Div3, x1, y1, cam, pattSel);
								break;
							}
							case 7:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil7Div6,	byteImg1Fil7Div3, x1, y1, cam, pattSel);
								break;
							}
							case 8:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil8Div6,	byteImg1Fil8Div3, x1, y1, cam, pattSel);
								break;
							}
							default:
							{
								//do nothing
							}
						}
						break;
					}
					case 2:
					{
						switch(filterSelPatt)
						{
							case 1:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg2BWDiv6,	byteImg2BWDiv3,	x1, y1, cam, pattSel);
								break;
							}
							case 2:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil2Div6,	byteImg2Fil2Div3, x1, y1, cam, pattSel);
								break;
							}
							case 3:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil3Div6,	byteImg2Fil3Div3, x1, y1, cam, pattSel);
								break;
							}
							case 4:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil4Div6,	byteImg2Fil4Div3, x1, y1, cam, pattSel);
								break;
							}
							case 5:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil5Div6,	byteImg2Fil5Div3, x1, y1, cam, pattSel);
								break;
							}
							case 6:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil6Div6,	byteImg2Fil6Div3, x1, y1, cam, pattSel);
								break;
							}
							case 7:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil7Div6,	byteImg2Fil7Div3, x1, y1, cam, pattSel);
								break;
							}
							case 8:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil8Div6,	byteImg2Fil8Div3, x1, y1, cam, pattSel);
								break;
							}
							default:
							{
								//do nothing
							}
						}
						break;
					}
					case 3:
					{
						switch(filterSelPatt)
						{
							case 1:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg3BWDiv6,	byteImg3BWDiv3,	x1, y1, cam, pattSel);
								break;
							}
							case 2:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil2Div6,	byteImg3Fil2Div3, x1, y1, cam, pattSel);
								break;
							}
							case 3:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil3Div6,	byteImg3Fil3Div3, x1, y1, cam, pattSel);
								break;
							}
							case 4:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil4Div6,	byteImg3Fil4Div3, x1, y1, cam, pattSel);
								break;
							}
							case 5:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil5Div6,	byteImg3Fil5Div3, x1, y1, cam, pattSel);
								break;
							}
							case 6:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil6Div6,	byteImg3Fil6Div3, x1, y1, cam, pattSel);
								break;
							}
							case 7:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil7Div6,	byteImg3Fil7Div3, x1, y1, cam, pattSel);
								break;
							}
							case 8:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil8Div6,	byteImg3Fil8Div3, x1, y1, cam, pattSel);
								break;
							}
							default:
							{
								//do nothing
							}
						}
						break;
					}
					case 4:
					{
						switch(filterSelPatt)
						{
							case 1:		
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg4BWDiv6,	byteImg4BWDiv3,	x1, y1, cam, pattSel);
								break;
							}
							case 2:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil2Div6,	byteImg4Fil2Div3, x1, y1, cam, pattSel);
								break;
							}
							case 3:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil3Div6,	byteImg4Fil3Div3, x1, y1, cam, pattSel);
								break;
							}
							case 4:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil4Div6,	byteImg4Fil4Div3, x1, y1, cam, pattSel);
								break;
							}
							case 5:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil5Div6,	byteImg4Fil5Div3, x1, y1, cam, pattSel);
								break;
							}
							case 6:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil6Div6,	byteImg4Fil6Div3, x1, y1, cam, pattSel);
								break;
							}
							case 7:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil7Div6,	byteImg4Fil7Div3, x1, y1, cam, pattSel);
								break;
							}
							case 8:	
							{
								pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil8Div6,	byteImg4Fil8Div3, x1, y1, cam, pattSel);
								break;
							}
							default:
							{
								//do nothing
							}
						}
						break;
					}
					default:
					{
						//do nothing
					}
				}

				pframe->m_pxaxis->wehaveTempX=true;
				pframe->m_pxaxis->colorReady=false;
			}
			///##################         Implies Pattern 1 was Checked   #####################
			/*
			CButton *m_ctlCheck = (CButton*) GetDlgItem(IDC_Method6_Pattern1);  
			int ChkBox = m_ctlCheck->GetCheck();
			if( ChkBox == BST_CHECKED)    
			{
				pattsel = 1;
				int x1 = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_x;
				int y1 = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt1_y;

				if(cam==1)
				{
					if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX_method6(byteImg1BWDiv6,	byteImg1BWDiv3,	x1, y1, cam, pattsel);}
					else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil2Div6,	byteImg1Fil2Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil3Div6,	byteImg1Fil3Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil4Div6,	byteImg1Fil4Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil5Div6,	byteImg1Fil5Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil6Div6,	byteImg1Fil6Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil7Div6,	byteImg1Fil7Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil8Div6,	byteImg1Fil8Div3, x1, y1, cam, pattsel);}
				}
				else if(cam==2)
				{
					if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX_method6(byteImg2BWDiv6,	byteImg2BWDiv3,	x1, y1, cam, pattsel);}
					else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil2Div6,	byteImg2Fil2Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil3Div6,	byteImg2Fil3Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil4Div6,	byteImg2Fil4Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil5Div6,	byteImg2Fil5Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil6Div6,	byteImg2Fil6Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil7Div6,	byteImg2Fil7Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil8Div6,	byteImg2Fil8Div3, x1, y1, cam, pattsel);}
				}
				else if(cam==3)
				{
					if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX_method6(byteImg3BWDiv6,	byteImg3BWDiv3,	x1, y1, cam, pattsel);}
					else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil2Div6,	byteImg3Fil2Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil3Div6,	byteImg3Fil3Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil4Div6,	byteImg3Fil4Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil5Div6,	byteImg3Fil5Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil6Div6,	byteImg3Fil6Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil7Div6,	byteImg3Fil7Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil8Div6,	byteImg3Fil8Div3, x1, y1, cam, pattsel);}
				}		
				else if(cam==4)
				{
					if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX_method6(byteImg4BWDiv6,	byteImg4BWDiv3,	x1, y1, cam, pattsel);}
					else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil2Div6,	byteImg4Fil2Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil3Div6,	byteImg4Fil3Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil4Div6,	byteImg4Fil4Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil5Div6,	byteImg4Fil5Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil6Div6,	byteImg4Fil6Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil7Div6,	byteImg4Fil7Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil8Div6,	byteImg4Fil8Div3, x1, y1, cam, pattsel);}
				}

				pframe->m_pxaxis->wehaveTempX=true;
				pframe->m_pxaxis->colorReady=false;
			}
			*/
			//UpdateValues();

			//###################     Implies Pattern 2 was Checked      #################
			/*
			CButton *m_ctlCheck2 = (CButton*) GetDlgItem(IDC_Method6_Pattern2);  
			int ChkBox2 = m_ctlCheck2->GetCheck();
			if( ChkBox2 == BST_CHECKED)
			{
				pattsel = 2;
				int x1 = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_x;
				int y1 = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt2_y;

				if(cam==1)
				{
					if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX_method6(byteImg1BWDiv6,	byteImg1BWDiv3,	x1, y1, cam, pattsel);}
					else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil2Div6,	byteImg1Fil2Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil3Div6,	byteImg1Fil3Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil4Div6,	byteImg1Fil4Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil5Div6,	byteImg1Fil5Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil6Div6,	byteImg1Fil6Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil7Div6,	byteImg1Fil7Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil8Div6,	byteImg1Fil8Div3, x1, y1, cam, pattsel);}
				}
				else if(cam==2)
				{
					if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX_method6(byteImg2BWDiv6,	byteImg2BWDiv3,	x1, y1, cam, pattsel);}
					else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil2Div6,	byteImg2Fil2Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil3Div6,	byteImg2Fil3Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil4Div6,	byteImg2Fil4Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil5Div6,	byteImg2Fil5Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil6Div6,	byteImg2Fil6Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil7Div6,	byteImg2Fil7Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil8Div6,	byteImg2Fil8Div3, x1, y1, cam, pattsel);}
				}
				else if(cam==3)
				{
					if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX_method6(byteImg3BWDiv6,	byteImg3BWDiv3,	x1, y1, cam, pattsel);}
					else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil2Div6,	byteImg3Fil2Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil3Div6,	byteImg3Fil3Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil4Div6,	byteImg3Fil4Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil5Div6,	byteImg3Fil5Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil6Div6,	byteImg3Fil6Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil7Div6,	byteImg3Fil7Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil8Div6,	byteImg3Fil8Div3, x1, y1, cam, pattsel);}
				}		
				else if(cam==4)
				{
					if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX_method6(byteImg4BWDiv6,	byteImg4BWDiv3,		x1, y1, cam, pattsel);}
					else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil2Div6,	byteImg4Fil2Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil3Div6,	byteImg4Fil3Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil4Div6,	byteImg4Fil4Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil5Div6,	byteImg4Fil5Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil6Div6,	byteImg4Fil6Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil7Div6,	byteImg4Fil7Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil8Div6,	byteImg4Fil8Div3, x1, y1, cam, pattsel);}
				}

				//Dunno about these 2 statements
				pframe->m_pxaxis->wehaveTempX=true;
				pframe->m_pxaxis->colorReady=false;
			}
			*/
			//UpdateValues();
			//###################     Implies Pattern 3 was Checked      #################
			/*
			CButton *m_ctlCheck3 = (CButton*) GetDlgItem(IDC_Method6_Pattern3);  
			int ChkBox3 = m_ctlCheck3->GetCheck();
			if( ChkBox3 == BST_CHECKED)
			{
				pattsel = 3;
				int x1 = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_x;
				int y1 = theapp->jobinfo[pframe->CurrentJobNum].meth6_patt3_y;

				if(cam==1)
				{
					if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX_method6(byteImg1BWDiv6,	byteImg1BWDiv3,	x1, y1, cam, pattsel);}
					else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil2Div6,	byteImg1Fil2Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil3Div6,	byteImg1Fil3Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil4Div6,	byteImg1Fil4Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil5Div6,	byteImg1Fil5Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil6Div6,	byteImg1Fil6Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil7Div6,	byteImg1Fil7Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX_method6(byteImg1Fil8Div6,	byteImg1Fil8Div3, x1, y1, cam, pattsel);}
				}
				else if(cam==2)
				{
					if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX_method6(byteImg2BWDiv6,	byteImg2BWDiv3,	x1, y1, cam, pattsel);}
					else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil2Div6,	byteImg2Fil2Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil3Div6,	byteImg2Fil3Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil4Div6,	byteImg2Fil4Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil5Div6,	byteImg2Fil5Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil6Div6,	byteImg2Fil6Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil7Div6,	byteImg2Fil7Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX_method6(byteImg2Fil8Div6,	byteImg2Fil8Div3, x1, y1, cam, pattsel);}
				}
				else if(cam==3)
				{
					if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX_method6(byteImg3BWDiv6,	byteImg3BWDiv3,	x1, y1, cam, pattsel);}
					else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil2Div6,	byteImg3Fil2Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil3Div6,	byteImg3Fil3Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil4Div6,	byteImg3Fil4Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil5Div6,	byteImg3Fil5Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil6Div6,	byteImg3Fil6Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil7Div6,	byteImg3Fil7Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX_method6(byteImg3Fil8Div6,	byteImg3Fil8Div3, x1, y1, cam, pattsel);}
				}		
				else if(cam==4)
				{
					if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX_method6(byteImg4BWDiv6,	byteImg4BWDiv3,	x1, y1, cam, pattsel);}
					else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil2Div6,	byteImg4Fil2Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil3Div6,	byteImg4Fil3Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil4Div6,	byteImg4Fil4Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil5Div6,	byteImg4Fil5Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil6Div6,	byteImg4Fil6Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil7Div6,	byteImg4Fil7Div3, x1, y1, cam, pattsel);}
					else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX_method6(byteImg4Fil8Div6,	byteImg4Fil8Div3, x1, y1, cam, pattsel);}
				}

				//Dunno about these 2 statements
				pframe->m_pxaxis->wehaveTempX=true;
				pframe->m_pxaxis->colorReady=false;
			}
			*/
			//UpdateValues();
		}
		else
		{
			int x1 = theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattSel];
			int y1 = theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattSel];

			if(cam==1)
			{
				if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX(byteImg1BWDiv6,	byteImg1BWDiv3,	x1, y1, cam, pattSel);}
				else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX(byteImg1Fil2Div6,	byteImg1Fil2Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX(byteImg1Fil3Div6,	byteImg1Fil3Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX(byteImg1Fil4Div6,	byteImg1Fil4Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX(byteImg1Fil5Div6,	byteImg1Fil5Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX(byteImg1Fil6Div6,	byteImg1Fil6Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX(byteImg1Fil7Div6,	byteImg1Fil7Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX(byteImg1Fil8Div6,	byteImg1Fil8Div3, x1, y1, cam, pattSel);}
			}
			else if(cam==2)
			{
				if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX(byteImg2BWDiv6,	byteImg2BWDiv3,	x1, y1, cam, pattSel);}
				else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX(byteImg2Fil2Div6,	byteImg2Fil2Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX(byteImg2Fil3Div6,	byteImg2Fil3Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX(byteImg2Fil4Div6,	byteImg2Fil4Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX(byteImg2Fil5Div6,	byteImg2Fil5Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX(byteImg2Fil6Div6,	byteImg2Fil6Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX(byteImg2Fil7Div6,	byteImg2Fil7Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX(byteImg2Fil8Div6,	byteImg2Fil8Div3, x1, y1, cam, pattSel);}
			}
			else if(cam==3)
			{
				if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX(byteImg3BWDiv6,	byteImg3BWDiv3,	x1, y1, cam, pattSel);}
				else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX(byteImg3Fil2Div6,	byteImg3Fil2Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX(byteImg3Fil3Div6,	byteImg3Fil3Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX(byteImg3Fil4Div6,	byteImg3Fil4Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX(byteImg3Fil5Div6,	byteImg3Fil5Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX(byteImg3Fil6Div6,	byteImg3Fil6Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX(byteImg3Fil7Div6,	byteImg3Fil7Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX(byteImg3Fil8Div6,	byteImg3Fil8Div3, x1, y1, cam, pattSel);}
			}		
			else if(cam==4)
			{
				if(filterSelPatt==1)		{pframe->m_pxaxis->LearnPattX(byteImg4BWDiv6,	byteImg4BWDiv3,	x1, y1, cam, pattSel);}
				else if(filterSelPatt==2)	{pframe->m_pxaxis->LearnPattX(byteImg4Fil2Div6,	byteImg4Fil2Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==3)	{pframe->m_pxaxis->LearnPattX(byteImg4Fil3Div6,	byteImg4Fil3Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==4)	{pframe->m_pxaxis->LearnPattX(byteImg4Fil4Div6,	byteImg4Fil4Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==5)	{pframe->m_pxaxis->LearnPattX(byteImg4Fil5Div6,	byteImg4Fil5Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==6)	{pframe->m_pxaxis->LearnPattX(byteImg4Fil6Div6,	byteImg4Fil6Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==7)	{pframe->m_pxaxis->LearnPattX(byteImg4Fil7Div6,	byteImg4Fil7Div3, x1, y1, cam, pattSel);}
				else if(filterSelPatt==8)	{pframe->m_pxaxis->LearnPattX(byteImg4Fil8Div6,	byteImg4Fil8Div3, x1, y1, cam, pattSel);}
			}

			pframe->m_pxaxis->wehaveTempX=true;
			pframe->m_pxaxis->colorReady=false;
		}
	}

	theapp->jobinfo[pframe->CurrentJobNum].taught=true;

	UpdateValues();
}

void Insp::OnFilter1() 
{filterSelPatt = 1;	UpdateValues();}	

void Insp::OnFilter2() 
{filterSelPatt = 2;	UpdateValues();}

void Insp::OnFilter3() 
{filterSelPatt = 3;	UpdateValues();}

void Insp::OnFilter4() 
{filterSelPatt = 4;	UpdateValues();}

void Insp::OnFilter5() 
{filterSelPatt = 5;	UpdateValues();}

//void Insp::OnFilter6() 
//{filterSelPatt = 6;	UpdateValues();}

void Insp::OnTargpattup2() 
{
	int pattCurr = 2;

	theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattCurr]-=resRes6;

	if(	theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattCurr]<12)
	{theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattCurr] = 12;}

	UpdateValues();		
}

void Insp::OnTargpattdw2() 
{
	int pattCurr = 2;
	
	theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattCurr]+=resRes6;

	if(	theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattCurr]+12>HEIGHT)
	{theapp->jobinfo[pframe->CurrentJobNum].tgYpatt[pattCurr]-=resRes6;}

	UpdateValues();		
}

void Insp::OnTargpattright2() 
{
	int pattCurr = 2;

	theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattCurr]+=resRes6;

	if(	theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattCurr]+12>WIDTH)
	{theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattCurr] -= resRes6;}

	UpdateValues();			
}

void Insp::OnTargpattleft2() 
{
	int pattCurr = 2;

	theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattCurr]-=resRes6;

	if(	theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattCurr]<12)
	{theapp->jobinfo[pframe->CurrentJobNum].tgXpatt[pattCurr] = 12;}

	UpdateValues();		
}

void Insp::OnFilter6() 
{
	filterSelPatt = 6;	UpdateValues();	
}

void Insp::OnSensmet3up() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sensMet3+=2;
	if(theapp->jobinfo[pframe->CurrentJobNum].sensMet3>=250)
	{theapp->jobinfo[pframe->CurrentJobNum].sensMet3=250;}

	m_sensMet3.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].sensMet3);
	SetTimer(1,3000,NULL);
	UpdateData(false);	
}

void Insp::OnSensmet3dw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sensMet3-=2;

	if(theapp->jobinfo[pframe->CurrentJobNum].sensMet3<10)
	{theapp->jobinfo[pframe->CurrentJobNum].sensMet3=10;}

	m_sensMet3.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].sensMet3);
	SetTimer(1,3000,NULL);
	UpdateData(false);		
}

void Insp::OnToplimpattup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt<5)
	{theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt=5;}

	m_topLimPatt.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt);
	//pframe->m_pxaxis->InvalidateRect(NULL,false);
	SetTimer(1,3000,NULL);
	UpdateData(false);	
	InvalidateRect(NULL,false);
}

void Insp::OnToplimpattdw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt>=200)
	{theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt=200;}

	m_topLimPatt.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt);
	//pframe->m_pxaxis->InvalidateRect(NULL,false);
	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
}

void Insp::OnBotlimpattup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt<5)
	{theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt=5;}

	m_botLimPatt.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt);
	//pframe->m_pxaxis->InvalidateRect(NULL,false);
	SetTimer(1,3000,NULL);
	UpdateData(false);	
	InvalidateRect(NULL,false);
}

void Insp::OnBotlimpattdw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt>=320)
	{theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt=320;}

	m_botLimPatt.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt);
	//pframe->m_pxaxis->InvalidateRect(NULL,false);
	SetTimer(1,3000,NULL);
	UpdateData(false);	
	InvalidateRect(NULL,false);
}

void Insp::OnFilter7() 
{
	filterSelPatt = 7;	UpdateValues();	
}

void Insp::OnFilter8() 
{
	filterSelPatt = 8;	UpdateValues();	
}

void Insp::OnLabintusemethod1() 
{
	chosenMethod = Method1;
	CheckDlgButton(IDC_LABINTUSEMETHOD1,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod1 = true);
	CheckDlgButton(IDC_LABINTUSEMETHOD2,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD3,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD4,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD5,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD6,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 = false);

	pattSel = Pattern1;
	CheckDlgButton(IDC_Method6_Pattern1, 1);
	CheckDlgButton(IDC_Method6_Pattern2, 0);
	CheckDlgButton(IDC_Method6_Pattern3, 0);
	UpdateValues();			
}

void Insp::OnLabintusemethod2() 
{
	chosenMethod = Method2;
	CheckDlgButton(IDC_LABINTUSEMETHOD1,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod1 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD2,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2 = true);
	CheckDlgButton(IDC_LABINTUSEMETHOD3,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD4,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD5,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD6,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 = false);

	pattSel = Pattern1;
	CheckDlgButton(IDC_Method6_Pattern1, 1);
	CheckDlgButton(IDC_Method6_Pattern2, 0);
	CheckDlgButton(IDC_Method6_Pattern3, 0);
	UpdateValues();			
}

void Insp::OnLabintusemethod3() 
{
	chosenMethod = Method3;
	CheckDlgButton(IDC_LABINTUSEMETHOD1,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod1 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD2,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD3,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3 = true);
	CheckDlgButton(IDC_LABINTUSEMETHOD4,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD5,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD6,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 = false);

	pattSel = Pattern1;
	CheckDlgButton(IDC_Method6_Pattern1, 1);
	CheckDlgButton(IDC_Method6_Pattern2, 0);
	CheckDlgButton(IDC_Method6_Pattern3, 0);
	UpdateValues();			
}

void Insp::OnLabintusemethod4() 
{
	chosenMethod = Method4;
	CheckDlgButton(IDC_LABINTUSEMETHOD1,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod1 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD2,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD3,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD4,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4 = true);
	CheckDlgButton(IDC_LABINTUSEMETHOD5,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD6,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 = false);

	pattSel = Pattern1;
	CheckDlgButton(IDC_Method6_Pattern1, 1);
	CheckDlgButton(IDC_Method6_Pattern2, 0);
	CheckDlgButton(IDC_Method6_Pattern3, 0);
	UpdateValues();
}

void Insp::OnLabintusemethod5() 
{
	chosenMethod = Method5;
	CheckDlgButton(IDC_LABINTUSEMETHOD1,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod1 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD2,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD3,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD4,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD5,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5 = true);
	CheckDlgButton(IDC_LABINTUSEMETHOD6,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 = false);

	pattSel = Pattern1;
	CheckDlgButton(IDC_Method6_Pattern1, 1);
	CheckDlgButton(IDC_Method6_Pattern2, 0);
	CheckDlgButton(IDC_Method6_Pattern3, 0);
	UpdateValues();	
}

void Insp::OnLabintusemethod6()   //new method added for Sunsweet 
{
	chosenMethod = Method6;
	CheckDlgButton(IDC_LABINTUSEMETHOD1,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod1 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD2,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD3,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD4,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD5,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5 = false);
	CheckDlgButton(IDC_LABINTUSEMETHOD6,theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 = true);

	pattSel = NoPattern;
	CheckDlgButton(IDC_Method6_Pattern1, 0);
	CheckDlgButton(IDC_Method6_Pattern2, 0);
	CheckDlgButton(IDC_Method6_Pattern3, 0);
	UpdateValues();	
}

void Insp::setAllCheckBoxesToFalse()
{
	CheckDlgButton(IDC_CHK1,0);
	CheckDlgButton(IDC_CHK2,0);
	CheckDlgButton(IDC_CHK3,0);	
	CheckDlgButton(IDC_CHK4,0);
	CheckDlgButton(IDC_CHK5,0);	
	CheckDlgButton(IDC_CHK6,0);
	CheckDlgButton(IDC_CHK7,0);
	CheckDlgButton(IDC_CHK8,0);
	CheckDlgButton(IDC_CHK9,0);
	CheckDlgButton(IDC_CHK10,0);
	CheckDlgButton(IDC_CHK11,0);

	//RadioButtons	
	CheckDlgButton(IDC_SELBODY1,0);
	CheckDlgButton(IDC_SELBODY2,0);
	CheckDlgButton(IDC_SELBODY3,0);
}

void Insp::OnLighttodark() 
{	
	theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition = 0;
	UpdateValues();
}

void Insp::OnDarktolight() 
{
	theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition = 1;	
	UpdateValues();
}

void Insp::OnSensedgeup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sensEdge+=2;
	if(theapp->jobinfo[pframe->CurrentJobNum].sensEdge>=250)
	{theapp->jobinfo[pframe->CurrentJobNum].sensEdge=250;}

	UpdateValues();
}

void Insp::OnSensedgedw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sensEdge-=2;

	if(theapp->jobinfo[pframe->CurrentJobNum].sensEdge<10)
	{theapp->jobinfo[pframe->CurrentJobNum].sensEdge=10;}

	UpdateValues();
}

BOOL Insp::DestroyWindow() 
{
	bool debugging=false;

	if(debugging)	{pframe->m_pxaxis->SaveBWCustom1(pframe->m_pxaxis->bytePattBy6[1],	sizepattBy6, sizepattBy6);}
	if(debugging)	{pframe->m_pxaxis->SaveBWCustom1(pframe->m_pxaxis->bytePattBy3[1],	sizepattBy3W,sizepattBy3W);}

//	free(bytePattBy6[1]);
//	free(bytePattBy6[2]);
//	free(bytePattBy6[3]);

//	free(bytePattBy3[1]);
//	free(bytePattBy3[2]);
//	free(bytePattBy3[3]);

	if(debugging)	{pframe->m_pxaxis->SaveBWCustom1(pframe->m_pxaxis->bytePattBy6[1],	sizepattBy6, sizepattBy6);}
	if(debugging)	{pframe->m_pxaxis->SaveBWCustom1(pframe->m_pxaxis->bytePattBy3[1],	sizepattBy3W,sizepattBy3W);}

		
	free(byteImgCoDiv3[1]);
	free(byteImgCoDiv3[2]);
	free(byteImgCoDiv3[3]);
	free(byteImgCoDiv3[4]);
	
//	free(static_byteImg1BWDiv3);

	free(byteImg1BWDiv3);
	free(byteImg2BWDiv3);
	free(byteImg3BWDiv3);
	free(byteImg4BWDiv3);
		
	free(byteImg1BWDiv6);
	free(byteImg2BWDiv6);
	free(byteImg3BWDiv6);
	free(byteImg4BWDiv6);

	free(byteImg1Fil2Div3);
	free(byteImg2Fil2Div3);
	free(byteImg3Fil2Div3);
	free(byteImg4Fil2Div3);

	free(byteImg1Fil2Div6);
	free(byteImg2Fil2Div6);
	free(byteImg3Fil2Div6);
	free(byteImg4Fil2Div6);

	free(byteImg1Fil3Div3);
	free(byteImg2Fil3Div3);
	free(byteImg3Fil3Div3);
	free(byteImg4Fil3Div3);

	free(byteImg1Fil3Div6);
	free(byteImg2Fil3Div6);
	free(byteImg3Fil3Div6);
	free(byteImg4Fil3Div6);

	free(byteImg1Fil4Div3);
	free(byteImg2Fil4Div3);
	free(byteImg3Fil4Div3);
	free(byteImg4Fil4Div3);

	free(byteImg1Fil4Div6);
	free(byteImg2Fil4Div6);
	free(byteImg3Fil4Div6);
	free(byteImg4Fil4Div6);

	free(byteImg1Fil5Div3);
	free(byteImg2Fil5Div3);
	free(byteImg3Fil5Div3);
	free(byteImg4Fil5Div3);

	free(byteImg1Fil5Div6);
	free(byteImg2Fil5Div6);
	free(byteImg3Fil5Div6);
	free(byteImg4Fil5Div6);

	free(byteImg1Fil6Div3);
	free(byteImg2Fil6Div3);
	free(byteImg3Fil6Div3);
	free(byteImg4Fil6Div3);

	free(byteImg1Fil6Div6);
	free(byteImg2Fil6Div6);
	free(byteImg3Fil6Div6);
	free(byteImg4Fil6Div6);

	free(byteImg1Fil7Div3);
	free(byteImg2Fil7Div3);
	free(byteImg3Fil7Div3);
	free(byteImg4Fil7Div3);

	free(byteImg1Fil7Div6);
	free(byteImg2Fil7Div6);
	free(byteImg3Fil7Div6);
	free(byteImg4Fil7Div6);

	free(byteImg1Fil8Div3);
	free(byteImg2Fil8Div3);
	free(byteImg3Fil8Div3);
	free(byteImg4Fil8Div3);

	free(byteImg1Fil8Div6);
	free(byteImg2Fil8Div6);
	free(byteImg3Fil8Div6);
	free(byteImg4Fil8Div6);

////////////////

	
	return CDialog::DestroyWindow();
}

void Insp::setAllMethodsToFalse()
{
	theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod1 = false;
	theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2 = false;
	theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3 = false;
	theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4 = false;
	theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5 = false;
	theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 = false;
}


void Insp::OnSelbody1() 
{
	setAllCheckBoxesToFalse();
	theapp->jobinfo[pframe->CurrentJobNum].bottleStyle=1;

	UpdateValues();		
}

void Insp::OnSelbody2() 
{
	setAllCheckBoxesToFalse();
	theapp->jobinfo[pframe->CurrentJobNum].bottleStyle=10;

	UpdateValues();			
}

void Insp::OnSelbody3() 
{
	setAllCheckBoxesToFalse();

	theapp->jobinfo[pframe->CurrentJobNum].bottleStyle=3;

	UpdateValues();		
}

void Insp::OnSelcap0() 
{
	setAllCapCheckBoxesToFalse();
	theapp->jobinfo[pframe->CurrentJobNum].sport = 0;
	UpdateValues();		
}

void Insp::OnSelcap1() 
{
	setAllCapCheckBoxesToFalse();
	theapp->jobinfo[pframe->CurrentJobNum].sport = 1;
	UpdateValues();			
}

void Insp::setAllCapCheckBoxesToFalse()
{
	//RadioButtons for Cap
	CheckDlgButton(IDC_SELCAP0,0);
	CheckDlgButton(IDC_SELCAP1,0);
}

void Insp::OnMethod6Pattern1() 
{
	CButton *m_ctlCheck = (CButton*) GetDlgItem(IDC_Method6_Pattern1);  ///For Pattern 1
	int P1Checked = m_ctlCheck->GetCheck();

	if(P1Checked)
	{
		pattSel = Pattern1;
		CheckDlgButton(IDC_Method6_Pattern2, 0);
		CheckDlgButton(IDC_Method6_Pattern3, 0);
	}
	else
	{
		pattSel = NoPattern;
	}
	
	UpdateValues();
}

void Insp::OnMethod6Pattern2() 
{
	CButton *m_ctlCheck = (CButton*) GetDlgItem(IDC_Method6_Pattern2);  ///For Pattern 2
	int P2Checked = m_ctlCheck->GetCheck();

	if(P2Checked)
	{
		pattSel = Pattern2;
		CheckDlgButton(IDC_Method6_Pattern1, 0);
		CheckDlgButton(IDC_Method6_Pattern3, 0);
	}
	else
	{
		pattSel = NoPattern;
	}
	
	UpdateValues();
}

void Insp::OnMethod6Pattern3() 
{
	CButton *m_ctlCheck = (CButton*) GetDlgItem(IDC_Method6_Pattern3);  ///For Pattern 3
	int P3Checked = m_ctlCheck->GetCheck();

	if(P3Checked)
	{
		pattSel = Pattern3;
		CheckDlgButton(IDC_Method6_Pattern1, 0);
		CheckDlgButton(IDC_Method6_Pattern2, 0);
	}
	else
	{
		pattSel = NoPattern;
	}
	
	UpdateValues();
}

void Insp::LoadCameraImages()
{

	int nCam;

	nCam = 1;//Color
	CVisRGBAByteImage	imgco1div3(widthBy3, heightBy3, 1, -1, byteImgCoDiv3[nCam]);
	imgco1div3.SetRect(0, 0, widthBy3, heightBy3);	imageCoDiv3[nCam] = imgco1div3; 
	CVisRGBAByteImage	imgcoC1div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_reduce1);
	imgcoC1div3.SetRect(0, 0, widthBy3, heightBy3);	imgcoC1div3.CopyPixelsTo(imageCoDiv3[nCam]);
	byteImgCoDiv3[nCam] = (unsigned char*)imageCoDiv3[nCam].PixelAddress(0,0,0);

	nCam = 2;//Color	Div3
	CVisRGBAByteImage	imgco2div3(widthBy3, heightBy3, 1, -1, byteImgCoDiv3[nCam]);
	imgco2div3.SetRect(0, 0, widthBy3, heightBy3);	imageCoDiv3[nCam] = imgco2div3; 
	CVisRGBAByteImage	imgcoC2div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_reduce2);
	imgcoC2div3.SetRect(0, 0, widthBy3, heightBy3);	imgcoC2div3.CopyPixelsTo(imageCoDiv3[nCam]);
	byteImgCoDiv3[nCam] = (unsigned char*)imageCoDiv3[nCam].PixelAddress(0,0,0);

	nCam = 3;//Color	Div3
	CVisRGBAByteImage	imgco3div3(widthBy3, heightBy3, 1, -1, byteImgCoDiv3[nCam]);
	imgco3div3.SetRect(0, 0, widthBy3, heightBy3);	imageCoDiv3[nCam] = imgco3div3; 
	CVisRGBAByteImage	imgcoC3div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_reduce3);
	imgcoC3div3.SetRect(0, 0, widthBy3, heightBy3);	imgcoC3div3.CopyPixelsTo(imageCoDiv3[nCam]);
	byteImgCoDiv3[nCam] = (unsigned char*)imageCoDiv3[nCam].PixelAddress(0,0,0);

	nCam = 4;//Color	Div3
	CVisRGBAByteImage	imgco4div3(widthBy3, heightBy3, 1, -1, byteImgCoDiv3[nCam]);
	imgco4div3.SetRect(0, 0, widthBy3, heightBy3);	imageCoDiv3[nCam] = imgco4div3; 
	CVisRGBAByteImage	imgcoC4div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_reduce4);
	imgcoC4div3.SetRect(0, 0, widthBy3, heightBy3);	imgcoC4div3.CopyPixelsTo(imageCoDiv3[nCam]);
	byteImgCoDiv3[nCam] = (unsigned char*)imageCoDiv3[nCam].PixelAddress(0,0,0);

	//////////////////////////////

	//BW	Div3 Filter 2
	//i = 1;

//	byteImg1BWDiv3 = static_byteImg1BWDiv3;

	//BW	Div3 Filter 1
	CVisByteImage		img1bw1div3(widthBy3, heightBy3, 1, -1, byteImg1BWDiv3);
	img1bw1div3.SetRect(0, 0, widthBy3, heightBy3);	image1BWDiv3 = img1bw1div3; 
	CVisByteImage		img1bwC1div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cambw1);
	img1bwC1div3.SetRect(0, 0, widthBy3, heightBy3);	img1bwC1div3.CopyPixelsTo(image1BWDiv3);
	byteImg1BWDiv3 = (unsigned char*)image1BWDiv3.PixelAddress(0, 0, 0);
		
	//BW	Div6
	CVisByteImage		img1bw1div6(widthBy6, heightBy6, 1, -1, byteImg1BWDiv6);
	img1bw1div6.SetRect(0, 0, widthBy6, heightBy6);	image1BWDiv6 = img1bw1div6; 
	CVisByteImage		img1bwC1div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam1bwDiv6);
	img1bwC1div6.SetRect(0, 0, widthBy6, heightBy6);	img1bwC1div6.CopyPixelsTo(image1BWDiv6);
	byteImg1BWDiv6 = (unsigned char*)image1BWDiv6.PixelAddress(0, 0, 0);

	CVisByteImage		img1fil2div3(widthBy3, heightBy3, 1, -1, byteImg1Fil2Div3);
	img1fil2div3.SetRect(0, 0, widthBy3, heightBy3);	image1Fil2Div3 = img1fil2div3; 
	CVisByteImage		img1filter2div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img1SatDiv3);
	img1filter2div3.SetRect(0, 0, widthBy3, heightBy3);	img1filter2div3.CopyPixelsTo(image1Fil2Div3);
	byteImg1Fil2Div3 = (unsigned char*)image1Fil2Div3.PixelAddress(0,0,0);

	CVisByteImage		img1fil2div6(widthBy6, heightBy6, 1, -1, byteImg1Fil2Div6);
	img1fil2div6.SetRect(0, 0, widthBy6, heightBy6);	image1Fil2Div6 = img1fil2div6; 
	CVisByteImage		img1filter2div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img1SatDiv6);
	img1filter2div6.SetRect(0, 0, widthBy6, heightBy6);	img1filter2div6.CopyPixelsTo(image1Fil2Div6);
	byteImg1Fil2Div6 = (unsigned char*)image1Fil2Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 3
	CVisByteImage		img1fil3div3(widthBy3, heightBy3, 1, -1, byteImg1Fil3Div3);
	img1fil3div3.SetRect(0, 0, widthBy3, heightBy3);	image1Fil3Div3 = img1fil3div3; 
	CVisByteImage		img1filter3div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img1MagDiv3);
	img1filter3div3.SetRect(0, 0, widthBy3, heightBy3);	img1filter3div3.CopyPixelsTo(image1Fil3Div3);
	byteImg1Fil3Div3 = (unsigned char*)image1Fil3Div3.PixelAddress(0,0,0);

	CVisByteImage		img1fil3div6(widthBy6, heightBy6, 1, -1, byteImg1Fil3Div6);
	img1fil3div6.SetRect(0, 0, widthBy6, heightBy6);	image1Fil3Div6 = img1fil3div6; 
	CVisByteImage		img1filter3div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img1MagDiv6);
	img1filter3div6.SetRect(0, 0, widthBy6, heightBy6);	img1filter3div6.CopyPixelsTo(image1Fil3Div6);
	byteImg1Fil3Div6 = (unsigned char*)image1Fil3Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 4
	CVisByteImage		img1fil4div3(widthBy3, heightBy3, 1, -1, byteImg1Fil4Div3);
	img1fil4div3.SetRect(0, 0, widthBy3, heightBy3);	image1Fil4Div3 = img1fil4div3; 
	CVisByteImage		img1filter4div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img1YelDiv3);
	img1filter4div3.SetRect(0, 0, widthBy3, heightBy3);	img1filter4div3.CopyPixelsTo(image1Fil4Div3);
	byteImg1Fil4Div3 = (unsigned char*)image1Fil4Div3.PixelAddress(0,0,0);

	CVisByteImage		img1fil4div6(widthBy6, heightBy6, 1, -1, byteImg1Fil4Div6);
	img1fil4div6.SetRect(0, 0, widthBy6, heightBy6);	image1Fil4Div6 = img1fil4div6; 
	CVisByteImage		img1filter4div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img1YelDiv6);
	img1filter4div6.SetRect(0, 0, widthBy6, heightBy6);	img1filter4div6.CopyPixelsTo(image1Fil4Div6);
	byteImg1Fil4Div6 = (unsigned char*)image1Fil4Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 5
	CVisByteImage		img1fil5div3(widthBy3, heightBy3, 1, -1, byteImg1Fil5Div3);
	img1fil5div3.SetRect(0, 0, widthBy3, heightBy3);	image1Fil5Div3 = img1fil5div3; 
	CVisByteImage		img1filter5div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img1CyaDiv3);
	img1filter5div3.SetRect(0, 0, widthBy3, heightBy3);	img1filter5div3.CopyPixelsTo(image1Fil5Div3);
	byteImg1Fil5Div3 = (unsigned char*)image1Fil5Div3.PixelAddress(0,0,0);

	CVisByteImage		img1fil5div6(widthBy6, heightBy6, 1, -1, byteImg1Fil5Div6);
	img1fil5div6.SetRect(0, 0, widthBy6, heightBy6);	image1Fil5Div6 = img1fil5div6; 
	CVisByteImage		img1filter5div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img1CyaDiv6);
	img1filter5div6.SetRect(0, 0, widthBy6, heightBy6);	img1filter5div6.CopyPixelsTo(image1Fil5Div6);
	byteImg1Fil5Div6 = (unsigned char*)image1Fil5Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 6
	CVisByteImage		img1fil6div3(widthBy3, heightBy3, 1, -1, byteImg1Fil6Div3);
	img1fil6div3.SetRect(0, 0, widthBy3, heightBy3);	image1Fil6Div3 = img1fil6div3; 
	CVisByteImage		img1filter6div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam1Rbw);
	img1filter6div3.SetRect(0, 0, widthBy3, heightBy3);	img1filter6div3.CopyPixelsTo(image1Fil6Div3);
	byteImg1Fil6Div3 = (unsigned char*)image1Fil6Div3.PixelAddress(0,0,0);

	CVisByteImage		img1fil6div6(widthBy6, heightBy6, 1, -1, byteImg1Fil6Div6);
	img1fil6div6.SetRect(0, 0, widthBy6, heightBy6);	image1Fil6Div6 = img1fil6div6; 
	CVisByteImage		img1filter6div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam1RbwDiv6);
	img1filter6div6.SetRect(0, 0, widthBy6, heightBy6);	img1filter6div6.CopyPixelsTo(image1Fil6Div6);
	byteImg1Fil6Div6 = (unsigned char*)image1Fil6Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 7
	CVisByteImage		img1fil7div3(widthBy3, heightBy3, 1, -1, byteImg1Fil7Div3);
	img1fil7div3.SetRect(0, 0, widthBy3, heightBy3);	image1Fil7Div3 = img1fil7div3; 
	CVisByteImage		img1filter7div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam1Gbw);
	img1filter7div3.SetRect(0, 0, widthBy3, heightBy3);	img1filter7div3.CopyPixelsTo(image1Fil7Div3);
	byteImg1Fil7Div3 = (unsigned char*)image1Fil7Div3.PixelAddress(0,0,0);

	CVisByteImage		img1fil7div6(widthBy6, heightBy6, 1, -1, byteImg1Fil7Div6);
	img1fil7div6.SetRect(0, 0, widthBy6, heightBy6);	image1Fil7Div6 = img1fil7div6; 
	CVisByteImage		img1filter7div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam1GbwDiv6);
	img1filter7div6.SetRect(0, 0, widthBy6, heightBy6);	img1filter7div6.CopyPixelsTo(image1Fil7Div6);
	byteImg1Fil7Div6 = (unsigned char*)image1Fil7Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 8
	CVisByteImage		img1fil8div3(widthBy3, heightBy3, 1, -1, byteImg1Fil8Div3);
	img1fil8div3.SetRect(0, 0, widthBy3, heightBy3);	image1Fil8Div3 = img1fil8div3; 
	CVisByteImage		img1filter8div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam1Bbw);
	img1filter8div3.SetRect(0, 0, widthBy3, heightBy3);	img1filter8div3.CopyPixelsTo(image1Fil8Div3);
	byteImg1Fil8Div3 = (unsigned char*)image1Fil8Div3.PixelAddress(0,0,0);

	CVisByteImage		img1fil8div6(widthBy6, heightBy6, 1, -1, byteImg1Fil8Div6);
	img1fil8div6.SetRect(0, 0, widthBy6, heightBy6);	image1Fil8Div6 = img1fil8div6; 
	CVisByteImage		img1filter8div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam1BbwDiv6);
	img1filter8div6.SetRect(0, 0, widthBy6, heightBy6);	img1filter8div6.CopyPixelsTo(image1Fil8Div6);
	byteImg1Fil8Div6 = (unsigned char*)image1Fil8Div6.PixelAddress(0,0,0);

	//i = 2;

	//BW	Div3 Filter 1
	CVisByteImage		img2bw1div3(widthBy3, heightBy3, 1, -1, byteImg2BWDiv3);
	img2bw1div3.SetRect(0, 0, widthBy3, heightBy3);	image2BWDiv3 = img2bw1div3; 
	CVisByteImage		img2bwC1div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cambw2);
	img2bwC1div3.SetRect(0, 0, widthBy3, heightBy3);	img2bwC1div3.CopyPixelsTo(image2BWDiv3);
	byteImg2BWDiv3 = (unsigned char*)image2BWDiv3.PixelAddress(0,0,0);
		
	//BW	Div6
	CVisByteImage		img2bw1div6(widthBy6, heightBy6, 1, -1, byteImg2BWDiv6);
	img2bw1div6.SetRect(0, 0, widthBy6, heightBy6);	image2BWDiv6 = img2bw1div6; 
	CVisByteImage		img2bwC1div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam2bwDiv6);
	img2bwC1div6.SetRect(0, 0, widthBy6, heightBy6);	img2bwC1div6.CopyPixelsTo(image2BWDiv6);
	byteImg2BWDiv6 = (unsigned char*)image2BWDiv6.PixelAddress(0,0,0);

	CVisByteImage		img2fil2div3(widthBy3, heightBy3, 1, -1, byteImg2Fil2Div3);
	img2fil2div3.SetRect(0, 0, widthBy3, heightBy3);	image2Fil2Div3 = img2fil2div3; 
	CVisByteImage		img2filter2div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img2SatDiv3);
	img2filter2div3.SetRect(0, 0, widthBy3, heightBy3);	img2filter2div3.CopyPixelsTo(image2Fil2Div3);
	byteImg2Fil2Div3 = (unsigned char*)image2Fil2Div3.PixelAddress(0,0,0);

	CVisByteImage		img2fil2div6(widthBy6, heightBy6, 1, -1, byteImg2Fil2Div6);
	img2fil2div6.SetRect(0, 0, widthBy6, heightBy6);	image2Fil2Div6 = img2fil2div6; 
	CVisByteImage		img2filter2div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img2SatDiv6);
	img2filter2div6.SetRect(0, 0, widthBy6, heightBy6);	img2filter2div6.CopyPixelsTo(image2Fil2Div6);
	byteImg2Fil2Div6 = (unsigned char*)image2Fil2Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 3
	CVisByteImage		img2fil3div3(widthBy3, heightBy3, 1, -1, byteImg2Fil3Div3);
	img2fil3div3.SetRect(0, 0, widthBy3, heightBy3);	image2Fil3Div3 = img2fil3div3; 
	CVisByteImage		img2filter3div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img2MagDiv3);
	img2filter3div3.SetRect(0, 0, widthBy3, heightBy3);	img2filter3div3.CopyPixelsTo(image2Fil3Div3);
	byteImg2Fil3Div3 = (unsigned char*)image2Fil3Div3.PixelAddress(0,0,0);

	CVisByteImage		img2fil3div6(widthBy6, heightBy6, 1, -1, byteImg2Fil3Div6);
	img2fil3div6.SetRect(0, 0, widthBy6, heightBy6);	image2Fil3Div6 = img2fil3div6; 
	CVisByteImage		img2filter3div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img2MagDiv6);
	img2filter3div6.SetRect(0, 0, widthBy6, heightBy6);	img2filter3div6.CopyPixelsTo(image2Fil3Div6);
	byteImg2Fil3Div6 = (unsigned char*)image2Fil3Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 4
	CVisByteImage		img2fil4div3(widthBy3, heightBy3, 1, -1, byteImg2Fil4Div3);
	img2fil4div3.SetRect(0, 0, widthBy3, heightBy3);	image2Fil4Div3 = img2fil4div3; 
	CVisByteImage		img2filter4div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img2YelDiv3);
	img2filter4div3.SetRect(0, 0, widthBy3, heightBy3);	img2filter4div3.CopyPixelsTo(image2Fil4Div3);
	byteImg2Fil4Div3 = (unsigned char*)image2Fil4Div3.PixelAddress(0,0,0);

	CVisByteImage		img2fil4div6(widthBy6, heightBy6, 1, -1, byteImg2Fil4Div6);
	img2fil4div6.SetRect(0, 0, widthBy6, heightBy6);	image2Fil4Div6 = img2fil4div6; 
	CVisByteImage		img2filter4div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img2YelDiv6);
	img2filter4div6.SetRect(0, 0, widthBy6, heightBy6);	img2filter4div6.CopyPixelsTo(image2Fil4Div6);
	byteImg2Fil4Div6 = (unsigned char*)image2Fil4Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 5
	CVisByteImage		img2fil5div3(widthBy3, heightBy3, 1, -1, byteImg2Fil5Div3);
	img2fil5div3.SetRect(0, 0, widthBy3, heightBy3);	image2Fil5Div3 = img2fil5div3; 
	CVisByteImage		img2filter5div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img2CyaDiv3);
	img2filter5div3.SetRect(0, 0, widthBy3, heightBy3);	img2filter5div3.CopyPixelsTo(image2Fil5Div3);
	byteImg2Fil5Div3 = (unsigned char*)image2Fil5Div3.PixelAddress(0,0,0);

	CVisByteImage		img2fil5div6(widthBy6, heightBy6, 1, -1, byteImg2Fil5Div6);
	img2fil5div6.SetRect(0, 0, widthBy6, heightBy6);	image2Fil5Div6 = img2fil5div6; 
	CVisByteImage		img2filter5div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img2CyaDiv6);
	img2filter5div6.SetRect(0, 0, widthBy6, heightBy6);	img2filter5div6.CopyPixelsTo(image2Fil5Div6);
	byteImg2Fil5Div6 = (unsigned char*)image2Fil5Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 6
	CVisByteImage		img2fil6div3(widthBy3, heightBy3, 1, -1, byteImg2Fil6Div3);
	img2fil6div3.SetRect(0, 0, widthBy3, heightBy3);	image2Fil6Div3 = img2fil6div3; 
	CVisByteImage		img2filter6div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam2Rbw);
	img2filter6div3.SetRect(0, 0, widthBy3, heightBy3);	img2filter6div3.CopyPixelsTo(image2Fil6Div3);
	byteImg2Fil6Div3 = (unsigned char*)image2Fil6Div3.PixelAddress(0,0,0);

	CVisByteImage		img2fil6div6(widthBy6, heightBy6, 1, -1, byteImg2Fil6Div6);
	img2fil6div6.SetRect(0, 0, widthBy6, heightBy6);	image2Fil6Div6 = img2fil6div6; 
	CVisByteImage		img2filter6div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam2RbwDiv6);
	img2filter6div6.SetRect(0, 0, widthBy6, heightBy6);	img2filter6div6.CopyPixelsTo(image2Fil6Div6);
	byteImg2Fil6Div6 = (unsigned char*)image2Fil6Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 7
	CVisByteImage		img2fil7div3(widthBy3, heightBy3, 1, -1, byteImg2Fil7Div3);
	img2fil7div3.SetRect(0, 0, widthBy3, heightBy3);	image2Fil7Div3 = img2fil7div3; 
	CVisByteImage		img2filter7div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam2Gbw);
	img2filter7div3.SetRect(0, 0, widthBy3, heightBy3);	img2filter7div3.CopyPixelsTo(image2Fil7Div3);
	byteImg2Fil7Div3 = (unsigned char*)image2Fil7Div3.PixelAddress(0,0,0);

	CVisByteImage		img2fil7div6(widthBy6, heightBy6, 1, -1, byteImg2Fil7Div6);
	img2fil7div6.SetRect(0, 0, widthBy6, heightBy6);	image2Fil7Div6 = img2fil7div6; 
	CVisByteImage		img2filter7div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam2GbwDiv6);
	img2filter7div6.SetRect(0, 0, widthBy6, heightBy6);	img2filter7div6.CopyPixelsTo(image2Fil7Div6);
	byteImg2Fil7Div6 = (unsigned char*)image2Fil7Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 8
	CVisByteImage		img2fil8div3(widthBy3, heightBy3, 1, -1, byteImg2Fil8Div3);
	img2fil8div3.SetRect(0, 0, widthBy3, heightBy3);	image2Fil8Div3 = img2fil8div3; 
	CVisByteImage		img2filter8div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam2Bbw);
	img2filter8div3.SetRect(0, 0, widthBy3, heightBy3);	img2filter8div3.CopyPixelsTo(image2Fil8Div3);
	byteImg2Fil8Div3 = (unsigned char*)image2Fil8Div3.PixelAddress(0,0,0);

	CVisByteImage		img2fil8div6(widthBy6, heightBy6, 1, -1, byteImg2Fil8Div6);
	img2fil8div6.SetRect(0, 0, widthBy6, heightBy6);	image2Fil8Div6 = img2fil8div6; 
	CVisByteImage		img2filter8div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam2BbwDiv6);
	img2filter8div6.SetRect(0, 0, widthBy6, heightBy6);	img2filter8div6.CopyPixelsTo(image2Fil8Div6);
	byteImg2Fil8Div6 = (unsigned char*)image2Fil8Div6.PixelAddress(0,0,0);

	//i = 3;

	//BW	Div3 Filter 1
	CVisByteImage		img3bw1div3(widthBy3, heightBy3, 1, -1, byteImg3BWDiv3);
	img3bw1div3.SetRect(0, 0, widthBy3, heightBy3);	image3BWDiv3 = img3bw1div3; 
	CVisByteImage		img3bwC1div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cambw3);
	img3bwC1div3.SetRect(0, 0, widthBy3, heightBy3);	img3bwC1div3.CopyPixelsTo(image3BWDiv3);
	byteImg3BWDiv3 = (unsigned char*)image3BWDiv3.PixelAddress(0,0,0);
		
	//BW	Div6
	CVisByteImage		img3bw1div6(widthBy6, heightBy6, 1, -1, byteImg3BWDiv6);
	img3bw1div6.SetRect(0, 0, widthBy6, heightBy6);	image3BWDiv6 = img3bw1div6; 
	CVisByteImage		img3bwC1div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam3bwDiv6);
	img3bwC1div6.SetRect(0, 0, widthBy6, heightBy6);	img3bwC1div6.CopyPixelsTo(image3BWDiv6);
	byteImg3BWDiv6 = (unsigned char*)image3BWDiv6.PixelAddress(0,0,0);
	
	CVisByteImage		img3fil2div3(widthBy3, heightBy3, 1, -1, byteImg3Fil2Div3);
	img3fil2div3.SetRect(0, 0, widthBy3, heightBy3);	image3Fil2Div3 = img3fil2div3; 
	CVisByteImage		img3filter2div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img3SatDiv3);
	img3filter2div3.SetRect(0, 0, widthBy3, heightBy3);	img3filter2div3.CopyPixelsTo(image3Fil2Div3);
	byteImg3Fil2Div3 = (unsigned char*)image3Fil2Div3.PixelAddress(0,0,0);

	CVisByteImage		img3fil2div6(widthBy6, heightBy6, 1, -1, byteImg3Fil2Div6);
	img3fil2div6.SetRect(0, 0, widthBy6, heightBy6);	image3Fil2Div6 = img3fil2div6; 
	CVisByteImage		img3filter2div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img3SatDiv6);
	img3filter2div6.SetRect(0, 0, widthBy6, heightBy6);	img3filter2div6.CopyPixelsTo(image3Fil2Div6);
	byteImg3Fil2Div6 = (unsigned char*)image3Fil2Div6.PixelAddress(0,0,0);

	CVisByteImage		img3fil3div3(widthBy3, heightBy3, 1, -1, byteImg3Fil3Div3);
	img3fil3div3.SetRect(0, 0, widthBy3, heightBy3);	image3Fil3Div3 = img3fil3div3; 
	CVisByteImage		img3filter3div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img3MagDiv3);
	img3filter3div3.SetRect(0, 0, widthBy3, heightBy3);	img3filter3div3.CopyPixelsTo(image3Fil3Div3);
	byteImg3Fil3Div3 = (unsigned char*)image3Fil3Div3.PixelAddress(0,0,0);

	CVisByteImage		img3fil3div6(widthBy6, heightBy6, 1, -1, byteImg3Fil3Div6);
	img3fil3div6.SetRect(0, 0, widthBy6, heightBy6);	image3Fil3Div6 = img3fil3div6; 
	CVisByteImage		img3filter3div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img3MagDiv6);
	img3filter3div6.SetRect(0, 0, widthBy6, heightBy6);	img3filter3div6.CopyPixelsTo(image3Fil3Div6);
	byteImg3Fil3Div6 = (unsigned char*)image3Fil3Div6.PixelAddress(0,0,0);
	
	//BW	Div3 Filter 4
	CVisByteImage		img3fil4div3(widthBy3, heightBy3, 1, -1, byteImg3Fil4Div3);
	img3fil4div3.SetRect(0, 0, widthBy3, heightBy3);	image3Fil4Div3 = img3fil4div3; 
	CVisByteImage		img3filter4div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img3YelDiv3);
	img3filter4div3.SetRect(0, 0, widthBy3, heightBy3);	img3filter4div3.CopyPixelsTo(image3Fil4Div3);
	byteImg3Fil4Div3 = (unsigned char*)image3Fil4Div3.PixelAddress(0,0,0);

	CVisByteImage		img3fil4div6(widthBy6, heightBy6, 1, -1, byteImg3Fil4Div6);
	img3fil4div6.SetRect(0, 0, widthBy6, heightBy6);	image3Fil4Div6 = img3fil4div6; 
	CVisByteImage		img3filter4div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img3YelDiv6);
	img3filter4div6.SetRect(0, 0, widthBy6, heightBy6);	img3filter4div6.CopyPixelsTo(image3Fil4Div6);
	byteImg3Fil4Div6 = (unsigned char*)image3Fil4Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 5
	CVisByteImage		img3fil5div3(widthBy3, heightBy3, 1, -1, byteImg3Fil5Div3);
	img3fil5div3.SetRect(0, 0, widthBy3, heightBy3);	image3Fil5Div3 = img3fil5div3; 
	CVisByteImage		img3filter5div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img3CyaDiv3);
	img3filter5div3.SetRect(0, 0, widthBy3, heightBy3);	img3filter5div3.CopyPixelsTo(image3Fil5Div3);
	byteImg3Fil5Div3 = (unsigned char*)image3Fil5Div3.PixelAddress(0,0,0);

	CVisByteImage		img3fil5div6(widthBy6, heightBy6, 1, -1, byteImg3Fil5Div6);
	img3fil5div6.SetRect(0, 0, widthBy6, heightBy6);	image3Fil5Div6 = img3fil5div6; 
	CVisByteImage		img3filter5div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img3CyaDiv6);
	img3filter5div6.SetRect(0, 0, widthBy6, heightBy6);	img3filter5div6.CopyPixelsTo(image3Fil5Div6);
	byteImg3Fil5Div6 = (unsigned char*)image3Fil5Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 6
	CVisByteImage		img3fil6div3(widthBy3, heightBy3, 1, -1, byteImg3Fil6Div3);
	img3fil6div3.SetRect(0, 0, widthBy3, heightBy3);	image3Fil6Div3 = img3fil6div3; 
	CVisByteImage		img3filter6div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam3Rbw);
	img3filter6div3.SetRect(0, 0, widthBy3, heightBy3);	img3filter6div3.CopyPixelsTo(image3Fil6Div3);
	byteImg3Fil6Div3 = (unsigned char*)image3Fil6Div3.PixelAddress(0,0,0);

	CVisByteImage		img3fil6div6(widthBy6, heightBy6, 1, -1, byteImg3Fil6Div6);
	img3fil6div6.SetRect(0, 0, widthBy6, heightBy6);	image3Fil6Div6 = img3fil6div6; 
	CVisByteImage		img3filter6div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam3RbwDiv6);
	img3filter6div6.SetRect(0, 0, widthBy6, heightBy6);	img3filter6div6.CopyPixelsTo(image3Fil6Div6);
	byteImg3Fil6Div6 = (unsigned char*)image3Fil6Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 7
	CVisByteImage		img3fil7div3(widthBy3, heightBy3, 1, -1, byteImg3Fil7Div3);
	img3fil7div3.SetRect(0, 0, widthBy3, heightBy3);	image3Fil7Div3 = img3fil7div3; 
	CVisByteImage		img3filter7div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam3Gbw);
	img3filter7div3.SetRect(0, 0, widthBy3, heightBy3);	img3filter7div3.CopyPixelsTo(image3Fil7Div3);
	byteImg3Fil7Div3 = (unsigned char*)image3Fil7Div3.PixelAddress(0,0,0);

	CVisByteImage		img3fil7div6(widthBy6, heightBy6, 1, -1, byteImg3Fil7Div6);
	img3fil7div6.SetRect(0, 0, widthBy6, heightBy6);	image3Fil7Div6 = img3fil7div6; 
	CVisByteImage		img3filter7div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam3GbwDiv6);
	img3filter7div6.SetRect(0, 0, widthBy6, heightBy6);	img3filter7div6.CopyPixelsTo(image3Fil7Div6);
	byteImg3Fil7Div6 = (unsigned char*)image3Fil7Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 8
	CVisByteImage		img3fil8div3(widthBy3, heightBy3, 1, -1, byteImg3Fil8Div3);
	img3fil8div3.SetRect(0, 0, widthBy3, heightBy3);	image3Fil8Div3 = img3fil8div3; 
	CVisByteImage		img3filter8div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam3Bbw);
	img3filter8div3.SetRect(0, 0, widthBy3, heightBy3);	img3filter8div3.CopyPixelsTo(image3Fil8Div3);
	byteImg3Fil8Div3 = (unsigned char*)image3Fil8Div3.PixelAddress(0,0,0);

	CVisByteImage		img3fil8div6(widthBy6, heightBy6, 1, -1, byteImg3Fil8Div6);
	img3fil8div6.SetRect(0, 0, widthBy6, heightBy6);	image3Fil8Div6 = img3fil8div6; 
	CVisByteImage		img3filter8div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam3BbwDiv6);
	img3filter8div6.SetRect(0, 0, widthBy6, heightBy6);	img3filter8div6.CopyPixelsTo(image3Fil8Div6);
	byteImg3Fil8Div6 = (unsigned char*)image3Fil8Div6.PixelAddress(0,0,0);

	//i = 4;

	//BW	Div3 Filter 1
	CVisByteImage		img4bw1div3(widthBy3, heightBy3, 1, -1, byteImg4BWDiv3);
	img4bw1div3.SetRect(0, 0, widthBy3, heightBy3);	image4BWDiv3 = img4bw1div3; 
	CVisByteImage		img4bwC1div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cambw4);
	img4bwC1div3.SetRect(0, 0, widthBy3, heightBy3);	img4bwC1div3.CopyPixelsTo(image4BWDiv3);
	byteImg4BWDiv3 = (unsigned char*)image4BWDiv3.PixelAddress(0,0,0);
		
	//BW	Div6
	CVisByteImage		img4bw1div6(widthBy6, heightBy6, 1, -1, byteImg4BWDiv6);
	img4bw1div6.SetRect(0, 0, widthBy6, heightBy6);	image4BWDiv6 = img4bw1div6; 
	CVisByteImage		img4bwC1div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam4bwDiv6);
	img4bwC1div6.SetRect(0, 0, widthBy6, heightBy6);	img4bwC1div6.CopyPixelsTo(image4BWDiv6);
	byteImg4BWDiv6 = (unsigned char*)image4BWDiv6.PixelAddress(0,0,0);

	CVisByteImage		img4fil2div3(widthBy3, heightBy3, 1, -1, byteImg4Fil2Div3);
	img4fil2div3.SetRect(0, 0, widthBy3, heightBy3);	image4Fil2Div3 = img4fil2div3; 
	CVisByteImage		img4filter2div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img4SatDiv3);
	img4filter2div3.SetRect(0, 0, widthBy3, heightBy3);	img4filter2div3.CopyPixelsTo(image4Fil2Div3);
	byteImg4Fil2Div3 = (unsigned char*)image4Fil2Div3.PixelAddress(0,0,0);

	CVisByteImage		img4fil2div6(widthBy6, heightBy6, 1, -1, byteImg4Fil2Div6);
	img4fil2div6.SetRect(0, 0, widthBy6, heightBy6);	image4Fil2Div6 = img4fil2div6; 
	CVisByteImage		img4filter2div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img4SatDiv6);
	img4filter2div6.SetRect(0, 0, widthBy6, heightBy6);	img4filter2div6.CopyPixelsTo(image4Fil2Div6);
	byteImg4Fil2Div6 = (unsigned char*)image4Fil2Div6.PixelAddress(0,0,0);

	CVisByteImage		img4fil3div3(widthBy3, heightBy3, 1, -1, byteImg4Fil3Div3);
	img4fil3div3.SetRect(0, 0, widthBy3, heightBy3);	image4Fil3Div3 = img4fil3div3; 
	CVisByteImage		img4filter3div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img4MagDiv3);
	img4filter3div3.SetRect(0, 0, widthBy3, heightBy3);	img4filter3div3.CopyPixelsTo(image4Fil3Div3);
	byteImg4Fil3Div3 = (unsigned char*)image4Fil3Div3.PixelAddress(0,0,0);

	CVisByteImage		img4fil3div6(widthBy6, heightBy6, 1, -1, byteImg4Fil3Div6);
	img4fil3div6.SetRect(0, 0, widthBy6, heightBy6);	image4Fil3Div6 = img4fil3div6; 
	CVisByteImage		img4filter3div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img4MagDiv6);
	img4filter3div6.SetRect(0, 0, widthBy6, heightBy6);	img4filter3div6.CopyPixelsTo(image4Fil3Div6);
	byteImg4Fil3Div6 = (unsigned char*)image4Fil3Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 4
	CVisByteImage		img4fil4div3(widthBy3, heightBy3, 1, -1, byteImg4Fil4Div3);
	img4fil4div3.SetRect(0, 0, widthBy3, heightBy3);	image4Fil4Div3 = img4fil4div3; 
	CVisByteImage		img4filter4div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img4YelDiv3);
	img4filter4div3.SetRect(0, 0, widthBy3, heightBy3);	img4filter4div3.CopyPixelsTo(image4Fil4Div3);
	byteImg4Fil4Div3 = (unsigned char*)image4Fil4Div3.PixelAddress(0,0,0);

	CVisByteImage		img4fil4div6(widthBy6, heightBy6, 1, -1, byteImg4Fil4Div6);
	img4fil4div6.SetRect(0, 0, widthBy6, heightBy6);	image4Fil4Div6 = img4fil4div6; 
	CVisByteImage		img4filter4div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img4YelDiv6);
	img4filter4div6.SetRect(0, 0, widthBy6, heightBy6);	img4filter4div6.CopyPixelsTo(image4Fil4Div6);
	byteImg4Fil4Div6 = (unsigned char*)image4Fil4Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 5
	CVisByteImage		img4fil5div3(widthBy3, heightBy3, 1, -1, byteImg4Fil5Div3);
	img4fil5div3.SetRect(0, 0, widthBy3, heightBy3);	image4Fil5Div3 = img4fil5div3; 
	CVisByteImage		img4filter5div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->img4CyaDiv3);
	img4filter5div3.SetRect(0, 0, widthBy3, heightBy3);	img4filter5div3.CopyPixelsTo(image4Fil5Div3);
	byteImg4Fil5Div3 = (unsigned char*)image4Fil5Div3.PixelAddress(0,0,0);

	CVisByteImage		img4fil5div6(widthBy6, heightBy6, 1, -1, byteImg4Fil5Div6);
	img4fil5div6.SetRect(0, 0, widthBy6, heightBy6);	image4Fil5Div6 = img4fil5div6; 
	CVisByteImage		img4filter5div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->img4CyaDiv6);
	img4filter5div6.SetRect(0, 0, widthBy6, heightBy6);	img4filter5div6.CopyPixelsTo(image4Fil5Div6);
	byteImg4Fil5Div6 = (unsigned char*)image4Fil5Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 6
	CVisByteImage		img4fil6div3(widthBy3, heightBy3, 1, -1, byteImg4Fil6Div3);
	img4fil6div3.SetRect(0, 0, widthBy3, heightBy3);	image4Fil6Div3 = img4fil6div3; 
	CVisByteImage		img4filter6div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam4Rbw);
	img4filter6div3.SetRect(0, 0, widthBy3, heightBy3);	img4filter6div3.CopyPixelsTo(image4Fil6Div3);
	byteImg4Fil6Div3 = (unsigned char*)image4Fil6Div3.PixelAddress(0,0,0);

	CVisByteImage		img4fil6div6(widthBy6, heightBy6, 1, -1, byteImg4Fil6Div6);
	img4fil6div6.SetRect(0, 0, widthBy6, heightBy6);	image4Fil6Div6 = img4fil6div6; 
	CVisByteImage		img4filter6div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam4RbwDiv6);
	img4filter6div6.SetRect(0, 0, widthBy6, heightBy6);	img4filter6div6.CopyPixelsTo(image4Fil6Div6);
	byteImg4Fil6Div6 = (unsigned char*)image4Fil6Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 7
	CVisByteImage		img4fil7div3(widthBy3, heightBy3, 1, -1, byteImg4Fil7Div3);
	img4fil7div3.SetRect(0, 0, widthBy3, heightBy3);	image4Fil7Div3 = img4fil7div3; 
	CVisByteImage		img4filter7div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam4Gbw);
	img4filter7div3.SetRect(0, 0, widthBy3, heightBy3);	img4filter7div3.CopyPixelsTo(image4Fil7Div3);
	byteImg4Fil7Div3 = (unsigned char*)image4Fil7Div3.PixelAddress(0,0,0);

	CVisByteImage		img4fil7div6(widthBy6, heightBy6, 1, -1, byteImg4Fil7Div6);
	img4fil7div6.SetRect(0, 0, widthBy6, heightBy6);	image4Fil7Div6 = img4fil7div6; 
	CVisByteImage		img4filter7div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam4GbwDiv6);
	img4filter7div6.SetRect(0, 0, widthBy6, heightBy6);	img4filter7div6.CopyPixelsTo(image4Fil7Div6);
	byteImg4Fil7Div6 = (unsigned char*)image4Fil7Div6.PixelAddress(0,0,0);

	//BW	Div3 Filter 8
	CVisByteImage		img4fil8div3(widthBy3, heightBy3, 1, -1, byteImg4Fil8Div3);
	img4fil8div3.SetRect(0, 0, widthBy3, heightBy3);	image4Fil8Div3 = img4fil8div3; 
	CVisByteImage		img4filter8div3(widthBy3, heightBy3, 1, -1, pframe->m_pxaxis->im_cam4Bbw);
	img4filter8div3.SetRect(0, 0, widthBy3, heightBy3);	img4filter8div3.CopyPixelsTo(image4Fil8Div3);
	byteImg4Fil8Div3 = (unsigned char*)image4Fil8Div3.PixelAddress(0,0,0);

	CVisByteImage		img4fil8div6(widthBy6, heightBy6, 1, -1, byteImg4Fil8Div6);
	img4fil8div6.SetRect(0, 0, widthBy6, heightBy6);	image4Fil8Div6 = img4fil8div6; 
	CVisByteImage		img4filter8div6(widthBy6, heightBy6, 1, -1, pframe->m_pxaxis->im_cam4BbwDiv6);
	img4filter8div6.SetRect(0, 0, widthBy6, heightBy6);	img4filter8div6.CopyPixelsTo(image4Fil8Div6);
	byteImg4Fil8Div6 = (unsigned char*)image4Fil8Div6.PixelAddress(0,0,0);
}
