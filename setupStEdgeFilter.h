#if !defined(AFX_SETUPSTEDGEFILTER_H__5C5373B0_72EA_4636_93B4_F73188CA1EEA__INCLUDED_)
#define AFX_SETUPSTEDGEFILTER_H__5C5373B0_72EA_4636_93B4_F73188CA1EEA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// setupStEdgeFilter.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// setupStEdgeFilter dialog

class setupStEdgeFilter : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
	// Construction
public:
	void UpdateDisplay();
	void UpdateValues();
	setupStEdgeFilter(CWnd* pParent = NULL);   // standard constructor
	CBottleApp *theapp;
	CMainFrame *pframe;
	int resSen;
	bool stepSlow;

// Dialog Data
	//{{AFX_DATA(setupStEdgeFilter)
	enum { IDD = IDD_STEDGEFILTER };
	CButton	m_step;
	CString	m_senFilterEdge;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(setupStEdgeFilter)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(setupStEdgeFilter)
	virtual void OnOK();
	afx_msg void OnUseimgst1();
	afx_msg void OnUseimgst2();
	afx_msg void OnUseimgst3();
	afx_msg void OnUseimgst4();
	afx_msg void OnUseimgst5();
	afx_msg void OnUseimgst6();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnStep();
	afx_msg void OnSenedgeup();
	afx_msg void OnSenedgedw();
	afx_msg void OnUseimgst7();
	afx_msg void OnUseimgst8();
	afx_msg void OnUseimgst9();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETUPSTEDGEFILTER_H__5C5373B0_72EA_4636_93B4_F73188CA1EEA__INCLUDED_)
