// NoLabelSetup.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "NoLabelSetup.h"

#include "mainfrm.h"
#include "xaxisview.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// NoLabelSetup dialog


NoLabelSetup::NoLabelSetup(CWnd* pParent /*=NULL*/)
	: CDialog(NoLabelSetup::IDD, pParent)
{
	//{{AFX_DATA_INIT(NoLabelSetup)
	m_editnloffset = _T("");
	m_labelSen = _T("");
	m_editnloffsetX1 = _T("");
	m_editnloffsetX2 = _T("");
	m_editnloffsetX3 = _T("");
	m_editnloffsetX4 = _T("");
	m_editnloffsetW = _T("");
	m_editnloffsetH = _T("");
	//}}AFX_DATA_INIT
}


void NoLabelSetup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(NoLabelSetup)
	DDX_Control(pDX, IDC_FINEMOD, m_fine);
	DDX_Text(pDX, IDC_EDITNLOFFSET, m_editnloffset);
	DDX_Text(pDX, IDC_NOLABELSEN, m_labelSen);
	DDX_Text(pDX, IDC_OFFSETX1, m_editnloffsetX1);
	DDX_Text(pDX, IDC_OFFSETX2, m_editnloffsetX2);
	DDX_Text(pDX, IDC_OFFSETX3, m_editnloffsetX3);
	DDX_Text(pDX, IDC_OFFSETX4, m_editnloffsetX4);
	DDX_Text(pDX, IDC_WIDTH, m_editnloffsetW);
	DDX_Text(pDX, IDC_HEIGHT, m_editnloffsetH);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(NoLabelSetup, CDialog)
	//{{AFX_MSG_MAP(NoLabelSetup)
	ON_BN_CLICKED(IDC_FINEMOD, OnFinemod)
	ON_BN_CLICKED(IDC_NLUP, OnNlup)
	ON_BN_CLICKED(IDC_NLDN, OnNldn)
	ON_BN_CLICKED(IDC_NOLABELSENUP, OnNolabelsenup)
	ON_BN_CLICKED(IDC_NOLABELSENDW, OnNolabelsendw)
	ON_BN_CLICKED(IDC_USEIMG1, OnUseimg1)
	ON_BN_CLICKED(IDC_USEIMG2, OnUseimg2)
	ON_BN_CLICKED(IDC_USEIMG3, OnUseimg3)
	ON_BN_CLICKED(IDC_USEIMG4, OnUseimg4)
	ON_BN_CLICKED(IDC_USEIMG5, OnUseimg5)
	ON_BN_CLICKED(IDC_USEIMG6, OnUseimg6)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_OFFSETXUP1, OnOffsetxup1)
	ON_BN_CLICKED(IDC_OFFSETXDW1, OnOffsetxdw1)
	ON_BN_CLICKED(IDC_OFFSETXUP2, OnOffsetxup2)
	ON_BN_CLICKED(IDC_OFFSETXUP3, OnOffsetxup3)
	ON_BN_CLICKED(IDC_OFFSETXUP4, OnOffsetxup4)
	ON_BN_CLICKED(IDC_OFFSETXDW2, OnOffsetxdw2)
	ON_BN_CLICKED(IDC_OFFSETXDW3, OnOffsetxdw3)
	ON_BN_CLICKED(IDC_OFFSETXDW4, OnOffsetxdw4)
	ON_BN_CLICKED(IDC_WIDTHUP, OnWidthup)
	ON_BN_CLICKED(IDC_WIDTHDOWN, OnWidthdown)
	ON_BN_CLICKED(IDC_HEIGHTUP, OnHeightup)
	ON_BN_CLICKED(IDC_HEIGHTDOWN, OnHeightdown)
	ON_BN_CLICKED(IDC_USEIMG7, OnUseimg7)
	ON_BN_CLICKED(IDC_USEIMG8, OnUseimg8)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// NoLabelSetup message handlers

void NoLabelSetup::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

BOOL NoLabelSetup::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pNoLabelSetup=this;
	theapp=(CBottleApp*)AfxGetApp();
	
	WidthCam = theapp->camHeight[1];
	HeightCam= theapp->camWidth[1];

	m_fine.SetWindowText("Slow"); 
	res=1;
	resOff = 3;

	UpdateDisplay();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void NoLabelSetup::OnFinemod() 
{
	if(res==1)
	{m_fine.SetWindowText("Fast"); CheckDlgButton(IDC_FINEMOD,1); res=10;	resOff=12;}
	else
	{m_fine.SetWindowText("Slow"); CheckDlgButton(IDC_FINEMOD,0); res=1;	resOff=3;}
	
	UpdateData(false);		
}

void NoLabelSetup::UpdateDisplay()
{
	m_editnloffset.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset);

	m_editnloffsetX1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[1]);
	m_editnloffsetX2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[2]);
	m_editnloffsetX3.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[3]);
	m_editnloffsetX4.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[4]);

	m_editnloffsetW.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetW);
	m_editnloffsetH.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetH);

	m_labelSen.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].noLabelSen);

	int filterSelNolabel = theapp->jobinfo[pframe->CurrentJobNum].filterSelNolabel;

	if(filterSelNolabel==1)	{CheckDlgButton(IDC_USEIMG1,1);}else{CheckDlgButton(IDC_USEIMG1,0);}
	if(filterSelNolabel==2)	{CheckDlgButton(IDC_USEIMG2,1);}else{CheckDlgButton(IDC_USEIMG2,0);}
	if(filterSelNolabel==3)	{CheckDlgButton(IDC_USEIMG3,1);}else{CheckDlgButton(IDC_USEIMG3,0);}
	if(filterSelNolabel==4)	{CheckDlgButton(IDC_USEIMG4,1);}else{CheckDlgButton(IDC_USEIMG4,0);}
	if(filterSelNolabel==5)	{CheckDlgButton(IDC_USEIMG5,1);}else{CheckDlgButton(IDC_USEIMG5,0);}
	if(filterSelNolabel==6)	{CheckDlgButton(IDC_USEIMG6,1);}else{CheckDlgButton(IDC_USEIMG6,0);}
	if(filterSelNolabel==7)	{CheckDlgButton(IDC_USEIMG7,1);}else{CheckDlgButton(IDC_USEIMG7,0);}
	if(filterSelNolabel==8)	{CheckDlgButton(IDC_USEIMG8,1);}else{CheckDlgButton(IDC_USEIMG8,0);}

	UpdateData(false);	
}

void NoLabelSetup::OnNlup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset-=res;
	
	UpdateValues();		
	
}

void NoLabelSetup::OnNldn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset+=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset<=0) 
	{theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset=0;}
	
	UpdateValues();		

}

void NoLabelSetup::OnNolabelsenup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].noLabelSen+=res;
	UpdateValues();		
	
}

void NoLabelSetup::OnNolabelsendw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].noLabelSen-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].noLabelSen<=0)
	{theapp->jobinfo[pframe->CurrentJobNum].noLabelSen=0;}
	
	UpdateValues();		
		
}

void NoLabelSetup::OnUseimg1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelNolabel = 1;
	UpdateValues();		
}

void NoLabelSetup::OnUseimg2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelNolabel = 2;
	UpdateValues();		
}

void NoLabelSetup::OnUseimg3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelNolabel = 3;
	UpdateValues();		
}

void NoLabelSetup::OnUseimg4() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelNolabel = 4;
	UpdateValues();		
}

void NoLabelSetup::OnUseimg5() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelNolabel = 5;
	UpdateValues();		
}

void NoLabelSetup::OnUseimg6() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelNolabel = 6;
	UpdateValues();		
}

void NoLabelSetup::UpdateValues()
{
	SetTimer(1,500,NULL);
	UpdateDisplay();

	pframe->m_pxaxis->calcNoLabelCoordinates();
	pframe->m_pxaxis->InvalidateRect(NULL,false);
}

void NoLabelSetup::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{KillTimer(1); pframe->SaveJob(pframe->CurrentJobNum);}

	CDialog::OnTimer(nIDEvent);
}

void NoLabelSetup::OnOffsetxup1() 
{
	int cam = 1;
	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[cam]+=resOff;
	
	UpdateValues();		
}

void NoLabelSetup::OnOffsetxup2() 
{
	int cam = 2;
	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[cam]+=resOff;
	
	UpdateValues();		
}

void NoLabelSetup::OnOffsetxup3() 
{
	int cam = 3;
	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[cam]+=resOff;
	
	UpdateValues();		
}

void NoLabelSetup::OnOffsetxup4() 
{
	int cam = 4;
	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[cam]+=resOff;
	
	UpdateValues();		
}

void NoLabelSetup::OnOffsetxdw1() 
{
	int cam = 1;
	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[cam]-=resOff;
	
	UpdateValues();		
}

void NoLabelSetup::OnOffsetxdw2() 
{
	int cam = 2;
	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[cam]-=resOff;
	
	UpdateValues();		
}

void NoLabelSetup::OnOffsetxdw3() 
{
	int cam = 3;
	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[cam]-=resOff;
	
	UpdateValues();		
}

void NoLabelSetup::OnOffsetxdw4() 
{
	int cam = 4;
	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[cam]-=resOff;
	
	UpdateValues();		
}

void NoLabelSetup::OnWidthup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetW+=resOff;

	if(	(theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetW+
		theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[1])>WidthCam ||
		(theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetW+
		theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[2])>WidthCam ||
		(theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetW+
		theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[3])>WidthCam ||
		(theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetW+
		theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[4])>WidthCam	)
	{theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetW-=resOff;}
	
	UpdateValues();			
}

void NoLabelSetup::OnWidthdown() 
{
	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetW-=resOff;

	if(theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetW<30)
	{theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetW=30;}	
	
	UpdateValues();			
}

void NoLabelSetup::OnHeightup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetH+=resOff;

	if(	(theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetH+
		theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset)> HeightCam)
	{theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetH-=resOff;}
	
	UpdateValues();				
}

void NoLabelSetup::OnHeightdown() 
{
	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetH-=resOff;

	if(	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetH< 45)
	{theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetH=15;}
	
	UpdateValues();					
}

void NoLabelSetup::OnUseimg7() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelNolabel = 7;
	UpdateValues();		
}

void NoLabelSetup::OnUseimg8() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelNolabel = 8;
	UpdateValues();		
}
