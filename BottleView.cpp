// BottleView.cpp : implementation of the CBottleView class
//

#include "stdafx.h"
#include "Bottle.h"

#include "BottleDoc.h"
#include "BottleView.h"
#include "mainfrm.h"

#include "xaxisview.h"
#include "View.h"
#include "serial.h"
#include "camera.h"
#include "prompt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBottleView

IMPLEMENT_DYNCREATE(CBottleView, CFormView)

BEGIN_MESSAGE_MAP(CBottleView, CFormView)
	//{{AFX_MSG_MAP(CBottleView)
	ON_BN_CLICKED(IDC_CONT, OnCont)
	ON_BN_CLICKED(IDC_TRIG1, OnTrig1)
	ON_BN_CLICKED(IDC_RTOT, OnRtot)
	ON_BN_CLICKED(IDC_REJECTALL, OnRejectall)
	ON_BN_CLICKED(IDC_FREEZE, OnFreeze)
	ON_BN_CLICKED(IDC_FREEZE2, OnFreeze2)
	ON_BN_CLICKED(IDC_ONLINE, OnOnline)
	ON_BN_CLICKED(IDC_OFFLINE, OnOffline)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_TESTEJECT, OnTesteject)
	ON_BN_CLICKED(IDC_MORE3, OnMore3)
	ON_BN_CLICKED(IDC_LESS3, OnLess3)
	ON_BN_CLICKED(IDC_CAMBUTTON, OnCambutton)
	ON_BN_CLICKED(IDC_SHOWJOB, OnShowjob)
	ON_BN_CLICKED(IDC_RESETCAMS, OnResetcams)
	ON_BN_CLICKED(IDC_MORE5, OnMore5)
	ON_BN_CLICKED(IDC_LESS4, OnLess4)
	ON_BN_CLICKED(IDC_TEST24, OnTest24)
	ON_BN_CLICKED(IDC_NOMOT, OnNomot)
	ON_BN_CLICKED(IDC_MORE6, OnMore6)
	ON_BN_CLICKED(IDC_LESS5, OnLess5)
	ON_BN_CLICKED(IDC_EJECT2, OnEject2)
	ON_BN_CLICKED(IDC_ENABLESEC, OnEnablesec)
	ON_BN_CLICKED(IDC_PASSNOTFINISHED, OnPassnotfinished)
	ON_BN_CLICKED(IDC_TEMP_PLC, OnTempPlc)
	ON_BN_CLICKED(IDC_TEMP_USB, OnTempUsb)
	ON_BN_CLICKED(IDC_TEMP_NONE, OnTempNone)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBottleView construction/destruction

CBottleView::CBottleView()
	: CFormView(CBottleView::IDD)
{
	//{{AFX_DATA_INIT(CBottleView)
		m_total = _T("");
	m_rejects = _T("");
	m_status = _T("");
	m_result2 = _T("");
	m_result3 = _T("");
	m_result4 = _T("");
	m_result5 = _T("");
	m_result6 = _T("");
	m_result7 = _T("");
	m_result8 = _T("");
	m_time = _T("");
	m_h1 = _T("");
	m_h2 = _T("");
	m_h3 = _T("");
	m_h4 = _T("");
	m_h5 = _T("");
	m_h6 = _T("");
	m_h7 = _T("");
	m_h8 = _T("");
	m_insp1 = _T("");
	m_insp2 = _T("");
	m_insp3 = _T("");
	m_insp4 = _T("");
	m_insp5 = _T("");
	m_insp6 = _T("");
	m_insp7 = _T("");
	m_insp8 = _T("");
	m_l1 = _T("");
	m_l2 = _T("");
	m_l3 = _T("");
	m_l4 = _T("");
	m_l5 = _T("");
	m_l6 = _T("");
	m_l7 = _T("");
	m_l8 = _T("");
	m_editcam1 = _T("");
	m_editcam2 = _T("");
	m_editcam3 = _T("");
	m_editcam4 = _T("");
	m_editcam5 = _T("");
	m_time1 = _T("");
	m_time2 = _T("");
	m_time3 = _T("");
	m_time4 = _T("");
	m_time5 = _T("");
	m_shutter = _T("");
	m_result1r = _T("");
	m_inspdelay = _T("");
	m_result1l = _T("");
	m_b1 = _T("");
	m_b2 = _T("");
	m_b3 = _T("");
	m_b4 = _T("");
	m_h9 = _T("");
	m_insp9 = _T("");
	m_result9 = _T("");
	m_l9 = _T("");
	m_shutter2 = _T("");
	m_label = _T("");
	m_bottle = _T("");
	m_camrestart = _T("");
	m_result10 = _T("");
	m_l10 = _T("");
	m_h10 = _T("");
	m_insp10 = _T("");
	m_result11 = _T("");
	m_result12 = _T("");
	m_l11 = _T("");
	m_h11 = _T("");
	m_insp11 = _T("");
	m_deltaC1 = _T("");
	m_deltaC2 = _T("");
	m_deltaC3 = _T("");
	m_deltaC4 = _T("");
	//}}AFX_DATA_INIT
	// TODO: add construction code here
}

CBottleView::~CBottleView()
{
}

void CBottleView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBottleView)
	DDX_Control(pDX, IDC_RES12, m_bxRes12);
	DDX_Control(pDX, IDC_RES11, m_bxRes11);
	DDX_Control(pDX, IDC_RES10, m_bxRes10);
	DDX_Control(pDX, IDC_RES8, m_bxRes8);
	DDX_Control(pDX, IDC_RESBX, m_resultBx);
	DDX_Control(pDX, IDC_RES9, m_bxRes9);
	DDX_Control(pDX, IDC_RES7, m_bxRes7);
	DDX_Control(pDX, IDC_RES6, m_bxRes6);
	DDX_Control(pDX, IDC_RES5, m_bxRes5);
	DDX_Control(pDX, IDC_RES4, m_bxRes4);
	DDX_Control(pDX, IDC_RES3, m_bxRes3);
	DDX_Control(pDX, IDC_RES2, m_bxRes2);
	DDX_Control(pDX, IDC_RES1, m_bxRes1);
	DDX_Control(pDX, IDC_STATUS, m_statuscontrol);
	DDX_Control(pDX, IDC_EDITCAM5, m_camser5);
	DDX_Control(pDX, IDC_EDITCAM3, m_camser3);
	DDX_Control(pDX, IDC_EDITCAM4, m_camser4);
	DDX_Control(pDX, IDC_EDITCAM2, m_camser2);
	DDX_Control(pDX, IDC_EDITCAM1, m_camser1);
	DDX_Text(pDX, IDC_TOTAL, m_total);
	DDX_Text(pDX, IDC_REJECTS, m_rejects);
	DDX_Text(pDX, IDC_STATUS, m_status);
	DDX_Text(pDX, IDC_RESULT2, m_result2);
	DDX_Text(pDX, IDC_RESULT3, m_result3);
	DDX_Text(pDX, IDC_RESULT4, m_result4);
	DDX_Text(pDX, IDC_RESULT5, m_result5);
	DDX_Text(pDX, IDC_RESULT6, m_result6);
	DDX_Text(pDX, IDC_RESULT7, m_result7);
	DDX_Text(pDX, IDC_RESULT8, m_result8);
	DDX_Text(pDX, IDC_TIME, m_time);
	DDX_Text(pDX, IDC_H1, m_h1);
	DDX_Text(pDX, IDC_H2, m_h2);
	DDX_Text(pDX, IDC_H3, m_h3);
	DDX_Text(pDX, IDC_H4, m_h4);
	DDX_Text(pDX, IDC_H5, m_h5);
	DDX_Text(pDX, IDC_H6, m_h6);
	DDX_Text(pDX, IDC_H7, m_h7);
	DDX_Text(pDX, IDC_H8, m_h8);
	DDX_Text(pDX, IDC_INSP1, m_insp1);
	DDX_Text(pDX, IDC_INSP2, m_insp2);
	DDX_Text(pDX, IDC_INSP3, m_insp3);
	DDX_Text(pDX, IDC_INSP4, m_insp4);
	DDX_Text(pDX, IDC_INSP5, m_insp5);
	DDX_Text(pDX, IDC_INSP6, m_insp6);
	DDX_Text(pDX, IDC_INSP7, m_insp7);
	DDX_Text(pDX, IDC_INSP8, m_insp8);
	DDX_Text(pDX, IDC_L1, m_l1);
	DDX_Text(pDX, IDC_L2, m_l2);
	DDX_Text(pDX, IDC_L3, m_l3);
	DDX_Text(pDX, IDC_L4, m_l4);
	DDX_Text(pDX, IDC_L5, m_l5);
	DDX_Text(pDX, IDC_L6, m_l6);
	DDX_Text(pDX, IDC_L7, m_l7);
	DDX_Text(pDX, IDC_L8, m_l8);
	DDX_Text(pDX, IDC_EDITCAM1, m_editcam1);
	DDX_Text(pDX, IDC_EDITCAM2, m_editcam2);
	DDX_Text(pDX, IDC_EDITCAM3, m_editcam3);
	DDX_Text(pDX, IDC_EDITCAM4, m_editcam4);
	DDX_Text(pDX, IDC_EDITCAM5, m_editcam5);
	DDX_Text(pDX, IDC_TIME1, m_time1);
	DDX_Text(pDX, IDC_TIME2, m_time2);
	DDX_Text(pDX, IDC_TIME3, m_time3);
	DDX_Text(pDX, IDC_TIME4, m_time4);
	DDX_Text(pDX, IDC_TIME5, m_time5);
	DDX_Text(pDX, IDC_SHUTTER, m_shutter);
	DDX_Text(pDX, IDC_RESULT1R, m_result1r);
	DDX_Text(pDX, IDC_INSPDELAY, m_inspdelay);
	DDX_Text(pDX, IDC_RESULT1L, m_result1l);
	DDX_Text(pDX, IDC_B1, m_b1);
	DDX_Text(pDX, IDC_B2, m_b2);
	DDX_Text(pDX, IDC_B3, m_b3);
	DDX_Text(pDX, IDC_B4, m_b4);
	DDX_Text(pDX, IDC_H9, m_h9);
	DDX_Text(pDX, IDC_INSP9, m_insp9);
	DDX_Text(pDX, IDC_RESULT9, m_result9);
	DDX_Text(pDX, IDC_L9, m_l9);
	DDX_Text(pDX, IDC_SHUTTER2, m_shutter2);
	DDX_Text(pDX, IDC_LABEL, m_label);
	DDX_Text(pDX, IDC_BOTTLE, m_bottle);
	DDX_Text(pDX, IDC_CAMRESTART, m_camrestart);
	DDX_Text(pDX, IDC_RESULT10, m_result10);
	DDX_Text(pDX, IDC_L10, m_l10);
	DDX_Text(pDX, IDC_H10, m_h10);
	DDX_Text(pDX, IDC_INSP10, m_insp10);
	DDX_Text(pDX, IDC_RESULT11, m_result11);
	DDX_Text(pDX, IDC_L11, m_l11);
	DDX_Text(pDX, IDC_H11, m_h11);
	DDX_Text(pDX, IDC_INSP11, m_insp11);
	DDX_Text(pDX, IDC_RESULT12, m_result12);
	DDX_Text(pDX, IDC_L12, m_l12);
	DDX_Text(pDX, IDC_H12, m_h12);
	DDX_Text(pDX, IDC_INSP12, m_insp12);
	DDX_Text(pDX, IDC_deltaC1, m_deltaC1);
	DDX_Text(pDX, IDC_deltaC2, m_deltaC2);
	DDX_Text(pDX, IDC_deltaC3, m_deltaC3);
	DDX_Text(pDX, IDC_deltaC4, m_deltaC4);
	DDX_Text(pDX, IDC_NOMOT, m_enable2);
	DDX_Text(pDX, IDC_EJECT2, m_enable5);
	//}}AFX_DATA_MAP
}

BOOL CBottleView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CBottleView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	ResizeParentToFit();
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pview=this;
	theapp=(CBottleApp*)AfxGetApp();
	toggle=true;
	m_status="Enter a job";
	EjectAll=false;
	ToggleColor=false;

	SetTimer(3,800,NULL);
/*
	if(!theapp->waistAvailable)
	{
		GetDlgItem(IDC_INSPEC11_TXT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_INSP11)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_L11)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RESULT11)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_H11)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RES11)->ShowWindow(SW_HIDE);
	}
*/
	if(!theapp->cam5Enable)
	{
		GetDlgItem(IDC_MORE6)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_LESS5)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SHUTTER2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATICCAM5)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDITCAM5)->ShowWindow(SW_HIDE);
	}

	m_editcam5.Format("%i",theapp->CamSer5);
	m_editcam4.Format("%i",theapp->CamSer4);
	m_editcam3.Format("%i",theapp->CamSer3);
	m_editcam2.Format("%i",theapp->CamSer2);
	m_editcam1.Format("%i",theapp->CamSer1);
	m_shutter.Format("%1.2f",theapp->savedShutter);
	m_shutter2.Format("%1.2f",theapp->savedShutter2);
	m_inspdelay.Format("%2i",theapp->inspDelay);

	if(!theapp->showMot)	
	{CheckDlgButton(IDC_NOMOT,0); m_enable2.Format("%s","Motor Disabled");}
	else
	{CheckDlgButton(IDC_NOMOT,1);m_enable2.Format("%s","Motor Enabled");}


	if(theapp->capEjectorInstalled)
	{CheckDlgButton(IDC_EJECT2,0);m_enable5.Format("%s","2nd Ejector Installed");}
	else
	{CheckDlgButton(IDC_EJECT2,1);m_enable5.Format("%s","2nd Ejector Not Installed");}


	if(theapp->securityEnable)	{CheckDlgButton(IDC_ENABLESEC,1);}
	else								{CheckDlgButton(IDC_ENABLESEC,0);}

	int tempSel = theapp->tempSelect;

	if(tempSel==0){CheckDlgButton(IDC_TEMP_NONE,1);}else{CheckDlgButton(IDC_TEMP_NONE,0);}
	if(tempSel==1){CheckDlgButton(IDC_TEMP_PLC,1);}else{CheckDlgButton(IDC_TEMP_PLC,0);}
	if(tempSel==2){CheckDlgButton(IDC_TEMP_USB,1);}else{CheckDlgButton(IDC_TEMP_USB,0);}

	UpdateData(false);
}

/////////////////////////////////////////////////////////////////////////////

void CBottleView::OnlineBox(int BoxColor)
{
	int x=136;
	int y=433; //438
	int x2=x+10;
	int y2=y+65;

	CRect a, b, c, d;
	//CRgn s;
	COLORREF sqrred, sqrgreen, sqrgray;
	CBrush Colorr, Colorg, Colorb;
	CClientDC ColorRect(this);

	//aa.SetRect(83,90,125,125);
	
	a.SetRect(x,y,x2,y2);
	b.SetRect(x+44,y,x2+44,y2);
	c.SetRect(x,y,x2+44,y+3);
	d.SetRect(x,y2-3,x2+44,y2);
	
	sqrred=RGB(200,0,0);
	sqrgreen=RGB(0,200,0);
	sqrgray=RGB(200,200,200);
	Colorr.CreateSolidBrush(sqrred);
	Colorg.CreateSolidBrush(sqrgreen);
	Colorb.CreateSolidBrush(sqrgray);

	switch (BoxColor)
	{
		case 1: 
		
			ColorRect.SelectObject(&Colorr);	
			ColorRect.FillRect(a,&Colorr);
			ColorRect.FillRect(b,&Colorr);
			ColorRect.FillRect(c,&Colorr);
			ColorRect.FillRect(d,&Colorr);
			break;
		
		case 2: 
		
			ColorRect.SelectObject(&Colorg);	
			ColorRect.FillRect(a,&Colorg);
			ColorRect.FillRect(b,&Colorg);
			ColorRect.FillRect(c,&Colorg);
			ColorRect.FillRect(d,&Colorg);
			break;
		
		case 3: 
		
			ColorRect.SelectObject(&Colorb);	
			ColorRect.FillRect(a,&Colorb);
			ColorRect.FillRect(b,&Colorb);
			ColorRect.FillRect(c,&Colorb);
			ColorRect.FillRect(d,&Colorb);
			break;
	}

	UpdateData(false);
}


void CBottleView::UpdateDisplay()
{
	int x=165;
	int y=126;//124
	int x2=x+20;
	int y2=y+20;

	//CRect aa, a, b, c, d, e, f, g, h, i,j,k;
	COLORREF sqrred, sqrgreen, sqrgray;
	CBrush Colorr, Colorg, Colorb;
	CClientDC ColorRect(this);

	CBrush BrushRed(RGB(200,0,0));
	CBrush BrushGreen(RGB(0,200,0));
	CBrush BrushGray(RGB(200,200,200));

	sqrred=RGB(200,0,0);
	sqrgreen=RGB(0,200,0);
	sqrgray=RGB(200,200,200);

	Colorr.CreateSolidBrush(sqrred);
	Colorg.CreateSolidBrush(sqrgreen);
	Colorb.CreateSolidBrush(sqrgray);

	CRect RectInsp1, RectInsp2, RectInsp3, RectInsp4, RectInsp5,RectInsp6;
	CRect RectInsp7, RectInsp8, RectInsp9, RectInsp10, RectInsp11, RectInsp12;
	CRect RectResult;
	
	CPen GrayPen(PS_SOLID, 1, RGB(100,100,100));
	ColorRect.SelectObject(&GrayPen);	//to make sure the rectangles have gray border

	if(theapp->Total<=0 )
	{
		m_total.Format(" %.5i",0);

		if (!pframe->InspOpen) 
		{
			m_result1l.Format("%3.1f",0);
			m_result1r.Format("%3.1f",0);
		
			m_result2.Format("%3.1f",0);
			m_result3.Format("%3.1f",0);
			m_result4.Format("%3.1f",0);
			m_result5.Format("%3.1f",0);
			m_result6.Format("%3.1f",0);
			m_result7.Format("%3.1f",0);
			m_result8.Format("%3.1f",0);
			m_result9.Format("%i",0);
			m_result10.Format("%3.0f",0);
			m_result11.Format("%3.0f",0);
			m_result12.Format("%3.0f",0);
		}
		else
		{
			m_result1l.Format("%3.1f",pframe->m_pxaxis->Result1l);
			m_result1r.Format("%3.1f",pframe->m_pxaxis->Result1r);
			m_result2.Format("%3.1f",pframe->m_pxaxis->Result2);
			m_result3.Format("%3.1f",pframe->m_pxaxis->Result3);
			m_result4.Format("%3.1f",pframe->m_pxaxis->Result4);
			m_result5.Format("%3.1f",pframe->m_pxaxis->Result5);
			m_result6.Format("%3.1f",pframe->m_pxaxis->Result6);
			m_result7.Format("%3.1f",pframe->m_pxaxis->Result7);
			m_result8.Format("%3.1f",pframe->m_pxaxis->Result8);
			m_result9.Format("%3.0f",pframe->m_pxaxis->Result9);
			m_result10.Format("%3.0f",pframe->m_pxaxis->Result10);
			m_result11.Format("%3.0f",pframe->m_pxaxis->Result11);
			m_result12.Format("%3.0f",pframe->m_pxaxis->Result12);
		}

		m_rejects.Format(" %.5i",0);
	}
	else
	{
		m_resultBx.GetWindowRect(RectResult);
		if(	pframe->m_pxaxis->BadCap ||
			pframe->m_pxaxis->BadCap2 )
		{
			ColorRect.SelectObject(&BrushRed);
		}
		else									
		{
			ColorRect.SelectObject(&BrushGreen);
		}
		ScreenToClient(&RectResult);			//move the screen coordinates to the rectangle
		ColorRect.Rectangle(RectResult);		//creates the rectangle
		
		if( theapp->cam5Enable)
		{
		////////////////////
		
			m_bxRes1.GetWindowRect(RectInsp1);	//gets the data ofthe rectangle
			if( theapp->inspctn[1].enable )
			{
				if(pframe->m_pxaxis->inspResultOK[1] )	{ColorRect.SelectObject(&BrushGreen);}
				else											{ColorRect.SelectObject(&BrushRed);}
			}
			else 
			{
				ColorRect.SelectObject(&BrushGray);
			}
			ScreenToClient(&RectInsp1);			//move the screen coordinates to the rectangle
			ColorRect.Rectangle(RectInsp1);		//creates the rectangle

			////////////////////

			m_bxRes2.GetWindowRect(RectInsp2);	//gets the data ofthe rectangle
			if( theapp->inspctn[2].enable )
			{
				if(pframe->m_pxaxis->inspResultOK[2] )	{ColorRect.SelectObject(&BrushGreen);}
				else											{ColorRect.SelectObject(&BrushRed);}
			}
			else {ColorRect.SelectObject(&BrushGray);}
			ScreenToClient(&RectInsp2);			//move the screen coordinates to the rectangle
			ColorRect.Rectangle(RectInsp2);		//creates the rectangle

			////////////////////
			
			m_bxRes3.GetWindowRect(RectInsp3);	//gets the data ofthe rectangle
			if( theapp->inspctn[3].enable )
			{
				if(pframe->m_pxaxis->inspResultOK[3] )	{ColorRect.SelectObject(&BrushGreen);}
				else											{ColorRect.SelectObject(&BrushRed);}
			}
			else {ColorRect.SelectObject(&BrushGray);}
			ScreenToClient(&RectInsp3);			//move the screen coordinates to the rectangle
			ColorRect.Rectangle(RectInsp3);		//creates the rectangle

				////////////////////

			m_bxRes4.GetWindowRect(RectInsp4);	//gets the data ofthe rectangle
			if( theapp->inspctn[4].enable )
			{
				if(pframe->m_pxaxis->inspResultOK[4] )	{ColorRect.SelectObject(&BrushGreen);}
				else											{ColorRect.SelectObject(&BrushRed);}
			}
			else {ColorRect.SelectObject(&BrushGray);}
			ScreenToClient(&RectInsp4);			//move the screen coordinates to the rectangle
			ColorRect.Rectangle(RectInsp4);		//creates the rectangle
		
		////////////////////

		}
		else  //When Cam 5 is disabled, Grey out controls for 1st 3 inspections  (also grey out control for I4)
		{
			//Insp1
			GetDlgItem(IDC_RESULT1L)->EnableWindow(FALSE);
			GetDlgItem(IDC_RESULT1R)->EnableWindow(FALSE);
			GetDlgItem(IDC_RES1)->EnableWindow(FALSE);
			GetDlgItem(IDC_L1)->EnableWindow(FALSE);
			GetDlgItem(IDC_H1)->EnableWindow(FALSE);
			GetDlgItem(IDC_INSP1)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC1)->EnableWindow(FALSE);

			//Insp 2
			GetDlgItem(IDC_RESULT2)->EnableWindow(FALSE);
			GetDlgItem(IDC_RES2)->EnableWindow(FALSE);
			GetDlgItem(IDC_L2)->EnableWindow(FALSE);
			GetDlgItem(IDC_H2)->EnableWindow(FALSE);
			GetDlgItem(IDC_INSP2)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC2)->EnableWindow(FALSE);

			//Insp 3
			GetDlgItem(IDC_RESULT3)->EnableWindow(FALSE);
			GetDlgItem(IDC_RES3)->EnableWindow(FALSE);
			GetDlgItem(IDC_L3)->EnableWindow(FALSE);
			GetDlgItem(IDC_H3)->EnableWindow(FALSE);
			GetDlgItem(IDC_INSP3)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC3)->EnableWindow(FALSE);

			//Insp 4
			GetDlgItem(IDC_RESULT4)->EnableWindow(FALSE);
			GetDlgItem(IDC_RES4)->EnableWindow(FALSE);
			GetDlgItem(IDC_L4)->EnableWindow(FALSE);
			GetDlgItem(IDC_H4)->EnableWindow(FALSE);
			GetDlgItem(IDC_INSP4)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC4)->EnableWindow(FALSE);
		}	

		////////////////////

		m_bxRes5.GetWindowRect(RectInsp5);	//gets the data ofthe rectangle
		if( theapp->inspctn[5].enable )
		{
			if(pframe->m_pxaxis->inspResultOK[5] )	{ColorRect.SelectObject(&BrushGreen);}
			else											{ColorRect.SelectObject(&BrushRed);}
		}
		else {ColorRect.SelectObject(&BrushGray);}
		ScreenToClient(&RectInsp5);			//move the screen coordinates to the rectangle
		ColorRect.Rectangle(RectInsp5);		//creates the rectangle

		////////////////////

		m_bxRes6.GetWindowRect(RectInsp6);	//gets the data ofthe rectangle
		if( theapp->inspctn[6].enable )
		{
			if(pframe->m_pxaxis->inspResultOK[6] )	{ColorRect.SelectObject(&BrushGreen);}
			else											{ColorRect.SelectObject(&BrushRed);}
		}
		else {ColorRect.SelectObject(&BrushGray);}
		ScreenToClient(&RectInsp6);			//move the screen coordinates to the rectangle
		ColorRect.Rectangle(RectInsp6);		//creates the rectangle

		////////////////////

		m_bxRes7.GetWindowRect(RectInsp7);	//gets the data ofthe rectangle
		if( theapp->inspctn[7].enable )
		{
			if(pframe->m_pxaxis->inspResultOK[7] )	{ColorRect.SelectObject(&BrushGreen);}
			else											{ColorRect.SelectObject(&BrushRed);}
		}
		else {ColorRect.SelectObject(&BrushGray);}
		ScreenToClient(&RectInsp7);			//move the screen coordinates to the rectangle
		ColorRect.Rectangle(RectInsp7);		//creates the rectangle

		////////////////////

		m_bxRes8.GetWindowRect(RectInsp8);	//gets the data ofthe rectangle
		if( theapp->inspctn[8].enable )
		{
			if(pframe->m_pxaxis->inspResultOK[8] )	{ColorRect.SelectObject(&BrushGreen);}
			else											{ColorRect.SelectObject(&BrushRed);}
		}
		else {ColorRect.SelectObject(&BrushGray);}
		ScreenToClient(&RectInsp8);			//move the screen coordinates to the rectangle
		ColorRect.Rectangle(RectInsp8);		//creates the rectangle

		////////////////////

		m_bxRes9.GetWindowRect(RectInsp9);	//gets the data ofthe rectangle
		if( theapp->inspctn[9].enable )
		{
			if(pframe->m_pxaxis->inspResultOK[9])	{ColorRect.SelectObject(&BrushGreen);}
			else											{ColorRect.SelectObject(&BrushRed);}
		}
		else {ColorRect.SelectObject(&BrushGray);}
		ScreenToClient(&RectInsp9);			//move the screen coordinates to the rectangle
		ColorRect.Rectangle(RectInsp9);		//creates the rectangle

		////////////////////

		m_bxRes10.GetWindowRect(RectInsp10);	//gets the data ofthe rectangle
		if( theapp->inspctn[10].enable )
		{
			if(pframe->m_pxaxis->inspResultOK[10])	{ColorRect.SelectObject(&BrushGreen);}
			else											{ColorRect.SelectObject(&BrushRed);}
		}
		else {ColorRect.SelectObject(&BrushGray);}
		ScreenToClient(&RectInsp10);			//move the screen coordinates to the rectangle
		ColorRect.Rectangle(RectInsp10);		//creates the rectangle
		////////////////////

		if(theapp->waistAvailable)
		{
			m_bxRes11.GetWindowRect(RectInsp11);	//gets the data ofthe rectangle
			if( theapp->inspctn[11].enable )
			{
				if(pframe->m_pxaxis->inspResultOK[11])	{ColorRect.SelectObject(&BrushGreen);}
				else											{ColorRect.SelectObject(&BrushRed);}
			}
			else {ColorRect.SelectObject(&BrushGray);}
			ScreenToClient(&RectInsp11);			//move the screen coordinates to the rectangle
			ColorRect.Rectangle(RectInsp11);		//creates the rectangle
		}
		else
		{
			//Insp 11
			GetDlgItem(IDC_RESULT11)->EnableWindow(FALSE);
			GetDlgItem(IDC_RES11)->EnableWindow(FALSE);
			GetDlgItem(IDC_L11)->EnableWindow(FALSE);
			GetDlgItem(IDC_H11)->EnableWindow(FALSE);
			GetDlgItem(IDC_INSP11)->EnableWindow(FALSE);
			GetDlgItem(IDC_INSPEC11_TXT)->EnableWindow(FALSE);
		}
		
		//align available or job for square bottle
		if(theapp->alignAvailable || theapp->jobinfo[pframe->CurrentJobNum].bottleStyle == 10)
		{
			m_bxRes12.GetWindowRect(RectInsp12);	//gets the data ofthe rectangle
			if( theapp->inspctn[12].enable )
			{
				if(pframe->m_pxaxis->inspResultOK[12])	{ColorRect.SelectObject(&BrushGreen);}
				else											{ColorRect.SelectObject(&BrushRed);}
			}
			else {ColorRect.SelectObject(&BrushGray);}
			ScreenToClient(&RectInsp12);			//move the screen coordinates to the rectangle
			ColorRect.Rectangle(RectInsp12);		//creates the rectangle
		}
		else
		{
			//Insp 12
			GetDlgItem(IDC_RESULT12)->EnableWindow(FALSE);
			GetDlgItem(IDC_RES12)->EnableWindow(FALSE);
			GetDlgItem(IDC_L12)->EnableWindow(FALSE);
			GetDlgItem(IDC_H12)->EnableWindow(FALSE);
			GetDlgItem(IDC_INSP12)->EnableWindow(FALSE);
			GetDlgItem(IDC_INSPEC12_TXT)->EnableWindow(FALSE);
		}

		////////////////////

		m_total.Format("%5i",theapp->Total);
		
		m_result1l.Format("%3.1f",pframe->m_pxaxis->Result1l);
		m_result1r.Format("%3.1f",pframe->m_pxaxis->Result1r);
		m_result2.Format("%3.1f",pframe->m_pxaxis->Result2);
		m_result3.Format("%3.1f",pframe->m_pxaxis->Result3);
		m_result4.Format("%3.1f",pframe->m_pxaxis->Result4);
		m_result5.Format("%3.1f",pframe->m_pxaxis->Result5);
		m_result6.Format("%3.1f",pframe->m_pxaxis->Result6);
		m_result7.Format("%3.1f",pframe->m_pxaxis->Result7);
		m_result8.Format("%3.1f",pframe->m_pxaxis->Result8);
		m_result9.Format("%3.0f",pframe->m_pxaxis->Result9);
		m_result10.Format("%3.0f",pframe->m_pxaxis->Result10);
		m_result11.Format("%3.0f",pframe->m_pxaxis->Result11);
		m_result12.Format("%3.0f",pframe->m_pxaxis->Result12);


		m_b1.Format("%3i",pframe->m_pxaxis->labelPos[1].x1);	//m_b1.Format("%3i",theapp->res1);
		m_b2.Format("%3i",pframe->m_pxaxis->labelPos[2].x1);	//m_b2.Format("%3i",theapp->res2);
		m_b3.Format("%3i",pframe->m_pxaxis->labelPos[3].x1);	//m_b3.Format("%3i",theapp->res3);
		m_b4.Format("%3i",pframe->m_pxaxis->labelPos[4].x1);	//m_b4.Format("%3i",theapp->res4);
		
		m_rejects.Format(" %5i",pframe->Rejects);

		m_time5.Format(" % .3f",float(pframe->m_pxaxis->dMicroSecondC1I10) );
	}

	m_time.Format(" %.5f",  pframe->m_pxaxis->spanElapsed);
	m_time1.Format(" %.5f", pframe->m_pxaxis->spanElapsedCam1DupAndCrop);
	m_time2.Format(" %.5f", pframe->m_pxaxis->spanElapsed0);
	m_camrestart.Format(" %2i",theapp->lostCam);

	m_deltaC1.Format("%i",pframe->m_pxaxis->label_mating_results[1].x);
	m_deltaC2.Format("%i",pframe->m_pxaxis->label_mating_results[2].x);
	m_deltaC3.Format("%i",pframe->m_pxaxis->label_mating_results[3].x);
	m_deltaC4.Format("%i",pframe->m_pxaxis->label_mating_results[4].x);

	m_bottle.Format(" %3i",theapp->bottlepos);
	m_label.Format(" %3i",theapp->labelpos);

	if(EjectAll)  m_status="Eject All is ON!!!";


	if(theapp->securityEnable)		{CheckDlgButton(IDC_ENABLESEC,1);}
	else									{CheckDlgButton(IDC_ENABLESEC,0);}

	if(theapp->passNotFinished)		{CheckDlgButton(IDC_PASSNOTFINISHED,1);}
	else									{CheckDlgButton(IDC_PASSNOTFINISHED,0);}

	int tempSel = theapp->tempSelect;

	if(tempSel==0){CheckDlgButton(IDC_TEMP_NONE,1);}else{CheckDlgButton(IDC_TEMP_NONE,0);}
	if(tempSel==1){CheckDlgButton(IDC_TEMP_PLC,1);}else{CheckDlgButton(IDC_TEMP_PLC,0);}
	if(tempSel==2){CheckDlgButton(IDC_TEMP_USB,1);}else{CheckDlgButton(IDC_TEMP_USB,0);}

	UpdateData(false);
}

void CBottleView::OnTrig1() 
{
	KillTimer(1);
	toggle=true;
	Sleep(10);
	pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	pframe->m_pxaxis->lockOut=false;
}


void CBottleView::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{pframe->SendMessage(WM_TRIGGER,NULL,NULL);}

	if (nIDEvent==2) 
	{
		if(ToggleColor)	{ OnlineBox(1); ToggleColor=false;}
		else					{ ToggleColor=true; OnlineBox(3);}
	}

	if (nIDEvent==3) 
	{ KillTimer(3); } 

	if (nIDEvent==4) 
	{ 
		KillTimer(4); 	
		
		if(theapp->serPresent)
		{pframe->m_pserial->SendChar(0,510); }
	} 

	if (nIDEvent==5) 
	{	
		if(ToggleColor)	{ OnlineBox(2); ToggleColor=false;}
		else					{ ToggleColor=true; OnlineBox(3);}
	}

	
	if (nIDEvent==8) 
	{
		KillTimer(8);
		pframe->SaveJob(pframe->CurrentJobNum);
	}

	CFormView::OnTimer(nIDEvent);
}


void CBottleView::OnRtot() 
{
	int result=AfxMessageBox("Do you want to reset the totals?", MB_YESNO);
	if(IDYES==result) pframe->ResetTotals();
}


void CBottleView::UpdateDisplay2()
{
	m_total.Format("%5i",theapp->Total);
	m_rejects.Format(" %5i",pframe->Rejects);
	m_b4.Format("%3i",pframe->temp_sleever);
	m_b3.Format("%3i",pframe->TEMP_COUNTER);
	UpdateData(false);
}


void CBottleView::OnRejectall() 
{	
	Prompt prompt;
	prompt.DoModal();
}

void CBottleView::OnOnline() 
{
	pframe->OnLine=true;
	KillTimer(2);

	SetTimer(5,1000,NULL);

	if(theapp->serPresent)
	{pframe->m_pserial->SendChar(0,400);}
}

void CBottleView::OnOffline() 
{
	if(pframe->SJob)
	{
		pframe->OnLine=false;
		KillTimer(5);
		SetTimer(2,500,NULL);
	
		if(theapp->serPresent)		{pframe->m_pserial->SendChar(0,410);}
	}
	else	{AfxMessageBox("Password Protected!");}
}


void CBottleView::OnCont() 
{
	if(toggle)
	{ SetTimer(1,theapp->timedTrigger,NULL); toggle=false;}
	else
	{toggle=true;	KillTimer(1);}
}


void CBottleView::OnTesteject() 
{
	if(theapp->serPresent)	{pframe->m_pserial->SendChar(0,500);}

	SetTimer(4,1000,NULL);
}


void CBottleView::OnMore3() 
{
	theapp->savedShutter+=0.1f;

	if(theapp->savedShutter>=30) //previously capped at 4
	{theapp->savedShutter=30;} //previously capped at 4
	else
	{
		pframe->m_pcamera->OnSsmore1();
		pframe->m_pcamera->OnSsmore2();
		pframe->m_pcamera->OnSsmore3();
		pframe->m_pcamera->OnSsmore4();
	}

	m_shutter.Format("%1.2f",theapp->savedShutter);

	pframe->SaveJob(pframe->CurrentJobNum);
	UpdateData(false);
}

void CBottleView::OnLess3() 
{
	theapp->savedShutter-=0.1f;

	if(theapp->savedShutter<=0.1)
	{theapp->savedShutter=0.1f;}
	else
	{
		pframe->m_pcamera->OnSsless1();
		pframe->m_pcamera->OnSsless2();
		pframe->m_pcamera->OnSsless3();
		pframe->m_pcamera->OnSsless4();
	}

	m_shutter.Format("%1.2f",theapp->savedShutter);

	pframe->SaveJob(pframe->CurrentJobNum);
	UpdateData(false);
}


void CBottleView::UpdateMinor() 
{
	int bottleStyle = theapp->jobinfo[pframe->CurrentJobNum].bottleStyle;

	////////////////////

	m_l1.Format("%3i",theapp->inspctn[1].lmin);
	m_l2.Format("%3i",theapp->inspctn[2].lmin);
	m_l3.Format("%3i",theapp->inspctn[3].lmin);
	m_l4.Format("%3i",theapp->inspctn[4].lmin);
	m_l5.Format("%3i",theapp->inspctn[5].lmin);

	//Wolverine
	if(bottleStyle==3)			{m_l6.Format("%3i",theapp->inspctn[6].lmin);}	//Wolverine
	else if(bottleStyle==10)	{m_l6.Format("%3i",theapp->inspctn[6].lminC);}	//Square
	else						{m_l6.Format("%3i",theapp->inspctn[6].lminB);}	//Cylindrical

	m_l7.Format("%3i",theapp->inspctn[7].lmin);
	m_l8.Format("%3i",theapp->inspctn[8].lmin);
	m_l9.Format("%3i",theapp->inspctn[9].lmin);
	m_l10.Format("%3i",theapp->inspctn[10].lmin);
	m_l11.Format("%3i",theapp->inspctn[11].lmin);
	m_l12.Format("%3i",theapp->inspctn[12].lmin);

	////////////////////
	
	m_h1.Format("%3i",theapp->inspctn[1].lmax);
	m_h2.Format("%3i",theapp->inspctn[2].lmax);
	m_h3.Format("%3i",theapp->inspctn[3].lmax);
	m_h4.Format("%3i",theapp->inspctn[4].lmax);
	m_h5.Format("%3i",theapp->inspctn[5].lmax);

	if(bottleStyle==3)			{m_h6.Format("%3i",theapp->inspctn[6].lmax);}	//Wolverine
	else if(bottleStyle==10)	{m_h6.Format("%3i",theapp->inspctn[6].lmaxC);}	//Square
	else						{m_h6.Format("");}								//Cylindrical
	
	m_h7.Format("%3i",theapp->inspctn[7].lmax);
	m_h8.Format("%3i",theapp->inspctn[8].lmax);
	m_h9.Format("%3i",theapp->inspctn[9].lmax);
	m_h10.Format("%3i",theapp->inspctn[10].lmax);
	m_h11.Format("%3i",theapp->inspctn[11].lmax);
	m_h12.Format("%3i",theapp->inspctn[12].lmax);
	////////////////////

	m_insp1.Format("%s",theapp->inspctn[1].name);
	m_insp2.Format("%s",theapp->inspctn[2].name);
	m_insp3.Format("%s",theapp->inspctn[3].name);
	m_insp4.Format("%s",theapp->inspctn[4].name);
	m_insp5.Format("%s",theapp->inspctn[5].name);
	m_insp6.Format("%s",theapp->inspctn[6].name);
	m_insp7.Format("%s",theapp->inspctn[7].name);
	m_insp8.Format("%s",theapp->inspctn[8].name);
	m_insp9.Format("%s",theapp->inspctn[9].name);
	m_insp10.Format("%s",theapp->inspctn[10].name);
	m_insp11.Format("%s",theapp->inspctn[11].name);
	m_insp12.Format("%s",theapp->inspctn[12].name);

}


void CBottleView::OnCambutton() 
{
	int len;
	
	CString tempString1;
	len=m_camser1.LineLength(m_camser1.LineIndex(0));
	m_camser1.GetLine(0,tempString1.GetBuffer(len),len);
	theapp->CamSer1=atoi(tempString1);
	m_editcam1.Format("%i",theapp->CamSer1);

	CString tempString2;
	len=m_camser2.LineLength(m_camser2.LineIndex(0));
	m_camser2.GetLine(0,tempString2.GetBuffer(len),len);
	theapp->CamSer2=atoi(tempString2);
	m_editcam2.Format("%i",theapp->CamSer2);

	CString tempString3;
	len=m_camser3.LineLength(m_camser3.LineIndex(0));
	m_camser3.GetLine(0,tempString3.GetBuffer(len),len);
	theapp->CamSer3=atoi(tempString3);
	m_editcam3.Format("%i",theapp->CamSer3);

	CString tempString4;
	len=m_camser4.LineLength(m_camser4.LineIndex(0));
	m_camser4.GetLine(0,tempString4.GetBuffer(len),len);
	theapp->CamSer4=atoi(tempString4);
	m_editcam4.Format("%i",theapp->CamSer4);

	CString tempString5;
	len=m_camser5.LineLength(m_camser5.LineIndex(0));
	m_camser5.GetLine(0,tempString5.GetBuffer(len),len);
	theapp->CamSer5=atoi(tempString5);
	m_editcam5.Format("%i",theapp->CamSer5);

	pframe->SaveJob(pframe->CurrentJobNum);
	UpdateData(false);	
}

// CBottleView diagnostics

#ifdef _DEBUG
void CBottleView::AssertValid() const
{
	CFormView::AssertValid();
}

void CBottleView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CBottleDoc* CBottleView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CBottleDoc)));
	return (CBottleDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CBottleView message handlers
void CBottleView::OnFreeze() 
{
	pframe->Freeze=true;
	pframe->FreezeReset=true;
}

void CBottleView::OnFreeze2() 
{
	pframe->Freeze=false;
}


void CBottleView::OnShowjob() 
{
	m_status=m_currentjob;
	UpdateData(false);
}

void CBottleView::OnResetcams() 
{
	//pframe->m_pcamera->Restart();
}

void CBottleView::OnMore5() 
{
	theapp->inspDelay+=1;
	m_inspdelay.Format("%2i",theapp->inspDelay);
	
	pframe->SaveJob(pframe->CurrentJobNum);
	UpdateData(false);
}

void CBottleView::OnLess4() 
{
	theapp->inspDelay-=1;
	m_inspdelay.Format("%2i",theapp->inspDelay);
	
	pframe->SaveJob(pframe->CurrentJobNum);
	UpdateData(false);
}

void CBottleView::OnTest24() 
{
	pframe->CopyDone=false;
	pframe->testData=true;
	pframe->timesThru=28;
	pframe->UpdatePLCData();	
}

void CBottleView::OnNomot() 
{
	if(theapp->showMot)
	{
		theapp->showMot=false;
		CheckDlgButton(IDC_NOMOT,0); 
		m_enable2.Format("%s","Motor Disabled");
	}
	else
	{
		theapp->showMot=true;
		CheckDlgButton(IDC_NOMOT,1);
		m_enable2.Format("%s","Motor Enabled");
	}

	m_statuscontrol.SetFocus();

	pframe->SaveJob(pframe->CurrentJobNum);
	UpdateData(false);	
}

void CBottleView::OnMore6() 
{
	theapp->savedShutter2+=0.1f;

	if(theapp->savedShutter2>=2)	{theapp->savedShutter2=2;}
	else							{pframe->m_pcamera->OnSsmore5();}

	m_shutter2.Format("%1.2f",theapp->savedShutter2);

	pframe->SaveJob(pframe->CurrentJobNum);
	UpdateData(false);
}

void CBottleView::OnLess5() 
{
	theapp->savedShutter2-=0.1f;

	if(theapp->savedShutter<=0.1)	{theapp->savedShutter=0.1f;}
	else							{pframe->m_pcamera->OnSsless5();}
	
	m_shutter2.Format("%1.2f",theapp->savedShutter2);
	pframe->SaveJob(pframe->CurrentJobNum);
	UpdateData(false);
}


void CBottleView::OnEject2() 
{
	if(theapp->capEjectorInstalled)
	{
		theapp->capEjectorInstalled=false;
		CheckDlgButton(IDC_EJECT2,1); 
		m_enable5.Format("%s","2nd Ejector Not Installed");
	}

	else
	{
		theapp->capEjectorInstalled=true;
		CheckDlgButton(IDC_EJECT2,0);
		m_enable5.Format("%s","2nd Ejector Installed");
	}

	pframe->SaveJob(pframe->CurrentJobNum);

	UpdateData(false);
}

void CBottleView::OnEnablesec() 
{
	CheckDlgButton(IDC_ENABLESEC, theapp->securityEnable ? 0 : 1);
	theapp->securityEnable = !theapp->securityEnable;

	///////////////////////////////////////////////

	theapp->SecOff = !theapp->securityEnable;

	//with SecOff==true  NO security
	//with SecOff==false there is security
	pframe->setSecurityFlagsOFF(theapp->SecOff);

	///////////////////////////////////////////////

	pframe->SaveJob(pframe->CurrentJobNum);
	UpdateData(false);	
}

void CBottleView::OnPassnotfinished() 
{
	CheckDlgButton(IDC_PASSNOTFINISHED, theapp->passNotFinished ? 0 : 1);
	theapp->passNotFinished = !theapp->passNotFinished;

	///////////////////////////////////////////////

	pframe->SaveJob(pframe->CurrentJobNum);
	UpdateData(false);	
}

void CBottleView::OnTempPlc() 
{
	theapp->tempSelect = 1;	UpdateValues();
}

void CBottleView::OnTempUsb() 
{
	theapp->tempSelect = 2;	UpdateValues();
}

void CBottleView::UpdateValues()
{
	KillTimer(8);	SetTimer(8,500,NULL);
	UpdateDisplay();

}

void CBottleView::OnTempNone() 
{
	theapp->tempSelect = 0;	UpdateValues();
}
