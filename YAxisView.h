#if !defined(AFX_YAXISVIEW_H__2B44279A_7430_4FB5_B724_77D056E1C66E__INCLUDED_)
#define AFX_YAXISVIEW_H__2B44279A_7430_4FB5_B724_77D056E1C66E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// YAxisView.h : header file
//
#include "mainfrm.h"
//#include "VisCore.h"

//#include <pcrcam.h>
/////////////////////////////////////////////////////////////////////////////
// CYAxisView view



typedef struct  {
	int x; 
	int y; 
	int x1; 
	int y1;
	int x2; 
	int y2;} SixPoint;
/////////////////////////////////////////////////////////////////////////////
// CYAxisView view

class CYAxisView : public CView
{
	friend class CBottleApp;
	friend class CMainFrame;
	friend class View;
	friend class Modify;
	friend class Splice;
	friend class Lim;
	friend class Xtra;
	friend class CCap;
	friend class StitchSetup;
	friend class WaistInspection;
	friend class WaistInspection2;
	friend class LabelMatingSetup;
protected:
	CYAxisView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CYAxisView)

public:
/*	CVisByteImage m_image;
	// Get the image.
	const CVisImageBase& Image() const
		{ return m_image; }

	CVisImageBase& Image()
		{ return m_image; }*/
// Operations
public:
	bool toggle;
	CTabCtrl m_tabmenu;
	
	CBottleApp	*theapp;
	CMainFrame	*pframe;

	View		*m_pstats;
	Modify		*m_pmod;
	Splice		*m_psplice;
	Lim			*m_plimit;
	Xtra		*m_puser;
	CCap		*m_pcap;
	StitchSetup	*m_pstitchSetup;
	WaistInspection *m_pWaistInspection;
	WaistInspection2 *m_pWaistInspection2;
	CCapSetup1	*m_pcapSetup1;

	LabelMatingSetup *m_pLabelMatingSetup;
	int STATSTAB, MODTAB, LIMTAB, CAPTAB, SPLICETAB, TEARTAB, ALIGNTAB, WAISTTAB, WAIST2TAB;

	BYTE *imgBuf0;
	BYTE *im_result;
	
//	CICamera *cam1;
	DWORD areaDxBW;	// DX of the image area being processed
	DWORD areaDyBW;  // DY of the image area being processed
//	CAM_ATTR attrBW;

	void OnManual();

	void OnSnap();

	void LoadCam();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CYAxisView)
	public:
	virtual void OnInitialUpdate();
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CYAxisView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CYAxisView)
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_YAXISVIEW_H__2B44279A_7430_4FB5_B724_77D056E1C66E__INCLUDED_)
