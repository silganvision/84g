// XAxisView.cpp : implementation file
//

#include "stdafx.h"
#include <math.h>
#include <algorithm>

#include "Bottle.h"
#include "BottleView.h"
#include "XAxisView.h"
#include "Camera.h"

#include "mainfrm.h"
#include "modify.h"
#include "serial.h"
#include "Splice.h"
#include "LabelMatingSetup.h"
#include "CapSetup1.h"
#include <iomanip>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

char buf[4];

using namespace std;

//extern int _maxNoSleevers;

UINT ID_CAM5BUTTON= 1234;


//// MACROS TO FIND MAX AND MIN
//#define MAX(_a,_b) ((_a)<(_b)?(_b):(_a))
//#define MIN(_a,_b) ((_a)<(_b)?(_a):(_b))
//#define ABS(_a)    ((_a)<0?-(_a):(_a))
////////////////////////////////

////////////////////////////////
/*
//Just to test MEMORY USAGE

#include "windows.h"     
#include "psapi.h" 

//#pragma comment(lib, "Psapi.lib")



// Use to convert bytes to MB 
#define DIV 1048576  
// Use to convert bytes to MB //#define DIV 1024  
// Specify the width of the field in which to print the numbers.  
// The asterisk in the format specifier "%*I64d" takes an integer  
// argument and uses it to pad and right justify the number.  
#define WIDTH 7
  
*/
////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// CXAxisView

IMPLEMENT_DYNCREATE(CXAxisView, CView)

CXAxisView::CXAxisView()
{
}

CXAxisView::~CXAxisView()
{
}



BEGIN_MESSAGE_MAP(CXAxisView, CView)
	//{{AFX_MSG_MAP(CXAxisView)
	ON_WM_TIMER()
	ON_COMMAND(ID_CAM5BUTTON, DoHide)
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXAxisView drawing


/////////////////////////////////////////////////////////////////////////////
// CXAxisView diagnostics

#ifdef _DEBUG
void CXAxisView::AssertValid() const
{
	CView::AssertValid();
}

void CXAxisView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CXAxisView message handlers

void CXAxisView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
		
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pxaxis=this;
	theapp=(CBottleApp*)AfxGetApp();

	heightBy3 = MainDisplayImageHeight / 3;
	widthBy3 = MainDisplayImageWidth / 3;
	heightBy6 = MainDisplayImageHeight / 6;
	widthBy6 = MainDisplayImageWidth / 6;

	CRect buttonRect;
	buttonRect.SetRect(505,335,505+72,335+30);

	pic1=0;

	enter_OnDraw=true;
	_tempCtr=0;
	
	/////////

	picNum=0;
	colorCount=0;
	lockOut=false;
	labelTaughtDone=false;
	
	fault1=fault2=fault3=fault4=fault5=fault6=fault7=fault8=fault9=fault10=fault11=false;
	
	z1Avg=0; z2Avg=0; z3Avg=0; z4Avg=0; z5Avg=0; 
	z6Avg=0; z7Avg=0; z8Avg=0; z9Avg=0; z10Avg=0;

	LHInc=RHInc=DInc=FInc=LInc=TBInc=TB2Inc=PInc=NoInc=LCInc=TLInc=WsInc=0;

	Result1l=Result1r=Result2=Result3=Result4=Result5=Result6=Result7=Result8=Result9=Result11=Result12=0;

	LHsd=Psd=Dsd=Fsd=Lsd=TBsd=TB2sd=RHsd=Nosd=LCsd=TLsd=Wssd=0;
	LHt=Dt=Lt=Ft=TBt=TB2t=RHt=Pt=Not=LCt=TLt=Wst=0;

	piccount=0;
	reteachColor=false;
	reteachColorCap=false;

	LearnDone=false;
	TimesThru=0;
	RejectAll=false;
	first=true;
	XStart=50;
	XEnd=490;

	StartLockOut=true;

	e1Cnt=0; e2Cnt=0; e3Cnt=0; e4Cnt=0; e5Cnt=0; 
	e6Cnt=0; e7Cnt=0; e8Cnt=0; e9Cnt=0; e10Cnt=0;

	E1Cnt=0; E2Cnt=0; E3Cnt=0; E4Cnt=0; E5Cnt=0; 
	E6Cnt=0; E7Cnt=0; E8Cnt=0; E9Cnt=0; E10Cnt=0;

	E1Cntx=0; E2Cntx=0; E3Cntx=0; E4Cntx=0; E5Cntx=0; 
	E6Cntx=0; E7Cntx=0;	E8Cntx=0; E9Cntx=0; E10Cntx=0;

	E1Cnt24=0; E2Cnt24=0; E3Cnt24=0; E4Cnt24=0; E5Cnt24=0;
	E6Cnt24=0; E7Cnt24=0; E8Cnt24=0; E9Cnt24=0; E10Cnt24=0;

	//im_trouble		= (BYTE*)GlobalAlloc(GMEM_FIXED, 100* 32 *4);
	
	im_trouble		= (BYTE*)malloc(100* 32 *4);
	im_patl2		= (BYTE*)malloc(48 * 48 *4);
	im_color2		= (BYTE*)malloc(100* 32 *4);
	im_patr2x		= (BYTE*)malloc(48 * 48 *4);
	im_patr2		= (BYTE*)malloc(70 * 50 *4);
	im_pat			= (BYTE*)malloc(48 * 48 *4);	//pattern 12x12
	im_pat3			= (BYTE*)malloc(48 * 48 *4);
	im_thresh		= (BYTE*)malloc(112* 270*4);

	int totBytes = 0;
	totBytes = 100*32*4+ 48*48*4+100*32*4+48*48*4+70*50*4+48*48*4+48*48*4+112*270*4;

	int fact=4;

	C1LengthBWDiv3	= CroppedCameraHeight/3;	C1HeightBWDiv3	= CroppedCameraWidth/3;
	C1LengthBWDiv6	= CroppedCameraHeight/6;	C1HeightBWDiv6	= CroppedCameraWidth/6;
	C1LengthBWDiv12	= CroppedCameraHeight/12;	C1HeightBWDiv12	= CroppedCameraWidth/12;

	int i;
	/////////////////////////
	//Method5
	for(i=1; i<=4; i++)
	{
		bxMethod5_Xt[i] = 0;
		bxMethod5_Xb[i] = 0;
	}


	/////////////////////////
	int fac=1;

	/*
	for(i=1; i<=4; i++)
	{
		fac = 3;
		widthby3[i]		= theapp->camHeight/fac;
		heighby3[i]		= theapp->camWidth[i]/fac;

		fac = 6;
		widthby6[i]		= theapp->camHeight[i]/fac;
		heighby6[i]		= theapp->camWidth[i]/fac;
	}
	*/

	/////////////////////////

	int width, height;
	//int i;

	i=1; width=CroppedCameraWidth; height=CroppedCameraHeight;
	im_reduce1		= (BYTE*)malloc(width*height*4);totBytes += (width*height*4);
	img1SatDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img1Sa2Div3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cambw1		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img1MagDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img1YelDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img1CyaDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam1Bbw		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam1Gbw		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam1Rbw		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam1bwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img1MagDiv6		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img1YelDiv6		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img1CyaDiv6		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam1RbwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam1GbwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam1BbwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);

//	img_cam1Ligh	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img_cam1Dark	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img_cam1Edge	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	im1LighSeq	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	im1DarkSeq	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	im1EdgeSeq	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img1Edg	= (BYTE*)malloc(width*height);	totBytes += (width*height);

//	img1MiddBottle	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img1ForNoLabelDiv3	= (BYTE*)malloc(width*height);	totBytes += (width*height);

	//img1ForSpliceDiv3	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	
	im_cam1EdgH		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam1EdgV		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam1Bin		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	byteBin1Edges	= (BYTE*)malloc(width*height);	totBytes += (width*height);

	byteBinMet5C1	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	byteBinMet5C2	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	byteBinMet5C3	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	byteBinMet5C4	= (BYTE*)malloc(width*height);	totBytes += (width*height);

//	img1ForShiftedDiv3	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img1ForShiftedDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img1ForPattMatchDiv3	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img1ForPattMatchDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);

	i=2;
	im_reduce2		= (BYTE*)malloc(width*height*4);totBytes += (width*height*4);
	img2SatDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img2Sa2Div3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cambw2		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img2MagDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img2YelDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img2CyaDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam2Bbw		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam2Gbw		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam2Rbw		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam2bwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img2MagDiv6		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img2YelDiv6		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img2CyaDiv6		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam2RbwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam2GbwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam2BbwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);	
//	img_cam2Ligh	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img_cam2Dark	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img_cam2Edge	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	im2LighSeq	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	im2DarkSeq	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	im2EdgeSeq	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img2Edg	= (BYTE*)malloc(width*height);	totBytes += (width*height);

//	img2MiddBottle	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img2ForNoLabelDiv3	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	//img2ForSpliceDiv3	= (BYTE*)malloc(width*height);	
	totBytes += (width*height);
	im_cam2EdgH	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam2EdgV	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam2Bin	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	byteBin2Edges	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img2ForShiftedDiv3	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img2ForShiftedDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img2ForPattMatchDiv3	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img2ForPattMatchDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);


	i=3;
	im_reduce3		= (BYTE*)malloc(width*height*4);totBytes += (width*height*4);
	img3SatDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img3Sa2Div3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cambw3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img3MagDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img3YelDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img3CyaDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam3Bbw		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam3Gbw		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam3Rbw		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam3bwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img3MagDiv6		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img3YelDiv6		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img3CyaDiv6		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam3RbwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam3GbwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam3BbwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img_cam3Ligh	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img_cam3Dark	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img_cam3Edge	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	im3LighSeq	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	im3DarkSeq	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	im3EdgeSeq	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img3Edg	= (BYTE*)malloc(width*height);	totBytes += (width*height);

//	img3MiddBottle	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img3ForNoLabelDiv3	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	//img3ForSpliceDiv3	= (BYTE*)malloc(width*height);	
	totBytes += (width*height);
	im_cam3EdgH	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam3EdgV	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam3Bin	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	byteBin3Edges	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img3ForShiftedDiv3	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img3ForShiftedDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img3ForPattMatchDiv3	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img3ForPattMatchDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);


	i=4;
	im_reduce4		= (BYTE*)malloc(width*height*4);totBytes += (width*height*4);
	img4SatDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img4Sa2Div3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cambw4		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img4MagDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img4YelDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img4CyaDiv3		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam4Bbw		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam4Gbw		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam4Rbw		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam4bwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img4MagDiv6		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img4YelDiv6		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img4CyaDiv6		= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam4RbwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam4GbwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam4BbwDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img_cam4Ligh	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img_cam4Dark	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img_cam4Edge	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	im4LighSeq	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	im4DarkSeq	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	im4EdgeSeq	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	img4Edg	= (BYTE*)malloc(width*height);	totBytes += (width*height);

//	img4MiddBottle	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img4ForNoLabelDiv3	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	//img4ForSpliceDiv3	= (BYTE*)malloc(width*height);	
	totBytes += (width*height);
	im_cam4EdgH	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam4EdgV	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	im_cam4Bin	= (BYTE*)malloc(width*height);	totBytes += (width*height);
	byteBin4Edges	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img4ForShiftedDiv3	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img4ForShiftedDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img4ForPattMatchDiv3	= (BYTE*)malloc(width*height);	totBytes += (width*height);
//	img4ForPattMatchDiv6	= (BYTE*)malloc(width*height);	totBytes += (width*height);

	i=1;
	img1SatDiv6 = (BYTE*)malloc(width*height);	totBytes += (width*height);
	
	i=2;
	img2SatDiv6 = (BYTE*)malloc(width*height);	totBytes += (width*height);
	
	i=3;
	img3SatDiv6 = (BYTE*)malloc(width*height);
	totBytes += (width*height);
	
	i=4;
	img4SatDiv6 = (BYTE*)malloc(width*height );	totBytes += (width*height);

	i=5;

	im_reduce5	= (BYTE*)malloc(width*height*4);	totBytes += (width*height*4);
	im_cambw5	= (BYTE*)malloc(width*height);		totBytes += (width*height);
	im_cam5Bbw	= (BYTE*)malloc(width*height);		totBytes += (width*height);
	im_cam5Gbw	= (BYTE*)malloc(width*height);		totBytes += (width*height);
	im_cam5Rbw	= (BYTE*)malloc(width*height);		totBytes += (width*height);	

	im_matchl		=	(BYTE*)malloc( 60 * 50 );
	totBytes += ( 60 * 50 );

	im_matchr		=	(BYTE*)malloc( 60 * 50 );
	totBytes += ( 60 * 50 );

	im_match2		=	(BYTE*)malloc( 60 * 50 );
	totBytes += ( 60 * 50 );

	im_mat			=	(BYTE*)malloc( 20 * 20 );
	totBytes += ( 20 * 20 );

	im_mat2			=	(BYTE*)malloc( theapp->sizepattBy3_2W * theapp->sizepattBy3_2H );
	totBytes += ( theapp->sizepattBy3_2W * theapp->sizepattBy3_2H );

	im_match3		=	(BYTE*)malloc( 60 * 50 );
	totBytes += ( 60 * 50);

	im_mat3			=	(BYTE*)malloc( 20 * 20 );
	totBytes += ( 20 * 20);


	imgBuf0			=	(BYTE*)malloc(MainDisplayColorImageSize);
	totBytes += (MainDisplayImageHeight * MainDisplayImageWidth * 4);

	im_patshow		=	(BYTE*)malloc( 100 * 100 * 4 );
	totBytes += (100 * 100 * 4);
	
	img1LargeBuff		=	(BYTE*)malloc(FullImageScaledRotatedWidth*FullImageScaledRotatedHeight*bytesPerPixel);
	img2LargeBuff		=	(BYTE*)malloc(FullImageScaledRotatedWidth*FullImageScaledRotatedHeight*bytesPerPixel);
	img3LargeBuff		=	(BYTE*)malloc(FullImageScaledRotatedWidth*FullImageScaledRotatedHeight*bytesPerPixel);
	img4LargeBuff		=	(BYTE*)malloc(FullImageScaledRotatedWidth*FullImageScaledRotatedHeight*bytesPerPixel);

	CVisRGBAByteImage image1(CroppedCameraHeight, CroppedCameraWidth,	1, -1, imgBuf0);
	CVisRGBAByteImage image2(CroppedCameraHeight, CroppedCameraWidth,	1, -1, imgBuf0);
	CVisRGBAByteImage image3(CroppedCameraHeight, CroppedCameraWidth,	1, -1, imgBuf0);
	CVisRGBAByteImage image4(CroppedCameraHeight, CroppedCameraWidth,	1, -1, imgBuf0);
	CVisRGBAByteImage image5(CroppedCameraWidth, CroppedCameraHeight,	1, -1, imgBuf0);

	CVisRGBAByteImage image1Large(FullImageScaledRotatedWidth, FullImageScaledRotatedHeight, 1, -1, img1LargeBuff);
	CVisRGBAByteImage image2Large(FullImageScaledRotatedWidth, FullImageScaledRotatedHeight, 1, -1, img2LargeBuff);
	CVisRGBAByteImage image3Large(FullImageScaledRotatedWidth, FullImageScaledRotatedHeight, 1, -1, img3LargeBuff);
	CVisRGBAByteImage image4Large(FullImageScaledRotatedWidth, FullImageScaledRotatedHeight, 1, -1, img4LargeBuff);

	totBytes += 4 * MainDisplayColorImageSize;
	totBytes += 1 * MainDisplayColorImageSize;

	imageC			=	image1;
	imageC2			=	image2;
	imageC3			=	image3;
	imageC4			=	image4;
	imageC5			=	image5;

	imageC1Large = image1Large;
	imageC2Large = image2Large;
	imageC3Large = image3Large;
	imageC4Large = image4Large;
	////////////

	imgBuf0bw =	(BYTE*)malloc(MainDisplayBWImageSize);
	totBytes += (MainDisplayBWImageSize);

	CVisByteImage image1bw(CroppedCameraHeight,CroppedCameraWidth, 1,-1,imgBuf0bw);
	CVisByteImage image2bw(CroppedCameraHeight,CroppedCameraWidth, 1,-1,imgBuf0bw);
	CVisByteImage image3bw(CroppedCameraHeight,CroppedCameraWidth, 1,-1,imgBuf0bw);
	CVisByteImage image4bw(CroppedCameraHeight,CroppedCameraWidth, 1,-1,imgBuf0bw);

	for(i=1; i<=4; i++)
	{
		imageYel[i]	= image1bw;
		imageMag[i]	= image1bw;
		imageCya[i]	= image1bw;
		imageBW[i]	= image1bw;
		imageBWB[i]	= image1bw;
		imageBWG[i]	= image1bw;
		imageBWR[i]	= image1bw;
		imageSat[i]	= image1bw;
		imageSa2[i]	= image1bw;
		imageSobH[i]= image1bw;
		imageSobV[i]= image1bw;
		imageBin[i]= image1bw;
		
		imageBinEdges[i]= image1bw;
		imageBinMeth5[i]= image1bw;
	}
	
	/////////////////////

	imgBuf5bw =	(BYTE*)malloc(MainDisplayBWImageSize);
	totBytes += (MainDisplayBWImageSize);

	CVisByteImage image5bw(CroppedCameraWidth, CroppedCameraHeight, 1,-1,imgBuf5bw);
	
	imageBW[5]	= image5bw;
	imageBWB[5]	= image5bw;
	imageBWG[5]	= image5bw;
	imageBWR[5]	= image5bw;

	////////////

	xdistPatt = 5;
	teachingMiddle=false;

	////////////////////////////////

	/////////////////////////////////
	//Stitch variables

	imgStitchedLigh	= (BYTE*)malloc(MainDisplayColorImageSize / 9);
	totBytes += (MainDisplayColorImageSize / 9);

	imgStitchedDark	= (BYTE*)malloc(MainDisplayColorImageSize / 9);
	totBytes += (MainDisplayColorImageSize / 9);

	imgStitchedEdg	= (BYTE*)malloc(MainDisplayColorImageSize / 9);
	totBytes += (MainDisplayColorImageSize / 9);

	CVisByteImage imgstlig2(bytesPerPixel * CroppedCameraHeight/3,CroppedCameraWidth/3, 1, -1, imgStitchedLigh);
	imageStitchedLigh= imgstlig2;

	CVisByteImage imgstdar2(bytesPerPixel * CroppedCameraHeight/3,CroppedCameraWidth/3, 1, -1, imgStitchedDark);
	imageStitchedDark= imgstdar2;

	CVisByteImage imgstedg2(bytesPerPixel * CroppedCameraHeight/3,CroppedCameraWidth/3, 1, -1, imgStitchedEdg);
	imageStitchedEdg= imgstedg2;

	/////////////////////////////////

	imgStitchedFilLigh	= (BYTE*)malloc(MainDisplayColorImageSize / 9);
	totBytes += (MainDisplayColorImageSize / 9);

	imgStitchedFilDark	= (BYTE*)malloc(MainDisplayColorImageSize / 9);
	totBytes += (MainDisplayColorImageSize / 9);

	imgStitchedFilEdg	= (BYTE*)malloc(MainDisplayColorImageSize / 9);
	totBytes += (MainDisplayColorImageSize / 9);

	imgStitchedFilFinal	= (BYTE*)malloc(MainDisplayColorImageSize / 9);
	totBytes += (MainDisplayColorImageSize / 9);

	imgStitchedBW		= (BYTE*)malloc(MainDisplayColorImageSize / 9);
	totBytes += (MainDisplayColorImageSize / 9);


	CVisByteImage imgstlig(bytesPerPixel * CroppedCameraHeight / 3, CroppedCameraWidth / 3, 1, -1, imgStitchedFilLigh);
	imageStitchedFilLigh= imgstlig;

	CVisByteImage imgstdar(bytesPerPixel * CroppedCameraHeight / 3, CroppedCameraWidth / 3, 1, -1, imgStitchedFilDark);
	imageStitchedFilDark= imgstdar;

	CVisByteImage imgstedg(bytesPerPixel * CroppedCameraHeight / 3, CroppedCameraWidth / 3, 1, -1, imgStitchedFilEdg);
	imageStitchedFilEdg= imgstedg;

	CVisByteImage imgstfin(bytesPerPixel * CroppedCameraHeight / 3, CroppedCameraWidth / 3, 1, -1, imgStitchedFilFinal);
	imageStitchedFilFinal= imgstfin;
	
	CVisByteImage imgstbw1(bytesPerPixel * CroppedCameraHeight / 3, CroppedCameraWidth / 3, 1, -1, imgStitchedBW);
	imageStitchedbw	= imgstbw1;

	/////////////////////////////////

	sizepattBy12	= theapp->sizepattBy12;
	sizepattBy6		= theapp->sizepattBy6;
	sizepattBy3W	= theapp->sizepattBy3_2W;
	sizepattBy3H	= theapp->sizepattBy3_2H;

	for(i=1; i<=3; i++)
	{
		bytePattBy6[i]	=	(BYTE*)malloc( sizepattBy6*sizepattBy6);
		totBytes += sizepattBy6*sizepattBy6;

		bytePattBy3[i]	=	(BYTE*)malloc( sizepattBy3W*sizepattBy3H);
		totBytes += sizepattBy3W*sizepattBy3H;
	}

	//## 2 new pointers to store patterns taught For Method 6
	bytePattBy6_method6_1	=	(BYTE*)malloc( sizepattBy6*sizepattBy6);  //Pattern number 1 for Method 6
	bytePattBy3_method6_1	=	(BYTE*)malloc( sizepattBy3W*sizepattBy3H);
	
	bytePattBy6_method6	=	(BYTE*)malloc( sizepattBy6*sizepattBy6);  //Pattern number 2 for Method 6
	bytePattBy3_method6	=	(BYTE*)malloc( sizepattBy3W*sizepattBy3H);
	
	bytePattBy6_method6_num3	=	(BYTE*)malloc( sizepattBy6*sizepattBy6); //Pattern number 3 for Method 6
	bytePattBy3_method6_num3	=	(BYTE*)malloc( sizepattBy3W*sizepattBy3H);
	
	//for(int pattType=0; pattType<3; pattType++)
	//{
	//	bytePattBy6[pattType] = (BYTE*)malloc(sizepattBy6*sizepattBy6);
	//	bytePattBy3[pattType] = (BYTE*)malloc(sizepattBy3W*sizepattBy3H);
	//}
	
	wehaveTempX=false;

	for(i=1; i<=5; i++)
	{countTeachingPics[i] = 0;}

	//*** Picture Log

	for(i=1; i<=4; i++)
	{w[i] = CroppedCameraHeight/3;	h[i] = CroppedCameraWidth/3;}

	w[5] = CroppedCameraWidth/2;	h[5] = CroppedCameraHeight/2;



/*	imgBuf3RGB_cam1	=	(BYTE*)malloc(w[1]*h[1]*4*MAXNUMDEFPIC);
	totBytes += (w[1]*h[1]*4*MAXNUMDEFPIC);

	imgBuf3RGB_cam2	=	(BYTE*)malloc(w[2]*h[2]*4*MAXNUMDEFPIC);
	totBytes += (w[2]*h[2]*4*MAXNUMDEFPIC);

	imgBuf3RGB_cam3	=	(BYTE*)malloc(w[3]*h[3]*4*MAXNUMDEFPIC);
	totBytes += (w[3]*h[3]*4*MAXNUMDEFPIC);

	imgBuf3RGB_cam4	=	(BYTE*)malloc(w[4]*h[4]*4*MAXNUMDEFPIC);
	totBytes += (w[4]*h[4]*4*MAXNUMDEFPIC);

	imgBuf3RGB_cam5	=	(BYTE*)malloc(w[5]*h[5]*4*MAXNUMDEFPIC);
	totBytes += (w[5]*h[5]*4*MAXNUMDEFPIC);
*/

	imgBuf3RGB_cam1	=	(BYTE*)GlobalAlloc(GMEM_FIXED, w[1]*h[1]*4*MAXNUMDEFPIC );
	imgBuf3RGB_cam2	=	(BYTE*)GlobalAlloc(GMEM_FIXED, w[2]*h[2]*4*MAXNUMDEFPIC );
	imgBuf3RGB_cam3	=	(BYTE*)GlobalAlloc(GMEM_FIXED, w[3]*h[3]*4*MAXNUMDEFPIC );
	imgBuf3RGB_cam4	=	(BYTE*)GlobalAlloc(GMEM_FIXED, w[4]*h[4]*4*MAXNUMDEFPIC );
	imgBuf3RGB_cam5	=	(BYTE*)GlobalAlloc(GMEM_FIXED, w[5]*h[5]*4*MAXNUMDEFPIC );
	totBytes += 5*(w[1]*h[1]*4*MAXNUMDEFPIC);


	arr1[0]			=	imgBuf3RGB_cam1;
	arr1[1]			=	imgBuf3RGB_cam2;
	arr1[2]			=	imgBuf3RGB_cam3;
	arr1[3]			=	imgBuf3RGB_cam4;
	arr1[4]			=	imgBuf3RGB_cam5;

	typofault=0;showostore=0;imge_no=0;

	for(int i1=0;i1<MAXNUMDEFPIC;i1++)
	{
		for(int j=0;j<=3;j++) {tim_stamp[i1][j]=0;}
	}

	for(i=0; i<190; i++)
	{
		cameraDone[1][i] = false;
		cameraDone[2][i] = false;
		cameraDone[3][i] = false;
		cameraDone[4][i] = false;
		cameraDone[5][i] = false;
	}

	/////////////////////////////////
/*
	//Measuring memory usage

	MEMORYSTATUSEX statex;    
	statex.dwLength = sizeof (statex);   
	GlobalMemoryStatusEx (&statex);     
	_tprintf (TEXT("There is  %*ld percent of memory in use.\n"),WIDTH, statex.dwMemoryLoad);   
	_tprintf (TEXT("There are %*I64d total Mbytes of physical memory.\n"),WIDTH,statex.ullTotalPhys/DIV);   
	_tprintf (TEXT("There are %*I64d free Mbytes of physical memory.\n"),WIDTH, statex.ullAvailPhys/DIV);   
	_tprintf (TEXT("There are %*I64d total Mbytes of paging file.\n"),WIDTH, statex.ullTotalPageFile/DIV);   
	_tprintf (TEXT("There are %*I64d free Mbytes of paging file.\n"),WIDTH, statex.ullAvailPageFile/DIV);   
	_tprintf (TEXT("There are %*I64d total Mbytes of virtual memory.\n"),WIDTH, statex.ullTotalVirtual/DIV);   
	_tprintf (TEXT("There are %*I64d free Mbytes of virtual memory.\n"),WIDTH, statex.ullAvailVirtual/DIV);   
	_tprintf (TEXT("There are %*I64d free Mbytes of extended memory.\n"),WIDTH, statex.ullAvailExtendedVirtual/DIV); 

*/
	/////////////////////////////////

}

//Method to do inspections of camera1, other InpectX are for the other cameras
//but they are the same as Inspect1
TwoPoint CXAxisView::Inspect1(BYTE *im_color, BYTE *im_src1 )
{
	TwoPoint BotPos; int ncam=1;
	BotPos.x = 0;
	BotPos.x1 = 0;
	BotPos.y = 0;
	BotPos.y1 = 0;
	///////////////////////////////////////////

	//Finding Middle of the Bottle
	if(theapp->jobinfo[pframe->CurrentJobNum].findMiddles)//find middle of the bottle
	{ int fact = 3; findBottleInPicC1(ncam, fact); }
	else //or just take middle of the image
	{
		bottleXL[ncam]	= 0 + 30;
		bottleXR[ncam]	= CroppedCameraHeight - 30; 
		xMidd[ncam]		= CroppedCameraHeight/2;
	}

	///////////////////////////////////////////
	//image selection for the next method to find the reference of the bottle height, 
	//it finds this by pattern matching or edge detection
	int filterSelPatt =	theapp->jobinfo[pframe->CurrentJobNum].filterSelPatt;

	if(filterSelPatt==1)		{img1ForPattMatchDiv3 = im_cambw1;		img1ForPattMatchDiv6 =	im_cam1bwDiv6;}
	else if(filterSelPatt==2)	{img1ForPattMatchDiv3 = img1SatDiv3;	img1ForPattMatchDiv6 = img1SatDiv6;}
	else if(filterSelPatt==3)	{img1ForPattMatchDiv3 = img1MagDiv3;	img1ForPattMatchDiv6 = img1MagDiv6;}
	else if(filterSelPatt==4)	{img1ForPattMatchDiv3 = img1YelDiv3;	img1ForPattMatchDiv6 = img1YelDiv6;}
	else if(filterSelPatt==5)	{img1ForPattMatchDiv3 = img1CyaDiv3;	img1ForPattMatchDiv6 = img1CyaDiv6;}
	else if(filterSelPatt==6)	{img1ForPattMatchDiv3 = im_cam1Rbw;		img1ForPattMatchDiv6 = im_cam1RbwDiv6;}
	else if(filterSelPatt==7)	{img1ForPattMatchDiv3 = im_cam1Gbw;		img1ForPattMatchDiv6 = im_cam1GbwDiv6;}
	else if(filterSelPatt==8)	{img1ForPattMatchDiv3 = im_cam1Bbw;		img1ForPattMatchDiv6 = im_cam1BbwDiv6;}

	QueryPerformanceFrequency(&m_lnC1FreqI7); QueryPerformanceCounter(&m_lnC1StartI7); //Timing

	//-------------LABEL INTEGRITY ------------------
	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod1)		// METHOD 1
	{	BotPos = FindPatternX1_C1(img1ForPattMatchDiv3, ncam);	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2)	//METHOD 2        //Pattern Matching at different Scales
	{
		if(wehaveTempX)
		{
			int imgFac; int pattType = 1; 
			imgFac = 6;
			//Appending 12-15-2016
			///##
		//	bytePattBy6[pattType]=bytePattBy6_method6_1;//bytePattBy6[pattType]
		//	bytePattBy3[pattType]=bytePattBy3_method6_1;
			///##
		
			pattLeftPosBy6[ncam] = FindPattA1_C1(img1ForPattMatchDiv6, bytePattBy6_method6_1, ncam, imgFac, pattType);  //First pattern match with smaller resolution template

			imgFac = 3;
			pattLeftPosBy3[ncam] = FindPattHigherRes_C1(img1ForPattMatchDiv3, bytePattBy3_method6_1, ncam, imgFac, pattLeftPosBy6[ncam]);

			BotPos.x = pattLeftPosBy3[ncam].x/3; 
			BotPos.y = pattLeftPosBy3[ncam].y/3; 
			BotPos.x1= pattLeftPosBy6[ncam].c;
		}
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3)	//METHOD 3   //edge + area under the edge
	{
		int imgFac; int pattType = 1; 

		imgFac = 3;

		pattLeftPosBy3[ncam] = FindLabelEdge_C1b(img1ForPattMatchDiv3, ncam, imgFac, pattLeftPosBy6[ncam]);

		BotPos.x = pattLeftPosBy3[ncam].x/3; 
		BotPos.y = pattLeftPosBy3[ncam].y/3; 
		BotPos.x1= pattLeftPosBy3[ncam].c;
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4)	//METHOD 4   //search edges on a band
	{
		int imgFac; int pattType = 1; 

		imgFac = 3;
		pattLeftPosBy3[ncam] = FindWhiteBand_C1(img1ForPattMatchDiv3, bytePattBy3[pattType], ncam, imgFac, pattLeftPosBy6[ncam]);
			
		BotPos.x = pattLeftPosBy3[ncam].x/3; 
		BotPos.y = pattLeftPosBy3[ncam].y/3; 
		BotPos.x1= pattLeftPosBy3[ncam].c;
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5) //edge + area under the edge but automatic... this has not been tested yet
	{
		int fact = 3; 
		int width = CroppedCameraHeight/3; int height = CroppedCameraWidth/3;

		int tmpW	= theapp->jobinfo[pframe->CurrentJobNum].boxWt;
		
		int tmpHt	= theapp->jobinfo[pframe->CurrentJobNum].boxHt;
		int tmpYt	= theapp->jobinfo[pframe->CurrentJobNum].boxYt;

		int tmpHb	= theapp->jobinfo[pframe->CurrentJobNum].boxHb;
		int tmpYb	= theapp->jobinfo[pframe->CurrentJobNum].boxYb;

		bxMethod5_Xt[ncam] = bxMethod5_Xb[ncam] = xMidd[ncam] - tmpW/2;
		if(bxMethod5_Xt[ncam]<0)	{bxMethod5_Xt[ncam] = bxMethod5_Xb[ncam] = 0;}

		//Binarized automatically, images come from filter selected on Insp dlg box
		createHistAndBinarizeC1(	im_cam1Rbw, im_cam1Gbw, im_cam1Bbw,
									img1ForPattMatchDiv3,
									byteBinMet5C1,
									ncam,
									bxMethod5_Xt[ncam], tmpYt, tmpHt,
									bxMethod5_Xb[ncam], tmpYb, tmpHb,
									tmpW,fact);
			
		width = MainDisplayImageWidth/3; height = MainDisplayImageHeight/3;
		CVisByteImage imgbinEdg(width,height,1,-1,  byteBinMet5C1); imageBinMeth5[ncam] = imgbinEdg;

		int limYtop = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt;
		int limYbot = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt;
		int limX	= bxMethod5_Xt[ncam];
		int limW	= tmpW;

		//Now find the edge with the binary image
		labelPos[ncam].x = 0; labelPos[ncam].y = 0; labelPos[ncam].x1 = 100;
		BotPos = FindLabelEdge_Method5C1(byteBinMet5C1, ncam, fact, limYtop, limYbot,limX,limW);
		BotPos.x1 = 100;
	}
	else if( theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 )  //Method 6
	{
		if(wehaveTempX)
		{
			int imgFac; int pattType = 1;
			//	1st Pattern Match
			imgFac = 6;
			pattLeftPosBy6_method6_1[ncam]  = FindPattA1_C1(img1ForPattMatchDiv6, bytePattBy6_method6_1, ncam, imgFac, pattType);  //First pattern match with smaller resolution template

			imgFac = 3;
			pattLeftPosBy3_method6_1[ncam] = FindPattHigherRes_C1(img1ForPattMatchDiv3, bytePattBy3_method6_1, ncam, imgFac, pattLeftPosBy6_method6_1[ncam]);

			TRACE( "I1: New Patt cor %f \n\n",pattLeftPosBy6_method6[ncam].c);
			/*
			imgFac = 6;
			pattLeftPosBy6[ncam] = FindPattA1_C1(img1ForPattMatchDiv6, bytePattBy6[pattType], ncam, imgFac, pattType);  //First pattern match with smaller resolution template

			imgFac = 3;
			pattLeftPosBy3[ncam] = FindPattHigherRes_C1(img1ForPattMatchDiv3, bytePattBy3[pattType], ncam, imgFac, pattLeftPosBy6[ncam]);
			TRACE( "I1: Orig Patt cor %f \n",pattLeftPosBy6[ncam].c);
			*/

			pattType = 2;
			//Adding 2nd pattern match
			imgFac = 6;
			pattLeftPosBy6_method6[ncam]  = FindPattA1_C1(img1ForPattMatchDiv6, bytePattBy6_method6, ncam, imgFac, pattType);  //First pattern match with smaller resolution template

			imgFac = 3;
			pattLeftPosBy3_method6[ncam] = FindPattHigherRes_C1(img1ForPattMatchDiv3, bytePattBy3_method6, ncam, imgFac, pattLeftPosBy6_method6[ncam]);

			TRACE( "I1: New Patt cor %f \n\n",pattLeftPosBy6_method6[ncam].c);			
			
			//Adding 3rd pattern match
			pattType = 3;
			imgFac = 6;
			pattLeftPosBy6_method6_num3[ncam]  = FindPattA1_C1(img1ForPattMatchDiv6, bytePattBy6_method6_num3, ncam, imgFac, pattType);  //First pattern match with smaller resolution template

			imgFac = 3;
			pattLeftPosBy3_method6_num3[ncam] = FindPattHigherRes_C1(img1ForPattMatchDiv3, bytePattBy3_method6_num3, ncam, imgFac, pattLeftPosBy6_method6_num3[ncam]);

			TRACE( "I1: New Patt cor %f \n\n",pattLeftPosBy6_method6_num3[ncam].c);	

			//if orig pattern is returning a better corelation
			if((pattLeftPosBy3_method6_1[ncam].c >= pattLeftPosBy6_method6[ncam].c) &&
				(pattLeftPosBy3_method6_1[ncam].c >= pattLeftPosBy6_method6_num3[ncam].c))
			{
				BotPos.x = pattLeftPosBy3_method6_1[ncam].x/3; 
				BotPos.y = pattLeftPosBy3_method6_1[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6_method6_1[ncam].c;
				/*
				BotPos.x = pattLeftPosBy3[ncam].x/3; 
				BotPos.y = pattLeftPosBy3[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6[ncam].c;
				*/
			}
			else if((pattLeftPosBy6_method6[ncam].c >= pattLeftPosBy3_method6_1[ncam].c) &&
				(pattLeftPosBy6_method6[ncam].c >= pattLeftPosBy6_method6_num3[ncam].c)  )//if new pattern is returning a better correlation
			{
				BotPos.x = pattLeftPosBy3_method6[ncam].x/3; 
				BotPos.y = pattLeftPosBy3_method6[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6_method6[ncam].c;
			}
			else if((pattLeftPosBy6_method6_num3[ncam].c >= pattLeftPosBy3_method6_1[ncam].c ) &&
				(pattLeftPosBy6_method6_num3[ncam].c >= pattLeftPosBy6_method6[ncam].c))
			{
				BotPos.x = pattLeftPosBy3_method6_num3[ncam].x/3; 
				BotPos.y = pattLeftPosBy3_method6_num3[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6_method6_num3[ncam].c;
			}
		}
	}

	QueryPerformanceCounter(&m_lnC1EndI7); nDiff = 0; dMicroSecondC1I7 = 0;	//Time
	if( m_lnC1StartI7.QuadPart !=0 && m_lnC1EndI7.QuadPart != 0)
	{
		dMicroSecondC1I7	= (double)(m_lnC1FreqI7.QuadPart/1000); 
		nDiff = m_lnC1EndI7.QuadPart-m_lnC1StartI7.QuadPart; dMicroSecondC1I7 = nDiff/dMicroSecondC1I7;
	}
		
	ComposeCam1WaistInspection();

	///////////////////////////////////////////

	//*** Inspection 6 ... only for square bottles or bottleStyle==10
	//see dlg box IDD_INSP and radio button Square Bottle
	//we need to find the edges of the open area on the middle of the square bottle
	QueryPerformanceFrequency(&m_lnC1FreqI6); QueryPerformanceCounter(&m_lnC1StartI6); //Timing

	xShift[ncam] = 0;
	yShift[ncam] = theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam] + 15;
	hShift[ncam] =	(theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam] - 
						theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]) - 30;
	int fact = 3;

	if(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle==10) //Lipton - Jeremy in Boston
	{findShiftedEdges7_C1(fact, ncam);}
	
	QueryPerformanceCounter(&m_lnC1EndI6); nDiff = 0; dMicroSecondC1I6 = 0;	//Time
	if( m_lnC1StartI6.QuadPart !=0 && m_lnC1EndI6.QuadPart != 0)
	{
		dMicroSecondC1I6	= (double)(m_lnC1FreqI6.QuadPart/1000); 
		nDiff = m_lnC1EndI6.QuadPart-m_lnC1StartI6.QuadPart; dMicroSecondC1I6 = nDiff/dMicroSecondC1I6;
	}

	return BotPos;	
}

TwoPoint CXAxisView::Inspect2(BYTE *im_color,BYTE *im_src2)
{
	TwoPoint BotPos; int ncam=2;
	BotPos.x = 0;
	BotPos.x1 = 0;
	BotPos.y = 0;
	BotPos.y1 = 0;

	///////////////////////////////////////////

	//Finding Middle of the Bottle
	if(theapp->jobinfo[pframe->CurrentJobNum].findMiddles)
	{int fact = 3; findBottleInPicC2(ncam, fact);}
	else
	{
		bottleXL[ncam]	= 0 + 30;
		bottleXR[ncam]	= MainDisplayImageWidth - 30; 
		xMidd[ncam]		= MainDisplayImageWidth/2;
	}

	///////////////////////////////////////////

	int filterSelPatt =	theapp->jobinfo[pframe->CurrentJobNum].filterSelPatt;

	if(filterSelPatt==1)		{img2ForPattMatchDiv3 = im_cambw2;		img2ForPattMatchDiv6 =	im_cam2bwDiv6;}
	else if(filterSelPatt==2)	{img2ForPattMatchDiv3 = img2SatDiv3;	img2ForPattMatchDiv6 = img2SatDiv6;}
	else if(filterSelPatt==3)	{img2ForPattMatchDiv3 = img2MagDiv3;	img2ForPattMatchDiv6 = img2MagDiv6;}
	else if(filterSelPatt==4)	{img2ForPattMatchDiv3 = img2YelDiv3;	img2ForPattMatchDiv6 = img2YelDiv6;}
	else if(filterSelPatt==5)	{img2ForPattMatchDiv3 = img2CyaDiv3;	img2ForPattMatchDiv6 = img2CyaDiv6;}
	else if(filterSelPatt==6)	{img2ForPattMatchDiv3 = im_cam2Rbw;	img2ForPattMatchDiv6 = im_cam2RbwDiv6;}
	else if(filterSelPatt==7)	{img2ForPattMatchDiv3 = im_cam2Gbw;	img2ForPattMatchDiv6 = im_cam2GbwDiv6;}
	else if(filterSelPatt==8)	{img2ForPattMatchDiv3 = im_cam2Bbw;	img2ForPattMatchDiv6 = im_cam2BbwDiv6;}

	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod1)
	{	BotPos = FindPatternX1_C2(img2ForPattMatchDiv3, ncam);	}
	// Pattern Matching at different Scales
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2)
	{
		if(wehaveTempX)
		{
			int imgFac;	int pattType=1;
			
			imgFac = 6;
				//Appending 12-15-2016
			///##
		//	bytePattBy6[pattType]=bytePattBy6_method6_1;
		//	bytePattBy3[pattType]=bytePattBy3_method6_1;
			///##
			pattLeftPosBy6[ncam] = FindPattA1_C2(img2ForPattMatchDiv6, bytePattBy6_method6_1, ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3[ncam] = FindPattHigherRes_C2(img2ForPattMatchDiv3, bytePattBy3_method6_1, ncam, imgFac, pattLeftPosBy6[ncam]);

			BotPos.x = pattLeftPosBy3[ncam].x/3; 
			BotPos.y = pattLeftPosBy3[ncam].y/3; 
			BotPos.x1= pattLeftPosBy6[ncam].c;
		}
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3)
	{
		int imgFac; int pattType = 1; 

		imgFac = 3;
		pattLeftPosBy3[ncam] = FindLabelEdge_C2b(img2ForPattMatchDiv3, ncam, imgFac, pattLeftPosBy6[ncam]);

		BotPos.x = pattLeftPosBy3[ncam].x/3; 
		BotPos.y = pattLeftPosBy3[ncam].y/3; 
		BotPos.x1= pattLeftPosBy3[ncam].c;
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4)
	{
		int imgFac; int pattType = 1; 

		imgFac = 3;
		pattLeftPosBy3[ncam] = FindWhiteBand_C2(img2ForPattMatchDiv3, bytePattBy3[pattType], ncam, imgFac, pattLeftPosBy6[ncam]);
		
		BotPos.x = pattLeftPosBy3[ncam].x/3; 
		BotPos.y = pattLeftPosBy3[ncam].y/3; 
		BotPos.x1= pattLeftPosBy3[ncam].c;
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5)
	{
		int fact = 3; 
		int width = CroppedCameraHeight/3; int height = CroppedCameraWidth/3;

		int tmpW	= theapp->jobinfo[pframe->CurrentJobNum].boxWt;
		
		int tmpHt	= theapp->jobinfo[pframe->CurrentJobNum].boxHt;
		int tmpYt	= theapp->jobinfo[pframe->CurrentJobNum].boxYt;

		int tmpHb	= theapp->jobinfo[pframe->CurrentJobNum].boxHb;
		int tmpYb	= theapp->jobinfo[pframe->CurrentJobNum].boxYb;

		bxMethod5_Xt[ncam] = bxMethod5_Xb[ncam] = xMidd[ncam] - tmpW/2;
		if(bxMethod5_Xt[ncam]<0)	{bxMethod5_Xt[ncam] = bxMethod5_Xb[ncam] = 0;}

		//Binarized automatically, images come from filter selected on Insp dlg box
		createHistAndBinarizeC2(	im_cam2Rbw, im_cam2Gbw, im_cam2Bbw,
									img2ForPattMatchDiv3,
									byteBinMet5C2,
									ncam,
									bxMethod5_Xt[ncam], tmpYt, tmpHt,
									bxMethod5_Xb[ncam], tmpYb, tmpHb,
									tmpW,fact);

		width = CroppedCameraHeight/3; height = CroppedCameraWidth/3;
		CVisByteImage imgbinEdg(width,height,1,-1,  byteBinMet5C2); imageBinMeth5[ncam] = imgbinEdg;

		int limYtop = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt;
		int limYbot = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt;
		int limX	= bxMethod5_Xt[ncam];
		int limW	= tmpW;

		//Now find the edge with the binary image
		labelPos[ncam].x = 0; labelPos[ncam].y = 0; labelPos[ncam].x1 = 100;
		BotPos = FindLabelEdge_Method5C2(byteBinMet5C2, ncam, fact, limYtop, limYbot,limX,limW);
		BotPos.x1 = 100;
	}
	else if( theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 )
	{
		if(wehaveTempX)
		{
			int imgFac;	int pattType=1;
		
			//##Pattern 1
			imgFac = 6;
			pattLeftPosBy6_method6_1[ncam] = FindPattA1_C2(img2ForPattMatchDiv6,bytePattBy6_method6_1, ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3_method6_1[ncam] = FindPattHigherRes_C2(img2ForPattMatchDiv3, bytePattBy3_method6_1, ncam, imgFac, pattLeftPosBy6_method6_1[ncam]);
				
			TRACE( "I4: New Patt cor %f \n\n",pattLeftPosBy6_method6[ncam].c);
			/*
			imgFac = 6;
			pattLeftPosBy6[ncam] = FindPattA1_C2(img2ForPattMatchDiv6, bytePattBy6[pattType], ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3[ncam] = FindPattHigherRes_C2(img2ForPattMatchDiv3, bytePattBy3[pattType], ncam, imgFac, pattLeftPosBy6[ncam]);

			TRACE( "I2: Old Patt cor %f \n",pattLeftPosBy6[ncam].c);
			*/
			
			//#Pattern 2 
			pattType=2;
			imgFac = 6;
			pattLeftPosBy6_method6[ncam] = FindPattA1_C2(img2ForPattMatchDiv6, bytePattBy6_method6, ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3_method6[ncam] = FindPattHigherRes_C2(img2ForPattMatchDiv3, bytePattBy3_method6, ncam, imgFac, pattLeftPosBy6_method6[ncam]);

			TRACE( "I2: New Patt cor %f \n\n",pattLeftPosBy6_method6[ncam].c);

			//#Pattern 3 
			pattType=3;
			imgFac = 6;
			pattLeftPosBy6_method6_num3[ncam] = FindPattA1_C2(img2ForPattMatchDiv6, bytePattBy6_method6_num3, ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3_method6_num3[ncam] = FindPattHigherRes_C2(img2ForPattMatchDiv3, bytePattBy3_method6_num3, ncam, imgFac, pattLeftPosBy6_method6_num3[ncam]);

			TRACE( "I2: New Patt cor %f \n\n",pattLeftPosBy6_method6_num3[ncam].c);

			//if orig pattern is returning a better corelation
			//if((pattLeftPosBy6[ncam].c >= pattLeftPosBy6_method6[ncam].c) &&(pattLeftPosBy6[ncam].c >= pattLeftPosBy6_method6_num3[ncam].c))
			if((pattLeftPosBy3_method6_1[ncam].c >= pattLeftPosBy6_method6[ncam].c) &&
				(pattLeftPosBy3_method6_1[ncam].c >= pattLeftPosBy6_method6_num3[ncam].c)  )
			{
				BotPos.x = pattLeftPosBy3_method6_1[ncam].x/3; 
				BotPos.y = pattLeftPosBy3_method6_1[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6_method6_1[ncam].c;
				/*
				BotPos.x = pattLeftPosBy3[ncam].x/3; 
				BotPos.y = pattLeftPosBy3[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6[ncam].c;
				*/
			}
			else if((pattLeftPosBy6_method6[ncam].c >= pattLeftPosBy3_method6_1[ncam].c) &&
				(pattLeftPosBy6_method6[ncam].c >= pattLeftPosBy6_method6_num3[ncam].c)  )//if new pattern is returning a better correlation
			{
				BotPos.x = pattLeftPosBy3_method6[ncam].x/3; 
				BotPos.y = pattLeftPosBy3_method6[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6_method6[ncam].c;
			}
			else if((pattLeftPosBy6_method6_num3[ncam].c >= pattLeftPosBy3_method6_1[ncam].c ) &&
				(pattLeftPosBy6_method6_num3[ncam].c >= pattLeftPosBy6_method6[ncam].c))
			{
				BotPos.x = pattLeftPosBy3_method6_num3[ncam].x/3; 
				BotPos.y = pattLeftPosBy3_method6_num3[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6_method6_num3[ncam].c;
			}
		}
	}

	///////////////////////////////////////////
	
	xShift[ncam] = 0;
	yShift[ncam] = theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam] + 15;
	hShift[ncam] =	(theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam] - 
					theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]) - 30;
	int fact = 3;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle==10) //Lipton - Square
	{findShiftedEdges7_C2(fact, ncam);}

	ComposeCam2WaistInspection();

	return BotPos;	
}


TwoPoint CXAxisView::Inspect3(BYTE *im_color,BYTE *im_src3)
{
	TwoPoint BotPos; int ncam=3;
	BotPos.x = 0;
	BotPos.x1 = 0;
	BotPos.y = 0;
	BotPos.y1 = 0;

	///////////////////////////////////////////

	//Finding Middle of the Bottle
	if(theapp->jobinfo[pframe->CurrentJobNum].findMiddles)
	{int fact = 3; findBottleInPicC3(ncam, fact);}
	else
	{
		bottleXL[ncam]	= 0 + 30;
		bottleXR[ncam]	= CroppedCameraHeight - 30; 
		xMidd[ncam]		= CroppedCameraHeight/2;
	}

	///////////////////////////////////////////

	int filterSelPatt =	theapp->jobinfo[pframe->CurrentJobNum].filterSelPatt;

	if(filterSelPatt==1)		{img3ForPattMatchDiv3 = im_cambw3;		img3ForPattMatchDiv6 =	im_cam3bwDiv6;}
	else if(filterSelPatt==2)	{img3ForPattMatchDiv3 = img3SatDiv3;	img3ForPattMatchDiv6 = img3SatDiv6;}
	else if(filterSelPatt==3)	{img3ForPattMatchDiv3 = img3MagDiv3;	img3ForPattMatchDiv6 = img3MagDiv6;}
	else if(filterSelPatt==4)	{img3ForPattMatchDiv3 = img3YelDiv3;	img3ForPattMatchDiv6 = img3YelDiv6;}
	else if(filterSelPatt==5)	{img3ForPattMatchDiv3 = img3CyaDiv3;	img3ForPattMatchDiv6 = img3CyaDiv6;}
	else if(filterSelPatt==6)	{img3ForPattMatchDiv3 = im_cam3Rbw;	img3ForPattMatchDiv6 = im_cam3RbwDiv6;}
	else if(filterSelPatt==7)	{img3ForPattMatchDiv3 = im_cam3Gbw;	img3ForPattMatchDiv6 = im_cam3GbwDiv6;}
	else if(filterSelPatt==8)	{img3ForPattMatchDiv3 = im_cam3Bbw;	img3ForPattMatchDiv6 = im_cam3BbwDiv6;}

	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod1)
	{	BotPos = FindPatternX1_C3(img3ForPattMatchDiv3, ncam);	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2)//Pattern Matching at different Scale
	{
		if(wehaveTempX)
		{
			int imgFac; int pattType =1;
			
		
				//Appending 12-15-2016
			///##
		//	bytePattBy6[pattType]=bytePattBy6_method6_1;
		//	bytePattBy3[pattType]=bytePattBy3_method6_1;
			///##

				imgFac = 6;
			pattLeftPosBy6[ncam] = FindPattA1_C3(img3ForPattMatchDiv6, bytePattBy6_method6_1, ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3[ncam] = FindPattHigherRes_C3(img3ForPattMatchDiv3, bytePattBy3_method6_1, ncam, imgFac, pattLeftPosBy6[ncam]);

			BotPos.x = pattLeftPosBy3[ncam].x/3; 
			BotPos.y = pattLeftPosBy3[ncam].y/3; 
			BotPos.x1= pattLeftPosBy6[ncam].c;
		}
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3)
	{
		int imgFac; int pattType = 1; 

		imgFac = 3;
		pattLeftPosBy3[ncam] = FindLabelEdge_C3b(img3ForPattMatchDiv3, ncam, imgFac, pattLeftPosBy6[ncam]);

		BotPos.x = pattLeftPosBy3[ncam].x/3; 
		BotPos.y = pattLeftPosBy3[ncam].y/3; 
		BotPos.x1= pattLeftPosBy3[ncam].c;
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4)
	{
		int imgFac; int pattType = 1; 

		imgFac = 3;
		pattLeftPosBy3[ncam] = FindWhiteBand_C3(img3ForPattMatchDiv3, bytePattBy3[pattType], ncam, imgFac, pattLeftPosBy6[ncam]);
		
		BotPos.x = pattLeftPosBy3[ncam].x/3; 
		BotPos.y = pattLeftPosBy3[ncam].y/3; 
		BotPos.x1= pattLeftPosBy3[ncam].c;
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5)
	{
		int fact = 3; 
		int width = CroppedCameraHeight/3; int height = CroppedCameraWidth/3;

		int tmpW	= theapp->jobinfo[pframe->CurrentJobNum].boxWt;
		
		int tmpHt	= theapp->jobinfo[pframe->CurrentJobNum].boxHt;
		int tmpYt	= theapp->jobinfo[pframe->CurrentJobNum].boxYt;

		int tmpHb	= theapp->jobinfo[pframe->CurrentJobNum].boxHb;
		int tmpYb	= theapp->jobinfo[pframe->CurrentJobNum].boxYb;

		bxMethod5_Xt[ncam] = bxMethod5_Xb[ncam] = xMidd[ncam] - tmpW/2;
		if(bxMethod5_Xt[ncam]<0)	{bxMethod5_Xt[ncam] = bxMethod5_Xb[ncam] = 0;}

		//Binarized automatically, images come from filter selected on Insp dlg box
		createHistAndBinarizeC3(	im_cam3Rbw, im_cam3Gbw, im_cam3Bbw,
									img3ForPattMatchDiv3,
									byteBinMet5C3,
									ncam,
									bxMethod5_Xt[ncam], tmpYt, tmpHt,
									bxMethod5_Xb[ncam], tmpYb, tmpHb,
									tmpW,fact);

		width = CroppedCameraHeight/3; height = CroppedCameraWidth/3;
		CVisByteImage imgbinEdg(width,height,1,-1,  byteBinMet5C3); imageBinMeth5[ncam] = imgbinEdg;

		int limYtop = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt;
		int limYbot = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt;
		int limX	= bxMethod5_Xt[ncam];
		int limW	= tmpW;

		//Now find the edge with the binary image
		labelPos[ncam].x = 0; labelPos[ncam].y = 0; labelPos[ncam].x1 = 100;
		BotPos = FindLabelEdge_Method5C3(byteBinMet5C3, ncam, fact, limYtop, limYbot,limX,limW);
		BotPos.x1 = 100;
	}

	else if( theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 )
	{
		if(wehaveTempX)
		{

			int imgFac; int pattType =1;
			

			//##PAttern 1
			imgFac = 6;
			pattLeftPosBy6_method6_1[ncam] = FindPattA1_C3(img3ForPattMatchDiv6,bytePattBy6_method6_1, ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3_method6_1[ncam] = FindPattHigherRes_C3(img3ForPattMatchDiv3, bytePattBy3_method6_1, ncam, imgFac, pattLeftPosBy6_method6_1[ncam]);
				
			TRACE( "I4: New Patt cor %f \n\n",pattLeftPosBy6_method6[ncam].c);
			/*
			imgFac = 6;
			pattLeftPosBy6[ncam] = FindPattA1_C3(img3ForPattMatchDiv6, bytePattBy6[pattType], ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3[ncam] = FindPattHigherRes_C3(img3ForPattMatchDiv3, bytePattBy3[pattType], ncam, imgFac, pattLeftPosBy6[ncam]);
			
			TRACE( "I3: Old Patt cor %f \n",pattLeftPosBy6[ncam].c);
			*/

			//Pattern # 2
			pattType = 2;
			imgFac = 6;
			pattLeftPosBy6_method6[ncam] = FindPattA1_C3(img3ForPattMatchDiv6, bytePattBy6_method6, ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3_method6[ncam] = FindPattHigherRes_C3(img3ForPattMatchDiv3, bytePattBy3_method6, ncam, imgFac, pattLeftPosBy6_method6[ncam]);
				
			TRACE( "I3: New Patt cor %f \n\n",pattLeftPosBy6_method6[ncam].c);

			//Pattern # 3 
			pattType = 3;
			imgFac = 6;
			pattLeftPosBy6_method6_num3[ncam] = FindPattA1_C3(img3ForPattMatchDiv6, bytePattBy6_method6_num3, ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3_method6_num3[ncam] = FindPattHigherRes_C3(img3ForPattMatchDiv3, bytePattBy3_method6_num3, ncam, imgFac, pattLeftPosBy6_method6_num3[ncam]);
				
			TRACE( "I3: New Patt cor %f \n\n",pattLeftPosBy6_method6_num3[ncam].c);

			//if orig pattern is returning a better corelation
			//if((pattLeftPosBy6[ncam].c >= pattLeftPosBy6_method6[ncam].c) &&(pattLeftPosBy6[ncam].c >= pattLeftPosBy6_method6_num3[ncam].c))
			if((pattLeftPosBy3_method6_1[ncam].c >= pattLeftPosBy3_method6[ncam].c) &&
				(pattLeftPosBy3_method6_1[ncam].c >= pattLeftPosBy6_method6_num3[ncam].c)  )
			{
				BotPos.x = pattLeftPosBy3_method6_1[ncam].x/3; 
				BotPos.y = pattLeftPosBy3_method6_1[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6_method6_1[ncam].c;
				/*
				BotPos.x = pattLeftPosBy3[ncam].x/3; 
				BotPos.y = pattLeftPosBy3[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6[ncam].c;
				*/
			}
			else if((pattLeftPosBy6_method6[ncam].c >= pattLeftPosBy3_method6_1[ncam].c) &&
				(pattLeftPosBy6_method6[ncam].c >= pattLeftPosBy6_method6_num3[ncam].c)  )//if new pattern is returning a better correlation
			{
				BotPos.x = pattLeftPosBy3_method6[ncam].x/3; 
				BotPos.y = pattLeftPosBy3_method6[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6_method6[ncam].c;
			}
			else if((pattLeftPosBy6_method6_num3[ncam].c >= pattLeftPosBy3_method6_1[ncam].c ) && 
				(pattLeftPosBy6_method6_num3[ncam].c >= pattLeftPosBy6_method6[ncam].c))
			{
				BotPos.x = pattLeftPosBy3_method6_num3[ncam].x/3; 
				BotPos.y = pattLeftPosBy3_method6_num3[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6_method6_num3[ncam].c;
			}
		}
	}

	///////////////////////////////////////////

	xShift[ncam] = 0;
	yShift[ncam] = theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam] + 15;
	hShift[ncam] =	(theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam] - 
					theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]) - 30;
	int fact = 3;

	if(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle==10) //Lipton - Square
	{findShiftedEdges7_C3(fact, ncam);}

	ComposeCam3WaistInspection();
	///////////////////////////////////////////

	return BotPos;	
}


TwoPoint CXAxisView::Inspect4(BYTE *im_color,BYTE *im_src4)
{
	TwoPoint BotPos; int ncam=4;
	BotPos.x = 0;
	BotPos.x1 = 0;
	BotPos.y = 0;
	BotPos.y1 = 0;

	///////////////////////////////////////////

	//Finding Middle of the Bottle
	if(theapp->jobinfo[pframe->CurrentJobNum].findMiddles)
	{
		int fact = 3; findBottleInPicC4(ncam, fact);
	}
	else
	{
		bottleXL[ncam]	= 0 + 30;
		bottleXR[ncam]	= CroppedCameraHeight - 30; 
		xMidd[ncam]		= CroppedCameraHeight/2;
	}

	///////////////////////////////////////////

	int filterSelPatt =	theapp->jobinfo[pframe->CurrentJobNum].filterSelPatt;

	if(filterSelPatt==1)		{img4ForPattMatchDiv3 = im_cambw4;		img4ForPattMatchDiv6 =	im_cam4bwDiv6;}
	else if(filterSelPatt==2)	{img4ForPattMatchDiv3 = img4SatDiv3;	img4ForPattMatchDiv6 = img4SatDiv6;}
	else if(filterSelPatt==3)	{img4ForPattMatchDiv3 = img4MagDiv3;	img4ForPattMatchDiv6 = img4MagDiv6;}
	else if(filterSelPatt==4)	{img4ForPattMatchDiv3 = img4YelDiv3;	img4ForPattMatchDiv6 = img4YelDiv6;}
	else if(filterSelPatt==5)	{img4ForPattMatchDiv3 = img4CyaDiv3;	img4ForPattMatchDiv6 = img4CyaDiv6;}
	else if(filterSelPatt==6)	{img4ForPattMatchDiv3 = im_cam4Rbw;	img4ForPattMatchDiv6 = im_cam4RbwDiv6;}
	else if(filterSelPatt==7)	{img4ForPattMatchDiv3 = im_cam4Gbw;	img4ForPattMatchDiv6 = im_cam4GbwDiv6;}
	else if(filterSelPatt==8)	{img4ForPattMatchDiv3 = im_cam4Bbw;	img4ForPattMatchDiv6 = im_cam4BbwDiv6;}
	
	//Pattern Match at different Scales
	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod1)
	{	BotPos = FindPatternX1_C4(img4ForPattMatchDiv3, ncam);	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2)
	{
		if(wehaveTempX)
		{
			int imgFac;	int pattType = 1;
			
				//Appending 12-15-2016
			///##
		//	bytePattBy6[pattType]=bytePattBy6_method6_1;
		//	bytePattBy3[pattType]=bytePattBy3_method6_1;
			///##

			imgFac = 6;
			pattLeftPosBy6[ncam] = FindPattA1_C4(img4ForPattMatchDiv6, bytePattBy6_method6_1, ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3[ncam] = FindPattHigherRes_C4(img4ForPattMatchDiv3, bytePattBy3_method6_1, ncam, imgFac, pattLeftPosBy6[ncam]);

			BotPos.x = pattLeftPosBy3[ncam].x/3; 
			BotPos.y = pattLeftPosBy3[ncam].y/3; 
			BotPos.x1= pattLeftPosBy6[ncam].c;
		}
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3)
	{
		int imgFac; int pattType = 1; 

		imgFac = 3;
		pattLeftPosBy3[ncam] = FindLabelEdge_C4b(img4ForPattMatchDiv3, ncam, imgFac, pattLeftPosBy6[ncam]);

		BotPos.x = pattLeftPosBy3[ncam].x/3; 
		BotPos.y = pattLeftPosBy3[ncam].y/3; 
		BotPos.x1= pattLeftPosBy3[ncam].c;
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4)
	{
		int imgFac; int pattType = 1; 

		imgFac = 3;
		pattLeftPosBy3[ncam] = FindWhiteBand_C4(img4ForPattMatchDiv3, bytePattBy3[pattType], ncam, imgFac, pattLeftPosBy6[ncam]);
		
		BotPos.x = pattLeftPosBy3[ncam].x/3; 
		BotPos.y = pattLeftPosBy3[ncam].y/3; 
		BotPos.x1= pattLeftPosBy3[ncam].c;
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5)
	{
		int fact = 3; 
		int width = CroppedCameraHeight/3; int height = CroppedCameraWidth/3;

		int tmpW	= theapp->jobinfo[pframe->CurrentJobNum].boxWt;
		
		int tmpHt	= theapp->jobinfo[pframe->CurrentJobNum].boxHt;
		int tmpYt	= theapp->jobinfo[pframe->CurrentJobNum].boxYt;

		int tmpHb	= theapp->jobinfo[pframe->CurrentJobNum].boxHb;
		int tmpYb	= theapp->jobinfo[pframe->CurrentJobNum].boxYb;

		bxMethod5_Xt[ncam] = bxMethod5_Xb[ncam] = xMidd[ncam] - tmpW/2;
		if(bxMethod5_Xt[ncam]<0)	{bxMethod5_Xt[ncam] = bxMethod5_Xb[ncam] = 0;}

		//Binarized automatically, images come from filter selected on Insp dlg box
		createHistAndBinarizeC4(	im_cam4Rbw, im_cam4Gbw, im_cam4Bbw,
									img4ForPattMatchDiv3,
									byteBinMet5C4,
									ncam,
									bxMethod5_Xt[ncam], tmpYt, tmpHt,
									bxMethod5_Xb[ncam], tmpYb, tmpHb,
									tmpW,fact);

		width = CroppedCameraHeight/3; height = CroppedCameraWidth/3;
		CVisByteImage imgbinEdg(width,height,1,-1,  byteBinMet5C4); imageBinMeth5[ncam] = imgbinEdg;

		int limYtop = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt;
		int limYbot = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt;
		int limX	= bxMethod5_Xt[ncam];
		int limW	= tmpW;

		//Now find the edge with the binary image
		labelPos[ncam].x = 0; labelPos[ncam].y = 0; labelPos[ncam].x1 = 100;
		BotPos = FindLabelEdge_Method5C4(byteBinMet5C4, ncam, fact, limYtop, limYbot,limX,limW);
		BotPos.x1 = 100;
	}
	else if( theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6 )
	{
		if(wehaveTempX)
		{

			int imgFac;	int pattType = 1;
			
			//##Pattern 1
			imgFac = 6;
			pattLeftPosBy6_method6_1[ncam] = FindPattA1_C4(img4ForPattMatchDiv6,bytePattBy6_method6_1, ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3_method6_1[ncam] = FindPattHigherRes_C4(img4ForPattMatchDiv3, bytePattBy3_method6_1, ncam, imgFac, pattLeftPosBy6_method6_1[ncam]);
				
			TRACE( "I4: New Patt cor %f \n\n",pattLeftPosBy6_method6[ncam].c);
			/*
			imgFac = 6;
			pattLeftPosBy6[ncam] = FindPattA1_C4(img4ForPattMatchDiv6, bytePattBy6[pattType], ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3[ncam] = FindPattHigherRes_C4(img4ForPattMatchDiv3, bytePattBy3[pattType], ncam, imgFac, pattLeftPosBy6[ncam]);
			
			TRACE( "I4: Old Patt cor %f \n",pattLeftPosBy6[ncam].c);
			*/

			//##PAttern 2
			pattType = 2;
			imgFac = 6;
			pattLeftPosBy6_method6[ncam] = FindPattA1_C4(img4ForPattMatchDiv6,bytePattBy6_method6, ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3_method6[ncam] = FindPattHigherRes_C4(img4ForPattMatchDiv3, bytePattBy3_method6, ncam, imgFac, pattLeftPosBy6_method6[ncam]);
				
			TRACE( "I4: New Patt cor %f \n\n",pattLeftPosBy6_method6[ncam].c);

			//##PAttern 3
			pattType = 3;
			imgFac = 6;
			pattLeftPosBy6_method6_num3[ncam] = FindPattA1_C4(img4ForPattMatchDiv6,bytePattBy6_method6_num3, ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3_method6_num3[ncam] = FindPattHigherRes_C4(img4ForPattMatchDiv3, bytePattBy3_method6_num3, ncam, imgFac, pattLeftPosBy6_method6_num3[ncam]);
				
			TRACE( "I4: New Patt cor %f \n\n",pattLeftPosBy6_method6_num3[ncam].c);

			//if orig pattern is returning a better corelation
			//if((pattLeftPosBy6[ncam].c >= pattLeftPosBy6_method6[ncam].c) &&(pattLeftPosBy6[ncam].c >= pattLeftPosBy6_method6_num3[ncam].c))
			if((pattLeftPosBy6_method6_1[ncam].c >= pattLeftPosBy6_method6_num3[ncam].c) && 
				(pattLeftPosBy6_method6_1[ncam].c >= pattLeftPosBy6_method6[ncam].c))
			{
				BotPos.x = pattLeftPosBy3_method6_1[ncam].x/3; 
				BotPos.y = pattLeftPosBy3_method6_1[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6_method6_1[ncam].c;
				/*
				BotPos.x = pattLeftPosBy3[ncam].x/3; 
				BotPos.y = pattLeftPosBy3[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6[ncam].c;
				*/
			}
			else if((pattLeftPosBy6_method6[ncam].c >= pattLeftPosBy6_method6_1[ncam].c) && 
				(pattLeftPosBy6_method6[ncam].c >= pattLeftPosBy6_method6_num3[ncam].c)  )//if new pattern is returning a better correlation
			{
				BotPos.x = pattLeftPosBy3_method6[ncam].x/3; 
				BotPos.y = pattLeftPosBy3_method6[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6_method6[ncam].c;
			}
			else if((pattLeftPosBy6_method6_num3[ncam].c >= pattLeftPosBy6_method6_1[ncam].c ) && 
				(pattLeftPosBy6_method6_num3[ncam].c >= pattLeftPosBy6_method6[ncam].c))
			{
				BotPos.x = pattLeftPosBy3_method6_num3[ncam].x/3; 
				BotPos.y = pattLeftPosBy3_method6_num3[ncam].y/3; 
				BotPos.x1= pattLeftPosBy6_method6_num3[ncam].c;
			}
		}
	}

	///////////////////////////////////////////

	xShift[ncam] = 0;
	yShift[ncam] = theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam] + 15;
	hShift[ncam] =	(theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam] - 
					theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]) - 30;
	int fact = 3;

	if(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle==10) //Lipton - Square
	{findShiftedEdges7_C4(fact, ncam);}

	///////////////////////////////////////////

	ComposeCam4WaistInspection();

	//---------------------------------------------------------

	return BotPos;	
}


bool CXAxisView::Inspect5(BYTE *im_color,BYTE *im_src5)
{
	int ncam = 5;

	currEdgeL = currEdgeR = currEdgeM = 0;

	int filterSelPattCap =	theapp->jobinfo[pframe->CurrentJobNum].filterSelPattCap;

	if(filterSelPattCap==2)			{byte5ForPattMatchDiv3 = im_cam5Rbw;}
	else if(filterSelPattCap==3)	{byte5ForPattMatchDiv3 = im_cam5Gbw;}
	else if(filterSelPattCap==4)	{byte5ForPattMatchDiv3 = im_cam5Bbw;}
	else							{byte5ForPattMatchDiv3 = im_cambw5;}

	/////////////
	
	QueryPerformanceFrequency(&m_lnC1FreqI1); QueryPerformanceCounter(&m_lnC1StartI1);	//Timing

	//Edge detection Left, Right, Middle edges... before im_src5
	CapLSide=FindCapSide(byte5ForPattMatchDiv3,theapp->jobinfo[pframe->CurrentJobNum].neckBarY,true);
	CapRSide=FindCapSide(byte5ForPattMatchDiv3,theapp->jobinfo[pframe->CurrentJobNum].neckBarY,false);

	ldist=CapLSide.x+theapp->jobinfo[pframe->CurrentJobNum].lDist;
	rdist=CapRSide.x-theapp->jobinfo[pframe->CurrentJobNum].lDist;

	CapLTop=FindCapTop(byte5ForPattMatchDiv3,ldist, 0);
	CapRTop=FindCapTop(byte5ForPattMatchDiv3,rdist, 1);

	MidTop.x= float( (CapRSide.x+CapLSide.x)/2 );

	QueryPerformanceCounter(&m_lnC1EndI1); nDiff=0; dMicroSecondC1I1= 0;			//Time

	if( m_lnC1StartI1.QuadPart !=0 && m_lnC1EndI1.QuadPart != 0)
	{
		dMicroSecondC1I1	= (double)(m_lnC1FreqI1.QuadPart/1000); 
		nDiff				= m_lnC1EndI1.QuadPart-m_lnC1StartI1.QuadPart; 
		dMicroSecondC1I1	= nDiff/dMicroSecondC1I1;
	}
	///////////////////////////////////

	if(!LearnDone)
	{
		//NeckLSide=FindNeckSide(im_srcIn5,theapp->jobinfo[pframe->CurrentJobNum].neckBarY,CapLSide.x,true);
		//NeckRSide=FindNeckSide(im_srcIn5,theapp->jobinfo[pframe->CurrentJobNum].neckBarY,CapRSide.x,false);
		
		//NeckOffsetX=NeckLSide.x-CapLSide.x;
	
		reteachColor=true;
	}
	else
	{
		QueryPerformanceFrequency(&m_lnC1FreqI4); QueryPerformanceCounter(&m_lnC1StartI4);	//Timing

		NeckLSide.x=CapLSide.x;//+NeckOffsetX;
		NeckLSide.y=theapp->jobinfo[pframe->CurrentJobNum].neckBarY;
				
		NeckRSide.x=CapRSide.x;//-NeckOffsetX;		
		NeckRSide.y=theapp->jobinfo[pframe->CurrentJobNum].neckBarY;
					
		NeckLLip=FindNeckLipPat(byte5ForPattMatchDiv3, NeckLSide, true);
		NeckRLip=FindNeckLipPat(byte5ForPattMatchDiv3, NeckRSide, false);
				
		if(NeckLLip.c<=NeckRLip.c)	{Result4=NeckLLip.c; leftLipDefect=true;}
		else						{Result4=NeckRLip.c; leftLipDefect=false;}
				
		Result1l=sqrtf( (NeckLLip.y-CapLTop.y)*(NeckLLip.y-CapLTop.y) + (NeckLLip.x-CapLTop.x)*(NeckLLip.x-CapLTop.x) );	
		Result1r=sqrtf( (NeckRLip.y-CapRTop.y)*(NeckRLip.y-CapRTop.y) + (NeckRLip.x-CapRTop.x)*(NeckRLip.x-CapRTop.x) );

		QueryPerformanceCounter(&m_lnC1EndI4); nDiff=0; dMicroSecondC1I4= 0;			//Time

		if( m_lnC1StartI4.QuadPart !=0 && m_lnC1EndI4.QuadPart != 0)
		{
			dMicroSecondC1I4	= (double)(m_lnC1FreqI4.QuadPart/1000); 
			nDiff				= m_lnC1EndI4.QuadPart-m_lnC1StartI4.QuadPart; 
			dMicroSecondC1I4	= nDiff/dMicroSecondC1I4;
		}

		QueryPerformanceFrequency(&m_lnC1FreqI2); QueryPerformanceCounter(&m_lnC1StartI2);	//Timing

		if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)
		{
			sportBand	=	CheckSportBand(byte5ForPattMatchDiv3, CapLTop, CapRTop);
			Result2		=	sportBand.x1 - sportBand.x;
		}
		else
		{
			MidBot.y	=	(NeckRLip.y + NeckLLip.y)/2;
			MidTop.y	=	FindCapMid(byte5ForPattMatchDiv3, MidTop.x);

			Result2		=	MidBot.y - MidTop.y;
		}

		/////////////

		QueryPerformanceCounter(&m_lnC1EndI2); nDiff=0; dMicroSecondC1I2= 0;			//Time

		if( m_lnC1StartI2.QuadPart !=0 && m_lnC1EndI2.QuadPart != 0)
		{
			dMicroSecondC1I2	= (double)(m_lnC1FreqI2.QuadPart/1000); 
			nDiff				= m_lnC1EndI2.QuadPart-m_lnC1StartI2.QuadPart; 
			dMicroSecondC1I2	= nDiff/dMicroSecondC1I2;
		}
	}

	QueryPerformanceFrequency(&m_lnC1FreqI3); QueryPerformanceCounter(&m_lnC1StartI3);	//Timing

	if(reteachColorCap)
	{
		reteachColorCap=false;

		ColorTarget2=DoUser2(im_color, ncam, NeckLLip.x+80,NeckLLip.y-theapp->jobinfo[pframe->CurrentJobNum].capColorOffset, 60, 0);
					
		theapp->jobinfo[pframe->CurrentJobNum].colorTargR2=GetRValue(ColorTarget2);
		theapp->jobinfo[pframe->CurrentJobNum].colorTargG2=GetGValue(ColorTarget2);
		theapp->jobinfo[pframe->CurrentJobNum].colorTargB2=GetBValue(ColorTarget2);
		theapp->savedColor2=ColorTarget2;
	}
	else
	{
		ColorResult2=DoUser2(im_color, ncam, NeckLLip.x+80,NeckLLip.y-theapp->jobinfo[pframe->CurrentJobNum].capColorOffset, 60, 0);

		int targetR=theapp->jobinfo[pframe->CurrentJobNum].colorTargR2;
		int targetG=theapp->jobinfo[pframe->CurrentJobNum].colorTargG2;
		int targetB=theapp->jobinfo[pframe->CurrentJobNum].colorTargB2;

		int resR=GetRValue(ColorResult2);
		int resG=GetGValue(ColorResult2);
		int resB=GetBValue(ColorResult2);

		Result3=sqrtf( fabs((resB-targetB)*(resB-targetB)) + fabs((resG-targetG)*(resG-targetG)) + fabs((resR-targetR)*(resR-targetR)));
	}

	QueryPerformanceCounter(&m_lnC1EndI3); nDiff=0; dMicroSecondC1I3= 0;			//Time

	if( m_lnC1StartI3.QuadPart !=0 && m_lnC1EndI3.QuadPart != 0)
	{
		dMicroSecondC1I3	= (double)(m_lnC1FreqI3.QuadPart/1000); 
		nDiff				= m_lnC1EndI3.QuadPart-m_lnC1StartI3.QuadPart; 
		dMicroSecondC1I3	= nDiff/dMicroSecondC1I3;
	}

	return true;
}

void CXAxisView::DoResults()
{
	//breakdown of label inspection results for ease of readibility
	bool inspPassLabel, inspPassCap;
	int inspection_count = 12;
	std::vector<bool> inspect_failure(inspection_count+1);

	QueryPerformanceFrequency(&Frequency); QueryPerformanceCounter(&timeStart);

	//This helps to understand if all the cameras are finished or not with their 
	//respective inspections

	if(	!cameraDone[1][pframe->countC0] ||
		!cameraDone[2][pframe->countC1] ||
		!cameraDone[3][pframe->countC2] ||
		!cameraDone[4][pframe->countC3] ||
		!cameraDone[5][pframe->countC4])
	{
		pframe->notFinished++;
		
		if(pframe->notFinished>5000)	{pframe->notFinished = 0;}
	}

	CCamera* frame = pframe->m_pcamera;

	//To save one set of pictures
	if(theapp->saveOneSetCol)	
	{
		unsigned char* im1 = pframe->m_pcamera->m_imageProcessed[0][pframe->m_pcamera->readNext[0].load()]->GetData();
		unsigned char* im2 = pframe->m_pcamera->m_imageProcessed[1][pframe->m_pcamera->readNext[1].load()]->GetData();
		unsigned char* im3 = pframe->m_pcamera->m_imageProcessed[2][pframe->m_pcamera->readNext[2].load()]->GetData();
		unsigned char* im4 = pframe->m_pcamera->m_imageProcessed[3][pframe->m_pcamera->readNext[3].load()]->GetData();
		unsigned char* im5 = pframe->m_pcamera->m_imageProcessed[4][pframe->m_pcamera->readNext[4].load()]->GetData();
		OnSaveSet(im1, im2, im3, im4, im5, pframe->picNum);
		theapp->saveOneSetCol=false;
	}
	
	//to save 50 sets of pictures
	if(theapp->saveManySetCol)	
	{
		unsigned char* im1 = pframe->m_pcamera->m_imageProcessed[0][pframe->m_pcamera->readNext[0].load()]->GetData();
		unsigned char* im2 = pframe->m_pcamera->m_imageProcessed[1][pframe->m_pcamera->readNext[1].load()]->GetData();
		unsigned char* im3 = pframe->m_pcamera->m_imageProcessed[2][pframe->m_pcamera->readNext[2].load()]->GetData();
		unsigned char* im4 = pframe->m_pcamera->m_imageProcessed[3][pframe->m_pcamera->readNext[3].load()]->GetData();
		unsigned char* im5 = pframe->m_pcamera->m_imageProcessed[4][pframe->m_pcamera->readNext[4].load()]->GetData();
		OnSaveSet(im1, im2, im3, im4, im5, pframe->picNum);
		pframe->picNum++;
		if(pframe->picNum>=50) {pframe->picNum=1; theapp->saveManySetCol=false;}
	}	

	///////////////////////////////////////////

	int zerobot	= theapp->jobinfo[pframe->CurrentJobNum].orienArea+13;

	resBest			= 0;
	cam				= 0;
	camToCheck		= 0;
	labelBox.x		= 0;
	labelBox.y		= 0;
	labelPosY		= 0;

	posPattBy6.x	= 0;
	posPattBy6.y	= 0; 
	bestcorPattBy6	= 0;
	bestcamPattBy6	= 0;
	bestPattTypBy6	= 0;

	if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod1)
	{
		//Finding camera with the best correlation
		for(int i=1; i<=4; i++)
		{
			if(labelPos[i].x1>=resBest)
			{
				resBest		=	labelPos[i].x1; //updating best correlation
				cam			=	i; 
				camToCheck	=	i; 
				labelBox.y	=	labelPos[i].y; 
				labelBox.x	=	labelPos[i].x;
				labelPosY	=	labelPos[i].y;
			}
		}
	}
	//PATTERN MATCHING USES NEW METHOD DIFF SCALES
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2)
	{
		if(wehaveTempX)
		{
			//Finding the camera that shows the best Pattern for Left
			float bestLeft=0; camLeft=0;

			for(int i=1; i<=4; i++)
			{
				if(labelPos[i].x1>=bestLeft)
				{bestLeft =	labelPos[i].x1; camLeft=i;}			//best in 4 cameras
			}

			bestcamPattBy6	= camLeft;  //Which camera gives the highest corelation
			bestcorPattBy6	= bestLeft; //Highest corelation
			posPattBy6		= pattLeftPosBy6[camLeft]; 
			bestPattTypBy6	= 1; 
			txtBestPatt		= "";

			resBest		=	bestLeft; //Best correlation at higher Scale
			//resBest		=	pattLeftPosBy6[camLeft].c; //Best correlation at higher Scale
			cam			=	camLeft; 
			camToCheck	=	camLeft; 
			labelBox.y	=	labelPos[camLeft].y; 
			labelBox.x	=	labelPos[camLeft].x;
			labelPosY	=	labelPos[camLeft].y;
		}
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3)
	{
		int maxQtyPt = 0;

		int minAvg = 255;
		
		//Finding camera with the best correlation
		for(int i=1; i<=4; i++)
		{
			if(labelPos[i].x1<minAvg)
			{
				minAvg		=	labelPos[i].x1;
				resBest		=	100; //updating best correlation
				cam			=	i; 
				camToCheck	=	i; 
				labelBox.y	=	labelPos[i].y; 
				labelBox.x	=	labelPos[i].x;
				labelPosY	=	labelPos[i].y;
			}
		}

		if(labelBox.y==CroppedCameraWidth/3) 
		//{labelBox.y=theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3}
		{labelBox.y=0;}
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4)
	{
		int minHeight = MainDisplayImageHeight;

		//Finding camera with the best correlation
		for(int i=1; i<=4; i++)
		{
			if(labelPos[i].y<minHeight)
			{
				minHeight	=	labelPos[i].y;
				resBest		=	100; //updating best correlation
				cam			=	i; 
				camToCheck	=	i; 
				labelBox.y	=	labelPos[i].y; 
				labelBox.x	=	labelPos[i].x;
				labelPosY	=	labelPos[i].y;
			}
		}
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5)
	{
		int fact = 3;
		int tmpMax = 0;
		int tmpMin = 10000;
		int sumHs  = 0;

		for(int i=1; i<=4; i++)
		{
			sumHs+=labelPos[i].y;

			if(labelPos[i].y>=tmpMax) tmpMax = labelPos[i].y;
			if(labelPos[i].y<=tmpMin) tmpMin = labelPos[i].y;
		}
		resBest = 100;

		sumHs-=tmpMax; sumHs-=tmpMin;
		labelPosY = (sumHs/2)/fact;
	}

	else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6)  
	{
		//Finding the camera that shows the best Pattern for Left
		float bestLeft=0; camLeft=0;

		for(int i=1; i<=4; i++)
		{
			if(labelPos[i].x1>=bestLeft)					//labelPos[i].x1 = corelation 
			{bestLeft =	labelPos[i].x1; camLeft=i;}			//best in 4 cameras
		}

		bestcamPattBy6	= camLeft;  //Which camera gives the highest corelation
		bestcorPattBy6	= bestLeft; //Highest corelation
		posPattBy6.x	= labelPos[camLeft].x*3; //pattLeftPosBy6[camLeft];  //posPattBy6 is used in Ondraw to Draw Red Box
		posPattBy6.y	= labelPos[camLeft].y*3;
		bestPattTypBy6	= 1; 
		txtBestPatt		= "";

		resBest		=	bestLeft; //Best correlation at higher Scale
		//resBest		=	pattLeftPosBy6[camLeft].c; //Best correlation at higher Scale
		cam			=	camLeft; 
		camToCheck	=	camLeft; 
		labelBox.y	=	labelPos[camLeft].y;  //By 3 Resolution Best Corelation Y Cord.
		labelBox.x	=	labelPos[camLeft].x;  //By 3 Resolution Best Corelation X Cord.
		labelPosY	=	labelPos[camLeft].y;
	
		TRACE( "Best Cor Camera %d \n\n",camLeft);

	}

	//*** Inspection 6 - just for Square bottles, just finding results
	float maxIncDeg=0; 
	int i;
	if(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle==10) //Lipton - Jeremy in Boston
	{
		for(i=1; i<=4; i++)
		{shiftedMidd[i] = abs(middlShift[i]-xMidd[i]);}

		maxShift = shiftedMidd[1];
		minShift = shiftedMidd[1];

		for(i=2; i<=4; i++) //finding the biggest shifted distance among the four cameras
		{	if(shiftedMidd[i]>maxShift)	{maxShift = shiftedMidd[i];}	}

		Result6 = maxShift;

		///////////////////////////////////////////

		for(i=1; i<=4; i++)
		{shiftedMiddWPoints[i] = abs(middlShiftWPoints[i]-xMidd[i]);}

		maxShiftWPoints = shiftedMiddWPoints[1];

		for(i=2; i<=4; i++)
		{	if(shiftedMiddWPoints[i]>maxShiftWPoints)	{maxShiftWPoints = shiftedMiddWPoints[i];}	}

		Result6 = maxShiftWPoints;

		///////////////////////////////////////////

		maxIncDeg = MAX(angIncDegL[1], angIncDegR[1]);
		float maxTmp;

		for(i=2; i<=4; i++)
		{
			maxTmp = MAX(angIncDegL[i],angIncDegR[i]);
			if(maxTmp>maxIncDeg) {maxIncDeg = maxTmp;}
		}
	}

	//*** Inspection 8 - No Label inspection
	QueryPerformanceFrequency(&m_lnC1FreqI8); QueryPerformanceCounter(&m_lnC1StartI8); //Timing

		int fact=3; int  sumAvg=0;

		for(int i=1; i<=4; i++) //Four cameras, then run this four times
		{ NoLbl[i]=CheckLabelNatural(i, fact); sumAvg += NoLbl[i].x;} //qty of pixels that exceed a threshold

		Result8=sumAvg/4;

	QueryPerformanceCounter(&m_lnC1EndI8); nDiff = 0; dMicroSecondC1I8 = 0;	//Time
	if( m_lnC1StartI8.QuadPart !=0 && m_lnC1EndI8.QuadPart != 0)
	{
		dMicroSecondC1I8	= (double)(m_lnC1FreqI8.QuadPart/1000); 
		nDiff = m_lnC1EndI8.QuadPart-m_lnC1StartI8.QuadPart; dMicroSecondC1I8 = nDiff/dMicroSecondC1I8;
	}

	//*** Inspection 9
	//Finding the smallest Splice avg value
	QueryPerformanceFrequency(&m_lnC1FreqI9); QueryPerformanceCounter(&m_lnC1StartI9); //Timing

		fact=3;

		int wsplice		= theapp->jobinfo[pframe->CurrentJobNum].wSplice;
		int hsplice		= theapp->jobinfo[pframe->CurrentJobNum].hSplice;
		int hsplicesmBx	= theapp->jobinfo[pframe->CurrentJobNum].hSpliceSmBx;

		for(i=1; i<=4; i++)
		{
			bottleYspliceIni[i] = theapp->jobinfo[pframe->CurrentJobNum].ySplice[1];

			if(theapp->jobinfo[pframe->CurrentJobNum].findMiddles)
			{bottleXspliceIni[i] = xMidd[i] - wsplice/2;}
			else
			{bottleXspliceIni[i] = (CroppedCameraHeight - wsplice)/2;}

			findSplice(i, fact, bottleXspliceIni[i], bottleYspliceIni[i], wsplice, hsplice, hsplicesmBx);

			//TRACE("%i vari:%i stdev:%i\n", i, varianceSplice[i], sdeviatiSplice[i]);
		}

		int valMin=255;
		int thVariance = theapp->jobinfo[pframe->CurrentJobNum].spliceVar;  // From the UI. Called " Dispersion Limit"

		for(i=1; i<=4; i++)  // Even if 1 camera satisfies the condition
		{ 
			if(minSpliceAvg[i]<valMin && varianceSplice[i]<thVariance) 
			{
				valMin=minSpliceAvg[i];
			} 
		}

		Result9 = valMin;


	QueryPerformanceCounter(&m_lnC1EndI9); nDiff=0; dMicroSecondC1I9= 0;			//Time

	if( m_lnC1StartI9.QuadPart !=0 && m_lnC1EndI9.QuadPart != 0)
	{
		dMicroSecondC1I9	= (double)(m_lnC1FreqI9.QuadPart/1000); 
		nDiff				= m_lnC1EndI9.QuadPart-m_lnC1StartI9.QuadPart; 
		dMicroSecondC1I9	= nDiff/dMicroSecondC1I9;
	}

	//*** Inspection 10 - Tear Label
	QueryPerformanceFrequency(&m_lnC1FreqI10); QueryPerformanceCounter(&m_lnC1StartI10);//Timing

	bool takeC1, takeC2, takeC3, takeC4;
	takeC1 = takeC2 = takeC3 = takeC4 = false;

	if		(camToCheck==1) {takeC4=takeC1=takeC2=true;}
	else if	(camToCheck==2) {takeC1=takeC2=takeC3=true;}
	else if	(camToCheck==3) {takeC2=takeC3=takeC4=true;}
	else if	(camToCheck==4) {takeC3=takeC4=takeC1=true;}

	int fct	= 1; stitchW = 0; stitchH = 0;

	for(int c=1; c<=4; c++)	{stitchX[c] = 0; stitchY[c] = 0;}

	calcTearCoordinates();
	makeStitchImgSelection();

/*	int offsetX = abs(theapp->jobinfo[pframe->CurrentJobNum].avoidOffX);

	if(offsetX>stitchW/4)	{offsetX=stitchW/4;}

	for(i=1; i<=4; i++)
	{
		xAvoidIni[i] = (i-1)*stitchW + stitchW/2 - offsetX;
		xAvoidEnd[i] = (i-1)*stitchW + stitchW/2 + offsetX;
	}*/
	
	int qtyPixels = 0;

	// STITCHING images for 
	qtyPixels =	Stitch4ImgX(stitchW, stitchH,
				img_cam1Ligh, img_cam1Dark, img_cam1Edge, stitchX[1], stitchY[1],	//im_S1Div3
				img_cam2Ligh, img_cam2Dark, img_cam2Edge, stitchX[2], stitchY[2],	//im_S2Div3
				img_cam3Ligh, img_cam3Dark, img_cam3Edge, stitchX[3], stitchY[3],	//im_S3Div3
				img_cam4Ligh, img_cam4Dark, img_cam4Edge, stitchX[4], stitchY[4],	//im_S4Div3
				camToCheck, labelBox,
				takeC1, takeC2, takeC3, takeC4, fct=3);

	StitchedLength	= 4*stitchW/fct;
	StitchedHeight	= stitchH/fct;

	if(StitchedLength>0 && StitchedHeight>0)
	{
		CVisByteImage imgstbw(StitchedLength, StitchedHeight, 1, -1, imgStitchedBW);
		imageStitchedbw=imgstbw;

		CVisByteImage imgstdark1(StitchedLength, StitchedHeight, 1, -1, imgStitchedDark);
		imageStitchedDark=imgstdark1;

		CVisByteImage imgstdark2(StitchedLength, StitchedHeight, 1, -1, imgStitchedFilDark);
		imageStitchedFilDark=imgstdark2;

		CVisByteImage imgstligh1(StitchedLength, StitchedHeight, 1, -1, imgStitchedLigh);
		imageStitchedLigh=imgstligh1;

		CVisByteImage imgstligh2(StitchedLength, StitchedHeight, 1, -1, imgStitchedFilLigh);
		imageStitchedFilLigh=imgstligh2;

		CVisByteImage imgstedg1(StitchedLength, StitchedHeight, 1, -1,	imgStitchedEdg);
		imageStitchedEdg=imgstedg1;

		CVisByteImage imgstedg2(StitchedLength, StitchedHeight, 1, -1,	imgStitchedFilEdg);
		imageStitchedFilEdg=imgstedg2;

		CVisByteImage imgstfin(StitchedLength, StitchedHeight, 1, -1,	imgStitchedFilFinal);
		imageStitchedFilFinal=imgstfin;
	}

	//Result6 = edgeProp;
	Result10 = float(qtyPixels);
			
	QueryPerformanceCounter(&m_lnC1EndI10); nDiff=0; dMicroSecondC1I10= 0; //Time

	if( m_lnC1StartI10.QuadPart !=0 && m_lnC1EndI10.QuadPart != 0)
	{
		dMicroSecondC1I10	= (double)(m_lnC1FreqI10.QuadPart/1000); 
		nDiff				= m_lnC1EndI10.QuadPart-m_lnC1StartI10.QuadPart; 
		dMicroSecondC1I10	= nDiff/dMicroSecondC1I10;
	}

	//////////////////////////////////////

	if(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle==3 && cam!=0)//wolverine
	{
		if(cam==1)	{Ledge=FindLedge3(im_cambw1,labelPos[cam],cam);}
		if(cam==2)	{Ledge=FindLedge3(im_cambw2,labelPos[cam],cam);}
		if(cam==3)	{Ledge=FindLedge3(im_cambw3,labelPos[cam],cam);}
		if(cam==4)	{Ledge=FindLedge3(im_cambw4,labelPos[cam],cam);}

		if(cam==3)
		{neckEdge=FindPattern3(im_cambw3);}

		Result5=(zerobot-labelPosY)/2;	//Height

		if(Result5>0)	{Result5-=1;}
		if(Result5<0)	{Result5+=1;}

		int value2=int(Result5);

		switch(value2)
		{
			case 0:	theapp->outh0+=1;	break;	case 1: theapp->outh1+=1;	break;
			case 2: theapp->outh2+=1;	break;	case 3: theapp->outh3+=1;	break;
			case 4: theapp->outh4+=1;	break;	case 5: theapp->outh5+=1;	break;
			case 6: theapp->outh6+=1;	break;	case -1:theapp->outh1m+=1;	break;
			case -2:theapp->outh2m+=1;	break;	case -3:theapp->outh3m+=1;	break;
			case -4:theapp->outh4m+=1;	break;	case -5:theapp->outh5m+=1;	break;
			case -6:theapp->outh6m+=1;	break;
		}

		Result7=resBest;

		if( theapp->inspctn[7].enable)
		{
			if(  Result7>= theapp->inspctn[7].lmin && Result7<= theapp->inspctn[7].lmax)
			{integrityOK=true;}
			else	{integrityOK=false;}
		}
		else {integrityOK=true;}

		if(!integrityOK)	{Result5=7;}
		if(Result5>=7)			{theapp->outh7+=1;}
		if(Result5<=-7)			{theapp->outh7m+=1;}

		//Result6=(Ledge.y1);//rotate
		theapp->bottlepos=Ledge.y1;

		int modRes=(135-Ledge.x)/2;
		theapp->labelpos=modRes;

		Result6=(modRes-Ledge.y1)-theapp->jobinfo[pframe->CurrentJobNum].rotOffset;
		Result6=Result6*0.9;
	
		if(Result6>=8)			{Result6-=2;}
		if(Result6>=3)			{Result6-=1;}
		if(Result6<=-8)			{Result6+=2;}
		if(Result6<=-3)			{Result6+=1;}
		if(Result6>=20)			{Result6=20;}
		if(Result6<=-20)		{Result6=-20;}

		if(Result6<=-19)		{theapp->out19m+=1;}
		
		int value=int(Result6);
		
		switch(value)
		{
			case 0:	theapp->out0+=1;	break;	case 1:	theapp->out1+=1;	break;
			case 2:	theapp->out2+=1;	break;	case 3:	theapp->out3+=1;	break;
			case 4:	theapp->out4+=1;	break;	case 5:	theapp->out5+=1;	break;
			case 6:	theapp->out6+=1;	break;	case 7:	theapp->out7+=1;	break;
			case 8:	theapp->out8+=1;	break;	case 9:	theapp->out9+=1;	break;
			case 10:theapp->out10+=1;	break;	case 11:theapp->out11+=1;	break;
			case 12:theapp->out12+=1;	break;	case 13:theapp->out13+=1;	break;
			case 14:theapp->out14+=1;	break;	case 15:theapp->out15+=1;	break;
			case 16:theapp->out16+=1;	break;	case 17:theapp->out17+=1;	break;
			case 18:theapp->out18+=1;	break;

			case -1:theapp->out1m+=1;	break;
			case -2:theapp->out2m+=1;	break;	case -3:theapp->out3m+=1;	break;
			case -4:theapp->out4m+=1;	break;	case -5:theapp->out5m+=1;	break;
			case -6:theapp->out6m+=1;	break;	case -7:theapp->out7m+=1;	break;
			case -8:theapp->out8m+=1;	break;	case -9:theapp->out9m+=1;	break;
			case -10:theapp->out10m+=1;	break;	case -11:theapp->out11m+=1;	break;
			case -12:theapp->out12m+=1;	break;	case -13:theapp->out13m+=1;	break;
			case -14:theapp->out14m+=1;	break;	case -15:theapp->out15m+=1;	break;
			case -16:theapp->out16m+=1;	break;	case -17:theapp->out17m+=1;	break;
			case -18:theapp->out18m+=1;	break;
		}

		if(Result6>=19)		{theapp->out19+=1;}
		if(Result6<=-19)	{theapp->out19m+=1;}
	}

	else//Sobe
	{
		Result5=(zerobot-labelPosY)/2;	//Height
		//if(Result5>0)	{Result5-=1;}
		//if(Result5<0)	{Result5+=1;}

		int value2=int(Result5);
		
		switch(value2)
		{
			case 0:	theapp->outh0+=1;	theapp->outh0s+=1;	break;
			case 1: theapp->outh1+=1;	theapp->outh1s+=1;	break;
			case 2: theapp->outh2+=1;	theapp->outh2s+=1;	break;	
			case 3: theapp->outh3+=1;	theapp->outh3s+=1;	break;
			case 4: theapp->outh4+=1;	theapp->outh4s+=1;	break;	
			case 5: theapp->outh5+=1;	theapp->outh5s+=1;	break;
			case 6: theapp->outh6+=1;	theapp->outh6s+=1;	break;	
			case -1:theapp->outh1m+=1;	theapp->outh1ms+=1;	break;
			case -2:theapp->outh2m+=1;	theapp->outh2ms+=1;	break;	
			case -3:theapp->outh3m+=1;	theapp->outh3ms+=1;	break;
			case -4:theapp->outh4m+=1;	theapp->outh4ms+=1;	break;	
			case -5:theapp->outh5m+=1;	theapp->outh5ms+=1;	break;
			case -6:theapp->outh6m+=1;	theapp->outh6ms+=1;	break;
		}

		if(Result5>=7)	{theapp->outh7+=1;	theapp->outh7s+=1;}
		if(Result5<=-7)	{theapp->outh7m+=1;	theapp->outh7ms+=1;}

		Result7=resBest;
	}


	//*** Inspection 4 for Lipton ONLY
	// if(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle==10)
	// {Result4 = maxIncDeg;}

	// if(Result4>= theapp->inspctn[4].lmin && Result4<= theapp->inspctn[4].lmax)
	// {neckDefectToShow=false;}
	// else {neckDefectToShow=true;}
	if(theapp->inspctn[12].enable)
	 {
	 if(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle==10)
	 {Result12 = maxIncDeg;}
	 //else
	 // {

	//########VK Start of NEW INSPECTION 12  (Cocked or Skewed Label)
	// int   ncam=1;int factor=3;
	// diff[1]=FindCockedLabel1(im_cambw1,ncam,factor);

	// ncam=2;factor=3;
	// diff[2]=FindCockedLabel2(im_cambw2,ncam,factor);

	// ncam=3;factor=3;
	// diff[3]=FindCockedLabel3(im_cambw3,ncam,factor);

	// ncam=4;factor=3;
	// diff[4]=FindCockedLabel4(im_cambw4,ncam,factor);
	 // }


	// Result12 = MAX(MAX(MAX(diff[1],diff[2]),diff[3] ),diff[4] );//To find the greatest difference  //now is finding the greatest slope
	 //######VK  --End of NEW INSPECTION 12
	 }


//-------------------------------------------------

	Result11 = 2000;
	if(Result11Cam1 < Result11)
	{
		Result11 = Result11Cam1;
	}
	if(Result11Cam2 < Result11)
	{
		Result11 = Result11Cam2;
	}
	if(Result11Cam3 < Result11)
	{
		Result11 = Result11Cam3;
	}
	if(Result11Cam4 < Result11)
	{
		Result11 = Result11Cam4;
	}

////////////////////

	typofault = 0;

	if(theapp->inspctn[1].enable)
	{
		if(	Result1l>=theapp->inspctn[1].lmin && Result1r>=theapp->inspctn[1].lmin && 
			Result1l<=theapp->inspctn[1].lmax && Result1r<=theapp->inspctn[1].lmax)
		{inspResultOK[1]=true;}	else {inspResultOK[1]=false; typofault = 1; e1Cnt+=1;}
	}	else{inspResultOK[1]=true;}

	if( theapp->inspctn[2].enable)
	{
		if(Result2>=theapp->inspctn[2].lmin && Result2<=theapp->inspctn[2].lmax)
		{inspResultOK[2]=true;} else {inspResultOK[2]=false; typofault = 2; e2Cnt+=1;} 
	}else	{inspResultOK[2]=true;}

	if(theapp->inspctn[3].enable)
	{
		if(Result3>=theapp->inspctn[3].lmin && Result3<=theapp->inspctn[3].lmax)
		{inspResultOK[3]=true;} else {inspResultOK[3]=false; typofault = 3; e3Cnt+=1;} 
	}	else	{inspResultOK[3]=true;}
	
	if( theapp->inspctn[4].enable)
	{
		if(Result4>=theapp->inspctn[4].lmin && Result4<=theapp->inspctn[4].lmax)
		{inspResultOK[4]=true;}	else {inspResultOK[4]=false; typofault = 4; e4Cnt+=1;} 
	}	else	{inspResultOK[4]=true;}

	if( theapp->inspctn[5].enable)
	{
		if(Result5>=theapp->inspctn[5].lmin && Result5<=theapp->inspctn[5].lmax)
		{inspResultOK[5]=true;} else {inspResultOK[5]=false; typofault = 5; e5Cnt+=1;} 
	}	else	{inspResultOK[5]=true;}	

	if( theapp->inspctn[6].enable)
	{
		int bottleStyle = theapp->jobinfo[pframe->CurrentJobNum].bottleStyle;

		if(bottleStyle==3)//Wolverine
		{
			if(Result6>=theapp->inspctn[6].lmin && Result6<=theapp->inspctn[6].lmax)
			{inspResultOK[6]=true;}	
			else {inspResultOK[6]=false; typofault = 6;} 
		}
		else if(bottleStyle==10)
		{
			if(Result6<=theapp->inspctn[6].lmaxC)	{inspResultOK[6]=true;}	
			else									{inspResultOK[6]=false;	typofault = 6;}
		}
		else	//This is supposed to work for TEAR DETECTION
		{
			if(Result6>=theapp->inspctn[6].lminB)//&& Result6<=theapp->inspctn[6].lmaxB)
			{inspResultOK[6]=true;}	
			else {inspResultOK[6]=false;} 
		}

		if(!inspResultOK[6]) {e6Cnt+=1;}

	}	else	{inspResultOK[6]=true;fault8=false;}

	if( theapp->inspctn[7].enable)
	{	
		if(Result7>=theapp->inspctn[7].lmin && Result7<=theapp->inspctn[7].lmax)
		{inspResultOK[7]=true;}	else {inspResultOK[7]=false; typofault = 7; e7Cnt+=1;} 
	}	else	{inspResultOK[7]=true;}

	if( theapp->inspctn[8].enable)
	{	
		if(Result8>=theapp->inspctn[8].lmin && Result8<=theapp->inspctn[8].lmax)
		{inspResultOK[8]=true;} else {inspResultOK[8]=false; typofault = 8; e8Cnt+=1;} 
	}	else	{inspResultOK[8]=true;}

	if( theapp->inspctn[9].enable)
	{
		if(Result9>theapp->inspctn[9].lmin && Result9<=theapp->inspctn[9].lmax)
		{inspResultOK[9]=true;} else{inspResultOK[9]=false; typofault = 9; e9Cnt+=1;} 
	}	else	{inspResultOK[9]=true;}

	if( theapp->inspctn[10].enable)
	{
		if(Result10==0 || (Result10>theapp->inspctn[10].lmin && Result10<=theapp->inspctn[10].lmax))
		{inspResultOK[10]=true;} else{inspResultOK[10]=false; typofault = 10; e10Cnt+=1;} 
	}	else	{inspResultOK[10]=true;}
	
	if(theapp->waistAvailable)
	{
		if(theapp->inspctn[11].enable)
		{
			if(Result11>theapp->inspctn[11].lmin && Result11<=theapp->inspctn[11].lmax)
			{
				inspResultOK[11]=true;
			} 
			else
			{
				inspResultOK[11]=false; 
				typofault = 11; 
				e11Cnt+=1;
			} 
		}	
		else	
		{
			inspResultOK[11]=true;
		}
	}
	else
	{
		inspResultOK[11]=true;
	}

	//align available or square bottle job
	if(theapp->alignAvailable || theapp->jobinfo[pframe->CurrentJobNum].bottleStyle == 10)
	{
		if(theapp->inspctn[12].enable)
		{
			if(Result12>=theapp->inspctn[12].lmin && Result12<=theapp->inspctn[12].lmax)
			{
				inspResultOK[12]=true;
			}
			else
			{
				inspResultOK[12]=false;
				typofault = 12;
				e12Cnt+=1;
			}
		}
		else
		{
			inspResultOK[12]=true;
		}
	}

	//////////////////////////

	///Andrew Gawlik adding simplification for success and failure for use further in program
	//camera 5 cap inspections for 1-4, nature may be changed due to job settings
	for(int cap_insp_num = 1; cap_insp_num <= 4; cap_insp_num++)
	{
		inspect_failure[cap_insp_num] = theapp->cam5Enable && theapp->inspctn[cap_insp_num].enable && !inspResultOK[cap_insp_num];
	}

	//standard label inspections are inspections 5-10, nature may be changed due to job settings
	for(int label_insp_num = 5; label_insp_num <= 10; label_insp_num++)
	{
		inspect_failure[label_insp_num] = theapp->inspctn[label_insp_num].enable && !inspResultOK[label_insp_num];
	}

	//waist inspection is the 11th inspection
	inspect_failure[11] = theapp->waistAvailable && theapp->inspctn[11].enable && !inspResultOK[11];

	//alignment inspection for 12th inspection
	inspect_failure[12] = (theapp->alignAvailable || theapp->jobinfo[pframe->CurrentJobNum].bottleStyle == 10) && theapp->inspctn[12].enable && !inspResultOK[12];

	/// end of Andrew's additions for simplification

		if(theapp->inspctn[4].enable)
		{
			if(!inspResultOK[4])
			{
				E4Cnt += 1;
				E4Cntx += 1;
				E4Cnt24 += 1;
				goto ExitTree;
			}
			else goto CapLR;
		}
		else
		{
			E4Cnt24 = 0;
			goto CapLR;
		}
CapLR:
		if(theapp->inspctn[1].enable)	//	Cap L-R
		{
			if(!inspResultOK[1])
			{
				E1Cnt += 1;
				E1Cntx += 1;
				E1Cnt24 += 1;
				goto ExitTree;
			}
			else goto CapMid;
		}
		else
		{
			E1Cnt24 = 0;
			goto CapMid;
		}
CapMid:
		if(theapp->inspctn[2].enable)	//	Cap Mid
		{
			if(!inspResultOK[2])
			{
				E2Cnt += 1;
				E2Cntx += 1;
				E2Cnt24 += 1;
				goto ExitTree;
			}
			else goto Fault4;
		}
		else
		{
			E2Cnt24 = 0;
			goto Fault4;
		}
Fault4:
		if(theapp->inspctn[8].enable)
		{
			if(!inspResultOK[8])
			{
				E8Cnt += 1;
				E8Cntx += 1;
				E8Cnt24 += 1;
				goto ExitTree;
			}
			else goto Fault5;
		}
		else
		{
			E8Cnt24 = 0;
			goto Fault5;
		}
Fault5:
		if(theapp->inspctn[3].enable)
		{
			if(!inspResultOK[3])
			{
				E3Cnt += 1;
				E3Cntx += 1;
				E3Cnt24 += 1;
				goto ExitTree;
			}
			else goto Fault6;
		}
		else
		{
			E3Cnt24 = 0;
			goto Fault6;
		}
Fault6:
		if(theapp->inspctn[7].enable)
		{
			if(!inspResultOK[7])
			{
				E7Cnt += 1;
				E7Cntx += 1;
				E7Cnt24 += 1;
				goto ExitTree;
			}
			else goto Fault7;
		}
		else
		{
			E7Cnt24 = 0;
			goto Fault7;
		}
Fault7:
		if(theapp->inspctn[5].enable)
		{
			if(!inspResultOK[5])
			{
				E5Cnt += 1;
				E5Cntx += 1;
				E5Cnt24 += 1;
				goto ExitTree;
			}
			else goto Fault8;
		}
		else
		{
			E5Cnt24 = 0;
			goto Fault8;
		}
Fault8:
		if(theapp->inspctn[9].enable)
		{
			if(!inspResultOK[9])
			{
				E9Cnt += 1;
				E9Cntx += 1;
				E9Cnt24 += 1;
				goto ExitTree;
			}
			else goto Fault9;
		}
		else
		{
			E9Cnt24 = 0;
			goto Fault9;
		}
Fault9:
		if(theapp->inspctn[6].enable)
		{
			if(!inspResultOK[6])
			{
				E6Cnt += 1;
				E6Cntx += 1;
				E6Cnt24 += 1;
				goto ExitTree;
			}
			else goto Fault10;
		}
		else
		{
			E6Cnt24 = 0;
			goto Fault10;
		}
Fault10:
		if(theapp->inspctn[10].enable)
		{
			if(!inspResultOK[10])
			{
				E10Cnt += 1;
				E10Cntx += 1;
				E10Cnt24 += 1;
				goto ExitTree;
			}
			else goto ExitTree;
		}
		else
		{
			E10Cnt24 = 0;
			goto ExitTree;
		}
ExitTree:
	pframe->PopulateArray();
	pframe->m_pview->UpdateDisplay2();

	///###################  Sleever tracker implementation
	if(pframe->sleeverHeadUnlocked)
	{
	  //////
	

		QueryPerformanceFrequency(&m_lnC1FreqI10); QueryPerformanceCounter(&m_lnC1StartI10);//Timing

	}


		QueryPerformanceCounter(&m_lnC1EndI10); nDiff=0; dMicroSecondC1I10= 0; //Time

	if( m_lnC1StartI10.QuadPart !=0 && m_lnC1EndI10.QuadPart != 0)
	{
		dMicroSecondC1I10	= (double)(m_lnC1FreqI10.QuadPart/1000); 
		nDiff				= m_lnC1EndI10.QuadPart-m_lnC1StartI10.QuadPart; 
		dMicroSecondC1I10	= nDiff/dMicroSecondC1I10;
	}
	/////////

	////removing initialization of BadCap and BadCap2 in lieu of using simple boolean expressions to determine their values
	//BadCap=false;	BadCap2=false;

	//need to add logic in place to establish that inspection 4 will be checked with label if parameters identify it as a label inspection, otherwise as a cap inspection
	//boolean expressions for the cap inspection passing which depends on inspections being enabled and camera 5 being enabled (disabled items will cause a pass)
	/*
	inspPassCap = (
					(inspResultOK[1]||!theapp->inspctn[1].enable)&& // inspection 1 passes or is not enabled
					(inspResultOK[2]||!theapp->inspctn[2].enable)&& // inspection 2 passes or is not enabled
					(inspResultOK[3]||!theapp->inspctn[3].enable)&& // inspection 3 passes or is not enabled
					(inspResultOK[4]||!theapp->inspctn[4].enable || (theapp->jobinfo[pframe->CurrentJobNum].bottleStyle == 10)) // inspection 4 passes or is not enabled or bottle style is 10 (which converts inspection 4 to label)
				) || !theapp->cam5Enable; //or running as a 51R84
	*/
	inspPassCap = !inspect_failure[1] && !inspect_failure[2] && !inspect_failure[3] && !inspect_failure[4]; //in this case only looking when there isn't a failure, not necessarily a success (which works with disabled inspections)

	//boolean expressions for the label inspection passing which depends on inspections being enabled and for the case of inspection 11, the waist inspection being available (disabled items will cause a pass)
	/*
	inspPassLabel = (inspResultOK[4]||!theapp->inspctn[4].enable||(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle != 10)) && //inspection 4 passes or is not enabled or not bottle style 10 (which switches inspection 4 to label)
					(inspResultOK[5]||!theapp->inspctn[5].enable)&&  //inspection 5 passes or is not enabled
					(inspResultOK[6]||!theapp->inspctn[6].enable)&&  //inspection 6 passes or is not enabled
					(inspResultOK[7]||!theapp->inspctn[7].enable)&&  //inspection 7 passes or is not enabled
					(inspResultOK[8]||!theapp->inspctn[8].enable)&&  //inspection 8 passes or is not enabled
					(inspResultOK[9]||!theapp->inspctn[9].enable)&&  //inspection 9 passes or is not enabled
					(inspResultOK[10]||!theapp->inspctn[10].enable)&& //inspection 10 passes or is not enabled
					(!theapp->waistAvailable || inspResultOK[11] || !theapp->inspctn[11].enable);  //inspection 11 passes, or is not enabled or waist inspection is unavailable
	*/
	inspPassLabel = !inspect_failure[5] && !inspect_failure[6] && !inspect_failure[7] && !inspect_failure[8] && !inspect_failure[9] && !inspect_failure[10] && !inspect_failure[11] && !inspect_failure[12];

	BadCap = !inspPassCap;  // BadCap=> cap insp result taken as whether there was a cap inspection failure
	BadCap2 = inspPassCap && !inspPassLabel; //BadCap2 => label insp result, taken as whether there was a label inspection failure when cap inspection passed
	
	////removing the if statements centered around inspections, everything that was used by if statements has been calculated above, and a replacement if statement has been prepared to
	////maintain existing control flow
	/*
	if(	(
			(
				(inspResultOK[1]||!theapp->inspctn[1].enable)&&
				(inspResultOK[2]||!theapp->inspctn[2].enable)&&
				(inspResultOK[3]||!theapp->inspctn[3].enable)&&
				(inspResultOK[4]||!theapp->inspctn[4].enable)
			)
			||!theapp->cam5Enable
		)&&
		(inspResultOK[5]||!theapp->inspctn[5].enable)&&(inspResultOK[6]||!theapp->inspctn[6].enable)&&
		(inspResultOK[7]||!theapp->inspctn[7].enable)&&(inspResultOK[8]||!theapp->inspctn[8].enable)&&
		(inspResultOK[9]||!theapp->inspctn[9].enable)&&(inspResultOK[10]||!theapp->inspctn[10].enable)&&
		(
			!theapp->waistAvailable|| inspResultOK[11]|| !theapp->inspctn[11].enable
		)
		
	)
	{	BadCap=false;	BadCap2=false;	} //BadCap=>  cap insp result ... BadCap2=> label insp result
	else
	{
	*/
	if(BadCap || BadCap2)
	{
	/*
		//Replacing branches of if statements with simple logic from inspPassCap and inspPassLabel
		//BadCap=true;
		if(!inspResultOK[1] || !inspResultOK[2] || !inspResultOK[3] || !inspResultOK[4])
		{BadCap=false;	BadCap2=true;}
		else
		{BadCap=true;	BadCap2=false;}
		*/

		//****** For Picture Recall


		if((pframe->CurrentJobNum>=1)  )
		{
			if(showostore== 0) //FLag which is set to 1 when the Dialog for teh pictures is Opened  ////// 0 otherwise
			{
				CVisRGBAByteImage im_FileBW3_cam1(w[1], h[1], 1, -1, imgBuf3RGB_cam1);
				CVisRGBAByteImage im_FileBW3_cam2(w[2], h[2], 1, -1, imgBuf3RGB_cam2);
				CVisRGBAByteImage im_FileBW3_cam3(w[3], h[3], 1, -1, imgBuf3RGB_cam3);
				CVisRGBAByteImage im_FileBW3_cam4(w[4], h[4], 1, -1, imgBuf3RGB_cam4);
				CVisRGBAByteImage im_FileBW3_cam5(w[5], h[5], 1, -1, imgBuf3RGB_cam5);

				CRect c1rect = imageC.Rect();
				if(c1rect.Width()==w[1]	&& c1rect.Height()==h[1])	{imageC.CopyPixelsTo(im_FileBW3_cam1);}
				else												{AfxMessageBox("do not match1");}

				CRect c2rect = imageC2.Rect();
				if(c2rect.Width()==w[2] && c2rect.Height()==h[2])	{imageC2.CopyPixelsTo(im_FileBW3_cam2);}
				else												{AfxMessageBox("do not match2");}

				CRect c3rect = imageC3.Rect();
				if(c3rect.Width()==w[3] && c3rect.Height()==h[3])	{imageC3.CopyPixelsTo(im_FileBW3_cam3);}
				else												{AfxMessageBox("do not match3");}

				CRect c4rect = imageC4.Rect();
				if(c4rect.Width()==w[4] && c4rect.Height()==h[4])	{imageC4.CopyPixelsTo(im_FileBW3_cam4);}
				else												{AfxMessageBox("do not match4");}

				if(theapp->cam5Enable)
				{
					CRect c5rect = imageC5.Rect();
					if(c5rect.Width()==w[5] && c5rect.Height()==h[5]){imageC5.CopyPixelsTo(im_FileBW3_cam5);}
					else											{AfxMessageBox("do not match5");}
				}

				/////Introducing the time stamp 
				SYSTEMTIME sysTime;
				GetLocalTime(&sysTime);//char timeStr[255];
			
				*(imgBuf3RGB_cam1 )=typofault;//Inspection which failed

				tim_stamp[imge_no][0]=1;
				tim_stamp[imge_no][1]=sysTime.wHour;
				tim_stamp[imge_no][2]=sysTime.wMinute;
				tim_stamp[imge_no][3]=sysTime.wSecond;

				imge_no++;
				////////////////////////////////////////////////


				// CAmera 1 Pointer Increment
				if(imgBuf3RGB_cam1 != arr1[0]+((MAXNUMDEFPIC-1)*(w[1]*h[1]*4)))//MAXNUMDEFPIC images Limit
				{imgBuf3RGB_cam1+=(w[1]*h[1]*4);}
				else ///Else Default to the 1st address. &&&& Default imge_no to 0
				{imgBuf3RGB_cam1 = arr1[0]; imge_no=0; for(int a=0;a<1000;a++){tim_stamp[a][0]=0;} }	
			
				// CAmera 2 Pointer Increment
				if(imgBuf3RGB_cam2 != arr1[1]+((MAXNUMDEFPIC-1)*(w[2]*h[2]*4)))//MAXNUMDEFPIC images Limit
				{imgBuf3RGB_cam2+=(w[2]*h[2]*4);}
				else {imgBuf3RGB_cam2 = arr1[1];}

				// CAmera 3 Pointer Increment
				if(imgBuf3RGB_cam3 != arr1[2]+((MAXNUMDEFPIC-1)*(w[3]*h[3]*4)))//MAXNUMDEFPIC images Limit
				{imgBuf3RGB_cam3+=(w[3]*h[3]*4);}
				else {imgBuf3RGB_cam3=arr1[2];}

				// CAmera 4 Pointer Increment
				if(imgBuf3RGB_cam4 != arr1[3]+((MAXNUMDEFPIC-1)*(w[4]*h[4]*4)))//MAXNUMDEFPIC images Limit
				{imgBuf3RGB_cam4+=(w[4]*h[4]*4);}
				else {imgBuf3RGB_cam4=arr1[3];}

				// CAmera 5 Pointer Increment
				if(imgBuf3RGB_cam5 != arr1[4]+((MAXNUMDEFPIC-1)*(w[5]*h[5]*4)))//MAXNUMDEFPIC images Limit
				{imgBuf3RGB_cam5+=(w[5]*h[5]*4);}
				else {imgBuf3RGB_cam5=arr1[4];}

			}
		}	


		//to save 50 sets of pictures
		//if(theapp->saveDefects && (Result10==0 || !inspResultOK[10] ) )	
		if(theapp->saveDefects)
		{
			BYTE* im1 = pframe->m_pcamera->m_imageProcessed[0][pframe->m_pcamera->readNext[0]]->GetData();
			BYTE* im2 = pframe->m_pcamera->m_imageProcessed[1][pframe->m_pcamera->readNext[1]]->GetData();
			BYTE* im3 = pframe->m_pcamera->m_imageProcessed[2][pframe->m_pcamera->readNext[2]]->GetData();
			BYTE* im4 = pframe->m_pcamera->m_imageProcessed[3][pframe->m_pcamera->readNext[3]]->GetData();
			BYTE* im5 = pframe->m_pcamera->m_imageProcessed[4][pframe->m_pcamera->readNext[4]]->GetData();
			OnSaveDefSet(im1, im2, im3, im4, im5,pframe->picDefNum);

			pframe->picDefNum++;

			if(pframe->picDefNum>=50) {pframe->picDefNum=1; theapp->saveDefects=false;}
		}
	}
	
	if(RejectAll) BadCap=true;

	pframe->SendMessage(WM_INSPECT,NULL,NULL);

	TimesThru+=1;
	
	if(TimesThru >=5) {StartLockOut=false; TimesThru=6;}

	if(!StartLockOut)
	{
		if(inspResultOK[1] && theapp->inspctn[1].enable)
		{
			LHInc+=1;
			LHArray[LHInc]=Result1r;
			
			if(LHInc>=10)
			{ 
				LHInc=0; 
				LHtf=(LHArray[1]+LHArray[2]+LHArray[3]+LHArray[4]+LHArray[5]+LHArray[6]+LHArray[7]+LHArray[8]+LHArray[9]+LHArray[10]);
				LHt=LHtf/10;
				
				LHsd=sqrt(   (	((LHArray[1] * LHArray[1])+(LHArray[2] * LHArray[2])+
								 (LHArray[3] * LHArray[3])+(LHArray[4] * LHArray[4])+
								 (LHArray[5] * LHArray[5])+(LHArray[6] * LHArray[6])+
								 (LHArray[7] * LHArray[7])+(LHArray[8] * LHArray[8])+
								 (LHArray[9] * LHArray[9])+(LHArray[10]* LHArray[10]))/10)-(LHt*LHt) );//-LHti
			
				if(LHsd<=0) {LHsd=0;}													
			}

			RHInc+=1;
			RHArray[RHInc]=Result1l;
			
			if(RHInc>=10)
			{ 
				RHInc=0; 
				RHtf=(RHArray[1]+RHArray[2]+RHArray[3]+RHArray[4]+RHArray[5]+RHArray[6]+RHArray[7]+RHArray[8]+RHArray[9]+RHArray[10]);
				RHt=RHtf/10;
				
				RHsd=sqrt(   (	((RHArray[1] * RHArray[1])+(RHArray[2] * RHArray[2])+
								 (RHArray[3] * RHArray[3])+(RHArray[4] * RHArray[4])+
								 (RHArray[5] * RHArray[5])+(RHArray[6] * RHArray[6])+
								 (RHArray[7] * RHArray[7])+(RHArray[8] * RHArray[8])+
								 (RHArray[9] * RHArray[9])+(RHArray[10]* RHArray[10]))/10)-(RHt*RHt) );//-LHti
				
				if(RHsd<=0) {RHsd=0;}													
			}
		}

		if(inspResultOK[2] && theapp->inspctn[2].enable)
		{
			DInc+=1;
			DArray[DInc]=Result2;
			if(DInc>=10)
			{ 
				DInc=0; 
				Dtf=(DArray[1]+DArray[2]+DArray[3]+DArray[4]+DArray[5]+DArray[6]+DArray[7]+DArray[8]+DArray[9]+DArray[10]);
				Dt=Dtf/10;
				
				Dsd=sqrt(   (	((DArray[1] * DArray[1])+(DArray[2] * DArray[2])+
								 (DArray[3] * DArray[3])+(DArray[4] * DArray[4])+
								 (DArray[5] * DArray[5])+(DArray[6] * DArray[6])+
								 (DArray[7] * DArray[7])+(DArray[8] * DArray[8])+
								 (DArray[9] * DArray[9])+(DArray[10]* DArray[10]))/10)-(Dt*Dt) );//-LHti
				
				if(Dsd<=0) {Dsd=0;}													
			}
		}


		if(inspResultOK[3] && theapp->inspctn[3].enable)
		{
			FInc+=1;
			FArray[FInc]=Result3;
			if(FInc>=10)
			{ 
				FInc=0; 
				Ftf=(FArray[1]+FArray[2]+FArray[3]+FArray[4]+FArray[5]+FArray[6]+FArray[7]+FArray[8]+FArray[9]+FArray[10]);
				Ft=Ftf/10;
				
				Fsd=sqrt(   (	((FArray[1] * FArray[1])+(FArray[2] * FArray[2])+
								 (FArray[3] * FArray[3])+(FArray[4] * FArray[4])+
								 (FArray[5] * FArray[5])+(FArray[6] * FArray[6])+
								 (FArray[7] * FArray[7])+(FArray[8] * FArray[8])+
								 (FArray[9] * FArray[9])+(FArray[10] * FArray[10]))/10)-(Ft*Ft) );//-LHti
				
				if(Fsd<=0)	{Fsd=0;}													
			}
		}

		if(inspResultOK[4] && theapp->inspctn[4].enable)
		{
			LInc+=1;
			LArray[LInc]=Result4;
			
			if(LInc>=10)
			{ 
				LInc=0; 
				Ltf=(LArray[1]+LArray[2]+LArray[3]+LArray[4]+LArray[5]+LArray[6]+LArray[7]+LArray[8]+LArray[9]+LArray[10]);
				Lt=Ltf/10;
				
				Lsd=sqrt(   (	((LArray[1] * LArray[1])+(LArray[2] * LArray[2])+
								 (LArray[3] * LArray[3])+(LArray[4] * LArray[4])+
								 (LArray[5] * LArray[5])+(LArray[6] * LArray[6])+
								 (LArray[7] * LArray[7])+(LArray[8] * LArray[8])+
								 (LArray[9] * LArray[9])+(LArray[10]* LArray[10]))/10)-(Lt*Lt) );//-LHti
				
				if(Lsd<=0)	{Lsd=0;}													
			}
		}

		if(inspResultOK[5] && theapp->inspctn[5].enable)
		{
			TBInc+=1;
			TBArray[TBInc]=Result5;

			if(TBInc>=10)
			{ 
				TBInc=0; 
				TBtf=(TBArray[1]+TBArray[2]+TBArray[3]+TBArray[4]+TBArray[5]+TBArray[6]+TBArray[7]+TBArray[8]+TBArray[9]+TBArray[10]);
				TBt=TBtf/10;
				
				TBsd=sqrt(   (	((TBArray[1] * TBArray[1])+(TBArray[2] * TBArray[2])+
								 (TBArray[3] * TBArray[3])+(TBArray[4] * TBArray[4])+
								 (TBArray[5] * TBArray[5])+(TBArray[6] * TBArray[6])+
								 (TBArray[7] * TBArray[7])+(TBArray[8] * TBArray[8])+
								 (TBArray[9] * TBArray[9])+(TBArray[10] * TBArray[10]))/10)-(TBt*TBt) );//-LHti
				
				if(TBsd<=0)	{TBsd=0;}													
			}
		}

		if(inspResultOK[6] && theapp->inspctn[6].enable)
		{
			TB2Inc+=1;
			TB2Array[TB2Inc]=Result6;

			if(TB2Inc>=10)
			{ 
				TB2Inc=0; 
				TB2tf=(TB2Array[1]+TB2Array[2]+TB2Array[3]+TB2Array[4]+TB2Array[5]+TB2Array[6]+TB2Array[7]+TB2Array[8]+TB2Array[9]+TB2Array[10]);
				TB2t=TB2tf/10;
				
				TB2sd=sqrt( (	((TB2Array[1] * TB2Array[1])+(TB2Array[2] * TB2Array[2])+
								 (TB2Array[3] * TB2Array[3])+(TB2Array[4] * TB2Array[4])+
								 (TB2Array[5] * TB2Array[5])+(TB2Array[6] * TB2Array[6])+
								 (TB2Array[7] * TB2Array[7])+(TB2Array[8] * TB2Array[8])+
								 (TB2Array[9] * TB2Array[9])+(TB2Array[10] * TB2Array[10]))/10)-(TB2t*TB2t) );//-LHti
				
				if(TB2sd<=0) {TB2sd=0;}													
			}
		}

		if(inspResultOK[7] && theapp->inspctn[7].enable)
		{
			PInc+=1;
			PArray[PInc]=Result7;

			if(PInc>=10)
			{ 
				PInc=0; 
				Ptf=(PArray[1]+PArray[2]+PArray[3]+PArray[4]+PArray[5]+PArray[6]+PArray[7]+PArray[8]+PArray[9]+PArray[10]);
				Pt=Ptf/10;
				
				Psd=sqrt(   (	((PArray[1] * PArray[1])+(PArray[2] * PArray[2])+
								 (PArray[3] * PArray[3])+(PArray[4] * PArray[4])+
								 (PArray[5] * PArray[5])+(PArray[6] * PArray[6])+
								 (PArray[7] * PArray[7])+(PArray[8] * PArray[8])+
								 (PArray[9] * PArray[9])+(PArray[10] * PArray[10]))/10)-(Pt*Pt) );//-LHti
				
				if(Psd<=0)	{Psd=0;}													
			}
		}

		if(inspResultOK[8] && theapp->inspctn[8].enable)
		{
			NoInc+=1;
			NoArray[NoInc]=Result8;
			
			if(NoInc>=10)
			{ 
				NoInc=0; 
				Notf=(NoArray[1]+NoArray[2]+NoArray[3]+NoArray[4]+NoArray[5]+NoArray[6]+NoArray[7]+NoArray[8]+NoArray[9]+NoArray[10]);
				Not=Notf/10;
				
				Nosd=sqrt(   (	((NoArray[1] * NoArray[1])+(NoArray[2] * NoArray[2])+
								 (NoArray[3] * NoArray[3])+(NoArray[4] * NoArray[4])+
								 (NoArray[5] * NoArray[5])+(NoArray[6] * NoArray[6])+
								 (NoArray[7] * NoArray[7])+(NoArray[8] * NoArray[8])+
								 (NoArray[9] * NoArray[9])+(NoArray[10] * NoArray[10]))/10)-(Not*Not) );//-LHti
				
				if(Nosd<=0)	{Nosd=0;}													
			}
		}

		if(inspResultOK[9] && theapp->inspctn[9].enable)
		{
			LCInc+=1;
			LCArray[LCInc]=Result9;
			
			if(LCInc>=10)
			{ 
				LCInc=0; 
				LCtf=(LCArray[1]+LCArray[2]+LCArray[3]+LCArray[4]+LCArray[5]+LCArray[6]+LCArray[7]+LCArray[8]+LCArray[9]+LCArray[10]);
				LCt=LCtf/10;
				
				LCsd=sqrt(   ( ( (LCArray[1] * LCArray[1])+(LCArray[2] * LCArray[2])+
								 (LCArray[3] * LCArray[3])+(LCArray[4] * LCArray[4])+
								 (LCArray[5] * LCArray[5])+(LCArray[6] * LCArray[6])+
								 (LCArray[7] * LCArray[7])+(LCArray[8] * LCArray[8])+
								 (LCArray[9] * LCArray[9])+(LCArray[10] * LCArray[10]))/10)-(LCt*LCt) );//-LHti
				
				if(LCsd<=0)	{LCsd=0;}													
			}
		}

		if(inspResultOK[10] && theapp->inspctn[10].enable)
		{
			TLInc+=1;
			TLArray[TLInc]=Result10;

			if(TLInc>=10)
			{ 
				TLInc=0; 
				TLtf=(TLArray[1]+TLArray[2]+TLArray[3]+TLArray[4]+TLArray[5]+TLArray[6]+TLArray[7]+TLArray[8]+TLArray[9]+TLArray[10]);
				TLt=TLtf/10;
				
				TLsd=sqrt(   (	((TLArray[1] * TLArray[1])+(TLArray[2] * TLArray[2])+
								 (TLArray[3] * TLArray[3])+(TLArray[4] * TLArray[4])+
								 (TLArray[5] * TLArray[5])+(TLArray[6] * TLArray[6])+
								 (TLArray[7] * TLArray[7])+(TLArray[8] * TLArray[8])+
								 (TLArray[9] * TLArray[9])+(TLArray[10] * TLArray[10]))/10)-(TLt*TLt) );//-LHti
				
				if(TLsd<=0)	{TLsd=0;}													
			}
		}
		
		if(inspResultOK[11] && theapp->inspctn[11].enable)
		{
			WsInc+=1;
			WsArray[WsInc]=Result11;

			if(WsInc>=10)
			{ 
				WsInc=0; 
				Wstf=(WsArray[1]+WsArray[2]+WsArray[3]+WsArray[4]+WsArray[5]+WsArray[6]+WsArray[7]+WsArray[8]+WsArray[9]+WsArray[10]);
				Wst=Wstf/10;
				
				Wssd=sqrt(   (	((WsArray[1] * WsArray[1])+(WsArray[2] * WsArray[2])+
								 (WsArray[3] * WsArray[3])+(WsArray[4] * WsArray[4])+
								 (WsArray[5] * WsArray[5])+(WsArray[6] * WsArray[6])+
								 (WsArray[7] * WsArray[7])+(WsArray[8] * WsArray[8])+
								 (WsArray[9] * WsArray[9])+(WsArray[10] * WsArray[10]))/10)-(Wst*Wst) );//-LHti
				
				if(Wssd<=0)	{Wssd=0;}													
			}
		}
	}

	//////////////////////////////////////

	//////////////////////////////////////
	
	if(!pframe->Freeze)			{InvalidateRect(NULL,false);}
	else
	{
		if(pframe->FreezeReset)	{InvalidateRect(NULL,false);}
		
		if(BadCap||BadCap2) {InvalidateRect(NULL,false); pframe->FreezeReset=false;}
		else							{pframe->m_pview->UpdateDisplay2();}
	}

	//////////////////////////////////////	
	
	QueryPerformanceCounter(&timeEnd);
	
	if(timeStart.QuadPart!=0 && timeEnd.QuadPart!=0)
	{
		spanElapsed	= (double)(Frequency.QuadPart/1000); 
		nDiff = timeEnd.QuadPart-timeStart.QuadPart;	
		spanElapsed = nDiff/spanElapsed;
	}	

	//////////////////////////////////////

	first=true;
	lockOut=false;
}


void CXAxisView::LearnPattern2(BYTE *im_srcIn, SinglePoint pos)
{
	int xpos=0;		int ypos=0;
	int xpos2=0;	int ypos2=0;
	int xpos3=0;
		
	int LineLength=CroppedCameraHeight/3;
	int LineLength2=12*4;
	int LineLength3=12;
	int cnt=0;

	for(ypos=(pos.y)-6; ypos<(pos.y)+6; ypos++)
	{
		for(xpos=(pos.x)-6;xpos<(pos.x)+6;xpos++)
		{	
			//for comparison later
			*(im_mat + xpos3 + LineLength3 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
			//only for show
			*(im_pat + xpos2 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
			*(im_pat + xpos2 +1 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
			*(im_pat + xpos2 +2 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
			xpos2+=4;
			xpos3+=1;
			cnt+=1;
		}

		xpos3=0;
		xpos2=0;
		ypos2+=1;
	}

	SavePattern2(im_mat);
	LearnDone=true;
}



void CXAxisView::LearnPattern(BYTE *im_srcIn, SinglePoint side, bool left)
{
	int xpos=0;		int ypos=0;
	int xpos2=0;	int ypos2=0;
	int xpos3=0;

	int LineLength	=	CroppedCameraWidth/2;
	int Height		=	CroppedCameraHeight/2;

	int LineLength2	=	28*4;
	int LineLength3	=	28;

	int cnt=0;
	int neckBarY=theapp->jobinfo[pframe->CurrentJobNum].neckBarY;


	//if(NeckLSide.x > 30 && NeckLSide.x <1000 )
	//{
	if(left)
	{
		for(ypos=neckBarY-28; ypos<neckBarY; ypos++)
		{
			for(xpos=side.x-24;xpos<side.x+4;xpos++)
			{	
				//for comparison later
				*(im_matchl+ xpos3 + 0 + LineLength3*ypos2)	= *(im_srcIn + xpos + LineLength*ypos);
				
				//only for show - COLOR
				*(im_patl2 + xpos2 + 0 + LineLength2*ypos2)	= *(im_srcIn + xpos + LineLength*ypos);
				*(im_patl2 + xpos2 + 1 + LineLength2*ypos2)	= *(im_srcIn + xpos + LineLength*ypos);
				*(im_patl2 + xpos2 + 2 + LineLength2*ypos2)	= *(im_srcIn + xpos + LineLength*ypos);

				xpos2+=4;
				xpos3+=1;
				cnt+=1;
			}

			xpos3=0; xpos2=0; ypos2+=1;
		}

		//SavePattern(im_matchl,true);
		SavePatternCap(im_matchl, true);
	}
	else
	{
		for(ypos=neckBarY-28; ypos<neckBarY; ypos++)
		{
			for(xpos=side.x-4;xpos<side.x+24;xpos++)
			{	
				//for comparison later
				*(im_matchr+ xpos3 + 0 + LineLength3 *ypos2)= *(im_srcIn + xpos + LineLength*ypos);

				//only for show - COLOR
				*(im_patr2x + xpos2 + 0 + LineLength2*ypos2)= *(im_srcIn + xpos + LineLength*ypos);
				*(im_patr2x + xpos2 + 1 + LineLength2*ypos2)= *(im_srcIn + xpos + LineLength*ypos);
				*(im_patr2x + xpos2 + 2 + LineLength2*ypos2)= *(im_srcIn + xpos + LineLength*ypos);
				
				xpos2+=4; xpos3+=1; cnt+=1;
			}

			xpos3=0; xpos2=0; ypos2+=1;
		}

		//SavePattern(im_matchr,false);
		SavePatternCap(im_matchr, false);
	}

	//}
	LearnDone=true;

}


SpecialPoint CXAxisView::FindNeckLipPat(BYTE* im_srcIn, SinglePoint side, bool left)
{
	int nCam	=	5;
	SpecialPoint result;
	result.x=result.y=result.c=0;

	int r,q;
	
	int		LineLength	= theapp->camWidth[nCam]/2;
	int		Height		= theapp->camHeight[nCam]/2;
	int		LineLength2 = 28;
	float	n			= LineLength2*LineLength2;

	bool debugging=false;

	if(debugging)
	{SaveBWCustom1(im_srcIn, LineLength, Height);}

	int xpos1=1;	int ypos1=1;
	int xpos2=0;	int ypos2=0;
	int xpos3=0;

	float sumxy=0;
	float sumx=0;
	float sumy=0;
	float sumxsq=0;
	float sumysq=0;

	float f0=0;
	float f1=0;
	float f2=0;

	float corr=0;
	float savedcorr=0.3f;

	int		savedpos = -50;
	int		neckBarY = theapp->jobinfo[pframe->CurrentJobNum].neckBarY;

	if(neckBarY+14+5>Height) {neckBarY = Height - 19;}
	
	if(side.x>24 && side.x<LineLength-24 && neckBarY-44>0)
	{
		xpos1=side.x;

		if(left)
		{
			for(q=-30; q<=5; q++)
			{
				for(ypos1=neckBarY-14; ypos1<neckBarY+14; ypos1++)
				{
					for(xpos3=xpos1-24;xpos3<xpos1+4;xpos3++)
					{					
						*(im_match2+ xpos2+ LineLength2*ypos2)= *(im_srcIn+ xpos3 + LineLength * (ypos1+q));
						xpos2+=1;
					}

					xpos2=0; ypos2+=1;
				}

				ypos2=0;
				
				//Calculate Correlation
				//OnSaveBW(im_match2, 6);
				
				sumxy=0.0; sumx=0.0; sumy=0.0; sumxsq=0.0; sumysq=0.0;

				for(r=0; r<n; r++)
				{
					sumxy+= (*(im_matchl+r)) * (*(im_match2+r));
					sumx+= *(im_matchl+r);
					sumy+= *(im_match2+r);
					sumxsq+= (*(im_matchl+r)) * (*(im_matchl+r));
					sumysq+= (*(im_match2+r)) * (*(im_match2+r));
				}

				f0=sumxy-((sumx*sumy)/n);
				f1=sumxsq-((sumx*sumx)/n);
				f2=sumysq-((sumy*sumy)/n);
				
				if( sqrtf(f1*f2)!=0.0 ) {corr=f0/sqrtf(f1*f2);}
				else					{corr=0;}

				if(corr>=savedcorr)
				{result.c=savedcorr=corr; savedpos=q;}
				
				result.x=side.x;
				result.y=savedpos+neckBarY;
			}			
		}

		else
		{
			for(q=-30; q<=5; q++)
			{
				for(ypos1=neckBarY-14; ypos1<neckBarY+14; ypos1++)
				{
					for(xpos3=xpos1-4;xpos3<xpos1+24;xpos3++)
					{					
						*(im_match2+ xpos2+ LineLength2*ypos2)= *(im_srcIn+ xpos3 + LineLength * (ypos1+q));
						xpos2+=1;
					}

					xpos2=0; ypos2+=1;
				}

				ypos2=0;

				//Calculate Correlation
				sumxy=0.0; sumx=0.0; sumy=0.0; sumxsq=0.0; sumysq=0.0;

				for(r=0; r < n; r++)
				{
					sumxy+= (*(im_matchr+r)) * (*(im_match2+r));
					sumx+= *(im_matchr+r);
					sumy+= *(im_match2+r);
					sumxsq+= (*(im_matchr+r)) * (*(im_matchr+r));
					sumysq+= (*(im_match2+r)) * (*(im_match2+r));
				}

				f0=sumxy-((sumx*sumy)/n);
				f1=sumxsq-((sumx*sumx)/n);
				f2=sumysq-((sumy*sumy)/n);

				if( sqrtf(f1*f2)!=0.0 )	{corr=f0/sqrtf(f1*f2);}
				else					{corr=0;}

				if(corr>=savedcorr)		{result.c=savedcorr=corr;	savedpos=q;}
				
				result.x=side.x;
				result.y=savedpos+neckBarY;
			}			
		}
	}

	result.c = 100*result.c;

	return result;
}


double CXAxisView::GetMin(int numvals, double a, double b, double c, double d)
{
	double result=0;
	double t1, t2, t3, t4;	t1=t2=t3=t4=0;

	switch(numvals)
	{
		case 1:	result=a;											break;
		case 2:	result=__min(a,b);									break;
		case 3:	t1=__min(a,b); result=__min(t1, c);					break;
		case 4:	t1=__min(a,b); t2=__min(t1, c); result=__min(t2,d);	break;
	}

	return result;
}

double CXAxisView::GetMax(int numvals, double a, double b, double c, double d)
{
	double result=0;
	double t1, t2, t3, t4, t5;	t1=t2=t3=t4=t5=0;

	switch(numvals)
	{
		case 1:	result=a;											break;
		case 2:	result=__max(a,b);									break;
		case 3:	t1=__max(a,b); result=__max(t1, c);					break;
		case 4:	t1=__max(a,b); t2=__max(t1, c); result=__max(t2,d); break;
	}

	return result;
}


void CXAxisView::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1)	{KillTimer(1);DoResults();}
	if (nIDEvent==2) 	{KillTimer(2);}
	
	CView::OnTimer(nIDEvent);
}

void CXAxisView::OnLoad2(int Job)
{
	//CVisByteImage image(theapp->sizepattBy3_2W, theapp->sizepattBy3_2H, 1, -1, pPatDivBy3);
	CVisByteImage image;
	
	char buf[10];
	char currentpath[60];
	char currentpath2[60];
	char* pFileName = "c:\\silgandata\\pat\\";//drive
	CString c;
	
	c=itoa(Job,buf,10);
		
	strcpy(currentpath,pFileName);
	strcat(currentpath,c);
	strcat(currentpath,"\\");
	strcat(currentpath,c);
	strcat(currentpath,".bmp");

	strcpy(currentpath2,pFileName);
	strcat(currentpath2,c);
	strcat(currentpath2,"\\image");
	strcat(currentpath2,".txt");
	int s=0;
	char bufDig[2];
	CString N1;

	//try loading the bmp first then the text file as a backup
	int pxWidth;  //pixel width
	int pxHeight; //pixel height
	BYTE pxValue; //pixel value
	try
	{

		image.ReadFile(currentpath);
		pxWidth = image.Width();
		pxHeight = image.Height();
		
		free(im_pat);
		free(im_mat);
		im_mat = (BYTE*)malloc(pxWidth * pxHeight);
		im_pat = (BYTE*)malloc(pxWidth * pxHeight*4);

		//treat bitmap image as single channel

		for(int col=0; col<pxWidth; col++)
		{
			for(int  row=0; row<pxHeight; row++)
			{
				pxValue = image.Pixel(col,row); //assuming only one band since image needs to be grayscale

				*(im_mat+col+row*pxWidth) = pxValue;
				
				*(im_pat+0+4*(col+row*pxWidth)) = pxValue;
				*(im_pat+1+4*(col+row*pxWidth)) = pxValue;
				*(im_pat+2+4*(col+row*pxWidth)) = pxValue;
			}
		}

		pframe->JobNoGood=false;
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		CFileException ce;
		bool success=ImageFile.Open(currentpath2, CFile::modeRead, &ce) > 0;
		
		if(success!=0)
		{
			CString szDigit;
			int digit;
			 
			for(int g=0; g<144; g++) // 12x12
			{
				ImageFile.Read(bufDig,3);
				N1=bufDig[0];
				N1+=bufDig[1];
				N1+=bufDig[2];
				//ImageFile.Seek(1, CFile::current );
				digit=atoi(N1);
				//szDigit.Format("%3i",digit);
				N1="";
				*(im_mat+g)=digit;	
				*(im_pat+s)=digit;
				*(im_pat+s+1)=digit;
				*(im_pat+s+2)=digit;
				s=s+4;
			}

			ImageFile.Close();
			pframe->JobNoGood=false;
		}
		else
		{
			AfxMessageBox("The label pattern needs to be taught first");
			pframe->JobNoGood=true;

			//AfxMessageBox("Image file 2 could not be opened. You will need to setup a new job");
		}
	}

}


void CXAxisView::OnLoad3(int Job)
{
	CVisByteImage image;
	
	char buf[10];
	char currentpath[60];
	char currentpath2[60];
	char* pFileName = "c:\\silgandata\\pat\\";//drive
	CString c;
	
	c=itoa(Job,buf,10);
		
	strcpy(currentpath,pFileName);
	strcat(currentpath,c);
	strcat(currentpath,"\\");
	strcat(currentpath,c);
	strcat(currentpath,".bmp");

	strcpy(currentpath2,pFileName);
	strcat(currentpath2,c);
	strcat(currentpath2,"\\image3");
	strcat(currentpath2,".txt");
	int s=0;
	char bufDig[2];
	CString N1;
	
	//try loading the bmp first then the text file as a backup
	int pxWidth;  //pixel width
	int pxHeight; //pixel height
	BYTE pxValue; //pixel value
	try
	{
		image.ReadFile(currentpath);
		pxWidth = image.Width();
		pxHeight = image.Height();
		
		//treat bitmap image as single channel
		free(im_pat3);
		free(im_mat3);
		im_mat3 = (BYTE*)malloc(pxWidth * pxHeight);
		im_pat3 = (BYTE*)malloc(pxWidth * pxHeight * 4);

		for(int col=0; col<pxWidth; col++)
		{
			for(int  row=0; row<pxHeight; row++)
			{
				pxValue = image.Pixel(col,row); //assuming only one band since image needs to be grayscale

				*(im_mat3+col+row*pxWidth) = pxValue;
				
				*(im_pat3+0+4*col+row*pxWidth) = pxValue;
				*(im_pat3+1+4*col+row*pxWidth) = pxValue;
				*(im_pat3+2+4*col+row*pxWidth) = pxValue;
			}
		}

		pframe->JobNoGood=false;
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		CFileException ce;
		bool success=ImageFile.Open(currentpath2, CFile::modeRead, &ce) > 0;

		if(success!=0)
		{
			CString szDigit;
			int digit;

			for(int g=0; g<144; g++)
			{
				ImageFile.Read(bufDig,3);
				N1=bufDig[0];
				N1+=bufDig[1];
				N1+=bufDig[2];
				//ImageFile.Seek(1, CFile::current );
				digit=atoi(N1);
				//szDigit.Format("%3i",digit);
				N1="";
				*(im_mat3+g)=digit;	
				*(im_pat3+s)=digit;
				*(im_pat3+s+1)=digit;
				*(im_pat3+s+2)=digit;
				s=s+4;
			}

			ImageFile.Close();
			pframe->JobNoGood=false;
		}

		else
		{
			//AfxMessageBox("Ignore this message for now");
			//pframe->JobNoGood=true;
		}
	}
}

void CXAxisView::OnLoadCap(int Job, bool left)
{
	CVisByteImage image;
	
	char buf[10];
	char currentpath[60]; //path for bmp
	char currentpath2[60];
	char* pFileName = "c:\\silgandata\\pat\\";//drive
	CString c;
	BYTE *cap_match; //pointer for cap match (used for reference only not for allocation/deallocation)
	BYTE *cap_pattern; //pointer for cap pattern (used for reference only not for allocation/deallocation)
	
	c=itoa(Job,buf,10);
	
	strcpy(currentpath,pFileName);
	strcat(currentpath,c);

	strcpy(currentpath2,pFileName);
	strcat(currentpath2,c);


	///to reduce branching in the below loop, changing pointer references here -- Andrew Gawlik 2017-07-18
	if(left)
	{
		cap_match = im_matchl;
		cap_pattern = im_patl2;

		strcat(currentpath,"\\capimagel");
		strcat(currentpath2,"\\capimagel");
	}
	else
	{
		cap_match = im_matchr;
		cap_pattern = im_patr2x;

		strcat(currentpath,"\\capimager");
		strcat(currentpath2,"\\capimager");
	}

	strcat(currentpath,".bmp");
	strcat(currentpath2,".txt");

	//try loading the bmp first then the text file as a backup
	int pxWidth;  //pixel width
	int pxHeight; //pixel height
	BYTE pxValue; //pixel value
	try
	{
		image.ReadFile(currentpath);
		pxWidth = image.Width();
		pxHeight = image.Height();
		
		//treat bitmap image as single channel

		for(int col=0; col<pxWidth; col++)
		{
			for(int  row=0; row<pxHeight; row++)
			{
				pxValue = image.Pixel(col,row); //assuming only one band since image needs to be grayscale

				*(cap_match+col+row*pxWidth) = pxValue;
				
				*(cap_pattern+0+4*(col+row*pxWidth)) = pxValue;
				*(cap_pattern+1+4*(col+row*pxWidth)) = pxValue;
				*(cap_pattern+2+4*(col+row*pxWidth)) = pxValue;
			}
		}

		LearnDone=true;
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		
		//if(left)	{strcat(currentpath2,"\\capimagel");}
		//else			{strcat(currentpath2,"\\capimager");}
		
		//strcat(currentpath2,".txt");
		int s=0;
		char bufDig[2];
		CString N1;
		
		bool success=CapImage.Open(currentpath2, CFile::modeRead) > 0;//| CFile::modeCreate

		if(success!=0)
		{
			CString szDigit;
			int digit;
				
			for(int g=0; g<784; g++)
			{	
				CapImage.Read(bufDig,3);
				N1=bufDig[0];
				N1+=bufDig[1];
				N1+=bufDig[2];
					//ImageFile.Seek(1, CFile::current );
				digit=atoi(N1);
					//szDigit.Format("%3i",digit);
				N1="";

				///replacing excess branching with existing pointers

				*(cap_match+g) = digit;
				*(cap_pattern+s) = digit;
				*(cap_pattern+s+1) = digit;
				*(cap_pattern+s+2) = digit;

				//if(left)
				//{
				//	*(im_matchl+g)	= digit;	
				//	*(im_patl2+s)	= digit;
				//	*(im_patl2+s+1)	= digit;
				//	*(im_patl2+s+2)	= digit;
				//}
				//else
				//{
				//	*(im_matchr+g)	= digit;	
				//	*(im_patr2x+s)	= digit;
				//	*(im_patr2x+s+1)= digit;
				//	*(im_patr2x+s+2)= digit;
				//}

				s+=4;
			}

			CapImage.Close();
			LearnDone=true;
		}
		else
		{
			// Warn the user.
			AfxMessageBox("The cap needs to be taught first");
			pframe->JobNoGood=true;
		}
	}

	cap_pattern = NULL;
	cap_match = NULL;
}

void CXAxisView::SavePattern(BYTE *im_sr)
{
	char buf[10];
	char currentpath[60];
	char* pFileName = "c:\\silgandata\\pat\\";//drive
	CString c;
	c =itoa(pframe->CurrentJobNum,buf,10);

	strcpy(currentpath,pFileName);
	strcat(currentpath,c);
	strcat(currentpath,"\\");
	strcat(currentpath,c);
	strcat(currentpath,".bmp");
	
	CVisByteImage image(12,12,1,-1,im_sr);
	imagepat=image;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imagepat.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{AfxMessageBox("The pat file could not be saved.");}// Warn the user.
}


void CXAxisView::SavePattern2(BYTE *im_sr)
{
	char buf[10];
//	char buf2[200];
	char currentpath[60];
	char currentpath2[60];
	char* pFileName = "c:\\silgandata\\pat\\";//drive
	CString c;

	c =itoa(pframe->CurrentJobNum,buf,10);
	
	strcpy(currentpath,pFileName);
	strcat(currentpath,c);

	///commenting out text file save
	strcpy(currentpath2,pFileName);
	strcat(currentpath2,c);
	strcat(currentpath2,"\\image");
	strcat(currentpath2,".txt");

	ImageFile.Open(currentpath2, CFile::modeWrite | CFile::modeCreate);
	
	CString szDigit;
	int digit;

	for(int g=0; g<144; g++)
	{
		digit=*(im_sr+g);
		szDigit.Format("%3i",digit);
		//szDigit=itoa(digit,bufd,10);
		ImageFile.Write(szDigit,3);
		//ImageFile.Write(",",1);
		//ImageFile.Write(szDigit,szDigit.GetLength());
	}
	
	ImageFile.Close();
     
	//ImageFile.Open(currentpath2, CFile::modeRead | CFile::modeCreate);
	//UINT lBytesRead = ImageFile.Read (szSampleText,100); 
	//ImageFile.Read(m_r1,m_r1.GetLength());
	//ImageFile.Close();

	strcat(currentpath,"\\");
	strcat(currentpath,c);
	strcat(currentpath,".bmp");

	//sprintf(buf2,currentpath2,im_sr);
	//writeOutGrayArrayFile(currentpath2,
	//				 12,
	//				 12,
	//				 im_sr);

	CVisByteImage image(12,12,1,-1,im_sr);
	imagepat=image;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imagepat.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{AfxMessageBox("The pat file could not be saved.");}// Warn the user.
}


void CXAxisView::SavePattern3(BYTE *im_sr)
{
	char buf[10];
	//char buf2[200];
	char currentpath[60];
	//char currentpath2[60];
	char* pFileName = "c:\\silgandata\\pat\\";//drive
	
	CString c;

	c =itoa(pframe->CurrentJobNum,buf,10);
	
	strcpy(currentpath,pFileName);
	strcat(currentpath,c);
	//strcpy(currentpath2,pFileName);
	//strcat(currentpath2,c);
	//strcat(currentpath2,"\\image3");
	//strcat(currentpath2,".txt");

	///comment out text file save
	//ImageFile.Open(currentpath2, CFile::modeWrite | CFile::modeCreate);
	
	//CString szDigit;
	//int digit;

	//for(int g=0; g<144; g++)
	//{
	//	digit=*(im_sr+g);
	//	szDigit.Format("%3i",digit);
		//szDigit=itoa(digit,bufd,10);
		//ImageFile.Write(szDigit,3);
		//ImageFile.Write(",",1);
	//	ImageFile.Write(szDigit,szDigit.GetLength());
	//}

	//ImageFile.Close();
     
	//ImageFile.Open(currentpath2, CFile::modeRead | CFile::modeCreate);
	//UINT lBytesRead = ImageFile.Read (szSampleText,100); 
	//ImageFile.Read(m_r1,m_r1.GetLength());
	//ImageFile.Close();

	strcat(currentpath,"\\");
	strcat(currentpath,c);
	strcat(currentpath,".bmp");

	//sprintf(buf2,currentpath2,im_sr);
	//writeOutGrayArrayFile(currentpath2,
					 //12,
					 //12,
					 //im_sr);

	CVisByteImage image(12,12,1,-1,im_sr);
	//image.SetRect(0,40,640,480);
	imagepat=image;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imagepat.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{AfxMessageBox("The pat file could not be saved.");}// Warn the user.
}


void CXAxisView::SavePatternCap(BYTE *im_sr, int left)
{
	char buf[10];
	//char buf2[200];
	char currentpath[60];
	char currentpath2[60];
	char* pFileName = "c:\\silgandata\\pat\\";//drive
	int szPattH,szPattW;

	szPattH=szPattW=28; //identified by looking at necklippatt and comparing this with the original loop
	
	CString c;

	c =itoa(pframe->CurrentJobNum,buf,10);
	
	strcpy(currentpath,pFileName);
	strcat(currentpath,c);

	strcat(currentpath, left ?  "\\capimagel" : "\\capimager");
	strcpy(currentpath2,currentpath);

	strcat(currentpath, ".bmp");
	strcat(currentpath2, ".txt");

	/// adding save as bmp (copied from CXAxisView::SavePattern2X
	CVisByteImage image(szPattW,szPattH,1,-1,im_sr);
	imagepat=image;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imagepat.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{AfxMessageBox("The pat file could not be saved.");}// Warn the user.

	strcpy(currentpath2,pFileName);
	strcat(currentpath2,c);

	strcat(currentpath2, left ? "\\capimagel" : "\\capimager");
	strcat(currentpath2,".txt");

	CapImage.Open(currentpath2, CFile::modeWrite | CFile::modeCreate);
	
	CString szDigit;
	int digit;
	
	for(int g=0; g<784; g++)
	{
		digit=*(im_sr+g);
		szDigit.Format("%3i",digit);
		CapImage.Write(szDigit,szDigit.GetLength());
	}

	CapImage.Close();
}


void CXAxisView::InitPic()
{
	CVisRGBAByteImage image;

	try
	{
		image.ReadFile("c:\\silgandata\\initPicColor.bmp");
		imageC.Allocate(image.Rect());
		image.CopyPixelsTo(imageC);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Give up trying to read an image from this file.
		// Warn the user.
		AfxMessageBox("The file specified could not be opened.");
		//return FALSE;
	}
	
	//imI=(unsigned char *)image2.PixelAddress(0,0,1);
	//imI=(unsigned char *)image2.PtrToFirstPixelInRow(0);
	//im_read=Convert(imI);
	//Inspect(im_read,imgBuf0);

	InvalidateRect(false);
}

void CXAxisView::OnSaveC(BYTE *im_sr, int who)
{
	char buf[10];
	
	if(piccount>=1000)	{piccount=0;}
	piccount+=1;

	CString c =itoa(theapp->picNum,buf,10);
	CString d = c + "saved";
	
	char* pFileName = "c:\\silgandata\\xtra\\";//drive	
	char currentpath[60];
	strcpy(currentpath,pFileName);
	strcat(currentpath,d);

	strcat(currentpath,".bmp");

	int xdist=MainDisplayImageHeight;
	int ydist=768;

	switch(who)
	{
		case 0: xdist=CroppedCameraHeight;		ydist=CroppedCameraWidth;		break;
		case 1: xdist=CroppedCameraWidth;		ydist=CroppedCameraHeight;		break;
		case 2:	xdist=CroppedCameraWidth;		ydist=CroppedCameraHeight;		break;
		case 3:	xdist=CroppedCameraWidth;		ydist=CroppedCameraHeight;		break;
		case 4:	xdist=CroppedCameraWidth;		ydist=CroppedCameraHeight;		break;
		case 5:	xdist=CroppedCameraWidth;		ydist=CroppedCameraHeight;		break;
		case 6:	xdist=CroppedCameraHeight/3;	ydist=CroppedCameraWidth/3;	break;
		case 7:	xdist=MainDisplayImageHeight; ydist=MainDisplayImageWidth;	break;
	}
	
	CVisRGBAByteImage image(xdist,ydist,1,-1,im_sr);
	imageCS=image;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imageCS.WriteFile(filedescriptorT);
	}

	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Warn the user.
		AfxMessageBox("The file cound not be saved. ");
	}
}


void CXAxisView::OnSaveBW(BYTE *im_sr, int who)
{
	char buf[10];
	
	if(piccount>=100)	{piccount=0;}
	piccount+=1;

	CString c	=	itoa(piccount,buf,10);
	CString d	=	itoa(who,buf,10);
	CString e	=	"target";
	CString f	=	"current";
	
	char* pFileName = "c:\\silgandata\\save\\";//drive	
	char currentpath[60];
	strcpy(currentpath,pFileName);
	strcat(currentpath,d);
	strcat(currentpath,c);

	if(who==6) {strcat(currentpath,e);}
	if(who==7) {strcat(currentpath,f);}
	
	strcat(currentpath,".bmp");
	
	int xdist=192;
	int ydist=MainDisplayImageHeight;

	switch(who)
	{
		case 1:	xdist=CroppedCameraHeight/3;	ydist=CroppedCameraWidth/3;	break;
		case 2:	xdist=CroppedCameraHeight;		ydist=CroppedCameraWidth;		break;
		case 3:	xdist=CroppedCameraHeight;		ydist=CroppedCameraWidth;		break;
		case 4:	xdist=CroppedCameraHeight;		ydist=CroppedCameraWidth;		break;
		case 5:	xdist=CroppedCameraWidth/2;	ydist=CroppedCameraHeight/2;	break;
		case 6:	xdist=28; ydist=28;												break;
		case 7:	xdist=28; ydist=28;												break;
		case 8:	xdist=48; ydist=24;
	}
	
	CVisByteImage image(xdist,ydist,1,-1,im_sr);
	imageS=image;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imageS.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{AfxMessageBox("The file could not be saved. ");}// Warn the user.
}


int CXAxisView::FindMode(int *ar, int num)
{
	int result=0;
	int va;
	int va2;
	int bestVa;
	int currentCount=0;
	int storedCount=0;

	for(int i=0; i<num; i++)
	{
		va=*(ar+i);

		for(int j=0; j<num;j++)
		{
			va2=*(ar+j);

			if(va==va2)						{currentCount+=1;}
			if(currentCount>=storedCount)	{storedCount=currentCount; bestVa=va;}
		}

		currentCount=0;
	}

	result=bestVa;
	return result;
}

BYTE* CXAxisView::Reduce(BYTE* Image)
{
	int nCam = 1;
	int cWidth = MainDisplayImageHeight;
	int cHeight = MainDisplayImageWidth;
	int cOffset = 0;

	int pix1,	pix2,	pix3,	pix4,	pix5,	pix6,	pix7,	pix8,	pix9;
	int pix1x,	pix2x,	pix3x,	pix4x,	pix5x,	pix6x,	pix7x,	pix8x,	pix9x;
	int pix1y,	pix2y,	pix3y,	pix4y,	pix5y,	pix6y,	pix7y,	pix8y,	pix9y;

	int pixR, pixG, pixB;
	int pixC; int pixM; int pixY;
	
	int max, min, I;//, S, 
	int SI, SI2;
		
	int origLineLength	= 4*cWidth;

	//this is a rotated image so height will become new length
	int newLineLengthColDiv3= 4*cHeight/3;
	int newLengthBWDiv3		= cHeight/3;
	int newHeightBWDiv3		= cWidth/3;

	bool debugging=false;

	int startx=0;	int endx=4*cWidth;
	int starty=0;	int endy=cHeight;

	int j	=	0;
	int g	=	0;

	int l	=	newHeightBWDiv3+cOffset;

	int shift=0;
	//int	origLineLengthTimesiplus0;
	//int	origLineLengthTimesiplus1;
	//int	origLineLengthTimesiplus2;

	for(int i=starty; i<endy-2; i+=3)
	{
		//origLineLengthTimesiplus0=origLineLength*(i+0);
		//origLineLengthTimesiplus1=origLineLength*(i+1);
		//origLineLengthTimesiplus2=origLineLength*(i+2);
		
		l=(newHeightBWDiv3 - 1)+cOffset;
		//l=newLengthBWDiv3+cOffset;
		

		for(int k=startx; k<endx-12; k+=12)
		{
			pix1	= *(Image + k + 0  + origLineLength*(i+0));
			pix2	= *(Image + k + 1  + origLineLength*(i+0));
			pix3	= *(Image + k + 2  + origLineLength*(i+0));
			pix4	= *(Image + k + 4  + origLineLength*(i+0));
			pix5	= *(Image + k + 5  + origLineLength*(i+0));
			pix6	= *(Image + k + 6  + origLineLength*(i+0));
			pix7	= *(Image + k + 8  + origLineLength*(i+0));
			pix8	= *(Image + k + 9  + origLineLength*(i+0));
			pix9	= *(Image + k + 10 + origLineLength*(i+0));

			pix1x	= *(Image + k + 0  + origLineLength*(i+1));
			pix2x	= *(Image + k + 1  + origLineLength*(i+1));
			pix3x	= *(Image + k + 2  + origLineLength*(i+1));
			pix4x	= *(Image + k + 4  + origLineLength*(i+1));
			pix5x	= *(Image + k + 5  + origLineLength*(i+1));
			pix6x	= *(Image + k + 6  + origLineLength*(i+1));
			pix7x	= *(Image + k + 8  + origLineLength*(i+1));
			pix8x	= *(Image + k + 9  + origLineLength*(i+1));
			pix9x	= *(Image + k + 10 + origLineLength*(i+1));

			pix1y	= *(Image + k + 0  + origLineLength*(i+2));
			pix2y	= *(Image + k + 1  + origLineLength*(i+2));
			pix3y	= *(Image + k + 2  + origLineLength*(i+2));
			pix4y	= *(Image + k + 4  + origLineLength*(i+2));
			pix5y	= *(Image + k + 5  + origLineLength*(i+2));
			pix6y	= *(Image + k + 6  + origLineLength*(i+2));
			pix7y	= *(Image + k + 8  + origLineLength*(i+2));
			pix8y	= *(Image + k + 9  + origLineLength*(i+2));
			pix9y	= *(Image + k + 10 + origLineLength*(i+2));

			pixB	= (pix1+pix4+pix7+pix1x+pix4x+pix7x+pix1y+pix4y+pix7y)/9;
			pixG	= (pix2+pix5+pix8+pix2x+pix5x+pix8x+pix2y+pix5y+pix8y)/9;
			pixR	= (pix3+pix6+pix9+pix3x+pix6x+pix9x+pix3y+pix6y+pix9y)/9;
	
			*(im_reduce1 + j + 0 + newLineLengthColDiv3*l)=pixB;
			*(im_reduce1 + j + 1 + newLineLengthColDiv3*l)=pixG;
			*(im_reduce1 + j + 2 + newLineLengthColDiv3*l)=pixR;
			
			*(im_cambw1 + g + newLengthBWDiv3*l)=(pixR+pixG+pixB)/3;

			////////////////
			
			*(im_cam1Bbw + g + newLengthBWDiv3*l)=pixB;
			*(im_cam1Gbw + g + newLengthBWDiv3*l)=pixG;
			*(im_cam1Rbw + g + newLengthBWDiv3*l)=pixR;

			////////////////

			min = MIN(pixB, pixG);	min = MIN(min, pixR);
			max = MAX(pixB, pixG);	max = MAX(max, pixR);

			I	= max;

			if(I<10)	{SI = 0;}
			else		{SI	= 255*(I-min)/I;}

			SI = MAX(SI, 0);	SI = MIN(SI, 255);

			*(img1Sa2Div3 + g + newLengthBWDiv3*l) = SI;

			///////////////////////

			SI2	= max-min;
			*(img1SatDiv3 + g + newLengthBWDiv3*l) = SI2;

			///////////////////////

			pixC = I - pixR; pixM = I - pixG; pixY = I - pixB;

			*(img1MagDiv3 + g + newLengthBWDiv3*l) = pixM;
			*(img1YelDiv3 + g + newLengthBWDiv3*l) = pixY;
			*(img1CyaDiv3 + g + newLengthBWDiv3*l) = pixC;

			////////////////

			if(l>0)	{l-=1;}
		}
		
		j+=4; 
		g+=1; 
	}
	
	//////////
	
	int newLengthBWDiv6	= newLengthBWDiv3/2;
	int newHeightBWDiv6	= newHeightBWDiv3/2;

	int newLengthBWDiv3yplus0;
	int newLengthBWDiv3yplus1;

	int p=0; int q=0; int pix;

	for(int y=0; y<newHeightBWDiv3-1; y+=2)
	{
		newLengthBWDiv3yplus0 = newLengthBWDiv3*(y+0);
		newLengthBWDiv3yplus1 = newLengthBWDiv3*(y+1);
		
		p=0;

		for(int x=0; x<newLengthBWDiv3-1; x+=2 )
		{
			pix1	= *((im_cambw1 + x + 0 + newLengthBWDiv3yplus0));
			pix2	= *((im_cambw1 + x + 1 + newLengthBWDiv3yplus0));
			pix3	= *((im_cambw1 + x + 0 + newLengthBWDiv3yplus1));
			pix4	= *((im_cambw1 + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam1bwDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////
			pix1	= *((img1SatDiv3 + x + 0 + newLengthBWDiv3yplus0));
			pix2	= *((img1SatDiv3 + x + 1 + newLengthBWDiv3yplus0));
			pix3	= *((img1SatDiv3 + x + 0 + newLengthBWDiv3yplus1));
			pix4	= *((img1SatDiv3 + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(img1SatDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////

			pix1	= *((img1MagDiv3 + x + 0 + newLengthBWDiv3yplus0));
			pix2	= *((img1MagDiv3 + x + 1 + newLengthBWDiv3yplus0));
			pix3	= *((img1MagDiv3 + x + 0 + newLengthBWDiv3yplus1));
			pix4	= *((img1MagDiv3 + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(img1MagDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////

			pix1	= *((img1YelDiv3 + x + 0 + newLengthBWDiv3yplus0));
			pix2	= *((img1YelDiv3 + x + 1 + newLengthBWDiv3yplus0));
			pix3	= *((img1YelDiv3 + x + 0 + newLengthBWDiv3yplus1));
			pix4	= *((img1YelDiv3 + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(img1YelDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////

			pix1	= *((img1CyaDiv3 + x + 0 + newLengthBWDiv3yplus0));
			pix2	= *((img1CyaDiv3 + x + 1 + newLengthBWDiv3yplus0));
			pix3	= *((img1CyaDiv3 + x + 0 + newLengthBWDiv3yplus1));
			pix4	= *((img1CyaDiv3 + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(img1CyaDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////

			pix1	= *((im_cam1Rbw + x + 0 + newLengthBWDiv3yplus0));
			pix2	= *((im_cam1Rbw + x + 1 + newLengthBWDiv3yplus0));
			pix3	= *((im_cam1Rbw + x + 0 + newLengthBWDiv3yplus1));
			pix4	= *((im_cam1Rbw + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam1RbwDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////

			pix1	= *((im_cam1Gbw + x + 0 + newLengthBWDiv3yplus0));
			pix2	= *((im_cam1Gbw + x + 1 + newLengthBWDiv3yplus0));
			pix3	= *((im_cam1Gbw + x + 0 + newLengthBWDiv3yplus1));
			pix4	= *((im_cam1Gbw + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam1GbwDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////

			pix1	= *((im_cam1Bbw + x + 0 + newLengthBWDiv3yplus0));
			pix2	= *((im_cam1Bbw + x + 1 + newLengthBWDiv3yplus0));
			pix3	= *((im_cam1Bbw + x + 0 + newLengthBWDiv3yplus1));
			pix4	= *((im_cam1Bbw + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam1BbwDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////

			p++;
		}

		q++;
	}

	//////////

	if(debugging==true)	{SaveBWCustom1(im_cambw1,		newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img1SatDiv3,		newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img1MagDiv3,		newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img1YelDiv3,		newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img1CyaDiv3,		newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(im_cam1Rbw,		newLengthBWDiv6, newHeightBWDiv6);}

	if(debugging==true)	{SaveBWCustom1(im_cam1bwDiv6,	newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img1SatDiv6,		newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img1MagDiv6,		newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img1YelDiv6,		newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img1CyaDiv6,		newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(im_cam1RbwDiv6,	newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(im_cam1GbwDiv6,	newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(im_cam1BbwDiv6,	newLengthBWDiv6, newHeightBWDiv6);}

	return im_reduce1;
}


BYTE* CXAxisView::Reduce2(BYTE* Image)
{
	int nCam = 2;
	int cWidth = MainDisplayImageHeight;
	int cHeight = MainDisplayImageWidth;
	int cOffset = 0;

	int pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pix1x, pix2x, pix3x, pix4x, pix5x, pix6x, pix7x, pix8x, pix9x;
	int pix1y, pix2y, pix3y, pix4y, pix5y, pix6y, pix7y, pix8y, pix9y;
	int pixR, pixG, pixB;
	int pixC; int pixM; int pixY;
	int max, min, I;//, S, 
	int SI, SI2;
			
	int origLineLength		=	4*cWidth;
	int newLineLengthColDiv3=	4*cHeight/3;
	
	int newLengthBWDiv3		=	cHeight/3;
	int newHeightBWDiv3		=	cWidth/3;

	bool debugging=false;

	int startx=0;	int endx = 4*cWidth;
	int starty=0;	int endy = cHeight;

	//why on earth did I come up with this?
	//int j =	4*cHeight-4;
	int j = 4*(newLengthBWDiv3-1);

	//int g =	cHeight-1;
	int g = newLengthBWDiv3 - 1;

	int h =	cOffset;
	int l =	cOffset;
	
	int filterSelPatt =	theapp->jobinfo[pframe->CurrentJobNum].filterSelPatt;

	for(int i=starty; i<endy-3; i+=3)
	{
		l=cOffset;
		h=cOffset;

		for(int k=startx; k<endx-12; k+=12)
		{
			pix1	= *((Image + k +		origLineLength*(i)));
			pix2	= *((Image + k + 1 +	origLineLength*(i)));
			pix3	= *((Image + k + 2 +	origLineLength*(i)));
			pix4	= *((Image + k + 4 +	origLineLength*(i)));
			pix5	= *((Image + k + 5 +	origLineLength*(i)));
			pix6	= *((Image + k + 6 +	origLineLength*(i)));

			pix7	= *((Image + k + 8 +	origLineLength*(i)));
			pix8	= *((Image + k + 9 +	origLineLength*(i)));
			pix9	= *((Image + k +10 +	origLineLength*(i)));

			pix1x	= *((Image + k +		origLineLength*(i+1)));
			pix2x	= *((Image + k + 1 +	origLineLength*(i+1)));
			pix3x	= *((Image + k + 2 +	origLineLength*(i+1)));
			pix4x	= *((Image + k + 4 +	origLineLength*(i+1)));
			pix5x	= *((Image + k + 5 +	origLineLength*(i+1)));
			pix6x	= *((Image + k + 6 +	origLineLength*(i+1)));

			pix7x	= *((Image + k + 8 +	origLineLength*(i+1)));
			pix8x	= *((Image + k + 9 +	origLineLength*(i+1)));
			pix9x	= *((Image + k +10 +	origLineLength*(i+1)));

			pix1y	= *((Image + k +		origLineLength*(i+2)));
			pix2y	= *((Image + k + 1 +	origLineLength*(i+2)));
			pix3y	= *((Image + k + 2 +	origLineLength*(i+2)));
			
			pix4y	= *((Image + k + 4 +	origLineLength*(i+2)));
			pix5y	= *((Image + k + 5 +	origLineLength*(i+2)));
			pix6y	= *((Image + k + 6 +	origLineLength*(i+2)));

			pix7y	= *((Image + k + 8 +	origLineLength*(i+2)));
			pix8y	= *((Image + k + 9 +	origLineLength*(i+2)));
			pix9y	= *((Image + k +10 +	origLineLength*(i+2)));

			pixB	= (pix1+pix4+pix7+pix1x+pix4x+pix7x+pix1y+pix4y+pix7y)/9;
			pixG	= (pix2+pix5+pix8+pix2x+pix5x+pix8x+pix2y+pix5y+pix8y)/9;
			pixR	= (pix3+pix6+pix9+pix3x+pix6x+pix9x+pix3y+pix6y+pix9y)/9;
		
			*(im_reduce2 + j +		newLineLengthColDiv3*l)	= pixB;
			*(im_reduce2 + j + 1 +	newLineLengthColDiv3*l)	= pixG;
			*(im_reduce2 + j + 2 +	newLineLengthColDiv3*l)	= pixR;

			*(im_cambw2 + g + newLengthBWDiv3*h)=(pixR+pixG+pixB)/3;

			////////////////
			
			*(im_cam2Bbw + g + newLengthBWDiv3*h)=pixB;
			*(im_cam2Gbw + g + newLengthBWDiv3*h)=pixG;
			*(im_cam2Rbw + g + newLengthBWDiv3*h)=pixR;
			
			////////////////

			min = MIN(pixB, pixG);	min = MIN(min, pixR);
			max = MAX(pixB, pixG);	max = MAX(max, pixR);
			I	= max;

			if(I<10)	{SI = 0;}
			else		{SI	= 255*(I-min)/I;}

			SI = MAX(SI, 0);	SI = MIN(SI, 255);

			*(img2Sa2Div3 + g + newLengthBWDiv3*h) = SI;

			//////////////////
			
			SI2	= max-min;
			*(img2SatDiv3 + g + newLengthBWDiv3*h) = SI2;

			///////////////////////


			//min = MIN(pixB, pixG);	min = MIN(min, pixR);
			//max = MAX(pixB, pixG);	max = MAX(max, pixR);

			//I	= max;	SI = I-min;

			//*(img2SatDiv3 + g + newLengthBWDiv3*h) = SI;

			////////////////
			pixC = I - pixR; pixM = I - pixG; pixY = I - pixB;

			*(img2MagDiv3 + g + newLengthBWDiv3*h) = pixM;
			*(img2YelDiv3 + g + newLengthBWDiv3*h) = pixY;
			*(img2CyaDiv3 + g + newLengthBWDiv3*h) = pixC;

			////////////////
		
			l+=1; 
			h+=1;	
		}
    			
		if(j>=4)	{j-=4;}
		if(g>=1)	{g-=1;}
	}
	
	//////////
	//debugging = true;
	if(debugging==true)
	{SaveColCustom1(im_reduce2, newLengthBWDiv3, newHeightBWDiv3);}


	//////////
	
	int newLengthBWDiv6	= newLengthBWDiv3/2;
	int newHeightBWDiv6	= newHeightBWDiv3/2;

	int p=0; int q=0; int pix;

	for(int y=0; y<newHeightBWDiv3-1; y+=2)
	{
		p=0;

		for(int x=0; x<newLengthBWDiv3-1; x+=2 )
		{
			pix1	= *((im_cambw2 + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((im_cambw2 + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((im_cambw2 + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((im_cambw2 + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam2bwDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////////

			pix1	= *((img2SatDiv3 + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((img2SatDiv3 + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((img2SatDiv3 + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((img2SatDiv3 + x + 1 + newLengthBWDiv3*(y+1)));

			pix		= (pix1+pix2+pix3+pix4)/4;

			*(img2SatDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////////
			pix1	= *((img2MagDiv3 + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((img2MagDiv3 + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((img2MagDiv3 + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((img2MagDiv3 + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(img2MagDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////////

			pix1	= *((img2YelDiv3 + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((img2YelDiv3 + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((img2YelDiv3 + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((img2YelDiv3 + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(img2YelDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////////

			pix1	= *((img2CyaDiv3 + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((img2CyaDiv3 + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((img2CyaDiv3 + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((img2CyaDiv3 + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(img2CyaDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////////

			pix1	= *((im_cam2Rbw + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((im_cam2Rbw + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((im_cam2Rbw + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((im_cam2Rbw + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam2RbwDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////////

			pix1	= *((im_cam2Gbw + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((im_cam2Gbw + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((im_cam2Gbw + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((im_cam2Gbw + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam2GbwDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////////

			pix1	= *((im_cam2Bbw + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((im_cam2Bbw + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((im_cam2Bbw + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((im_cam2Bbw + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam2BbwDiv6 + p  + newLengthBWDiv6 * q) = pix;

			////////////////
			
			p++;
		}

		q++;
	}
	
	//////////
	
	if(debugging==true)	{SaveBWCustom1(im_cambw2,			newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img2SatDiv3,			newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img2MagDiv3,			newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img2YelDiv3,			newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img2CyaDiv3,			newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(im_cam2Rbw,			newLengthBWDiv6, newHeightBWDiv6);}

	if(debugging==true)	{SaveBWCustom1(im_cam2bwDiv6,		newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img2SatDiv6,			newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img2MagDiv6,			newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img2YelDiv6,			newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img2CyaDiv6,			newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(im_cam2RbwDiv6,		newLengthBWDiv6, newHeightBWDiv6);}

	return im_reduce2;
}


BYTE* CXAxisView::Reduce3(BYTE* Image)
{
	int nCam = 3;
	int cWidth = MainDisplayImageHeight;
	int cHeight = MainDisplayImageWidth;
	int cOffset = 0;

	int pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pix1x, pix2x, pix3x, pix4x, pix5x, pix6x, pix7x, pix8x, pix9x;
	int pix1y, pix2y, pix3y, pix4y, pix5y, pix6y, pix7y, pix8y, pix9y;
	int pixR, pixG, pixB;
	int pixC; int pixM; int pixY;
	
	int max, min, I;//, S, 
	int SI, SI2;

	bool debugging=false;

	int origLineLength			= 4*cWidth;

	//this is a rotated image so height will become new length
	int newLineLengthColDiv3	= 4*cHeight/3;
	int newLengthBWDiv3			= cHeight/3;
	int newHeightBWDiv3			= cWidth/3;
	
	int startx=0;	int endx = 4*cWidth;
	int starty=0;	int endy = cHeight;

	int j		=	0;
	int g		=	0;

	int l		=	newHeightBWDiv3+cOffset;
	
	int filterSelPatt =	theapp->jobinfo[pframe->CurrentJobNum].filterSelPatt;

	for(int i=starty; i<endy-2; i+=3)
	{
		l = (newHeightBWDiv3 - 1) + cOffset;

		for(int k=startx; k<endx-12; k+=12)
		{
			pix1	=	*((Image + k +		origLineLength*(i)));
			pix2	=	*((Image + k + 1 +	origLineLength*(i)));
			pix3	=	*((Image + k + 2 +	origLineLength*(i)));
			pix4	=	*((Image + k + 4 +	origLineLength*(i)));
			pix5	=	*((Image + k + 5 +	origLineLength*(i)));
			pix6	=	*((Image + k + 6 +	origLineLength*(i)));

			pix7	=	*((Image + k + 8 +	origLineLength*(i)));
			pix8	=	*((Image + k + 9 +	origLineLength*(i)));
			pix9	=	*((Image + k +10 +	origLineLength*(i)));

			pix1x	=	*((Image + k +		origLineLength*(i+1)));
			pix2x	=	*((Image + k + 1 +	origLineLength*(i+1)));
			pix3x	=	*((Image + k + 2 +	origLineLength*(i+1)));
			pix4x	=	*((Image + k + 4 +	origLineLength*(i+1)));
			pix5x	=	*((Image + k + 5 +	origLineLength*(i+1)));
			pix6x	=	*((Image + k + 6 +	origLineLength*(i+1)));

			pix7x	=	*((Image + k + 8 +	origLineLength*(i+1)));
			pix8x	=	*((Image + k + 9 +	origLineLength*(i+1)));
			pix9x	=	*((Image + k +10 +	origLineLength*(i+1)));

			pix1y	=	*((Image + k +		origLineLength*(i+2)));
			pix2y	=	*((Image + k + 1 +	origLineLength*(i+2)));
			pix3y	=	*((Image + k + 2 +	origLineLength*(i+2)));
			
			pix4y	=	*((Image + k + 4 +	origLineLength*(i+2)));
			pix5y	=	*((Image + k + 5 +	origLineLength*(i+2)));
			pix6y	=	*((Image + k + 6 +	origLineLength*(i+2)));

			pix7y	=	*((Image + k + 8 +	origLineLength*(i+2)));
			pix8y	=	*((Image + k + 9 +	origLineLength*(i+2)));
			pix9y	=	*((Image + k +10 +	origLineLength*(i+2)));

			pixB	=	(pix1+pix4+pix7+pix1x+pix4x+pix7x+pix1y+pix4y+pix7y)/9;
			pixG	=	(pix2+pix5+pix8+pix2x+pix5x+pix8x+pix2y+pix5y+pix8y)/9;
			pixR	=	(pix3+pix6+pix9+pix3x+pix6x+pix9x+pix3y+pix6y+pix9y)/9;
		
			*(im_reduce3 + j +		newLineLengthColDiv3*l)=pixB;
			*(im_reduce3 + j + 1 +	newLineLengthColDiv3*l)=pixG;
			*(im_reduce3 + j + 2 +	newLineLengthColDiv3*l)=pixR;

			*(im_cambw3 + g + newLengthBWDiv3*l)=(pixR + pixG + pixB)/3;

			////////////////
			
			*(im_cam3Bbw + g + newLengthBWDiv3*l)=pixB;
			*(im_cam3Gbw + g + newLengthBWDiv3*l)=pixG;
			*(im_cam3Rbw + g + newLengthBWDiv3*l)=pixR;

			////////////////

			min = MIN(pixB, pixG);	min = MIN(min, pixR);
			max = MAX(pixB, pixG);	max = MAX(max, pixR);
			I	= max;

			if(I<10)	{SI = 0;}
			else		{SI	= 255*(I-min)/I;}

			SI = MAX(SI, 0);	SI = MIN(SI, 255);

			*(img3Sa2Div3 + g + newLengthBWDiv3*l) = SI;

			////////////////

			SI2	= max-min;
			*(img3SatDiv3 + g + newLengthBWDiv3*l) = SI2;

			////////////////

			//min = MIN(pixB, pixG);	min = MIN(min, pixR);
			//max = MAX(pixB, pixG);	max = MAX(max, pixR);

			//I	= max;	SI = I-min;

			//*(img3SatDiv3 + g + newLengthBWDiv3*l) = SI;

			////////////////

			pixC = I - pixR; pixM = I - pixG; pixY = I - pixB;

			*(img3MagDiv3 + g + newLengthBWDiv3*l) = pixM;
			*(img3YelDiv3 + g + newLengthBWDiv3*l) = pixY;
			*(img3CyaDiv3 + g + newLengthBWDiv3*l) = pixC;

			////////////////

			if(l>0)	{l-=1;}
		}
		
		j+=4;
		g+=1;
	}

	////////////////////

	int newLengthBWDiv6	= newLengthBWDiv3/2;
	int newHeightBWDiv6	= newHeightBWDiv3/2;

	int p=0; int q=0; int pix;

	//////////


	p=0; q=0;

	for(int y=0; y<newHeightBWDiv3-1; y+=2)
	{
		p=0;

		for(int x=0; x<newLengthBWDiv3-1; x+=2 )
		{
			pix1	= *((im_cambw3 + x + 0 + newLengthBWDiv3*(y)));
			pix2	= *((im_cambw3 + x + 1 + newLengthBWDiv3*(y)));
			pix3	= *((im_cambw3 + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((im_cambw3 + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam3bwDiv6 + p  + newLengthBWDiv6 * q) = pix;
			
			//////////
			pix1	= *((img3SatDiv3 + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((img3SatDiv3 + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((img3SatDiv3 + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((img3SatDiv3 + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(img3SatDiv6 + p  + newLengthBWDiv6 * q) = pix;

			//////////
			pix1	= *((img3MagDiv3 + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((img3MagDiv3 + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((img3MagDiv3 + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((img3MagDiv3 + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(img3MagDiv6 + p  + newLengthBWDiv6 * q) = pix;

			//////////
			pix1	= *((img3YelDiv3 + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((img3YelDiv3 + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((img3YelDiv3 + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((img3YelDiv3 + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(img3YelDiv6 + p  + newLengthBWDiv6 * q) = pix;

			//////////
			pix1	= *((img3CyaDiv3 + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((img3CyaDiv3 + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((img3CyaDiv3 + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((img3CyaDiv3 + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(img3CyaDiv6 + p  + newLengthBWDiv6 * q) = pix;

			//////////
			pix1	= *((im_cam3Rbw + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((im_cam3Rbw + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((im_cam3Rbw + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((im_cam3Rbw + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam3RbwDiv6 + p  + newLengthBWDiv6 * q) = pix;

			//////////

			pix1	= *((im_cam3Gbw + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((im_cam3Gbw + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((im_cam3Gbw + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((im_cam3Gbw + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam3GbwDiv6 + p  + newLengthBWDiv6 * q) = pix;

			//////////

			pix1	= *((im_cam3Bbw + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((im_cam3Bbw + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((im_cam3Bbw + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((im_cam3Bbw + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam3BbwDiv6 + p  + newLengthBWDiv6 * q) = pix;

			//////////

			p++;
		}

		q++;
	}

	//////////

	if(debugging==true)	{SaveBWCustom1(im_cambw3,			newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img3SatDiv3,			newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img3MagDiv3,			newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img3YelDiv3,			newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img3CyaDiv3,			newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(im_cam3Rbw,			newLengthBWDiv6, newHeightBWDiv6);}

	if(debugging==true)	{SaveBWCustom1(im_cam3bwDiv6,		newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img3SatDiv6,			newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img3MagDiv6,			newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img3YelDiv6,			newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img3CyaDiv6,			newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(im_cam3RbwDiv6,		newLengthBWDiv6, newHeightBWDiv6);}

	return im_reduce3;
}


BYTE* CXAxisView::Reduce4(BYTE* Image)
{
	int nCam = 4;
	int cWidth = MainDisplayImageHeight;
	int cHeight = MainDisplayImageWidth;
	int cOffset = 0;

	int pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pix1x, pix2x, pix3x, pix4x, pix5x, pix6x, pix7x, pix8x, pix9x;
	int pix1y, pix2y, pix3y, pix4y, pix5y, pix6y, pix7y, pix8y, pix9y;
	int pixR, pixG, pixB;
	int pixC; int pixM; int pixY;
	int max, min, I;//, S, 
	int SI, SI2;
	
	int origLineLength		=	4*cWidth;
	int newLineLengthColDiv3=	(4*cHeight)/3;
	
	int newLengthBWDiv3		=	cHeight/3;
	int newHeightBWDiv3		=	cWidth/3;

	bool debugging = false;

	int startx=0;	int endx = 4*cWidth;
	int starty=0;	int endy = cHeight;
	
	//int j		=	4*cHeight-4;
	int j		=	4*(newLengthBWDiv3-1);

	//int g =	cHeight-1;
	int g		=	newLengthBWDiv3 - 1;

	int l		=	cOffset;
	int h		=	cOffset;
	
	int filterSelPatt =	theapp->jobinfo[pframe->CurrentJobNum].filterSelPatt;

	for(int i=starty; i<endy-3; i+=3)
	{
		l=cOffset;
		h=cOffset;

		for(int k=startx; k<endx-12; k+=12)
		{
			pix1	= *((Image + k +		origLineLength*(i)));
			pix2	= *((Image + k + 1 +	origLineLength*(i)));
			pix3	= *((Image + k + 2 +	origLineLength*(i)));
			
			pix4	= *((Image + k + 4 +	origLineLength*(i)));
			pix5	= *((Image + k + 5 +	origLineLength*(i)));
			pix6	= *((Image + k + 6 +	origLineLength*(i)));

			pix7	= *((Image + k + 8 +	origLineLength*(i)));
			pix8	= *((Image + k + 9 +	origLineLength*(i)));
			pix9	= *((Image + k +10 +	origLineLength*(i)));

			pix1x	= *((Image + k +		origLineLength*(i+1)));
			pix2x	= *((Image + k + 1 +	origLineLength*(i+1)));
			pix3x	= *((Image + k + 2 +	origLineLength*(i+1)));
			pix4x	= *((Image + k + 4 +	origLineLength*(i+1)));
			pix5x	= *((Image + k + 5 +	origLineLength*(i+1)));
			pix6x	= *((Image + k + 6 +	origLineLength*(i+1)));

			pix7x	= *((Image + k + 8 +	origLineLength*(i+1)));
			pix8x	= *((Image + k + 9 +	origLineLength*(i+1)));
			pix9x	= *((Image + k +10 +	origLineLength*(i+1)));

			pix1y	= *((Image + k +		origLineLength*(i+2)));
			pix2y	= *((Image + k + 1 +	origLineLength*(i+2)));
			pix3y	= *((Image + k + 2 +	origLineLength*(i+2)));
			
			pix4y	= *((Image + k + 4 +	origLineLength*(i+2)));
			pix5y	= *((Image + k + 5 +	origLineLength*(i+2)));
			pix6y	= *((Image + k + 6 +	origLineLength*(i+2)));

			pix7y	= *((Image + k + 8 +	origLineLength*(i+2)));
			pix8y	= *((Image + k + 9 +	origLineLength*(i+2)));
			pix9y	= *((Image + k +10 +	origLineLength*(i+2)));

		    pixB	= (pix1+pix4+pix7+pix1x+pix4x+pix7x+pix1y+pix4y+pix7y)/9;
			pixG	= (pix2+pix5+pix8+pix2x+pix5x+pix8x+pix2y+pix5y+pix8y)/9;
			pixR	= (pix3+pix6+pix9+pix3x+pix6x+pix9x+pix3y+pix6y+pix9y)/9;
		
			*(im_reduce4 + j +		newLineLengthColDiv3*l)	= pixB;
			*(im_reduce4 + j + 1 +	newLineLengthColDiv3*l)	= pixG;
			*(im_reduce4 + j + 2 +	newLineLengthColDiv3*l)	= pixR;

			*(im_cambw4 + g + newLengthBWDiv3*h)=(pixR+pixG+pixB)/3;
			
			*(im_cam4Bbw+ g + newLengthBWDiv3*h)=pixB;
			*(im_cam4Gbw+ g + newLengthBWDiv3*h)=pixG;
			*(im_cam4Rbw+ g + newLengthBWDiv3*h)=pixR;

			////////////////

			min = MIN(pixB, pixG);	min = MIN(min, pixR);
			max = MAX(pixB, pixG);	max = MAX(max, pixR);

			I	= max;

			if(I<10)	{SI = 0;}
			else		{SI	= 255*(I-min)/I;}

			SI = MAX(SI, 0);	SI = MIN(SI, 255);

			*(img4Sa2Div3 + g + newLengthBWDiv3*h) = SI;

			////////////////

			SI2	= max-min;
			*(img4SatDiv3 + g + newLengthBWDiv3*h) = SI2;

			////////////////


			//min = MIN(pixB, pixG);	min = MIN(min, pixR);
			//max = MAX(pixB, pixG);	max = MAX(max, pixR);

			//I	= max;
			//SI	= I-min;

			//*(img4SatDiv3 + g + newLengthBWDiv3*h) = SI;

			////////////////

			pixC = I - pixR; 
			pixM = I - pixG; 
			pixY = I - pixB;

			*(img4MagDiv3 + g + newLengthBWDiv3*h) = pixM;
			*(img4YelDiv3 + g + newLengthBWDiv3*h) = pixY;
			*(img4CyaDiv3 + g + newLengthBWDiv3*h) = pixC;

			////////////////

			l+=1; 
			h+=1;	
		}

		if(j>=4)	{j-=4;}
		if(g>=1)	{g-=1;}
	}

	////////

	int newLengthBWDiv6	= newLengthBWDiv3/2;
	int newHeightBWDiv6	= newHeightBWDiv3/2;

	int p=0; int q=0; int pix;

	//////////

	p=0; q=0;

	for(int y=0; y<newHeightBWDiv3-1; y+=2)
	{
		p=0;

		for(int x=0; x<newLengthBWDiv3-1; x+=2 )
		{
			pix1	= *((im_cambw4 + x +		newLengthBWDiv3*(y)));
			pix2	= *((im_cambw4 + x + 1 +	newLengthBWDiv3*(y)));
			pix3	= *((im_cambw4 + x +		newLengthBWDiv3*(y+1)));
			pix4	= *((im_cambw4 + x + 1 +	newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam4bwDiv6 + p + newLengthBWDiv6*q) = pix;
			
			//////////////
			pix1	= *((img4SatDiv3 + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((img4SatDiv3 + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((img4SatDiv3 + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((img4SatDiv3 + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(img4SatDiv6 + p  + newLengthBWDiv6 * q) = pix;

			//////////////
			pix1	= *((img4MagDiv3 + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((img4MagDiv3 + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((img4MagDiv3 + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((img4MagDiv3 + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(img4MagDiv6 + p  + newLengthBWDiv6 * q) = pix;

			//////////////

			pix1	= *((img4YelDiv3 + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((img4YelDiv3 + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((img4YelDiv3 + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((img4YelDiv3 + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(img4YelDiv6 + p  + newLengthBWDiv6 * q) = pix;

			//////////////

			pix1	= *((img4CyaDiv3 + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((img4CyaDiv3 + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((img4CyaDiv3 + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((img4CyaDiv3 + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(img4CyaDiv6 + p  + newLengthBWDiv6 * q) = pix;

			//////////////

			pix1	= *((im_cam4Rbw + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((im_cam4Rbw + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((im_cam4Rbw + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((im_cam4Rbw + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam4RbwDiv6 + p  + newLengthBWDiv6 * q) = pix;

			//////////////

			pix1	= *((im_cam4Gbw + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((im_cam4Gbw + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((im_cam4Gbw + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((im_cam4Gbw + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam4GbwDiv6 + p  + newLengthBWDiv6 * q) = pix;

			//////////////
		
			pix1	= *((im_cam4Bbw + x + 0 + newLengthBWDiv3*(y+0)));
			pix2	= *((im_cam4Bbw + x + 1 + newLengthBWDiv3*(y+0)));
			pix3	= *((im_cam4Bbw + x + 0 + newLengthBWDiv3*(y+1)));
			pix4	= *((im_cam4Bbw + x + 1 + newLengthBWDiv3*(y+1)));

			pix = (pix1+pix2+pix3+pix4)/4;

			*(im_cam4BbwDiv6 + p  + newLengthBWDiv6 * q) = pix;

			//////////////

			p++;
		}

		q++;
	}

	//////////

	if(debugging==true)	{SaveBWCustom1(im_cambw4,			newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img4SatDiv3,			newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img4MagDiv3,			newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img4YelDiv3,			newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(img4CyaDiv3,			newLengthBWDiv3, newHeightBWDiv3);}
	if(debugging==true)	{SaveBWCustom1(im_cam4Rbw,			newLengthBWDiv6, newHeightBWDiv6);}

	if(debugging==true)	{SaveBWCustom1(im_cam4bwDiv6,		newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img4SatDiv6,			newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img4MagDiv6,			newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img4YelDiv6,			newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(img4CyaDiv6,			newLengthBWDiv6, newHeightBWDiv6);}
	if(debugging==true)	{SaveBWCustom1(im_cam4RbwDiv6,		newLengthBWDiv6, newHeightBWDiv6);}

	return im_reduce4;
}

//	This function is expecting an image of height 1024 and a width of 408
BYTE* CXAxisView::Reduce5(BYTE* Image)
{
	int nCam		=	5;
	int pix1, pix2, pix3, pix4, pix5, pix6;
	int pix1x, pix2x, pix3x, pix4x, pix5x, pix6x;
	int resultr, resultg, resultb;

	int camWidth = MainDisplayImageHeight;
	int camHeight = MainDisplayImageWidth;

	int LineLength	=	camWidth*4;
	int LineLength2	=	(4*camWidth)/2;
	int LineLength3	=	camWidth/2;

	int startx		=	0;
	int starty		=	0;
	int endx		=	4*(camWidth-8);
	int endy		=	camHeight;
	
	int j=0;
	int l=0;
	int r=0;
	int m=0;
	
	for(int i=starty; i<endy-2;)
	{
		for(int k=startx; k<endx;)
		{
			pix1	= *((Image + k +		LineLength*(i)));
			pix2	= *((Image + k + 1 +	LineLength*(i)));
			pix3	= *((Image + k + 2 +	LineLength*(i)));
			pix4	= *((Image + k + 4 +	LineLength*(i)));
			pix5	= *((Image + k + 5 +	LineLength*(i)));
			pix6	= *((Image + k + 6 +	LineLength*(i)));

			pix1x	= *((Image + k +		LineLength*(i+1)));
			pix2x	= *((Image + k + 1 +	LineLength*(i+1)));
			pix3x	= *((Image + k + 2 +	LineLength*(i+1)));
			pix4x	= *((Image + k + 4 +	LineLength*(i+1)));
			pix5x	= *((Image + k + 5 +	LineLength*(i+1)));
			pix6x	= *((Image + k + 6 +	LineLength*(i+1)));

			resultb	= (pix1+pix4+pix1x+pix4x)/4;
			resultg	= (pix2+pix5+pix2x+pix5x)/4;
			resultr	= (pix3+pix6+pix3x+pix6x)/4;
		
			*(im_reduce5 + j +		LineLength2*l)	= resultb;
			*(im_reduce5 + j + 1 +	LineLength2*l)	= resultg;
			*(im_reduce5 + j + 2 +	LineLength2*l)	= resultr;

			*(im_cambw5 + m + LineLength3*l)			= (resultr+resultg+resultb)/3;

			//////////////////
			
			*(im_cam5Bbw + m + LineLength3*l)		= resultb;
			*(im_cam5Gbw + m + LineLength3*l)		= resultg;
			*(im_cam5Rbw + m + LineLength3*l)		= resultr;

			//////////////////
			
			k+=8; j+=4; m+=1;
		}

		i+=2; j=0; l+=1; m=0;
	}

	return im_reduce5;
}


COLORREF CXAxisView::DoUser(BYTE *im_srcIn, int nCam, int x, int y, int width, int UserStrength)
{
	COLORREF resultColor;
	
	int pixB2, pixR2, pixG2, pixB, pixR, pixG;

	int	totR, totB, totG;
	totR=totB=totG=0;
	
	int	count=0;
	int LineLength	= (MainDisplayImageHeight/2)*4;
	int LineLength2	= width*4;
	
	int camWidth = MainDisplayImageWidth;
	int camHeight = MainDisplayImageHeight;

	int xwidth=width*4;
	int yheight=theapp->jobinfo[pframe->CurrentJobNum].fillSize;
	int filter =255;//theapp->jobinfo[pframe->CurrentJobNum].colorFilter2;
	
	int xpos3=0;	int ypos3=10;
	int xpos=x;		int ypos=0;

	if(xpos % 4 !=0)
	{
		xpos+=1;

		if(xpos % 4 !=0)
		{
			xpos+=1;

			if(xpos % 4 !=0)	{xpos+=1;}
		}
	}

	if(xpos>=0 && xpos<=camWidth-30 && y>=0 && y+yheight <=camHeight-30)
	{
		for(ypos=y; ypos<yheight+y; ypos++)
		{
			for(int xpos2=xpos*4; xpos2<xwidth+(xpos*4); xpos2+=4)
			{	
				pixB = *(im_srcIn + xpos2 +		LineLength*ypos);
				pixG = *(im_srcIn + xpos2 + 1 +	LineLength*ypos);
				pixR = *(im_srcIn + xpos2 + 2 +	LineLength*ypos);
						
				if( (pixB<=filter && pixG<=filter) || (pixB<=filter && pixR<=filter) || (pixG<=filter && pixR<=filter))
				{
					pixB2 = *(im_color2 + xpos3 +		LineLength2*ypos3) = pixB;
					pixG2 = *(im_color2 + xpos3 + 1 +	LineLength2*ypos3) = pixG;
					pixR2 = *(im_color2 + xpos3 + 2 +	LineLength2*ypos3) = pixR;
						
					count+=1;
					totR+=pixR2;
					totG+=pixG2;
					totB+=pixB2;
				}
				else
				{
					*(im_color2 + xpos3 +		LineLength2*ypos3) = 0;
					*(im_color2 + xpos3 + 1 +	LineLength2*ypos3) = 0;
					*(im_color2 + xpos3 + 2 +	LineLength2*ypos3) = 0;
				}

				if(ypos3>0)ypos3-=1;
			}

			xpos3+=4; ypos3=10;
		}

		if(count>0)
		{
			totB=(totB/count);
			totR=(totR/count);
			totG=(totG/count);

			resultColor = RGB(totR,totG,totB);
		}						
	}

	return resultColor;
}


COLORREF CXAxisView::DoUser2(BYTE *im_srcIn, int nCam, int x, int y, int width, int UserStrength)
{
	COLORREF resultColor = 0;
	
	int pixB2, pixR2, pixG2, pixB, pixR, pixG;
	
	int	totR, totB, totG;
	totR=totB=totG=0;

	int	count		=	0;
	int LineLength	=	4*(MainDisplayImageHeight/2);
	int LineLength2	=	4*width;
	int ypos		=	0;
	int xwidth		=	4*32;
	int yheight		=	width;

	int xpos		=	x;
	int filter		=	255;//theapp->jobinfo[pframe->CurrentJobNum].colorFilter;

	if(xpos % 4 !=0)
	{
		xpos+=1;

		if(xpos % 4 !=0)
		{ xpos+=1;	if(xpos%4 !=0)	{xpos+=1;} }
	}

	int xpos3=0;	int ypos3=32;

	if(xpos>=0 && xpos<=MainDisplayImageWidth-30 && y>=0 && y <=MainDisplayImageHeight-30)
	{
		for(ypos=y; ypos<yheight+y; ypos++)
		{
			for(int xpos2=4*xpos; xpos2<xwidth+4*xpos; xpos2+=4)
			{	
				pixB	= *(im_srcIn + xpos2 +		LineLength*ypos);
				pixG	= *(im_srcIn + xpos2 + 1 +	LineLength*ypos);
				pixR	= *(im_srcIn + xpos2 + 2 +	LineLength*ypos);

				if( (pixB<=filter && pixG<=filter) || (pixB<=filter && pixR<=filter) || 
					(pixG<=filter && pixR<=filter)	)
				{
					pixB2=pixB;
					pixG2=pixG;
					pixR2=pixR;

					*(im_trouble + xpos3 +		LineLength2*ypos3)	= pixB;
					*(im_trouble + xpos3 + 1 +	LineLength2*ypos3)	= pixG;
					*(im_trouble + xpos3 + 2 +	LineLength2*ypos3)	= pixR;
					
					count+=1;
					totR+=pixR2; totG+=pixG2; totB+=pixB2;
				}
				else
				{
					*(im_trouble + xpos3 +		LineLength2*ypos3)	=	0;
					*(im_trouble + xpos3 + 1 +	LineLength2*ypos3)	=	0;
					*(im_trouble + xpos3 + 2 +	LineLength2*ypos3)	=	0;
				}

				if(ypos3>0)	{ypos3-=1;}
			}

			xpos3+=4;	ypos3=31;
		}

		if(count>0)
		{
			totB=totB/count; totR=totR/count; totG=totG/count;
			resultColor = RGB(totR,totG,totB);
		}				
	}

	return resultColor;
}


SinglePoint CXAxisView::FindCapSide(BYTE* im_srcIn, int MidCap, bool left)
{
	int nCam	=	5;
	SinglePoint result; result.x=result.y=0;

	int pixa, pixb, pixc, pixd, pixe, pixf;//, pixg, pixh;
	
	int Limit=theapp->jobinfo[pframe->CurrentJobNum].neckStrength;

	int d1=0;
	
	int LineLength	=	MainDisplayImageHeight/2;
	int Height		=	MainDisplayImageWidth/2;

	int xpos		=	10;
	int ypos		=	MidCap;
	int startx		=	20;
	int endx;

	bool debugging=false;

	if(debugging)	{SaveBWCustom1(im_srcIn, LineLength, Height);}
	
	if(ypos<=Height-5 && ypos>=1)
	{
		if(left)
		{
			capLeftVal = 0;
			
			startx=30; endx=LineLength*3/4;

			for(xpos=startx; xpos<endx; xpos++)
			{	
				pixa	= *(im_srcIn+xpos-2+LineLength*(ypos-1));
				pixd	= *(im_srcIn+xpos+2+LineLength*(ypos-1));	
				
				pixb	= *(im_srcIn+xpos-2+LineLength*(ypos)); 
				pixe	= *(im_srcIn+xpos+2+LineLength*(ypos));
				
				pixc	= *(im_srcIn+xpos-2+LineLength*(ypos+1));  
				pixf	= *(im_srcIn+xpos+2+LineLength*(ypos+1));

				d1		= abs((pixa+2*pixb+pixc)-(pixd+2*pixe+pixf));
						
				if(d1>Limit)//&& (d2 > Limit) && (d3 > Limit )
				{result.y=ypos; result.x=xpos; capLeftVal = d1; break;}
			}
		}
		else
		{
			capRighVal = 0;

			startx=475; endx=LineLength/4;

			for(xpos=startx; xpos>endx; xpos--)
			{	
				pixa	= *(im_srcIn+xpos-2+LineLength*(ypos-1));  
				pixd	= *(im_srcIn+xpos+2+LineLength*(ypos-1));	

				pixb	= *(im_srcIn+xpos-2+LineLength*(ypos));  
				pixe	= *(im_srcIn+xpos+2+LineLength*(ypos));

				pixc	= *(im_srcIn+xpos-2+LineLength*(ypos+1));
				pixf	= *(im_srcIn+xpos+2+LineLength*(ypos+1));

				d1 =abs((pixa+2*pixb+pixc)-(pixd+2*pixe+pixf));
						
				if(d1>Limit)//&& (d2 > Limit) && (d3 > Limit )
				{result.y=ypos; result.x=xpos-2; capRighVal = d1; break;}
			}
		}
	}

	return result;
}


void CXAxisView::Derivative(BYTE *image, int xpos)
{
	int pixa, pixb, pixc, pixd, pixe, pixf;

	int d1=0;
	int pix1=0;

	int LineLength=CroppedCameraWidth;
	int LineLength2=270*4;
	int startx=xpos-28;//theapp->cam3Left*4;
	int endx=xpos+28;// +theapp->cam3Left*4;
	int width=endx-startx;
	int yStart=300;	int yEnd=30;
	int totalHeight=yEnd-yStart;
	int thresh=theapp->jobinfo[pframe->CurrentJobNum].labelSen;
	int t=270;
	int z=56;
	
	for(int k=startx; k<endx; k++)//
	{
		for(int i=yStart; i>yEnd; i--)
		{
			pixa	= *((image + k - 1 +	LineLength*(i-1)));
			pixb	= *((image + k +		LineLength*(i-1)));
			pixc	= *((image + k + 1 +	LineLength*(i-1)));
			pixd	= *((image + k - 1 +	LineLength*(i+1)));
			pixe	= *((image + k +		LineLength*(i+1)));
			pixf	= *((image + k + 1 +	LineLength*(i+1)));
			
			d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
						
			if(d1<=thresh)	{pix1=0;}
			else			{pix1=255;}
		
			*(im_thresh + t +		LineLength2*z) = pix1;
			*(im_thresh + t + 1 +	LineLength2*z) = pix1;
			*(im_thresh + t + 2 +	LineLength2*z) = pix1;
			
			t-=4;
		}

		if(z>=0)	{z-=1;}
		
		t=270*4;
	}

	//SaveBWSize(im_bw, width, totalHeight);
}


void CXAxisView::Threshold(BYTE *image, int xpos)
{
	int pix1=0;
	int LineLength=CroppedCameraWidth;
	int LineLength2=270*4;
	int startx=xpos-88;//theapp->cam3Left*4;
	int endx=xpos+28;// +theapp->cam3Left*4;
	int width=endx-startx;
	int yStart=300;
	int yEnd=30;
	int totalHeight=yEnd-yStart;
	int thresh=theapp->jobinfo[pframe->CurrentJobNum].labelSen;
	int t=270;
	int z=112;
	
	for(int k=startx; k<endx; k++)//
	{
		for(int i=yStart; i>yEnd; i--)
		{
			pix1=*((image+k +LineLength * (i)));
			
			if(pix1<=thresh)	{pix1=0;}
			else				{pix1=255;}
		
			*(im_thresh + t +		LineLength2*z) = pix1;
			*(im_thresh + t + 1 +	LineLength2*z) = pix1;
			*(im_thresh + t + 2 +	LineLength2*z) = pix1;
			
			t-=4;
		}

		if(z>=0)	{z-=1;}

		t=270*4;
	}

	//SaveBWSize(im_bw, width, totalHeight);
}


void CXAxisView::DoHide()
{
	if(theapp->showCam5)	{theapp->showCam5=false;}
	else						{theapp->showCam5=true;}
	
	InvalidateRect(false,NULL);
}


float CXAxisView::FindCapMid(BYTE* im_srcIn, float MidTop)
{
	int nCam	=	5;
	float result;

	int pixa, pixb, pixc, pixd, pixe, pixf;
	int pixax, pixbx, pixcx, pixdx, pixex, pixfx;
	int pixam, pixbm, pixcm, pixdm, pixem, pixfm;
	int pixal, pixbl, pixcl, pixdl, pixel, pixfl;
	int pixar, pixbr, pixcr, pixdr, pixer, pixfr;

	int Limit=theapp->jobinfo[pframe->CurrentJobNum].capStrength2;
	int dl,dr,dm;//, dmx, 
	int dx, dz;

	int LineLength	= CroppedCameraWidth/2;
	int Height		=CroppedCameraHeight/2; 
	
	bool debugging=false;

	if(debugging)	{SaveBWCustom1(im_srcIn, LineLength, Height);}

	int xpos=MidTop;
	int ypos=1;

	int xshift1 = 3;
	int xshift2 = -3;
	int xshift3 = 6;
	int xshift4 = -6;
	int maxdif = 0;

	int r = 2;
		
	if(xpos<=500 && xpos >=1 )
	{
		for(ypos=3;ypos<250;ypos++)
		{	
			pixa	= *(im_srcIn + xpos - 1 +			LineLength*(ypos-r));  	
			pixb	= *(im_srcIn + xpos +				LineLength*(ypos-r));    
			pixc	= *(im_srcIn + xpos + 1 +			LineLength*(ypos-r));  

			pixd	= *(im_srcIn + xpos - 1 +			LineLength*(ypos+r));
			pixe	= *(im_srcIn + xpos +				LineLength*(ypos+r));
			pixf	= *(im_srcIn + xpos + 1 +			LineLength*(ypos+r));
		
			pixal	= *(im_srcIn + xpos + xshift1 - 1 +	LineLength*(ypos-r));  	
			pixbl	= *(im_srcIn + xpos + xshift1 +		LineLength*(ypos-r));    
			pixcl	= *(im_srcIn + xpos + xshift1 + 1 +	LineLength*(ypos-r));  

			pixdl	= *(im_srcIn + xpos + xshift1 - 1 +	LineLength*(ypos+r));
			pixel	= *(im_srcIn + xpos + xshift1 +		LineLength*(ypos+r));
			pixfl	= *(im_srcIn + xpos + xshift1 + 1 +	LineLength*(ypos+r));

			pixar	= *(im_srcIn + xpos + xshift2 - 1 +	LineLength*(ypos-r));  	
			pixbr	= *(im_srcIn + xpos + xshift2 +		LineLength*(ypos-r));    
			pixcr	= *(im_srcIn + xpos + xshift2 + 1 +	LineLength*(ypos-r));  

			pixdr	= *(im_srcIn + xpos + xshift2 - 1 +	LineLength*(ypos+r));
			pixer	= *(im_srcIn + xpos + xshift2 +		LineLength*(ypos+r));
			pixfr	= *(im_srcIn + xpos + xshift2 + 1 +	LineLength*(ypos+r));

			pixam	= *(im_srcIn + xpos + xshift3 - 1 +	LineLength*(ypos-r));  	
			pixbm	= *(im_srcIn + xpos + xshift3 +		LineLength*(ypos-r));    
			pixcm	= *(im_srcIn + xpos + xshift3 + 1 +	LineLength*(ypos-r));  

			pixdm	= *(im_srcIn + xpos + xshift3 - 1 +	LineLength*(ypos+r));
			pixem	= *(im_srcIn + xpos + xshift3 +		LineLength*(ypos+r));
			pixfm	= *(im_srcIn + xpos + xshift3 + 1 +	LineLength*(ypos+r));

			pixax	= *(im_srcIn + xpos + xshift4 - 1 +	LineLength*(ypos-r));  	
			pixbx	= *(im_srcIn + xpos + xshift4 +		LineLength*(ypos-r));    
			pixcx	= *(im_srcIn + xpos + xshift4 + 1 +	LineLength*(ypos-r));  

			pixdx	= *(im_srcIn + xpos + xshift4 - 1 +	LineLength*(ypos+r));
			pixex	= *(im_srcIn + xpos + xshift4 +		LineLength*(ypos+r));
			pixfx	= *(im_srcIn + xpos + xshift4 + 1 +	LineLength*(ypos+r));

			dm		= abs((pixa+2*pixb+pixc)-(pixd+2*pixe+pixf));
			dz		= abs((pixam+2*pixbm+pixcm)-(pixdm+2*pixem+pixfm));
			dl		= abs((pixal+2*pixbl+pixcl)-(pixdl+2*pixel+pixfl));
			dr		= abs((pixar+2*pixbr+pixcr)-(pixdr+2*pixer+pixfr));
			dx		= abs((pixax+2*pixbx+pixcx)-(pixdx+2*pixex+pixfx));
							
			if(dm>Limit && dl>Limit && dr>Limit && dz>Limit && dx>Limit)//&& (d2 > Limit) && (d3 > Limit )
			{
				maxdif = dm;
				if(dl>maxdif) maxdif = dl;
				if(dr>maxdif) maxdif = dr;
				if(dz>maxdif) maxdif = dz;
				if(dx>maxdif) maxdif = dx;


				
				result=ypos;
			//	ypos=200;
				break;
			}
		}
	}

	currEdgeM = maxdif;


	return result;
}


SinglePoint CXAxisView::FindNeckSide(BYTE* im_srcIn, int NeckPos, int CapPos, bool left)
{
	int nCam = 5;

	SinglePoint result;

	result.x=result.y=0;

	int pixa, pixb, pixc, pixd, pixe, pixf;
	int d1, d2, d3;

	int Limit		=	40;

	int LineLength	=	MainDisplayImageHeight/2;
	int xpos		=	1;
	int ypos		=	NeckPos;

	if(ypos<=CroppedCameraHeight-30 && ypos >=1 && CapPos+100 <=MainDisplayImageHeight && CapPos-100 >=0)
	{
		if(left)
		{
			for(xpos=CapPos-100;xpos<XEnd;xpos++)
			{	
				pixa	= *(im_srcIn + xpos - 1 +	LineLength*(ypos-1));
				pixb	= *(im_srcIn + xpos - 1 +	LineLength*(ypos));    
				pixc	= *(im_srcIn + xpos - 1 +	LineLength*(ypos+1));  

				pixd	= *(im_srcIn + xpos + 1 +	LineLength*(ypos-1));	
				pixe	= *(im_srcIn + xpos + 1 +	LineLength*(ypos));
				pixf	= *(im_srcIn + xpos + 1 +	LineLength*(ypos+1));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));

				pixa	= *(im_srcIn + xpos - 1 +	LineLength*(ypos-4));  
				pixb	= *(im_srcIn + xpos - 1 +	LineLength*(ypos-3));  
				pixc	= *(im_srcIn + xpos - 1 +	LineLength*(ypos-2));  

				pixd	= *(im_srcIn + xpos + 1 +	LineLength*(ypos-4));	
				pixe	= *(im_srcIn + xpos + 1 +	LineLength*(ypos-3));
				pixf	= *(im_srcIn + xpos + 1 +	LineLength*(ypos-2));

				d2=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));	
				
				pixa	= *(im_srcIn + xpos - 1 +	LineLength*(ypos+4));  
				pixb	= *(im_srcIn + xpos - 1 +	LineLength*(ypos+3));    
				pixc	= *(im_srcIn + xpos - 1 +	LineLength*(ypos+2));  

				pixd	= *(im_srcIn + xpos + 1 +	LineLength*(ypos+4));	
				pixe	= *(im_srcIn + xpos + 1 +	LineLength*(ypos+3));
				pixf	= *(im_srcIn + xpos + 1 +	LineLength*(ypos+2));

				d3=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));

				if(d1>Limit || d2>Limit || d3>Limit)
				{result.y=ypos; result.x=xpos;	xpos=XEnd;}
			}
		}
		else
		{
			for(xpos=CapPos+100;xpos>XStart;xpos--)
			{	
				pixa	= *(im_srcIn + xpos - 1 +	LineLength*(ypos-1));  
				pixb	= *(im_srcIn + xpos - 1 +	LineLength*(ypos));    
				pixc	= *(im_srcIn + xpos - 1 +	LineLength*(ypos+1));  

				pixd	= *(im_srcIn + xpos + 1 +	LineLength*(ypos-1));	
				pixe	= *(im_srcIn + xpos + 1 +	LineLength*(ypos));
				pixf	= *(im_srcIn + xpos + 1 +	LineLength*(ypos+1));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
						
				pixa	= *(im_srcIn + xpos-1 +		LineLength*(ypos-4));  
				pixb	= *(im_srcIn + xpos-1 +		LineLength*(ypos-3));  
				pixc	= *(im_srcIn + xpos-1 +		LineLength*(ypos-2));  

				pixd	= *(im_srcIn + xpos + 1 +	LineLength*(ypos-4));	
				pixe	= *(im_srcIn + xpos + 1 +	LineLength*(ypos-3));
				pixf	= *(im_srcIn + xpos + 1 +	LineLength*(ypos-2));

				d2		= abs((pixa+2*pixb+pixc)-(pixd+2*pixe+pixf));	
				
				pixa	= *(im_srcIn + xpos-1 +		LineLength*(ypos+4));  
				pixb	= *(im_srcIn + xpos-1 +		LineLength*(ypos+3));  
				pixc	= *(im_srcIn + xpos-1 +		LineLength*(ypos+2));  

				pixd	= *(im_srcIn + xpos + 1 +	LineLength*(ypos+4));	
				pixe	= *(im_srcIn + xpos + 1 +	LineLength*(ypos+3));
				pixf	= *(im_srcIn + xpos + 1 +	LineLength*(ypos+2));

				d3		= abs((pixa+2*pixb+pixc)-(pixd+2*pixe+pixf));

				if(d1>Limit || d2>Limit || d3>Limit)
				{result.y=ypos; result.x=xpos; xpos=XStart;}
			}
		}
	}

	return result;
}


SinglePoint CXAxisView::FindCapTop(BYTE* im_srcIn, int SideDist, int side)
{
	int nCam	=	5;
	SinglePoint result;
	result.x=result.y=0;

	int pixa1, pixb1, pixc1, pixd1, pixe1, pixf1;
	int pixa2, pixb2, pixc2, pixd2, pixe2, pixf2;
	int pixa3, pixb3, pixc3, pixd3, pixe3, pixf3;
	
	int Limit=theapp->jobinfo[pframe->CurrentJobNum].capStrength;
	int d1, d2, d3;	
	
	int LineLength	= MainDisplayImageHeight/2;
	int Height		= MainDisplayImageWidth/2;
	
	bool debugging=false;

	if(debugging)	{SaveBWCustom1(im_srcIn, LineLength, Height);}

	int xpos=SideDist;
	int ypos=1;

	int XStart	= 10;
	int XEnd	= LineLength - 10;

	int maxdif = 0;

	if(xpos>=XStart && xpos<=XEnd)
	{
		for(ypos=4;ypos<Height-30;ypos++)
		{	
 			pixa1	= *(im_srcIn + xpos - 4 + LineLength*(ypos-2));  	
			pixb1	= *(im_srcIn + xpos - 3 + LineLength*(ypos-2));    
			pixc1	= *(im_srcIn + xpos - 2 + LineLength*(ypos-2));  
			pixd1	= *(im_srcIn + xpos - 4 + LineLength*(ypos+2));
			pixe1	= *(im_srcIn + xpos - 3 + LineLength*(ypos+2));
			pixf1	= *(im_srcIn + xpos - 2 + LineLength*(ypos+2));

			pixa2	= *(im_srcIn + xpos - 1 + LineLength*(ypos-2));  	
			pixb2	= *(im_srcIn + xpos + 0 + LineLength*(ypos-2));    
			pixc2	= *(im_srcIn + xpos + 1 + LineLength*(ypos-2));  
			pixd2	= *(im_srcIn + xpos - 1 + LineLength*(ypos+2));
			pixe2	= *(im_srcIn + xpos + 0 + LineLength*(ypos+2));
			pixf2	= *(im_srcIn + xpos + 1 + LineLength*(ypos+2));

			pixa3	= *(im_srcIn + xpos + 2 + LineLength*(ypos-2));  	
			pixb3	= *(im_srcIn + xpos + 3 + LineLength*(ypos-2));    
			pixc3	= *(im_srcIn + xpos + 4 + LineLength*(ypos-2));  
			pixd3	= *(im_srcIn + xpos + 2 + LineLength*(ypos+2));
			pixe3	= *(im_srcIn + xpos + 3 + LineLength*(ypos+2));
			pixf3	= *(im_srcIn + xpos + 4 + LineLength*(ypos+2));

			d1		= abs((pixa1+2*pixb1+pixc1)-(pixd1+2*pixe1+pixf1));
			d2		= abs((pixa2+2*pixb2+pixc2)-(pixd2+2*pixe2+pixf2));
			d3		= abs((pixa3+2*pixb3+pixc3)-(pixd3+2*pixe3+pixf3));
			
			if( d1>Limit || d2>Limit || d3>Limit )
			//if( d1>Limit && d2>Limit && d3>Limit )
			{
				result.y=ypos; 
				result.x=xpos; 

				maxdif = MAX(d1, d2);
				maxdif = MAX(d3, maxdif);
				ypos=Height-30;
			}
		}
	}

	if(side==0)			{currEdgeL = maxdif;}
	else if(side==1)	{currEdgeR = maxdif;}



	return result;
}


SinglePointF CXAxisView::Rgb2HS(ColorRGB rgb)
{
	SinglePointF result;

    double r = rgb.R/255.0;
    double g = rgb.G/255.0;
    double b = rgb.B/255.0;
    double v;
    double m;
    double vm;
    double r2, g2, b2;
    double h = 0; // default to black
    double s = 0;
    double l = 0;
            
	v = GetMax(3,r,g,b,0);
    m = GetMin(3,r,g,b,0);
    l = (m + v) / 2.0;

    if (l<=0.0)		{return result;}

    vm = v - m;
    s = vm;

    if (s > 0.0)	{s /= (l <= 0.5) ? (v + m ) : (2.0 - v - m) ;} 
    else			{return result;}

	r2 = (v - r) / vm;
	g2 = (v - g) / vm;
	b2 = (v - b) / vm;

   if (r == v)		{h = (g == m ? 5.0 + b2 : 1.0 - g2);}
   else if (g == v) {h = (b == m ? 1.0 + r2 : 3.0 - b2);}
   else				{h = (r == m ? 3.0 + g2 : 5.0 - r2);}

   h /= 6.0;

   result.x=h;
   result.y=s;

   return result;
}


bool CXAxisView::FoundLBolt2(BYTE *c_image, int x, int y, int cam)
{
	bool result=false;

	int pixB1, pixB2, pixB3, pixB4; 
	int pixR1, pixR2, pixR3, pixR4;
	int pixG1, pixG2, pixG3, pixG4;

	int totB, totG, totR;
	
	//double perG, perB, perR;
	//double allC;
	double hue=0;
	double sat=0;

	SinglePointF color;
	color.x=0;
	color.y=0;

	double green= double(theapp->jobinfo[pframe->CurrentJobNum].greenFilter);
	double red	= double(theapp->jobinfo[pframe->CurrentJobNum].redFilter);
	double blue	= double(theapp->jobinfo[pframe->CurrentJobNum].blueFilter);
	
	int startY=y;	int endY=y-2;

	int j, k;//, h, g, kk, jj;

	int LineLength	= (4*CroppedCameraHeight)/3;
	int LineLength2	= (CroppedCameraHeight)/3;
	
	ColorRGB rgb;
	int deadBand=20;
	int count=0;
	int avg=0;
	int finalShade=0;
	int lim=90;

	if(x % 4 !=0)
	{
		x+=1;

		if(x % 4 !=0)
		{ x+=1; if(x%4 != 0) {x+=1;} 	}
	}

	j=y;
	
	if(j<=1)	j=1;
	if(j>=200)	j=200;
	
	int startX=x*4;
	int endX=startX-32;
	
	for(k=startX; k>=endX; k-=8)//
	{	
		pixB1	= *((c_image + k +		LineLength*(j)));
		pixG1	= *((c_image + k + 1 +	LineLength*(j)));
		pixR1	= *((c_image + k + 2 +	LineLength*(j)));

		pixB2	= *((c_image + k + 4 +	LineLength*(j)));
		pixG2	= *((c_image + k + 5 +	LineLength*(j)));
		pixR2	= *((c_image + k + 6 +	LineLength*(j)));

		pixB3	= *((c_image + k +		LineLength*(j+1)));
		pixG3	= *((c_image + k + 1 +	LineLength*(j+1)));
		pixR3	= *((c_image + k + 2 +	LineLength*(j+1)));
		
		pixB4	= *((c_image + k + 4 +	LineLength*(j+1)));
		pixG4	= *((c_image + k + 5 +	LineLength*(j+1)));
		pixR4	= *((c_image + k + 6 +	LineLength*(j+1)));

		rgb.B = totB = (pixB1+pixB2+pixB3+pixB4)/4;
		rgb.G = totG = (pixG1+pixG2+pixG3+pixG4)/4;
		rgb.R = totR = (pixR1+pixR2+pixR3+pixR4)/4;

		color	= Rgb2HS(rgb);
		hue		= color.x*255;
		sat		= color.y*255;
		
		if(cam==3)	{bool stop=true;}
		
		if(hue<=20 && sat>=100)// <= red+3 && hue >= red-3
		{k=endX; j=endY; result=true;}					
	}
	
	return result;
}



TwoPoint CXAxisView::FindPattern3(BYTE* im_srcIn)
{
	TwoPoint result;
	result.x=result.y=result.x1=50;

	int Limit		=	40;
	int d1			=	0;
	int r,q;
	
	int LineLength	=	CroppedCameraHeight/3;
	int Height		=	CroppedCameraWidth/3;

	bool debugging=false;

	if(debugging)	{SaveBWCustom1(im_srcIn, LineLength, Height);}

	int		LineLength2	=	12;
	float	n			=	LineLength2*LineLength2;

	int		xpos	=	1;
	int		xpos2	=	0;
	int		ypos	=	1;
	int		ypos2	=	0;
	int		xpos3	=	0;

	float	sumxy	=	0;
	float	sumx	=	0;
	float	sumy	=	0;
	float	sumxsq	=	0;
	float	sumysq	=	0;
	
	float	f0		=	0;
	float	f1		=	0;
	float	f2		=	0;
	
	float	corr	=	0;

	float savedcorr	=	0.3f;
	int savedpos	=	0;
	int cnt			=	0;
	int startY		=	theapp->boltOffset2-theapp->boltOffset;
	
	if(startY<=10)	{startY=40;}
	if(startY>=300)	{startY=300;}
	
	for(int w=startY; w<=startY+12; w++)//22
	{
		for(q=40; q<=90; q++)
		{
			for(ypos=-6; ypos<6; ypos++)
			{
				for(xpos3=-6;xpos3<6;xpos3++)
				{					
					*(im_match3+ xpos2+ LineLength2 *(ypos2))= *(im_srcIn+ xpos3+q + LineLength * (ypos+w));
					xpos2+=1;
				}
				
				xpos2=0;
				ypos2+=1;
			}

			ypos2=0;

			//calculate correlation
			//OnSaveBW(im_match2, 6);
			
			sumxy=0.0; sumx=0.0; sumy=0.0; sumxsq=0.0; sumysq=0.0;

			for(r=0; r<n; r++)
			{
				sumxy	+= (*(im_mat3+r))*(*(im_match3+r));
				sumx	+= *(im_mat3+r);
				sumy	+= *(im_match3+r);
				sumxsq	+= (*(im_mat3+r))*(*(im_mat3+r));
				sumysq	+= (*(im_match3+r))*(*(im_match3+r));
			}

			f0 = sumxy-((sumx*sumy)/n);
			f1 = sumxsq-((sumx*sumx)/n);
			f2 = sumysq-((sumy*sumy)/n);
			
			if( sqrtf(f1*f2)!=0.0 ) {corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedcorr)
			{
				savedcorr=corr;
				savedpos=q;
				result.x1=int(savedcorr*100);
				result.y=w+6;
				result.x=q+6;
			}	
		}
	}
	
	return result;
}


int CXAxisView::NoLabel(BYTE *im_src)
{
	int result;
	result=0;

	int Limit=theapp->jobinfo[pframe->CurrentJobNum].labelSen;

	int LineLength	=	CroppedCameraHeight/3;
	int xEnd		=	120;
	int xStart		=	10;//rect.right;
	int yStart		=	42;
	int yEnd		=	yStart+4;
	int pixa		=	0;
	
	for(int xpos=xStart; xpos<=xEnd; xpos++)
	{
		for(int ypos=yStart; ypos<=yEnd; ypos++)
		{	
			pixa = *(im_src + xpos + LineLength*ypos);  
		
			if(pixa>=Limit)	{result+=1;}//white bottle
		}
	}

	return result;
}


int CXAxisView::CheckIntegrity(BYTE *im_src, TwoPoint labelPos)
{
	int result;	result=0;

	int Limit=40;
	int d1=0;
	int r,q;
	
	int LineLength	=	CroppedCameraHeight/3;
	int LineLength2	=	12;

	int xpos=1;		int ypos=1;
	int xpos2=0;	int ypos2=0;
	int xpos3=0;

	float sumxy=0;
	float sumx=0;
	float sumy=0;
	float sumxsq=0;
	float sumysq=0;
	float n=144;
	float f0=0;
	float f1=0;
	float f2=0;
	float corr=0;
	float savedcorr=0.3f;

	int savedpos=0;
	int cnt=0;
	int w;
	int startY=labelPos.y-30;
	
	if(startY<=10)	startY=10;

	int startX=labelPos.x-theapp->boltOffset2;

	if(startX<=10)		startX=10;
	if(startX+42 >=130) startX=130-42;
	
	//int neckBarX=(theapp->jobinfo[pframe->CurrentJobNum].neckBarX)*2;

	for(w=startY; w<=startY+20; w++)
	{
		for(q=startX; q<=startX+42; q++)
		{
			for(ypos=-6; ypos<6; ypos++)
			{
				for(xpos3=-6;xpos3<6;xpos3++)
				{					
					*(im_match2 + xpos2 + LineLength2*ypos2)= *(im_src+ xpos3 + q + LineLength*(ypos+w));
					xpos2+=1;
				}

				xpos2=0; ypos2+=1;
			}

			ypos2=0;
			//calculate correlation
			//OnSaveBW(im_match2, 6);

			sumxy=0.0;
			sumx=0.0;
			sumy=0.0;
			sumxsq=0.0;
			sumysq=0.0;

			for(r=0; r < n; r++)
			{
				sumxy	+= (*(im_mat+r))*(*(im_match2+r));
				sumx	+= *(im_mat+r);
				sumy	+= *(im_match2+r);
				sumxsq	+= (*(im_mat+r))*(*(im_mat+r));
				sumysq	+= (*(im_match2+r))*(*(im_match2+r));
			}

			f0	= sumxy-((sumx * sumy)/n);
			f1	= sumxsq-((sumx * sumx)/n);
			f2	= sumysq-((sumy * sumy)/n);
			
			if( sqrtf(f1*f2)!=0.0 ) {corr=f0/sqrtf(f1*f2);}else{corr=0;}
			
			if(corr>=savedcorr)
			{
				savedcorr=corr;
				savedpos=q;
				result=int(savedcorr*100);
			}	
		}
	}
	
	return result;
}

TwoPoint CXAxisView::CheckLabel(BYTE* im_s, int y)
{
	TwoPoint result;
	result.x=result.y=result.x1=result.y1=0;
	
	int yStart		=	y;

	if(yStart<=20)	{yStart=20;}
	if(yStart>=300)	{yStart=300;}
	
	int yEnd=y+40;
	
	int xStart=45;
	int xEnd=90;

	int LineLength	=	CroppedCameraHeight/3;
	int d1			=	0;

	int pixa;
	int brightest	=	0;

	int Thresh		=	theapp->jobinfo[pframe->CurrentJobNum].noLabelSen;
	
	for(int xpos=xStart; xpos<=xEnd; xpos++)
	{
		for(int ypos=yStart; ypos<=yEnd; ypos++)
		{	
			pixa=*(im_s + xpos + LineLength*ypos);  
			
			if(pixa>=Thresh)	{result.x+=1;}
		}
	}

	return result;
}

//returns the amount of pixels that exceed a threshold
TwoPoint CXAxisView::CheckLabelNatural(int nCam, int fact)
{
	int filterSelNolabel =	theapp->jobinfo[pframe->CurrentJobNum].filterSelNolabel;

	if(filterSelNolabel==1)
	{
		if(nCam==1)	{img1ForNoLabelDiv3 = im_cambw1;}
		if(nCam==2)	{img2ForNoLabelDiv3 = im_cambw2;}
		if(nCam==3)	{img3ForNoLabelDiv3 = im_cambw3;}
		if(nCam==4)	{img4ForNoLabelDiv3 = im_cambw4;}
	}
	else if(filterSelNolabel==2)
	{
		if(nCam==1)	{img1ForNoLabelDiv3 = img1SatDiv3;}
		if(nCam==2)	{img2ForNoLabelDiv3 = img2SatDiv3;}
		if(nCam==3)	{img3ForNoLabelDiv3 = img3SatDiv3;}
		if(nCam==4)	{img4ForNoLabelDiv3 = img4SatDiv3;}
	}
	else if(filterSelNolabel==3)
	{
		if(nCam==1)	{img1ForNoLabelDiv3 = img1MagDiv3;}
		if(nCam==2)	{img2ForNoLabelDiv3 = img2MagDiv3;}
		if(nCam==3)	{img3ForNoLabelDiv3 = img3MagDiv3;}
		if(nCam==4)	{img4ForNoLabelDiv3 = img4MagDiv3;}
	}
	else if(filterSelNolabel==4)
	{
		if(nCam==1)	{img1ForNoLabelDiv3 = img1YelDiv3;}
		if(nCam==2)	{img2ForNoLabelDiv3 = img2YelDiv3;}
		if(nCam==3)	{img3ForNoLabelDiv3 = img3YelDiv3;}
		if(nCam==4)	{img4ForNoLabelDiv3 = img4YelDiv3;}
	}
	else if(filterSelNolabel==5)	
	{
		if(nCam==1)	{img1ForNoLabelDiv3 = img1CyaDiv3;}
		if(nCam==2)	{img2ForNoLabelDiv3 = img2CyaDiv3;}
		if(nCam==3)	{img3ForNoLabelDiv3 = img3CyaDiv3;}
		if(nCam==4)	{img4ForNoLabelDiv3 = img4CyaDiv3;}		
	}
	else if(filterSelNolabel==6)	
	{
		if(nCam==1)	{img1ForNoLabelDiv3 = im_cam1Rbw;}
		if(nCam==2)	{img2ForNoLabelDiv3 = im_cam2Rbw;}
		if(nCam==3)	{img3ForNoLabelDiv3 = im_cam3Rbw;}
		if(nCam==4)	{img4ForNoLabelDiv3 = im_cam4Rbw;}
	}
	else if(filterSelNolabel==7)
	{
		if(nCam==1)	{img1ForNoLabelDiv3 = im_cam1Gbw;}
		if(nCam==2)	{img2ForNoLabelDiv3 = im_cam2Gbw;}
		if(nCam==3)	{img3ForNoLabelDiv3 = im_cam3Gbw;}
		if(nCam==4)	{img4ForNoLabelDiv3 = im_cam4Gbw;}
	}
	else if(filterSelNolabel==8)	
	{
		if(nCam==1)	{img1ForNoLabelDiv3 = im_cam1Bbw;}
		if(nCam==2)	{img2ForNoLabelDiv3 = im_cam2Bbw;}
		if(nCam==3)	{img3ForNoLabelDiv3 = im_cam3Bbw;}
		if(nCam==4)	{img4ForNoLabelDiv3 = im_cam4Bbw;}
	}

	TwoPoint result;
	result.x=result.y=result.x1=result.y1=0;

	int Length	=	MainDisplayImageWidth/fact;
	int Height	=	MainDisplayImageHeight/fact;

	int yStart	=	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset;
	int xOffset =	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[nCam]/fact; 

	wNoLbl[nCam]=	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetW/fact; 
	hNoLbl[nCam]=	theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetH/fact; 

	xNoLbl[nCam]=	MIN(xOffset+30, Length - wNoLbl[nCam] - 5);
	yNoLbl[nCam]=	MIN(yStart,		Height - hNoLbl[nCam] - 5);

	int d1		=	0;
	int pixa;
	int Thresh	=	theapp->jobinfo[pframe->CurrentJobNum].noLabelSen;
	int sumPix	=	0;
	int nPix	=	0;

	if(nCam==1)
	{
		for(int xpos=xNoLbl[nCam]; xpos<=(xNoLbl[nCam]+wNoLbl[nCam]); xpos++)
		{
			for(int ypos=yNoLbl[nCam]; ypos<=yNoLbl[nCam]+hNoLbl[nCam]; ypos++)
			{	
				pixa=*(img1ForNoLabelDiv3 + xpos + Length*ypos);  
				
				if(pixa>=Thresh)	{result.x+=1;}
				
				sumPix +=pixa;	nPix++;
			}
		}
	}
	else if(nCam==2)
	{
		for(int xpos=xNoLbl[nCam]; xpos<=(xNoLbl[nCam]+wNoLbl[nCam]); xpos++)
		{
			for(int ypos=yNoLbl[nCam]; ypos<=yNoLbl[nCam]+hNoLbl[nCam]; ypos++)
			{	
				pixa=*(img2ForNoLabelDiv3 + xpos + Length*ypos);  
				
				if(pixa>=Thresh)	{result.x+=1;}
				
				sumPix +=pixa;	nPix++;
			}
		}
	}
	else if(nCam==3)
	{
		for(int xpos=xNoLbl[nCam]; xpos<=(xNoLbl[nCam]+wNoLbl[nCam]); xpos++)
		{
			for(int ypos=yNoLbl[nCam]; ypos<=yNoLbl[nCam]+hNoLbl[nCam]; ypos++)
			{	
				pixa=*(img3ForNoLabelDiv3 + xpos + Length*ypos);  
				
				if(pixa>=Thresh)	{result.x+=1;}
				
				sumPix +=pixa;	nPix++;
			}
		}
	}
	else if(nCam==4)
	{
		for(int xpos=xNoLbl[nCam]; xpos<=(xNoLbl[nCam]+wNoLbl[nCam]); xpos++)
		{
			for(int ypos=yNoLbl[nCam]; ypos<=yNoLbl[nCam]+hNoLbl[nCam]; ypos++)
			{	
				pixa=*(img4ForNoLabelDiv3 + xpos + Length*ypos);  
				
				if(pixa>=Thresh)	{result.x+=1;}
				
				sumPix +=pixa;	nPix++;
			}
		}
	}	

	if(nPix!=0) avgNoLabel[nCam] = sumPix/nPix;

	return result;
}

COLORREF CXAxisView::CheckForSplice(BYTE *im_color, int y)
{
	COLORREF resultColor;
	
	int pixB, pixR, pixG;//pixB2, pixR2, pixG2,;
	int	totR, totB, totG;
	totR=totB=totG=0;

	int	count		= 0;
	int LineLength	= 4*((CroppedCameraHeight)/3);

	int ypos	= 0;
	int yheight	= 4;
	int filter	= 255;
	
	int xStart	= 30;
	int xEnd	= 100;

	if( (y>=10) && (y+yheight <=CroppedCameraHeight-4) )
	{
		for(ypos=y; ypos<yheight+y; ypos++)
		{
			for(int xpos2=xStart*4; xpos2<xEnd*4; xpos2+=4)
			{	
				pixB=*(im_color + xpos2 +		LineLength*ypos);
				pixG=*(im_color + xpos2 + 1 +	LineLength*ypos);
				pixR=*(im_color + xpos2 + 2 +	LineLength*ypos);
						
				if( (pixB+pixG+pixR)/3 <=filter )
				{count+=1; totR+=pixR; totG+=pixG; totB+=pixB;}
			}
		}

		if(count>0)
		{
			totB=(totB/count); totR=(totR/count); totG=(totG/count); 
			resultColor = RGB(totR,totG,totB);
		}						
	}

	return resultColor;
}

TwoPoint CXAxisView::FindLedge3(BYTE *image, TwoPoint Bolt, int cam)
{
	TwoPoint result;
	result.x=result.y=result.x1=result.y1=50;

	result.x		=	Bolt.x;

	int pixa, pixb, pixc, pixd, pixe, pixf;
	int pixa2, pixb2, pixc2, pixd2, pixe2, pixf2;
	int pixa3, pixb3, pixc3, pixd3, pixe3, pixf3;
	
	int smallValue	=	0;

	SinglePoint posR,  posL;
	SinglePoint posR2, posL2;

	int d1, d2, d3; d1=d2=d3=0;

	int combined	=	0;
	int startX		=	46;
	int endX		=	83;

	int startY		=	theapp->boltOffset2;

	if(startY>=340)	{startY=340;}
	if(startY<=30)	{startY=30;}
	
	int endY		=	startY-theapp->boltOffset;
	
	int xposR		=	83;
	int xposR2		=	73;
	int xposL2		=	56;
	int xposL		=	46;
	
	int h,g;//,j,k,d,t;
	int smallest	=	1;

	int LineLength2	=	CroppedCameraHeight/3;

	int o			=	0;
	int p			=	0;
	int count		=	5;
	int avg			=	0;
	int finalShade	=	0;
	int thresh		=	theapp->jobinfo[pframe->CurrentJobNum].ellipseSen;
	int avgy		=	0;
	int best		=	0;
	int shift		=	30;
	int left		=	0;
	int right		=	0;
	int direction	=	0;

	for(h=startX; h<endX; h+=2)
	{
		for(g=startY; g>endY; g--)
		{
			pixa	= *(image + h + 1 +	(LineLength2*(g)));  	
			pixb	= *(image + h +		(LineLength2*(g)));   
			pixc	= *(image + h - 1 +	(LineLength2*(g)));  

			pixd	= *(image + h + 1 +	(LineLength2*(g+4)));
			pixe	= *(image + h +		(LineLength2*(g+4)));
			pixf	= *(image + h - 1 +	(LineLength2*(g+4)));

			d1=( (pixa+2*pixb+pixc)-(pixd+2*pixe+pixf) );	
			
			if(d1>=thresh)
			{ledgePoint[count].x=h; ledgePoint[count].y=g; g=endY;}
			else
			{ledgePoint[count].x=0; ledgePoint[count].y=200;}
		}

		count+=1;		
	}

	for(int jj=7; jj<count; jj++)
	{
		direction=ledgePoint[jj].y-ledgePoint[jj-1].y;
		
		if(direction<0)	{left+=1;}
		if(direction>0)	{right+=1;}
	}

	h=xposR;

	for(g=startY; g> endY; g--)
	{
		pixa	= *(image + h + 1 +		(LineLength2*(g)));  	
		pixb	= *(image + h +			(LineLength2*(g)));   
		pixc	= *(image + h - 1 +		(LineLength2*(g)));  

		pixd	= *(image + h + 1 +		(LineLength2*(g+4)));
		pixe	= *(image + h +			(LineLength2*(g+4)));
		pixf	= *(image + h - 1 +		(LineLength2*(g+4)));

		pixa2	= *(image + h + 1 + 1 +	(LineLength2*(g-1)));  	
		pixb2	= *(image + h + 1 +		(LineLength2*(g-1)));   
		pixc2	= *(image + h - 1 + 1 +	(LineLength2*(g-1)));  

		pixd2	= *(image + h + 1 + 1 +	(LineLength2*(g+3)));
		pixe2	= *(image + h + 1 +		(LineLength2*(g+3)));
		pixf2	= *(image + h - 1 + 1 +	(LineLength2*(g+3)));

		pixa3	= *(image + h + 1 + 2 +	(LineLength2*(g-2)));  	
		pixb3	= *(image + h + 2 +		(LineLength2*(g-2)));   
		pixc3	= *(image + h - 1 + 2 +	(LineLength2*(g-2)));  

		pixd3	= *(image + h + 1 + 2 +	(LineLength2*(g+2)));
		pixe3	= *(image + h + 2 +		(LineLength2*(g+2)));
		pixf3	= *(image + h - 1 + 2 +	(LineLength2*(g+2)));

		d1		= ((pixa+2*pixb+pixc)-(pixd+2*pixe+pixf));
		d2		= ((pixa2+2*pixb2+pixc2)-(pixd2+2*pixe2+pixf2));
		d3		= ((pixa3+2*pixb3+pixc3)-(pixd3+2*pixe3+pixf3));
		
		if(d1>=thresh && d2>=thresh && d3>=thresh )
		{ledgePoint[1].x=posR.x=h; ledgePoint[1].y=posR.y=g; g=endY;}
		else
		{ledgePoint[1].x=posR.x=0; ledgePoint[1].y=posR.y=200;}
	}

	h=xposR2;

	for(g=startY; g> endY; g--)
	{
		pixa	= *(image + h + 1 +		LineLength2*(g));  	
		pixb	= *(image + h +			LineLength2*(g));   
		pixc	= *(image + h - 1 +		LineLength2*(g));  

		pixd	= *(image + h + 1 +		LineLength2*(g+4));
		pixe	= *(image + h +			LineLength2*(g+4));
		pixf	= *(image + h - 1 +		LineLength2*(g+4));

		pixa2	= *(image + h + 1 + 1 +	LineLength2*(g-1));  	
		pixb2	= *(image + h + 1 +		LineLength2*(g-1));   
		pixc2	= *(image + h - 1 + 1 +	LineLength2*(g-1));  

		pixd2	= *(image + h + 1 + 1 +	LineLength2*(g+3));
		pixe2	= *(image + h + 1 +		LineLength2*(g+3));
		pixf2	= *(image + h - 1 + 1 +	LineLength2*(g+3));

		pixa3	= *(image + h + 1 + 2 +	LineLength2*(g-2));  	
		pixb3	= *(image + h + 2 +		LineLength2*(g-2));   
		pixc3	= *(image + h - 1 + 2 +	LineLength2*(g-2));  

		pixd3	= *(image + h + 1 + 2 +	LineLength2*(g+2));
		pixe3	= *(image + h + 2 +		LineLength2*(g+2));
		pixf3	= *(image + h - 1 + 2 +	LineLength2*(g+2));

		d1		= ((pixa+2*pixb+pixc)-(pixd+2*pixe+pixf));
		d2		= ((pixa2+2*pixb2+pixc2)-(pixd2+2*pixe2+pixf2));
		d3		= ((pixa3+2*pixb3+pixc3)-(pixd3+2*pixe3+pixf3));
		
		if(d1>=thresh && d2>=thresh && d3>=thresh )
		{ledgePoint[2].x=posR2.x=h; ledgePoint[2].y=posR2.y=g; g=endY;}
		else
		{ledgePoint[2].x=posR2.x=0;	ledgePoint[2].y=posR2.y=200;}
	}
		
	h=xposL;
	
	for(g=startY; g> endY; g--)
	{
		pixa	= *(image + h + 1 +		LineLength2*(g));  	
		pixb	= *(image + h +			LineLength2*(g));   
		pixc	= *(image + h - 1 +		LineLength2*(g));  

		pixd	= *(image + h + 1 +		LineLength2*(g+4));
		pixe	= *(image + h +			LineLength2*(g+4));
		pixf	= *(image + h - 1 +		LineLength2*(g+4));

		pixa2	= *(image + h + 1 + 1 +	LineLength2*(g+1));  	
		pixb2	= *(image + h + 1 +		LineLength2*(g+1));   
		pixc2	= *(image + h - 1 + 1 +	LineLength2*(g+1));  

		pixd2	= *(image + h + 1 + 1 +	LineLength2*(g+5));
		pixe2	= *(image + h + 1 +		LineLength2*(g+5));
		pixf2	= *(image + h - 1 + 1 + LineLength2*(g+5));

		pixa3	= *(image + h + 1 + 2 +	LineLength2*(g+2));  	
		pixb3	= *(image + h + 2 +		LineLength2*(g+2));   
		pixc3	= *(image + h - 1 + 2 +	LineLength2*(g+2));  

		pixd3	= *(image + h + 1 + 2 +	LineLength2*(g+6));
		pixe3	= *(image + h + 2 +		LineLength2*(g+6));
		pixf3	= *(image + h - 1 + 2 +	LineLength2*(g+6));

		d1	= ((pixa+2*pixb+pixc)-(pixd+2*pixe+pixf));
		d2	= ((pixa2+2*pixb2+pixc2)-(pixd2+2*pixe2+pixf2));
		d3	= ((pixa3+2*pixb3+pixc3)-(pixd3+2*pixe3+pixf3));
		
		if(d1>=thresh && d2>=thresh && d3>=thresh )
		{ledgePoint[3].x=posL.x=h; ledgePoint[3].y=posL.y=g; g=endY;}
		else
		{ledgePoint[3].x=posL.x=0; ledgePoint[3].y=posL.y=200;}
	}

	h=xposL2;
		
	for(g=startY; g> endY; g--)
	{
		pixa	= *(image + h + 1 +		LineLength2*(g));  	
		pixb	= *(image + h +			LineLength2*(g));   
		pixc	= *(image + h - 1 +		LineLength2*(g));  

		pixd	= *(image + h + 1 +		LineLength2*(g+4));
		pixe	= *(image + h +			LineLength2*(g+4));
		pixf	= *(image + h - 1 +		LineLength2*(g+4));

		pixa2	= *(image + h + 1 + 1 +	LineLength2*(g+1));  	
		pixb2	= *(image + h + 1 +		LineLength2*(g+1));   
		pixc2	= *(image + h - 1 + 1 +	LineLength2*(g+1));  

		pixd2	= *(image + h + 1 + 1 +	LineLength2*(g+5));
		pixe2	= *(image + h + 1 +		LineLength2*(g+5));
		pixf2	= *(image + h - 1 + 1 +	LineLength2*(g+5));

		pixa3	= *(image + h + 1 + 2 +	LineLength2*(g+2));  	
		pixb3	= *(image + h + 2 +		LineLength2*(g+2));   
		pixc3	= *(image + h - 1 + 2 +	LineLength2*(g+2));  

		pixd3	= *(image + h + 1 + 2 +	LineLength2*(g+6));
		pixe3	= *(image + h + 2 +		LineLength2*(g+6));
		pixf3	= *(image + h - 1 + 2 +	LineLength2*(g+6));

		d1	= ((pixa+2*pixb+pixc)-(pixd+2*pixe+pixf));
		d2	= ((pixa2+2*pixb2+pixc2)-(pixd2+2*pixe2+pixf2));
		d3	= ((pixa3+2*pixb3+pixc3)-(pixd3+2*pixe3+pixf3));
		
		if(d1>=thresh && d2>=thresh && d3>=thresh )
		{ledgePoint[4].x=posL2.x=h; ledgePoint[4].y=posL2.y=g; g=endY;}
		else
		{ledgePoint[4].x=posL2.x=0;	ledgePoint[4].y=posL2.y=200;}
	}

	int pR	= startY - posR.y;
	int pR2	= startY - posR2.y;
	int pL	= startY - posL.y;
	int pL2	= startY - posL2.y;

	if(pR2>pL2)					{result.y1=38+pR2;}
	else if(pL2>pR2)			{result.y1=42-pL2;}
	
	if(pR2>pR && pL2<0 && pL<0)	{result.y1=36-pR2;}
	if(pL2>pL && pR2<0 && pR<0)	{result.y1=38+pL2;}
	
	if(pR2<15 && pL2<15)
	{
		if(pR>=pL)				{result.y1=34+pR;}
		else if(pL>pR)			{result.y1=46-pL;}
	}
		
	if(pR2<0 && pL2<0 && pL<0)	{result.y1=34-pR;}
	if(pR2<0 && pL2<0 && pR<0)	{result.y1=38+pL;}

	result.x1=Bolt.x;

	int modRes=(135-Bolt.x)/2;

	if(modRes<=25)	{ if(left>right) result.y1=200; }
	if(modRes>=54)	{ if(left<right) result.y1=200; }
		
	float resTemp=float(result.y1)*0.94;
	result.y1=resTemp;		
	
	return result;
}

BYTE* CXAxisView::LoadAnything(BYTE* image)
{
	int LineLength=CroppedCameraWidth*4;
	
	for(int i=0; i<CroppedCameraHeight;i++)
	{
		for(int k=0; k<CroppedCameraWidth;k++)
		{*(image +k +(LineLength) * i )=150;}
	}

	return im_reduce5;
}

TwoPoint CXAxisView::GetLayers(BYTE *im_src)
{
	TwoPoint result;
	result.x=result.y=result.x1=result.y1=0;

	int l1	=	63;
	int l2	=	126;
	int l3	=	189;
	int l4	=	255;

	int LineLength	=	(CroppedCameraHeight)/3;
	int xStart		=	25;//rect.right;
	int xEnd		=	110;

	int yStart	=	72;
	int yEnd	=	yStart+35;

	int pixa	=	0;
	
	for(int xpos=xStart; xpos<=xEnd; xpos++)
	{
		for(int ypos=yStart; ypos<=yEnd; ypos++)
		{	
			pixa=*(im_src+xpos+(LineLength * (ypos)));  
		
			if(pixa<=l1)			result.x	+= 1;//white bottle
			if(pixa>l1 && pixa<=l2) result.y	+= 1;
			if(pixa>l2 && pixa<=l3) result.x1	+= 1;
			if(pixa>l3 && pixa<=l4) result.y1	+= 1;
		}
	}

	return result;
}

TwoPoint CXAxisView::CheckSportBand(BYTE* im_srcIn, SinglePoint CapLTop, SinglePoint CapRTop)
{
	TwoPoint result;
	result.x=result.y=result.x1=result.y1=0;

	int xpos	=	0;
	int ypos	=	theapp->jobinfo[pframe->CurrentJobNum].sportPos;//CapLTop.y-90;
	
	if(ypos<=3)	{ypos=3;}
	
	int count=0;

	int LineLength	= CroppedCameraWidth/2;
	int Height		= CroppedCameraHeight/2;
	
	bool debugging=false;

	if(debugging)
	{SaveBWCustom1(im_srcIn, LineLength, Height);}

	int pixa, pixb, pixc, pixd, pixe, pixf;
	int Limit	=	theapp->jobinfo[pframe->CurrentJobNum].sportSen;

	int d1=0;

	if(CapLTop.x>10 && CapLTop.x<500 && CapRTop.x>10 && CapRTop.x<500 && ypos>=3 && ypos<100)
	{
		for(xpos=CapLTop.x-5;xpos<400;xpos++)
		{	
			pixa	= *(im_srcIn + xpos - 2 + LineLength*(ypos-1));
			pixb	= *(im_srcIn + xpos - 2 + LineLength*(ypos)); 
			pixc	= *(im_srcIn + xpos - 2 + LineLength*(ypos+1));  

			pixd	= *(im_srcIn + xpos + 2 + LineLength*(ypos-1));
			pixe	= *(im_srcIn + xpos + 2 + LineLength*(ypos));
			pixf	= *(im_srcIn + xpos + 2 + LineLength*(ypos+1));

			d1=abs((pixa+2*pixb+pixc)-(pixd+2*pixe+pixf));
						
			if(d1>Limit)//&& (d2 > Limit) && (d3 > Limit )
			{result.y=ypos; result.x=xpos; xpos=400;}
		}

		for(xpos=CapRTop.x+5;xpos>100;xpos--)
		{	
			pixa	= *(im_srcIn + xpos - 2 + LineLength*(ypos-1));
			pixb	= *(im_srcIn + xpos - 2 + LineLength*(ypos));   
			pixc	= *(im_srcIn + xpos - 2 + LineLength*(ypos+1)); 

			pixd	= *(im_srcIn + xpos + 2 + LineLength*(ypos-1));
			pixe	= *(im_srcIn + xpos + 2 + LineLength*(ypos));
			pixf	= *(im_srcIn + xpos + 2 + LineLength*(ypos+1));

			d1=abs((pixa+2*pixb+pixc)-(pixd+2*pixe+pixf));
						
			if(d1>Limit)//&& (d2 > Limit) && (d3 > Limit )
			{result.y1=ypos; result.x1=xpos; xpos=100;}
		}
	}
				
	return result;
}

void CXAxisView::OnSaveSet(BYTE *im_sr1, BYTE *im_sr2, BYTE *im_sr3, BYTE *im_sr4, BYTE *im_sr5, int picnum)
{
	char buf[10];
	
	CString c =itoa(picnum,buf,10);
	CString d = c;
	CString number;

	char* pFileName = "c:\\silgandata\\xtra\\C";
	char currentpath[60];

	int xdist;
	int ydist;

/// Andrew Gawlik 2017-06-06
/// commenting out this code as it is suspect to be the culprit of the camera bug
/*
	for(int i = 1; i <= 5; i++)		//	5 is number of cams
	{
		number.Format("%d_", i);

		strcpy(currentpath,pFileName);
		strcat(currentpath,number);
		strcat(currentpath,d);
		strcat(currentpath,".bmp");
	
		xdist	=	CroppedCameraWidth; 
		ydist	=	CroppedCameraHeight;
	
		CVisRGBAByteImage image(xdist,ydist,1,-1,im_sr1);
		imageCS		=	image;
	
		try
		{
			SVisFileDescriptor filedescriptorT;
			ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
			filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
			filedescriptorT.jpeg_quality = 0;
			filedescriptorT.filename = currentpath;
			imageCS.WriteFile(filedescriptorT);
		}
		catch (...)
		{AfxMessageBox("The file could not be saved. ");}
	}
	*/

	//replacement with each camera separated
	//camera 1
	number.Format("%d_", 1);

	strcpy(currentpath,pFileName);
	strcat(currentpath,number);
	strcat(currentpath,d);
	strcat(currentpath,".bmp");

	xdist	=	CroppedCameraWidth; 
	ydist	=	CroppedCameraHeight;

	CVisRGBAByteImage image(xdist,ydist,1,-1,im_sr1);
	imageCS		=	image;

	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imageCS.WriteFile(filedescriptorT);
	}
	catch (...)
	{AfxMessageBox("The file could not be saved. ");}

	//camera 2
	number.Format("%d_", 2);

	strcpy(currentpath,pFileName);
	strcat(currentpath,number);
	strcat(currentpath,d);
	strcat(currentpath,".bmp");

	xdist	=	CroppedCameraWidth; 
	ydist	=	CroppedCameraHeight;

	CVisRGBAByteImage image2(xdist,ydist,1,-1,im_sr2);
	imageCS		=	image2;

	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imageCS.WriteFile(filedescriptorT);
	}
	catch (...)
	{AfxMessageBox("The file could not be saved. ");}

	//camera 3
	number.Format("%d_", 3);

	strcpy(currentpath,pFileName);
	strcat(currentpath,number);
	strcat(currentpath,d);
	strcat(currentpath,".bmp");

	xdist	=	CroppedCameraWidth; 
	ydist	=	CroppedCameraHeight;

	CVisRGBAByteImage image3(xdist,ydist,1,-1,im_sr3);
	imageCS		=	image3;

	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imageCS.WriteFile(filedescriptorT);
	}
	catch (...)
	{AfxMessageBox("The file could not be saved. ");}

	//camera 4
	number.Format("%d_", 4);

	strcpy(currentpath,pFileName);
	strcat(currentpath,number);
	strcat(currentpath,d);
	strcat(currentpath,".bmp");

	xdist	=	CroppedCameraWidth; 
	ydist	=	CroppedCameraHeight;

	CVisRGBAByteImage image4(xdist,ydist,1,-1,im_sr4);
	imageCS		=	image4;

	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imageCS.WriteFile(filedescriptorT);
	}
	catch (...)
	{AfxMessageBox("The file could not be saved. ");}

	//camera 5
	if(theapp->cam5Enable)
	{
		number.Format("%d_", 5);

		strcpy(currentpath,pFileName);
		strcat(currentpath,number);
		strcat(currentpath,d);
		strcat(currentpath,".bmp");

		xdist	=	CroppedCameraWidth; 
		ydist	=	CroppedCameraHeight;

		CVisRGBAByteImage image5(xdist,ydist,1,-1,im_sr5);
		imageCS		=	image5;

		try
		{
			SVisFileDescriptor filedescriptorT;
			ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
			filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
			filedescriptorT.jpeg_quality = 0;
			filedescriptorT.filename = currentpath;
			imageCS.WriteFile(filedescriptorT);
		}
		catch (...)
		{AfxMessageBox("The file could not be saved. ");}
	}
}

void CXAxisView::LoadImagePic(int picNum)
{
	int width,height;
	//To Measure Total Time
	QueryPerformanceFrequency(&Frequency); QueryPerformanceCounter(&timeStart);

	gotPic1 = false;
	gotPic2 = false;
	gotPic3 = false;
	gotPic4 = false;
	gotPic5 = false;

	///////////////////////

	CVisRGBAByteImage imagex;
	//int nCam;
	char* pFileName2;

	char buf[10];
	char currentpath[60];

	pic1+=1;
	if(pic1>=156)pic1=1;

	CString c =itoa(picNum,buf,10);
	CString d = c;
	
	pFileName2 = "c:\\silgandata\\xtra\\C1_";//drive	
	strcpy(currentpath,pFileName2);
	strcat(currentpath,d);
	strcat(currentpath,".bmp");

	try
	{
		imagex.ReadFile(currentpath);
		height = imagex.Height();
		width = imagex.Width();
		mFlgWaist[1] = (height > camera1CroppedImageHeight);
		theapp->camWidth[1] = mFlgWaist[1] ? camera1FullImageWidth : camera1CroppedImageWidth;
		theapp->camHeight[1] = mFlgWaist[1] ? camera1FullImageHeight : camera1CroppedImageHeight;
		
		imageFile1.Allocate(imagex.Rect());
		imagex.CopyPixelsTo(imageFile1);
		im_dataC=imageFile1.PbFirstPixelInRow(0);
		gotPic1 = true;

		Display(NULL);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Give up trying to read an image from this file.
		// Warn the user.
		AfxMessageBox("The file specified could not be opened.");
		theapp->pframeSub->KillContinuousTrigger();
	}

	////////////////

	CVisRGBAByteImage imagey;
	
	pFileName2 = "c:\\silgandata\\xtra\\C2_";//drive	
	strcpy(currentpath,pFileName2);
	strcat(currentpath,d);
	strcat(currentpath,".bmp");

	try
	{
		imagey.ReadFile(currentpath);
		height = imagey.Height();
		width = imagey.Width();
		mFlgWaist[2] = (height > camera2CroppedImageHeight);
		theapp->camWidth[2] = mFlgWaist[2] ? camera2FullImageWidth : camera2CroppedImageWidth;
		theapp->camHeight[2] = mFlgWaist[2] ? camera2FullImageHeight : camera2CroppedImageHeight;
		imageFile2.Allocate(imagey.Rect());
		imagey.CopyPixelsTo(imageFile2);
		im_dataC2=imageFile2.PbFirstPixelInRow(0);
		gotPic2 = true;		

		Display2(NULL);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Give up trying to read an image from this file.
		// Warn the user.
		AfxMessageBox("The file specified could not be opened.");
		theapp->pframeSub->KillContinuousTrigger();
	}

	////////////////

	CVisRGBAByteImage imagez;
	
	pFileName2 = "c:\\silgandata\\xtra\\C3_";//drive	
	strcpy(currentpath,pFileName2);
	strcat(currentpath,d);
	strcat(currentpath,".bmp");

	try
	{
		imagez.ReadFile(currentpath);
		height = imagez.Height();
		width = imagez.Width();
		mFlgWaist[3] = (height > camera3CroppedImageHeight);
		theapp->camWidth[3] = mFlgWaist[3] ? camera3FullImageWidth : camera3CroppedImageWidth;
		theapp->camHeight[3] = mFlgWaist[3] ? camera3FullImageHeight : camera3CroppedImageHeight;
		imageFile3.Allocate(imagez.Rect());
		imagez.CopyPixelsTo(imageFile3);
		im_dataC3=imageFile3.PbFirstPixelInRow(0);
		gotPic3 = true;

		width = imagex.Width();
		height = imagex.Height();

		Display3(NULL);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Give up trying to read an image from this file.
		// Warn the user.
		AfxMessageBox("The file specified could not be opened.");
		theapp->pframeSub->KillContinuousTrigger();
	}

	////////////////

	CVisRGBAByteImage imager;

	pFileName2 = "c:\\silgandata\\xtra\\C4_";//drive	
	strcpy(currentpath,pFileName2);
	strcat(currentpath,d);
	strcat(currentpath,".bmp");

	try
	{
		imager.ReadFile(currentpath);
		height = imager.Height();
		width = imager.Width();
		mFlgWaist[4] = (height > camera4CroppedImageHeight);
		theapp->camWidth[4] = mFlgWaist[4] ? camera4FullImageWidth : camera4CroppedImageWidth;
		theapp->camHeight[4] = mFlgWaist[4] ? camera4FullImageHeight : camera4CroppedImageHeight;
		imageFile4.Allocate(imager.Rect());
		imager.CopyPixelsTo(imageFile4);
		im_dataC4=imageFile4.PbFirstPixelInRow(0);
		gotPic4 = true;

		width = imagex.Width();
		height = imagex.Height();

		Display4(NULL);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Give up trying to read an image from this file.
		// Warn the user.
		AfxMessageBox("The file specified could not be opened.");
		theapp->pframeSub->KillContinuousTrigger();
	}

	////////////////

	//cam 5
	CVisRGBAByteImage imaget;

	//encapsulating camera5 display inside a conditional requiring camera5 to be enabled
	if(theapp->cam5Enable)
	{

		pFileName2 = "c:\\silgandata\\xtra\\C5_";//drive	
		strcpy(currentpath,pFileName2);
		strcat(currentpath,d);
		strcat(currentpath,".bmp");

		try
		{
			imaget.ReadFile(currentpath);
			imageFile5.Allocate(imaget.Rect());
			imaget.CopyPixelsTo(imageFile5);
			im_dataC5=imageFile5.PbFirstPixelInRow(0);
			gotPic5 = true;
			Display5(NULL);
		}
		catch (...)  // "..." is bad - we could leak an exception object.
		{
			// Give up trying to read an image from this file.
			// Warn the user.
			AfxMessageBox("The file specified could not be opened.");
			theapp->pframeSub->KillContinuousTrigger();
		}
	}

	InvalidateRect(false);
}


void CXAxisView::DuplicateImage(unsigned char *originalImage, unsigned char *duplicateImage,
								int imageWidth, int imageHeight, int bytesPerPix)
{
	int imageSizeOrg = imageWidth*imageHeight*bytesPerPix;
	memcpy (duplicateImage, originalImage, imageSizeOrg );
}


void CXAxisView::CropCameraImage(unsigned char *imageToCrop, unsigned char *croppedImage,
								 int imageWidth, int imageHeight, 
								 int croppedImageWidth, int croppedImageHeight,
								 int croppedImageXOffset, int croppedImageYOffset, 
								 int bytesPerPix)
{
	int imageStrideNew = croppedImageWidth*bytesPerPix;

	for(int y = 0; y < croppedImageHeight; y++ )
    {
		memcpy (croppedImage + (y * croppedImageWidth * bytesPerPix),
				imageToCrop + ((y + croppedImageYOffset) * imageWidth * bytesPerPix) + (croppedImageXOffset * bytesPerPix),
				imageStrideNew);
    }
}


//this methods finds the minimum average of pixel value found in a rectangle
//and it also calculates the standard deviation inside of that box
void CXAxisView::findSplice(int nCam, int fact,
							int xSplice, int ySplice, int wSplice, 
							int hSplice, int hsmSplice)
{
	////////////////////////////
	int filterSelSplice =	theapp->jobinfo[pframe->CurrentJobNum].filterSelSplice;

	//Img selection
	if(filterSelSplice==1)		
	{
		if(nCam==1)	{img1ForSpliceDiv3 = im_cambw1;}
		if(nCam==2)	{img2ForSpliceDiv3 = im_cambw2;}
		if(nCam==3)	{img3ForSpliceDiv3 = im_cambw3;}
		if(nCam==4)	{img4ForSpliceDiv3 = im_cambw4;}
	}
	else if(filterSelSplice==2)
	{
		if(nCam==1)	{img1ForSpliceDiv3 = img1SatDiv3;}
		if(nCam==2)	{img2ForSpliceDiv3 = img2SatDiv3;}
		if(nCam==3)	{img3ForSpliceDiv3 = img3SatDiv3;}
		if(nCam==4)	{img4ForSpliceDiv3 = img4SatDiv3;}
	}
	else if(filterSelSplice==3)	
	{
		if(nCam==1)	{img1ForSpliceDiv3 = img1MagDiv3;}
		if(nCam==2)	{img2ForSpliceDiv3 = img2MagDiv3;}
		if(nCam==3)	{img3ForSpliceDiv3 = img3MagDiv3;}
		if(nCam==4)	{img4ForSpliceDiv3 = img4MagDiv3;}
	}
	else if(filterSelSplice==4)	
	{
		if(nCam==1)	{img1ForSpliceDiv3 = img1YelDiv3;}
		if(nCam==2)	{img2ForSpliceDiv3 = img2YelDiv3;}
		if(nCam==3)	{img3ForSpliceDiv3 = img3YelDiv3;}
		if(nCam==4)	{img4ForSpliceDiv3 = img4YelDiv3;}
	}
	else if(filterSelSplice==5)	
	{
		if(nCam==1)	{img1ForSpliceDiv3 = img1CyaDiv3;}
		if(nCam==2)	{img2ForSpliceDiv3 = img2CyaDiv3;}
		if(nCam==3)	{img3ForSpliceDiv3 = img3CyaDiv3;}
		if(nCam==4)	{img4ForSpliceDiv3 = img4CyaDiv3;}
	}
	else if(filterSelSplice==6)	
	{
		if(nCam==1)	{img1ForSpliceDiv3 = im_cam1Rbw;}
		if(nCam==2)	{img2ForSpliceDiv3 = im_cam2Rbw;}
		if(nCam==3)	{img3ForSpliceDiv3 = im_cam3Rbw;}
		if(nCam==4)	{img4ForSpliceDiv3 = im_cam4Rbw;}
	}
	else if(filterSelSplice==7)	
	{
		if(nCam==1)	{img1ForSpliceDiv3 = im_cam1Gbw;}
		if(nCam==2)	{img2ForSpliceDiv3 = im_cam2Gbw;}
		if(nCam==3)	{img3ForSpliceDiv3 = im_cam3Gbw;}
		if(nCam==4)	{img4ForSpliceDiv3 = im_cam4Gbw;}
	}
	else if(filterSelSplice==8)	
	{
		if(nCam==1)	{img1ForSpliceDiv3 = im_cam1Bbw;}
		if(nCam==2)	{img2ForSpliceDiv3 = im_cam2Bbw;}
		if(nCam==3)	{img3ForSpliceDiv3 = im_cam3Bbw;}
		if(nCam==4)	{img4ForSpliceDiv3 = im_cam4Bbw;}
	}
	else if(filterSelSplice==9)	
	{
		if(nCam==1)	{img1ForSpliceDiv3 = img1Sa2Div3;}
		if(nCam==2)	{img2ForSpliceDiv3 = img2Sa2Div3;}
		if(nCam==3)	{img3ForSpliceDiv3 = img3Sa2Div3;}
		if(nCam==4)	{img4ForSpliceDiv3 = img4Sa2Div3;}
	}

	////////////////////////////

	int Length		= MainDisplayImageWidth/fact;
	int Height		= MainDisplayImageHeight/fact;

	int xsplice		= xSplice/fact;
	int ysplice		= ySplice/fact;
	int wsplice		= wSplice/fact;
	int hsplice		= hSplice/fact;
	int hsplicesmBx	= hsmSplice/fact;

	////////////////////////////

	bool debugging=false;

	if(debugging)	{SaveBWCustom1(img1ForSpliceDiv3, Length, Height);}

	int sumPix=0;
	int y=0;
	int pix;

	int sumWhitePix = 0;

	
	minSpliceAvg[nCam] = 255;

	//If out of bounds, just exit
	if(	wsplice<=0 || hsplice<=0 || hsplicesmBx<=0 ||
		xsplice<0 || ysplice<0 ||
		(xsplice + wsplice)>Length || 
		(ysplice+hsplice)>Height)
	{return;}

	if(nCam==1)
	{
		for(int i=0; i<hsplice; i++)
		{
			y = i + ysplice; sumPix=0;
			
			for(int x=xsplice; x<xsplice+wsplice; x++)
			{pix = *(img1ForSpliceDiv3 + x + Length*y); sumPix+=pix;}
			
			avgPixInRow[i] = sumPix/wsplice;  //Calculating the pixel intenstity of each row
		}
	}
	else if(nCam==2)
	{
		for(int i=0; i<hsplice; i++)
		{
			y = i + ysplice; sumPix=0;
			
			for(int x=xsplice; x<xsplice+wsplice; x++)
			{pix = *(img2ForSpliceDiv3 + x + Length*y); sumPix+=pix;}
			
			avgPixInRow[i] = sumPix/wsplice;
		}
	}
	else if(nCam==3)
	{
		for(int i=0; i<hsplice; i++)
		{
			y = i + ysplice; sumPix=0;
			
			for(int x=xsplice; x<xsplice+wsplice; x++)
			{pix = *(img3ForSpliceDiv3 + x + Length*y); sumPix+=pix;}
			
			avgPixInRow[i] = sumPix/wsplice;
		}
	}
	else if(nCam==4)
	{
		for(int i=0; i<hsplice; i++)
		{
			y = i + ysplice; sumPix=0;
			
			for(int x=xsplice; x<xsplice+wsplice; x++)
			{pix = *(img4ForSpliceDiv3 + x + Length*y); sumPix+=pix;}
			
			avgPixInRow[i] = sumPix/wsplice;
		}
	}

	int sumAvg=0;
	int currentAvgInGreenBox=0;
	int minAvg=255;

	//Efficient method
	int i;
	for(i=0; i<hsplicesmBx; i++)
	{sumAvg+=avgPixInRow[i];}

	if(hsplicesmBx>0)
	{minAvg = sumAvg/hsplicesmBx;}

	yposSplice[nCam] = ysplice*fact;

	for(i=0; i<hsplice-hsplicesmBx; i++)
	{
		//substracting the previous row
		sumAvg -= avgPixInRow[i];
		//adding the next row
		sumAvg += avgPixInRow[i+hsplicesmBx];

		if(hsplicesmBx>0)
		{
			currentAvgInGreenBox = sumAvg/hsplicesmBx;

			if(currentAvgInGreenBox<minAvg)
			{
				minAvg = currentAvgInGreenBox; 
				yposSplice[nCam] = (i+ysplice+1)*fact;
			}
		}
	}

	minSpliceAvg[nCam] = minAvg;

	//////////////////////////////////////////////
	sumPix = 0;
	int sumPixPix = 0;
	int npix = 0;
	
	//in case of the black splice finding the minimum is enough
	//but in the case of silver splice we also need to calculate the standard deviation
	
	if(nCam==1)
	{
		for(y=yposSplice[nCam]/fact; y<(yposSplice[nCam]/fact + hsplicesmBx); y++)
		{
			for(int x=xsplice; x<xsplice+wsplice; x++)
			{
				pix = *(img1ForSpliceDiv3 + x + Length*y); 
				sumPix		+= pix;
				sumPixPix	+= (pix*pix);
				npix++;
			}
		}
	}
	else if(nCam==2)
	{
		for(y=yposSplice[nCam]/fact; y<(yposSplice[nCam]/fact + hsplicesmBx); y++)
		{
			for(int x=xsplice; x<xsplice+wsplice; x++)
			{
				pix = *(img2ForSpliceDiv3 + x + Length*y); 
				sumPix		+= pix;
				sumPixPix	+= (pix*pix);
				npix++;
			}
		}
	}
	else if(nCam==3)
	{
		for(y=yposSplice[nCam]/fact; y<(yposSplice[nCam]/fact + hsplicesmBx); y++)
		{
			for(int x=xsplice; x<xsplice+wsplice; x++)
			{
				pix = *(img3ForSpliceDiv3 + x + Length*y); 
				sumPix		+= pix;
				sumPixPix	+= (pix*pix);
				npix++;
			}
		}
	}
	else if(nCam==4)
	{
		for(y=yposSplice[nCam]/fact; y<(yposSplice[nCam]/fact + hsplicesmBx); y++)
		{
			for(int x=xsplice; x<xsplice+wsplice; x++)
			{
				pix = *(img4ForSpliceDiv3 + x + Length*y); 
				sumPix		+= pix;
				sumPixPix	+= (pix*pix);
				npix++;
			}
		}
	}

	if(npix<=0) {npix=1;}

	int avgpix = sumPix/npix;
	int avgpixpix = sumPixPix/npix;

	varianceSplice[nCam] = avgpixpix - avgpix*avgpix;
	sdeviatiSplice[nCam] = sqrt(abs(varianceSplice[nCam]));
}


TwoPoint CXAxisView::findSideEdge(BYTE *imgIn, int nCam)
{
	TwoPoint result;
	
	bool debugging = false;

	int Length	= CroppedCameraHeight/3;
	int Height	= CroppedCameraWidth/3;

	if(debugging)	{SaveBWCustom1(imgIn, Length, Height);}

	int pix1, pix2, diff;
	int th = 50;
	int width	= theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[nCam];
	int ypos	= theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[nCam];

	result.x = result.x1 = 0;
	result.y = result.y1 = ypos;

	if(theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[nCam])
	{
		for(int x = 0; x<Length/2; x++)
		{
			pix1 = *(imgIn + x +		Length*ypos);
			pix2 = *(imgIn + x + 2 +	Length*ypos);

			diff = pix2 - pix1;

			if(diff>th)
			{result.x = x; x = Length/2;}
		}

		result.x1 = result.x + width;
	}
	else
	{
		for(int x = Length; x>Length/2; x--)
		{
			pix1 = *(imgIn + x +		Length*ypos);
			pix2 = *(imgIn + x - 2 +	Length*ypos);

			diff = pix2 - pix1;

			if(diff>th)
			{result.x = x; x = Length/2;}
		}

		result.x1 = result.x - width;
	}

	return result;
}

TwoPoint CXAxisView::FindPatternX(BYTE *im_srcIn, int nCam)
{
	TwoPoint result;
	result.x = result.y = result.x1 = 50;

	int Limit			=	40;
	int d1				=	0;
	int r;//,q;
	
	int LineLength		=	CroppedCameraHeight/3;
	int Height			=	CroppedCameraWidth/3;
	
	bool debugging=false;

	if(debugging)		{SaveBWCustom1(im_srcIn, LineLength, Height);}

	int LineLength2		=	12;
	float	n			=	LineLength2*LineLength2;

	int xpos2=0;	int ypos2=0;

	float	sumXY		=	0;
	float	sumX		=	0;
	float	sumY		=	0;
	float	sumXX		=	0;
	float	sumYY		=	0;

	float	f0			=	0;
	float	f1			=	0;
	float	f2			=	0;
	float	corr		=	0;
	float	savedcorr	=	0.3f;
	int		savedpos	=	0;
	int		cnt			=	0;

	int		startY		=	theapp->jobinfo[pframe->CurrentJobNum].orienArea;

	/////////////////

	sumX=0.0;	sumXX=0.0;
	
	for(r=0; r < n; r++)
	{
		sumX	+= *(im_mat+r);
		sumXX	+= (*(im_mat+r))*(*(im_mat+r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;

	//////////////////

	if(startY<=10)	{startY=10;}

	for(int y=startY; y<=startY+26; y++)//22
	{
		for(x=10; x<=126; x++)
		{
			sumXY=0.0; sumY=0.0;  sumYY=0.0;
			
			for(ypos=-6; ypos<6; ypos++)
			{
				for(xpos=-6; xpos<6; xpos++)
				{	
					pix = *(im_srcIn + xpos + x + LineLength*(ypos+y));
					pixT= *(im_mat + xpos + 6 + 12*(ypos+6));
					
					sumY	+= pix;
					sumYY	+= pix*pix;
					sumXY	+= pix*pixT;

					//*(im_match2+ xpos2+ LineLength2 *ypos2)= pix;
					//xpos2+=1;
				}

				//xpos2=0; ypos2+=1;
			}

			//ypos2=0;

			f0	=	sumXY - ((sumX*sumY)/n);
			f1	=	sumXX - ((sumX*sumX)/n);
			f2	=	sumYY - ((sumY*sumY)/n);
			
			if(sqrtf(f1*f2)!=0.0)	{corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedcorr)
			{
				savedcorr	=	corr;
				savedpos	=	x;
				result.x1	=	int(savedcorr*100);
				result.y	=	y;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x=savedpos;

	return result;
}

TwoPoint CXAxisView::FindPatternX1_C1(BYTE *im_srcIn, int nCam)
{
	TwoPoint result;
	result.x = result.y = result.x1 = 50;

	int Limit			=	40;
	int d1				=	0;
	int r;//,q;
	
	int LineLength		=	CroppedCameraHeight/3;
	int Height			=	CroppedCameraWidth/3;
	
	bool debugging=false;

	if(debugging)	{SaveBWCustom1(im_srcIn, LineLength, Height);}

	int		LineLength2	=	12;
	float	n			=	LineLength2*LineLength2;  // since it is a square

	int		xpos2		=	0;	
	int		ypos2		=	0;

	float	sumXY		=	0;
	float	sumX		=	0;
	float	sumXX		=	0;
	float	sumY		=	0;
	float	sumYY		=	0;

	float	f0			=	0;
	float	f1			=	0;
	float	f2			=	0;
	float	corr		=	0;

	float	savedBestCorr	=	0.3f;
	int		savedPosX		=	0;
	int		cnt				=	0;
	int		startY			=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt;

	/////////////////

	sumX = 0.0;	sumXX = 0.0;
	
	for(r=0; r < n; r++)
	{
		sumX	+= *(im_mat+r);
		sumXX	+= (*(im_mat+r))*(*(im_mat+r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;
	bool atInitLoop = false;

	//////////////////

//	int yIni	= theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/3;
//	int yEnd	= theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/3;

	
	int szpattHalf = LineLength2/2;
	int yTopLim = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/3;
	int yBotLim = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/3;
	int yIni	= MAX(yTopLim + szpattHalf, szpattHalf);
	int yEnd	= MIN(Height-szpattHalf,yBotLim-szpattHalf);

	//for(int y=startY; y<=startY+searchHeight; y++)//22
	for(int y=yIni; y<=yEnd; y++)
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop	=	true;

		for(x=10; x<=126; x++)
		{
			sumXY	=	0.0; 
			
			for(ypos=-6; ypos<6; ypos++)
			{
				if(atInitLoop)
				{
					for(xpos=-6; xpos<6; xpos++)
					{
						pixT	=	*(im_mat+xpos+6+12*(ypos+6));
						pix		=	*(im_srcIn+ xpos + x + LineLength*(ypos+y));
						
						sumY	+=	pix;
						sumYY	+=	pix*pix;
						sumXY	+=	pixT*pix;
					}
				}
				else
				{
					pixOld		=	*(im_srcIn - 7 + x + LineLength*(ypos+y));
					pixNew		=	*(im_srcIn + 5 + x + LineLength*(ypos+y));
					
					for(xpos=-6; xpos<6; xpos++)
					{
						pixT	=	*(im_mat+xpos+6+12*(ypos+6));
						pix		=	*(im_srcIn+ xpos + x + LineLength*(ypos+y));
						
						sumXY	+=	pixT*pix;
					}

					sumY		+=	pixNew;
					sumY		-=	pixOld;
					sumYY		+=	pixNew*pixNew;
					sumYY		-=	pixOld*pixOld;
				}
			}

			atInitLoop = false;

			f0	=	sumXY - ((sumX*sumY)/n);
			f1	=	sumXX - ((sumX*sumX)/n);
			f2	=	sumYY - ((sumY*sumY)/n);
			
			if(sqrtf(f1*f2)!=0.0)	{corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedBestCorr)
			{
				savedBestCorr	=	corr;
				savedPosX		=	x;
				result.x1		=	int(savedBestCorr*100);
				result.y		=	y;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x	=	savedPosX;

	return result;
}


TwoPoint CXAxisView::FindPatternX1_C2(BYTE *im_srcIn, int nCam)
{
	TwoPoint result;
	result.x = result.y = result.x1 = 50;

	int Limit			=	40;
	int d1				=	0;
	int r;//,q;
	
	int LineLength		=	CroppedCameraHeight/3;
	int Height			=	CroppedCameraWidth/3;
	
	bool debugging=false;

	if(debugging)	{SaveBWCustom1(im_srcIn, LineLength, Height);}

	int		LineLength2	=	12;
	float	n			=	LineLength2*LineLength2;

	int		xpos2		=	0;	
	int		ypos2		=	0;

	float	sumXY		=	0;
	float	sumX		=	0;
	float	sumXX		=	0;
	float	sumY		=	0;
	float	sumYY		=	0;

	float	f0			=	0;
	float	f1			=	0;
	float	f2			=	0;
	float	corr		=	0;

	float	savedBestCorr	=	0.3f;
	int		savedPosX		=	0;
	int		cnt				=	0;
	int		startY			=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt;

	/////////////////

	sumX = 0.0;	sumXX = 0.0;
	
	for(r=0; r < n; r++)
	{
		sumX	+= *(im_mat+r);
		sumXX	+= (*(im_mat+r))*(*(im_mat+r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;
	bool atInitLoop = false;

	//////////////////
//	int yIni	= theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/3;
//	int yEnd	= theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/3;

	
	int szpattHalf = LineLength2/2;
	int yTopLim = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/3;
	int yBotLim = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/3;
	int yIni	= MAX(yTopLim + szpattHalf, szpattHalf);
	int yEnd	= MIN(Height-szpattHalf,yBotLim-szpattHalf);

	//for(int y=startY; y<=startY+searchHeight; y++)//22
	for(int y=yIni; y<=yEnd; y++)
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop	=	true;

		for(x=10; x<=126; x++)
		{
			sumXY	=	0.0; 
			
			for(ypos=-6; ypos<6; ypos++)
			{
				if(atInitLoop)
				{
					for(xpos=-6; xpos<6; xpos++)
					{
						pixT	=	*(im_mat+xpos+6+12*(ypos+6));
						pix		=	*(im_srcIn+ xpos + x + LineLength*(ypos+y));
						
						sumY	+=	pix;
						sumYY	+=	pix*pix;
						sumXY	+=	pixT*pix;
					}
				}
				else
				{
					pixOld		=	*(im_srcIn - 7 + x + LineLength*(ypos+y));
					pixNew		=	*(im_srcIn + 5 + x + LineLength*(ypos+y));
					
					for(xpos=-6; xpos<6; xpos++)
					{
						pixT	=	*(im_mat+xpos+6+12*(ypos+6));
						pix		=	*(im_srcIn+ xpos + x + LineLength*(ypos+y));
						
						sumXY	+=	pixT*pix;
					}

					sumY		+=	pixNew;
					sumY		-=	pixOld;
					sumYY		+=	pixNew*pixNew;
					sumYY		-=	pixOld*pixOld;
				}
			}

			atInitLoop = false;

			f0	=	sumXY - ((sumX*sumY)/n);
			f1	=	sumXX - ((sumX*sumX)/n);
			f2	=	sumYY - ((sumY*sumY)/n);
			
			if(sqrtf(f1*f2)!=0.0)	{corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedBestCorr)
			{
				savedBestCorr	=	corr;
				savedPosX		=	x;
				result.x1		=	int(savedBestCorr*100);
				result.y		=	y;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x	=	savedPosX;

	return result;
}


TwoPoint CXAxisView::FindPatternX1_C3(BYTE *im_srcIn, int nCam)
{
	TwoPoint result;
	result.x = result.y = result.x1 = 50;

	int Limit			=	40;
	int d1				=	0;
	int r;//,q;
	
	int LineLength		=	CroppedCameraHeight/3;
	int Height			=	CroppedCameraWidth/3;
	
	bool debugging=false;

	if(debugging)	{SaveBWCustom1(im_srcIn, LineLength, Height);}

	int		LineLength2	=	12;
	float	n			=	LineLength2*LineLength2;

	int		xpos2		=	0;	
	int		ypos2		=	0;

	float	sumXY		=	0;
	float	sumX		=	0;
	float	sumXX		=	0;
	float	sumY		=	0;
	float	sumYY		=	0;

	float	f0			=	0;
	float	f1			=	0;
	float	f2			=	0;
	float	corr		=	0;

	float	savedBestCorr	=	0.3f;
	int		savedPosX		=	0;
	int		cnt				=	0;
	int		startY			=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt;

	/////////////////

	sumX = 0.0;	sumXX = 0.0;
	
	for(r=0; r < n; r++)
	{
		sumX	+= *(im_mat+r);
		sumXX	+= (*(im_mat+r))*(*(im_mat+r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;
	bool atInitLoop = false;

	//////////////////
//	int yIni	= theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/3;
//	int yEnd	= theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/3;

	
	int szpattHalf = LineLength2/2;
	int yTopLim = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/3;
	int yBotLim = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/3;
	int yIni	= MAX(yTopLim + szpattHalf, szpattHalf);
	int yEnd	= MIN(Height-szpattHalf,yBotLim-szpattHalf);

	//for(int y=startY; y<=startY+searchHeight; y++)//22
	for(int y=yIni; y<=yEnd; y++)
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop	=	true;

		for(x=10; x<=126; x++)
		{
			sumXY	=	0.0; 
			
			for(ypos=-6; ypos<6; ypos++)
			{
				if(atInitLoop)
				{
					for(xpos=-6; xpos<6; xpos++)
					{
						pixT	=	*(im_mat+xpos+6+12*(ypos+6));
						pix		=	*(im_srcIn+ xpos + x + LineLength*(ypos+y));
						
						sumY	+=	pix;
						sumYY	+=	pix*pix;
						sumXY	+=	pixT*pix;
					}
				}
				else
				{
					pixOld		=	*(im_srcIn - 7 + x + LineLength*(ypos+y));
					pixNew		=	*(im_srcIn + 5 + x + LineLength*(ypos+y));
					
					for(xpos=-6; xpos<6; xpos++)
					{
						pixT	=	*(im_mat+xpos+6+12*(ypos+6));
						pix		=	*(im_srcIn+ xpos + x + LineLength*(ypos+y));
						
						sumXY	+=	pixT*pix;
					}

					sumY		+=	pixNew;
					sumY		-=	pixOld;
					sumYY		+=	pixNew*pixNew;
					sumYY		-=	pixOld*pixOld;
				}
			}

			atInitLoop = false;

			f0	=	sumXY - ((sumX*sumY)/n);
			f1	=	sumXX - ((sumX*sumX)/n);
			f2	=	sumYY - ((sumY*sumY)/n);
			
			if(sqrtf(f1*f2)!=0.0)	{corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedBestCorr)
			{
				savedBestCorr	=	corr;
				savedPosX		=	x;
				result.x1		=	int(savedBestCorr*100);
				result.y		=	y;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x	=	savedPosX;

	return result;
}

TwoPoint CXAxisView::FindPatternX1_C4(BYTE *im_srcIn, int nCam)
{
	TwoPoint result;
	result.x = result.y = result.x1 = 50;

	int Limit			=	40;
	int d1				=	0;
	int r;//,q;
	
	int LineLength		=	CroppedCameraHeight/3;
	int Height			=	CroppedCameraWidth/3;
	
	bool debugging=false;

	if(debugging)	{SaveBWCustom1(im_srcIn, LineLength, Height);}

	int		LineLength2	=	12;
	float	n			=	LineLength2*LineLength2;

	int		xpos2		=	0;	
	int		ypos2		=	0;

	float	sumXY		=	0;
	float	sumX		=	0;
	float	sumXX		=	0;
	float	sumY		=	0;
	float	sumYY		=	0;

	float	f0			=	0;
	float	f1			=	0;
	float	f2			=	0;
	float	corr		=	0;

	float	savedBestCorr	=	0.3f;
	int		savedPosX		=	0;
	int		cnt				=	0;
	int		startY			=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt;

	/////////////////

	sumX = 0.0;	sumXX = 0.0;
	
	for(r=0; r < n; r++)
	{
		sumX	+= *(im_mat+r);
		sumXX	+= (*(im_mat+r))*(*(im_mat+r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;
	bool atInitLoop = false;

	//////////////////
//	int yIni	= theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/3;
//	int yEnd	= theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/3;

	
	int szpattHalf = LineLength2/2;
	int yTopLim = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/3;
	int yBotLim = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/3;
	int yIni	= MAX(yTopLim + szpattHalf, szpattHalf);
	int yEnd	= MIN(Height-szpattHalf,yBotLim-szpattHalf);

	//for(int y=startY; y<=startY+searchHeight; y++)//22
	for(int y=yIni; y<=yEnd; y++)
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop	=	true;

		for(x=10; x<=126; x++)
		{
			sumXY	=	0.0; 
			
			for(ypos=-6; ypos<6; ypos++)
			{
				if(atInitLoop)
				{
					for(xpos=-6; xpos<6; xpos++)
					{
						pixT	=	*(im_mat+xpos+6+12*(ypos+6));
						pix		=	*(im_srcIn+ xpos + x + LineLength*(ypos+y));
						
						sumY	+=	pix;
						sumYY	+=	pix*pix;
						sumXY	+=	pixT*pix;
					}
				}
				else
				{
					pixOld		=	*(im_srcIn - 7 + x + LineLength*(ypos+y));
					pixNew		=	*(im_srcIn + 5 + x + LineLength*(ypos+y));
					
					for(xpos=-6; xpos<6; xpos++)
					{
						pixT	=	*(im_mat+xpos+6+12*(ypos+6));
						pix		=	*(im_srcIn+ xpos + x + LineLength*(ypos+y));
						
						sumXY	+=	pixT*pix;
					}

					sumY		+=	pixNew;
					sumY		-=	pixOld;
					sumYY		+=	pixNew*pixNew;
					sumYY		-=	pixOld*pixOld;
				}
			}

			atInitLoop = false;

			f0	=	sumXY - ((sumX*sumY)/n);
			f1	=	sumXX - ((sumX*sumX)/n);
			f2	=	sumYY - ((sumY*sumY)/n);
			
			if(sqrtf(f1*f2)!=0.0)	{corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedBestCorr)
			{
				savedBestCorr	=	corr;
				savedPosX		=	x;
				result.x1		=	int(savedBestCorr*100);
				result.y		=	y;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x	=	savedPosX;

	return result;
}


void CXAxisView::OnLoadPatternX(int Job, int pattType)
{
	CVisByteImage image;
	int pxWidth;  //pixel width
	int pxHeight; //pixel height
	BYTE pxValue; //pixel value

	int		szPatt6	=	theapp->sizepattBy6;
	int		szPatt3W=	theapp->sizepattBy3_2W;
	int		szPatt3H=	theapp->sizepattBy3_2H;

	bool	success	=	false;

	CString c;	c=itoa(Job,buf,10);
		
	////////////

	//char	buf[10];
	//char	currentpath[60];
	char	currentpathTxt2[60];
	char	currentpathTxt3[60];
	char	currentpathBmp2[60];
	char	currentpathBmp3[60];

	char	*pFileName = "c:\\silgandata\\pat\\";//drive
	CString diffError = "The label pattern diff scales needs to be taught first";

	strcpy(currentpathTxt2,pFileName);	strcat(currentpathTxt2,c);
	strcpy(currentpathTxt3,pFileName);	strcat(currentpathTxt3,c);
	strcpy(currentpathBmp2,pFileName);	strcat(currentpathBmp2,c);
	strcpy(currentpathBmp3,pFileName);	strcat(currentpathBmp3,c);

	//if(pattType==1)	
	//{
	//	strcat(currentpathTxt2,"\\imgBy6L.txt"); 
	//	strcat(currentpathTxt3,"\\imgBy3L.txt");
	//}
	//
	//if(pattType==2)	
	//{
	//	strcat(currentpathTxt2,"\\imgBy6M.txt"); 
	//	strcat(currentpathTxt3,"\\imgBy3M.txt");
	//}
	//
	//if(pattType==3)	
	//{
	//	strcat(currentpathTxt2,"\\imgBy6R.txt"); 
	//	strcat(currentpathTxt3,"\\imgBy3R.txt");
	//}

	switch(pattType)
	{
		case 1:
		{
			strcat(currentpathTxt2,"\\imgBy6L.txt");
			strcat(currentpathBmp2,"\\patBy6L.bmp"); 

			strcat(currentpathTxt3,"\\imgBy3L.txt");
			strcat(currentpathBmp3,"\\patBy3L.bmp");
			break;
		}
		case 2:
		{
			strcat(currentpathTxt2,"\\imgBy6M.txt"); 
			strcat(currentpathBmp2,"\\patBy6M.bmp"); 

			strcat(currentpathTxt3,"\\imgBy3M.txt");
			strcat(currentpathBmp3,"\\patBy3M.bmp");
			break;
		}
		case 3:
		{
			strcat(currentpathTxt2,"\\imgBy6R.txt");
			strcat(currentpathBmp2,"\\patBy6R.bmp"); 

			strcat(currentpathTxt3,"\\imgBy3R.txt");
			strcat(currentpathBmp3,"\\patBy3R.bmp");
			break;
		}
		default:
		{
			strcat(currentpathTxt2,"\\imgBy6.txt");  //error if here
			strcat(currentpathBmp2,"\\patBy6.bmp");

			strcat(currentpathTxt3,"\\imgBy3.txt");
			strcat(currentpathBmp3,"\\patBy3.bmp");
		}
	}

	/////////////////////

	CString N2; 
	char	bufDig2[2];
	int		digit2;
	CFileException ce;

	//try loading the bmp first then the text file as a backup
	try
	{
		image.ReadFile(currentpathBmp2);
		pxWidth = image.Width();
		pxHeight = image.Height();
		
		//treat bitmap image as single channel

		for(int col=0; col<pxWidth; col++)
		{
			for(int  row=0; row<pxHeight; row++)
			{
				pxValue = image.Pixel(col,row); //assuming only one band since image needs to be grayscale

				*(bytePattBy6[pattType]+col+row*pxWidth)=pxValue;
			}
		}

		pframe->JobNoGood=false;
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		success=ImageFile.Open(currentpathTxt2, CFile::modeRead, &ce) > 0;

		if(success!=0)
		{
			for(int g=0; g<szPatt6*szPatt6; g++)
			{
				ImageFile.Read(bufDig2,3);
				N2	 =	bufDig2[0];
				N2	+=	bufDig2[1];
				N2	+=	bufDig2[2];
				digit2=	atoi(N2);
				
				N2	=	"";
				*(bytePattBy6[pattType]+g)=digit2;
			}

			ImageFile.Close();
			pframe->JobNoGood=false;
		}
		else
		{
			AfxMessageBox(diffError + "\n" + currentpathTxt2);
			wehaveTempX=false;
		}
	}
	////////////////////

	CString N3; 
	char	bufDig3[2];
	int		digit3;

	//try loading the bmp first then the text file as a backup

	try
	{
		image.ReadFile(currentpathBmp3);
		pxWidth = image.Width();
		pxHeight = image.Height();
		
		//treat bitmap image as single channel

		for(int col=0; col<pxWidth; col++)
		{
			for(int  row=0; row<pxHeight; row++)
			{
				pxValue = image.Pixel(col,row); //assuming only one band since image needs to be grayscale

				*(bytePattBy3[pattType]+col+row*pxWidth) = pxValue;
			}
		}

		pframe->JobNoGood=false;
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		success=ImageFile.Open(currentpathTxt3, CFile::modeRead, &ce) > 0;

		if(success!=0)
		{
			for(int g=0; g<szPatt3W*szPatt3H; g++)
			{
				ImageFile.Read(bufDig3,3);
				N3	 =	bufDig3[0];
				N3	+=	bufDig3[1];
				N3	+=	bufDig3[2];
				digit3=	atoi(N3);
				
				N3	=	"";
				*(bytePattBy3[pattType]+g)=digit3;
			}

			ImageFile.Close();
			pframe->JobNoGood=false;
		}
		else
		{
			AfxMessageBox(diffError + "\n" + currentpathTxt3);
			wehaveTempX=false;
		}
	}
	/////////////////////

	bool debugging=false;
	if(debugging)	{SaveBWCustom1(bytePattBy6[pattType], szPatt6,	szPatt6);}
	if(debugging)	{SaveBWCustom1(bytePattBy3[pattType], szPatt3W, szPatt3H);}
}


SpecialPoint CXAxisView::FindPattA1_C1(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, int pattTyp)
{
	SpecialPoint result;
	result.x=result.y=result.c=0.00;

	int LineLength			=	MainDisplayImageWidth/factor;
	int Height				=	MainDisplayImageHeight/factor;

	int szpatt;
	if(factor==12)	{szpatt = theapp->sizepattBy12;}
	if(factor==6)	{szpatt = theapp->sizepattBy6;}
	
	int szpattHalf			=	szpatt/2;
	int LineLengthT			=	szpatt;
	float n					=	szpatt*szpatt;

	int xpos2				=	0;
	int ypos2				=	0;

	float	sumXY			=	0;
	float	sumX			=	0;
	float	sumXX			=	0;
	float	sumY			=	0;
	float	sumYY			=	0;

	float	f0				=	0;
	float	f1				=	0;
	float	f2				=	0;
	float	corr			=	0;

	float	savedBestCorr	=	0.0;
	int		savedPosX		=	0;
	int		cnt				=	0;

	int fct2 = 1;
	if(factor==12)	{fct2 = 4;}
	if(factor==6)	{fct2 = 2;}

	bool debugging=false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgPatt,	LineLengthT,LineLengthT);}

	/////////////////

	sumX = 0.0;	sumXX = 0.0;
	
	for(int r=0; r < n; r++)
	{
		sumX	+= *(imgPatt + r);
		sumXX	+= (*(imgPatt+r))*(*(imgPatt+r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;

	bool atInitLoop			=	false;

	//////////////////

	int yTopLim = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;  ///Marked from the UI
	int yBotLim = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;  
	int yIni	= MAX(yTopLim + szpattHalf, yTopLim);  //Initial Y of the search area
	int yEnd	= MIN(Height-szpattHalf,yBotLim-szpattHalf);

/*
	int yTopLim = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int yBotLim = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
	int yIni	= MAX(szpattHalf, yTopLim);
	int yEnd	= MIN(Height-szpattHalf,yBotLim);
*/

	//for(int y=startY; y<=startY+searchHeight; y++)//22
	for(int y=yIni; y<=yEnd; y++)
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop	=	true;

		for(x=szpattHalf; x<LineLength - szpattHalf; x++)
		{
			sumXY	=	0.0; 
			
			for(ypos=-szpattHalf; ypos<szpattHalf; ypos++)
			{
				if(atInitLoop)
				{
					for(xpos=-szpattHalf; xpos<szpattHalf; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalf+LineLengthT*(ypos+szpattHalf));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumY	+=	pix;
						sumYY	+=	pix*pix;
						sumXY	+=	pixT*pix;
					}
				}
				else
				{
					pixOld		=	*(imgIn - szpattHalf - 1 + x + LineLength*(ypos+y));
					pixNew		=	*(imgIn + szpattHalf - 1 + x + LineLength*(ypos+y));
					
					for(xpos=-szpattHalf; xpos<szpattHalf; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalf+LineLengthT*(ypos+szpattHalf));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumXY	+=	pixT*pix;
					}

					sumY		+=	pixNew;
					sumY		-=	pixOld;
					sumYY		+=	pixNew*pixNew;
					sumYY		-=	pixOld*pixOld;
				}
			}

			atInitLoop = false;

			f0	=	sumXY - ((sumX*sumY)/n);
			f1	=	sumXX - ((sumX*sumX)/n);
			f2	=	sumYY - ((sumY*sumY)/n);
			
			if(sqrtf(f1*f2)!=0.0)	{corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedBestCorr)
			{
				savedBestCorr	=	corr;
				savedPosX		=	x*factor;
				result.c		=	100*savedBestCorr;
				result.y		=	y*factor;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x	=	savedPosX;

	return result;
}


SpecialPoint CXAxisView::FindPattA1_C2(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, int pattTyp)
{
	SpecialPoint result;
	result.x=result.y=result.c=0.00;

	int LineLength			=	MainDisplayImageWidth/factor;
	int Height				=	MainDisplayImageHeight/factor;

	int szpatt;
	if(factor==12)	{szpatt = theapp->sizepattBy12;}
	if(factor==6)	{szpatt = theapp->sizepattBy6;}
	
	int szpattHalf			=	szpatt/2;
	int LineLengthT			=	szpatt;
	float n					=	szpatt*szpatt;

	int xpos2				=	0;
	int ypos2				=	0;

	float	sumXY			=	0;
	float	sumX			=	0;
	float	sumXX			=	0;
	float	sumY			=	0;
	float	sumYY			=	0;

	float	f0				=	0;
	float	f1				=	0;
	float	f2				=	0;
	float	corr			=	0;

	float	savedBestCorr	=	0.0;
	int		savedPosX		=	0;
	int		cnt				=	0;

	int fct2 = 1;
	if(factor==12)	{fct2 = 4;}
	if(factor==6)	{fct2 = 2;}

	bool debugging=false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgPatt,	LineLengthT,LineLengthT);}

	/////////////////

	sumX = 0.0;	sumXX = 0.0;
	
	for(int r=0; r < n; r++)
	{
		sumX	+= *(imgPatt + r);
		sumXX	+= (*(imgPatt+r))*(*(imgPatt+r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;

	bool atInitLoop			=	false;

	//////////////////

	int yTopLim = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int yBotLim = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
	int yIni	= MAX(yTopLim + szpattHalf, yTopLim);
	int yEnd	= MIN(Height-szpattHalf,yBotLim-szpattHalf);

/*
	int yTopLim = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int yBotLim = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
	int yIni	= MAX(szpattHalf, yTopLim);
	int yEnd	= MIN(Height-szpattHalf,yBotLim);
*/

	//for(int y=startY; y<=startY+searchHeight; y++)//22
	for(int y=yIni; y<=yEnd; y++)
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop	=	true;

		for(x=szpattHalf; x<LineLength - szpattHalf; x++)
		{
			sumXY	=	0.0; 
			
			for(ypos=-szpattHalf; ypos<szpattHalf; ypos++)
			{
				if(atInitLoop)
				{
					for(xpos=-szpattHalf; xpos<szpattHalf; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalf+LineLengthT*(ypos+szpattHalf));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumY	+=	pix;
						sumYY	+=	pix*pix;
						sumXY	+=	pixT*pix;
					}
				}
				else
				{
					pixOld		=	*(imgIn - szpattHalf - 1 + x + LineLength*(ypos+y));
					pixNew		=	*(imgIn + szpattHalf - 1 + x + LineLength*(ypos+y));
					
					for(xpos=-szpattHalf; xpos<szpattHalf; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalf+LineLengthT*(ypos+szpattHalf));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumXY	+=	pixT*pix;
					}

					sumY		+=	pixNew;
					sumY		-=	pixOld;
					sumYY		+=	pixNew*pixNew;
					sumYY		-=	pixOld*pixOld;
				}
			}

			atInitLoop = false;

			f0	=	sumXY - ((sumX*sumY)/n);
			f1	=	sumXX - ((sumX*sumX)/n);
			f2	=	sumYY - ((sumY*sumY)/n);
			
			if(sqrtf(f1*f2)!=0.0)	{corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedBestCorr)
			{
				savedBestCorr	=	corr;
				savedPosX		=	x*factor;
				result.c		=	100*savedBestCorr;
				result.y		=	y*factor;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x	=	savedPosX;

	return result;
}


SpecialPoint CXAxisView::FindPattA1_C3(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, int pattTyp)
{
	SpecialPoint result;
	result.x=result.y=result.c=0.00;

	int LineLength			=	MainDisplayImageWidth/factor;
	int Height				=	MainDisplayImageHeight/factor;

	int szpatt;
	if(factor==12)	{szpatt = theapp->sizepattBy12;}
	if(factor==6)	{szpatt = theapp->sizepattBy6;}
	
	int szpattHalf			=	szpatt/2;
	int LineLengthT			=	szpatt;
	float n					=	szpatt*szpatt;

	int xpos2				=	0;
	int ypos2				=	0;

	float	sumXY			=	0;
	float	sumX			=	0;
	float	sumXX			=	0;
	float	sumY			=	0;
	float	sumYY			=	0;

	float	f0				=	0;
	float	f1				=	0;
	float	f2				=	0;
	float	corr			=	0;

	float	savedBestCorr	=	0.0;
	int		savedPosX		=	0;
	int		cnt				=	0;

	int fct2 = 1;
	if(factor==12)	{fct2 = 4;}
	if(factor==6)	{fct2 = 2;}

	bool debugging=false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgPatt,	LineLengthT,LineLengthT);}

	/////////////////

	sumX = 0.0;	sumXX = 0.0;
	
	for(int r=0; r < n; r++)
	{
		sumX	+= *(imgPatt + r);
		sumXX	+= (*(imgPatt+r))*(*(imgPatt+r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;

	bool atInitLoop			=	false;

	//////////////////

	int yTopLim = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int yBotLim = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
	int yIni	= MAX(yTopLim + szpattHalf, yTopLim);
	int yEnd	= MIN(Height-szpattHalf,yBotLim-szpattHalf);

/*
	int yTopLim = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int yBotLim = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
	int yIni	= MAX(szpattHalf, yTopLim);
	int yEnd	= MIN(Height-szpattHalf,yBotLim);
*/

	//for(int y=startY; y<=startY+searchHeight; y++)//22
	for(int y=yIni; y<=yEnd; y++)
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop	=	true;

		for(x=szpattHalf; x<LineLength - szpattHalf; x++)
		{
			sumXY	=	0.0; 
			
			for(ypos=-szpattHalf; ypos<szpattHalf; ypos++)
			{
				if(atInitLoop)
				{
					for(xpos=-szpattHalf; xpos<szpattHalf; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalf+LineLengthT*(ypos+szpattHalf));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumY	+=	pix;
						sumYY	+=	pix*pix;
						sumXY	+=	pixT*pix;
					}
				}
				else
				{
					pixOld		=	*(imgIn - szpattHalf - 1 + x + LineLength*(ypos+y));
					pixNew		=	*(imgIn + szpattHalf - 1 + x + LineLength*(ypos+y));
					
					for(xpos=-szpattHalf; xpos<szpattHalf; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalf+LineLengthT*(ypos+szpattHalf));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumXY	+=	pixT*pix;
					}

					sumY		+=	pixNew;
					sumY		-=	pixOld;
					sumYY		+=	pixNew*pixNew;
					sumYY		-=	pixOld*pixOld;
				}
			}

			atInitLoop = false;

			f0	=	sumXY - ((sumX*sumY)/n);
			f1	=	sumXX - ((sumX*sumX)/n);
			f2	=	sumYY - ((sumY*sumY)/n);
			
			if(sqrtf(f1*f2)!=0.0)	{corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedBestCorr)
			{
				savedBestCorr	=	corr;
				savedPosX		=	x*factor;
				result.c		=	100*savedBestCorr;
				result.y		=	y*factor;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x	=	savedPosX;

	return result;
}


SpecialPoint CXAxisView::FindPattA1_C4(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, int pattTyp)
{
	SpecialPoint result;
	result.x=result.y=result.c=0.00;

	int LineLength			=	MainDisplayImageWidth/factor;
	int Height				=	MainDisplayImageHeight/factor;

	int szpatt;
	if(factor==12)	{szpatt = theapp->sizepattBy12;}
	if(factor==6)	{szpatt = theapp->sizepattBy6;}
	
	int szpattHalf			=	szpatt/2;
	int LineLengthT			=	szpatt;
	float n					=	szpatt*szpatt;

	int xpos2				=	0;
	int ypos2				=	0;

	float	sumXY			=	0;
	float	sumX			=	0;
	float	sumXX			=	0;
	float	sumY			=	0;
	float	sumYY			=	0;

	float	f0				=	0;
	float	f1				=	0;
	float	f2				=	0;
	float	corr			=	0;

	float	savedBestCorr	=	0.0;
	int		savedPosX		=	0;
	int		cnt				=	0;

	int fct2 = 1;
	if(factor==12)	{fct2 = 4;}
	if(factor==6)	{fct2 = 2;}

	bool debugging=false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgPatt,	LineLengthT,LineLengthT);}

	/////////////////

	sumX = 0.0;	sumXX = 0.0;
	
	for(int r=0; r < n; r++)
	{
		sumX	+= *(imgPatt + r);
		sumXX	+= (*(imgPatt+r))*(*(imgPatt+r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;

	bool atInitLoop			=	false;

	//////////////////

	int yTopLim = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int yBotLim = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
	int yIni	= MAX(yTopLim + szpattHalf, yTopLim);
	int yEnd	= MIN(Height-szpattHalf,yBotLim-szpattHalf);

/*
	int yTopLim = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int yBotLim = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
	int yIni	= MAX(szpattHalf, yTopLim);
	int yEnd	= MIN(Height-szpattHalf,yBotLim);
*/

	//for(int y=startY; y<=startY+searchHeight; y++)//22
	for(int y=yIni; y<=yEnd; y++)
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop	=	true;

		for(x=szpattHalf; x<LineLength - szpattHalf; x++)
		{
			sumXY	=	0.0; 
			
			for(ypos=-szpattHalf; ypos<szpattHalf; ypos++)
			{
				if(atInitLoop)
				{
					for(xpos=-szpattHalf; xpos<szpattHalf; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalf+LineLengthT*(ypos+szpattHalf));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumY	+=	pix;
						sumYY	+=	pix*pix;
						sumXY	+=	pixT*pix;
					}
				}
				else
				{
					pixOld		=	*(imgIn - szpattHalf - 1 + x + LineLength*(ypos+y));
					pixNew		=	*(imgIn + szpattHalf - 1 + x + LineLength*(ypos+y));
					
					for(xpos=-szpattHalf; xpos<szpattHalf; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalf+LineLengthT*(ypos+szpattHalf));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumXY	+=	pixT*pix;
					}

					sumY		+=	pixNew;
					sumY		-=	pixOld;
					sumYY		+=	pixNew*pixNew;
					sumYY		-=	pixOld*pixOld;
				}
			}

			atInitLoop = false;

			f0	=	sumXY - ((sumX*sumY)/n);
			f1	=	sumXX - ((sumX*sumX)/n);
			f2	=	sumYY - ((sumY*sumY)/n);
			
			if(sqrtf(f1*f2)!=0.0)	{corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedBestCorr)
			{
				savedBestCorr	=	corr;
				savedPosX		=	x*factor;
				result.c		=	100*savedBestCorr;
				result.y		=	y*factor;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x	=	savedPosX;

	return result;
}


SpecialPoint CXAxisView::FindPattHigherRes_C1(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;
	result.x=result.y=result.c=0.00;

	int LineLength			=	MainDisplayImageWidth/factor;
	int Height				=	MainDisplayImageHeight/factor;
	int szpattW				=	theapp->sizepattBy3_2W;
	int szpattH				=	theapp->sizepattBy3_2H;
	int szpattHalfW			=	szpattW/2;
	int szpattHalfH			=	szpattH/2;
	int LineLengthT			=	szpattW;
	float n					=	szpattW*szpattH;

	int xpos2				=	0;
	int ypos2				=	0;

	float	sumXY			=	0;
	float	sumX			=	0;
	float	sumXX			=	0;
	float	sumY			=	0;
	float	sumYY			=	0;

	float	f0				=	0;
	float	f1				=	0;
	float	f2				=	0;
	float	corr			=	0;

	float	savedBestCorr	=	0.0;
	int		savedPosX		=	0;
	int		cnt				=	0;
	int		startY			=	refPt.y/factor;
	int		startX			=	refPt.x/factor;
	int		searchHeight	=	26/2;	

	bool debugging=false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgPatt,	LineLengthT,LineLengthT);}

	/////////////////

	sumX = 0.0;	sumXX = 0.0;
	
	for(int r=0; r < n; r++)
	{
		sumX	+= *(imgPatt + r);
		sumXX	+= (*(imgPatt+r))*(*(imgPatt+r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;

	bool atInitLoop			=	false;

	//////////////////

	if(startY<=szpattHalfH)	{startY=szpattHalfH;}
	if(startX<=szpattHalfW)	{startX=szpattHalfW;}

	for(int y=startY; y<startY+searchHeight+szpattHalfH; y++)//22
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop	=	true;

		for(x=startX; x<startX + szpattHalfW; x++)
		{
			sumXY	=	0.0; 
			
			for(ypos=-szpattHalfH; ypos<szpattHalfH; ypos++)
			{
				if(atInitLoop)
				{
					for(xpos=-szpattHalfW; xpos<szpattHalfW; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalfW+LineLengthT*(ypos+szpattHalfH));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumY	+=	pix;
						sumYY	+=	pix*pix;
						sumXY	+=	pixT*pix;
					}
				}
				else
				{
					pixOld		=	*(imgIn - szpattHalfW - 1 + x + LineLength*(ypos+y));
					pixNew		=	*(imgIn + szpattHalfW - 1 + x + LineLength*(ypos+y));
					
					for(xpos=-szpattHalfW; xpos<szpattHalfW; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalfW+LineLengthT*(ypos+szpattHalfH));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumXY	+=	pixT*pix;
					}

					sumY		+=	pixNew;
					sumY		-=	pixOld;
					sumYY		+=	pixNew*pixNew;
					sumYY		-=	pixOld*pixOld;
				}
			}

			atInitLoop = false;

			f0	=	sumXY - ((sumX*sumY)/n);
			f1	=	sumXX - ((sumX*sumX)/n);
			f2	=	sumYY - ((sumY*sumY)/n);
			
			if(sqrtf(f1*f2)!=0.0)	{corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedBestCorr)
			{
				savedBestCorr	=	corr;
				savedPosX		=	x*factor;
				result.c		=	int(savedBestCorr*100);
				result.y		=	y*factor;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x	=	savedPosX;

	return result;
}

SpecialPoint CXAxisView::FindPattHigherRes_C2(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;
	result.x=result.y=result.c=0.00;

	int LineLength			=	MainDisplayImageWidth/factor;
	int Height				=	MainDisplayImageHeight/factor;
	int szpattW				=	theapp->sizepattBy3_2W;
	int szpattH				=	theapp->sizepattBy3_2H;
	int szpattHalfW			=	szpattW/2;
	int szpattHalfH			=	szpattH/2;
	int LineLengthT			=	szpattW;
	float n					=	szpattW*szpattH;

	int xpos2				=	0;
	int ypos2				=	0;

	float	sumXY			=	0;
	float	sumX			=	0;
	float	sumXX			=	0;
	float	sumY			=	0;
	float	sumYY			=	0;

	float	f0				=	0;
	float	f1				=	0;
	float	f2				=	0;
	float	corr			=	0;

	float	savedBestCorr	=	0.0;
	int		savedPosX		=	0;
	int		cnt				=	0;
	int		startY			=	refPt.y/factor;
	int		startX			=	refPt.x/factor;
	int		searchHeight	=	26/2;	

	bool debugging=false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgPatt,	LineLengthT,LineLengthT);}

	/////////////////

	sumX = 0.0;	sumXX = 0.0;
	
	for(int r=0; r < n; r++)
	{
		sumX	+= *(imgPatt + r);
		sumXX	+= (*(imgPatt+r))*(*(imgPatt+r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;

	bool atInitLoop			=	false;

	//////////////////

	if(startY<=szpattHalfH)	{startY=szpattHalfH;}
	if(startX<=szpattHalfW)	{startX=szpattHalfW;}

	for(int y=startY; y<startY+searchHeight+szpattHalfH; y++)//22
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop	=	true;

		for(x=startX; x<startX + szpattHalfW; x++)
		{
			sumXY	=	0.0; 
			
			for(ypos=-szpattHalfH; ypos<szpattHalfH; ypos++)
			{
				if(atInitLoop)
				{
					for(xpos=-szpattHalfW; xpos<szpattHalfW; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalfW+LineLengthT*(ypos+szpattHalfH));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumY	+=	pix;
						sumYY	+=	pix*pix;
						sumXY	+=	pixT*pix;
					}
				}
				else
				{
					pixOld		=	*(imgIn - szpattHalfW - 1 + x + LineLength*(ypos+y));
					pixNew		=	*(imgIn + szpattHalfW - 1 + x + LineLength*(ypos+y));
					
					for(xpos=-szpattHalfW; xpos<szpattHalfW; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalfW+LineLengthT*(ypos+szpattHalfH));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumXY	+=	pixT*pix;
					}

					sumY		+=	pixNew;
					sumY		-=	pixOld;
					sumYY		+=	pixNew*pixNew;
					sumYY		-=	pixOld*pixOld;
				}
			}

			atInitLoop = false;

			f0	=	sumXY - ((sumX*sumY)/n);
			f1	=	sumXX - ((sumX*sumX)/n);
			f2	=	sumYY - ((sumY*sumY)/n);
			
			if(sqrtf(f1*f2)!=0.0)	{corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedBestCorr)
			{
				savedBestCorr	=	corr;
				savedPosX		=	x*factor;
				result.c		=	int(savedBestCorr*100);
				result.y		=	y*factor;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x	=	savedPosX;

	return result;
}

SpecialPoint CXAxisView::FindPattHigherRes_C3(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;
	result.x=result.y=result.c=0.00;

	int LineLength			=	MainDisplayImageWidth/factor;
	int Height				=	MainDisplayImageHeight/factor;
	int szpattW				=	theapp->sizepattBy3_2W;
	int szpattH				=	theapp->sizepattBy3_2H;
	int szpattHalfW			=	szpattW/2;
	int szpattHalfH			=	szpattH/2;
	int LineLengthT			=	szpattW;
	float n					=	szpattW*szpattH;

	int xpos2				=	0;
	int ypos2				=	0;

	float	sumXY			=	0;
	float	sumX			=	0;
	float	sumXX			=	0;
	float	sumY			=	0;
	float	sumYY			=	0;

	float	f0				=	0;
	float	f1				=	0;
	float	f2				=	0;
	float	corr			=	0;

	float	savedBestCorr	=	0.0;
	int		savedPosX		=	0;
	int		cnt				=	0;
	int		startY			=	refPt.y/factor;
	int		startX			=	refPt.x/factor;
	int		searchHeight	=	26/2;	

	bool debugging=false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgPatt,	LineLengthT,LineLengthT);}

	/////////////////

	sumX = 0.0;	sumXX = 0.0;
	
	for(int r=0; r < n; r++)
	{
		sumX	+= *(imgPatt + r);
		sumXX	+= (*(imgPatt+r))*(*(imgPatt+r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;

	bool atInitLoop			=	false;

	//////////////////

	if(startY<=szpattHalfH)	{startY=szpattHalfH;}
	if(startX<=szpattHalfW)	{startX=szpattHalfW;}

	for(int y=startY; y<startY+searchHeight+szpattHalfH; y++)//22
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop	=	true;

		for(x=startX; x<startX + szpattHalfW; x++)
		{
			sumXY	=	0.0; 
			
			for(ypos=-szpattHalfH; ypos<szpattHalfH; ypos++)
			{
				if(atInitLoop)
				{
					for(xpos=-szpattHalfW; xpos<szpattHalfW; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalfW+LineLengthT*(ypos+szpattHalfH));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumY	+=	pix;
						sumYY	+=	pix*pix;
						sumXY	+=	pixT*pix;
					}
				}
				else
				{
					pixOld		=	*(imgIn - szpattHalfW - 1 + x + LineLength*(ypos+y));
					pixNew		=	*(imgIn + szpattHalfW - 1 + x + LineLength*(ypos+y));
					
					for(xpos=-szpattHalfW; xpos<szpattHalfW; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalfW+LineLengthT*(ypos+szpattHalfH));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumXY	+=	pixT*pix;
					}

					sumY		+=	pixNew;
					sumY		-=	pixOld;
					sumYY		+=	pixNew*pixNew;
					sumYY		-=	pixOld*pixOld;
				}
			}

			atInitLoop = false;

			f0	=	sumXY - ((sumX*sumY)/n);
			f1	=	sumXX - ((sumX*sumX)/n);
			f2	=	sumYY - ((sumY*sumY)/n);
			
			if(sqrtf(f1*f2)!=0.0)	{corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedBestCorr)
			{
				savedBestCorr	=	corr;
				savedPosX		=	x*factor;
				result.c		=	int(savedBestCorr*100);
				result.y		=	y*factor;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x	=	savedPosX;

	return result;
}


SpecialPoint CXAxisView::FindPattHigherRes_C4(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;
	result.x=result.y=result.c=0.00;

	int LineLength			=	MainDisplayImageWidth/factor;
	int Height				=	MainDisplayImageHeight/factor;
	int szpattW				=	theapp->sizepattBy3_2W;
	int szpattH				=	theapp->sizepattBy3_2H;
	int szpattHalfW			=	szpattW/2;
	int szpattHalfH			=	szpattH/2;
	int LineLengthT			=	szpattW;
	float n					=	szpattW*szpattH;

	int xpos2				=	0;
	int ypos2				=	0;

	float	sumXY			=	0;
	float	sumX			=	0;
	float	sumXX			=	0;
	float	sumY			=	0;
	float	sumYY			=	0;

	float	f0				=	0;
	float	f1				=	0;
	float	f2				=	0;
	float	corr			=	0;

	float	savedBestCorr	=	0.0;
	int		savedPosX		=	0;
	int		cnt				=	0;
	int		startY			=	refPt.y/factor;
	int		startX			=	refPt.x/factor;
	int		searchHeight	=	26/2;	

	bool debugging=false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgPatt,	LineLengthT,LineLengthT);}

	/////////////////

	sumX = 0.0;	sumXX = 0.0;
	
	for(int r=0; r < n; r++)
	{
		sumX	+= *(imgPatt + r);
		sumXX	+= (*(imgPatt+r))*(*(imgPatt+r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;

	bool atInitLoop			=	false;

	//////////////////

	if(startY<=szpattHalfH)	{startY=szpattHalfH;}
	if(startX<=szpattHalfW)	{startX=szpattHalfW;}

	for(int y=startY; y<startY+searchHeight+szpattHalfH; y++)//22
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop	=	true;

		for(x=startX; x<startX + szpattHalfW; x++)
		{
			sumXY	=	0.0; 
			
			for(ypos=-szpattHalfH; ypos<szpattHalfH; ypos++)
			{
				if(atInitLoop)
				{
					for(xpos=-szpattHalfW; xpos<szpattHalfW; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalfW+LineLengthT*(ypos+szpattHalfH));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumY	+=	pix;
						sumYY	+=	pix*pix;
						sumXY	+=	pixT*pix;
					}
				}
				else
				{
					pixOld		=	*(imgIn - szpattHalfW - 1 + x + LineLength*(ypos+y));
					pixNew		=	*(imgIn + szpattHalfW - 1 + x + LineLength*(ypos+y));
					
					for(xpos=-szpattHalfW; xpos<szpattHalfW; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalfW+LineLengthT*(ypos+szpattHalfH));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumXY	+=	pixT*pix;
					}

					sumY		+=	pixNew;
					sumY		-=	pixOld;
					sumYY		+=	pixNew*pixNew;
					sumYY		-=	pixOld*pixOld;
				}
			}

			atInitLoop = false;

			f0	=	sumXY - ((sumX*sumY)/n);
			f1	=	sumXX - ((sumX*sumX)/n);
			f2	=	sumYY - ((sumY*sumY)/n);
			
			if(sqrtf(f1*f2)!=0.0)	{corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedBestCorr)
			{
				savedBestCorr	=	corr;
				savedPosX		=	x*factor;
				result.c		=	int(savedBestCorr*100);
				result.y		=	y*factor;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x	=	savedPosX;

	return result;
}

void CXAxisView::LearnPattern2X(BYTE *im_srcIn, SinglePoint pos)
{
	int xpos=0;		int ypos=0;
	int xpos2=0;	int ypos2=0;
	int xpos3=0;
		
	int LineLength=CroppedCameraHeight/3;

	int szPattW		=	theapp->sizepattBy3_2W;
	int szPattH		=	theapp->sizepattBy3_2H;
	int HalfSzPattW	=	szPattW/2;
	int HalfSzPattH	=	szPattH/2;
	int LineLength3	=	szPattW;

	for(ypos=(pos.y)-HalfSzPattH; ypos<pos.y+HalfSzPattH; ypos++)
	{
		for(xpos=(pos.x)-HalfSzPattW;xpos<pos.x+HalfSzPattW;xpos++)
		{	
			*(im_mat2 + xpos3 + LineLength3*ypos2)= *(im_srcIn + xpos + LineLength*ypos);
			xpos3+=1;
		}

		xpos3=0;
		ypos2+=1;
	}

	bool debugging = false;

	if(debugging)	{SaveBWCustom1(im_mat2, szPattW, szPattH);}

	SavePattern2X(im_mat2);
	LearnDone=true;
}


void CXAxisView::SavePattern2X(BYTE *im_sr)
{
	char buf[10];
	//char buf2[200];
	char currentpath[60];
	//char currentpath2[60];
	char* pFileName = "c:\\silgandata\\pat\\";//drive
	CString c;
	
	//copied from LearnPattern2X (the method that calls this one)
	int szPattW		=	theapp->sizepattBy3_2W;
	int szPattH		=	theapp->sizepattBy3_2H;

	c =itoa(pframe->CurrentJobNum,buf,10);
	
	strcpy(currentpath,pFileName);
	strcat(currentpath,c);

	//strcpy(currentpath2,pFileName);
	//strcat(currentpath2,c);
	//strcat(currentpath2,"\\image2X");

	//strcat(currentpath2,".txt");

	///comment out text file save
	//ImageFile.Open(currentpath2, CFile::modeWrite | CFile::modeCreate);
	
	//CString szDigit;
	//int digit;

	//int szPattW		=	theapp->sizepattBy3_2W;
	//int szPattH		=	theapp->sizepattBy3_2H;
	//int nsize		=	szPattW*szPattH;

	//for(int g=0; g<nsize; g++)
	//{
	//	digit=*(im_sr+g);
	//	szDigit.Format("%3i",digit);
		//szDigit=itoa(digit,bufd,10);
		//ImageFile.Write(szDigit,3);
		//ImageFile.Write(",",1);
	//	ImageFile.Write(szDigit,szDigit.GetLength());
	//}
	
	//ImageFile.Close();
     
	//ImageFile.Open(currentpath2, CFile::modeRead | CFile::modeCreate);
	//UINT lBytesRead = ImageFile.Read (szSampleText,100); 
	//ImageFile.Read(m_r1,m_r1.GetLength());
	//ImageFile.Close();

	strcat(currentpath,"\\image2XBMP");
	strcat(currentpath,".bmp");

	//sprintf(buf2,currentpath2,im_sr);
	//writeOutGrayArrayFile(currentpath2,
					 //12,
					 //12,
					 //im_sr);

	CVisByteImage image(szPattW,szPattH,1,-1,im_sr);
	imagepat=image;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imagepat.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{AfxMessageBox("The pat file could not be saved.");}// Warn the user.
}

void CXAxisView::OnLoad2X(int Job)
{
	CVisByteImage image;
	
	char buf[10];
	char currentpath[60];
	char currentpath2[60];
	char* pFileName = "c:\\silgandata\\pat\\";//drive
	CString c;
	
	c=itoa(Job,buf,10);
		
	strcpy(currentpath,pFileName);
	strcat(currentpath,c);
	strcat(currentpath,"\\");
	//strcat(currentpath,c); // wrong filename (does not match SavePattern2X)
	strcat(currentpath,"image2XBMP"); //from SavePattern2X
	strcat(currentpath,".bmp");

	strcpy(currentpath2,pFileName);
	strcat(currentpath2,c);
	strcat(currentpath2,"\\image2X");
	strcat(currentpath2,".txt");

	//try loading the bmp first then the text file as a backup
	int pxWidth;  //pixel width
	int pxHeight; //pixel height
	BYTE pxValue; //pixel value
	try
	{
		image.ReadFile(currentpath);
		pxWidth = image.Width();
		pxHeight = image.Height();
		
		//treat bitmap image as single channel

		for(int col=0; col<pxWidth; col++)
		{
			for(int  row=0; row<pxHeight; row++)
			{
				pxValue = image.Pixel(col,row); //assuming only one band since image needs to be grayscale

				*(im_mat2+col+row*pxWidth) = pxValue;
			}
		}
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		
		int s=0;
		char bufDig[2];
		CString N1;

		int szPattW = theapp->sizepattBy3_2W;
		int szPattH = theapp->sizepattBy3_2H;
		int nsize	= szPattW*szPattH;

		CFileException ce;
		bool success=ImageFile.Open(currentpath2, CFile::modeRead, &ce) > 0;
		
		if(success!=0)
		{
			int digit;
			
			for(int g=0; g<nsize; g++)
			{
				ImageFile.Read(bufDig,3);
				
				N1				=	bufDig[0];
				N1				+=	bufDig[1];
				N1				+=	bufDig[2];

				//ImageFile.Seek(1, CFile::current );
				
				digit=atoi(N1);
				
				//szDigit.Format("%3i",digit);
				
				N1				=	"";
				*(im_mat2+g)	=	digit;	
				s=s+4;
			}

			ImageFile.Close();
			pframe->JobNoGood=false;
		}
		else
		{AfxMessageBox("Image file 2 could not be opened. You will need to setup a new job");}
	}
}


SpecialPoint CXAxisView::FindAPattern(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor)
{
	SpecialPoint result;
	result.x=result.y=result.c=0.00;

	int LineLength			=	theapp->camHeight[lookinCam]/factor;
	int Height				=	theapp->camWidth[lookinCam]/factor;

	int szpattW				=	theapp->sizepattBy3_2W;
	int szpattH				=	theapp->sizepattBy3_2H;
	int szpattHalfW			=	szpattW/2;
	int szpattHalfH			=	szpattH/2;
	
	int LineLengthT			=	szpattW;
	float n					=	szpattW*szpattH;

	int xpos2				=	0;
	int ypos2				=	0;

	float	sumXY			=	0;
	float	sumX			=	0;
	float	sumXX			=	0;
	float	sumY			=	0;
	float	sumYY			=	0;

	float	f0				=	0;
	float	f1				=	0;
	float	f2				=	0;
	float	corr			=	0;

	float	savedBestCorr	=	0.0;
	int		savedPosX		=	0;
	int		cnt				=	0;

	int fct2 = 1;

	if(factor==12)	{fct2 = 4;}
	if(factor==6)	{fct2 = 2;}
	if(factor==3)	{fct2 = 1;}

	int		startY			=	theapp->jobinfo[pframe->CurrentJobNum].orienArea/fct2;
	int		searchHeight	=	32/fct2;

	bool debugging=false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgPatt,	LineLengthT,LineLengthT);}

	/////////////////

	sumX = 0.0;	sumXX = 0.0;
	
	for(int r=0; r < n; r++)
	{
		sumX	+= *(imgPatt + r);
		sumXX	+= (*(imgPatt+r))*(*(imgPatt+r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;

	bool atInitLoop			=	false;

	//////////////////

	if(startY<=szpattHalfH)	{startY=szpattHalfH;}

	for(int y=startY; y<=startY+searchHeight; y++)//22
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop	=	true;

		for(x=szpattHalfW; x<LineLength - szpattHalfW; x++)
		{
			sumXY	=	0.0; 
			
			for(ypos=-szpattHalfH; ypos<szpattHalfH; ypos++)
			{
				if(atInitLoop)
				{
					for(xpos=-szpattHalfW; xpos<szpattHalfW; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalfW+LineLengthT*(ypos+szpattHalfH));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumY	+=	pix;
						sumYY	+=	pix*pix;
						sumXY	+=	pixT*pix;
					}
				}
				else
				{
					pixOld		=	*(imgIn - szpattHalfW - 1 + x + LineLength*(ypos+y));
					pixNew		=	*(imgIn + szpattHalfW - 1 + x + LineLength*(ypos+y));
					
					for(xpos=-szpattHalfW; xpos<szpattHalfW; xpos++)
					{
						pixT	=	*(imgPatt+xpos+szpattHalfW+LineLengthT*(ypos+szpattHalfH));
						pix		=	*(imgIn+ xpos + x + LineLength*(ypos+y));
						
						sumXY	+=	pixT*pix;
					}

					sumY		+=	pixNew;
					sumY		-=	pixOld;
					sumYY		+=	pixNew*pixNew;
					sumYY		-=	pixOld*pixOld;
				}
			}

			atInitLoop = false;

			f0	=	sumXY - ((sumX*sumY)/n);
			f1	=	sumXX - ((sumX*sumX)/n);
			f2	=	sumYY - ((sumY*sumY)/n);
			
			if(sqrtf(f1*f2)!=0.0)	{corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedBestCorr)
			{
				savedBestCorr	=	corr;
				savedPosX		=	x*factor;
				result.c		=	100*savedBestCorr;
				result.y		=	y*factor;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x	=	savedPosX;

	return result;
}




int CXAxisView::Stitch4ImgX(int wBox, int hBox, 
		BYTE *im_Ligh1, BYTE *im_Dark1, BYTE *im_Edge1, int xBox1, int yBox1, 
		BYTE *im_Ligh2, BYTE *im_Dark2, BYTE *im_Edge2, int xBox2, int yBox2, 
		BYTE *im_Ligh3, BYTE *im_Dark3, BYTE *im_Edge3, int xBox3, int yBox3, 
		BYTE *im_Ligh4, BYTE *im_Dark4, BYTE *im_Edge4, int xBox4, int yBox4, 
		int camPatt, TwoPoint pattPos,  
		bool takecam1, bool takecam2, bool takecam3, bool takecam4, int fact)
{
	int wBx = wBox/fact;	int hBx = hBox/fact;

	int xBx1= xBox1/fact;	int yBx1= yBox1/fact;
	int xBx2= xBox2/fact;	int yBx2= yBox2/fact;
	int xBx3= xBox3/fact;	int yBx3= yBox3/fact;
	int xBx4= xBox4/fact;	int yBx4= yBox4/fact; 

	int xbx1= 0;	int ybx1= 0;
	int xbx2= 0;	int ybx2= 0;
	int xbx3= 0;	int ybx3= 0;
	int xbx4= 0;	int ybx4= 0; 

	int StitchedLength = 4*wBx;
	int StitchedHeight = hBx;

	int cam;
	int camLength = MainDisplayImageWidth/fact;
	int camHeight = MainDisplayImageHeight/fact;

	int offX;// pixS, ;
	int y;

	bool debugging=false;

	//////////////////

	int thLight = theapp->jobinfo[pframe->CurrentJobNum].stLighSen;
	int thDark = theapp->jobinfo[pframe->CurrentJobNum].stDarkSen;
	int thEdges = theapp->jobinfo[pframe->CurrentJobNum].stEdgeSen;

	//Always show pattern at the SECOND position
	if(camPatt==0) {camPatt=1;}
	camPatt=2;
	
	if(camPatt == 1)	
	{
		im1LighSeq = im_Ligh4; im2LighSeq = im_Ligh1; im3LighSeq = im_Ligh2; im4LighSeq = im_Ligh3;
		im1DarkSeq = im_Dark4; im2DarkSeq = im_Dark1; im3DarkSeq = im_Dark2; im4DarkSeq = im_Dark3;
		im1EdgeSeq = im_Edge4; im2EdgeSeq = im_Edge1; im3EdgeSeq = im_Edge2; im4EdgeSeq = im_Edge3;

		xbx1 = xBx4;	ybx1 = yBx4;
		xbx2 = xBx1;	ybx2 = yBx1;
		xbx3 = xBx2;	ybx3 = yBx2;
		xbx4 = xBx3;	ybx4 = yBx3;
	}

	if(camPatt == 2)	
	{
		im1LighSeq = im_Ligh1; im2LighSeq = im_Ligh2; im3LighSeq = im_Ligh3; im4LighSeq = im_Ligh4;
		im1DarkSeq = im_Dark1; im2DarkSeq = im_Dark2; im3DarkSeq = im_Dark3; im4DarkSeq = im_Dark4;
		im1EdgeSeq = im_Edge1; im2EdgeSeq = im_Edge2; im3EdgeSeq = im_Edge3; im4EdgeSeq = im_Edge4;

		xbx1 = xBx1;	ybx1 = yBx1;
		xbx2 = xBx2;	ybx2 = yBx2;
		xbx3 = xBx3;	ybx3 = yBx3;
		xbx4 = xBx4;	ybx4 = yBx4;
	}

	if(camPatt == 3)	
	{
		im1LighSeq = im_Ligh2; im2LighSeq = im_Ligh3; im3LighSeq = im_Ligh4; im4LighSeq = im_Ligh1;
		im1DarkSeq = im_Dark2; im2DarkSeq = im_Dark3; im3DarkSeq = im_Dark4; im4DarkSeq = im_Dark1;
		im1EdgeSeq = im_Edge2; im2EdgeSeq = im_Edge3; im3EdgeSeq = im_Edge4; im4EdgeSeq = im_Edge1;

		xbx1 = xBx2;	ybx1 = yBx2;
		xbx2 = xBx3;	ybx2 = yBx3;
		xbx3 = xBx4;	ybx3 = yBx4;
		xbx4 = xBx1;	ybx4 = yBx1;
	}

	if(camPatt == 4)	
	{
		im1LighSeq = im_Ligh3; im2LighSeq = im_Ligh4; im3LighSeq = im_Ligh1; im4LighSeq = im_Ligh2;
		im1DarkSeq = im_Dark3; im2DarkSeq = im_Dark4; im3DarkSeq = im_Dark1; im4DarkSeq = im_Dark2;
		im1EdgeSeq = im_Edge3; im2EdgeSeq = im_Edge4; im3EdgeSeq = im_Edge1; im4EdgeSeq = im_Edge2;

		xbx1 = xBx3;	ybx1 = yBx3;
		xbx2 = xBx4;	ybx2 = yBx4;
		xbx3 = xBx1;	ybx3 = yBx1;
		xbx4 = xBx2;	ybx4 = yBx2;
	}


	for(y = 0; y<StitchedHeight; y++)
	{
		for(int x = 0; x<StitchedLength; x++)
		{
			*(imgStitchedDark + x + StitchedLength*y) = 255;
			*(imgStitchedLigh + x + StitchedLength*y) = 255;
			*(imgStitchedEdg + x + StitchedLength*y) = 255;

			*(imgStitchedFilDark + x + StitchedLength*y) = 255;
			*(imgStitchedFilLigh + x + StitchedLength*y) = 255;
			*(imgStitchedFilEdg + x + StitchedLength*y) = 255;

			*(imgStitchedFilFinal + x + StitchedLength*y) = 0;
			
			/////////////

			*(imgStitchedBW + x + StitchedLength*y) = 255;
		}
	}


	bool stitOutOfRange = false;

	if(	xBox1<0 || yBox1<0 || xBox2<0 || yBox2<0 || 
		xBox3<0 || yBox3<0 || xBox4<0 || yBox4<0)
	{stitOutOfRange = true; edgeProp =10000;}


	if(stitOutOfRange)	{return 0;}

	//Why is this doing this???
//	if(camPatt==0)	{return 0;}

	int pattPosX = pattPos.x;

	///////////
	///////////

	cam		= 1;
	offX	= (cam-1)*wBx;
	
	for(y = ybx1; y<(ybx1+hBx); y++)
	{
		for(int x = xbx1; x<(xbx1+wBx); x++)
		{
			*(imgStitchedLigh+ (x - xbx1) + offX + StitchedLength*(y-ybx1))= *(im1LighSeq + x + camLength*y);
			*(imgStitchedDark+ (x - xbx1) + offX + StitchedLength*(y-ybx1))= *(im1DarkSeq + x + camLength*y);
			*(imgStitchedEdg + (x - xbx1) + offX + StitchedLength*(y-ybx1))= *(im1EdgeSeq + x + camLength*y);
		}
	}

	///////////

	cam		= 2;
	offX	= (cam-1)*wBx;
	
	for(y = ybx2; y<(ybx2+hBx); y++)
	{
		for(int x = xbx2; x<(xbx2+wBx); x++)
		{
			*(imgStitchedLigh+ (x - xbx2) + offX + StitchedLength*(y-ybx2))= *(im2LighSeq + x + camLength*y);
			*(imgStitchedDark+ (x - xbx2) + offX + StitchedLength*(y-ybx2))= *(im2DarkSeq + x + camLength*y);
			*(imgStitchedEdg + (x - xbx2) + offX + StitchedLength*(y-ybx2))= *(im2EdgeSeq + x + camLength*y);
		}
	}

	///////////

	cam		= 3;
	offX	= (cam-1)*wBx;
	
	for(y = ybx3; y<(ybx3+hBx); y++)
	{
		for(int x = xbx3; x<(xbx3+wBx); x++)
		{
			*(imgStitchedLigh+ (x - xbx3) + offX + StitchedLength*(y-ybx3)) = *(im3LighSeq + x + camLength*y);
			*(imgStitchedDark+ (x - xbx3) + offX + StitchedLength*(y-ybx3)) = *(im3DarkSeq + x + camLength*y);
			*(imgStitchedEdg + (x - xbx3) + offX + StitchedLength*(y-ybx3)) = *(im3EdgeSeq + x + camLength*y);
		}
	}

	///////////

	cam		= 4;
	offX	= (cam-1)*wBx;
	
	for(y = ybx4; y<(ybx4+hBx); y++)
	{
		for(int x = xbx4; x<(xbx4+wBx); x++)
		{
			*(imgStitchedLigh+ (x - xbx4) + offX + StitchedLength*(y-ybx4)) = *(im4LighSeq + x + camLength*y);
			*(imgStitchedDark+ (x - xbx4) + offX + StitchedLength*(y-ybx4)) = *(im4DarkSeq + x + camLength*y);
			*(imgStitchedEdg + (x - xbx4) + offX + StitchedLength*(y-ybx4)) = *(im4EdgeSeq + x + camLength*y);			
		}
	}

	///////////

	if(debugging)	
	{SaveBWCustom1(imgStitchedBW,	StitchedLength, StitchedHeight);}


	//		*(imgStitchedFilDark + x + StitchedLength*y) = 255;
	//		*(imgStitchedFilLigh + x + StitchedLength*y) = 255;

	/////////////////////////

	int pix, pixVal;

	for(y=1; y<StitchedHeight-1; y++)
	{
		for(int x=1; x<StitchedLength-1; x++ )
		{
			pix	= *(imgStitchedLigh + x + StitchedLength*y);

			if(pix>thLight)	{pixVal = 255;} 
			else			{pixVal = 0;}
			
			*(imgStitchedFilLigh + x  + StitchedLength * y) = pixVal;

			//////////////////////

			pix	= *(imgStitchedDark + x + StitchedLength*y);

			if(pix<thDark)	{pixVal = 255;} 
			else			{pixVal = 0;}
			
			*(imgStitchedFilDark + x  + StitchedLength * y) = pixVal;
		}
	}
	

	int pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	

	// Creating the Sobel image
	for(y=1; y<StitchedHeight-1; y++)
	{
		for(int x=1; x<StitchedLength-1; x++ )
		{
			pix1	= *(imgStitchedEdg + x - 1  + StitchedLength*(y - 1));
			pix2	= *(imgStitchedEdg + x + 0  + StitchedLength*(y - 1));
			pix3	= *(imgStitchedEdg + x + 1  + StitchedLength*(y - 1));

			pix4	= *(imgStitchedEdg + x - 1  + StitchedLength*(y + 0));
			pix5	= *(imgStitchedEdg + x + 0  + StitchedLength*(y + 0));
			pix6	= *(imgStitchedEdg + x + 1  + StitchedLength*(y + 0));

			pix7	= *(imgStitchedEdg + x - 1  + StitchedLength*(y + 1));
			pix8	= *(imgStitchedEdg + x + 0  + StitchedLength*(y + 1));
			pix9	= *(imgStitchedEdg + x + 1  + StitchedLength*(y + 1));

			//pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
			//		abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pix =	abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MIN(pix,255);

			if(pixVal>thEdges)
			{*(imgStitchedFilEdg + x  + StitchedLength * y) = 255;}
			else
			{*(imgStitchedFilEdg + x  + StitchedLength * y) = 0;}
		}
	}


	bool useStLigh = theapp->jobinfo[pframe->CurrentJobNum].useStLigh;
	bool useStDark = theapp->jobinfo[pframe->CurrentJobNum].useStDark;
	bool useStEdge = theapp->jobinfo[pframe->CurrentJobNum].useStEdge;

	if(useStLigh)
	{
		for(y=1; y<StitchedHeight-1; y++)
		{
			for(int x=1; x<StitchedLength-1; x++ )
			{
				pix = *(imgStitchedFilLigh + x + StitchedLength*y);

				if(pix==255) {*(imgStitchedFilFinal + x + StitchedLength*y) = 255;}
			}
		}
	}

	if(useStDark)
	{
		for(y=1; y<StitchedHeight-1; y++)
		{
			for(int x=1; x<StitchedLength-1; x++ )
			{
				pix = *(imgStitchedFilDark + x + StitchedLength*y);

				if(pix==255) {*(imgStitchedFilFinal + x + StitchedLength*y) = 255;}
			}
		}
	}

	if(useStEdge)
	{
		for(y=1; y<StitchedHeight-1; y++)
		{
			for(int x=1; x<StitchedLength-1; x++ )
			{
				pix = *(imgStitchedFilEdg + x + StitchedLength*y);

				if(pix==255) {*(imgStitchedFilFinal + x + StitchedLength*y) = 255;}
			}
		}
	}

	int ini1 =	xAvoidIni[1]/fact; int end1 = xAvoidEnd[1]/fact;
	int ini2 =	xAvoidIni[2]/fact; int end2 = xAvoidEnd[2]/fact;
	int ini3 =	xAvoidIni[3]/fact; int end3 = xAvoidEnd[3]/fact;
	int ini4 =	xAvoidIni[4]/fact; int end4 = xAvoidEnd[4]/fact;

	int countPix=0;

	for(int x=1; x<StitchedLength-1; x++ )
	{
		if(	(x>ini1 && x<end1) || (x>ini2 && x<end2) ||
			(x>ini3 && x<end3) || (x>ini4 && x<end4) )
		{continue;}
		else
		{
			for(y=1; y<StitchedHeight-1; y++)
			{
				pix = *(imgStitchedFilFinal + x + StitchedLength*y);

				if(pix==0)	{countPix++;}
			}
		}
	}

	/*
	int nEdge = 0;	int nTotal= 0;
	
	for(y=seamRecScaled.y; y<seamRecScaled.y+seamRecScaled.h; y++)
	{
		for(int x=seamRecScaled.x; x<seamRecScaled.x+seamRecScaled.w; x++ )
		{
			pix1	= *(imgStitchedFilEdg + x + StitchedLength*y);

			if(pix1>th)
			{nEdge++;}
			
			nTotal++;
		}
	}

	if(nTotal>0)
	{edgeProp = 1000*float(nEdge)/float(nTotal);}*/

	return countPix;
}

void CXAxisView::calcTearCoordinates()
{
	stitchW	= theapp->jobinfo[pframe->CurrentJobNum].stitchW;
	stitchH	= theapp->jobinfo[pframe->CurrentJobNum].stitchH;

	int offsetX = abs(theapp->jobinfo[pframe->CurrentJobNum].avoidOffX);
	if(offsetX>stitchW/4)	{offsetX=stitchW/4;}
	int i;

	for(i=1; i<=4; i++)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].findMiddles)
		{	stitchX[i]		= xMidd[i] - stitchW/2;}
		else
		{
			int LengthOrig	= MainDisplayImageWidth;
			stitchX[i]		= (LengthOrig - stitchW)/2;
		}
		
		stitchY[i]		= theapp->jobinfo[pframe->CurrentJobNum].stitchY[i];

		/////////////////////
		//Finding the avoided areas in each camera
	}

	if(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle==10)
	{
		for(i=1; i<=4; i++)
		{
			xAvoidIni[i] = (i-1)*stitchW + ((middlShiftWPoints[i] - offsetX) - stitchX[i]);
			xAvoidEnd[i] = xAvoidIni[i] + 2*offsetX;
		}
	}
	else
	{
		for(i=1; i<=4; i++)
		{
			xAvoidIni[i] = (i-1)*stitchW + ((xMidd[i] - offsetX) - stitchX[i]);
			xAvoidEnd[i] = xAvoidIni[i] + 2*offsetX;
		}
	}

	//just to avoid out of range limits
	for(i=1; i<=4; i++)
	{
		if(xAvoidIni[i]<0 || xAvoidEnd[i]>(i)*stitchW)
		{
			xAvoidIni[i] = (i-1)*stitchW + ((xMidd[i] - offsetX) - stitchX[i]);
			xAvoidEnd[i] = xAvoidIni[i] + 2*offsetX;
		}
		
	}
}

//this method just finds edges of the bottle neck according to the threshold
void CXAxisView::findBottleInPicC1(int nCam, int fact)
{
	int Length	= CroppedCameraHeight/fact;
	int Height	= CroppedCameraWidth/fact;

	//bottleXL[nCam]	= 0;	bottleYL[nCam]	= 0; 
	//bottleXR[nCam]	= 0;	bottleYR[nCam]	= 0;
	//xMidd[nCam]		= 0;

	int pix0, pix1, pix2, pix3, pix4, pix5, pix6;
	int diff1, diff2, diff3;

	int th = theapp->jobinfo[pframe->CurrentJobNum].senMidd;
	int midImg = theapp->jobinfo[pframe->CurrentJobNum].MiddImg;

	//image selection
	if(midImg==1)		{img1MiddBottle = im_cambw1;}
	else if(midImg==2)	{img1MiddBottle = img1SatDiv3;}
	else if(midImg==3)	{img1MiddBottle = img1MagDiv3;}
	else if(midImg==4)	{img1MiddBottle = img1YelDiv3;}
	else if(midImg==5)	{img1MiddBottle = img1CyaDiv3;}
	else if(midImg==6)	{img1MiddBottle = im_cam1Rbw;}
	else if(midImg==7)	{img1MiddBottle = im_cam1Gbw;}
	else if(midImg==8)	{img1MiddBottle = im_cam1Bbw;}

	bool debugging=false;
	if(debugging)	{SaveBWCustom1(img1MiddBottle, Length, Height);}

	int y = theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[nCam]/fact;

	if(y<0 || y>Height) return;
	int x;
	if(midImg==1 || midImg==6 || midImg==7 || midImg==8) //using BW image with White Plastic as background
	{
		for(x=10; x<Length/2; x++)
		{
			pix1 = *(img1MiddBottle + x - 1 + Length*y);
			pix2 = *(img1MiddBottle + x - 2 + Length*y);
			pix3 = *(img1MiddBottle + x - 3 + Length*y);
			pix4 = *(img1MiddBottle + x + 1 + Length*y);
			pix5 = *(img1MiddBottle + x + 2 + Length*y);
			pix6 = *(img1MiddBottle + x + 3 + Length*y);

			diff1 = pix1 - pix4; diff2 = pix2 - pix5; diff3 = pix3 - pix6;

			if(diff1>th && diff2>th && diff3>th)
			{bottleXL[nCam]=x*fact;	bottleYL[nCam]=y*fact;	break;}
		}

		for(x=Length-10; x>Length/2; x--)
		{
			pix1 = *(img1MiddBottle + x - 1 + Length*y);
			pix2 = *(img1MiddBottle + x - 2 + Length*y);
			pix3 = *(img1MiddBottle + x - 3 + Length*y);
			pix4 = *(img1MiddBottle + x + 1 + Length*y);
			pix5 = *(img1MiddBottle + x + 2 + Length*y);
			pix6 = *(img1MiddBottle + x + 3 + Length*y);

			diff1 = pix4 - pix1; diff2 = pix5 - pix2; diff3 = pix6 - pix3;

			if(diff1>th && diff2>th && diff3>th)
			{bottleXR[nCam]=x*fact;	bottleYR[nCam]=y*fact;	break;}
		}
	}
	else
	{
		for(int x=0; x<Length/2; x++)
		{
			pix0 = *(img1MiddBottle + x + 0 + Length*y);
			pix1 = *(img1MiddBottle + x + 1 + Length*y);
			pix2 = *(img1MiddBottle + x + 2 + Length*y);
			pix3 = *(img1MiddBottle + x + 3 + Length*y);

			if(pix0>th && pix1>th && pix2>th && pix3>th)
			{bottleXL[nCam]=x*fact;	bottleYL[nCam]=y*fact;	break;}
		}

		for(x=Length; x>Length/2; x--)
		{
			pix0 = *(img1MiddBottle + x - 0 + Length*y);
			pix1 = *(img1MiddBottle + x - 1 + Length*y);
			pix2 = *(img1MiddBottle + x - 2 + Length*y);
			pix3 = *(img1MiddBottle + x - 3 + Length*y);

			if(pix0>th && pix1>th && pix2>th && pix3>th)
			{bottleXR[nCam]=x*fact;	bottleYR[nCam]=y*fact;	break;}
		}
	}

	xMidd[nCam] = (bottleXL[nCam]+bottleXR[nCam])/2;
}

void CXAxisView::findBottleInPicC2(int nCam, int fact)
{
	int Length	= CroppedCameraHeight/fact;
	int Height	= CroppedCameraWidth/fact;

	//bottleXL[nCam]	= 0;	bottleYL[nCam]	= 0; 
	//bottleXR[nCam]	= 0;	bottleYR[nCam]	= 0;
	//xMidd[nCam]		= 0;

	int pix0, pix1, pix2, pix3, pix4, pix5, pix6;
	int diff1, diff2, diff3;

	int th = theapp->jobinfo[pframe->CurrentJobNum].senMidd;
	int midImg = theapp->jobinfo[pframe->CurrentJobNum].MiddImg;

	if(midImg==1)		{img2MiddBottle = im_cambw2;} 
	else if(midImg==2)	{img2MiddBottle = img2SatDiv3;}
	else if(midImg==3)	{img2MiddBottle = img2MagDiv3;}
	else if(midImg==4)	{img2MiddBottle = img2YelDiv3;}
	else if(midImg==5)	{img2MiddBottle = img2CyaDiv3;}
	else if(midImg==6)	{img2MiddBottle = im_cam2Rbw;}
	else if(midImg==7)	{img2MiddBottle = im_cam2Gbw;}
	else if(midImg==8)	{img2MiddBottle = im_cam2Bbw;}

	bool debugging=false;
	if(debugging)	{SaveBWCustom1(img2MiddBottle, Length, Height);}

	int y = theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[nCam]/fact;

	if(y<0 || y>Height) return;

	int x;
	if(midImg==1 || midImg==6 || midImg==7 || midImg==8) //using BW image with White Plastic as background
	{
		for(x=10; x<Length/2; x++)
		{
			pix1 = *(img2MiddBottle + x - 1 + Length*y);
			pix2 = *(img2MiddBottle + x - 2 + Length*y);
			pix3 = *(img2MiddBottle + x - 3 + Length*y);
			pix4 = *(img2MiddBottle + x + 1 + Length*y);
			pix5 = *(img2MiddBottle + x + 2 + Length*y);
			pix6 = *(img2MiddBottle + x + 3 + Length*y);

			diff1 = pix1 - pix4; diff2 = pix2 - pix5; diff3 = pix3 - pix6;

			if(diff1>th && diff2>th && diff3>th)
			{bottleXL[nCam]=x*fact;	bottleYL[nCam]=y*fact;	break;}
		}

		for(x=Length-10; x>Length/2; x--)
		{
			pix1 = *(img2MiddBottle + x - 1 + Length*y);
			pix2 = *(img2MiddBottle + x - 2 + Length*y);
			pix3 = *(img2MiddBottle + x - 3 + Length*y);
			pix4 = *(img2MiddBottle + x + 1 + Length*y);
			pix5 = *(img2MiddBottle + x + 2 + Length*y);
			pix6 = *(img2MiddBottle + x + 3 + Length*y);

			diff1 = pix4 - pix1; diff2 = pix5 - pix2; diff3 = pix6 - pix3;

			if(diff1>th && diff2>th && diff3>th)
			{bottleXR[nCam]=x*fact;	bottleYR[nCam]=y*fact;	break;}
		}
	}
	else
	{
		for(int x=0; x<Length/2; x++)
		{
			pix0 = *(img2MiddBottle + x + 0 + Length*y);
			pix1 = *(img2MiddBottle + x + 1 + Length*y);
			pix2 = *(img2MiddBottle + x + 2 + Length*y);
			pix3 = *(img2MiddBottle + x + 3 + Length*y);

			if(pix0>th && pix1>th && pix2>th && pix3>th)
			{bottleXL[nCam]=x*fact;	bottleYL[nCam]=y*fact;	break;}
		}

		for(x=Length; x>Length/2; x--)
		{
			pix0 = *(img2MiddBottle + x - 0 + Length*y);
			pix1 = *(img2MiddBottle + x - 1 + Length*y);
			pix2 = *(img2MiddBottle + x - 2 + Length*y);
			pix3 = *(img2MiddBottle + x - 3 + Length*y);

			if(pix0>th && pix1>th && pix2>th && pix3>th)
			{bottleXR[nCam]=x*fact;	bottleYR[nCam]=y*fact;	break;}
		}
	}

	xMidd[nCam] = (bottleXL[nCam]+bottleXR[nCam])/2;
}

void CXAxisView::findBottleInPicC3(int nCam, int fact)
{
	int Length	= CroppedCameraHeight/fact;
	int Height	= CroppedCameraWidth/fact;

	//bottleXL[nCam]	= 0;	bottleYL[nCam]	= 0; 
	//bottleXR[nCam]	= 0;	bottleYR[nCam]	= 0;
	//xMidd[nCam]		= 0;

	int pix0, pix1, pix2, pix3, pix4, pix5, pix6;
	int diff1, diff2, diff3;

	int th = theapp->jobinfo[pframe->CurrentJobNum].senMidd;
	int midImg = theapp->jobinfo[pframe->CurrentJobNum].MiddImg;

	if(midImg==1)		{img3MiddBottle = im_cambw3;}
	else if(midImg==2)	{img3MiddBottle = img3SatDiv3;}
	else if(midImg==3)	{img3MiddBottle = img3MagDiv3;}
	else if(midImg==4)	{img3MiddBottle = img3YelDiv3;}
	else if(midImg==5)	{img3MiddBottle = img3CyaDiv3;}
	else if(midImg==6)	{img3MiddBottle = im_cam3Rbw;}
	else if(midImg==7)	{img3MiddBottle = im_cam3Gbw;}
	else if(midImg==8)	{img3MiddBottle = im_cam3Bbw;}

	bool debugging=false;
	if(debugging)	{SaveBWCustom1(img3MiddBottle, Length, Height);}

	int y = theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[nCam]/fact;

	if(y<0 || y>Height) return;
	int x;
	if(midImg==1 || midImg==6 || midImg==7 || midImg==8) //using BW image with White Plastic as background
	{
		for(x=10; x<Length/2; x++)
		{
			pix1 = *(img3MiddBottle + x - 1 + Length*y);
			pix2 = *(img3MiddBottle + x - 2 + Length*y);
			pix3 = *(img3MiddBottle + x - 3 + Length*y);
			pix4 = *(img3MiddBottle + x + 1 + Length*y);
			pix5 = *(img3MiddBottle + x + 2 + Length*y);
			pix6 = *(img3MiddBottle + x + 3 + Length*y);

			diff1 = pix1 - pix4; diff2 = pix2 - pix5; diff3 = pix3 - pix6;

			if(diff1>th && diff2>th && diff3>th)
			{bottleXL[nCam]=x*fact;	bottleYL[nCam]=y*fact;	break;}
		}

		for(x=Length-10; x>Length/2; x--)
		{
			pix1 = *(img3MiddBottle + x - 1 + Length*y);
			pix2 = *(img3MiddBottle + x - 2 + Length*y);
			pix3 = *(img3MiddBottle + x - 3 + Length*y);
			pix4 = *(img3MiddBottle + x + 1 + Length*y);
			pix5 = *(img3MiddBottle + x + 2 + Length*y);
			pix6 = *(img3MiddBottle + x + 3 + Length*y);

			diff1 = pix4 - pix1; diff2 = pix5 - pix2; diff3 = pix6 - pix3;

			if(diff1>th && diff2>th && diff3>th)
			{bottleXR[nCam]=x*fact;	bottleYR[nCam]=y*fact;	break;}
		}
	}
	else
	{
		for(int x=0; x<Length/2; x++)
		{
			pix0 = *(img3MiddBottle + x + 0 + Length*y);
			pix1 = *(img3MiddBottle + x + 1 + Length*y);
			pix2 = *(img3MiddBottle + x + 2 + Length*y);
			pix3 = *(img3MiddBottle + x + 3 + Length*y);

			if(pix0>th && pix1>th && pix2>th && pix3>th)
			{bottleXL[nCam]=x*fact;	bottleYL[nCam]=y*fact;	break;}
		}

		for(x=Length; x>Length/2; x--)
		{
			pix0 = *(img3MiddBottle + x - 0 + Length*y);
			pix1 = *(img3MiddBottle + x - 1 + Length*y);
			pix2 = *(img3MiddBottle + x - 2 + Length*y);
			pix3 = *(img3MiddBottle + x - 3 + Length*y);

			if(pix0>th && pix1>th && pix2>th && pix3>th)
			{bottleXR[nCam]=x*fact;	bottleYR[nCam]=y*fact;	break;}
		}
	}

	xMidd[nCam] = (bottleXL[nCam]+bottleXR[nCam])/2;
}


void CXAxisView::findBottleInPicC4(int nCam, int fact)
{
	int Length	= CroppedCameraHeight/fact;
	int Height	= CroppedCameraWidth/fact;

	//bottleXL[nCam]	= 0;	bottleYL[nCam]	= 0; 
	//bottleXR[nCam]	= 0;	bottleYR[nCam]	= 0;
	//xMidd[nCam]		= 0;

	int pix0, pix1, pix2, pix3, pix4, pix5, pix6;
	int diff1, diff2, diff3;

	int th = theapp->jobinfo[pframe->CurrentJobNum].senMidd;
	int midImg = theapp->jobinfo[pframe->CurrentJobNum].MiddImg;

	if(midImg==1)		{img4MiddBottle = im_cambw4;}
	else if(midImg==2)	{img4MiddBottle = img4SatDiv3;}
	else if(midImg==3)	{img4MiddBottle = img4MagDiv3;}
	else if(midImg==4)	{img4MiddBottle = img4YelDiv3;}
	else if(midImg==5)	{img4MiddBottle = img4CyaDiv3;}
	else if(midImg==6)	{img4MiddBottle = im_cam4Rbw;}
	else if(midImg==7)	{img4MiddBottle = im_cam4Gbw;}
	else if(midImg==8)	{img4MiddBottle = im_cam4Bbw;}

	bool debugging=false;
	if(debugging)	{SaveBWCustom1(img4MiddBottle, Length, Height);}

	int y = theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[nCam]/fact;

	if(y<0 || y>Height) return;
	int x;
	if(midImg==1 || midImg==6 || midImg==7 || midImg==8) //using BW image with White Plastic as background
	{
		for(x=10; x<Length/2; x++)
		{
			pix1 = *(img4MiddBottle + x - 1 + Length*y);
			pix2 = *(img4MiddBottle + x - 2 + Length*y);
			pix3 = *(img4MiddBottle + x - 3 + Length*y);
			pix4 = *(img4MiddBottle + x + 1 + Length*y);
			pix5 = *(img4MiddBottle + x + 2 + Length*y);
			pix6 = *(img4MiddBottle + x + 3 + Length*y);

			diff1 = pix1 - pix4; diff2 = pix2 - pix5; diff3 = pix3 - pix6;

			if(diff1>th && diff2>th && diff3>th)
			{bottleXL[nCam]=x*fact;	bottleYL[nCam]=y*fact;	break;}
		}

		for(x=Length-10; x>Length/2; x--)
		{
			pix1 = *(img4MiddBottle + x - 1 + Length*y);
			pix2 = *(img4MiddBottle + x - 2 + Length*y);
			pix3 = *(img4MiddBottle + x - 3 + Length*y);
			pix4 = *(img4MiddBottle + x + 1 + Length*y);
			pix5 = *(img4MiddBottle + x + 2 + Length*y);
			pix6 = *(img4MiddBottle + x + 3 + Length*y);

			diff1 = pix4 - pix1; diff2 = pix5 - pix2; diff3 = pix6 - pix3;

			if(diff1>th && diff2>th && diff3>th)
			{bottleXR[nCam]=x*fact;	bottleYR[nCam]=y*fact;	break;}
		}
	}
	else
	{
		for(x=0; x<Length/2; x++)
		{
			pix0 = *(img4MiddBottle + x + 0 + Length*y);
			pix1 = *(img4MiddBottle + x + 1 + Length*y);
			pix2 = *(img4MiddBottle + x + 2 + Length*y);
			pix3 = *(img4MiddBottle + x + 3 + Length*y);

			if(pix0>th && pix1>th && pix2>th && pix3>th)
			{bottleXL[nCam]=x*fact;	bottleYL[nCam]=y*fact;	break;}
		}

		for(x=Length; x>Length/2; x--)
		{
			pix0 = *(img4MiddBottle + x - 0 + Length*y);
			pix1 = *(img4MiddBottle + x - 1 + Length*y);
			pix2 = *(img4MiddBottle + x - 2 + Length*y);
			pix3 = *(img4MiddBottle + x - 3 + Length*y);

			if(pix0>th && pix1>th && pix2>th && pix3>th)
			{bottleXR[nCam]=x*fact;	bottleYR[nCam]=y*fact;	break;}
		}
	}

	xMidd[nCam] = (bottleXL[nCam]+bottleXR[nCam])/2;
}

TwoPoint CXAxisView::FindPatternWithDoubleCheck(BYTE *im_srcIn, int nCam)
{
	TwoPoint result;
	result.x = result.y = result.x1 = 50;

	int Limit			=	40;
	int d1				=	0;
	int r;//,q;
	
	int LineLength		=	theapp->camHeight[nCam]/3;
	int Height			=	theapp->camWidth[nCam]/3;
	
	bool debugging=false;

	if(debugging)	{SaveBWCustom1(im_srcIn, LineLength, Height);}

	int		LineLength2	=	12;
	float	n			=	LineLength2*LineLength2;

	int		xpos2		=	0;	
	int		ypos2		=	0;

	float	sumXY		=	0;
	float	sumX		=	0;
	float	sumXX		=	0;
	float	sumY		=	0;
	float	sumYY		=	0;

	float	f0			=	0;
	float	f1			=	0;
	float	f2			=	0;
	float	corr		=	0;

	float	savedBestCorr	=	0.3f;
	int		savedPosX		=	0;
	int		cnt				=	0;
//	int		startY			=	theapp->jobinfo[pframe->CurrentJobNum].tgYpattern;
	int		startY			=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt;

	/////////////////

	sumX = 0.0;	sumXX = 0.0;
	
	for(r=0; r < n; r++)
	{
		sumX	+= *(im_mat+r);
		sumXX	+= (*(im_mat+r))*(*(im_mat+r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;
	bool atInitLoop = false;

	int thWhiteG = 180;
	int countWhiteInG = 0;


	//////////////////
//	if(startY<=50)	{startY=50;}

	if(startY<=50)	{startY=50;}

//	for(int y=startY-13; y<=startY+13; y++)//22
	for(int y=startY-20; y<=startY+20; y++)//22
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop	=	true;

		for(x=10; x<=126; x++)
		{
			sumXY	=	0.0; 
			
			for(ypos=-6; ypos<6; ypos++)
			{
				if(atInitLoop)
				{
					for(xpos=-6; xpos<6; xpos++)
					{
						pixT	=	*(im_mat+xpos+6+12*(ypos+6));
						pix		=	*(im_srcIn+ xpos + x + LineLength*(ypos+y));
						
						sumY	+=	pix;
						sumYY	+=	pix*pix;
						sumXY	+=	pixT*pix;
					}
				}
				else
				{
					pixOld		=	*(im_srcIn - 7 + x + LineLength*(ypos+y));
					pixNew		=	*(im_srcIn + 5 + x + LineLength*(ypos+y));
					
					for(xpos=-6; xpos<6; xpos++)
					{
						pixT	=	*(im_mat+xpos+6+12*(ypos+6));
						pix		=	*(im_srcIn+ xpos + x + LineLength*(ypos+y));
						
						sumXY	+=	pixT*pix;
					}

					sumY		+=	pixNew;
					sumY		-=	pixOld;
					sumYY		+=	pixNew*pixNew;
					sumYY		-=	pixOld*pixOld;
				}
			}

			atInitLoop = false;

			f0	=	sumXY - ((sumX*sumY)/n);
			f1	=	sumXX - ((sumX*sumX)/n);
			f2	=	sumYY - ((sumY*sumY)/n);
			
			if(sqrtf(f1*f2)!=0.0)	{corr=f0/sqrtf(f1*f2);}
			else					{corr=0;}
			
			if(corr>=savedBestCorr)
			{
				countWhiteInG = 0;

				//Double Checking in White Area
				for(int i = x-3; i<x+3; i++)
				{
					for(int j = y+5; j<=y+15; j++)
					{
						pix	= *(im_srcIn+ i + LineLength*j);

						if(pix>thWhiteG)
						{countWhiteInG++;}
					}
				}

				if(countWhiteInG>=20)
				{
					savedBestCorr	=	corr;
					savedPosX		=	x;
					result.x1		=	int(savedBestCorr*100);
					result.y		=	y;
				}
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x	=	savedPosX;

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C1(BYTE *imgIn, int nCam, int factor, SpecialPoint refPt)
{
/*	SpecialPoint result;
	result.x=result.c=0.00;
	result.y = MainDisplayImageHeight;
	avgBlock[nCam]=255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int x;
	int pix, pix1, pix2;
	int diff;

	//////////////////

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int th			=	50;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	
	for(x=15; x<120; x++)
	{
		foundEdge=false;

		for(int y=startY; (y<endY && !foundEdge); y++)
		{
			//**** First look for an edge
			pix1	= *(imgIn+ x + LineLength*y);
			pix		= *(imgIn+ x + LineLength*(y+3));

			//diff = abs(pix1-pix);
			diff = pix1-pix;

			if(diff>th)
			{
				stopNow			=	false;
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				for(int y2=y+4; (y2<y+hTaught && !stopNow); y2++)
				{
					for(int x2=x; (x2<x+15 && !stopNow); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(pix2<=senTaught)	{counterBlock++; sumpix+=pix2;}
						else				{stopNow=true;}
					}
				}
				
				//*** Only go inside if block consistently found
				if(!stopNow)
				{
					foundEdge=true; //no need to keep looking in this column

					if((y*factor)<result.y)
					{
						if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
						else				{avgBlock[nCam] = 0;}

						result.x	=	x*factor;
						result.y	=	y*factor;
					}
				}
			}
		}
	}

	return result;*/



	avgBlock[nCam]=255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	/////////////////

	int x;
	int pix, pix1, pix2;
	int diff;

	//////////////////

	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;
	//int th			=	50;
	int th			=	theapp->jobinfo[pframe->CurrentJobNum].sensEdge;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	SpecialPoint result;
	result.x	=	result.c	=	0.00;
	result.y	=	startY*factor;


	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	int minAvg = 255;
	int avgNow;
	int cntEdges = 0;

	for(x=15; x<120 - wTaught; x++)
	{
		for(int y=startY; (y<endY); y++)
		{
			foundEdge=true;
			cntEdges = 0;

			//**** First look for an edge
			for(int xW = x; xW<x+wTaught; xW++)
			{
				pix1	= *(imgIn + xW + LineLength*y);
				pix		= *(imgIn + xW + LineLength*(y+2));

				if(EdgeTransition==0)	{diff = pix1-pix;}
				else					{diff = pix-pix1;}

				if(diff<th)	{foundEdge = false; break;}
			}

			if(foundEdge)
			{
				stopNow			=	false;
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				//for(int y2=y+4; (y2<y+hTaught && !stopNow); y2++)
				for(int y2=y+4; (y2<y+hTaught); y2++)
				{
					//for(int x2=x; (x2<x+wTaught && !stopNow); x2++)
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);
						counterBlock++; sumpix+=pix2;
					}
				}
				
				//*** Only go inside if block consistently found
				if(counterBlock!=0) {avgNow = sumpix/counterBlock;}
				else				{avgNow = 255;}

				//if((y*factor)<result.y && avgBlock[lookinCam]<minAvg )
				if(avgNow<minAvg )
				{
					avgBlock[nCam] = avgNow;
					minAvg		=	avgBlock[nCam];
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	avgBlock[nCam];
				}
			}
		}
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C1x(BYTE *imgIn, BYTE *imgPatt, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	/////////////////

	int x;
	int pix, pix2;//, pix1;
	//int diff;

	//////////////////

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
	
	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//Let's find threshold
	//////////////////////////
	int i;
	for(i=0; i<=255; i++)	{pixVal[nCam][i]=0;}
	int ncount=0;
	int y;
	for( y=startY; (y<endY); y++)
	{
		for(x=20; x<LineLength-20; x++)
		{
			pix = *(imgIn+ x + LineLength*y);

			pixVal[nCam][pix]++;
			ncount++;
		}
	}

	if(ncount<=0) {ncount=1;}

	int thVal05perc = ncount*05/100;
	int thVal10perc = ncount*10/100;
	int thVal15perc = ncount*15/100;
	int thVal25perc = ncount*25/100;
	int thVal50perc = ncount*50/100;
	int thVal75perc = ncount*75/100;
	int thVal85perc = ncount*85/100;
	int thVal95perc = ncount*95/100;

	int greyVal05perc = 0;
	int greyVal10perc = 0;
	int greyVal15perc = 0;
	int greyVal25perc = 0;
	int greyVal50perc = 0;
	int greyVal75perc = 0;
	int greyVal85perc = 0;
	int greyVal95perc = 0;
	
	bool th05Found = false;
	bool th10Found = false;
	bool th15Found = false;
	bool th25Found = false;
	bool th50Found = false;
	bool th75Found = false;
	bool th85Found = false;
	bool th95Found = false;
	
	for( i=0; i<=255; i++)
	{
		sumpix	+=	pixVal[nCam][i];

		if(sumpix>=thVal05perc && !th05Found)		{greyVal05perc =	i; th05Found =	true;}
		if(sumpix>=thVal10perc && !th10Found)		{greyVal10perc =	i; th10Found =	true;}
		if(sumpix>=thVal15perc && !th15Found)		{greyVal15perc =	i; th15Found =	true;}
		if(sumpix>=thVal25perc && !th25Found)		{greyVal25perc =	i; th25Found =	true;}
		if(sumpix>=thVal50perc && !th50Found)		{greyVal50perc	=	i; th50Found = true;}
		if(sumpix>=thVal75perc && !th75Found)		{greyVal75perc	=	i; th75Found =	true;}
		if(sumpix>=thVal85perc && !th85Found)		{greyVal85perc	=	i; th85Found =	true;}
		if(sumpix>=thVal95perc && !th95Found)		{greyVal95perc	=	i; th95Found =	true;}
	}

	int qtyInBlock = 0;

	//////////////////////////
//	int th = theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int th				= greyVal05perc;
	int thBrightness	= greyVal95perc;

	if(EdgeTransition==0)	//dark band
	{th = greyVal05perc;}
	else					//light band
	{th = greyVal75perc;}

	for(y=startY; (y<endY-hTaught); y++)
	{
		for(x=15; x<LineLength - wTaught; x++)
		{
			counterBlock	=	0; 
			sumpix			=	0;

			//**** Second look for a consistent block
			for(int y2=y; (y2<y+hTaught); y2++)
			{
				for(int x2=x; (x2<x+wTaught); x2++)
				{
					pix2 = *(imgIn+ x2 + LineLength*y2);

					if(EdgeTransition==0)	//dark band
					{
						if(pix2<th)	
						{counterBlock++; sumpix+=pix2;}
					}
					else
					{
						if(pix2>th && pix2<thBrightness)
						{counterBlock++; sumpix+=pix2;}
					}

				}
			}
			
			qtyInBlock = counterBlock;

			if(qtyInBlock>maxCount )
			{
				maxCount	=	qtyInBlock;

				if(qtyInBlock!=0)
				{avgBlock[nCam] = sumpix/qtyInBlock;}
				
				result.x	=	x*factor;
				result.y	=	y*factor;
			}
		}
	}

/*	endY = (result.y/factor) + hTaught;

	int starX = result.x/factor;
	int endX = result.x/factor + wTaught ;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int sumpixTop, sumpixBot, avgTop, avgBot;

	th = theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	for(y=endY; y>startY; y--)
	{
		cntTransition	=	0; 
		sumpixTop		=	0;
		
		for(int y2=0; y2<3; y2++)
		{
			for(x=starX; x<endX; x++)
			{
				cntTransition++;
				pix2 = *(imgIn+ x + LineLength*(y-y2));
				sumpixTop+=pix2; 
			}
		}

		if(cntTransition<=0)	{cntTransition=1;}

		avgTop	=	sumpixTop/cntTransition;

		if(EdgeTransition==0)	//dark band
		{currTransition = avgTop - avgBlock[nCam];}
		else					//light band
		{currTransition = avgBlock[nCam] - avgTop;}

		if(currTransition>th )
		{
			maxTransition	=	currTransition;

			result.x	=	starX*factor;
			result.y	=	y*factor;
			break;
		}
	}*/

	return result;
}

void CXAxisView::FindLabelEdge_C1z1(BYTE *imgIn, int nCam, int factor)
{
	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int pix;
	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;


	//////////////////////////
	for(int i=0; i<=255; i++)	{pixVal[nCam][i]=0;}
	nhisPix[nCam]=0;

	for(int y=startY; y<endY; y++)
	{
		for(int x=20; x<LineLength-20; x++)
		{
			pix = *(imgIn+ x + LineLength*y);

			pixVal[nCam][pix]++;
			nhisPix[nCam]++;
		}
	}
}

void CXAxisView::FindLabelEdge_C2z1(BYTE *imgIn, int nCam, int factor)
{
	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int pix;
	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;


	//////////////////////////
	for(int i=0; i<=255; i++)	{pixVal[nCam][i]=0;}
	nhisPix[nCam]=0;

	for(int y=startY; y<endY; y++)
	{
		for(int x=20; x<LineLength-20; x++)
		{
			pix = *(imgIn+ x + LineLength*y);

			pixVal[nCam][pix]++;
			nhisPix[nCam]++;
		}
	}
}

void CXAxisView::FindLabelEdge_C3z1(BYTE *imgIn, int nCam, int factor)
{
	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int pix;
	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	//////////////////////////
	for(int i=0; i<=255; i++)	{pixVal[nCam][i]=0;}
	nhisPix[nCam]=0;

	for(int y=startY; y<endY; y++)
	{
		for(int x=20; x<LineLength-20; x++)
		{
			pix = *(imgIn+ x + LineLength*y);

			pixVal[nCam][pix]++;
			nhisPix[nCam]++;
		}
	}
}

void CXAxisView::FindLabelEdge_C4z1(BYTE *imgIn, int nCam, int factor)
{
	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int pix;
	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;


	//////////////////////////
	for(int i=0; i<=255; i++)	{pixVal[nCam][i]=0;}
	nhisPix[nCam]=0;

	for(int y=startY; y<endY; y++)
	{
		for(int x=20; x<LineLength-20; x++)
		{
			pix = *(imgIn+ x + LineLength*y);

			pixVal[nCam][pix]++;
			nhisPix[nCam]++;
		}
	}
}

TwoPoint CXAxisView::FindLabelEdge_C1z2(BYTE *imgIn, int nCam, int factor, int thLow, int thHigh)
{
	TwoPoint result;

	result.x		=	result.x1	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2;
	//int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	20;
	int endX			=	LineLength - 20 - wTaught;
	int sumpixTop, sumpixBot, avgTop, avgBot;

	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;
	int th	= thLow;
	int thBrightness = thHigh;
	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			cntTransition	=	0; 
			sumpixTop		=	0;
			sumpixBot		=	0;
			
			for(int y2=1; y2<=3; y2++)
			{
				for(int x2=0; x2<wTaught; x2++)
				{
					cntTransition++;
					pix1 = *(imgIn + x + x2 + LineLength*(y-y2));	sumpixTop+=pix1; 
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));	sumpixBot+=pix2; 
				}
			}

			if(cntTransition<=0)	{cntTransition=1;}

			avgTop	=	sumpixTop/cntTransition;
			avgBot	=	sumpixBot/cntTransition;

			if(EdgeTransition==0)	{currTransition = avgTop - avgBot;}	//dark band
			else					{currTransition = avgBot - avgTop;}	//light band
			
			if(currTransition>thEdge)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<th)						{counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>th && pix2<thBrightness)	{counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount )
				{
					maxCount	=	counterBlock;

					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.x1	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	
	for(y=result.y/factor; y<result.y/factor + hTaught; y++)
	{
		for(x=result.x/factor; x<result.x/factor + wTaught; x++)
		{
			pix2 = *(imgIn+ x + LineLength*y);

			if(i<990)
			{
				if(pix2<th)						
				{
					xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
					i++;
				}	

			}
		}
	}

	nPaint[nCam] = i;

	return result;
}


TwoPoint CXAxisView::FindLabelEdge_C2z2(BYTE *imgIn, int nCam, int factor, int thLow, int thHigh)
{
	TwoPoint result;

	result.x		=	result.x1	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2;
	//int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	20;
	int endX			=	LineLength - 20 - wTaught;
	int sumpixTop, sumpixBot, avgTop, avgBot;

	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;
	int th	= thLow;
	int thBrightness = thHigh;
	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			cntTransition	=	0; 
			sumpixTop		=	0;
			sumpixBot		=	0;
			
			for(int y2=1; y2<=3; y2++)
			{
				for(int x2=0; x2<wTaught; x2++)
				{
					cntTransition++;
					pix1 = *(imgIn + x + x2 + LineLength*(y-y2));	sumpixTop+=pix1; 
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));	sumpixBot+=pix2; 
				}
			}

			if(cntTransition<=0)	{cntTransition=1;}

			avgTop	=	sumpixTop/cntTransition;
			avgBot	=	sumpixBot/cntTransition;

			if(EdgeTransition==0)	{currTransition = avgTop - avgBot;}	//dark band
			else					{currTransition = avgBot - avgTop;}	//light band
			
			if(currTransition>thEdge)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<th)						{counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>th && pix2<thBrightness)	{counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount )
				{
					maxCount	=	counterBlock;

					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.x1	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	
	for(y=result.y/factor; y<result.y/factor + hTaught; y++)
	{
		for(x=result.x/factor; x<result.x/factor + wTaught; x++)
		{
			pix2 = *(imgIn+ x + LineLength*y);

			if(i<990)
			{
				if(pix2<th)						
				{
					xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
					i++;
				}	

			}
		}
	}

	nPaint[nCam] = i;

	return result;
}

TwoPoint CXAxisView::FindLabelEdge_C3z2(BYTE *imgIn, int nCam, int factor, int thLow, int thHigh)
{
	TwoPoint result;

	result.x		=	result.x1	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2;
	//int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	20;
	int endX			=	LineLength - 20 - wTaught;
	int sumpixTop, sumpixBot, avgTop, avgBot;

	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;
	int th	= thLow;
	int thBrightness = thHigh;

	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			cntTransition	=	0; 
			sumpixTop		=	0;
			sumpixBot		=	0;
			
			for(int y2=1; y2<=3; y2++)
			{
				for(int x2=0; x2<wTaught; x2++)
				{
					cntTransition++;
					pix1 = *(imgIn + x + x2 + LineLength*(y-y2));	sumpixTop+=pix1; 
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));	sumpixBot+=pix2; 
				}
			}

			if(cntTransition<=0)	{cntTransition=1;}

			avgTop	=	sumpixTop/cntTransition;
			avgBot	=	sumpixBot/cntTransition;

			if(EdgeTransition==0)	{currTransition = avgTop - avgBot;}	//dark band
			else					{currTransition = avgBot - avgTop;}	//light band
			
			if(currTransition>thEdge)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<th)						{counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>th && pix2<thBrightness)	{counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount )
				{
					maxCount	=	counterBlock;

					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.x1	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	
	for(y=result.y/factor; y<result.y/factor + hTaught; y++)
	{
		for(x=result.x/factor; x<result.x/factor + wTaught; x++)
		{
			pix2 = *(imgIn+ x + LineLength*y);

			if(i<990)
			{
				if(pix2<th)						
				{
					xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
					i++;
				}	

			}
		}
	}

	nPaint[nCam] = i;

	return result;
}

TwoPoint CXAxisView::FindLabelEdge_C4z2(BYTE *imgIn, int nCam, int factor, int thLow, int thHigh)
{
	TwoPoint result;

	result.x		=	result.x1	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2;
	//int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	20;
	int endX			=	LineLength - 20 - wTaught;
	int sumpixTop, sumpixBot, avgTop, avgBot;

	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;
	int th	= thLow;
	int thBrightness = thHigh;
	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			cntTransition	=	0; 
			sumpixTop		=	0;
			sumpixBot		=	0;
			
			for(int y2=1; y2<=3; y2++)
			{
				for(int x2=0; x2<wTaught; x2++)
				{
					cntTransition++;
					pix1 = *(imgIn + x + x2 + LineLength*(y-y2));	sumpixTop+=pix1; 
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));	sumpixBot+=pix2; 
				}
			}

			if(cntTransition<=0)	{cntTransition=1;}

			avgTop	=	sumpixTop/cntTransition;
			avgBot	=	sumpixBot/cntTransition;

			if(EdgeTransition==0)	{currTransition = avgTop - avgBot;}	//dark band
			else					{currTransition = avgBot - avgTop;}	//light band
			
			if(currTransition>thEdge)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<th)						{counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>th && pix2<thBrightness)	{counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount )
				{
					maxCount	=	counterBlock;

					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.x1	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	
	for(y=result.y/factor; y<result.y/factor + hTaught; y++)
	{
		for(x=result.x/factor; x<result.x/factor + wTaught; x++)
		{
			pix2 = *(imgIn+ x + LineLength*y);

			if(i<990)
			{
				if(pix2<th)						
				{
					xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
					i++;
				}
			}
		}
	}

	nPaint[nCam] = i;

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C1y(BYTE *imgIn, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2;
	//int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength - wTaught;
	int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;
	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			cntTransition	=	0; 
			sumpixTop		=	0;
			sumpixBot		=	0;
			
			for(int y2=1; y2<=3; y2++)
			{
				for(int x2=0; x2<wTaught; x2++)
				{
					cntTransition++;
					pix1 = *(imgIn + x + x2 + LineLength*(y-y2));	sumpixTop+=pix1; 
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));	sumpixBot+=pix2; 
				}
			}

			if(cntTransition<=0)	{cntTransition=1;}

			avgTop	=	sumpixTop/cntTransition;
			avgBot	=	sumpixBot/cntTransition;

			if(EdgeTransition==0)	{currTransition = avgTop - avgBot;}	//dark band
			else					{currTransition = avgBot - avgTop;}	//light band
			
			if(currTransition>thEdge)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<thBlock){counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>thBlock){counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount)
				{
					maxCount		=	counterBlock;
					edgeBlock[nCam] =	currTransition;
					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	
	if(result.y!=startY*factor)
	{
		for(y=result.y/factor + 1; y<result.y/factor + hTaught - 1; y++)
		{
			for(x=result.x/factor + 1; x<result.x/factor + wTaught - 1; x++)
			{
				pix2 = *(imgIn+ x + LineLength*y);

				if(i<9900)
				{
					if(EdgeTransition==0 && pix2<thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
					else if(EdgeTransition==1 && pix2>thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C2y(BYTE *imgIn, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2;
	//int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength - wTaught;
	int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;
	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			cntTransition	=	0; 
			sumpixTop		=	0;
			sumpixBot		=	0;
			
			for(int y2=1; y2<=3; y2++)
			{
				for(int x2=0; x2<wTaught; x2++)
				{
					cntTransition++;
					pix1 = *(imgIn + x + x2 + LineLength*(y-y2));	sumpixTop+=pix1; 
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));	sumpixBot+=pix2; 
				}
			}

			if(cntTransition<=0)	{cntTransition=1;}

			avgTop	=	sumpixTop/cntTransition;
			avgBot	=	sumpixBot/cntTransition;

			if(EdgeTransition==0)	{currTransition = avgTop - avgBot;}	//dark band
			else					{currTransition = avgBot - avgTop;}	//light band
			
			if(currTransition>thEdge)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<thBlock){counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>thBlock){counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount)
				{
					maxCount		=	counterBlock;
					edgeBlock[nCam] =	currTransition;
					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	
	if(result.y!=startY*factor)
	{
		for(y=result.y/factor + 1; y<result.y/factor + hTaught - 1; y++)
		{
			for(x=result.x/factor + 1; x<result.x/factor + wTaught - 1; x++)
			{
				pix2 = *(imgIn+ x + LineLength*y);

				if(i<9900)
				{
					if(EdgeTransition==0 && pix2<thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
					else if(EdgeTransition==1 && pix2>thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C3y(BYTE *imgIn, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2;
	//int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength - wTaught;
	int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;
	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			cntTransition	=	0; 
			sumpixTop		=	0;
			sumpixBot		=	0;
			
			for(int y2=1; y2<=3; y2++)
			{
				for(int x2=0; x2<wTaught; x2++)
				{
					cntTransition++;
					pix1 = *(imgIn + x + x2 + LineLength*(y-y2));	sumpixTop+=pix1; 
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));	sumpixBot+=pix2; 
				}
			}

			if(cntTransition<=0)	{cntTransition=1;}

			avgTop	=	sumpixTop/cntTransition;
			avgBot	=	sumpixBot/cntTransition;

			if(EdgeTransition==0)	{currTransition = avgTop - avgBot;}	//dark band
			else					{currTransition = avgBot - avgTop;}	//light band
			
			if(currTransition>thEdge)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<thBlock){counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>thBlock){counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount)
				{
					maxCount	=	counterBlock;
					edgeBlock[nCam] =	currTransition;
					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////


	int i = 0; 
	nPaint[nCam]=0;
	
	if(result.y!=startY*factor)
	{
		for(y=result.y/factor + 1; y<result.y/factor + hTaught - 1; y++)
		{
			for(x=result.x/factor + 1; x<result.x/factor + wTaught - 1; x++)
			{
				pix2 = *(imgIn+ x + LineLength*y);

				if(i<9900)
				{
					if(EdgeTransition==0 && pix2<thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
					else if(EdgeTransition==1 && pix2>thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C4y(BYTE *imgIn, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2;
	//int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength - wTaught;
	int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;
	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			cntTransition	=	0; 
			sumpixTop		=	0;
			sumpixBot		=	0;
			
			for(int y2=1; y2<=3; y2++)
			{
				for(int x2=0; x2<wTaught; x2++)
				{
					cntTransition++;
					pix1 = *(imgIn + x + x2 + LineLength*(y-y2));	sumpixTop+=pix1; 
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));	sumpixBot+=pix2; 
				}
			}

			if(cntTransition<=0)	{cntTransition=1;}

			avgTop	=	sumpixTop/cntTransition;
			avgBot	=	sumpixBot/cntTransition;

			if(EdgeTransition==0)	{currTransition = avgTop - avgBot;}	//dark band
			else					{currTransition = avgBot - avgTop;}	//light band
			
			if(currTransition>thEdge)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<thBlock){counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>thBlock){counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount)
				{
					maxCount		=	counterBlock;
					edgeBlock[nCam] =	currTransition;
					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	
	if(result.y!=startY*factor)
	{
		for(y=result.y/factor + 1; y<result.y/factor + hTaught - 1; y++)
		{
			for(x=result.x/factor + 1; x<result.x/factor + wTaught - 1; x++)
			{
				pix2 = *(imgIn+ x + LineLength*y);

				if(i<9900)
				{
					if(EdgeTransition==0 && pix2<thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
					else if(EdgeTransition==1 && pix2>thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C2(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt)
{
/*	SpecialPoint result;
	result.x=result.c=0.00;
	result.y = MainDisplayImageHeight;
	avgBlock[lookinCam]=255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	/////////////////

	int x;
	int pix, pix1, pix2;
	int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int th			=	50;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	
	for(x=15; x<120; x++)
	{
		foundEdge=false;

		for(int y=startY; (y<endY && !foundEdge); y++)
		{
			//**** First look for an edge
			pix1	= *(imgIn+ x + LineLength*y);
			pix		= *(imgIn+ x + LineLength*(y+3));

			if(EdgeTransition==0)	{diff = pix1-pix;}
			else					{diff = pix-pix1;}

			if(diff>th)
			{
				stopNow			=	false;
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				for(int y2=y+4; (y2<y+hTaught && !stopNow); y2++)
				{
					for(int x2=x; (x2<x+wTaught && !stopNow); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(pix2<=senTaught)	{counterBlock++; sumpix+=pix2;}
						else				{stopNow=true;}
					}
				}
				
				//*** Only go inside if block consistently found
				if(!stopNow)
				{
					foundEdge=true; //no need to keep looking in this column

					if((y*factor)<result.y)
					{
						if(counterBlock!=0) {avgBlock[lookinCam] = sumpix/counterBlock;}
						else				{avgBlock[lookinCam] = 0;}

						result.x	=	x*factor;
						result.y	=	y*factor;
					}
				}
			}
		}
	}

	return result;*/

	avgBlock[lookinCam]=255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	/////////////////

	int x;
	int pix, pix1, pix2;
	int diff;

	//////////////////

	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;
	//int th			=	50;
	int th			=	theapp->jobinfo[pframe->CurrentJobNum].sensEdge;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	SpecialPoint result;
	result.x	=	result.c	=	0.00;
	result.y	=	startY*factor;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	int minAvg = 255;
	int avgNow;
	int y;
	for(x=15; x<120 - wTaught; x++)
	{

		for(y=startY; (y<endY); y++)
		{
			foundEdge=true;

			//**** First look for an edge
			for(int xW = x; xW<x+ wTaught; xW++)
			{
				pix1	= *(imgIn + xW + LineLength*y);
				pix		= *(imgIn + xW + LineLength*(y+2));

				if(EdgeTransition==0)	{diff = pix1-pix;}
				else					{diff = pix-pix1;}

				if(diff<th)
				{foundEdge = false; break;}
			}

			if(foundEdge)
			{
				stopNow			=	false;
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				//for(int y2=y+4; (y2<y+hTaught && !stopNow); y2++)
				for(int y2=y+4; (y2<y+hTaught); y2++)
				{
					//for(int x2=x; (x2<x+wTaught && !stopNow); x2++)
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);
						counterBlock++; sumpix+=pix2;
					}
				}
				
				//*** Only go inside if block consistently found
				if(counterBlock!=0) {avgNow = sumpix/counterBlock;}
				else				{avgNow = 255;}

				//if((y*factor)<result.y && avgBlock[lookinCam]<minAvg )
				if(avgNow<minAvg )
				{
					avgBlock[lookinCam] = avgNow;
					minAvg		=	avgBlock[lookinCam];
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	avgBlock[lookinCam];
				}


			}
		}
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C3(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt)
{
	/*SpecialPoint result;
	result.x=result.c=0.00;
	result.y = MainDisplayImageHeight;
	avgBlock[lookinCam]=255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	/////////////////

	int x;
	int pix, pix1, pix2;
	int diff;

	//////////////////

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int th			=	50;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	
	for(x=15; x<120; x++)
	{
		foundEdge=false;

		for(int y=startY; (y<Height-hTaught && !foundEdge); y++)
		{
			//**** First look for an edge
			pix1	= *(imgIn+ x + LineLength*y);
			pix		= *(imgIn+ x + LineLength*(y+3));

			if(EdgeTransition==0)	{diff = pix1-pix;}
			else					{diff = pix-pix1;}

			if(diff>th)
			{
				stopNow			=	false;
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				for(int y2=y+4; (y2<y+hTaught && !stopNow); y2++)
				{
					for(int x2=x; (x2<x+wTaught && !stopNow); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(pix2<=senTaught)	{counterBlock++; sumpix+=pix2;}
						else				{stopNow=true;}
					}
				}
				
				//*** Only go inside if block consistently found
				if(!stopNow)
				{
					foundEdge=true; //no need to keep looking in this column

					if((y*factor)<result.y)
					{
						if(counterBlock!=0) {avgBlock[lookinCam] = sumpix/counterBlock;}
						else				{avgBlock[lookinCam] = 0;}

						result.x	=	x*factor;
						result.y	=	y*factor;
					}
				}
			}
		}
	}

	return result;*/
	
	avgBlock[lookinCam]=255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	/////////////////

	int x;
	int pix, pix1, pix2;
	int diff;

	//////////////////

	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;
//	int th			=	50;
	int th			=	theapp->jobinfo[pframe->CurrentJobNum].sensEdge;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	SpecialPoint result;
	result.x	=	result.c	=	0.00;
	result.y	=	startY*factor;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;
	int avgNow;

	int minAvg = 255;

	for(x=15; x<120 - wTaught; x++)
	{

		for(int y=startY; y<endY; y++)
		{
			foundEdge=true;

			//**** First look for an edge
			for(int xW = x; xW<x+wTaught; xW++)
			{
				pix1	= *(imgIn + xW + LineLength*y);
				pix		= *(imgIn + xW + LineLength*(y+2));

				if(EdgeTransition==0)	{diff = pix1-pix;}
				else					{diff = pix-pix1;}

				if(diff<th)
				{foundEdge = false; break;}
			}

			if(foundEdge)
			{
				stopNow			=	false;
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				//for(int y2=y+4; (y2<y+hTaught && !stopNow); y2++)
				for(int y2=y+4; (y2<y+hTaught); y2++)
				{
					//for(int x2=x; (x2<x+wTaught && !stopNow); x2++)
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);
						counterBlock++; sumpix+=pix2;
					}
				}
				
				//*** Only go inside if block consistently found
				if(counterBlock!=0) {avgNow = sumpix/counterBlock;}
				else				{avgNow = 255;}

				//if((y*factor)<result.y && avgBlock[lookinCam]<minAvg )
				if(avgNow<minAvg )
				{
					avgBlock[lookinCam] = avgNow;
					minAvg		=	avgBlock[lookinCam];
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	avgBlock[lookinCam];
				}
			}
		}
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C4(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt)
{
	/*SpecialPoint result;
	result.x=result.c=0.00;
	result.y = MainDisplayImageHeight;
	avgBlock[lookinCam]=255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	/////////////////

	int x;
	int pix, pix1, pix2;
	int diff;

	//////////////////

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int th			=	50;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	
	for(x=15; x<120; x++)
	{
		foundEdge=false;

		for(int y=startY; (y<Height-hTaught && !foundEdge); y++)
		{
			//**** First look for an edge
			pix1	= *(imgIn+ x + LineLength*y);
			pix		= *(imgIn+ x + LineLength*(y+3));

			if(EdgeTransition==0)	{diff = pix1-pix;}
			else					{diff = pix-pix1;}

			if(diff>th)
			{
				stopNow			=	false;
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				for(int y2=y+4; (y2<y+hTaught && !stopNow); y2++)
				{
					for(int x2=x; (x2<x+wTaught && !stopNow); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(pix2<=senTaught)	{counterBlock++; sumpix+=pix2;}
						else				{stopNow=true;}
					}
				}
				
				//*** Only go inside if block consistently found
				if(!stopNow)
				{
					foundEdge=true; //no need to keep looking in this column

					if((y*factor)<result.y)
					{
						if(counterBlock!=0) {avgBlock[lookinCam] = sumpix/counterBlock;}
						else				{avgBlock[lookinCam] = 0;}

						result.x	=	x*factor;
						result.y	=	y*factor;
					}
				}
			}
		}
	}

	return result;*/

	avgBlock[lookinCam]=255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	/////////////////

	int x;
	int pix, pix1, pix2;
	int diff;

	//////////////////
	
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;
	//int th			=	50;
	int th			=	theapp->jobinfo[pframe->CurrentJobNum].sensEdge;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	SpecialPoint result;
	result.x	=	result.c	=	0.00;
	result.y	=	startY*factor;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	int minAvg = 255;
	int avgNow;
	
	for(x=15; x<120 - wTaught; x++)
	{
		for(int y=startY; y<endY; y++)
		{
			foundEdge=true;

			//**** First look for an edge
			for(int xW = x; xW<x+wTaught; xW++)
			{
				pix1	= *(imgIn + xW + LineLength*y);
				pix		= *(imgIn + xW + LineLength*(y+2));

				if(EdgeTransition==0)	{diff = pix1-pix;}
				else					{diff = pix-pix1;}

				if(diff<th)
				{foundEdge = false; break;}
			}

			if(foundEdge)
			{
				stopNow			=	false;
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				//Calculating the average intensity of the block
				//for(int y2=y+4; (y2<y+hTaught && !stopNow); y2++)
				for(int y2=y+4; (y2<y+hTaught); y2++)
				{
					//for(int x2=x; (x2<x+wTaught && !stopNow); x2++)
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);
						counterBlock++; sumpix+=pix2;
					}
				}				
				
				//*** Only go inside if block consistently found
				if(counterBlock!=0) {avgNow = sumpix/counterBlock;}
				else				{avgNow = 255;}

				//if((y*factor)<result.y && avgBlock[lookinCam]<minAvg )
				if(avgNow<minAvg )
				{
					avgBlock[lookinCam] = avgNow;
					minAvg		=	avgBlock[lookinCam];
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	avgBlock[lookinCam];
				}
			}
		}
	}

	return result;
}

void CXAxisView::OnSaveDefSet(BYTE *im_sr1, BYTE *im_sr2, BYTE *im_sr3, BYTE *im_sr4, BYTE *im_sr5, int picnum)
{
	char buf[10];
	
	CString c =itoa(picnum,buf,10);
	CString d = c;
	
	char* pFileName = "c:\\silganData\\xtraDef\\C1_";//drive	
	char currentpath[60];

	strcpy(currentpath,pFileName);
	strcat(currentpath,d);
	strcat(currentpath,".bmp");
	
	int xdist=CroppedCameraWidth; 
	int ydist=CroppedCameraHeight;
	
	CVisRGBAByteImage image1(xdist,ydist,1,-1,im_sr1);
	imageCS=image1;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imageCS.WriteFile(filedescriptorT);
	}
	catch (...)
	{AfxMessageBox("21The file could not be saved. ");}

	////////

	pFileName = "c:\\silganData\\xtraDef\\C2_";//drive	
	
	strcpy(currentpath,pFileName);
	strcat(currentpath,d);
	strcat(currentpath,".bmp");
	
	xdist=CroppedCameraWidth; 
	ydist=CroppedCameraHeight; 
	
	CVisRGBAByteImage image2(xdist,ydist,1,-1,im_sr2);
	imageCS=image2;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imageCS.WriteFile(filedescriptorT);
	}
	catch (...)
	{AfxMessageBox("22The file could not be saved. ");}

	////////

	pFileName = "c:\\silganData\\xtraDef\\C3_";//drive	
	
	strcpy(currentpath,pFileName);
	strcat(currentpath,d);
	strcat(currentpath,".bmp");
	
	xdist=CroppedCameraWidth;  
	ydist=CroppedCameraHeight;  
	
	CVisRGBAByteImage image3(xdist,ydist,1,-1,im_sr3);
	imageCS=image3;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imageCS.WriteFile(filedescriptorT);
	}
	catch (...)
	{AfxMessageBox("23The file could not be saved. ");}

	////////

	pFileName = "c:\\silganData\\xtraDef\\C4_";//drive	
	
	strcpy(currentpath,pFileName);
	strcat(currentpath,d);
	strcat(currentpath,".bmp");
	
	xdist=CroppedCameraWidth;   
	ydist=CroppedCameraHeight;   
	
	CVisRGBAByteImage image4(xdist,ydist,1,-1,im_sr4);
	imageCS=image4;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imageCS.WriteFile(filedescriptorT);
	}
	catch (...)
	{AfxMessageBox("24The file could not be saved. ");}

	////////

	if(theapp->cam5Enable)
	{
		pFileName = "c:\\silganData\\xtraDef\\C5_";//drive	
		
		strcpy(currentpath,pFileName);
		strcat(currentpath,d);
		strcat(currentpath,".bmp");
		
		xdist=theapp->camWidth[5]; 
		ydist=theapp->camHeight[5]; 
		
		CVisRGBAByteImage image5(xdist,ydist,1,-1,im_sr5);
		imageCS=image5;
		
		try
		{
			SVisFileDescriptor filedescriptorT;
			ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
			filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
			filedescriptorT.jpeg_quality = 0;
			filedescriptorT.filename = currentpath;
			imageCS.WriteFile(filedescriptorT);
		}
		catch (...)
		{AfxMessageBox("25The file could not be saved. ");}
	}
}

SpecialPoint CXAxisView::FindWhiteBand_C1(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;
	result.x=result.c=0.00;
	result.y = MainDisplayImageHeight;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	int pix1,pix2,pix3,pix4,pix5,pix6,pix7,pix8,pix9;
	int pix, pixVal;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}
	int y;
	// Creating the Sobel image
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn + x + 1  + LineLength*(y + 1));

			//pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
			//		abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pix =	abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MIN(pix,255);

			*(img1Edg + x  + LineLength * y) = pixVal;
		}
	}

	if(debugging)	{SaveBWCustom1(img1Edg ,	LineLength,	Height);}

	/////////////////

	int x;
	//int diff;
	//int xpos, ypos;
	bool atInitLoop	=	false;

	//////////////////

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int th			=	80;

	int	counterV	=	0;
	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;

	int endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
		
	int countEdges	=	0;
	int	maxEdges	=	0;

	for(y=startY; y<endY; y++)
	{
		countEdges = 0;

		for(x=15; x<120; x++)
		{
			pix1	= *(img1Edg + x + LineLength*y);

			if(pix1>th)	{countEdges++;}
		}

		if(countEdges>maxEdges)
		{
			maxEdges	=	countEdges;
			result.x	=	(LineLength/2)*factor;
			result.y	=	(y)*factor;
		}
	}

	return result;
}

SpecialPoint CXAxisView::FindWhiteBand_C2(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;
	result.x=result.c=0.00;
	result.y = MainDisplayImageHeight;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	int pix1,pix2,pix3,pix4,pix5,pix6,pix7,pix8,pix9;
	int pix, pixVal;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	// Creating the Sobel image
	int y;
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn + x + 1  + LineLength*(y + 1));

			//pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
			//		abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pix =	abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MIN(pix,255);

			*(img2Edg + x  + LineLength * y) = pixVal;

			//if(pixVal>th)	{*(imgEdg[lookinCam]  + x  + LineLength * y) = 255;}
			//else			{*(imgEdg[lookinCam]  + x  + LineLength * y) = 0;}
		}
	}

	if(debugging)	{SaveBWCustom1(img2Edg ,	LineLength,	Height);}

	/////////////////

	int x;
	//int diff;
	//int xpos, ypos;
	bool atInitLoop	=	false;

	//////////////////

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int th			=	80;

	int	counterV	=	0;
	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;

	int endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
		
	int countEdges	=	0;
	int	maxEdges	=	0;

	for(y=startY; y<endY; y++)
	{
		countEdges = 0;

		for(x=15; x<120; x++)
		{
			pix1	= *(img2Edg + x + LineLength*y);

			if(pix1>th)	{countEdges++;}
		}

		if(countEdges>maxEdges)
		{
			maxEdges	=	countEdges;
			result.x	=	(LineLength/2)*factor;
			result.y	=	(y)*factor;
		}
	}

	return result;
}

SpecialPoint CXAxisView::FindWhiteBand_C3(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;
	result.x=result.c=0.00;
	result.y = MainDisplayImageHeight;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	int pix1,pix2,pix3,pix4,pix5,pix6,pix7,pix8,pix9;
	int pix, pixVal;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int y;
	// Creating the Sobel image
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn + x + 1  + LineLength*(y + 1));

			//pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
			//		abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pix =	abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MIN(pix,255);

			*(img3Edg + x  + LineLength * y) = pixVal;

			//if(pixVal>th)	{*(imgEdg[lookinCam]  + x  + LineLength * y) = 255;}
			//else			{*(imgEdg[lookinCam]  + x  + LineLength * y) = 0;}
		}
	}

	if(debugging)	{SaveBWCustom1(img3Edg ,	LineLength,	Height);}

	/////////////////

	int x;
	//int diff;
	//int xpos, ypos;
	bool atInitLoop	=	false;

	//////////////////

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int th			=	80;

	int	counterV	=	0;
	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;

	int endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
		
	int countEdges	=	0;
	int	maxEdges	=	0;

	for(y=startY; y<endY; y++)
	{
		countEdges = 0;

		for(x=15; x<120; x++)
		{
			pix1	= *(img3Edg + x + LineLength*y);

			if(pix1>th)	{countEdges++;}
		}

		if(countEdges>maxEdges)
		{
			maxEdges	=	countEdges;
			result.x	=	(LineLength/2)*factor;
			result.y	=	(y)*factor;
		}
	}

	return result;
}

SpecialPoint CXAxisView::FindWhiteBand_C4(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;
	result.x=result.c=0.00;
	result.y = MainDisplayImageHeight;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	int pix1,pix2,pix3,pix4,pix5,pix6,pix7,pix8,pix9;
	int pix, pixVal;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	// Creating the Sobel image
	int y;
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn + x + 1  + LineLength*(y + 1));

			//pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
			//		abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pix =	abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MIN(pix,255);

			*(img4Edg + x  + LineLength * y) = pixVal;

			//if(pixVal>th)	{*(imgEdg[lookinCam]  + x  + LineLength * y) = 255;}
			//else			{*(imgEdg[lookinCam]  + x  + LineLength * y) = 0;}
		}
	}

	if(debugging)	{SaveBWCustom1(img4Edg ,	LineLength,	Height);}

	/////////////////

	int x;
	//int diff;
	//int xpos, ypos;
	bool atInitLoop	=	false;

	//////////////////

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int th			=	80;

	int	counterV	=	0;
	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;

	int endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
		
	int countEdges	=	0;
	int	maxEdges	=	0;

	for(y=startY; y<endY; y++)
	{
		countEdges = 0;

		for(x=15; x<120; x++)
		{
			pix1	= *(img4Edg + x + LineLength*y);

			if(pix1>th)	{countEdges++;}
		}

		if(countEdges>maxEdges)
		{
			maxEdges	=	countEdges;
			result.x	=	(LineLength/2)*factor;
			result.y	=	(y)*factor;
		}
	}

	return result;
}

SpecialPoint CXAxisView::FindBlackBand_C1(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;
	result.x=result.c=0.00;
	result.y = MainDisplayImageHeight;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	int pix1,pix2,pix3,pix4,pix5,pix6,pix7,pix8,pix9;
	int pix, pixVal;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int y;
	// Creating the Sobel image
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn + x + 1  + LineLength*(y + 1));

			//pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
			//		abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pix =	abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MIN(pix,255);

			*(img1Edg + x  + LineLength * y) = pixVal;

			//if(pixVal>th)	{*(img1Edg  + x  + LineLength * y) = 255;}
			//else			{*(img1Edg  + x  + LineLength * y) = 0;}
		}
	}

	if(debugging)	{SaveBWCustom1(img1Edg ,	LineLength,	Height);}

	/////////////////

	int x;
	//int diff;
	//int xpos, ypos;
	bool atInitLoop	=	false;

	//////////////////

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int th			=	80;

	int	counterV	=	0;
	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;

	int endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
		
	int countEdges	=	0;
	int	maxEdges	=	0;

	for(y=startY; y<endY; y++)
	{
		countEdges = 0;

		for(x=15; x<120; x++)
		{
			pix1	= *(img1Edg + x + LineLength*y);

			if(pix1>th)	{countEdges++;}
		}

		if(countEdges>maxEdges)
		{
			maxEdges	=	countEdges;
			result.x	=	(LineLength/2)*factor;
			result.y	=	(y)*factor;
		}
	}

	return result;
}

SpecialPoint CXAxisView::FindBlackBand_C2(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;
	result.x=result.c=0.00;
	result.y = MainDisplayImageHeight;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	int pix1,pix2,pix3,pix4,pix5,pix6,pix7,pix8,pix9;
	int pix, pixVal;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	// Creating the Sobel image
	int y;
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn + x + 1  + LineLength*(y + 1));

			//pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
			//		abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pix =	abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MIN(pix,255);

			*(img2Edg + x  + LineLength * y) = pixVal;

			//if(pixVal>th)	{*(img2Edg  + x  + LineLength * y) = 255;}
			//else			{*(img2Edg  + x  + LineLength * y) = 0;}
		}
	}

	if(debugging)	{SaveBWCustom1(img2Edg ,	LineLength,	Height);}

	/////////////////

	int x;
	//int diff;
	//int xpos, ypos;
	bool atInitLoop	=	false;

	//////////////////

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int th			=	80;

	int	counterV	=	0;
	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;

	int endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
		
	int countEdges	=	0;
	int	maxEdges	=	0;

	for(y=startY; y<endY; y++)
	{
		countEdges = 0;

		for(x=15; x<120; x++)
		{
			pix1	= *(img2Edg + x + LineLength*y);

			if(pix1>th)	{countEdges++;}
		}

		if(countEdges>maxEdges)
		{
			maxEdges	=	countEdges;
			result.x	=	(LineLength/2)*factor;
			result.y	=	(y)*factor;
		}
	}

	return result;
}

SpecialPoint CXAxisView::FindBlackBand_C3(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;
	result.x=result.c=0.00;
	result.y = MainDisplayImageHeight;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	int pix1,pix2,pix3,pix4,pix5,pix6,pix7,pix8,pix9;
	int pix, pixVal;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	// Creating the Sobel image
	int y;
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn + x + 1  + LineLength*(y + 1));

			//pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
			//		abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pix =	abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MIN(pix,255);

			*(img3Edg + x  + LineLength * y) = pixVal;

			//if(pixVal>th)	{*(imgEdg[lookinCam]  + x  + LineLength * y) = 255;}
			//else			{*(imgEdg[lookinCam]  + x  + LineLength * y) = 0;}
		}
	}

	if(debugging)	{SaveBWCustom1(img3Edg ,	LineLength,	Height);}

	/////////////////

	int x;
	//int diff;
	//int xpos, ypos;
	bool atInitLoop	=	false;

	//////////////////

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int th			=	80;

	int	counterV	=	0;
	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;

	int endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
		
	int countEdges	=	0;
	int	maxEdges	=	0;

	for(y=startY; y<endY; y++)
	{
		countEdges = 0;

		for(x=15; x<120; x++)
		{
			pix1	= *(img3Edg + x + LineLength*y);

			if(pix1>th)	{countEdges++;}
		}

		if(countEdges>maxEdges)
		{
			maxEdges	=	countEdges;
			result.x	=	(LineLength/2)*factor;
			result.y	=	(y)*factor;
		}
	}

	return result;
}

SpecialPoint CXAxisView::FindBlackBand_C4(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;
	result.x=result.c=0.00;
	result.y = MainDisplayImageHeight;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	int pix1,pix2,pix3,pix4,pix5,pix6,pix7,pix8,pix9;
	int pix, pixVal;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	// Creating the Sobel image
	int y;
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn + x + 1  + LineLength*(y + 1));

			//pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
			//		abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pix =	abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MIN(pix,255);

			*(img4Edg + x  + LineLength * y) = pixVal;

			//if(pixVal>th)	{*(imgEdg[lookinCam]  + x  + LineLength * y) = 255;}
			//else			{*(imgEdg[lookinCam]  + x  + LineLength * y) = 0;}
		}
	}

	if(debugging)	{SaveBWCustom1(img4Edg ,	LineLength,	Height);}

	/////////////////

	int x;
	//int diff;
	//int xpos, ypos;
	bool atInitLoop	=	false;

	//////////////////

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int th			=	80;

	int	counterV	=	0;
	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;

	int endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
		
	int countEdges	=	0;
	int	maxEdges	=	0;

	for(y=startY; y<endY; y++)
	{
		countEdges = 0;

		for(x=15; x<120; x++)
		{
			pix1	= *(img4Edg + x + LineLength*y);

			if(pix1>th)	{countEdges++;}
		}

		if(countEdges>maxEdges)
		{
			maxEdges	=	countEdges;
			result.x	=	(LineLength/2)*factor;
			result.y	=	(y)*factor;
		}
	}

	return result;
}

void CXAxisView::verAxisPtsRemOutliers(std::vector<SinglePoint> &v)
{
	bool debugging=false;

	//SinglePoint tmp;

	if(v.size()>0 && debugging)
	{for(int i=0; i<v.size(); i++) {TRACE("%i %i %i\n", i, v[i].x/3, v[i].y/3);}}

	if(v.size()>0)
	{
		std::sort( v.begin(), v.end(), SingPtXAsc() ); 
		
		int medianIndex	= v.size()/2;
		int medianX		= v[medianIndex].x;
		int diff;

		if(debugging)
		{for(int i=0; i<v.size(); i++) {TRACE("%i %i %i\n", i, v[i].x/3, v[i].y/3);}}
	
		
		for(int i=0; i<v.size(); i++)
		{
			int xval = v[i].x;
			diff = abs(xval - medianX);

			if(diff > 10) //10
			{
				v.erase (v.begin()+i);
				i = i - 1;
			}
		}

		//if(debugging)
		//{for(int i=0; i<v.size(); i++) {TRACE("%i %i %i\n", i, result[i].x/3, result[i].y/3);}}

		if(debugging)
		{for(int i=0; i<v.size(); i++) {TRACE("%i %i %i\n", i, v[i].x/3, v[i].y/3);}}

		//if(debugging)	
		//{for(int i=0; i<v.size(); i++)	{TRACE("%i\n", v[i].x);}}
	}

	if(v.size()>0 && debugging)
	{for(int i=0; i<v.size(); i++) {TRACE("%i %i %i\n", i, v[i].x/3, v[i].y/3);}}

	else	{return;}
}



//Calculating linear regression with respect to Y
linearRegData CXAxisView::linearRegY(std::vector<SinglePoint> points)
{
	linearRegData result;

	result.avgx		= 0;
	result.avgy		= 0;
	result.incl		= 0;
	result.angIncRad= 0;
	result.angIncDeg= 0;

	int npoints = points.size();

	bool debugging = false;
		
	if(npoints>0 && debugging)
	{for(int i=0; i<points.size(); i++) {TRACE("%i %i\n", points[i].x, points[i].y);}}

	if(npoints>2)
	{
		long int sumX	= 0;	long int sumY	= 0;
		long int sumXY	= 0;	long int sumYY	= 0;

		float avgX		= 0;	float avgY		= 0;
		float avgXY		= 0;	float avgYY		= 0;
		float incli		= 0;

		SinglePoint tmp;

		for(int i=0; i<npoints; i++)
		{
			tmp = points[i];

			sumX	+= tmp.x;
			sumY	+= tmp.y;
			sumXY	+= tmp.x*tmp.y;
			sumYY	+= tmp.y*tmp.y;
		}
		
		avgX	= float(sumX)/npoints;
		avgY	= float(sumY)/npoints;
		avgXY	= float(sumXY)/npoints;
		avgYY	= float(sumYY)/npoints;

		float denomin = avgYY - avgY*avgY;
		if(denomin<=0)	{denomin = 1;}

		incli = (avgXY - avgX*avgY)/denomin;

		result.avgx		= avgX;
		result.avgy		= avgY;
		result.incl		= incli;
		result.angIncRad= atan(incli);
		result.angIncDeg= (atan(incli))*180/3.1416;
	}

	return result;
}

void CXAxisView::SaveColCustom1(BYTE *im_sr, int width, int height)
{
	//char buf[10];
	char *pFileName = "c:\\silganData\\save\\saved1Col.bmp";//drive
	strcpy(currentpath,pFileName);

	CVisRGBAByteImage image1(width, height, 1,1,im_sr);
	imageCS		=	image1;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imageCS.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		CString errorMsg;
		errorMsg.Format("9The pat file %s cound not be saved.", currentpath);
		AfxMessageBox(errorMsg);
	}// Warn the user.
}

void CXAxisView::SaveBWCustom1(BYTE *im_sr, int width, int height)
{
	CVisByteImage imageS;

	//char buf[10];
	
	char *pFileName = "c:\\silganData\\save\\saved1.bmp";//drive
	strcpy(currentpath,pFileName);
	
	CVisByteImage image(width, height, 1,1,im_sr);
	imageS=image;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpath;
		imageS.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		CString errorMsg;
		errorMsg.Format("9The pat file %s cound not be saved.", currentpath);

		AfxMessageBox(errorMsg);
	}// Warn the user.
}

void CXAxisView::calcNoLabelCoordinates()
{
	int yStart	= theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset;
	int fact	= 3;
	
	for(int i=1; i<=4; i++)
	{
		int Length	= CroppedCameraHeight/fact;
		int Height	= CroppedCameraWidth/fact;
		int xOffset = theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetX[i]/fact; 

		wNoLbl[i]	= theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetW/fact; 
		hNoLbl[i]	= theapp->jobinfo[pframe->CurrentJobNum].noLabelOffsetH/fact; 
		xNoLbl[i]	= MIN(xOffset+30,	Length - wNoLbl[i] - 5);
		yNoLbl[i]	= MIN(yStart,		Height - hNoLbl[i] - 5);
	}
}

void CXAxisView::makeStitchImgSelection()
{
	int stLighImg = theapp->jobinfo[pframe->CurrentJobNum].stLighImg;
	int stDarkImg = theapp->jobinfo[pframe->CurrentJobNum].stDarkImg;
	int stEdgeImg = theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg;

	if(stLighImg==1)		{img_cam1Ligh = im_cambw1;		img_cam2Ligh = im_cambw2;	img_cam3Ligh = im_cambw3;	img_cam4Ligh = im_cambw4;}
	else if(stLighImg==2)	{img_cam1Ligh = img1SatDiv3;	img_cam2Ligh = img2SatDiv3;	img_cam3Ligh = img3SatDiv3;	img_cam4Ligh = img4SatDiv3;}
	else if(stLighImg==3)	{img_cam1Ligh = img1MagDiv3;	img_cam2Ligh = img2MagDiv3;	img_cam3Ligh = img3MagDiv3;	img_cam4Ligh = img4MagDiv3;}
	else if(stLighImg==4)	{img_cam1Ligh = img1YelDiv3;	img_cam2Ligh = img2YelDiv3;	img_cam3Ligh = img3YelDiv3;	img_cam4Ligh = img4YelDiv3;}
	else if(stLighImg==5)	{img_cam1Ligh = img1CyaDiv3;	img_cam2Ligh = img2CyaDiv3;	img_cam3Ligh = img3CyaDiv3;	img_cam4Ligh = img4CyaDiv3;}
	else if(stLighImg==6)	{img_cam1Ligh = im_cam1Rbw;		img_cam2Ligh = im_cam2Rbw;	img_cam3Ligh = im_cam3Rbw;	img_cam4Ligh = im_cam4Rbw;}
	else if(stLighImg==7)	{img_cam1Ligh = im_cam1Gbw;		img_cam2Ligh = im_cam2Gbw;	img_cam3Ligh = im_cam3Gbw;	img_cam4Ligh = im_cam4Gbw;}
	else if(stLighImg==8)	{img_cam1Ligh = im_cam1Bbw;		img_cam2Ligh = im_cam2Bbw;	img_cam3Ligh = im_cam3Bbw;	img_cam4Ligh = im_cam4Bbw;}
	else if(stLighImg==9)	{img_cam1Ligh = img1Sa2Div3;	img_cam2Ligh = img2Sa2Div3;	img_cam3Ligh = img3Sa2Div3;	img_cam4Ligh = img4Sa2Div3;}

	if(stDarkImg==1)		{img_cam1Dark = im_cambw1;		img_cam2Dark = im_cambw2;	img_cam3Dark = im_cambw3;	img_cam4Dark = im_cambw4;}
	else if(stDarkImg==2)	{img_cam1Dark = img1SatDiv3;	img_cam2Dark = img2SatDiv3;	img_cam3Dark = img3SatDiv3;	img_cam4Dark = img4SatDiv3;}
	else if(stDarkImg==3)	{img_cam1Dark = img1MagDiv3;	img_cam2Dark = img2MagDiv3;	img_cam3Dark = img3MagDiv3;	img_cam4Dark = img4MagDiv3;}
	else if(stDarkImg==4)	{img_cam1Dark = img1YelDiv3;	img_cam2Dark = img2YelDiv3;	img_cam3Dark = img3YelDiv3;	img_cam4Dark = img4YelDiv3;}
	else if(stDarkImg==5)	{img_cam1Dark = img1CyaDiv3;	img_cam2Dark = img2CyaDiv3;	img_cam3Dark = img3CyaDiv3;	img_cam4Dark = img4CyaDiv3;}
	else if(stDarkImg==6)	{img_cam1Dark = im_cam1Rbw;		img_cam2Dark = im_cam2Rbw;	img_cam3Dark = im_cam3Rbw;	img_cam4Dark = im_cam4Rbw;}
	else if(stDarkImg==7)	{img_cam1Dark = im_cam1Gbw;		img_cam2Dark = im_cam2Gbw;	img_cam3Dark = im_cam3Gbw;	img_cam4Dark = im_cam4Gbw;}
	else if(stDarkImg==8)	{img_cam1Dark = im_cam1Bbw;		img_cam2Dark = im_cam2Bbw;	img_cam3Dark = im_cam3Bbw;	img_cam4Dark = im_cam4Bbw;}
	else if(stDarkImg==9)	{img_cam1Dark = img1Sa2Div3;	img_cam2Dark = img2Sa2Div3;	img_cam3Dark = img3Sa2Div3;	img_cam4Dark = img4Sa2Div3;}


	if(stEdgeImg==1)		{img_cam1Edge = im_cambw1;		img_cam2Edge = im_cambw2;	img_cam3Edge = im_cambw3;	img_cam4Edge = im_cambw4;}
	else if(stEdgeImg==2)	{img_cam1Edge = img1SatDiv3;	img_cam2Edge = img2SatDiv3;	img_cam3Edge = img3SatDiv3;	img_cam4Edge = img4SatDiv3;}
	else if(stEdgeImg==3)	{img_cam1Edge = img1MagDiv3;	img_cam2Edge = img2MagDiv3;	img_cam3Edge = img3MagDiv3;	img_cam4Edge = img4MagDiv3;}
	else if(stEdgeImg==4)	{img_cam1Edge = img1YelDiv3;	img_cam2Edge = img2YelDiv3;	img_cam3Edge = img3YelDiv3;	img_cam4Edge = img4YelDiv3;}
	else if(stEdgeImg==5)	{img_cam1Edge = img1CyaDiv3;	img_cam2Edge = img2CyaDiv3;	img_cam3Edge = img3CyaDiv3;	img_cam4Edge = img4CyaDiv3;}
	else if(stEdgeImg==6)	{img_cam1Edge = im_cam1Rbw;		img_cam2Edge = im_cam2Rbw;	img_cam3Edge = im_cam3Rbw;	img_cam4Edge = im_cam4Rbw;}
	else if(stEdgeImg==7)	{img_cam1Edge = im_cam1Gbw;		img_cam2Edge = im_cam2Gbw;	img_cam3Edge = im_cam3Gbw;	img_cam4Edge = im_cam4Gbw;}
	else if(stEdgeImg==8)	{img_cam1Edge = im_cam1Bbw;		img_cam2Edge = im_cam2Bbw;	img_cam3Edge = im_cam3Bbw;	img_cam4Edge = im_cam4Bbw;}
	else if(stEdgeImg==9)	{img_cam1Edge = img1Sa2Div3;	img_cam2Edge = img2Sa2Div3;	img_cam3Edge = img3Sa2Div3;	img_cam4Edge = img4Sa2Div3;}

}


void CXAxisView::calculateHistThreshold(int& thl, int& thh)
{
	//Resetting values
	pixCntTotal = 0;
	for(int k=0; k<=255; k++)	{pixValTotal[k]=0;}
	int i;
	for(i=1; i<=4; i++)
	{
		for(int j=0; j<=255; j++)
		{pixValTotal[j] += pixVal[i][j];}
		
		pixCntTotal +=nhisPix[i];
	}

	if(pixCntTotal<=0) {pixCntTotal=1;}

	int thVal05perc = pixCntTotal*05/100;
	int thVal10perc = pixCntTotal*10/100;
	int thVal15perc = pixCntTotal*15/100;
	int thVal25perc = pixCntTotal*25/100;
	int thVal50perc = pixCntTotal*50/100;
	int thVal75perc = pixCntTotal*75/100;
	int thVal85perc = pixCntTotal*85/100;
	int thVal95perc = pixCntTotal*95/100;

	int greyVal05perc = 0;
	int greyVal10perc = 0;
	int greyVal15perc = 0;
	int greyVal25perc = 0;
	int greyVal50perc = 0;
	int greyVal75perc = 0;
	int greyVal85perc = 0;
	int greyVal95perc = 0;
	
	bool th05Found = false;
	bool th10Found = false;
	bool th15Found = false;
	bool th25Found = false;
	bool th50Found = false;
	bool th75Found = false;
	bool th85Found = false;
	bool th95Found = false;

	int sumpix = 0;
	
	for( i=0; i<=255; i++)
	{
		sumpix	+=	pixValTotal[i];

		if(sumpix>=thVal05perc && !th05Found)		{greyVal05perc =	i; th05Found = true;}
		if(sumpix>=thVal10perc && !th10Found)		{greyVal10perc =	i; th10Found = true;}
		if(sumpix>=thVal15perc && !th15Found)		{greyVal15perc =	i; th15Found = true;}
		if(sumpix>=thVal25perc && !th25Found)		{greyVal25perc =	i; th25Found = true;}
		if(sumpix>=thVal50perc && !th50Found)		{greyVal50perc	=	i; th50Found = true;}
		if(sumpix>=thVal75perc && !th75Found)		{greyVal75perc	=	i; th75Found = true;}
		if(sumpix>=thVal85perc && !th85Found)		{greyVal85perc	=	i; th85Found = true;}
		if(sumpix>=thVal95perc && !th95Found)		{greyVal95perc	=	i; th95Found = true;}
	}

	int th				= greyVal05perc;
	int thBrightness	= greyVal95perc;
	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	if(EdgeTransition==0)	{th = greyVal15perc;}	//dark band
	else					{th = greyVal75perc;}	//light band

	thl = th;
	thh = thBrightness;
}

void CXAxisView::edgeImgCreationC1(BYTE *imgIn, int fact, int nCam)
{
	int LineLength	=	MainDisplayImageWidth/fact;
	int Height		=	MainDisplayImageHeight/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn, LineLength, Height);}

	/////////////////

	int pix, pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pixH, pixV, pixValH, pixValV;

	int th = theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thV = th/2;

	// Creating the Sobel image
	for(int y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn + x + 1  + LineLength*(y + 1));

			pixH	= abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) );
			pixV	= abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pix		= pixH - 2*pixV;

			//pixValH = MAX(0, MIN(pixH,255));
			pixValH = MAX(0, MIN(pix,255));
			pixValV = MAX(0, MIN(pixV,255));
			
			*(im_cam1EdgH + x  + LineLength * y) = pixValH;
			*(im_cam1EdgV + x  + LineLength * y) = pixValV;


			pix = 0;

			if(pixValV>th)		{pix=150;}
			if(pixValH>th)		{pix=255;}
			if(pixValV>th && pixValH>th)		{pix=0;}

			*(im_cam1Bin + x  + LineLength * y) = pix;
		}
	}

	if(debugging)	{SaveBWCustom1(im_cam1EdgH, LineLength, Height);}

	//////////////////
}


void CXAxisView::edgeImgCreationC2(BYTE *imgIn, int fact, int nCam)
{
	int LineLength	=	MainDisplayImageWidth/fact;
	int Height		=	MainDisplayImageHeight/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn, LineLength, Height);}

	/////////////////

	int pix, pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pixH, pixV, pixValH, pixValV;

	int th = theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thV = th/2;

	// Creating the Sobel image
	for(int y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn + x + 1  + LineLength*(y + 1));

			pixH	= abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) );
			pixV	= abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pix		= pixH - 2*pixV;

			//pixValH = MAX(0, MIN(pixH,255));
			pixValH = MAX(0, MIN(pix,255));
			pixValV = MAX(0, MIN(pixV,255));
			
			*(im_cam2EdgH + x  + LineLength * y) = pixValH;
			*(im_cam2EdgV + x  + LineLength * y) = pixValV;

			pix = 0;
			
			if(pixValV>th)		{pix=150;}
			if(pixValH>th)		{pix=255;}
			if(pixValV>th && pixValH>th)		{pix=0;}

			*(im_cam2Bin + x  + LineLength * y) = pix;
		}
	}

	if(debugging)	{SaveBWCustom1(im_cam2EdgH, LineLength, Height);}

	//////////////////
}

void CXAxisView::edgeImgCreationC3(BYTE *imgIn, int fact, int nCam)
{
	int LineLength	=	MainDisplayImageWidth/fact;
	int Height		=	MainDisplayImageHeight/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn, LineLength, Height);}

	/////////////////

	int pix, pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pixH, pixV, pixValH, pixValV;

	int th = theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thV = th/2;

	// Creating the Sobel image
	for(int y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn + x + 1  + LineLength*(y + 1));

			pixH	= abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) );
			pixV	= abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pix		= pixH - 2*pixV;

			//pixValH = MAX(0, MIN(pixH,255));
			pixValH = MAX(0, MIN(pix,255));
			pixValV = MAX(0, MIN(pixV,255));
			
			*(im_cam3EdgH + x  + LineLength * y) = pixValH;
			*(im_cam3EdgV + x  + LineLength * y) = pixValV;

			pix = 0;
			
			if(pixValV>th)		{pix=150;}
			if(pixValH>th)		{pix=255;}
			if(pixValV>th && pixValH>th)		{pix=0;}


			*(im_cam3Bin + x  + LineLength * y) = pix;
		}
	}

	if(debugging)	{SaveBWCustom1(im_cam3EdgH, LineLength, Height);}

	//////////////////
}


void CXAxisView::edgeImgCreationC4(BYTE *imgIn, int fact, int nCam)
{
	int LineLength	=	MainDisplayImageWidth/fact;
	int Height		=	MainDisplayImageHeight/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn, LineLength, Height);}

	/////////////////

	int pix, pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pixH, pixV, pixValH, pixValV;

	int th = theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thV = th/2;

	// Creating the Sobel image
	for(int y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn + x + 1  + LineLength*(y + 1));

			pixH	= abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) );
			pixV	= abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pix		= pixH - 2*pixV;

			//pixValH = MAX(0, MIN(pixH,255));
			pixValH = MAX(0, MIN(pix,255));
			pixValV = MAX(0, MIN(pixV,255));
			
			*(im_cam4EdgH + x  + LineLength * y) = pixValH;
			*(im_cam4EdgV + x  + LineLength * y) = pixValV;

			pix = 0;
			
			if(pixValV>th)		{pix=150;}
			if(pixValH>th)		{pix=255;}
			if(pixValV>th && pixValH>th)		{pix=0;}


			*(im_cam4Bin + x  + LineLength * y) = pix;
		}
	}

	if(debugging)	{SaveBWCustom1(im_cam4EdgH, LineLength, Height);}

	//////////////////
}


SpecialPoint CXAxisView::FindLabelEdge_C1yy(BYTE *imgIn, int nCam, int factor)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int pix;
	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	5;
	int endX			=	LineLength - 5;

	int EdgeTransition	=	theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			=	theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x			=	startX*factor;
	result.y			=	startY*factor;

	int area = wTaught*hTaught;

	int y;
	for(y=startY; y<endY; y++)
	{
		cntTransition	=	0; 

		for(int x=startX; x<endX; x++)
		{
			for(int y2=1; y2<=hTaught; y2++)
			{
				pix = *(imgIn + x + LineLength*(y+y2));

				if(pix>thBlock) {cntTransition++;}
			}
		}

		if(cntTransition>maxCount)
		{
			maxCount	=	cntTransition;

			result.x	=	0*factor;
			result.y	=	y*factor;
			result.c	=	100*cntTransition/area;
		}
	}

	avgBlock[nCam] = result.c;

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	
	for(y=startY; y<endY; y++)
	//for(y=result.y/factor; y<result.y/factor + hTaught; y++)
	{
		for(int x=startX; x<endX; x++)
		{
			pix = *(imgIn+ x + LineLength*y);

			if(i<9900)
			{
				if(pix>thBlock)						
				{
					xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
					i++;
				}	
			}
		}
	}

	nPaint[nCam] = i;

	//////////////////////

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C2yy(BYTE *imgIn, int nCam, int factor)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int pix;
	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	5;
	int endX			=	LineLength - 5;

	int EdgeTransition	=	theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			=	theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x			=	startX*factor;
	result.y			=	startY*factor;

	int area = wTaught*hTaught;
	int y;
	for(y=startY; y<endY; y++)
	{
		cntTransition	=	0; 

		for(int x=startX; x<endX; x++)
		{
			for(int y2=1; y2<=hTaught; y2++)
			{
				pix = *(imgIn + x + LineLength*(y+y2));

				if(pix>thBlock) {cntTransition++;}
			}
		}

		if(cntTransition>maxCount)
		{
			maxCount	=	cntTransition;

			result.x	=	0*factor;
			result.y	=	y*factor;
			result.c	=	100*cntTransition/area;
		}
	}

	avgBlock[nCam] = result.c;

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	
	for(y=startY; y<endY; y++)
	//for(y=result.y/factor; y<result.y/factor + hTaught; y++)
	{
		for(int x=startX; x<endX; x++)
		{
			pix = *(imgIn+ x + LineLength*y);

			if(i<9900)
			{
				if(pix>thBlock)						
				{
					xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
					i++;
				}	
			}
		}
	}

	nPaint[nCam] = i;

	//////////////////////

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C3yy(BYTE *imgIn, int nCam, int factor)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int pix;
	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	5;
	int endX			=	LineLength - 5;

	int EdgeTransition	=	theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			=	theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x			=	startX*factor;
	result.y			=	startY*factor;

	int area = wTaught*hTaught;

	int y;
	for(y=startY; y<endY; y++)
	{
		cntTransition	=	0; 

		for(int x=startX; x<endX; x++)
		{
			for(int y2=1; y2<=hTaught; y2++)
			{
				pix = *(imgIn + x + LineLength*(y+y2));

				if(pix>thBlock) {cntTransition++;}
			}
		}

		if(cntTransition>maxCount)
		{
			maxCount	=	cntTransition;

			result.x	=	0*factor;
			result.y	=	y*factor;
			result.c	=	100*cntTransition/area;
		}
	}

	avgBlock[nCam] = result.c;

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	
	for(y=startY; y<endY; y++)
//	for(y=result.y/factor; y<result.y/factor + hTaught; y++)
	{
		for(int x=startX; x<endX; x++)
		{
			pix = *(imgIn+ x + LineLength*y);

			if(i<9900)
			{
				if(pix>thBlock)						
				{
					xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
					i++;
				}	
			}
		}
	}

	nPaint[nCam] = i;

	//////////////////////

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C4yy(BYTE *imgIn, int nCam, int factor)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int pix;
	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	5;
	int endX			=	LineLength - 5;

	int EdgeTransition	=	theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			=	theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x			=	startX*factor;
	result.y			=	startY*factor;

	int area = wTaught*hTaught;

	int y;
	for(y=startY; y<endY; y++)
	{
		cntTransition	=	0; 

		for(int x=startX; x<endX; x++)
		{
			for(int y2=1; y2<=hTaught; y2++)
			{
				pix = *(imgIn + x + LineLength*(y+y2));

				if(pix>thBlock) {cntTransition++;}
			}
		}

		if(cntTransition>maxCount)
		{
			maxCount	=	cntTransition;

			result.x	=	0*factor;
			result.y	=	y*factor;
			result.c	=	100*cntTransition/area;
		}
	}

	avgBlock[nCam] = result.c;

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	
	for(y=startY; y<endY; y++)
	//for(y=result.y/factor; y<result.y/factor + hTaught; y++)
	{
		for(int x=startX; x<endX; x++)
		{
			pix = *(imgIn+ x + LineLength*y);

			if(i<9900)
			{
				if(pix>thBlock)						
				{
					xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
					i++;
				}	
			}
		}
	}

	nPaint[nCam] = i;

	//////////////////////

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C1y2(BYTE *imgIn, BYTE *imgPatt, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2;
	//int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength - wTaught;
	int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
//	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;
	int i;
	//Automatic calculation of Edge Threshold
	for(i=0; i<=255; i++)	{histTransition[nCam][i]=0;}
	histcntTotalTrans[nCam]=0;
	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			cntTransition	=	0; 
			sumpixTop		=	0;
			sumpixBot		=	0;
			
			for(int y2=1; y2<=3; y2++)
			{
				for(int x2=0; x2<wTaught; x2++)
				{
					cntTransition++;
					pix1 = *(imgIn + x + x2 + LineLength*(y-y2));	sumpixTop+=pix1; 
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));	sumpixBot+=pix2; 
				}
			}

			if(cntTransition<=0)	{cntTransition=1;}

			avgTop	=	sumpixTop/cntTransition;
			avgBot	=	sumpixBot/cntTransition;

			if(EdgeTransition==0)	{currTransition = avgTop - avgBot;}	//dark band
			else			{currTransition = avgBot - avgTop;}	//light band

			if(currTransition>=0 && currTransition<=255)
			{
	 			histTransition[nCam][currTransition]++;
				histcntTotalTrans[nCam]++;
			}
		}
	}

	if(histcntTotalTrans[nCam]<=0) {histcntTotalTrans[nCam]=1;}

	int thVal05perc = histcntTotalTrans[nCam]*05/100;
	int thVal10perc = histcntTotalTrans[nCam]*10/100;
	int thVal15perc = histcntTotalTrans[nCam]*15/100;
	int thVal25perc = histcntTotalTrans[nCam]*25/100;
	int thVal50perc = histcntTotalTrans[nCam]*50/100;
	int thVal75perc = histcntTotalTrans[nCam]*75/100;
	int thVal85perc = histcntTotalTrans[nCam]*85/100;
	int thVal95perc = histcntTotalTrans[nCam]*95/100;

	int greyVal05perc = 0; int greyVal10perc = 0; int greyVal15perc = 0;
	int greyVal25perc = 0; int greyVal50perc = 0; int greyVal75perc = 0;
	int greyVal85perc = 0; int greyVal95perc = 0;
	
	bool th05Found = false;	bool th10Found = false;	bool th15Found = false;
	bool th25Found = false;	bool th50Found = false;	bool th75Found = false;
	bool th85Found = false;	bool th95Found = false;

	int sumQtyPix = 0;
	
	for( i=0; i<=255; i++)
	{
		sumQtyPix	+=	histTransition[nCam][i];

		if(sumQtyPix>=thVal05perc && !th05Found)		{greyVal05perc =	i; th05Found = true;}
		if(sumQtyPix>=thVal10perc && !th10Found)		{greyVal10perc =	i; th10Found = true;}
		if(sumQtyPix>=thVal15perc && !th15Found)		{greyVal15perc =	i; th15Found = true;}
		if(sumQtyPix>=thVal25perc && !th25Found)		{greyVal25perc =	i; th25Found = true;}
		if(sumQtyPix>=thVal50perc && !th50Found)		{greyVal50perc	=	i; th50Found = true;}
		if(sumQtyPix>=thVal75perc && !th75Found)		{greyVal75perc	=	i; th75Found = true;}
		if(sumQtyPix>=thVal85perc && !th85Found)		{greyVal85perc	=	i; th85Found = true;}
		if(sumQtyPix>=thVal95perc && !th95Found)		{greyVal95perc	=	i; th95Found = true;}

		TRACE("%i, %i\n", i, histTransition[nCam][i]);
	}

	int thEdge = greyVal75perc;

	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			cntTransition	=	0; 
			sumpixTop		=	0;
			sumpixBot		=	0;
			
			for(int y2=1; y2<=3; y2++)
			{
				for(int x2=0; x2<wTaught; x2++)
				{
					cntTransition++;
					pix1 = *(imgIn + x + x2 + LineLength*(y-y2));	sumpixTop+=pix1; 
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));	sumpixBot+=pix2; 
				}
			}

			if(cntTransition<=0)	{cntTransition=1;}

			avgTop	=	sumpixTop/cntTransition;
			avgBot	=	sumpixBot/cntTransition;

			if(EdgeTransition==0)	{currTransition = avgTop - avgBot;}	//dark band
			else			{currTransition = avgBot - avgTop;}	//light band
			
			if(currTransition>thEdge)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<thBlock){counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>thBlock){counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount)
				{
					maxCount		=	counterBlock;
					edgeBlock[nCam] =	currTransition;
					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	100*counterBlock/area;
				}
			}
		}
	}


	//////////////////////

	i = 0; 
	nPaint[nCam]=0;
	
	if(result.y!=startY*factor)
	{
		for(y=result.y/factor + 1; y<result.y/factor + hTaught - 1; y++)
		{
			for(x=result.x/factor + 1; x<result.x/factor + wTaught - 1; x++)
			{
				pix2 = *(imgIn+ x + LineLength*y);

				if(i<9900)
				{
					if(EdgeTransition==0 && pix2<thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
					else if(EdgeTransition==1 && pix2>thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}

/*
SpecialPoint CXAxisView::FindLabelEdge_C2y2(BYTE *imgIn, BYTE *imgPatt, int nCam, int factor, SpecialPoint refPt)
{
}

SpecialPoint CXAxisView::FindLabelEdge_C3y2(BYTE *imgIn, BYTE *imgPatt, int nCam, int factor, SpecialPoint refPt)
{
}

SpecialPoint CXAxisView::FindLabelEdge_C4y2(BYTE *imgIn, BYTE *imgPatt, int nCam, int factor, SpecialPoint refPt)
{
}
*/

SpecialPoint CXAxisView::FindLabelEdge_C1y3(BYTE *imgIn, BYTE *imgInEdge, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,		LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgInEdge,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2;
	int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength - wTaught;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;

	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			int cntEdges	=	0;

			for(int x2=0; x2<wTaught; x2++)
			{
				int cntV = 0;

				for(int y2=1; y2<=3; y2++)
				{
					pix1 = *(imgIn + x + x2 + LineLength*(y-y2));
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));

					diff = pix1 - pix2;

					if(diff>thEdge)	{cntV++;}
				}

				if(cntV>=2) {cntEdges++;}
			}

			if(cntEdges>30*wTaught/100)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<thBlock){counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>thBlock){counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount)
				{
					maxCount		=	counterBlock;
					edgeBlock[nCam] =	currTransition;
					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	
	if(result.y!=startY*factor)
	{
		for(y=result.y/factor + 1; y<result.y/factor + hTaught - 1; y++)
		{
			for(x=result.x/factor + 1; x<result.x/factor + wTaught - 1; x++)
			{
				pix2 = *(imgIn+ x + LineLength*y);

				if(i<9900)
				{
					if(EdgeTransition==0 && pix2<thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
					else if(EdgeTransition==1 && pix2>thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C2y3(BYTE *imgIn, BYTE *imgInEdge, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,		LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgInEdge,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2;
	int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength - wTaught;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;

	int y;
	for( y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			int cntEdges	=	0;

			for(int x2=0; x2<wTaught; x2++)
			{
				int cntV = 0;

				for(int y2=1; y2<=3; y2++)
				{
					pix1 = *(imgIn + x + x2 + LineLength*(y-y2));
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));

					diff = pix1 - pix2;

					if(diff>thEdge)	{cntV++;}
				}

				if(cntV>=2) {cntEdges++;}
			}

			if(cntEdges>30*wTaught/100)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<thBlock){counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>thBlock){counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount)
				{
					maxCount		=	counterBlock;
					edgeBlock[nCam] =	currTransition;
					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	
	if(result.y!=startY*factor)
	{
		for(y=result.y/factor + 1; y<result.y/factor + hTaught - 1; y++)
		{
			for(x=result.x/factor + 1; x<result.x/factor + wTaught - 1; x++)
			{
				pix2 = *(imgIn+ x + LineLength*y);

				if(i<9900)
				{
					if(EdgeTransition==0 && pix2<thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
					else if(EdgeTransition==1 && pix2>thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C3y3(BYTE *imgIn, BYTE *imgInEdge, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,		LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgInEdge,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2;
	int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength - wTaught;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;
	int maxEdges = 0;

	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			int cntEdges	=	0;

			for(int x2=0; x2<wTaught; x2++)
			{
				int cntV = 0;

				for(int y2=1; y2<=3; y2++)
				{
					pix1 = *(imgIn + x + x2 + LineLength*(y-y2));
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));

					diff = pix1 - pix2;

					if(diff>thEdge)	{cntV++;}
				}

				if(cntV>=2) {cntEdges++;}
			}

			if(cntEdges>30*wTaught/100)
			//if(cntEdges>maxEdges)
			{
				maxEdges = cntEdges;
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<thBlock){counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>thBlock){counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount)
				{
					maxCount		=	counterBlock;
					edgeBlock[nCam] =	currTransition;
					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	
	if(result.y!=startY*factor)
	{
		for(y=result.y/factor + 1; y<result.y/factor + hTaught - 1; y++)
		{
			for(x=result.x/factor + 1; x<result.x/factor + wTaught - 1; x++)
			{
				pix2 = *(imgIn+ x + LineLength*y);

				if(i<9900)
				{
					if(EdgeTransition==0 && pix2<thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
					else if(EdgeTransition==1 && pix2>thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C4y3(BYTE *imgIn, BYTE *imgInEdge, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,		LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgInEdge,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2;
	int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength - wTaught;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;

	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			int cntEdges	=	0;

			for(int x2=0; x2<wTaught; x2++)
			{
				int cntV = 0;

				for(int y2=1; y2<=3; y2++)
				{
					pix1 = *(imgIn + x + x2 + LineLength*(y-y2));
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));

					diff = pix1 - pix2;

					if(diff>thEdge)	{cntV++;}
				}

				if(cntV>=2) {cntEdges++;}
			}

			if(cntEdges>30*wTaught/100)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<thBlock){counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>thBlock){counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount)
				{
					maxCount		=	counterBlock;
					edgeBlock[nCam] =	currTransition;
					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	
	if(result.y!=startY*factor)
	{
		for(y=result.y/factor + 1; y<result.y/factor + hTaught - 1; y++)
		{
			for(x=result.x/factor + 1; x<result.x/factor + wTaught - 1; x++)
			{
				pix2 = *(imgIn+ x + LineLength*y);

				if(i<9900)
				{
					if(EdgeTransition==0 && pix2<thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
					else if(EdgeTransition==1 && pix2>thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
				}
			}
		}

		nPaint[nCam] = i;
	}

	if(nPaint[nCam]<10)
	{
		if(debugging)	{SaveBWCustom1(imgIn,		LineLength,	Height);}
		if(debugging)	{SaveBWCustom1(imgInEdge,	LineLength,	Height);}
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C1y4(BYTE *imgIn, BYTE *imgInEdge, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,		LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgInEdge,	LineLength,	Height);}

	/////////////////

	int x;
	int pix, pix1, pix2;
	//int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength - wTaught;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;

//////////////////////

	//Let's find threshold
	//////////////////////////
	int i, y;
	for(i=0; i<=255; i++)	{pixVal[nCam][i]=0;}
	int ncount=0;

	for(y=startY; (y<endY); y++)
	{
		for(x=20; x<LineLength-20; x++)
		{
			pix = *(imgIn+ x + LineLength*y);

			pixVal[nCam][pix]++;
			ncount++;
		}
	}

	if(ncount<=0) {ncount=1;}

	int thVal05perc = ncount*05/100;
	int thVal10perc = ncount*10/100;
	int thVal15perc = ncount*15/100;
	int thVal25perc = ncount*25/100;
	int thVal50perc = ncount*50/100;
	int thVal75perc = ncount*75/100;
	int thVal85perc = ncount*85/100;
	int thVal95perc = ncount*95/100;

	int greyVal05perc = 0;
	int greyVal10perc = 0;
	int greyVal15perc = 0;
	int greyVal25perc = 0;
	int greyVal50perc = 0;
	int greyVal75perc = 0;
	int greyVal85perc = 0;
	int greyVal95perc = 0;
	
	bool th05Found = false;
	bool th10Found = false;
	bool th15Found = false;
	bool th25Found = false;
	bool th50Found = false;
	bool th75Found = false;
	bool th85Found = false;
	bool th95Found = false;
	
	for( i=0; i<=255; i++)
	{
		sumpix	+=	pixVal[nCam][i];

		if(sumpix>=thVal05perc && !th05Found)		{greyVal05perc =	i; th05Found = true;}
		if(sumpix>=thVal10perc && !th10Found)		{greyVal10perc =	i; th10Found = true;}
		if(sumpix>=thVal15perc && !th15Found)		{greyVal15perc =	i; th15Found = true;}
		if(sumpix>=thVal25perc && !th25Found)		{greyVal25perc =	i; th25Found = true;}
		if(sumpix>=thVal50perc && !th50Found)		{greyVal50perc	=	i; th50Found = true;}
		if(sumpix>=thVal75perc && !th75Found)		{greyVal75perc	=	i; th75Found = true;}
		if(sumpix>=thVal85perc && !th85Found)		{greyVal85perc	=	i; th85Found = true;}
		if(sumpix>=thVal95perc && !th95Found)		{greyVal95perc	=	i; th95Found = true;}
	}

	int th				= greyVal05perc;

thBlock = th;
//////////////////////

	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			int cntEdges	=	0;

			for(int x2=0; x2<wTaught; x2++)
			{
				int cntV = 0;

				for(int y2=1; y2<=3; y2++)
				{
					pix1 = *(imgInEdge + x + x2 + LineLength*(y-y2));
					pix2 = *(imgInEdge + x + x2 + LineLength*(y+y2));

					if(pix1==255 || pix2==255)	{cntV++;}
				}

				if(cntV>=1) {cntEdges++;}
			}

			if(cntEdges>30*wTaught/100)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<thBlock){counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>thBlock){counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount)
				{
					maxCount		=	counterBlock;
					edgeBlock[nCam] =	currTransition;
					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////

	i = 0; 
	nPaint[nCam]=0;
	
	if(result.y!=startY*factor)
	{
		for(y=result.y/factor + 1; y<result.y/factor + hTaught - 1; y++)
		{
			for(x=result.x/factor + 1; x<result.x/factor + wTaught - 1; x++)
			{
				pix2 = *(imgIn+ x + LineLength*y);

				if(i<9900)
				{
					if(EdgeTransition==0 && pix2<thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
					else if(EdgeTransition==1 && pix2>thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C2y4(BYTE *imgIn, BYTE *imgInEdge, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,		LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgInEdge,	LineLength,	Height);}

	/////////////////

	int x;
	int pix, pix1, pix2;
	//int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength - wTaught;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;

//////////////////////

	//Let's find threshold
	//////////////////////////
	int i;
	for(i=0; i<=255; i++)	{pixVal[nCam][i]=0;}
	int ncount=0;

	int y;
	for(y=startY; (y<endY); y++)
	{
		for(x=20; x<LineLength-20; x++)
		{
			pix = *(imgIn+ x + LineLength*y);

			pixVal[nCam][pix]++;
			ncount++;
		}
	}

	if(ncount<=0) {ncount=1;}

	int thVal05perc = ncount*05/100;
	int thVal10perc = ncount*10/100;
	int thVal15perc = ncount*15/100;
	int thVal25perc = ncount*25/100;
	int thVal50perc = ncount*50/100;
	int thVal75perc = ncount*75/100;
	int thVal85perc = ncount*85/100;
	int thVal95perc = ncount*95/100;

	int greyVal05perc = 0;
	int greyVal10perc = 0;
	int greyVal15perc = 0;
	int greyVal25perc = 0;
	int greyVal50perc = 0;
	int greyVal75perc = 0;
	int greyVal85perc = 0;
	int greyVal95perc = 0;
	
	bool th05Found = false;
	bool th10Found = false;
	bool th15Found = false;
	bool th25Found = false;
	bool th50Found = false;
	bool th75Found = false;
	bool th85Found = false;
	bool th95Found = false;
	
	for( i=0; i<=255; i++)
	{
		sumpix	+=	pixVal[nCam][i];

		if(sumpix>=thVal05perc && !th05Found)		{greyVal05perc =	i; th05Found = true;}
		if(sumpix>=thVal10perc && !th10Found)		{greyVal10perc =	i; th10Found = true;}
		if(sumpix>=thVal15perc && !th15Found)		{greyVal15perc =	i; th15Found = true;}
		if(sumpix>=thVal25perc && !th25Found)		{greyVal25perc =	i; th25Found = true;}
		if(sumpix>=thVal50perc && !th50Found)		{greyVal50perc	=	i; th50Found = true;}
		if(sumpix>=thVal75perc && !th75Found)		{greyVal75perc	=	i; th75Found = true;}
		if(sumpix>=thVal85perc && !th85Found)		{greyVal85perc	=	i; th85Found = true;}
		if(sumpix>=thVal95perc && !th95Found)		{greyVal95perc	=	i; th95Found = true;}
	}

	int th				= greyVal05perc;

thBlock = th;
//////////////////////

	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			int cntEdges	=	0;

			for(int x2=0; x2<wTaught; x2++)
			{
				int cntV = 0;

				for(int y2=1; y2<=3; y2++)
				{
					pix1 = *(imgInEdge + x + x2 + LineLength*(y-y2));
					pix2 = *(imgInEdge + x + x2 + LineLength*(y+y2));

					if(pix1==255 || pix2==255)	{cntV++;}
				}

				if(cntV>=1) {cntEdges++;}
			}

			if(cntEdges>30*wTaught/100)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<thBlock){counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>thBlock){counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount)
				{
					maxCount		=	counterBlock;
					edgeBlock[nCam] =	currTransition;
					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////

	i = 0; 
	nPaint[nCam]=0;
	
	if(result.y!=startY*factor)
	{
		for(y=result.y/factor + 1; y<result.y/factor + hTaught - 1; y++)
		{
			for(x=result.x/factor + 1; x<result.x/factor + wTaught - 1; x++)
			{
				pix2 = *(imgIn+ x + LineLength*y);

				if(i<9900)
				{
					if(EdgeTransition==0 && pix2<thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
					else if(EdgeTransition==1 && pix2>thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C3y4(BYTE *imgIn, BYTE *imgInEdge, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,		LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgInEdge,	LineLength,	Height);}

	/////////////////

	int x;
	int pix, pix1, pix2;
	//int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength - wTaught;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

//////////////////////

	//Let's find threshold
	//////////////////////////
	int i;
	for(i=0; i<=255; i++)	{pixVal[nCam][i]=0;}
	int ncount=0;

	int y;
	for(y=startY; (y<endY); y++)
	{
		for(x=20; x<LineLength-20; x++)
		{
			pix = *(imgIn+ x + LineLength*y);

			pixVal[nCam][pix]++;
			ncount++;
		}
	}

	if(ncount<=0) {ncount=1;}

	int thVal05perc = ncount*05/100;
	int thVal10perc = ncount*10/100;
	int thVal15perc = ncount*15/100;
	int thVal25perc = ncount*25/100;
	int thVal50perc = ncount*50/100;
	int thVal75perc = ncount*75/100;
	int thVal85perc = ncount*85/100;
	int thVal95perc = ncount*95/100;

	int greyVal05perc = 0;
	int greyVal10perc = 0;
	int greyVal15perc = 0;
	int greyVal25perc = 0;
	int greyVal50perc = 0;
	int greyVal75perc = 0;
	int greyVal85perc = 0;
	int greyVal95perc = 0;
	
	bool th05Found = false;
	bool th10Found = false;
	bool th15Found = false;
	bool th25Found = false;
	bool th50Found = false;
	bool th75Found = false;
	bool th85Found = false;
	bool th95Found = false;
	
	for( i=0; i<=255; i++)
	{
		sumpix	+=	pixVal[nCam][i];

		if(sumpix>=thVal05perc && !th05Found)		{greyVal05perc =	i; th05Found = true;}
		if(sumpix>=thVal10perc && !th10Found)		{greyVal10perc =	i; th10Found = true;}
		if(sumpix>=thVal15perc && !th15Found)		{greyVal15perc =	i; th15Found = true;}
		if(sumpix>=thVal25perc && !th25Found)		{greyVal25perc =	i; th25Found = true;}
		if(sumpix>=thVal50perc && !th50Found)		{greyVal50perc	=	i; th50Found = true;}
		if(sumpix>=thVal75perc && !th75Found)		{greyVal75perc	=	i; th75Found = true;}
		if(sumpix>=thVal85perc && !th85Found)		{greyVal85perc	=	i; th85Found = true;}
		if(sumpix>=thVal95perc && !th95Found)		{greyVal95perc	=	i; th95Found = true;}
	}

	int th				= greyVal05perc;

thBlock = th;
//////////////////////

	int area = wTaught*hTaught;

	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			int cntEdges	=	0;

			for(int x2=0; x2<wTaught; x2++)
			{
				int cntV = 0;

				for(int y2=1; y2<=3; y2++)
				{
					pix1 = *(imgInEdge + x + x2 + LineLength*(y-y2));
					pix2 = *(imgInEdge + x + x2 + LineLength*(y+y2));

					if(pix1==255 || pix2==255)	{cntV++;}
				}

				if(cntV>=1) {cntEdges++;}
			}

			if(cntEdges>30*wTaught/100)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<thBlock){counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>thBlock){counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount)
				{
					maxCount		=	counterBlock;
					edgeBlock[nCam] =	currTransition;
					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////

	i = 0; 
	nPaint[nCam]=0;
	
	if(result.y!=startY*factor)
	{
		for(y=result.y/factor + 1; y<result.y/factor + hTaught - 1; y++)
		{
			for(x=result.x/factor + 1; x<result.x/factor + wTaught - 1; x++)
			{
				pix2 = *(imgIn+ x + LineLength*y);

				if(i<9900)
				{
					if(EdgeTransition==0 && pix2<thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
					else if(EdgeTransition==1 && pix2>thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C4y4(BYTE *imgIn, BYTE *imgInEdge, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,		LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgInEdge,	LineLength,	Height);}

	/////////////////

	int x;
	int pix, pix1, pix2;
	//int diff;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength - wTaught;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensEdge;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

//////////////////////

	//Let's find threshold
	//////////////////////////
	int i;
	for(i=0; i<=255; i++)	{pixVal[nCam][i]=0;}
	int ncount=0;

	int y;
	for(y=startY; (y<endY); y++)
	{
		for(x=20; x<LineLength-20; x++)
		{
			pix = *(imgIn+ x + LineLength*y);

			pixVal[nCam][pix]++;
			ncount++;
		}
	}

	if(ncount<=0) {ncount=1;}

	int thVal05perc = ncount*05/100;
	int thVal10perc = ncount*10/100;
	int thVal15perc = ncount*15/100;
	int thVal25perc = ncount*25/100;
	int thVal50perc = ncount*50/100;
	int thVal75perc = ncount*75/100;
	int thVal85perc = ncount*85/100;
	int thVal95perc = ncount*95/100;

	int greyVal05perc = 0;
	int greyVal10perc = 0;
	int greyVal15perc = 0;
	int greyVal25perc = 0;
	int greyVal50perc = 0;
	int greyVal75perc = 0;
	int greyVal85perc = 0;
	int greyVal95perc = 0;
	
	bool th05Found = false;
	bool th10Found = false;
	bool th15Found = false;
	bool th25Found = false;
	bool th50Found = false;
	bool th75Found = false;
	bool th85Found = false;
	bool th95Found = false;
	
	for( i=0; i<=255; i++)
	{
		sumpix	+=	pixVal[nCam][i];

		if(sumpix>=thVal05perc && !th05Found)		{greyVal05perc =	i; th05Found = true;}
		if(sumpix>=thVal10perc && !th10Found)		{greyVal10perc =	i; th10Found = true;}
		if(sumpix>=thVal15perc && !th15Found)		{greyVal15perc =	i; th15Found = true;}
		if(sumpix>=thVal25perc && !th25Found)		{greyVal25perc =	i; th25Found = true;}
		if(sumpix>=thVal50perc && !th50Found)		{greyVal50perc	=	i; th50Found = true;}
		if(sumpix>=thVal75perc && !th75Found)		{greyVal75perc	=	i; th75Found = true;}
		if(sumpix>=thVal85perc && !th85Found)		{greyVal85perc	=	i; th85Found = true;}
		if(sumpix>=thVal95perc && !th95Found)		{greyVal95perc	=	i; th95Found = true;}
	}

	int th				= greyVal05perc;

thBlock = th;

//////////////////////

	int area = wTaught*hTaught;

	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			int cntEdges	=	0;

			for(int x2=0; x2<wTaught; x2++)
			{
				int cntV = 0;

				for(int y2=1; y2<=3; y2++)
				{
					pix1 = *(imgInEdge + x + x2 + LineLength*(y-y2));
					pix2 = *(imgInEdge + x + x2 + LineLength*(y+y2));

					if(pix1==255 || pix2==255)	{cntV++;}
				}

				if(cntV>=1) {cntEdges++;}
			}

			if(cntEdges>30*wTaught/100)
			{
				//just keep the one with the most quantity of dark points
				counterBlock	=	0; 
				sumpix			=	0;

				for(int y2=y; (y2<y+hTaught); y2++)
				{
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);

						if(EdgeTransition==0)	//dark band
						{if(pix2<thBlock){counterBlock++; sumpix+=pix2;}	}
						else
						{if(pix2>thBlock){counterBlock++; sumpix+=pix2;}}
					}
				}
				
				if(counterBlock>maxCount)
				{
					maxCount		=	counterBlock;
					edgeBlock[nCam] =	currTransition;
					if(counterBlock!=0) {avgBlock[nCam] = sumpix/counterBlock;}
					
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	100*counterBlock/area;
				}
			}
		}
	}

	//////////////////////

	i = 0; 
	nPaint[nCam]=0;
	
	if(result.y!=startY*factor)
	{
		for(y=result.y/factor + 1; y<result.y/factor + hTaught - 1; y++)
		{
			for(x=result.x/factor + 1; x<result.x/factor + wTaught - 1; x++)
			{
				pix2 = *(imgIn+ x + LineLength*y);

				if(i<9900)
				{
					if(EdgeTransition==0 && pix2<thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
					else if(EdgeTransition==1 && pix2>thBlock)						
					{
						xPaint[nCam][i] = x*factor; yPaint[nCam][i] = y*factor;
						i++;
					}	
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}




SpecialPoint CXAxisView::FindLabelEdge_C1y5(BYTE *imgIn, BYTE *imgInEdge, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,		LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgInEdge,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2, pix3, pix4;
	//int diff, 
	int diff1, diff2;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;
	int cntEdges = 0;
	int	maxcntEdges = 0;

	int y;
	for(y=startY; y<endY; y++)
	{
		cntEdges	=	0;

		for(x=startX; x<endX; x++)
		{
			pix1 = *(imgIn + x + LineLength*(y-1));
			pix2 = *(imgIn + x + LineLength*(y-2));

			for(int y2=1; y2<=hTaught; y2++)
			{
				pix4 = *(imgInEdge + x + LineLength*(y+y2));

				//if(pix4!=150)
				{
					pix3 = *(imgIn + x + LineLength*(y+y2));

					diff1 = pix1 - pix3;
					diff2 = pix2 - pix3;

					if(diff1>thEdge && diff2>thEdge)	{cntEdges++;}
				}
			}
		}

		if(cntEdges>maxcntEdges)
		{
			//just keep the one with the most quantity of dark points
			maxcntEdges	=	cntEdges; 
			sumpix			=	0;
			
				
			result.x	=	x*factor;
			result.y	=	y*factor;
			result.c	=	100*counterBlock/area;
		}
	}

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	cntEdges = 0;

	if(result.y!=startY*factor)
	{
		y = result.y/factor;

		for(x=startX; x<endX; x++)
		{

  			pix1 = *(imgIn + x + LineLength*(y-1));
			pix2 = *(imgIn + x + LineLength*(y-2));

			for(int y2=1; y2<=hTaught; y2++)
			{
				pix3 = *(imgIn + x + LineLength*(y+y2));

				diff1 = pix1 - pix3;
				diff2 = pix2 - pix3;

				if(diff1>thEdge && diff2>thEdge)
				{
					cntEdges++;
					xPaint[nCam][i] = x*factor; yPaint[nCam][i] = (y+y2)*factor;
					i++;
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C2y5(BYTE *imgIn, BYTE *imgInEdge, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,		LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgInEdge,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2, pix3, pix4;
	//int diff, 
	int diff1, diff2;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;
	int cntEdges = 0;
	int	maxcntEdges = 0;
	int y;
	for(y=startY; y<endY; y++)
	{
		cntEdges	=	0;

		for(x=startX; x<endX; x++)
		{
			pix1 = *(imgIn + x + LineLength*(y-1));
			pix2 = *(imgIn + x + LineLength*(y-2));

			for(int y2=1; y2<=hTaught; y2++)
			{
				pix4 = *(imgInEdge + x + LineLength*(y+y2));

				//if(pix4!=150)
				{
					pix3 = *(imgIn + x + LineLength*(y+y2));

					diff1 = pix1 - pix3;
					diff2 = pix2 - pix3;

					if(diff1>thEdge && diff2>thEdge)	{cntEdges++;}
				}
			}
		}

		if(cntEdges>maxcntEdges)
		{
			//just keep the one with the most quantity of dark points
			maxcntEdges	=	cntEdges; 
			sumpix			=	0;
			
				
			result.x	=	x*factor;
			result.y	=	y*factor;
			result.c	=	100*counterBlock/area;
		}
	}

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	cntEdges = 0;

	if(result.y!=startY*factor)
	{
		y = result.y/factor;

		for(x=startX; x<endX; x++)
		{

  			pix1 = *(imgIn + x + LineLength*(y-1));
			pix2 = *(imgIn + x + LineLength*(y-2));

			for(int y2=1; y2<=hTaught; y2++)
			{
				pix3 = *(imgIn + x + LineLength*(y+y2));

				diff1 = pix1 - pix3;
				diff2 = pix2 - pix3;

				if(diff1>thEdge && diff2>thEdge)
				{
					cntEdges++;
					xPaint[nCam][i] = x*factor; yPaint[nCam][i] = (y+y2)*factor;
					i++;
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C3y5(BYTE *imgIn, BYTE *imgInEdge, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,		LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgInEdge,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2, pix3, pix4;
	//int diff, 
	int diff1, diff2;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;
	int cntEdges = 0;
	int	maxcntEdges = 0;

	int y;
	for(y=startY; y<endY; y++)
	{
		cntEdges	=	0;

		for(x=startX; x<endX; x++)
		{
			pix1 = *(imgIn + x + LineLength*(y-1));
			pix2 = *(imgIn + x + LineLength*(y-2));

			for(int y2=1; y2<=hTaught; y2++)
			{
				pix4 = *(imgInEdge + x + LineLength*(y+y2));

				//if(pix4!=150)
				{
					pix3 = *(imgIn + x + LineLength*(y+y2));

					diff1 = pix1 - pix3;
					diff2 = pix2 - pix3;

					if(diff1>thEdge && diff2>thEdge)	{cntEdges++;}
				}
			}
		}

		if(cntEdges>maxcntEdges)
		{
			//just keep the one with the most quantity of dark points
			maxcntEdges	=	cntEdges; 
			sumpix			=	0;
			
				
			result.x	=	x*factor;
			result.y	=	y*factor;
			result.c	=	100*counterBlock/area;
		}
	}

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	cntEdges = 0;

	if(result.y!=startY*factor)
	{
		y = result.y/factor;

		for(x=startX; x<endX; x++)
		{

  			pix1 = *(imgIn + x + LineLength*(y-1));
			pix2 = *(imgIn + x + LineLength*(y-2));

			for(int y2=1; y2<=hTaught; y2++)
			{
				pix3 = *(imgIn + x + LineLength*(y+y2));

				diff1 = pix1 - pix3;
				diff2 = pix2 - pix3;

				if(diff1>thEdge && diff2>thEdge)
				{
					cntEdges++;
					xPaint[nCam][i] = x*factor; yPaint[nCam][i] = (y+y2)*factor;
					i++;
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C4y5(BYTE *imgIn, BYTE *imgInEdge, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,		LineLength,	Height);}
	if(debugging)	{SaveBWCustom1(imgInEdge,	LineLength,	Height);}

	/////////////////

	int x;
	//int pix, 
	int pix1, pix2, pix3, pix4;
	//int diff, 
	int diff1, diff2;

	//////////////////

	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	//////////////////////////

	int qtyInBlock		=	0;
	int cntTransition	=	0;
	int	maxTransition	=	0;
	int currTransition	=	0;
	int startX			=	0;
	int endX			=	LineLength;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	int EdgeTransition	= theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;
	int thEdge			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int thBlock			= theapp->jobinfo[pframe->CurrentJobNum].sensMet3;

	result.x	=	startX*factor;
	result.y	=	startY*factor;

	int area = wTaught*hTaught;
	int cntEdges = 0;
	int	maxcntEdges = 0;

	int y;
	for(y=startY; y<endY; y++)
	{
		cntEdges	=	0;

		for(x=startX; x<endX; x++)
		{
			pix1 = *(imgIn + x + LineLength*(y-1));
			pix2 = *(imgIn + x + LineLength*(y-2));

			for(int y2=1; y2<=hTaught; y2++)
			{
				pix4 = *(imgInEdge + x + LineLength*(y+y2));

				//if(pix4!=150)
				{
					pix3 = *(imgIn + x + LineLength*(y+y2));

					diff1 = pix1 - pix3;
					diff2 = pix2 - pix3;

					if(diff1>thEdge && diff2>thEdge)	{cntEdges++;}
				}
			}
		}

		if(cntEdges>maxcntEdges)
		{
			//just keep the one with the most quantity of dark points
			maxcntEdges	=	cntEdges; 
			sumpix			=	0;
			
				
			result.x	=	x*factor;
			result.y	=	y*factor;
			result.c	=	100*counterBlock/area;
		}
	}

	//////////////////////

	int i = 0; 
	nPaint[nCam]=0;
	cntEdges = 0;

	if(result.y!=startY*factor)
	{
		y = result.y/factor;

		for(x=startX; x<endX; x++)
		{

  			pix1 = *(imgIn + x + LineLength*(y-1));
			pix2 = *(imgIn + x + LineLength*(y-2));

			for(int y2=1; y2<=hTaught; y2++)
			{
				pix3 = *(imgIn + x + LineLength*(y+y2));

				diff1 = pix1 - pix3;
				diff2 = pix2 - pix3;

				if(diff1>thEdge && diff2>thEdge)
				{
					cntEdges++;
					xPaint[nCam][i] = x*factor; yPaint[nCam][i] = (y+y2)*factor;
					i++;
				}
			}
		}

		nPaint[nCam] = i;
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C1y6(BYTE *imgIn, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	/////////////////

	int x;
	//int pix, pix1, 
	int pix2;
	int diff;

	//////////////////

	
	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int startX		=	10;
	int endX		=	LineLength - wTaught-10;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	int minAvg = 255;

	//////////////////////////

	int th = theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			counterBlock	=	0; 
			sumpix			=	0;

			//**** Second look for a consistent block
			for(int y2=0; y2<hTaught; y2++)
			{
				for(int x2=0; x2<wTaught; x2++)
				{
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));

					counterBlock++; 
					sumpix+=pix2;
				}
			}
			
			if(counterBlock>0) {avgBlock[nCam] = sumpix/counterBlock;}

			if(avgBlock[nCam]<minAvg)
			{
				minAvg	=	avgBlock[nCam];

				result.x	=	x*factor;
				result.y	=	y*factor;
			}
		}
	}

	////////////

	endY = (result.y/factor) + hTaught/2;

	startX = result.x/factor;
	endX = result.x/factor + wTaught ;
	

	int	pixTop;
	int pixBot;

	int cntTransition	=	0;
	int	maxTransition	=	0;
	
	int currTransition	=	0;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	th = theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int cntV = 0;

	for(y=endY; y>startY; y--)
	{
		int cntEdges	=	0; 
		
		for(x=startX; x<endX; x++)
		{
			cntV = 0;

			for(int y2=1; y2<=3; y2++)
			{
				pixTop = *(imgIn+ x + LineLength*(y-y2));
				pixBot = *(imgIn+ x + LineLength*(y+y2));

				diff = pixTop - pixBot;
				
				if(diff>th)
				{cntV++;}
			}
			
			if(cntV>2)
			{cntEdges++;}
		}

		if(cntEdges>30*wTaught/100)
		{
			result.y	=	y*factor;
			result.c	=	cntEdges;
			break;
		}
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C2y6(BYTE *imgIn, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	/////////////////

	int x;
	//int pix, pix1, 
	int pix2;
	int diff;

	//////////////////

	
	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int startX		=	10;
	int endX		=	LineLength - wTaught-10;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	int minAvg = 255;

	//////////////////////////

	int th = theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			counterBlock	=	0; 
			sumpix			=	0;

			//**** Second look for a consistent block
			for(int y2=0; y2<hTaught; y2++)
			{
				for(int x2=0; x2<wTaught; x2++)
				{
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));

					counterBlock++; 
					sumpix+=pix2;
				}
			}
			
			if(counterBlock>0) {avgBlock[nCam] = sumpix/counterBlock;}

			if(avgBlock[nCam]<minAvg)
			{
				minAvg	=	avgBlock[nCam];

				result.x	=	x*factor;
				result.y	=	y*factor;
			}
		}
	}

	////////////

	endY = (result.y/factor) + hTaught/2;

	startX = result.x/factor;
	endX = result.x/factor + wTaught ;
	

	int	pixTop;
	int pixBot;

	int cntTransition	=	0;
	int	maxTransition	=	0;
	
	int currTransition	=	0;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	th = theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int cntV = 0;

	for(y=endY; y>startY; y--)
	{
		int cntEdges	=	0; 
		
		for(x=startX; x<endX; x++)
		{
			cntV = 0;

			for(int y2=1; y2<=3; y2++)
			{
				pixTop = *(imgIn+ x + LineLength*(y-y2));
				pixBot = *(imgIn+ x + LineLength*(y+y2));

				diff = pixTop - pixBot;
				
				if(diff>th)
				{cntV++;}
			}
			
			if(cntV>2)
			{cntEdges++;}
		}

		if(cntEdges>30*wTaught/100)
		{
			result.y	=	y*factor;
			result.c	=	cntEdges;
			break;
		}
	}

	return result;
}


SpecialPoint CXAxisView::FindLabelEdge_C3y6(BYTE *imgIn, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	/////////////////

	int x;
	//int pix, pix1, 
	int pix2;
	int diff;

	//////////////////

	
	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int startX		=	10;
	int endX		=	LineLength - wTaught-10;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	int minAvg = 255;

	//////////////////////////

	int th = theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			counterBlock	=	0; 
			sumpix			=	0;

			//**** Second look for a consistent block
			for(int y2=0; y2<hTaught; y2++)
			{
				for(int x2=0; x2<wTaught; x2++)
				{
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));

					counterBlock++; 
					sumpix+=pix2;
				}
			}
			
			if(counterBlock>0) {avgBlock[nCam] = sumpix/counterBlock;}

			if(avgBlock[nCam]<minAvg)
			{
				minAvg	=	avgBlock[nCam];

				result.x	=	x*factor;
				result.y	=	y*factor;
			}
		}
	}

	////////////

	endY = (result.y/factor) + hTaught/2;

	startX = result.x/factor;
	endX = result.x/factor + wTaught ;

	int	pixTop;
	int pixBot;

	int cntTransition	=	0;
	int	maxTransition	=	0;
	
	int currTransition	=	0;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	th = theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int cntV = 0;

	for(y=endY; y>startY; y--)
	{
		int cntEdges	=	0; 
		
		for(x=startX; x<endX; x++)
		{
			cntV = 0;

			for(int y2=1; y2<=3; y2++)
			{
				pixTop = *(imgIn+ x + LineLength*(y-y2));
				pixBot = *(imgIn+ x + LineLength*(y+y2));

				diff = pixTop - pixBot;
				
				if(diff>th)
				{cntV++;}
			}
			
			if(cntV>2)
			{cntEdges++;}
		}

		if(cntEdges>30*wTaught/100)
		{
			result.y	=	y*factor;
			result.c	=	cntEdges;
			break;
		}
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C4y6(BYTE *imgIn, int nCam, int factor, SpecialPoint refPt)
{
	SpecialPoint result;

	result.x		=	result.c	=	0.00;
	result.y		=	MainDisplayImageHeight;
	avgBlock[nCam]	=	255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	/////////////////

	int x;
	//int pix, pix1
	int pix2;
	int diff;

	//////////////////

	
	int senTaught	=	theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;

	int startX		=	10;
	int endX		=	LineLength - wTaught-10;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	result.y = CroppedCameraWidth;
	int maxCount = 0;
	//int x1, y1;

	int minAvg = 255;

	//////////////////////////

	int th = theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int y;
	for(y=startY; y<endY; y++)
	{
		for(x=startX; x<endX; x++)
		{
			counterBlock	=	0; 
			sumpix			=	0;

			//**** Second look for a consistent block
			for(int y2=0; y2<hTaught; y2++)
			{
				for(int x2=0; x2<wTaught; x2++)
				{
					pix2 = *(imgIn + x + x2 + LineLength*(y+y2));

					counterBlock++; 
					sumpix+=pix2;
				}
			}
			
			if(counterBlock>0) {avgBlock[nCam] = sumpix/counterBlock;}

			if(avgBlock[nCam]<minAvg)
			{
				minAvg	=	avgBlock[nCam];

				result.x	=	x*factor;
				result.y	=	y*factor;
			}
		}
	}

	////////////

	endY = (result.y/factor) + hTaught/2;

	startX = result.x/factor;
	endX = result.x/factor + wTaught ;
	

	int	pixTop;
	int pixBot;

	int cntTransition	=	0;
	int	maxTransition	=	0;
	
	int currTransition	=	0;
	//int sumpixTop, sumpixBot, avgTop, avgBot;

	th = theapp->jobinfo[pframe->CurrentJobNum].sensMet3;
	int cntV = 0;

	for(y=endY; y>startY; y--)
	{
		int cntEdges	=	0; 
		
		for(x=startX; x<endX; x++)
		{
			cntV = 0;

			for(int y2=1; y2<=3; y2++)
			{
				pixTop = *(imgIn+ x + LineLength*(y-y2));
				pixBot = *(imgIn+ x + LineLength*(y+y2));

				diff = pixTop - pixBot;
				
				if(diff>th)
				{cntV++;}
			}
			
			if(cntV>2)
			{cntEdges++;}
		}

		if(cntEdges>30*wTaught/100)
		{
			result.y	=	y*factor;
			result.c	=	cntEdges;
			break;
		}
	}

	return result;
}



void CXAxisView::edgeImgBinC1(BYTE *imgIn1, BYTE *imgIn2, BYTE *imgIn3, int fact, int nCam)
{
	int LineLength	=	MainDisplayImageWidth/fact;
	int Height		=	MainDisplayImageHeight/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn1, LineLength, Height);}

	/////////////////

	int pix, pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pixVal;

	int th = 120;
	int y;
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{*(byteBin1Edges + x + LineLength*y) = 0;}
	}

	if(debugging)	{SaveBWCustom1(byteBin1Edges, LineLength, Height);}

	//////////////////

	// Creating the Sobel image
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn1 + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn1 + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn1 + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn1 + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn1 + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn1 + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn1 + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn1 + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn1 + x + 1  + LineLength*(y + 1));

			pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
					abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MAX(0, MIN(pix,255));
			
			if(pixVal>th)
			{*(byteBin1Edges + x  + LineLength * y) = 255;}
		}
	}

	if(debugging)	{SaveBWCustom1(byteBin1Edges, LineLength, Height);}

	//////////////////

	// Creating the Sobel image
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn2 + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn2 + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn2 + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn2 + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn2 + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn2 + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn2 + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn2 + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn2 + x + 1  + LineLength*(y + 1));

			pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
					abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MAX(0, MIN(pix,255));
			
			if(pixVal>th)
			{*(byteBin1Edges + x  + LineLength * y) = 255;}
		}
	}

	if(debugging)	{SaveBWCustom1(byteBin1Edges, LineLength, Height);}

	//////////////////

	// Creating the Sobel image
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn3 + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn3 + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn3 + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn3 + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn3 + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn3 + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn3 + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn3 + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn3 + x + 1  + LineLength*(y + 1));

			pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
					abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MAX(0, MIN(pix,255));
			
			if(pixVal>th)
			{*(byteBin1Edges + x  + LineLength * y) = 255;}
		}
	}

	if(debugging)	{SaveBWCustom1(byteBin1Edges, LineLength, Height);}

	//////////////////
}


void CXAxisView::edgeImgBinC2(BYTE *imgIn1, BYTE *imgIn2, BYTE *imgIn3, int fact, int nCam)
{
	int LineLength	=	MainDisplayImageWidth/fact;
	int Height		=	MainDisplayImageHeight/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn1, LineLength, Height);}

	/////////////////

	int pix, pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pixVal;

	int th = 120;
	int y;
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{*(byteBin2Edges + x + LineLength*y) = 0;}
	}

	if(debugging)	{SaveBWCustom1(byteBin2Edges, LineLength, Height);}

	//////////////////

	// Creating the Sobel image
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn1 + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn1 + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn1 + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn1 + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn1 + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn1 + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn1 + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn1 + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn1 + x + 1  + LineLength*(y + 1));

			pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
					abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MAX(0, MIN(pix,255));
			
			if(pixVal>th)
			{*(byteBin2Edges + x  + LineLength * y) = 255;}
		}
	}

	if(debugging)	{SaveBWCustom1(byteBin2Edges, LineLength, Height);}

	//////////////////

	// Creating the Sobel image
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn2 + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn2 + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn2 + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn2 + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn2 + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn2 + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn2 + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn2 + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn2 + x + 1  + LineLength*(y + 1));

			pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
					abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MAX(0, MIN(pix,255));
			
			if(pixVal>th)
			{*(byteBin2Edges + x  + LineLength * y) = 255;}
		}
	}

	if(debugging)	{SaveBWCustom1(byteBin2Edges, LineLength, Height);}

	//////////////////

	// Creating the Sobel image
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn3 + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn3 + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn3 + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn3 + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn3 + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn3 + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn3 + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn3 + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn3 + x + 1  + LineLength*(y + 1));

			pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
					abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MAX(0, MIN(pix,255));
			
			if(pixVal>th)
			{*(byteBin2Edges + x  + LineLength * y) = 255;}
		}
	}

	if(debugging)	{SaveBWCustom1(byteBin2Edges, LineLength, Height);}

	//////////////////
}


void CXAxisView::edgeImgBinC3(BYTE *imgIn1, BYTE *imgIn2, BYTE *imgIn3, int fact, int nCam)
{
	int LineLength	=	MainDisplayImageWidth/fact;
	int Height		=	MainDisplayImageHeight/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn1, LineLength, Height);}

	/////////////////

	int pix, pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pixVal;

	int th = 120;
	int y;
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{*(byteBin3Edges + x + LineLength*y) = 0;}
	}

	if(debugging)	{SaveBWCustom1(byteBin3Edges, LineLength, Height);}

	//////////////////

	// Creating the Sobel image
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn1 + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn1 + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn1 + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn1 + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn1 + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn1 + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn1 + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn1 + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn1 + x + 1  + LineLength*(y + 1));

			pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
					abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MAX(0, MIN(pix,255));
			
			if(pixVal>th)
			{*(byteBin3Edges + x  + LineLength * y) = 255;}
		}
	}

	if(debugging)	{SaveBWCustom1(byteBin3Edges, LineLength, Height);}

	//////////////////

	// Creating the Sobel image
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn2 + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn2 + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn2 + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn2 + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn2 + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn2 + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn2 + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn2 + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn2 + x + 1  + LineLength*(y + 1));

			pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
					abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MAX(0, MIN(pix,255));
			
			if(pixVal>th)
			{*(byteBin3Edges + x  + LineLength * y) = 255;}
		}
	}

	if(debugging)	{SaveBWCustom1(byteBin3Edges, LineLength, Height);}

	//////////////////

	// Creating the Sobel image
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn3 + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn3 + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn3 + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn3 + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn3 + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn3 + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn3 + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn3 + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn3 + x + 1  + LineLength*(y + 1));

			pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
					abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MAX(0, MIN(pix,255));
			
			if(pixVal>th)
			{*(byteBin3Edges + x  + LineLength * y) = 255;}
		}
	}

	if(debugging)	{SaveBWCustom1(byteBin3Edges, LineLength, Height);}

	//////////////////
}


void CXAxisView::edgeImgBinC4(BYTE *imgIn1, BYTE *imgIn2, BYTE *imgIn3, int fact, int nCam)
{
	int LineLength	=	MainDisplayImageWidth/fact;
	int Height		=	MainDisplayImageHeight/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn1, LineLength, Height);}

	/////////////////

	int pix, pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pixVal;

	int th = 120;
	int y;
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{*(byteBin4Edges+ x + LineLength*y) = 0;}
	}

	if(debugging)	{SaveBWCustom1(byteBin4Edges, LineLength, Height);}

	//////////////////

	// Creating the Sobel image
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn1 + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn1 + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn1 + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn1 + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn1 + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn1 + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn1 + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn1 + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn1 + x + 1  + LineLength*(y + 1));

			pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
					abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MAX(0, MIN(pix,255));
			
			if(pixVal>th)
			{*(byteBin4Edges + x  + LineLength * y) = 255;}
		}
	}

	if(debugging)	{SaveBWCustom1(byteBin4Edges, LineLength, Height);}

	//////////////////

	// Creating the Sobel image
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn2 + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn2 + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn2 + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn2 + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn2 + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn2 + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn2 + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn2 + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn2 + x + 1  + LineLength*(y + 1));

			pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
					abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MAX(0, MIN(pix,255));
			
			if(pixVal>th)
			{*(byteBin4Edges + x  + LineLength * y) = 255;}
		}
	}

	if(debugging)	{SaveBWCustom1(byteBin4Edges, LineLength, Height);}

	//////////////////

	// Creating the Sobel image
	for(y=3; y<Height-3; y++)
	{
		for(int x=3; x<LineLength-3; x++ )
		{
			pix1	= *(imgIn3 + x - 1  + LineLength*(y - 1));
			pix2	= *(imgIn3 + x + 0  + LineLength*(y - 1));
			pix3	= *(imgIn3 + x + 1  + LineLength*(y - 1));

			pix4	= *(imgIn3 + x - 1  + LineLength*(y + 0));
			pix5	= *(imgIn3 + x + 0  + LineLength*(y + 0));
			pix6	= *(imgIn3 + x + 1  + LineLength*(y + 0));

			pix7	= *(imgIn3 + x - 1  + LineLength*(y + 1));
			pix8	= *(imgIn3 + x + 0  + LineLength*(y + 1));
			pix9	= *(imgIn3 + x + 1  + LineLength*(y + 1));

			pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
					abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pixVal = MAX(0, MIN(pix,255));
			
			if(pixVal>th)
			{*(byteBin4Edges + x  + LineLength * y) = 255;}
		}
	}

	if(debugging)	{SaveBWCustom1(byteBin4Edges, LineLength, Height);}

	//////////////////
}


SpecialPoint CXAxisView::checkNoLabel1(int nCam, int fac, BYTE *imgIn, int x0, int y0, int x1, int h0, int w1, int h1)
{
	SpecialPoint result;
	result.x = result.y = result.c = 0;

	int camLength	= CroppedCameraHeight/fac;
	int camHeight	= CroppedCameraWidth/fac;
	int w1Sm		= w1/fac;
	int h1Sm		= h1/fac;
	int area		= w1Sm*h1Sm;
	
	if(area<=0)	{area = 1;}

	int maxDensity = 0;
	int curDensity = 0;
	int pix;

	int xini = MAX(x0/fac,10);	int xend = (x1 - w1)/fac - 10;
	int yini = MAX(y0/fac,10);	int yend = MIN((y0 + h0)/fac, camHeight);
	
	if(xini<0 || xend>camLength || yini<0 || yend>camHeight)	{return result;}

	int xDens= xini;
	int yDens= yini;
	
	result.x	= xini*fac;
	result.y	= yini*fac;
	result.c	= 0;

	bool debugging = false;
	if(debugging)	{SaveBWCustom1(imgIn, camLength, camHeight);}

	//this looks for the area with the most concentration of black points
	//if(pix==0) ... or if the pixel is black then add the concentration points
	//this will be the beginning of Inspection6
	//and it returns the position x, y of what is found
	int y;
	for(int x=xini; x<xend; x++)
	{
		for(y=yini; y<yend; y++)
		{
			if(x==xini)
			{
				curDensity = 0;

				//for each point calculate the concentration point
				for(int m=0; m<w1Sm; m++)//horizontal
				{
					pix = *(imgIn + x + m + y*camLength);
					if(pix==0)	{curDensity++;}
				}
			}
			else
			{
				//for each point calculate the concentration point
				int m = -1;

				pix = *(imgIn + x + m + y*camLength);
				if(pix==0)	{curDensity--;}
				
				m = w1Sm-1;

				pix = *(imgIn + x + m + y*camLength);
				if(pix==0)	{curDensity++;}
			}
		}

		if(curDensity>maxDensity)
		{maxDensity = curDensity; xDens = x*fac; yDens = y*fac;}
	}

	result.x	= xDens;
	result.y	= yDens;
	result.c	= 100*maxDensity/area;

	return result;
}



void CXAxisView::findShiftedEdges7_C1(int fact, int nCam)
{
	int filterSelShifted =	theapp->jobinfo[pframe->CurrentJobNum].filterSelShifted;

	if(filterSelShifted==1)			{img1ForShiftedDiv3 = im_cambw1;}
	else if(filterSelShifted==2)	{img1ForShiftedDiv3 = img1SatDiv3;}
	else if(filterSelShifted==3)	{img1ForShiftedDiv3 = img1MagDiv3;}
	else if(filterSelShifted==4)	{img1ForShiftedDiv3 = img1YelDiv3;}
	else if(filterSelShifted==5)	{img1ForShiftedDiv3 = img1CyaDiv3;}
	else if(filterSelShifted==6)	{img1ForShiftedDiv3 = im_cam1Rbw;}
	else if(filterSelShifted==7)	{img1ForShiftedDiv3 = im_cam1Gbw;}
	else if(filterSelShifted==8)	{img1ForShiftedDiv3 = im_cam1Bbw;}

	/////////////////////////////////////
	xLShiftPts[nCam].clear();
	xRShiftPts[nCam].clear();

	int LineLength	=	CroppedCameraHeight/fact;
	int Height		=	CroppedCameraWidth/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(img1ForShiftedDiv3, LineLength, Height);}

	/////////////////////////////////////

	int x;//; int pix, 
	int pix1, pix2, pix3, pix4, pix5, pix6;//, pix7, pix8, pix9, pixVal;
	
	/////////////////////////////////////
	
	int	yStart	=	theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[nCam]/fact;
	int	yEnd	=	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[nCam]/fact;
	int thEdge	=	theapp->jobinfo[pframe->CurrentJobNum].shiftedLabSen;

	if(yEnd<yStart || yEnd<0 || yStart<5)	{ return; }

	/////////////////////////////////////

	int x0 = theapp->jobinfo[pframe->CurrentJobNum].shiftHorIniLim;
	int x1 = theapp->jobinfo[pframe->CurrentJobNum].shiftHorEndLim;
	int y0 = theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[nCam];
	int h0 =	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[nCam] - 
				theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[nCam];
	int w1 = theapp->jobinfo[pframe->CurrentJobNum].shiftWidth;
	int h1 = h0;

	//First find the area and x,y position with the most amount of black points
	//*****Step 1 is finding area with most number of Black points
	SpecialPoint tmp = checkNoLabel1(nCam, fact, byteBin1Edges, x0, y0, x1, h0, w1, h1);
	xShift[nCam] = tmp.x;

	/////////////////////////////////////

	int thEdge2 = theapp->jobinfo[pframe->CurrentJobNum].EdgeLabSen;
	int diff1, diff2, diff3;

	SinglePoint tmpSinglePtLeft;
	SinglePoint tmpSinglePtRigh;

	tmpSinglePtLeft.x		=	0;
	tmpSinglePtRigh.x		=	0;

	middlShift[nCam] = tmp.x + w1/2 ; //positioning in the middle of the opening, tmp.x is on the left
	if(middlShift[nCam]<0) middlShift[nCam] = w1/2;

	//Find all the possible points to the LEFT
	int y;
	for(y=yStart; y<yEnd; y++)
	{
		for(x=middlShift[nCam]/fact; x>20; x--) //edge will not go under 20
		{
			pix1 = *(img1ForShiftedDiv3 + x + 1 + y*LineLength);
			pix2 = *(img1ForShiftedDiv3 + x - 1 + y*LineLength);
			
			pix3 = *(img1ForShiftedDiv3 + x + 2 + y*LineLength);
			pix4 = *(img1ForShiftedDiv3 + x - 2 + y*LineLength);

			pix5 = *(img1ForShiftedDiv3 + x + 3 + y*LineLength);
			pix6 = *(img1ForShiftedDiv3 + x - 3 + y*LineLength);

			diff1 = pix1 - pix2;
			diff2 = pix3 - pix4;
			diff3 = pix5 - pix6;

			//////////////////////////////////////////////////

			if(	(pix2<thEdge2 && pix4<thEdge2 && pix6<thEdge2) ||
				(diff1>thEdge2 && diff2>thEdge2 && diff3>thEdge2))
			{
				tmpSinglePtLeft.x = x*fact;
				tmpSinglePtLeft.y = y*fact;

				xLShiftPts[nCam].push_back(tmpSinglePtLeft);
				break;
			}
		}
	}

	///////////

	//Find all the possible points to the RIGHT
	for(y=yStart; y<yEnd; y++)
	{
		for(x=middlShift[nCam]/fact; x<LineLength-20; x++) //edge will not go higher than length -20
		{
			pix1 = *(img1ForShiftedDiv3 + x + 1 + y*LineLength);
			pix2 = *(img1ForShiftedDiv3 + x - 1 + y*LineLength);
			
			pix3 = *(img1ForShiftedDiv3 + x + 2 + y*LineLength);
			pix4 = *(img1ForShiftedDiv3 + x - 2 + y*LineLength);

			pix5 = *(img1ForShiftedDiv3 + x + 3 + y*LineLength);
			pix6 = *(img1ForShiftedDiv3 + x - 3 + y*LineLength);

			diff1 = pix2 - pix1;
			diff2 = pix4 - pix3;
			diff3 = pix6 - pix5;

			//////////////////////////////////////////////////

			if( (pix1<thEdge2 && pix3<thEdge2 && pix5<thEdge2) ||
				(diff1>thEdge2 && diff2>thEdge2 && diff3>thEdge2) )
			{
				tmpSinglePtRigh.x = x*fact;
				tmpSinglePtRigh.y = y*fact;

				xRShiftPts[nCam].push_back(tmpSinglePtRigh);
				break;
			}
		}
	}

	int yRedGreen = y0 + h0/2;

	////////////////////////////
	//Remove outliers
	verAxisPtsRemOutliers(xLShiftPts[nCam]);

	//Remove outliers
	verAxisPtsRemOutliers(xRShiftPts[nCam]);

	qtyShiftLeft[nCam]	= xLShiftPts[nCam].size();
	qtyShiftRigh[nCam]	= xRShiftPts[nCam].size();

	////////////////////////////

	//Calculate Skewness
	float incl;

	//Once we have clean set of points calculate linear regression with respect to Y - LEFT	
	linearRegData lineEqLeft = linearRegY(xLShiftPts[nCam]);
	incl = lineEqLeft.incl;	inclinL[nCam] = incl; angIncDegL[nCam] = fabs(lineEqLeft.angIncDeg);

	//Once we have clean set of points calculate linear regression with respect to Y - RIGHT	
	linearRegData lineEqRigh = linearRegY(xRShiftPts[nCam]);
	incl = lineEqRigh.incl;	inclinR[nCam] = incl; angIncDegR[nCam] = fabs(lineEqRigh.angIncDeg);

	xLBorder[nCam] = lineEqLeft.avgx + lineEqLeft.incl*(yRedGreen -	lineEqLeft.avgy);
	xRBorder[nCam] = lineEqRigh.avgx + lineEqRigh.incl*(yRedGreen -	lineEqRigh.avgy);
	middlShiftWPoints[nCam] = (xLBorder[nCam] + xRBorder[nCam])/2;
}

void CXAxisView::findShiftedEdges7_C2(int fact, int nCam)
{
	int filterSelShifted =	theapp->jobinfo[pframe->CurrentJobNum].filterSelShifted;

	if(filterSelShifted==1)			{img2ForShiftedDiv3 = im_cambw2;}
	else if(filterSelShifted==2)	{img2ForShiftedDiv3 = img2SatDiv3;}
	else if(filterSelShifted==3)	{img2ForShiftedDiv3 = img2MagDiv3;}
	else if(filterSelShifted==4)	{img2ForShiftedDiv3 = img2YelDiv3;}
	else if(filterSelShifted==5)	{img2ForShiftedDiv3 = img2CyaDiv3;}
	else if(filterSelShifted==6)	{img2ForShiftedDiv3 = im_cam2Rbw;}
	else if(filterSelShifted==7)	{img2ForShiftedDiv3 = im_cam2Gbw;}
	else if(filterSelShifted==8)	{img2ForShiftedDiv3 = im_cam2Bbw;}

	/////////////////////////////////////
	xLShiftPts[nCam].clear();
	xRShiftPts[nCam].clear();

	int LineLength	=	CroppedCameraHeight/fact;
	int Height		=	CroppedCameraWidth/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(img2ForShiftedDiv3, LineLength, Height);}

	/////////////////////////////////////

	int x; //int pix, 
	int pix1, pix2, pix3, pix4, pix5, pix6;//, pix7, pix8, pix9, pixVal;
	
	/////////////////////////////////////
	
	int	yStart	=	theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[nCam]/fact;
	int	yEnd	=	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[nCam]/fact;
	int thEdge	=	theapp->jobinfo[pframe->CurrentJobNum].shiftedLabSen;

	if(yEnd<yStart || yEnd<0 || yStart<5)	{ return; }

	/////////////////////////////////////

	int x0 = theapp->jobinfo[pframe->CurrentJobNum].shiftHorIniLim;
	int x1 = theapp->jobinfo[pframe->CurrentJobNum].shiftHorEndLim;
	int y0 = theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[nCam];
	int h0 =	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[nCam] - 
				theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[nCam];
	int w1 = theapp->jobinfo[pframe->CurrentJobNum].shiftWidth;
	int h1 = h0;

	SpecialPoint tmp = checkNoLabel2(nCam, fact, byteBin2Edges, x0, y0, x1, h0, w1, h1);
	xShift[nCam] = tmp.x;

	/////////////////////////////////////

	int thEdge2 = theapp->jobinfo[pframe->CurrentJobNum].EdgeLabSen;
	int diff1, diff2, diff3;

	SinglePoint tmpSinglePtLeft;
	SinglePoint tmpSinglePtRigh;

	tmpSinglePtLeft.x		=	0;
	tmpSinglePtRigh.x		=	0;

	middlShift[nCam] = tmp.x + w1/2 ;
	if(middlShift[nCam]<0) middlShift[nCam] = w1/2;

	//Find all the possible points to the LEFT
	int y;
	for(y=yStart; y<yEnd; y++)
	{
		for(x=middlShift[nCam]/fact; x>20; x--)
		{
			pix1 = *(img2ForShiftedDiv3 + x + 1 + y*LineLength);
			pix2 = *(img2ForShiftedDiv3 + x - 1 + y*LineLength);
			
			pix3 = *(img2ForShiftedDiv3 + x + 2 + y*LineLength);
			pix4 = *(img2ForShiftedDiv3 + x - 2 + y*LineLength);

			pix5 = *(img2ForShiftedDiv3 + x + 3 + y*LineLength);
			pix6 = *(img2ForShiftedDiv3 + x - 3 + y*LineLength);

			diff1 = pix1 - pix2;
			diff2 = pix3 - pix4;
			diff3 = pix5 - pix6;

			//////////////////////////////////////////////////

			if(	(pix2<thEdge2 && pix4<thEdge2 && pix6<thEdge2) ||
				(diff1>thEdge2 && diff2>thEdge2 && diff3>thEdge2))
			{
				tmpSinglePtLeft.x = x*fact;
				tmpSinglePtLeft.y = y*fact;

				xLShiftPts[nCam].push_back(tmpSinglePtLeft);
				break;
			}
		}
	}

	///////////

	//Find all the possible points to the RIGHT
	for(y=yStart; y<yEnd; y++)
	{
		for(x=middlShift[nCam]/fact; x<LineLength-20; x++)
		{
			pix1 = *(img2ForShiftedDiv3 + x + 1 + y*LineLength);
			pix2 = *(img2ForShiftedDiv3 + x - 1 + y*LineLength);
			
			pix3 = *(img2ForShiftedDiv3 + x + 2 + y*LineLength);
			pix4 = *(img2ForShiftedDiv3 + x - 2 + y*LineLength);

			pix5 = *(img2ForShiftedDiv3 + x + 3 + y*LineLength);
			pix6 = *(img2ForShiftedDiv3 + x - 3 + y*LineLength);

			diff1 = pix2 - pix1;
			diff2 = pix4 - pix3;
			diff3 = pix6 - pix5;

			//////////////////////////////////////////////////

			if( (pix1<thEdge2 && pix3<thEdge2 && pix5<thEdge2) ||
				(diff1>thEdge2 && diff2>thEdge2 && diff3>thEdge2) )
			{
				tmpSinglePtRigh.x = x*fact;
				tmpSinglePtRigh.y = y*fact;

				xRShiftPts[nCam].push_back(tmpSinglePtRigh);
				break;
			}
		}
	}

	////////////////////////////
	//Remove outliers
	verAxisPtsRemOutliers(xLShiftPts[nCam]);

	//Remove outliers
	verAxisPtsRemOutliers(xRShiftPts[nCam]);

	qtyShiftLeft[nCam]	= xLShiftPts[nCam].size();
	qtyShiftRigh[nCam]	= xRShiftPts[nCam].size();

	////////////////////////////

	//Calculate Skewness
	float incl;
	int yRedGreen = y0 + h0/2;

	//Once we have clean set of points calculate linear regression with respect to Y - LEFT	
	linearRegData lineEqLeft = linearRegY(xLShiftPts[nCam]);
	incl = lineEqLeft.incl;	inclinL[nCam] = incl; angIncDegL[nCam] = fabs(lineEqLeft.angIncDeg);

	//Once we have clean set of points calculate linear regression with respect to Y - LEFT	
	linearRegData lineEqRigh = linearRegY(xRShiftPts[nCam]);
	incl = lineEqRigh.incl;	inclinR[nCam] = incl; angIncDegR[nCam] = fabs(lineEqRigh.angIncDeg);

	xLBorder[nCam] = lineEqLeft.avgx + lineEqLeft.incl*(yRedGreen -	lineEqLeft.avgy);
	xRBorder[nCam] = lineEqRigh.avgx + lineEqRigh.incl*(yRedGreen -	lineEqRigh.avgy);
	middlShiftWPoints[nCam] = (xLBorder[nCam] + xRBorder[nCam])/2;
}

void CXAxisView::findShiftedEdges7_C3(int fact, int nCam)
{
	int filterSelShifted =	theapp->jobinfo[pframe->CurrentJobNum].filterSelShifted;

	if(filterSelShifted==1)			{img3ForShiftedDiv3 = im_cambw3;}
	else if(filterSelShifted==2)	{img3ForShiftedDiv3 = img3SatDiv3;}
	else if(filterSelShifted==3)	{img3ForShiftedDiv3 = img3MagDiv3;}
	else if(filterSelShifted==4)	{img3ForShiftedDiv3 = img3YelDiv3;}
	else if(filterSelShifted==5)	{img3ForShiftedDiv3 = img3CyaDiv3;}
	else if(filterSelShifted==6)	{img3ForShiftedDiv3 = im_cam3Rbw;}
	else if(filterSelShifted==7)	{img3ForShiftedDiv3 = im_cam3Gbw;}
	else if(filterSelShifted==8)	{img3ForShiftedDiv3 = im_cam3Bbw;}

	/////////////////////////////////////
	xLShiftPts[nCam].clear();
	xRShiftPts[nCam].clear();

	int LineLength	=	CroppedCameraHeight/fact;
	int Height		=	CroppedCameraWidth/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(img3ForShiftedDiv3, LineLength, Height);}

	/////////////////////////////////////

	int x; //int pix, 
	int pix1, pix2, pix3, pix4, pix5, pix6;//, pix7, pix8, pix9, pixVal;
	
	/////////////////////////////////////
	
	int	yStart	=	theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[nCam]/fact;
	int	yEnd	=	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[nCam]/fact;
	int thEdge	=	theapp->jobinfo[pframe->CurrentJobNum].shiftedLabSen;

	if(yEnd<yStart || yEnd<0 || yStart<5)	{ return; }

	/////////////////////////////////////

	int x0 = theapp->jobinfo[pframe->CurrentJobNum].shiftHorIniLim;
	int x1 = theapp->jobinfo[pframe->CurrentJobNum].shiftHorEndLim;
	int y0 = theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[nCam];
	int h0 =	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[nCam] - 
				theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[nCam];
	int w1 = theapp->jobinfo[pframe->CurrentJobNum].shiftWidth;
	int h1 = h0;

	SpecialPoint tmp = checkNoLabel3(nCam, fact, byteBin3Edges, x0, y0, x1, h0, w1, h1);
	xShift[nCam] = tmp.x;

	/////////////////////////////////////

	int thEdge2 = theapp->jobinfo[pframe->CurrentJobNum].EdgeLabSen;
	int diff1, diff2, diff3;

	SinglePoint tmpSinglePtLeft;
	SinglePoint tmpSinglePtRigh;

	tmpSinglePtLeft.x		=	0;
	tmpSinglePtRigh.x		=	0;

	middlShift[nCam] = tmp.x + w1/2 ;
	if(middlShift[nCam]<0) middlShift[nCam] = w1/2;

	//Find all the possible points to the LEFT
	int y;
	for(y=yStart; y<yEnd; y++)
	{
		for(x=middlShift[nCam]/fact; x>20; x--)
		{
			pix1 = *(img3ForShiftedDiv3 + x + 1 + y*LineLength);
			pix2 = *(img3ForShiftedDiv3 + x - 1 + y*LineLength);
			
			pix3 = *(img3ForShiftedDiv3 + x + 2 + y*LineLength);
			pix4 = *(img3ForShiftedDiv3 + x - 2 + y*LineLength);

			pix5 = *(img3ForShiftedDiv3 + x + 3 + y*LineLength);
			pix6 = *(img3ForShiftedDiv3 + x - 3 + y*LineLength);

			diff1 = pix1 - pix2;
			diff2 = pix3 - pix4;
			diff3 = pix5 - pix6;

			//////////////////////////////////////////////////

			if(	(pix2<thEdge2 && pix4<thEdge2 && pix6<thEdge2) ||
				(diff1>thEdge2 && diff2>thEdge2 && diff3>thEdge2))
			{
				tmpSinglePtLeft.x = x*fact;
				tmpSinglePtLeft.y = y*fact;

				xLShiftPts[nCam].push_back(tmpSinglePtLeft);
				break;
			}
		}
	}

	///////////

	//Find all the possible points to the RIGHT
	for(y=yStart; y<yEnd; y++)
	{
		for(x=middlShift[nCam]/fact; x<LineLength-20; x++)
		{
			pix1 = *(img3ForShiftedDiv3 + x + 1 + y*LineLength);
			pix2 = *(img3ForShiftedDiv3 + x - 1 + y*LineLength);
			
			pix3 = *(img3ForShiftedDiv3 + x + 2 + y*LineLength);
			pix4 = *(img3ForShiftedDiv3 + x - 2 + y*LineLength);

			pix5 = *(img3ForShiftedDiv3 + x + 3 + y*LineLength);
			pix6 = *(img3ForShiftedDiv3 + x - 3 + y*LineLength);

			diff1 = pix2 - pix1;
			diff2 = pix4 - pix3;
			diff3 = pix6 - pix5;

			//////////////////////////////////////////////////

			if( (pix1<thEdge2 && pix3<thEdge2 && pix5<thEdge2) ||
				(diff1>thEdge2 && diff2>thEdge2 && diff3>thEdge2) )
			{
				tmpSinglePtRigh.x = x*fact;
				tmpSinglePtRigh.y = y*fact;

				xRShiftPts[nCam].push_back(tmpSinglePtRigh);
				break;
			}
		}
	}

	////////////////////////////
	//Remove outliers
	verAxisPtsRemOutliers(xLShiftPts[nCam]);

	//Remove outliers
	verAxisPtsRemOutliers(xRShiftPts[nCam]);

	qtyShiftLeft[nCam]	= xLShiftPts[nCam].size();
	qtyShiftRigh[nCam]	= xRShiftPts[nCam].size();

	////////////////////////////

	//Calculate Skewness
	float incl;
	int yRedGreen = y0 + h0/2;
	
	//Once we have clean set of points calculate linear regression with respect to Y - LEFT	
	linearRegData lineEqLeft = linearRegY(xLShiftPts[nCam]);
	incl = lineEqLeft.incl;	inclinL[nCam] = incl; angIncDegL[nCam] = fabs(lineEqLeft.angIncDeg);

	//Once we have clean set of points calculate linear regression with respect to Y - LEFT	
	linearRegData lineEqRigh = linearRegY(xRShiftPts[nCam]);
	incl = lineEqRigh.incl;	inclinR[nCam] = incl; angIncDegR[nCam] = fabs(lineEqRigh.angIncDeg);

	xLBorder[nCam] = lineEqLeft.avgx + lineEqLeft.incl*(yRedGreen -	lineEqLeft.avgy);
	xRBorder[nCam] = lineEqRigh.avgx + lineEqRigh.incl*(yRedGreen -	lineEqRigh.avgy);
	middlShiftWPoints[nCam] = (xLBorder[nCam] + xRBorder[nCam])/2;
}

void CXAxisView::findShiftedEdges7_C4(int fact, int nCam)
{
	int filterSelShifted =	theapp->jobinfo[pframe->CurrentJobNum].filterSelShifted;

	if(filterSelShifted==1)			{img4ForShiftedDiv3 = im_cambw4;}
	else if(filterSelShifted==2)	{img4ForShiftedDiv3 = img4SatDiv3;}
	else if(filterSelShifted==3)	{img4ForShiftedDiv3 = img4MagDiv3;}
	else if(filterSelShifted==4)	{img4ForShiftedDiv3 = img4YelDiv3;}
	else if(filterSelShifted==5)	{img4ForShiftedDiv3 = img4CyaDiv3;}
	else if(filterSelShifted==6)	{img4ForShiftedDiv3 = im_cam4Rbw;}
	else if(filterSelShifted==7)	{img4ForShiftedDiv3 = im_cam4Gbw;}
	else if(filterSelShifted==8)	{img4ForShiftedDiv3 = im_cam4Bbw;}

	/////////////////////////////////////
	xLShiftPts[nCam].clear();
	xRShiftPts[nCam].clear();

	int LineLength	=	CroppedCameraHeight/fact;
	int Height		=	CroppedCameraWidth/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(img4ForShiftedDiv3, LineLength, Height);}

	/////////////////////////////////////

	int x; //int pix, 
	int pix1, pix2, pix3, pix4, pix5, pix6;//, pix7, pix8, pix9, pixVal;
	
	/////////////////////////////////////
	
	int	yStart	=	theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[nCam]/fact;
	int	yEnd	=	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[nCam]/fact;
	int thEdge	=	theapp->jobinfo[pframe->CurrentJobNum].shiftedLabSen;

	if(yEnd<yStart || yEnd<0 || yStart<5)	{ return; }

	/////////////////////////////////////

	int x0 = theapp->jobinfo[pframe->CurrentJobNum].shiftHorIniLim;
	int x1 = theapp->jobinfo[pframe->CurrentJobNum].shiftHorEndLim;
	int y0 = theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[nCam];
	int h0 =	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[nCam] - 
				theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[nCam];
	int w1 = theapp->jobinfo[pframe->CurrentJobNum].shiftWidth;
	int h1 = h0;

	SpecialPoint tmp = checkNoLabel4(nCam, fact, byteBin4Edges, x0, y0, x1, h0, w1, h1);
	xShift[nCam] = tmp.x;

	/////////////////////////////////////

	int thEdge2 = theapp->jobinfo[pframe->CurrentJobNum].EdgeLabSen;
	int diff1, diff2, diff3;

	SinglePoint tmpSinglePtLeft;
	SinglePoint tmpSinglePtRigh;

	tmpSinglePtLeft.x		=	0;
	tmpSinglePtRigh.x		=	0;

	middlShift[nCam] = tmp.x + w1/2 ;
	if(middlShift[nCam]<0) middlShift[nCam] = w1/2;

	//Find all the possible points to the LEFT
	int y;
	for(y=yStart; y<yEnd; y++)
	{
		for(x=middlShift[nCam]/fact; x>20; x--)
		{
			pix1 = *(img4ForShiftedDiv3 + x + 1 + y*LineLength);
			pix2 = *(img4ForShiftedDiv3 + x - 1 + y*LineLength);
			
			pix3 = *(img4ForShiftedDiv3 + x + 2 + y*LineLength);
			pix4 = *(img4ForShiftedDiv3 + x - 2 + y*LineLength);

			pix5 = *(img4ForShiftedDiv3 + x + 3 + y*LineLength);
			pix6 = *(img4ForShiftedDiv3 + x - 3 + y*LineLength);

			diff1 = pix1 - pix2;
			diff2 = pix3 - pix4;
			diff3 = pix5 - pix6;

			//////////////////////////////////////////////////

			if(	(pix2<thEdge2 && pix4<thEdge2 && pix6<thEdge2) ||
				(diff1>thEdge2 && diff2>thEdge2 && diff3>thEdge2))
			{
				tmpSinglePtLeft.x = x*fact;
				tmpSinglePtLeft.y = y*fact;

				xLShiftPts[nCam].push_back(tmpSinglePtLeft);
				break;
			}
		}
	}

	///////////

	//Find all the possible points to the RIGHT
	for(y=yStart; y<yEnd; y++)
	{
		for(x=middlShift[nCam]/fact; x<LineLength-20; x++)
		{
			pix1 = *(img4ForShiftedDiv3 + x + 1 + y*LineLength);
			pix2 = *(img4ForShiftedDiv3 + x - 1 + y*LineLength);
			
			pix3 = *(img4ForShiftedDiv3 + x + 2 + y*LineLength);
			pix4 = *(img4ForShiftedDiv3 + x - 2 + y*LineLength);

			pix5 = *(img4ForShiftedDiv3 + x + 3 + y*LineLength);
			pix6 = *(img4ForShiftedDiv3 + x - 3 + y*LineLength);

			diff1 = pix2 - pix1;
			diff2 = pix4 - pix3;
			diff3 = pix6 - pix5;

			//////////////////////////////////////////////////

			if( (pix1<thEdge2 && pix3<thEdge2 && pix5<thEdge2) ||
				(diff1>thEdge2 && diff2>thEdge2 && diff3>thEdge2) )
			{
				tmpSinglePtRigh.x = x*fact;
				tmpSinglePtRigh.y = y*fact;

				xRShiftPts[nCam].push_back(tmpSinglePtRigh);
				break;
			}
		}
	}

	////////////////////////////
	//Remove outliers
	verAxisPtsRemOutliers(xLShiftPts[nCam]);

	//Remove outliers
	verAxisPtsRemOutliers(xRShiftPts[nCam]);

	qtyShiftLeft[nCam]	= xLShiftPts[nCam].size();
	qtyShiftRigh[nCam]	= xRShiftPts[nCam].size();

	////////////////////////////

	//Calculate Skewness
	float incl;
	int yRedGreen = y0 + h0/2;
	
	//Once we have clean set of points calculate linear regression with respect to Y - LEFT	
	linearRegData lineEqLeft = linearRegY(xLShiftPts[nCam]);
	incl = lineEqLeft.incl;	inclinL[nCam] = incl; angIncDegL[nCam] = fabs(lineEqLeft.angIncDeg);

	//Once we have clean set of points calculate linear regression with respect to Y - LEFT	
	linearRegData lineEqRigh = linearRegY(xRShiftPts[nCam]);
	incl = lineEqRigh.incl;	inclinR[nCam] = incl; angIncDegR[nCam] = fabs(lineEqRigh.angIncDeg);

	xLBorder[nCam] = lineEqLeft.avgx + lineEqLeft.incl*(yRedGreen -	lineEqLeft.avgy);
	xRBorder[nCam] = lineEqRigh.avgx + lineEqRigh.incl*(yRedGreen -	lineEqRigh.avgy);
	middlShiftWPoints[nCam] = (xLBorder[nCam] + xRBorder[nCam])/2;
}

SpecialPoint CXAxisView::checkNoLabel2(int nCam, int fac, BYTE *imgIn, int x0, int y0, int x1, int h0, int w1, int h1)
{
	SpecialPoint result;
	result.x = result.y = result.c = 0;

	int camLength	= theapp->camHeight[nCam]/fac;
	int camHeight	= theapp->camWidth[nCam]/fac;
	int w1Sm		= w1/fac;
	int h1Sm		= h1/fac;
	int area		= w1Sm*h1Sm;
	
	if(area<=0)	{area = 1;}

	int maxDensity = 0;
	int curDensity = 0;
	int pix;

	int xini = MAX(x0/fac,10);	int xend = (x1 - w1)/fac - 10;
	int yini = MAX(y0/fac,10);	int yend = MIN((y0 + h0)/fac, camHeight);
	
	if(xini<0 || xend>camLength || yini<0 || yend>camHeight)	{return result;}

	int xDens= xini;
	int yDens= yini;
	
	result.x	= xini*fac;
	result.y	= yini*fac;
	result.c	= 0;

	bool debugging = false;
	if(debugging)	{SaveBWCustom1(imgIn, camLength, camHeight);}

	int y;
	for(int x=xini; x<xend; x++)
	{
		for(y=yini; y<yend; y++)
		{
			if(x==xini)
			{
				curDensity = 0;

				//for each point calculate the concentration point
				for(int m=0; m<w1Sm; m++)//horizontal
				{
					pix = *(imgIn + x + m + y*camLength);
					if(theapp->jobinfo[pframe->CurrentJobNum].findWhite)
					if(pix>200)	{curDensity++;}
				}
			}
			else
			{
				//for each point calculate the concentration point
				int m = -1;

				pix = *(imgIn + x + m + y*camLength);
				if(pix==0)	{curDensity--;}
				
				m = w1Sm-1;

				pix = *(imgIn + x + m + y*camLength);
				if(pix==0)	{curDensity++;}
			}
		}

		if(curDensity>maxDensity)
		{maxDensity = curDensity; xDens = x*fac; yDens = y*fac;}
	}

	result.x	= xDens;
	result.y	= yDens;
	result.c	= 100*maxDensity/area;

	return result;
}

SpecialPoint CXAxisView::checkNoLabel3(int nCam, int fac, BYTE *imgIn, int x0, int y0, int x1, int h0, int w1, int h1)
{
	SpecialPoint result;
	result.x = result.y = result.c = 0;

	int camLength	= theapp->camHeight[nCam]/fac;
	int camHeight	= theapp->camWidth[nCam]/fac;
	int w1Sm		= w1/fac;
	int h1Sm		= h1/fac;
	int area		= w1Sm*h1Sm;
	
	if(area<=0)	{area = 1;}

	int maxDensity = 0;
	int curDensity = 0;
	int pix;

	int xini = MAX(x0/fac,10);	int xend = (x1 - w1)/fac - 10;
	int yini = MAX(y0/fac,10);	int yend = MIN((y0 + h0)/fac, camHeight);
	
	if(xini<0 || xend>camLength || yini<0 || yend>camHeight)	{return result;}

	int xDens= xini;
	int yDens= yini;
	
	result.x	= xini*fac;
	result.y	= yini*fac;
	result.c	= 0;

	bool debugging = false;
	if(debugging)	{SaveBWCustom1(imgIn, camLength, camHeight);}
	int y;
	for(int x=xini; x<xend; x++)
	{
		for(y=yini; y<yend; y++)
		{
			if(x==xini)
			{
				curDensity = 0;

				//for each point calculate the concentration point
				for(int m=0; m<w1Sm; m++)//horizontal
				{
					pix = *(imgIn + x + m + y*camLength);
					if(pix==0)	{curDensity++;}
				}
			}
			else
			{
				//for each point calculate the concentration point
				int m = -1;

				pix = *(imgIn + x + m + y*camLength);
				if(pix==0)	{curDensity--;}
				
				m = w1Sm-1;

				pix = *(imgIn + x + m + y*camLength);
				if(pix==0)	{curDensity++;}
			}
		}

		if(curDensity>maxDensity)
		{maxDensity = curDensity; xDens = x*fac; yDens = y*fac;}
	}

	result.x	= xDens;
	result.y	= yDens;
	result.c	= 100*maxDensity/area;

	return result;
}

SpecialPoint CXAxisView::checkNoLabel4(int nCam, int fac, BYTE *imgIn, int x0, int y0, int x1, int h0, int w1, int h1)
{
	SpecialPoint result;
	result.x = result.y = result.c = 0;

	int camLength	= theapp->camHeight[nCam]/fac;
	int camHeight	= theapp->camWidth[nCam]/fac;
	int w1Sm		= w1/fac;
	int h1Sm		= h1/fac;
	int area		= w1Sm*h1Sm;
	
	if(area<=0)	{area = 1;}

	int maxDensity = 0;
	int curDensity = 0;
	int pix;

	int xini = MAX(x0/fac,10);	int xend = (x1 - w1)/fac - 10;
	int yini = MAX(y0/fac,10);	int yend = MIN((y0 + h0)/fac, camHeight);
	
	if(xini<0 || xend>camLength || yini<0 || yend>camHeight)	{return result;}

	int xDens= xini;
	int yDens= yini;
	
	result.x	= xini*fac;
	result.y	= yini*fac;
	result.c	= 0;

	bool debugging = false;
	if(debugging)	{SaveBWCustom1(imgIn, camLength, camHeight);}
	int y;
	for(int x=xini; x<xend; x++)
	{
		for(y=yini; y<yend; y++)
		{
			if(x==xini)
			{
				curDensity = 0;

				//for each point calculate the concentration point
				for(int m=0; m<w1Sm; m++)//horizontal
				{
					pix = *(imgIn + x + m + y*camLength);
					if(pix==0)	{curDensity++;}
				}
			}
			else
			{
				//for each point calculate the concentration point
				int m = -1;

				pix = *(imgIn + x + m + y*camLength);
				if(pix==0)	{curDensity--;}
				
				m = w1Sm-1;

				pix = *(imgIn + x + m + y*camLength);
				if(pix==0)	{curDensity++;}
			}
		}

		if(curDensity>maxDensity)
		{maxDensity = curDensity; xDens = x*fac; yDens = y*fac;}
	}

	result.x	= xDens;
	result.y	= yDens;
	result.c	= 100*maxDensity/area;

	return result;
}

void CXAxisView::SaveTemplateX(BYTE *im_sr2, BYTE *im_sr3, int ncam, int pattsel)
{
	char* pFileName = "c:\\silgandata\\pat\\";//drive
	CString c;

	c =itoa(pframe->CurrentJobNum, buf, 10);
	
	//strcpy(currentpathTxt,pFileName);	strcat(currentpathTxt,c);
	strcpy(currentpathBmp,pFileName);	strcat(currentpathBmp,c);

	//strcpy(currentpathTxt2,pFileName);	strcat(currentpathTxt2,c);
	strcpy(currentpathBmp2,pFileName);	strcat(currentpathBmp2,c);

	//strcpy(currentpathTxt3,pFileName);	strcat(currentpathTxt3,c);
	strcpy(currentpathBmp3,pFileName);	strcat(currentpathBmp3,c);

/*
	if(pattsel==1)
	{
		strcat(currentpathTxt2,"\\imgBy6L.txt");strcat(currentpathBmp2,"\\patBy6L.bmp");
		strcat(currentpathTxt3,"\\imgBy3L.txt");strcat(currentpathBmp3,"\\patBy3L.bmp");
	}
	else if(pattsel==2)
	{
		strcat(currentpathTxt2,"\\imgBy6M.txt");strcat(currentpathBmp2,"\\patBy6M.bmp");
		strcat(currentpathTxt3,"\\imgBy3M.txt");strcat(currentpathBmp3,"\\patBy3M.bmp");
	}
	else if(pattsel==3)
	{
		strcat(currentpathTxt2,"\\imgBy6R.txt");strcat(currentpathBmp2,"\\patBy6R.bmp");
		strcat(currentpathTxt3,"\\imgBy3R.txt");strcat(currentpathBmp3,"\\patBy3R.bmp");
	}
*/
	switch(pattsel)
	{
		case 1:
		{
			strcat(currentpathBmp2,"\\patBy6L.bmp");
			strcat(currentpathBmp3,"\\patBy3L.bmp");
			break;
		}
		case 2:
		{
			strcat(currentpathBmp2,"\\patBy6M.bmp");
			strcat(currentpathBmp3,"\\patBy3M.bmp");
			break;
		}
		case 3:
		{
			strcat(currentpathBmp2,"\\patBy6R.bmp");
			strcat(currentpathBmp3,"\\patBy3R.bmp");
			break;
		}
		default:
		{
			strcat(currentpathBmp2,"\\patBy6.bmp"); //error if at this area
			strcat(currentpathBmp3,"\\patBy3.bmp");
		}
	}

	int szpatt12	=	theapp->sizepattBy12;
	int szpatt6		=	theapp->sizepattBy6;
	int szpatt3W	=	theapp->sizepattBy3_2W;
	int szpatt3H	=	theapp->sizepattBy3_2H;

	//////////////////
/*
	ImageFile.Open(currentpathTxt2, CFile::modeWrite | CFile::modeCreate);
	
	CString szDigit2;	int digit2;

	for(int g=0; g<szpatt6*szpatt6; g++)
	{
		digit2=*(im_sr2+g);
		szDigit2.Format("%3i",digit2);
		ImageFile.Write(szDigit2,szDigit2.GetLength());
	}
	
	ImageFile.Close();
*/
	///////////////////

	CVisByteImage image2(szpatt6,szpatt6,1,-1,im_sr2);
	imagepat2=image2;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpathBmp2;
		imagepat2.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{AfxMessageBox("The pat file could not be saved.");}// Warn the user.

	///////////////////
/*	
	ImageFile.Open(currentpathTxt3, CFile::modeWrite | CFile::modeCreate);
	
	CString szDigit3;	int digit3;

	for(g=0; g<szpatt3W*szpatt3H; g++)
	{
		digit3=*(im_sr3+g);
		szDigit3.Format("%3i",digit3);
		ImageFile.Write(szDigit3,szDigit3.GetLength());
	}
	
	ImageFile.Close();
*/
	///////////////////

	CVisByteImage image3(szpatt3W,szpatt3H,1,-1,im_sr3);
	imagepat3=image3;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpathBmp3;
		imagepat3.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{AfxMessageBox("The pat file could not be saved.");}// Warn the user.

	///////////////////
}

void CXAxisView::LearnPattX(BYTE *imgInby6, BYTE *imgInby3, 
					  int posPattX1, int posPattY1, int cam, int pattsel)
{
	int xpos			=	0;		
	int ypos			=	0;
	int xposTmp			=	0;	
	int yposTmp			=	0;

	int LineLengthTmp	=	theapp->sizepattBy12;
	int szpatt12		=	theapp->sizepattBy12;
	int szpatt6			=	theapp->sizepattBy6;
	int LineLength;
	int LineHeight;

	bool debugging		=	false;

	int x1, y1;

	////////////////////

	LineLength		=	widthBy6;
	LineHeight		=	heightBy6;
	LineLengthTmp	=	theapp->sizepattBy6;

	if(debugging)	{SaveBWCustom1(imgInby6, LineLength, LineHeight);}

	x1	=	posPattX1/6;
	y1	=	posPattY1/6;

	xpos	= 0;		
	ypos	= 0;
	xposTmp	= 0;	
	yposTmp	= 0;

	for(ypos=y1-szpatt6/2; ypos<y1+szpatt6/2; ypos++)
	{
		for(xpos=x1-szpatt6/2;xpos<x1+szpatt6/2;xpos++)
		{	
			if(xpos>0 && xpos<LineLength && ypos>0 && ypos<LineHeight)
			{
				*(bytePattBy6[pattsel] + xposTmp + LineLengthTmp*yposTmp)= *(imgInby6 + xpos + LineLength*ypos);
				xposTmp++;
			}
		}

		xposTmp=0; yposTmp++;
	}

	
	////////////////////

	LineLength		=	widthBy3;
	LineHeight		=	heightBy3;
	int szpatt3W	=	theapp->sizepattBy3_2W;
	int szpatt3H	=	theapp->sizepattBy3_2H;
	LineLengthTmp	=	szpatt3W;

	if(debugging)	{SaveBWCustom1(imgInby3, LineLength, LineHeight);}

	x1	=	posPattX1/3;
	y1	=	posPattY1/3;

	xpos = 0; ypos = 0;	xposTmp	= 0; yposTmp = 0;

	for(ypos=y1-szpatt3H/2; ypos<y1+szpatt3H/2; ypos++)
	{
		for(xpos=x1-szpatt3W/2;xpos<x1+szpatt3W/2;xpos++)
		{	
			if(xpos>0 && xpos<LineLength && ypos>0 && ypos<LineHeight)
			{
				*(bytePattBy3[pattsel] + xposTmp + LineLengthTmp*yposTmp)= *(imgInby3 + xpos + LineLength*ypos);
				xposTmp++;
			}
		}

		xposTmp=0; yposTmp++;
	}

	////////////////////
	
	if(debugging)	{SaveBWCustom1(bytePattBy6[pattsel],	szpatt6, szpatt6);}
	if(debugging)	{SaveBWCustom1(bytePattBy3[pattsel],	szpatt3W, szpatt3H);}

	SaveTemplateX(bytePattBy6[pattsel], bytePattBy3[pattsel], cam, pattsel);

	UpdateData(false);
	InvalidateRect(NULL,false);
}

SpecialPoint CXAxisView::FindLabelEdge_C1b(BYTE *imgIn, int nCam, int factor, SpecialPoint refPt)
{
	avgBlock[nCam]=255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;  //whether Dark-> Light or ViceVersa

	/////////////////

	int x;
	int pix, pix1, pix2;
	int diff;

	//////////////////

	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;  //Height of window used in Method 3
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;  //Width of window used in Method 3
	int th			=	theapp->jobinfo[pframe->CurrentJobNum].sensEdge;         //Sensitivity````

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	SpecialPoint result;
	result.x	=	0;
	result.c	=	255;
	result.y	=	startY*factor;      //ini Result var with default values.

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	int minAvg = 255;
	int avgNow;

	int cntEdges = 0;
	int numEdgeReq = wTaught*3/4;       //Three Fourth of the Width windw shoudl satisfy the threshold criteria

	for(x=15; x<120 - wTaught; x++)
	{
		for(int y=startY; (y<endY); y++)
		{
			foundEdge=false;
			cntEdges = 0;

			//**** First look for an edge
			for(int xW = x; xW<x+wTaught; xW++)   //At a time, going as far as the width window
			{
				pix1	= *(imgIn + xW + LineLength*(y+0));
				pix		= *(imgIn + xW + LineLength*(y+2));

				if(EdgeTransition==0)	{diff = pix1-pix;}  //Based on the type of transition selected.
				else					{diff = pix-pix1;}

				if(diff>th) {cntEdges++;}
			}

			if(cntEdges>numEdgeReq) 
			{
				cntEdges = 0;

				for(int xW = x; xW<x+wTaught; xW++)
				{
					pix1	= *(imgIn + xW + LineLength*(y-2));
					pix		= *(imgIn + xW + LineLength*(y+2));

					if(EdgeTransition==0)	{diff = pix1-pix;}
					else					{diff = pix-pix1;}

					if(diff>th) {cntEdges++;}
				}

				if(cntEdges>numEdgeReq) 
				{foundEdge=true;}
			}
			
			if(foundEdge)
			{
				stopNow			=	false;
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				//for(int y2=y+4; (y2<y+hTaught && stopNow==false); y2++)
				for(int y2=y+4; (y2<y+hTaught); y2++)
				{
					//for(int x2=x; (x2<x+wTaught && stopNow==false); x2++)
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);
						counterBlock++; sumpix+=pix2;
					}
				}
				
				//*** Only go inside if block consistently found
				if(counterBlock!=0) {avgNow = sumpix/counterBlock;}
				else				{avgNow = 255;}

				//if((y*factor)<result.y && avgBlock[lookinCam]<minAvg )
				if(avgNow<minAvg )
				{
					avgBlock[nCam] = avgNow;
					minAvg		=	avgBlock[nCam];
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	avgBlock[nCam];
				}
			}
		}
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C2b(BYTE *imgIn, int nCam, int factor, SpecialPoint refPt)
{
	avgBlock[nCam]=255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	/////////////////

	int x;
	int pix, pix1, pix2;
	int diff;

	//////////////////

	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;
	int th			=	theapp->jobinfo[pframe->CurrentJobNum].sensEdge;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	SpecialPoint result;
	result.x	=	0;
	result.c	=	255;
	result.y	=	startY*factor;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	int minAvg = 255;
	int avgNow;

	int cntEdges = 0;
	int numEdgeReq = wTaught*3/4;

	for(x=15; x<120 - wTaught; x++)
	{
		for(int y=startY; (y<endY); y++)
		{
			foundEdge=false;
			cntEdges = 0;

			//**** First look for an edge
			for(int xW = x; xW<x+wTaught; xW++)
			{
				pix1	= *(imgIn + xW + LineLength*(y+0));
				pix		= *(imgIn + xW + LineLength*(y+2));

				if(EdgeTransition==0)	{diff = pix1-pix;}
				else					{diff = pix-pix1;}

				if(diff>th) {cntEdges++;}
			}

			if(cntEdges>numEdgeReq) 
			{
				cntEdges = 0;

				for(int xW = x; xW<x+wTaught; xW++)
				{
					pix1	= *(imgIn + xW + LineLength*(y-2));
					pix		= *(imgIn + xW + LineLength*(y+2));

					if(EdgeTransition==0)	{diff = pix1-pix;}
					else					{diff = pix-pix1;}

					if(diff>th) {cntEdges++;}
				}

				if(cntEdges>numEdgeReq) 
				{foundEdge=true;}
			}
			
			if(foundEdge)
			{
				stopNow			=	false;
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				//for(int y2=y+4; (y2<y+hTaught && stopNow==false); y2++)
				for(int y2=y+4; (y2<y+hTaught); y2++)
				{
					//for(int x2=x; (x2<x+wTaught && stopNow==false); x2++)
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);
						counterBlock++; sumpix+=pix2;
					}
				}
				
				//*** Only go inside if block consistently found
				if(counterBlock!=0) {avgNow = sumpix/counterBlock;}
				else				{avgNow = 255;}

				//if((y*factor)<result.y && avgBlock[lookinCam]<minAvg )
				if(avgNow<minAvg )
				{
					avgBlock[nCam] = avgNow;
					minAvg		=	avgBlock[nCam];
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	avgBlock[nCam];
				}
			}
		}
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C3b(BYTE *imgIn, int nCam, int factor, SpecialPoint refPt)
{
		avgBlock[nCam]=255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	/////////////////

	int x;
	int pix, pix1, pix2;
	int diff;

	//////////////////

	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;
	int th			=	theapp->jobinfo[pframe->CurrentJobNum].sensEdge;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	SpecialPoint result;
	result.x	=	0;
	result.c	=	255;
	result.y	=	startY*factor;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	int minAvg = 255;
	int avgNow;

	int cntEdges = 0;
	int numEdgeReq = wTaught*3/4;

	for(x=15; x<120 - wTaught; x++)
	{
		for(int y=startY; (y<endY); y++)
		{
			foundEdge=false;
			cntEdges = 0;

			//**** First look for an edge
			for(int xW = x; xW<x+wTaught; xW++)
			{
				pix1	= *(imgIn + xW + LineLength*(y+0));
				pix		= *(imgIn + xW + LineLength*(y+2));

				if(EdgeTransition==0)	{diff = pix1-pix;}
				else					{diff = pix-pix1;}

				if(diff>th) {cntEdges++;}
			}

			if(cntEdges>numEdgeReq) 
			{
				cntEdges = 0;

				for(int xW = x; xW<x+wTaught; xW++)
				{
					pix1	= *(imgIn + xW + LineLength*(y-2));
					pix		= *(imgIn + xW + LineLength*(y+2));

					if(EdgeTransition==0)	{diff = pix1-pix;}
					else					{diff = pix-pix1;}

					if(diff>th) {cntEdges++;}
				}

				if(cntEdges>numEdgeReq) 
				{foundEdge=true;}
			}
			
			if(foundEdge)
			{
				stopNow			=	false;
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				//for(int y2=y+4; (y2<y+hTaught && stopNow==false); y2++)
				for(int y2=y+4; (y2<y+hTaught); y2++)
				{
					//for(int x2=x; (x2<x+wTaught && stopNow==false); x2++)
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);
						counterBlock++; sumpix+=pix2;
					}
				}
				
				//*** Only go inside if block consistently found
				if(counterBlock!=0) {avgNow = sumpix/counterBlock;}
				else				{avgNow = 255;}

				//if((y*factor)<result.y && avgBlock[lookinCam]<minAvg )
				if(avgNow<minAvg )
				{
					avgBlock[nCam] = avgNow;
					minAvg		=	avgBlock[nCam];
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	avgBlock[nCam];
				}
			}
		}
	}

	return result;
}

SpecialPoint CXAxisView::FindLabelEdge_C4b(BYTE *imgIn, int nCam, int factor, SpecialPoint refPt)
{
	avgBlock[nCam]=255;

	int LineLength	=	CroppedCameraHeight/factor;
	int Height		=	CroppedCameraWidth/factor;

	bool debugging	=	false;

	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	int EdgeTransition = theapp->jobinfo[pframe->CurrentJobNum].EdgeTransition;

	/////////////////

	int x;
	int pix, pix1, pix2;
	int diff;

	//////////////////

	int hTaught		=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/factor;
	int wTaught		=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/factor;
	int th			=	theapp->jobinfo[pframe->CurrentJobNum].sensEdge;

	int	startY		=	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
	int	endY		=	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor - hTaught;

	SpecialPoint result;
	result.x	=	0;
	result.c	=	255;
	result.y	=	startY*factor;

	int counterBlock=	0;
	int sumpix		=	0;
	bool foundEdge	=	false;
	bool stopNow	=	false;

	int minAvg = 255;
	int avgNow;

	int cntEdges = 0;
	int numEdgeReq = wTaught*3/4;

	for(x=15; x<120 - wTaught; x++)
	{
		for(int y=startY; (y<endY); y++)
		{
			foundEdge=false;
			cntEdges = 0;

			//**** First look for an edge
			for(int xW = x; xW<x+wTaught; xW++)
			{
				pix1	= *(imgIn + xW + LineLength*(y+0));
				pix		= *(imgIn + xW + LineLength*(y+2));

				if(EdgeTransition==0)	{diff = pix1-pix;}
				else					{diff = pix-pix1;}

				if(diff>th) {cntEdges++;}
			}

			if(cntEdges>numEdgeReq) 
			{
				cntEdges = 0;

				for(int xW = x; xW<x+wTaught; xW++)
				{
					pix1	= *(imgIn + xW + LineLength*(y-2));
					pix		= *(imgIn + xW + LineLength*(y+2));

					if(EdgeTransition==0)	{diff = pix1-pix;}
					else					{diff = pix-pix1;}

					if(diff>th) {cntEdges++;}
				}

				if(cntEdges>numEdgeReq) 
				{foundEdge=true;}
			}
			
			if(foundEdge)
			{
				stopNow			=	false;
				counterBlock	=	0; 
				sumpix			=	0;

				//**** Second look for a consistent block
				//for(int y2=y+4; (y2<y+hTaught && stopNow==false); y2++)
				for(int y2=y+4; (y2<y+hTaught); y2++)
				{
					//for(int x2=x; (x2<x+wTaught && stopNow==false); x2++)
					for(int x2=x; (x2<x+wTaught); x2++)
					{
						pix2 = *(imgIn+ x2 + LineLength*y2);
						counterBlock++; sumpix+=pix2;
					}
				}
				
				//*** Only go inside if block consistently found
				if(counterBlock!=0) {avgNow = sumpix/counterBlock;}
				else				{avgNow = 255;}

				//if((y*factor)<result.y && avgBlock[lookinCam]<minAvg )
				if(avgNow<minAvg )
				{
					avgBlock[nCam] = avgNow;
					minAvg		=	avgBlock[nCam];
					result.x	=	x*factor;
					result.y	=	y*factor;
					result.c	=	avgBlock[nCam];
				}
			}
		}
	}

	return result;
}

void CXAxisView::SaveTempPic(BYTE *im_sr, int res)
{
	CVisRGBAByteImage imageS;
	char buf[100];
	char *pFileName = "c:\\SilganData\\Pat\\";//drive
	char *c = itoa(pframe->CurrentJobNum,buf,10);

	char currentpath[60];
	strcpy(currentpath, pFileName);
	strcat(currentpath,c);

	int width0=theapp->camWidth[1];
	int height0=theapp->camHeight[1];
	
	int width=0;
	int height=0;

	if(res==3)	
	{	
		strcat(currentpath,"\\TaughtPicR3.bmp");
		
		width=height0/3;	height=width0/3;
	}
	if (res == 0)
	{
		strcat(currentpath, "\\check0a.bmp");

		width = height0 / 3;	height = width0 / 3;
	}
	
	CVisRGBAByteImage image(width, height, 1, -1, im_sr);
	imageS=image;

	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel=CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality=0;
		filedescriptorT.filename=currentpath;
		imageS.WriteFile(filedescriptorT);

//		if(res==4)
//		{
//			
//			CVisRGBAByteImage imageQ4(width,height, 1, -1,im_sr);
//			imageQ4.CopyPixelsTo(imageC2Q4); //to show in SetupTab
//		}
	}
	catch(...)
	{
		CString errMsg;
		errMsg.Format("The pat file %s could not be saved", currentpath);
		AfxMessageBox(errMsg);
	}
}

void CXAxisView::method5_CalcRGBvalues(	BYTE *img1R, BYTE *img1G, BYTE *img1B, 
										BYTE *img2R, BYTE *img2G, BYTE *img2B, 
										BYTE *img3R, BYTE *img3G, BYTE *img3B,
										BYTE *img4R, BYTE *img4G, BYTE *img4B, 
										int tX, int tY, int tH,
										int bX, int bY, int bH,
										int W,	int fact,
										int *pRt, int *pGt, int *pBt,	float *pHt,
										int *pRb, int *pGb, int *pBb,	float *pHb)
{
	int tR = 255; int tG = 255; int tB = 255;
	int bR = 255; int bG = 255; int bB = 255;

	int R1, R2, R3, R4;
	int G1, G2, G3, G4;
	int B1, B2, B3, B4;

	/////

	bool debugging = false;

	int newLengthBWDiv3		=	CroppedCameraHeight/fact;
	int newHeightBWDiv3		=	CroppedCameraWidth/fact;

	int xIniT = tX/fact;	int xEndT = (tX + W)/fact;
	int yIniT = tY/fact;	int yEndT = (tY + tH)/fact;

	int xIniB = bX/fact;	int xEndB = (bX + W)/fact;
	int yIniB = bY/fact;	int yEndB = (bY + bH)/fact;

	if(debugging)
	{SaveBWCustom1(img1R,	newLengthBWDiv3, newHeightBWDiv3);}


	//////////
	//We need to double check that we are not out of range....
	//
	//
	//
	//////////
	int tRsum = 0; int tGsum = 0; int tBsum = 0; int tNpts = 0;
	int bRsum = 0; int bGsum = 0; int bBsum = 0; int bNpts = 0;
	int y;
	for(y=yIniT; y<yEndT; y++)
	{
		for(int x=xIniT; x<xEndT; x++)
		{
			R1	= *(img1R + x + newLengthBWDiv3*y);
			R2	= *(img2R + x + newLengthBWDiv3*y);
			R3	= *(img3R + x + newLengthBWDiv3*y);
			R4	= *(img4R + x + newLengthBWDiv3*y);

			G1	= *(img1G + x + newLengthBWDiv3*y);
			G2	= *(img2G + x + newLengthBWDiv3*y);
			G3	= *(img3G + x + newLengthBWDiv3*y);
			G4	= *(img4G + x + newLengthBWDiv3*y);

			B1	= *(img1B + x + newLengthBWDiv3*y);
			B2	= *(img2B + x + newLengthBWDiv3*y);
			B3	= *(img3B + x + newLengthBWDiv3*y);
			B4	= *(img4B + x + newLengthBWDiv3*y);

			tRsum += R1; tRsum += R2; tRsum += R3; tRsum += R4; 
			tGsum += G1; tGsum += G2; tGsum += G3; tGsum += G4; 
			tBsum += B1; tBsum += B2; tBsum += B3; tBsum += B4;

			tNpts+=4;
		}
	}

	int min1, min2, min3, min4;

	for(y=yIniB; y<yEndB; y++)
	{
		for(int x=xIniB; x<xEndB; x++)
		{
			R1	= *(img1R + x + newLengthBWDiv3*y);
			R2	= *(img2R + x + newLengthBWDiv3*y);
			R3	= *(img3R + x + newLengthBWDiv3*y);
			R4	= *(img4R + x + newLengthBWDiv3*y);

			G1	= *(img1G + x + newLengthBWDiv3*y);
			G2	= *(img2G + x + newLengthBWDiv3*y);
			G3	= *(img3G + x + newLengthBWDiv3*y);
			G4	= *(img4G + x + newLengthBWDiv3*y);

			B1	= *(img1B + x + newLengthBWDiv3*y);
			B2	= *(img2B + x + newLengthBWDiv3*y);
			B3	= *(img3B + x + newLengthBWDiv3*y);
			B4	= *(img4B + x + newLengthBWDiv3*y);

			//////////////
			min1 = MIN(R1, G1);
			min1 = MIN(min1, B1);

			min2 = MIN(R2, G2);
			min2 = MIN(min2, B2);

			min3 = MIN(R3, G3);
			min3 = MIN(min3, B3);

			min4 = MIN(R4, G4);
			min4 = MIN(min4, B4);


			//////////////
			bRsum += min1; bRsum += min2; bRsum += min3; bRsum += min4; 
			bGsum += min1; bGsum += min2; bGsum += min3; bGsum += min4; 
			bBsum += min1; bBsum += min2; bBsum += min3; bBsum += min4;

//			bRsum += R1; bRsum += R2; bRsum += R3; bRsum += R4; 
//			bGsum += G1; bGsum += G2; bGsum += G3; bGsum += G4; 
//			bBsum += B1; bBsum += B2; bBsum += B3; bBsum += B4;

			bNpts+=4;
		}
	}

	if(tNpts>0)	{tR = tRsum/tNpts; tG = tGsum/tNpts; tB = tBsum/tNpts;}
	if(bNpts>0)	{bR = bRsum/bNpts; bG = bGsum/bNpts; bB = bBsum/bNpts;}		
	
	//////////////////////////////

	//I am not sure if I should do this for each rgb...
	float tmin=0.0,	tmax=0.0,	tdiff=0.0;
	float tH1=0.0,	tS1=0.0,	tI1=0.0;

	float bmin=0.0,	bmax=0.0,	bdiff=0.0;
	float bH1=0.0,	bS1=0.0,	bI1=0.0;
			
	int hueAng60	= 60;
	int hueAng120	= 120;
	int hueAng240	= 240;
	int hueAng360	= 360;

	//////////////////////////////
	
	if(tB<=tG && tB<=tR)		{tmin = tB;}
	else if(tG<=tB && tG<=tR)	{tmin = tG;}
	else if(tR<=tB && tR<=tG)	{tmin = tR;}

	if(tB>=tG && tB>=tR)		{tmax = tB;}
	else if(tG>=tB && tG>=tR)	{tmax = tG;}
	else if(tR>=tB && tR>=tG)	{tmax = tR;}

	tH1=0; tS1=0; tI1=tmax;

	if(tI1>=0) {tS1=(tI1-tmin)/tI1;}	else {tS1=0.0;}

	if(tS1<=0) {tH1=-1.0;}
	else
	{
		//Compute the hue base on the relative sizes of the RGB components"
		tdiff=tI1 - tmin;
		//is the point within +/-60 degrees of the red axis?
		if(tR==tI1)			{tH1=hueAng60*(tG-tB)/tdiff;}
		//is the point within +/-60 degrees of the green axis?
		else if(tG==tI1)	{tH1=hueAng120+hueAng60*(tB-tR)/tdiff;}
		//is the point within +/-60 degrees of the blue axis?
		else if(tB==tI1)	{tH1=hueAng240+hueAng60*(tR-tG)/tdiff;}

		if(tH1<=0) {tH1=tH1+hueAng360;}
	}

	//////////////////////////////

	if(bB<=bG && bB<=bR)		{bmin = bB;}
	else if(bG<=bB && bG<=bR)	{bmin = bG;}
	else if(bR<=bB && bR<=bG)	{bmin = bR;}

	if(bB>=bG && bB>=bR)		{bmax = bB;}
	else if(bG>=bB && bG>=bR)	{bmax = bG;}
	else if(bR>=bB && bR>=bG)	{bmax = bR;}

	bH1=0; bS1=0; bI1=bmax;

	if(bI1>=0) {bS1=(bI1-bmin)/bI1;}	else {bS1=0.0;}

	if(bS1<=0) {bH1=-1.0;}
	else
	{
		//Compube bhe hue base on bhe relabive sizes of bhe RGB componenbs"
		bdiff=bI1 - bmin;
		//is bhe poinb wibhin +/-60 degrees of bhe red axis?
		if(bR==bI1)			{bH1=hueAng60*(bG-bB)/bdiff;}
		//is bhe poinb wibhin +/-60 degrees of bhe green axis?
		else if(bG==bI1)	{bH1=hueAng120+hueAng60*(bB-bR)/bdiff;}
		//is bhe poinb wibhin +/-60 degrees of bhe blue axis?
		else if(bB==bI1)	{bH1=hueAng240+hueAng60*(bR-bG)/bdiff;}

		if(bH1<=0) {bH1=bH1+hueAng360;}
	}

	//////////////////////////////	

	*pHt = tH1; *pRt = tR; *pGt = tG; *pBt = tB;
	*pHb = bH1; *pRb = bR; *pGb = bG; *pBb = bB;
}


void CXAxisView::createBinMethod5(	BYTE* imgR, BYTE* imgG, BYTE* imgB,  BYTE* byteBinOut,
						int fact, int nCam, 
						int tR, int tG, int tB, float tH, 
						int bR, int bG, int bB, float bH,
						int Intensity)
{
	float cR, cG, cB;
	float dt, db;
	//float tHdiff, bHdiff;
	
	
	//float dt, db;
	int pixVal;

	bool debugging = false;

	int newLengthBWDiv3		=	theapp->camHeight[nCam]/3;
	int newHeightBWDiv3		=	theapp->camWidth[nCam]/3;

	if(debugging)
	{
		SaveBWCustom1(imgR,	newLengthBWDiv3, newHeightBWDiv3);
		SaveBWCustom1(imgG,	newLengthBWDiv3, newHeightBWDiv3);
		SaveBWCustom1(imgB,	newLengthBWDiv3, newHeightBWDiv3);
	}
	
	//////////////
	//this relying in RGB
	for(int y=0; y<newHeightBWDiv3-1; y++)
	{
		for(int x=0; x<newLengthBWDiv3-1; x++)
		{
			cR	= *(imgR + x + newLengthBWDiv3*y)*100/Intensity;
			cG	= *(imgG + x + newLengthBWDiv3*y)*100/Intensity;
			cB	= *(imgB + x + newLengthBWDiv3*y)*100/Intensity;

			dt = abs(cR - tR) + abs(cG - tG) + abs(cB - tB);
			db = abs(cR - bR) + abs(cG - bG) + abs(cB - bB);

			if(dt>db)	{pixVal = 0;}
			else		{pixVal = 255;}

			*(byteBinOut + x  + newLengthBWDiv3 * y) = pixVal;
		}
	}

	/*
	//////////////////////////////

	//I am not sure if I should do this for each rgb...
	float cmin=0.0,	cmax=0.0,	cdiff=0.0;
	float cH1=0.0,	cS1=0.0,	cI1=0.0;
	float tHdiffIni, bHdiffIni;

	int hueAng60	= 60;
	int hueAng120	= 120;
	int hueAng240	= 240;
	int hueAng360	= 360;

	//////////////////////////////

	//this with Hue
	for(int y=0; y<newHeightBWDiv3-1; y++)
	{
		for(int x=0; x<newLengthBWDiv3-1; x++)
		{
			cR	= *(imgR + x + newLengthBWDiv3*y);
			cG	= *(imgG + x + newLengthBWDiv3*y);
			cB	= *(imgB + x + newLengthBWDiv3*y);
			
			//////////////////////
			//we just want to calculate hue

			if(cB<=cG && cB<=cR)		{cmin = cB;}
			else if(cG<=cB && cG<=cR)	{cmin = cG;}
			else if(cR<=cB && cR<=cG)	{cmin = cR;}

			if(cB>=cG && cB>=cR)		{cmax = cB;}
			else if(cG>=cB && cG>=cR)	{cmax = cG;}
			else if(cR>=cB && cR>=cG)	{cmax = cR;}

			cH1=0; cS1=0; cI1=cmax;

			if(cI1>=0) {cS1=(cI1-cmin)/cI1;}	else {cS1=0.0;}

			if(cS1<=0) {cH1=-1.0;}
			else
			{
				//Compube bhe hue base on bhe relabive sizes of bhe RGB componenbs"
				cdiff=cI1 - cmin;
				//is bhe poinb wibhin +/-60 degrees of bhe red axis?
				if(cR==cI1)			{cH1=hueAng60*(cG-cB)/cdiff;}
				//is bhe poinb wibhin +/-60 degrees of bhe green axis?
				else if(cG==cI1)	{cH1=hueAng120+hueAng60*(cB-cR)/cdiff;}
				//is bhe poinb wibhin +/-60 degrees of bhe blue axis?
				else if(cB==cI1)	{cH1=hueAng240+hueAng60*(cR-cG)/cdiff;}

				if(cH1<=0) {cH1=cH1+hueAng360;}
			}

			//////////////////////

			tHdiffIni = fabs(cH1 - tH);

			if(tHdiffIni < 360 - tHdiffIni)	{tHdiff = tHdiffIni;}
			else							{tHdiff	= 360 - tHdiffIni;}

			///////////////
			
			bHdiffIni = fabs(cH1 - bH);

			if(bHdiffIni < 360 - bHdiffIni)	{bHdiff = bHdiffIni;}
			else							{bHdiff	= 360 - bHdiffIni;}

			///////////////

			dt = fabs(tHdiff - tH);
			db = fabs(bHdiff - bH);

			if(dt>db)	{pixVal = 255;}
			else		{pixVal = 0;}

			*(byteBinOut + x  + newLengthBWDiv3 * y) = pixVal;
		}
	}

	//////////
	*/

	if(debugging)
	{SaveBWCustom1(byteBinOut,	newLengthBWDiv3, newHeightBWDiv3);}
}


void CXAxisView::createHistAndBinarize( BYTE *imgR1, BYTE *imgG1, BYTE *imgB1,
										BYTE *imgR2, BYTE *imgG2, BYTE *imgB2,
										BYTE *imgR3, BYTE *imgG3, BYTE *imgB3,
										BYTE *imgR4, BYTE *imgG4, BYTE *imgB4,
									    BYTE *imgIn1, BYTE *imgIn2, BYTE *imgIn3, BYTE *imgIn4,
									    BYTE *imgBin1,BYTE *imgBin2,BYTE *imgBin3,BYTE *imgBin4,
										int tX, int tY, int tH,
										int bX, int bY, int bH,
										int W,	int fact)
{
	int pix1, pix2, pix3, pix4;
	int pixR, pixG, pixB;

	/////

	bool debugging = false;

	int newLengthBWDiv3		=	CroppedCameraHeight/fact;
	int newHeightBWDiv3		=	CroppedCameraWidth/fact;

	int xIniT = tX/fact;	int xEndT = (tX + W)/fact;
	int yIniT = tY/fact;	int yEndT = (tY + tH)/fact;

	int xIniB = bX/fact;	int xEndB = (bX + W)/fact;
	int yIniB = bY/fact;	int yEndB = (bY + bH)/fact;

	if(debugging)
	{SaveBWCustom1(imgIn1,	newLengthBWDiv3, newHeightBWDiv3);}

	int i;
	for(i=0; i<=255; i++)	{histMethod5[i] = 0;}

	//////////
	//We need to double check that we are not out of range....
	//
	//
	//
	//////////
	int tRsum = 0; int tGsum = 0; int tBsum = 0; int tNpts = 0;
	int bRsum = 0; int bGsum = 0; int bBsum = 0; int bNpts = 0;
	int npts = 0;

	int y;
	for(y=yIniT; y<yEndT; y++)
	{
		for(int x=xIniT; x<xEndT; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3*y);
			pix2 = *(imgIn2 + x + newLengthBWDiv3*y);
			pix3 = *(imgIn3 + x + newLengthBWDiv3*y);
			pix4 = *(imgIn4 + x + newLengthBWDiv3*y);

			histMethod5[pix1]++;
			histMethod5[pix2]++;
			histMethod5[pix3]++;
			histMethod5[pix4]++;

			npts+=4;
		}
	}

	for(y=yIniB; y<yEndB; y++)
	{
		for(int x=xIniB; x<xEndB; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3*y);
			pix2 = *(imgIn2 + x + newLengthBWDiv3*y);
			pix3 = *(imgIn3 + x + newLengthBWDiv3*y);
			pix4 = *(imgIn4 + x + newLengthBWDiv3*y);

			histMethod5[pix1]++;
			histMethod5[pix2]++;
			histMethod5[pix3]++;
			histMethod5[pix4]++;

			npts+=4;
		}
	}

	//////////////////////////////
	int greyVal05perc = 0;
	int greyVal10perc = 0;
	int greyVal15perc = 0;
	int greyVal25perc = 0;
	int greyVal50perc = 0;
	int greyVal75perc = 0;
	int greyVal85perc = 0;
	int greyVal95perc = 0;
	
	bool th05Found = false;
	bool th10Found = false;
	bool th15Found = false;
	bool th25Found = false;
	bool th50Found = false;
	bool th75Found = false;
	bool th85Found = false;
	bool th95Found = false;

	int thVal05perc = npts*05/100;
	int thVal10perc = npts*10/100;
	int thVal15perc = npts*15/100;
	int thVal25perc = npts*25/100;
	int thVal50perc = npts*50/100;
	int thVal75perc = npts*75/100;
	int thVal85perc = npts*85/100;
	int thVal95perc = npts*95/100;

	int accPix = 0;
	
	for( i=0; i<=255; i++)
	{
		accPix	+=	histMethod5[i];

		if(accPix>=thVal05perc && !th05Found)	{greyVal05perc = i; th05Found =	true;}
		if(accPix>=thVal10perc && !th10Found)	{greyVal10perc = i; th10Found =	true;}
		if(accPix>=thVal15perc && !th15Found)	{greyVal15perc = i; th15Found =	true;}
		if(accPix>=thVal25perc && !th25Found)	{greyVal25perc = i; th25Found =	true;}
		if(accPix>=thVal50perc && !th50Found)	{greyVal50perc = i; th50Found = true;}
		if(accPix>=thVal75perc && !th75Found)	{greyVal75perc = i; th75Found =	true;}
		if(accPix>=thVal85perc && !th85Found)	{greyVal85perc = i; th85Found =	true;}
		if(accPix>=thVal95perc && !th95Found)	{greyVal95perc = i; th95Found =	true;}
	}

	int valLow  = greyVal25perc;
	int valHig  = greyVal75perc;
	int th = valLow + (valHig - valLow)*40/100;

	int binVal;
	//////////////////////////////	
	//Create binary images
	for(y=0; y<newHeightBWDiv3-1; y++)
	{
		for(int x=0; x<newLengthBWDiv3-1; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3*y);

			if(pix1>th)	{binVal = 255;} else {binVal = 0;}
			*(imgBin1 + x  + newLengthBWDiv3 * y) = binVal;
			
			//////////////
			
			pix2 = *(imgIn2 + x + newLengthBWDiv3*y);

			if(pix2>th)	{binVal = 255;} else {binVal = 0;}
			*(imgBin2 + x  + newLengthBWDiv3 * y) = binVal;

			//////////////
			
			pix3 = *(imgIn3 + x + newLengthBWDiv3*y);

			if(pix3>th)	{binVal = 255;} else {binVal = 0;}
			*(imgBin3 + x  + newLengthBWDiv3 * y) = binVal;

			//////////////
			
			pix4 = *(imgIn4 + x + newLengthBWDiv3*y);

			if(pix4>th)	{binVal = 255;} else {binVal = 0;}
			*(imgBin4 + x  + newLengthBWDiv3 * y) = binVal;
		}
	}

	//////////
	int thGlare = 230;
	
	for(y=0; y<newHeightBWDiv3-1; y++)
	{
		for(int x=0; x<newLengthBWDiv3-1; x++)
		{
			//Let's get rid of glare
			pixR = *(imgR1 + x + newLengthBWDiv3*y);
			pixG = *(imgG1 + x + newLengthBWDiv3*y);
			pixB = *(imgB1 + x + newLengthBWDiv3*y);

			if(pixR>thGlare && pixG>thGlare && pixB>thGlare) {*(imgBin1 + x  + newLengthBWDiv3 * y) = 255;}

			////////////

			//Let's get rid of glare
			pixR = *(imgR2 + x + newLengthBWDiv3*y);
			pixG = *(imgG2 + x + newLengthBWDiv3*y);
			pixB = *(imgB2 + x + newLengthBWDiv3*y);

			if(pixR>thGlare && pixG>thGlare && pixB>thGlare) {*(imgBin2 + x  + newLengthBWDiv3 * y) = 255;}

			////////////

			//Let's get rid of glare
			pixR = *(imgR3 + x + newLengthBWDiv3*y);
			pixG = *(imgG3 + x + newLengthBWDiv3*y);
			pixB = *(imgB3 + x + newLengthBWDiv3*y);

			if(pixR>thGlare && pixG>thGlare && pixB>thGlare) {*(imgBin3 + x  + newLengthBWDiv3 * y) = 255;}

			////////////

			//Let's get rid of glare
			pixR = *(imgR4 + x + newLengthBWDiv3*y);
			pixG = *(imgG4 + x + newLengthBWDiv3*y);
			pixB = *(imgB4 + x + newLengthBWDiv3*y);

			if(pixR>thGlare && pixG>thGlare && pixB>thGlare) {*(imgBin4 + x  + newLengthBWDiv3 * y) = 255;}
		}
	}
}

TwoPoint CXAxisView::FindLabelEdge_Method5(BYTE *imgIn, int nCam, int fact, int limYt, int limYb,int limX, int limW)
{
	TwoPoint result;
	result.x	=	0;
	result.y	=	0;
	result.x1	=	0;

	int LineLength	=	theapp->camHeight[nCam]/fact;
	int Height		=	theapp->camWidth[nCam]/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int pix;

	int Wslider = theapp->jobinfo[pframe->CurrentJobNum].widtMet5/fact;
	int hSlider = theapp->jobinfo[pframe->CurrentJobNum].heigMet5/fact;

	int yIni = limYt;
	int yEnd = (limYb) - hSlider;

	int xIni = limX/fact;
	int xEnd = (limX + limW)/fact;

	int npts = 0;
	int nptsLabel = 0;
	int nptsProdu = 0;
	int alphaInertia = 0;

	int i;
	for(i=0; i<Height; i++)	
	{
		transLabel[nCam][i]=0;
		transLabelX[nCam][i]=0; 
		transLabelY[nCam][i]=0; 
	}
/*
	for(int y=yIni; y<yEnd; y++)
	{
		transLabel[nCam][y] = 0;

		for(int x=xIni; x<xEnd - Wslider; x++)
		{
			nptsLabel = 0;
			nptsProdu = 0;
			
			for(int y1=y; y1<y+hSlider; y1++)
			{
				for(int x1=x; x1<x + Wslider; x1++)
				{
					pix	= *(imgIn + x1 + LineLength*y1);
					
					if(pix==0)	{nptsLabel++;}
					else		{nptsProdu++;}
				}
			}

			int diff = nptsLabel - nptsProdu;

			if(diff > transLabel[nCam][y] )
			{
				transLabel[nCam][y]		= diff;
				transLabelX[nCam][y]	= x*fact;
				transLabelY[nCam][y]	= y*fact;
			}
		}
		
		if(transLabel[nCam][y]>0)
		{transLabel[nCam][y] = hSlider*(limW/fact);}

		TRACE("%i Trans[%i]-ptProd[%i]-ptLabel[%i]-ratioLab/Prod[%.02f]\n", y, transLabel[nCam][y], nptsProdu, nptsLabel, float(nptsLabel)/float(nptsProdu));
	}*/

	bool atInitLoop;
	int x1;

	for(int y=yIni; y<yEnd; y++)
	{
		transLabel[nCam][y] = 0;
		atInitLoop = true;

		nptsLabel = 0;
		nptsProdu = 0;

		for(int x=xIni; x<xEnd - Wslider; x++)
		{
			//atInitLoop = true;

			if(atInitLoop)
			{
				for(x1=x; x1<x + Wslider; x1++)
				{
					for(int y1=y; y1<y+hSlider; y1++)
					{
						pix	= *(imgIn + x1 + LineLength*y1);
						if(pix==0)	{nptsLabel++;} else {nptsProdu++;}
					}
				}
			}
			else
			{
				x1 = x - 1;
				int y1;
				for(y1=y; y1<y+hSlider; y1++)
				{
					pix	= *(imgIn + x1 + LineLength*y1);
					if(pix==0) {nptsLabel--;} else {nptsProdu--;}
				}
				
				x1 = x + Wslider - 1;

				for(y1=y; y1<y+hSlider; y1++)
				{
					pix	= *(imgIn + x1 + LineLength*y1);
					if(pix==0) {nptsLabel++;} else {nptsProdu++;}
				}
			}
			
			atInitLoop = false;
			
			int diff = nptsLabel - nptsProdu;

			if(diff > transLabel[nCam][y] )
			{
				transLabel[nCam][y]		= diff;
				transLabelX[nCam][y]	= x*fact;
				transLabelY[nCam][y]	= y*fact;
			}
		}

		//If the labels are bigger than product then make it label
		if(transLabel[nCam][y]>0)
		{transLabel[nCam][y] = hSlider*(limW/fact);}
		
		//TRACE("%i Trans[%i]-ptProd[%i]-ptLabel[%i]-ratioLab/Prod[%.02f]\n", y, transLabel[nCam][y], nptsProdu, nptsLabel, float(nptsLabel)/float(nptsProdu));
	}

	///////////////
	//With the transition information decide where the edge is
	//inertia a way to keep memory of the previous transitions
	//0.0 - 1.0
	
	//0.0 : no inertia
	//1.0 : It never forgets, it always remembers all the transitions
	//0.5 : to small
	//0.8 : appropiate ... 4/5

	for(i=yIni; i<yEnd; i++)
	{
		alphaInertia = transLabel[nCam][i] + 4*alphaInertia/5;
		//TRACE("%i Trans[%i]-Alpha[%i]\n", y, transLabel[nCam][i], alphaInertia);

		if(alphaInertia>0)
		{
			result.x	=	transLabelX[nCam][i];
			result.y	=	transLabelY[nCam][i];;
			result.x1	=	0;

			break;
		}
	}

	return result;
}

TwoPoint CXAxisView::FindLabelEdge_Method5C1(BYTE *imgIn, int nCam, int fact, int limYt, int limYb,int limX, int limW)
{
	TwoPoint result;
	result.x	=	0;
	result.y	=	limYt;
	result.x1	=	0;

	int LineLength	=	CroppedCameraHeight/fact;
	int Height		=	CroppedCameraWidth/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int pix;

	int Wslider = theapp->jobinfo[pframe->CurrentJobNum].widtMet5/fact;
	int hSlider = theapp->jobinfo[pframe->CurrentJobNum].heigMet5/fact;

	int yIni = MAX(0, limYt);
	int yEnd = MIN(Height, limYb) - hSlider;
		yEnd = MAX(0, yEnd);

	int xIni = MAX(0, limX/fact);
	int xEnd = MIN(LineLength, (limX + limW)/fact) - Wslider;
		xEnd = MAX(0, xEnd);

	int npts = 0;
	int nptsLabel = 0;
	int nptsProdu = 0;
	int alphaInertia = 0;

	int i;
	for(i=0; i<Height; i++)	
	{
		transLabel[nCam][i]=0;
		transLabelX[nCam][i]=0; 
		transLabelY[nCam][i]=0; 
	}

	bool atInitLoop;
	int x1;

	for(int y=yIni; y<yEnd; y++)
	{
		transLabel[nCam][y] = 0;
		atInitLoop = true;

		nptsLabel = 0;
		nptsProdu = 0;

		for(int x=xIni; x<xEnd; x++)
		{
			if(atInitLoop)
			{
				for(x1=x; x1<x + Wslider; x1++)
				{
					for(int y1=y; y1<y+hSlider; y1++)
					{
						pix	= *(imgIn + x1 + LineLength*y1);
						if(pix==0)	{nptsLabel++;} else {nptsProdu++;}
					}
				}
			}
			else
			{
				x1 = x - 1;
				int y1;
				for(y1=y; y1<y+hSlider; y1++)
				{
					pix	= *(imgIn + x1 + LineLength*y1);
					if(pix==0) {nptsLabel--;} else {nptsProdu--;}
				}
				
				x1 = x + Wslider - 1;

				for(y1=y; y1<y+hSlider; y1++)
				{
					pix	= *(imgIn + x1 + LineLength*y1);
					if(pix==0) {nptsLabel++;} else {nptsProdu++;}
				}
			}
			
			atInitLoop = false;
			
			int diff = nptsLabel - nptsProdu;

			if(diff > transLabel[nCam][y] )
			{
				transLabel[nCam][y]		= diff;
				transLabelX[nCam][y]	= x*fact;
				transLabelY[nCam][y]	= y*fact;
			}
		}

		//If the labels are bigger than product then make it label
		if(transLabel[nCam][y]>0)
		{transLabel[nCam][y] = hSlider*(limW/fact);}
		
		//TRACE("%i Trans[%i]-ptProd[%i]-ptLabel[%i]-sum[%i]-ratioLab/Prod[%.02f]\n", y, transLabel[nCam][y], nptsProdu, nptsLabel, nptsProdu+ nptsLabel,float(nptsLabel)/float(nptsProdu));
	}

	///////////////
	//With the transition information decide where the edge is
	//inertia a way to keep memory of the previous transitions
	//0.0 - 1.0
	
	//0.0 : no inertia
	//1.0 : It never forgets, it always remembers all the transitions
	//0.5 : to small
	//0.8 : appropiate ... 4/5

	for(i=yIni; i<yEnd; i++)
	{
		alphaInertia = transLabel[nCam][i] + alphaInertia/2;
		//TRACE("%i Trans[%i]-Alpha[%i]\n", y, transLabel[nCam][i], alphaInertia);

		if(alphaInertia>0)
		{
			result.x	=	transLabelX[nCam][i];
			result.y	=	transLabelY[nCam][i] + hSlider*fact/2;
			result.x1	=	0;

			break;
		}
	}

	return result;
}

TwoPoint CXAxisView::FindLabelEdge_Method5C2(BYTE *imgIn, int nCam, int fact, int limYt, int limYb,int limX, int limW)
{
	TwoPoint result;
	result.x	=	0;
	result.y	=	limYt;
	result.x1	=	0;

	int LineLength	=	CroppedCameraHeight/fact;
	int Height		=	CroppedCameraWidth/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int pix;

	int Wslider = theapp->jobinfo[pframe->CurrentJobNum].widtMet5/fact;
	int hSlider = theapp->jobinfo[pframe->CurrentJobNum].heigMet5/fact;

	int yIni = MAX(0, limYt);
	int yEnd = MIN(Height, limYb) - hSlider;
		yEnd = MAX(0, yEnd);

	int xIni = MAX(0, limX/fact);
	int xEnd = MIN(LineLength, (limX + limW)/fact) - Wslider;
		xEnd = MAX(0, xEnd);

	int npts = 0;
	int nptsLabel = 0;
	int nptsProdu = 0;
	int alphaInertia = 0;

	int i;
	for(i=0; i<Height; i++)	
	{
		transLabel[nCam][i]=0;
		transLabelX[nCam][i]=0; 
		transLabelY[nCam][i]=0; 
	}

	bool atInitLoop;
	int x1;

	for(int y=yIni; y<yEnd; y++)
	{
		transLabel[nCam][y] = 0;
		atInitLoop = true;

		nptsLabel = 0;
		nptsProdu = 0;

		for(int x=xIni; x<xEnd; x++)
		{
			if(atInitLoop)
			{
				for(x1=x; x1<x + Wslider; x1++)
				{
					for(int y1=y; y1<y+hSlider; y1++)
					{
						pix	= *(imgIn + x1 + LineLength*y1);
						if(pix==0)	{nptsLabel++;} else {nptsProdu++;}
					}
				}
			}
			else
			{
				x1 = x - 1;
				int y1;
				for(y1=y; y1<y+hSlider; y1++)
				{
					pix	= *(imgIn + x1 + LineLength*y1);
					if(pix==0) {nptsLabel--;} else {nptsProdu--;}
				}
				
				x1 = x + Wslider - 1;

				for(y1=y; y1<y+hSlider; y1++)
				{
					pix	= *(imgIn + x1 + LineLength*y1);
					if(pix==0) {nptsLabel++;} else {nptsProdu++;}
				}
			}
			
			atInitLoop = false;
			
			int diff = nptsLabel - nptsProdu;

			if(diff > transLabel[nCam][y] )
			{
				transLabel[nCam][y]		= diff;
				transLabelX[nCam][y]	= x*fact;
				transLabelY[nCam][y]	= y*fact;
			}
		}

		//If the labels are bigger than product then make it label
		if(transLabel[nCam][y]>0)
		{transLabel[nCam][y] = hSlider*(limW/fact);}
		
		//TRACE("%i Trans[%i]-ptProd[%i]-ptLabel[%i]-sum[%i]-ratioLab/Prod[%.02f]\n", y, transLabel[nCam][y], nptsProdu, nptsLabel, nptsProdu+ nptsLabel,float(nptsLabel)/float(nptsProdu));
	}

	///////////////
	//With the transition information decide where the edge is
	//inertia a way to keep memory of the previous transitions
	//0.0 - 1.0
	
	//0.0 : no inertia
	//1.0 : It never forgets, it always remembers all the transitions
	//0.5 : to small
	//0.8 : appropiate ... 4/5

	for(i=yIni; i<yEnd; i++)
	{
		alphaInertia = transLabel[nCam][i] + alphaInertia/2;
		//TRACE("%i Trans[%i]-Alpha[%i]\n", y, transLabel[nCam][i], alphaInertia);

		if(alphaInertia>0)
		{
			result.x	=	transLabelX[nCam][i];
			result.y	=	transLabelY[nCam][i] + hSlider*fact/2;
			result.x1	=	0;

			break;
		}
	}

	return result;
}

TwoPoint CXAxisView::FindLabelEdge_Method5C3(BYTE *imgIn, int nCam, int fact, int limYt, int limYb,int limX, int limW)
{
	TwoPoint result;
	result.x	=	0;
	result.y	=	limYt;
	result.x1	=	0;

	int LineLength	=	CroppedCameraHeight/fact;
	int Height		=	CroppedCameraWidth/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int pix;

	int Wslider = theapp->jobinfo[pframe->CurrentJobNum].widtMet5/fact;
	int hSlider = theapp->jobinfo[pframe->CurrentJobNum].heigMet5/fact;

	int yIni = MAX(0, limYt);
	int yEnd = MIN(Height, limYb) - hSlider;
		yEnd = MAX(0, yEnd);

	int xIni = MAX(0, limX/fact);
	int xEnd = MIN(LineLength, (limX + limW)/fact) - Wslider;
		xEnd = MAX(0, xEnd);

	int npts = 0;
	int nptsLabel = 0;
	int nptsProdu = 0;
	int alphaInertia = 0;

	int i;
	for(i=0; i<Height; i++)	
	{
		transLabel[nCam][i]=0;
		transLabelX[nCam][i]=0; 
		transLabelY[nCam][i]=0; 
	}

	bool atInitLoop;
	int x1;

	for(int y=yIni; y<yEnd; y++)
	{
		transLabel[nCam][y] = 0;
		atInitLoop = true;

		nptsLabel = 0;
		nptsProdu = 0;

		for(int x=xIni; x<xEnd; x++)
		{
			if(atInitLoop)
			{
				for(x1=x; x1<x + Wslider; x1++)
				{
					for(int y1=y; y1<y+hSlider; y1++)
					{
						pix	= *(imgIn + x1 + LineLength*y1);
						if(pix==0)	{nptsLabel++;} else {nptsProdu++;}
					}
				}
			}
			else
			{
				x1 = x - 1;
				int y1;
				for(y1=y; y1<y+hSlider; y1++)
				{
					pix	= *(imgIn + x1 + LineLength*y1);
					if(pix==0) {nptsLabel--;} else {nptsProdu--;}
				}
				
				x1 = x + Wslider - 1;

				for(y1=y; y1<y+hSlider; y1++)
				{
					pix	= *(imgIn + x1 + LineLength*y1);
					if(pix==0) {nptsLabel++;} else {nptsProdu++;}
				}
			}
			
			atInitLoop = false;
			
			int diff = nptsLabel - nptsProdu;

			if(diff > transLabel[nCam][y] )
			{
				transLabel[nCam][y]		= diff;
				transLabelX[nCam][y]	= x*fact;
				transLabelY[nCam][y]	= y*fact;
			}
		}

		//If the labels are bigger than product then make it label
		if(transLabel[nCam][y]>0)
		{transLabel[nCam][y] = hSlider*(limW/fact);}
		
		//TRACE("%i Trans[%i]-ptProd[%i]-ptLabel[%i]-sum[%i]-ratioLab/Prod[%.02f]\n", y, transLabel[nCam][y], nptsProdu, nptsLabel, nptsProdu+ nptsLabel,float(nptsLabel)/float(nptsProdu));
	}

	///////////////
	//With the transition information decide where the edge is
	//inertia a way to keep memory of the previous transitions
	//0.0 - 1.0
	
	//0.0 : no inertia
	//1.0 : It never forgets, it always remembers all the transitions
	//0.5 : to small
	//0.8 : appropiate ... 4/5

	for(i=yIni; i<yEnd; i++)
	{
		alphaInertia = transLabel[nCam][i] + alphaInertia/2;
		//TRACE("%i Trans[%i]-Alpha[%i]\n", y, transLabel[nCam][i], alphaInertia);

		if(alphaInertia>0)
		{
			result.x	=	transLabelX[nCam][i];
			result.y	=	transLabelY[nCam][i] + hSlider*fact/2;
			result.x1	=	0;

			break;
		}
	}

	return result;
}

TwoPoint CXAxisView::FindLabelEdge_Method5C4(BYTE *imgIn, int nCam, int fact, int limYt, int limYb,int limX, int limW)
{
	TwoPoint result;
	result.x	=	0;
	result.y	=	limYt;
	result.x1	=	0;

	int LineLength	=	CroppedCameraHeight/fact;
	int Height		=	CroppedCameraWidth/fact;

	bool debugging	=	false;
	if(debugging)	{SaveBWCustom1(imgIn,	LineLength,	Height);}

	/////////////////

	int pix;

	int Wslider = theapp->jobinfo[pframe->CurrentJobNum].widtMet5/fact;
	int hSlider = theapp->jobinfo[pframe->CurrentJobNum].heigMet5/fact;

	int yIni = MAX(0, limYt);
	int yEnd = MIN(Height, limYb) - hSlider;
		yEnd = MAX(0, yEnd);

	int xIni = MAX(0, limX/fact);
	int xEnd = MIN(LineLength, (limX + limW)/fact) - Wslider;
		xEnd = MAX(0, xEnd);

	int npts = 0;
	int nptsLabel = 0;
	int nptsProdu = 0;
	int alphaInertia = 0;

	int i;
	for(i=0; i<Height; i++)	
	{
		transLabel[nCam][i]=0;
		transLabelX[nCam][i]=0; 
		transLabelY[nCam][i]=0; 
	}

	bool atInitLoop;
	int x1;

	for(int y=yIni; y<yEnd; y++)
	{
		transLabel[nCam][y] = 0;
		atInitLoop = true;

		nptsLabel = 0;
		nptsProdu = 0;

		for(int x=xIni; x<xEnd; x++)
		{
			if(atInitLoop)
			{
				for(x1=x; x1<x + Wslider; x1++)
				{
					for(int y1=y; y1<y+hSlider; y1++)
					{
						pix	= *(imgIn + x1 + LineLength*y1);
						if(pix==0)	{nptsLabel++;} else {nptsProdu++;}
					}
				}
			}
			else
			{
				x1 = x - 1;

				int y1;
				for(y1=y; y1<y+hSlider; y1++)
				{
					pix	= *(imgIn + x1 + LineLength*y1);
					if(pix==0) {nptsLabel--;} else {nptsProdu--;}
				}
				
				x1 = x + Wslider - 1;

				for(y1=y; y1<y+hSlider; y1++)
				{
					pix	= *(imgIn + x1 + LineLength*y1);
					if(pix==0) {nptsLabel++;} else {nptsProdu++;}
				}
			}
			
			atInitLoop = false;
			
			int diff = nptsLabel - nptsProdu;

			if(diff > transLabel[nCam][y] )
			{
				transLabel[nCam][y]		= diff;
				transLabelX[nCam][y]	= x*fact;
				transLabelY[nCam][y]	= y*fact;
			}
		}

		//If the labels are bigger than product then make it label
		if(transLabel[nCam][y]>0)
		{transLabel[nCam][y] = hSlider*(limW/fact);}
		
		//TRACE("%i Trans[%i]-ptProd[%i]-ptLabel[%i]-sum[%i]-ratioLab/Prod[%.02f]\n", y, transLabel[nCam][y], nptsProdu, nptsLabel, nptsProdu+ nptsLabel,float(nptsLabel)/float(nptsProdu));
	}

	///////////////
	//With the transition information decide where the edge is
	//inertia a way to keep memory of the previous transitions
	//0.0 - 1.0
	
	//0.0 : no inertia
	//1.0 : It never forgets, it always remembers all the transitions
	//0.5 : to small
	//0.8 : appropiate ... 4/5

	for(i=yIni; i<yEnd; i++)
	{
		alphaInertia = transLabel[nCam][i] + alphaInertia/2;
		//TRACE("%i Trans[%i]-Alpha[%i]\n", y, transLabel[nCam][i], alphaInertia);

		if(alphaInertia>0)
		{
			result.x	=	transLabelX[nCam][i];
			result.y	=	transLabelY[nCam][i] + hSlider*fact/2;
			result.x1	=	0;

			break;
		}
	}

	return result;
}

void CXAxisView::createHistAndBinarizeC1(	BYTE *imgR1, BYTE *imgG1, BYTE *imgB1,
											BYTE *imgIn1,
											BYTE *imgBin1,
											int nCam,
											int tX, int tY, int tH,
											int bX, int bY, int bH,
											int W,	int fact)
{
	int pix1;//, pix2, pix3, pix4;
	int pixR, pixG, pixB;

	/////

	bool debugging = false;

	int newLengthBWDiv3		=	CroppedCameraHeight/fact;
	int newHeightBWDiv3		=	CroppedCameraWidth/fact;

	/////////////////////

	int xIniT = MAX(0,tX/fact);			
	int xEndT = MIN(newLengthBWDiv3, (tX + W)/fact);
	
	int yIniT = MAX(0, tY/fact);	
	int yEndT = MIN(newHeightBWDiv3,(tY + tH)/fact);

	int xIniB = MAX(0, bX/fact);	
	int xEndB = MIN(newLengthBWDiv3,(bX + W)/fact);
	
	int yIniB = MAX(0, bY/fact);
	int yEndB = MIN(newHeightBWDiv3, (bY + bH)/fact);

	//////////////////////

	if(debugging)
	{SaveBWCustom1(imgIn1,	newLengthBWDiv3, newHeightBWDiv3);}

	int i;
	for(i=0; i<=255; i++)	{histMeth5[nCam][i] = 0;}
	
	//////////
	int tRsum = 0; int tGsum = 0; int tBsum = 0; int tNpts = 0;
	int bRsum = 0; int bGsum = 0; int bBsum = 0; int bNpts = 0;
	int npts = 0;

	int y;
	for(y=yIniT; y<yEndT; y++)
	{
		for(int x=xIniT; x<xEndT; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3*y);

			histMeth5[nCam][pix1]++;

			npts++;
		}
	}

	for(y=yIniB; y<yEndB; y++)
	{
		for(int x=xIniB; x<xEndB; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3*y);

			histMeth5[nCam][pix1]++;

			npts++;
		}
	}

	//////////////////////////////
	int greyVal05perc = 0;
	int greyVal10perc = 0;
	int greyVal15perc = 0;
	int greyVal25perc = 0;
	int greyVal50perc = 0;
	int greyVal75perc = 0;
	int greyVal85perc = 0;
	int greyVal95perc = 0;
	
	bool th05Found = false;
	bool th10Found = false;
	bool th15Found = false;
	bool th25Found = false;
	bool th50Found = false;
	bool th75Found = false;
	bool th85Found = false;
	bool th95Found = false;

	int thVal05perc = npts*05/100;
	int thVal10perc = npts*10/100;
	int thVal15perc = npts*15/100;
	int thVal25perc = npts*25/100;
	int thVal50perc = npts*50/100;
	int thVal75perc = npts*75/100;
	int thVal85perc = npts*85/100;
	int thVal95perc = npts*95/100;

	int accPix = 0;
	
	for( i=0; i<=255; i++)
	{
		accPix	+=	histMeth5[nCam][i];

		if(accPix>=thVal05perc && !th05Found)	{greyVal05perc = i; th05Found =	true;}
		if(accPix>=thVal10perc && !th10Found)	{greyVal10perc = i; th10Found =	true;}
		if(accPix>=thVal15perc && !th15Found)	{greyVal15perc = i; th15Found =	true;}
		if(accPix>=thVal25perc && !th25Found)	{greyVal25perc = i; th25Found =	true;}
		if(accPix>=thVal50perc && !th50Found)	{greyVal50perc = i; th50Found = true;}
		if(accPix>=thVal75perc && !th75Found)	{greyVal75perc = i; th75Found =	true;}
		if(accPix>=thVal85perc && !th85Found)	{greyVal85perc = i; th85Found =	true;}
		if(accPix>=thVal95perc && !th95Found)	{greyVal95perc = i; th95Found =	true;}
	}

	int valLow  = greyVal25perc;
	int valHig  = greyVal75perc;
	int th = valLow + (valHig - valLow)*40/100;

	int binVal;
	//////////////////////////////	
	//Create binary images
	for(y=0; y<newHeightBWDiv3-1; y++)
	{
		for(int x=0; x<newLengthBWDiv3-1; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3*y);

			if(pix1>th)	{binVal = 255;} else {binVal = 0;}
			*(imgBin1 + x  + newLengthBWDiv3 * y) = binVal;
		}
	}

	//////////
	int thGlare = 230;
	
	for(y=0; y<newHeightBWDiv3-1; y++)
	{
		for(int x=0; x<newLengthBWDiv3-1; x++)
		{
			//Let's get rid of glare
			pixR = *(imgR1 + x + newLengthBWDiv3*y);
			pixG = *(imgG1 + x + newLengthBWDiv3*y);
			pixB = *(imgB1 + x + newLengthBWDiv3*y);

			if(pixR>thGlare && pixG>thGlare && pixB>thGlare) {*(imgBin1 + x  + newLengthBWDiv3 * y) = 255;}
		}
	}
}

void CXAxisView::createHistAndBinarizeC2(	BYTE *imgR1, BYTE *imgG1, BYTE *imgB1,
											BYTE *imgIn1,
											BYTE *imgBin1,
											int nCam,
											int tX, int tY, int tH,
											int bX, int bY, int bH,
											int W,	int fact)
{
	int pix1;//, pix2, pix3, pix4;
	int pixR, pixG, pixB;

	/////

	bool debugging = false;

	int newLengthBWDiv3		=	CroppedCameraHeight/fact;
	int newHeightBWDiv3		=	CroppedCameraWidth/fact;

	/////////////////////
	int xIniT = MAX(0,tX/fact);			
	int xEndT = MIN(newLengthBWDiv3, (tX + W)/fact);
	
	int yIniT = MAX(0, tY/fact);	
	int yEndT = MIN(newHeightBWDiv3,(tY + tH)/fact);



	int xIniB = MAX(0, bX/fact);	
	int xEndB = MIN(newLengthBWDiv3,(bX + W)/fact);
	
	int yIniB = MAX(0, bY/fact);
	int yEndB = MIN(newHeightBWDiv3, (bY + bH)/fact);

	//////////////////////

	if(debugging)
	{SaveBWCustom1(imgIn1,	newLengthBWDiv3, newHeightBWDiv3);}

	int i;
	for(i=0; i<=255; i++)	{histMeth5[nCam][i] = 0;}

	//////////
	//We need to double check that we are not out of range....
	//
	//
	//
	//////////
	int tRsum = 0; int tGsum = 0; int tBsum = 0; int tNpts = 0;
	int bRsum = 0; int bGsum = 0; int bBsum = 0; int bNpts = 0;
	int npts = 0;

	int y;
	for(y=yIniT; y<yEndT; y++)
	{
		for(int x=xIniT; x<xEndT; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3*y);

			histMeth5[nCam][pix1]++;

			npts++;
		}
	}

	for(y=yIniB; y<yEndB; y++)
	{
		for(int x=xIniB; x<xEndB; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3*y);

			histMeth5[nCam][pix1]++;

			npts++;
		}
	}

	//////////////////////////////
	int greyVal05perc = 0;
	int greyVal10perc = 0;
	int greyVal15perc = 0;
	int greyVal25perc = 0;
	int greyVal50perc = 0;
	int greyVal75perc = 0;
	int greyVal85perc = 0;
	int greyVal95perc = 0;
	
	bool th05Found = false;
	bool th10Found = false;
	bool th15Found = false;
	bool th25Found = false;
	bool th50Found = false;
	bool th75Found = false;
	bool th85Found = false;
	bool th95Found = false;

	int thVal05perc = npts*05/100;
	int thVal10perc = npts*10/100;
	int thVal15perc = npts*15/100;
	int thVal25perc = npts*25/100;
	int thVal50perc = npts*50/100;
	int thVal75perc = npts*75/100;
	int thVal85perc = npts*85/100;
	int thVal95perc = npts*95/100;

	int accPix = 0;
	
	for( i=0; i<=255; i++)
	{
		accPix	+=	histMeth5[nCam][i];

		if(accPix>=thVal05perc && !th05Found)	{greyVal05perc = i; th05Found =	true;}
		if(accPix>=thVal10perc && !th10Found)	{greyVal10perc = i; th10Found =	true;}
		if(accPix>=thVal15perc && !th15Found)	{greyVal15perc = i; th15Found =	true;}
		if(accPix>=thVal25perc && !th25Found)	{greyVal25perc = i; th25Found =	true;}
		if(accPix>=thVal50perc && !th50Found)	{greyVal50perc = i; th50Found = true;}
		if(accPix>=thVal75perc && !th75Found)	{greyVal75perc = i; th75Found =	true;}
		if(accPix>=thVal85perc && !th85Found)	{greyVal85perc = i; th85Found =	true;}
		if(accPix>=thVal95perc && !th95Found)	{greyVal95perc = i; th95Found =	true;}
	}

	int valLow  = greyVal25perc;
	int valHig  = greyVal75perc;
	int th = valLow + (valHig - valLow)*40/100;

	int binVal;
	//////////////////////////////	
	//Create binary images
	for(y=0; y<newHeightBWDiv3-1; y++)
	{
		for(int x=0; x<newLengthBWDiv3-1; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3*y);

			if(pix1>th)	{binVal = 255;} else {binVal = 0;}
			*(imgBin1 + x  + newLengthBWDiv3 * y) = binVal;
		}
	}

	//////////
	int thGlare = 230;
	
	for(y=0; y<newHeightBWDiv3-1; y++)
	{
		for(int x=0; x<newLengthBWDiv3-1; x++)
		{
			//Let's get rid of glare
			pixR = *(imgR1 + x + newLengthBWDiv3*y);
			pixG = *(imgG1 + x + newLengthBWDiv3*y);
			pixB = *(imgB1 + x + newLengthBWDiv3*y);

			if(pixR>thGlare && pixG>thGlare && pixB>thGlare) {*(imgBin1 + x  + newLengthBWDiv3 * y) = 255;}
		}
	}
}

void CXAxisView::createHistAndBinarizeC3(	BYTE *imgR1, BYTE *imgG1, BYTE *imgB1,
											BYTE *imgIn1,
											BYTE *imgBin1,
											int nCam,
											int tX, int tY, int tH,
											int bX, int bY, int bH,
											int W,	int fact)
{
	int pix1;//, pix2, pix3, pix4;
	int pixR, pixG, pixB;

	/////

	bool debugging = false;

	int newLengthBWDiv3		=	CroppedCameraHeight/fact;
	int newHeightBWDiv3		=	CroppedCameraWidth/fact;

	/////////////////////
	int xIniT = MAX(0,tX/fact);			
	int xEndT = MIN(newLengthBWDiv3, (tX + W)/fact);
	
	int yIniT = MAX(0, tY/fact);	
	int yEndT = MIN(newHeightBWDiv3,(tY + tH)/fact);

	int xIniB = MAX(0, bX/fact);	
	int xEndB = MIN(newLengthBWDiv3,(bX + W)/fact);
	
	int yIniB = MAX(0, bY/fact);
	int yEndB = MIN(newHeightBWDiv3, (bY + bH)/fact);

	//////////////////////
	if(debugging)
	{SaveBWCustom1(imgIn1,	newLengthBWDiv3, newHeightBWDiv3);}

	int i;
	for(i=0; i<=255; i++)	{histMeth5[nCam][i] = 0;}

	//////////
	//We need to double check that we are not out of range....
	//
	//
	//
	//////////
	int tRsum = 0; int tGsum = 0; int tBsum = 0; int tNpts = 0;
	int bRsum = 0; int bGsum = 0; int bBsum = 0; int bNpts = 0;
	int npts = 0;
	int y;
	for(y=yIniT; y<yEndT; y++)
	{
		for(int x=xIniT; x<xEndT; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3*y);

			histMeth5[nCam][pix1]++;

			npts++;
		}
	}

	for(y=yIniB; y<yEndB; y++)
	{
		for(int x=xIniB; x<xEndB; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3*y);

			histMeth5[nCam][pix1]++;

			npts++;
		}
	}

	//////////////////////////////
	int greyVal05perc = 0;
	int greyVal10perc = 0;
	int greyVal15perc = 0;
	int greyVal25perc = 0;
	int greyVal50perc = 0;
	int greyVal75perc = 0;
	int greyVal85perc = 0;
	int greyVal95perc = 0;
	
	bool th05Found = false;
	bool th10Found = false;
	bool th15Found = false;
	bool th25Found = false;
	bool th50Found = false;
	bool th75Found = false;
	bool th85Found = false;
	bool th95Found = false;

	int thVal05perc = npts*05/100;
	int thVal10perc = npts*10/100;
	int thVal15perc = npts*15/100;
	int thVal25perc = npts*25/100;
	int thVal50perc = npts*50/100;
	int thVal75perc = npts*75/100;
	int thVal85perc = npts*85/100;
	int thVal95perc = npts*95/100;

	int accPix = 0;
	
	for( i=0; i<=255; i++)
	{
		accPix	+=	histMeth5[nCam][i];

		if(accPix>=thVal05perc && !th05Found)	{greyVal05perc = i; th05Found =	true;}
		if(accPix>=thVal10perc && !th10Found)	{greyVal10perc = i; th10Found =	true;}
		if(accPix>=thVal15perc && !th15Found)	{greyVal15perc = i; th15Found =	 true;}
		if(accPix>=thVal25perc && !th25Found)	{greyVal25perc = i; th25Found =	true;}
		if(accPix>=thVal50perc && !th50Found)	{greyVal50perc = i; th50Found = true;}
		if(accPix>=thVal75perc && !th75Found)	{greyVal75perc = i; th75Found =	true;}
		if(accPix>=thVal85perc && !th85Found)	{greyVal85perc = i; th85Found =	true;}
		if(accPix>=thVal95perc && !th95Found)	{greyVal95perc = i; th95Found =	true;}
	}

	int valLow  = greyVal25perc;
	int valHig  = greyVal75perc;
	int th = valLow + (valHig - valLow)*40/100;

	int binVal;
	//////////////////////////////	
	//Create binary images
	for(y=0; y<newHeightBWDiv3-1; y++)
	{
		for(int x=0; x<newLengthBWDiv3-1; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3*y);

			if(pix1>th)	{binVal = 255;} else {binVal = 0;}
			*(imgBin1 + x  + newLengthBWDiv3 * y) = binVal;
		}
	}

	//////////
	int thGlare = 230;
	
	for(y=0; y<newHeightBWDiv3-1; y++)
	{
		for(int x=0; x<newLengthBWDiv3-1; x++)
		{
			//Let's get rid of glare
			pixR = *(imgR1 + x + newLengthBWDiv3*y);
			pixG = *(imgG1 + x + newLengthBWDiv3*y);
			pixB = *(imgB1 + x + newLengthBWDiv3*y);

			if(pixR>thGlare && pixG>thGlare && pixB>thGlare) {*(imgBin1 + x  + newLengthBWDiv3 * y) = 255;}
		}
	}
}

void CXAxisView::createHistAndBinarizeC4(	BYTE *imgR1, BYTE *imgG1, BYTE *imgB1,
											BYTE *imgIn1,
											BYTE *imgBin1,
											int nCam,
											int tX, int tY, int tH,
											int bX, int bY, int bH,
											int W,	int fact)
{
	int pix1;//, pix2, pix3, pix4;
	int pixR, pixG, pixB;

	/////

	bool debugging = false;

	int newLengthBWDiv3		=	CroppedCameraHeight/fact;
	int newHeightBWDiv3		=	CroppedCameraWidth/fact;

	/////////////////////
	int xIniT = MAX(0,tX/fact);			
	int xEndT = MIN(newLengthBWDiv3, (tX + W)/fact);
	
	int yIniT = MAX(0, tY/fact);	
	int yEndT = MIN(newHeightBWDiv3,(tY + tH)/fact);



	int xIniB = MAX(0, bX/fact);	
	int xEndB = MIN(newLengthBWDiv3,(bX + W)/fact);
	
	int yIniB = MAX(0, bY/fact);
	int yEndB = MIN(newHeightBWDiv3, (bY + bH)/fact);

	//////////////////////

	if(debugging)
	{SaveBWCustom1(imgIn1,	newLengthBWDiv3, newHeightBWDiv3);}

	int i;
	for(i=0; i<=255; i++)	{histMeth5[nCam][i] = 0;}

	//////////
	//We need to double check that we are not out of range....
	//
	//
	//
	//////////
	int tRsum = 0; int tGsum = 0; int tBsum = 0; int tNpts = 0;
	int bRsum = 0; int bGsum = 0; int bBsum = 0; int bNpts = 0;
	int npts = 0;
	int y;
	for(y=yIniT; y<yEndT; y++)
	{
		for(int x=xIniT; x<xEndT; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3*y);

			histMeth5[nCam][pix1]++;

			npts++;
		}
	}

	for(y=yIniB; y<yEndB; y++)
	{
		for(int x=xIniB; x<xEndB; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3*y);

			histMeth5[nCam][pix1]++;

			npts++;
		}
	}

	//////////////////////////////
	int greyVal05perc = 0;
	int greyVal10perc = 0;
	int greyVal15perc = 0;
	int greyVal25perc = 0;
	int greyVal50perc = 0;
	int greyVal75perc = 0;
	int greyVal85perc = 0;
	int greyVal95perc = 0;
	
	bool th05Found = false;
	bool th10Found = false;
	bool th15Found = false;
	bool th25Found = false;
	bool th50Found = false;
	bool th75Found = false;
	bool th85Found = false;
	bool th95Found = false;

	int thVal05perc = npts*05/100;
	int thVal10perc = npts*10/100;
	int thVal15perc = npts*15/100;
	int thVal25perc = npts*25/100;
	int thVal50perc = npts*50/100;
	int thVal75perc = npts*75/100;
	int thVal85perc = npts*85/100;
	int thVal95perc = npts*95/100;

	int accPix = 0;
	
	for( i=0; i<=255; i++)
	{
		accPix	+=	histMeth5[nCam][i];

		if(accPix>=thVal05perc && !th05Found)	{greyVal05perc = i; th05Found =	true;}
		if(accPix>=thVal10perc && !th10Found)	{greyVal10perc = i; th10Found =	true;}
		if(accPix>=thVal15perc && !th15Found)	{greyVal15perc = i; th15Found =	 true;}
		if(accPix>=thVal25perc && !th25Found)	{greyVal25perc = i; th25Found =	true;}
		if(accPix>=thVal50perc && !th50Found)	{greyVal50perc = i; th50Found = true;}
		if(accPix>=thVal75perc && !th75Found)	{greyVal75perc = i; th75Found =	true;}
		if(accPix>=thVal85perc && !th85Found)	{greyVal85perc = i; th85Found =	true;}
		if(accPix>=thVal95perc && !th95Found)	{greyVal95perc = i; th95Found =	true;}
	}

	int valLow  = greyVal25perc;
	int valHig  = greyVal75perc;
	int th = valLow + (valHig - valLow)*40/100;

	int binVal;
	//////////////////////////////	
	//Create binary images
	for(y=0; y<newHeightBWDiv3-1; y++)
	{
		for(int x=0; x<newLengthBWDiv3-1; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3*y);

			if(pix1>th)	{binVal = 255;} else {binVal = 0;}
			*(imgBin1 + x  + newLengthBWDiv3 * y) = binVal;
		}
	}

	//////////
	int thGlare = 230;
	
	for(y=0; y<newHeightBWDiv3-1; y++)
	{
		for(int x=0; x<newLengthBWDiv3-1; x++)
		{
			//Let's get rid of glare
			pixR = *(imgR1 + x + newLengthBWDiv3*y);
			pixG = *(imgG1 + x + newLengthBWDiv3*y);
			pixB = *(imgB1 + x + newLengthBWDiv3*y);

			if(pixR>thGlare && pixG>thGlare && pixB>thGlare) {*(imgBin1 + x  + newLengthBWDiv3 * y) = 255;}
		}
	}
}

void CXAxisView::LearnPattX_method6(BYTE *imgInby6, BYTE *imgInby3, int posPattX1, int posPattY1, int cam, int pattsel)
{
	int xpos			=	0;		
	int ypos			=	0;
	int xposTmp			=	0;	
	int yposTmp			=	0;

	int LineLengthTmp	=	theapp->sizepattBy12;
	int szpatt12		=	theapp->sizepattBy12;
	int szpatt6			=	theapp->sizepattBy6;
	int szpatt3W		=	theapp->sizepattBy3_2W;
	int szpatt3H		=	theapp->sizepattBy3_2H;
	int LineLength;
	int LineHeight;

	bool debugging		=	false;

	int x1, y1;
	int g;


	////////////////////

	LineLength		=	widthBy6;
	LineHeight		=	heightBy6;
	LineLengthTmp	=	theapp->sizepattBy6;

	if(debugging)	{SaveBWCustom1(imgInby6, LineLength, LineHeight);}

	//x1	=	posPattX1/6;
	//y1	=	posPattY1/6;

	xpos	= 0;
	ypos	= 0;
	xposTmp	= 0;
	yposTmp	= 0;
///commenting out extra lines of code -- Andrew Gawlik 2017-07-20
	/*
	for(ypos=y1-szpatt6/2; ypos<y1+szpatt6/2; ypos++)
	{
		for(xpos=x1-szpatt6/2;xpos<x1+szpatt6/2;xpos++)
		{	
			if(xpos>0 && xpos<LineLength && ypos>0 && ypos<LineHeight)
			{
				if(pattsel == 2)
				{
					*(bytePattBy6_method6 + xposTmp + LineLengthTmp*yposTmp)= *(imgInby6 + xpos + LineLength*ypos);
					xposTmp++;
				}
				else
				{
					*(bytePattBy6_method6_1 + xposTmp + LineLengthTmp*yposTmp)= *(imgInby6 + xpos + LineLength*ypos);
					xposTmp++;
				}
			}
		}

		xposTmp=0; yposTmp++;
	}
*/
	////////////////////
///commenting out extra lines of code -- Andrew Gawlik 2017-07-20
/*
	LineLength		=	widthBy3;
	LineHeight		=	heightBy3;
	int szpatt3W	=	theapp->sizepattBy3_2W;
	int szpatt3H	=	theapp->sizepattBy3_2H;
	LineLengthTmp	=	szpatt3W;

	if(debugging)	{SaveBWCustom1(imgInby3, LineLength, LineHeight);}

	x1	=	posPattX1/3;
	y1	=	posPattY1/3;

	xpos = 0; ypos = 0;	xposTmp	= 0; yposTmp = 0;

	for(ypos=y1-szpatt3H/2; ypos<y1+szpatt3H/2; ypos++)
	{
		for(xpos=x1-szpatt3W/2;xpos<x1+szpatt3W/2;xpos++)
		{	
			if(xpos>0 && xpos<LineLength && ypos>0 && ypos<LineHeight)
			{
				if(pattsel == 2)
				{
					*(bytePattBy3_method6 + xposTmp + LineLengthTmp*yposTmp)= *(imgInby3 + xpos + LineLength*ypos);
					xposTmp++;
				}
				else
				{
					*(bytePattBy3_method6_1 + xposTmp + LineLengthTmp*yposTmp)= *(imgInby3 + xpos + LineLength*ypos);
					xposTmp++;
				}
			}
		}

		xposTmp=0; yposTmp++;
	}
*/
	////////////////////
///teaching already saves these	
//	if(debugging)	{SaveBWCustom1(bytePattBy6[pattsel],	szpatt6, szpatt6);}
//	if(debugging)	{SaveBWCustom1(bytePattBy3[pattsel],	szpatt3W, szpatt3H);}
///replacing if statement with single function/method call
/*
	if(pattsel == 2)
	{
		SaveTemplateX_method6(bytePattBy6_method6, bytePattBy3_method6, cam, insp);
	}
	else		//		Should only be 1 since there is a special method for Method 6 Pattern 3
	{
		SaveTemplateX_method6(bytePattBy6_method6_1, bytePattBy3_method6_1, cam, pattsel);
	}
*/
	BYTE	*pPatDivBy6;
	BYTE	*pPatDivBy3;
	RECT	srcRect;
	char	*pFilename = "c:\\silgandata\\pat\\"; //drive
	CString	c;

	c=itoa(pframe->CurrentJobNum, buf, 10);
	strcpy(currentpathBmp,pFilename);
	strcat(currentpathBmp,c);
	strcpy(currentpathBmp2,pFilename);
	strcat(currentpathBmp2,c);
	strcpy(currentpathBmp3,pFilename);
	strcat(currentpathBmp3,c);

	strcpy(currentpathTxt,pFilename);
	strcat(currentpathTxt,c);
	strcpy(currentpathTxt2,pFilename);
	strcat(currentpathTxt2,c);
	strcpy(currentpathTxt3,pFilename);
	strcat(currentpathTxt3,c);

	switch(pattsel)
	{
		case 1:
		{
			//determined from CXAxisView::SaveTemplateX_method6
			pPatDivBy3 = bytePattBy3_method6_1;
			pPatDivBy6 = bytePattBy6_method6_1;

			strcat(currentpathTxt3,"\\M6imgBy3L.txt");
			strcat(currentpathBmp3,"\\M6patBy3L.bmp");

			strcat(currentpathTxt2,"\\M6imgBy6L.txt");
			strcat(currentpathBmp2,"\\M6patBy6L.bmp");
			break;
		}
		case 2:
		{ 
			//determined from CXAxisView::SaveTemplateX_method6
			pPatDivBy3 = bytePattBy3_method6;
			pPatDivBy6 = bytePattBy6_method6;

			strcat(currentpathTxt3,"\\M6imgBy3M.txt");
			strcat(currentpathBmp3,"\\M6patBy3M.bmp");

			strcat(currentpathTxt2,"\\M6imgBy6M.txt");
			strcat(currentpathBmp2,"\\M6patBy6M.bmp");
			break;
		}
		case 3:
		{
			//copied this from OnLoadPatternX_method6_num3 (for code consolidation)
			pPatDivBy3 = bytePattBy3_method6_num3;
			pPatDivBy6 = bytePattBy6_method6_num3;
			
			strcat(currentpathTxt3,"\\M6imgBy3R_num3.txt");
			strcat(currentpathBmp3,"\\M6patBy3R_num3.bmp");

			strcat(currentpathTxt2,"\\M6imgBy6R_num3.txt");
			strcat(currentpathBmp2,"\\M6patBy6R_num3.bmp");
			break;
		}
		default:
		{
			pPatDivBy3 = NULL;
			pPatDivBy6 = NULL;
		}
	}

	if(pPatDivBy6 != NULL)
	{

		CVisByteImage srcDivBy6(widthBy6, heightBy6, 1, -1, imgInby6);
		CVisByteImage dstDivBy6(theapp->sizepattBy6, theapp->sizepattBy6, 1, -1, pPatDivBy6);
		//CVisByteImage dstDivBy6(theapp->sizepattBy6, theapp->sizepattBy6, 1, -1,  bytePattBy6[pattsel-1]);

		x1	=	posPattX1/6;
		y1	=	posPattY1/6;

		srcRect.left = x1-szpatt6/2;
		srcRect.top = y1-szpatt6/2;
		srcRect.right = x1+szpatt6/2;
		srcRect.bottom = y1+szpatt6/2;

		if(srcRect.left < 0 )
			srcRect.left = 0;
		if(srcRect.top < 0 )
			srcRect.top = 0;
		if(srcRect.right > widthBy6)
			srcRect.right = widthBy6;
		if(srcRect.bottom > heightBy6)
			srcRect.bottom = heightBy6;

		srcDivBy6.CopyPixelsTo(dstDivBy6,srcRect,evisnormalizeDefault,-1.0,1.0);

		try
		{
			SVisFileDescriptor filedescriptorT;
			ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
			filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
			filedescriptorT.jpeg_quality = 0;
			filedescriptorT.filename = currentpathBmp2;
			dstDivBy6.WriteFile(filedescriptorT);
		}
		catch (CVisError e)  // "..." is bad - we could leak an exception object.
		{AfxMessageBox("The pat file could not be saved.");}// Warn the user.
		//srcDivBy6.CopyPixelsTo(-1,-1,&dstDivBy6,const_cast<const RECT*>(&srcRect),const_cast<const POINT>(dstPoint),evisnormalizeDefault,1.0,-1.0);
		//for(ypos=y1-szpatt6/2; ypos<y1+szpatt6/2; ypos++)
		//{
		//	for(xpos=x1-szpatt6/2;xpos<x1+szpatt6/2;xpos++)
		//	{	
		//		if(xpos>0 && xpos<LineLength && ypos>0 && ypos<LineHeight)
		//		{
		//			*(patDivBy6 + xposTmp + LineLengthTmp*yposTmp)= *(imgInby6 + xpos + LineLength*ypos);
		//			xposTmp++;
		//		}
		//	}

		//	xposTmp=0; yposTmp++;
		//}

		ImageFile.Open(currentpathTxt2, CFile::modeWrite | CFile::modeCreate);
		
		CString szDigit2;	int digit2;

		for(g=0; g<szpatt6*szpatt6; g++)
		{
			digit2=*(pPatDivBy6+g);
			//digit2=*(bytePattBy6[pattsel-1]);
			szDigit2.Format("%3i",digit2);
			ImageFile.Write(szDigit2,/*szDigit2.GetLength()*/ 3);
		}

		ImageFile.Close();
	//}

	//if(pPatDivBy3 != NULL)
	//{
		CVisByteImage srcDivBy3(widthBy3, heightBy3, 1, -1, imgInby3);
		CVisByteImage dstDivBy3(theapp->sizepattBy3_2W, theapp->sizepattBy3_2H, 1, -1, pPatDivBy3);
		//CVisByteImage dstDivBy3(theapp->sizepattBy3_2W, theapp->sizepattBy3_2H, 1, -1, bytePattBy3[pattsel-1]);

		x1	=	posPattX1/3;
		y1	=	posPattY1/3;

		srcRect.left = x1-szpatt3W/2;
		srcRect.top = y1-szpatt3H/2;
		srcRect.right = x1+szpatt3W/2;
		srcRect.bottom = y1+szpatt3H/2;

		if(srcRect.left < 0 )
			srcRect.left = 0;
		if(srcRect.top < 0 )
			srcRect.top = 0;
		if(srcRect.right > widthBy3)
			srcRect.right = widthBy3;
		if(srcRect.bottom > heightBy3)
			srcRect.bottom = heightBy3;

		srcDivBy3.CopyPixelsTo(dstDivBy3,srcRect,evisnormalizeDefault,-1.0,1.0);

		try
		{
			SVisFileDescriptor filedescriptorT;
			ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
			filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
			filedescriptorT.jpeg_quality = 0;
			filedescriptorT.filename = currentpathBmp3;
			dstDivBy3.WriteFile(filedescriptorT);
		}
		catch (CVisError e)  // "..." is bad - we could leak an exception object.
		{AfxMessageBox("The pat file could not be saved.");}// Warn the user.

		ImageFile.Open(currentpathTxt3, CFile::modeWrite | CFile::modeCreate);

		CString szDigit3;	int digit3;

		for(g=0; g<szpatt3W*szpatt3H; g++)
		{
			digit3=*(pPatDivBy3+g);
			//digit3=*(bytePattBy3[pattsel-1]);
			szDigit3.Format("%3i",digit3);
			ImageFile.Write(szDigit3,/*szDigit3.GetLength()*/ 3);
		}

		ImageFile.Close();

		//for(ypos=y1-szpatt3H/2; ypos<y1+szpatt3H/2; ypos++)
		//{
		//
		//	for(xpos=x1-szpatt3W/2;xpos<x1+szpatt3W/2;xpos++)
		//	{	
		//		if(xpos>0 && xpos<LineLength && ypos>0 && ypos<LineHeight)
		//		{
		//			*(patDivBy3 + xposTmp + LineLengthTmp*yposTmp)= *(imgInby3 + xpos + LineLength*ypos);
		//			xposTmp++;
		//		}
		//	}

		//	xposTmp=0; yposTmp++;
		//}
	}

	//SaveTemplateX_method6(imgInby6, imgInby3, cam, pattsel);
	UpdateData(false);
	InvalidateRect(NULL,false);
	pPatDivBy3 = NULL;
	pPatDivBy6 = NULL;
}

void CXAxisView::SaveTemplateX_method6(BYTE *im_sr2, BYTE *im_sr3, int ncam, int pattsel)
{
	char* pFileName = "c:\\silgandata\\pat\\";//drive
	CString c;

	c =itoa(pframe->CurrentJobNum, buf, 10);

	strcpy(currentpathTxt,pFileName);	strcat(currentpathTxt,c);
	strcpy(currentpathBmp,pFileName);	strcat(currentpathBmp,c);

	strcpy(currentpathTxt2,pFileName);	strcat(currentpathTxt2,c);
	strcpy(currentpathBmp2,pFileName);	strcat(currentpathBmp2,c);

	strcpy(currentpathTxt3,pFileName);	strcat(currentpathTxt3,c);
	strcpy(currentpathBmp3,pFileName);	strcat(currentpathBmp3,c);

	///replacing if statements with switch
	//if(pattsel==1)
	//{
		//strcat(currentpathTxt2,"\\M6imgBy6L.txt");
		//strcat(currentpathBmp2,"\\M6patBy6L.bmp");  //M6: Method 6
		//strcat(currentpathTxt3,"\\M6imgBy3L.txt");
		//strcat(currentpathBmp3,"\\M6patBy3L.bmp");
	//}
	//else if(pattsel==2)
	//{
		//strcat(currentpathTxt2,"\\M6imgBy6M.txt");
	//	strcat(currentpathBmp2,"\\M6patBy6M.bmp");
		//strcat(currentpathTxt3,"\\M6imgBy3M.txt");
	//	strcat(currentpathBmp3,"\\M6patBy3M.bmp");
	//}
	//else if(pattsel==3)
	//{
		//strcat(currentpathTxt2,"\\M6imgBy6R.txt");
	//	strcat(currentpathBmp2,"\\M6patBy6R.bmp");
		//strcat(currentpathTxt3,"\\M6imgBy3R.txt");
	//	strcat(currentpathBmp3,"\\M6patBy3R.bmp");
	//}

	switch(pattsel)
	{
		case 1:
		{
			strcat(currentpathTxt2,"\\M6imgBy6L.txt");
			strcat(currentpathBmp2,"\\M6patBy6L.bmp");  //M6: Method 6

			strcat(currentpathTxt3,"\\M6imgBy3L.txt");
			strcat(currentpathBmp3,"\\M6patBy3L.bmp");
			break;
		}
		case 2:
		{
			strcat(currentpathTxt2,"\\M6imgBy6M.txt");
			strcat(currentpathBmp2,"\\M6patBy6M.bmp");

			strcat(currentpathTxt3,"\\M6imgBy3M.txt");
			strcat(currentpathBmp3,"\\M6patBy3M.bmp");
			break;
		}
		case 3:
		{
			strcat(currentpathTxt2,"\\M6imgBy6R_num3.txt");
			strcat(currentpathBmp2,"\\M6patBy6R_num3.bmp");

			strcat(currentpathTxt3,"\\M6txtBy3R_num3.txt");
			strcat(currentpathBmp3,"\\M6patBy3R_num3.bmp");
			break;
		}
		default:
		{
			strcat(currentpathTxt2,"\\M6imgBy6.txt"); 
			strcat(currentpathBmp2,"\\M6patBy6.bmp");  //signifying somethign went wrong if the last letter is missing

			strcat(currentpathTxt3,"\\M6imgBy3.txt");
			strcat(currentpathBmp3,"\\M6patBy3.bmp");
		}
	}

	int szpatt12	=	theapp->sizepattBy12;
	int szpatt6		=	theapp->sizepattBy6;
	int szpatt3W	=	theapp->sizepattBy3_2W;
	int szpatt3H	=	theapp->sizepattBy3_2H;

	//////////////////

	//commenting out text file save
	
	ImageFile.Open(currentpathTxt2, CFile::modeWrite | CFile::modeCreate);
	
	CString szDigit2;	int digit2;
	int g;
	for(g=0; g<szpatt6*szpatt6; g++)
	{
		digit2=*(im_sr2+g);
		szDigit2.Format("%3i",digit2);
		ImageFile.Write(szDigit2,szDigit2.GetLength());
	}

	ImageFile.Close();
	
	///////////////////

	CVisByteImage image2(szpatt6,szpatt6,1,-1,im_sr2);
	imagepat2=image2;
   
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpathBmp2;
		imagepat2.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{AfxMessageBox("The pat file could not be saved.");}// Warn the user.

	///////////////////

	//comment out text file save

	
	ImageFile.Open(currentpathTxt3, CFile::modeWrite | CFile::modeCreate);

	CString szDigit3;	int digit3;

	for(g=0; g<szpatt3W*szpatt3H; g++)
	{
		digit3=*(im_sr3+g);
		szDigit3.Format("%3i",digit3);
		ImageFile.Write(szDigit3,szDigit3.GetLength());
	}

	ImageFile.Close();

	///////////////////

	CVisByteImage image3(szpatt3W,szpatt3H,1,-1,im_sr3);
	imagepat3=image3;

	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpathBmp3;
		imagepat3.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{AfxMessageBox("The pat file could not be saved.");}// Warn the user.
}

void CXAxisView::Rotate270Right(const BYTE *image, int imageWidth, int imageHeight, int bytesPPixel, BYTE *rotatedImage)
{
	int imageWidthOrg = imageWidth;
	int imageHeightOrg = imageHeight;

	int imageWidthNew = imageHeight;
	int imageHeightNew  = imageWidth;

	int imageStrideOrg = imageWidthOrg*bytesPPixel;
	int imageStrideNew = imageHeight*bytesPPixel;

	int xCoordinateOrg = 0;
	int yCoordinateOrg = 0;

	int xCoordinateNew = 0;
	int yCoordinateNew = 0;

	int xPositionInArrayOrg = 0;
	int yPositionInArrayOrg = 0;

	int xPositionInArrayNew = 0;
	int yPositionInArrayNew = 0;


	int newXcoordinate = 0;
	int newYcoordinate = 0;

	for(int x = 0; x < imageWidth ; x++ )
	{
		for(int y = 0; y < imageHeight; y++ )
		{
			xCoordinateOrg = x;
			yCoordinateOrg = y;

			xPositionInArrayOrg = xCoordinateOrg*bytesPPixel;
			yPositionInArrayOrg = yCoordinateOrg;



			xCoordinateNew = yCoordinateOrg;
			yCoordinateNew = (imageWidth-xCoordinateOrg)-1;

			xPositionInArrayNew = xCoordinateNew*bytesPPixel;
			yPositionInArrayNew = yCoordinateNew;


			rotatedImage[xPositionInArrayNew+yPositionInArrayNew*imageStrideNew]   = image[xPositionInArrayOrg+yPositionInArrayOrg*imageStrideOrg];
			rotatedImage[1+xPositionInArrayNew+yPositionInArrayNew*imageStrideNew] = image[1+xPositionInArrayOrg+yPositionInArrayOrg*imageStrideOrg];
			rotatedImage[2+xPositionInArrayNew+yPositionInArrayNew*imageStrideNew] = image[2+xPositionInArrayOrg+yPositionInArrayOrg*imageStrideOrg];
			rotatedImage[3+xPositionInArrayNew+yPositionInArrayNew*imageStrideNew] = image[3+xPositionInArrayOrg+yPositionInArrayOrg*imageStrideOrg];
		}
	}
}

void CXAxisView::Rotate90Right(const BYTE *image, int imageWidth, int imageHeight, int bytesPPixel, BYTE *rotatedImage)
{
	int imageWidthOrg = imageWidth;
	int imageHeightOrg = imageHeight;

	int imageWidthNew = imageHeight;
	int imageHeightNew  = imageWidth;

	int imageStrideOrg = imageWidthOrg*bytesPPixel;
	int imageStrideNew = imageHeight*bytesPPixel;

	int xCoordinateOrg = 0;
	int yCoordinateOrg = 0;

	int xCoordinateNew = 0;
	int yCoordinateNew = 0;

	int xPositionInArrayOrg = 0;
	int yPositionInArrayOrg = 0;

	int xPositionInArrayNew = 0;
	int yPositionInArrayNew = 0;


	int newXcoordinate = 0;
	int newYcoordinate = 0;

	for(int x = 0; x < imageWidth ; x++ )
	{
		for(int y = 0; y < imageHeight; y++ )
		{
			xCoordinateOrg = x;
			yCoordinateOrg = y;

			xPositionInArrayOrg = xCoordinateOrg*bytesPPixel;
			yPositionInArrayOrg = yCoordinateOrg;


			xCoordinateNew =  (imageHeight-yCoordinateOrg)-1;
			yCoordinateNew = x;

			xPositionInArrayNew = xCoordinateNew*bytesPPixel;
			yPositionInArrayNew = yCoordinateNew;

			rotatedImage[xPositionInArrayNew+yPositionInArrayNew*imageStrideNew]   = image[xPositionInArrayOrg+yPositionInArrayOrg*imageStrideOrg];
			rotatedImage[1+xPositionInArrayNew+yPositionInArrayNew*imageStrideNew] = image[1+xPositionInArrayOrg+yPositionInArrayOrg*imageStrideOrg];
			rotatedImage[2+xPositionInArrayNew+yPositionInArrayNew*imageStrideNew] = image[2+xPositionInArrayOrg+yPositionInArrayOrg*imageStrideOrg];
			rotatedImage[3+xPositionInArrayNew+yPositionInArrayNew*imageStrideNew] = image[3+xPositionInArrayOrg+yPositionInArrayOrg*imageStrideOrg];
		}
	}
}

void CXAxisView::ScaleImageDownBySubsampling(const BYTE *image, int imageWidth, int imageHeight, int bytesPPixel, int scaleFactor, BYTE *scaledImage)
{
	int imageWidthOrg = imageWidth;
	int imageHeightOrg = imageHeight;
	int imageStrideOrg = imageWidthOrg*bytesPPixel;

	int xPositionInArrayOrg = 0;

	int imageWidthNew = imageWidthOrg/scaleFactor;
	int imageHeightNew  = imageHeightOrg/scaleFactor;
	int imageStrideNew = imageWidthNew*bytesPPixel;

	int xCoordinateNew = 0;
	int yCoordinateNew = 0;

	int xPositionInArrayNew = 0;
	int yPositionInArrayNew = 0;

	int newXcoordinate = 0;
	int newYcoordinate = 0;

	for(int x = 0; x < imageWidth; x+=scaleFactor )
	{
		for(int y = 0; y < imageHeight; y+=scaleFactor )
		{

			xPositionInArrayOrg = x*bytesPPixel;

			xCoordinateNew = x/scaleFactor;
			yCoordinateNew = y/scaleFactor;

			xPositionInArrayNew = xCoordinateNew*bytesPPixel;
			yPositionInArrayNew = yCoordinateNew;
/*
			memcpy (scaledImage+xPositionInArrayNew+yPositionInArrayNew*imageStrideNew, image+xPositionInArrayOrg+yPositionInArrayOrg*imageStrideOrg, 4 );
*/
			scaledImage[xPositionInArrayNew+yPositionInArrayNew*imageStrideNew] = image[xPositionInArrayOrg+y*imageStrideOrg];
			scaledImage[1+xPositionInArrayNew+yPositionInArrayNew*imageStrideNew] = image[1+xPositionInArrayOrg+y*imageStrideOrg];
			scaledImage[2+xPositionInArrayNew+yPositionInArrayNew*imageStrideNew] = image[2+xPositionInArrayOrg+y*imageStrideOrg];
			scaledImage[3+xPositionInArrayNew+yPositionInArrayNew*imageStrideNew] = image[3+xPositionInArrayOrg+y*imageStrideOrg];
		}
	}
}

void CXAxisView::ScaleImageDownByAveraging(const BYTE *image, int imageWidth, int imageHeight, int bytesPPixel, int scaleFactor, BYTE *scaledImage)
{
	int imageWidthOrg = imageWidth;
	int imageHeightOrg = imageHeight;
	int imageStrideOrg = imageWidthOrg*bytesPPixel;

	int xCoordinateOrg = 0;
	int yCoordinateOrg = 0;

	int xPositionInArrayOrg = 0;
	int yPositionInArrayOrg = 0;

	int imageWidthNew = imageWidthOrg/scaleFactor;
	int imageHeightNew  = imageHeightOrg/scaleFactor;
	int imageStrideNew = imageWidthNew*bytesPPixel;

	int xCoordinateNew = 0;
	int yCoordinateNew = 0;

	int xPositionInArrayNew = 0;
	int yPositionInArrayNew = 0;

	int newXcoordinate = 0;
	int newYcoordinate = 0;

	int BAverageValue = 0;
	int GAverageValue = 0;
	int RAverageValue = 0;
	int UAverageValue = 0;

	for(int x = 0; x < imageWidth; x+=scaleFactor )
	{
		for(int y = 0; y < imageHeight; y+=scaleFactor )
		{
			xCoordinateOrg = x;
			yCoordinateOrg = y;

			xPositionInArrayOrg = xCoordinateOrg*bytesPPixel;
			yPositionInArrayOrg = yCoordinateOrg;

			xCoordinateNew = xCoordinateOrg/scaleFactor;
			yCoordinateNew = yCoordinateOrg/scaleFactor;

			xPositionInArrayNew = xCoordinateNew*bytesPPixel;
			yPositionInArrayNew = yCoordinateNew;

			BAverageValue = 0;
			GAverageValue = 0;
			RAverageValue = 0;
			UAverageValue = 0;

			for(int xPositionOffset = 0; xPositionOffset < scaleFactor*bytesPPixel; xPositionOffset+=bytesPPixel )
			{
				for(int yPositionOffset = 0; yPositionOffset < scaleFactor; yPositionOffset++ )
				{
					BAverageValue = BAverageValue+image[xPositionInArrayOrg+xPositionOffset+(yPositionInArrayOrg+yPositionOffset)*imageStrideOrg];
					GAverageValue = GAverageValue+image[1+xPositionInArrayOrg+xPositionOffset+(yPositionInArrayOrg+yPositionOffset)*imageStrideOrg];
					RAverageValue = RAverageValue+image[2+xPositionInArrayOrg+xPositionOffset+(yPositionInArrayOrg+yPositionOffset)*imageStrideOrg];
					UAverageValue = UAverageValue+image[3+xPositionInArrayOrg+xPositionOffset+(yPositionInArrayOrg+yPositionOffset)*imageStrideOrg];
				}
			}

			BAverageValue = BAverageValue/(scaleFactor*scaleFactor);
			GAverageValue = GAverageValue/(scaleFactor*scaleFactor);
			RAverageValue = RAverageValue/(scaleFactor*scaleFactor);
			UAverageValue = UAverageValue/(scaleFactor*scaleFactor);

			scaledImage[xPositionInArrayNew+yPositionInArrayNew*imageStrideNew] = BAverageValue;
			scaledImage[1+xPositionInArrayNew+yPositionInArrayNew*imageStrideNew] = GAverageValue;
			scaledImage[2+xPositionInArrayNew+yPositionInArrayNew*imageStrideNew] = RAverageValue;
			scaledImage[3+xPositionInArrayNew+yPositionInArrayNew*imageStrideNew] = UAverageValue;
		}
	}
}

void CXAxisView::ConvertFromBGRUToGreyscale(const BYTE *image, int imageWidth, int imageHeight, BYTE *GreyscaleImage)
{
	int bytesPerPixelOld = 4;
	int bytesPerPixelNew = 1;

	int imageWidthOrg = imageWidth;
	int imageHeightOrg = imageHeight;
	int imageStrideOrg = imageWidthOrg*bytesPerPixelOld;

	int xCoordinateOrg = 0;
	int yCoordinateOrg = 0;

	int xPositionInArrayOrg = 0;
	int yPositionInArrayOrg = 0;

	int imageWidthNew = imageWidthOrg;
	int imageHeightNew  = imageHeightOrg;
	int imageStrideNew = imageWidthNew*bytesPerPixelNew;

	int xCoordinateNew = 0;
	int yCoordinateNew = 0;

	int xPositionInArrayNew = 0;
	int yPositionInArrayNew = 0;

	int newXcoordinate = 0;
	int newYcoordinate = 0;

	double greyScaleValue = 0;

	for(int x = 0; x < imageWidth; x++ )
	{
		for(int y = 0; y < imageHeight; y++ )
		{
			xCoordinateOrg = x;
			yCoordinateOrg = y;

			xPositionInArrayOrg = xCoordinateOrg*bytesPerPixelOld;
			yPositionInArrayOrg = yCoordinateOrg;

			xCoordinateNew = xCoordinateOrg;
			yCoordinateNew = yCoordinateOrg;

			xPositionInArrayNew = xCoordinateNew;
			yPositionInArrayNew = yCoordinateNew;

			greyScaleValue = 0.07*image[1+xPositionInArrayOrg+yPositionInArrayOrg*imageStrideOrg] +
							 0.72*image[2+xPositionInArrayOrg+yPositionInArrayOrg*imageStrideOrg] + 
							 0.21*image[3+xPositionInArrayOrg+yPositionInArrayOrg*imageStrideOrg];

			GreyscaleImage[xPositionInArrayNew+yPositionInArrayNew*imageStrideNew] =greyScaleValue +0.5;
		}
	}
}

void CXAxisView::Display(BYTE *image)
{	


	QueryPerformanceFrequency(&m_lnC1FreqI0);
	QueryPerformanceCounter(&m_lnC1StartI0); //Timing

	int nCam = 1;

	if(first)
	{
//		QueryPerformanceFrequency(&Frequency); QueryPerformanceCounter(&timeStart);
		first=false;
		pframe->countC0.store(pframe->countC1.load());		
		SetTimer(1, theapp->inspDelay, NULL);
	}

	QueryPerformanceFrequency(&Frequency0);
	QueryPerformanceCounter(&timeStart0);	//start measurement timer.

	int currentCamWidth = theapp->camWidth[nCam];
	int currentCamHeight = theapp->camHeight[nCam];

	int newWidth = MainDisplayImageHeight;
	int newHeight = MainDisplayImageWidth;

	int offsetX = MainCameraDisplayXOffset;
	int offsetY = (currentCamWidth == MainDisplayImageHeight) ? MainCameraDisplayYOffset : CroppedImageYOffset;

	im_dataC = pframe->m_pcamera->m_imageProcessed[0][pframe->m_pcamera->readNext[0].load()]->GetData();
	DuplicateImage(im_dataC, im_dataCFullRes,
		currentCamWidth, currentCamHeight, bytesPerPixel);

	CropCameraImage(im_dataCFullRes, im_dataC,
		currentCamWidth, currentCamHeight,
		newWidth, newHeight, offsetX, offsetY,
		bytesPerPixel);
	if(gotPic1 && (theapp->enabPicsToShow || !theapp->camsPresent))
	{

		/* HAS TO BE FIXED BECAUSE OF DIFFERENT SIZED SAVED IMAGES RELATED TO SWITCHING BETWEEN CAMERA SETTINGS (WAIST INSPECTION) - JUSTIN MARTIN */
		

		ScaleImageDownBySubsampling(im_dataCFullRes,
									currentCamWidth, currentCamHeight,bytesPerPixel,
									FullImageScaleDownFactor,
									cam1ImageFullScaled);

		Rotate270Right(cam1ImageFullScaled,
//		Rotate90Right(cam1ImageFullScaled,
			FullImageScaledWidth, FullImageScaledHeight, bytesPerPixel,
						cam1ImageFullScaledRotated);
		
		ConvertFromBGRUToGreyscale(cam1ImageFullScaledRotated, 
								   FullImageScaledRotatedWidth,
								   FullImageScaledRotatedHeight, 
								   cam1ImageFullScaledRotatedGreyscale);
		SaveTempPic(im_dataC, 3);

		im_reduce1			=	Reduce(im_dataC);	
		SaveTempPic(im_reduce1, 3);

	}
	else if(theapp->camsPresent)
	{
		im_reduce1 = Reduce(im_dataC);
//		im_reduce1 = Reduce(pframe->m_pcamera->cam1ImageCropped);

		if(theapp->waistAvailable)
		{
			//		WAIST INSPECTION SECTION
			ScaleImageDownBySubsampling(pframe->m_pcamera->cam1ImageFull,
									currentCamWidth, currentCamHeight, bytesPerPixel, FullImageScaleDownFactor,
									cam1ImageFullScaled);

			Rotate270Right(cam1ImageFullScaled,
//				Rotate90Right(cam1ImageFullScaled,
					FullImageScaledWidth, FullImageScaledHeight, bytesPerPixel,
						cam1ImageFullScaledRotated);
		
			ConvertFromBGRUToGreyscale(cam1ImageFullScaledRotated, 
								   FullImageScaledRotatedWidth,
								   FullImageScaledRotatedHeight, 
								   cam1ImageFullScaledRotatedGreyscale);
		}
	}

	QueryPerformanceCounter(&timeEnd0);	//end measurement timer.
	//Convert to seconds
	if(timeStart0.QuadPart!=0 && timeEnd0.QuadPart!=0)
	{
		spanElapsed0	= (double)(Frequency0.QuadPart/1000); 
		nDiff0 = timeEnd0.QuadPart-timeStart0.QuadPart;	
		spanElapsed0 = nDiff0/spanElapsed0;
	}

	CVisRGBAByteImage cam1Large(FullImageScaledRotatedWidth, FullImageScaledRotatedHeight, 1,-1, cam1ImageFullScaledRotated);
	imageC1Large = cam1Large;	//paint image to screen

	/////////////////
	//Image Preprocessing
	int widthImg = MainDisplayImageWidth/3;
	int heighImg = MainDisplayImageHeight/3;

	CVisRGBAByteImage image1(widthImg,	heighImg,	1,	-1,	im_reduce1);	imageC			=	image1;
	CVisByteImage imagebw(widthImg,		heighImg,	1,	-1, im_cambw1);		imageBW[nCam]	=	imagebw;
	CVisByteImage imagesat(widthImg,	heighImg,	1,	-1, img1SatDiv3);	imageSat[nCam]	=	imagesat;
	CVisByteImage imagesa2(widthImg,	heighImg,	1,	-1,	img1Sa2Div3);	imageSa2[nCam]	=	imagesa2;
	CVisByteImage imageMage(widthImg,	heighImg,	1,	-1,	img1MagDiv3);	imageMag[nCam]	=	imageMage;
	CVisByteImage imageYell(widthImg,	heighImg,	1,	-1,	img1YelDiv3);	imageYel[nCam]	=	imageYell;
	CVisByteImage imageCyan(widthImg,	heighImg,	1,	-1,	img1CyaDiv3);	imageCya[nCam]	=	imageCyan;
	CVisByteImage imagebwB(widthImg,	heighImg,	1,	-1,	im_cam1Bbw);	imageBWB[nCam]	=	imagebwB;
	CVisByteImage imagebwG(widthImg,	heighImg,	1,	-1,	im_cam1Gbw);	imageBWG[nCam]	=	imagebwG;
	CVisByteImage imagebwR(widthImg,	heighImg,	1,	-1,	im_cam1Rbw);	imageBWR[nCam]	=	imagebwR;

	///////////////////////////////////////////////////

	//when teaching, this saves the image for the job dlgbox
	if(pframe->TeachPicC1)	
	{
		pframe->TeachPicC1=false;
		SaveTempPic(im_reduce1, 3);
	}

	int filterSelPatt =	theapp->jobinfo[pframe->CurrentJobNum].filterSelPatt;

	if(filterSelPatt==1)		{img1ForPattMatchDiv3 = im_cambw1;}
	else if(filterSelPatt==2)	{img1ForPattMatchDiv3 = img1SatDiv3;}
	else if(filterSelPatt==3)	{img1ForPattMatchDiv3 = img1MagDiv3;}
	else if(filterSelPatt==4)	{img1ForPattMatchDiv3 = img1YelDiv3;}
	else if(filterSelPatt==5)	{img1ForPattMatchDiv3 = img1CyaDiv3;}
	else if(filterSelPatt==6)	{img1ForPattMatchDiv3 = im_cam1Rbw;}
	else if(filterSelPatt==7)	{img1ForPattMatchDiv3 = im_cam1Gbw;}
	else if(filterSelPatt==8)	{img1ForPattMatchDiv3 = im_cam1Bbw;}

	edgeImgCreationC1(img1ForPattMatchDiv3, 3, nCam);
	
	CVisByteImage imageEdgH(widthImg,	heighImg,	1,	-1, im_cam1EdgH);	imageSobH[nCam]	=	imageEdgH;
	CVisByteImage imageEdgV(widthImg,	heighImg,	1,	-1, im_cam1EdgV);	imageSobV[nCam]	=	imageEdgV;
	CVisByteImage imagebin(widthImg,	heighImg,	1,	-1,	im_cam1Bin);	imageBin[nCam]	=	imagebin;
	
	///////////////////////////////////////////////////
	//this image : byteBin1Edges is used for Inspection 6
	edgeImgBinC1(im_cam1Rbw, im_cam1Gbw, im_cam1Bbw, 3, nCam);
	CVisByteImage imgbinEdg(widthImg,	heighImg,	1,	-1,	byteBin1Edges);	imageBinEdges[nCam]	=	imgbinEdg;

	///////////////////////////////////////////////////
 	//*** INSPECTION CAMERA 1
	labelPos[nCam]	=	Inspect1(im_reduce1,im_cambw1);
	/////////////////

	cameraDone[nCam][pframe->countC1]	= true;	//Tracking cam performance


	//###Adding new function for Clover Prune to find the label transition
	//FindingLabelTransition_Cam1(im_reduce1,1,3);
	/*
	if(theapp->inspctn[12].enable)
	{
		int imageWidth = (4*currentCamHeight)/3; 
		int imageHeight = currentCamWidth/3;
		int xOffSet = 212;
		int yOffSet = 158;
		int teachWidth = 3;
		int teachHeight = 3;

		int searchXOffSet = (theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound*4);
		int searchYOffSet = theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound;
		int searchWidth = ((theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound-theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound)*4);
		int searchHeight = (theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound-theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);

		int RTolerance = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtRTolerance;
		int GTolerance = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtGTolerance;
		int BTolerance = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtBTolerance;

		int numberOfPixelsToCheck = 5;

		int taughtR = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtR;
		int taughtG = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtG;
		int taughtB = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtB;;

		FindLabelTransition(im_reduce1, imageWidth, imageHeight,
							searchXOffSet, searchYOffSet, 
							searchWidth, searchHeight,
							taughtR, taughtG, taughtB,
							RTolerance, GTolerance, BTolerance,
							numberOfPixelsToCheck,
							vector_points1);

		int windowSize = 5;
		int deltaBetweenWindows = 3;
		int heightDeltaThreshold = 10;
		SinglePoint failingXCoordinate;
		int heightDeltaFound = 0;
		int cam_no=1;

		FindBadLabelMating(vector_points1, windowSize, deltaBetweenWindows, 
						   heightDeltaThreshold, failingXCoordinate, heightDeltaFound,cam_no);

		label_mating_results[cam_no].x=heightDeltaFound;
	}
*/

	cameraDone[nCam][pframe->countC1] = true;	//Tracking cam performance

	QueryPerformanceCounter(&m_lnC1EndI0); nDiff=0; timeMicroSecondC1I0= 0; //Time

	if( m_lnC1StartI0.QuadPart !=0 && m_lnC1EndI0.QuadPart != 0)
	{
		timeMicroSecondC1I0	= (double)(m_lnC1FreqI0.QuadPart/1000); 
		nDiff			= m_lnC1EndI0.QuadPart-m_lnC1StartI0.QuadPart; 
		timeMicroSecondC1I0	= nDiff/timeMicroSecondC1I0;
	}
}

void CXAxisView::Display2(BYTE *image)
{
	QueryPerformanceFrequency(&m_lnC2FreqI0); 
	QueryPerformanceCounter(&m_lnC2StartI0); //Timing

	int nCam = 2;

	if(first)
	{
//		QueryPerformanceFrequency(&Frequency); QueryPerformanceCounter(&timeStart);
		first=false;
		pframe->countC0.store(pframe->countC2);		
		SetTimer(1,theapp->inspDelay,NULL);
	}

	int currentCamWidth = theapp->camWidth[nCam];
	int currentCamHeight = theapp->camHeight[nCam];

	int newWidth = MainDisplayImageHeight;
	int newHeight = MainDisplayImageWidth;

	int offsetX = MainCameraDisplayXOffset;
	int offsetY = (currentCamWidth == MainDisplayImageHeight) ? MainCameraDisplayYOffset : CroppedImageYOffset;

	im_dataC2 = pframe->m_pcamera->m_imageProcessed[1][pframe->m_pcamera->readNext[1].load()]->GetData();
	/* HAS TO BE FIXED BECAUSE OF DIFFERENT SIZED SAVED IMAGES RELATED TO SWITCHING BETWEEN CAMERA SETTINGS (WAIST INSPECTION) - JUSTIN MARTIN */
	DuplicateImage(im_dataC2, im_dataC2FullRes, currentCamWidth, currentCamHeight, bytesPerPixel);

	CropCameraImage(im_dataC2FullRes, im_dataC2,
		currentCamWidth, currentCamHeight,
		newWidth, newHeight, offsetX, offsetY,
		bytesPerPixel);

	if(gotPic2 && (theapp->enabPicsToShow || !theapp->camsPresent))
	{

		ScaleImageDownBySubsampling(im_dataC2FullRes,
									currentCamWidth, currentCamHeight,bytesPerPixel,
									FullImageScaleDownFactor,
									cam2ImageFullScaled);

		Rotate90Right(cam2ImageFullScaled,
						FullImageScaledWidth, FullImageScaledHeight, bytesPerPixel,
						cam2ImageFullScaledRotated);
		
		ConvertFromBGRUToGreyscale(cam2ImageFullScaledRotated, 
								   FullImageScaledRotatedWidth,
								   FullImageScaledRotatedHeight, 
								   cam2ImageFullScaledRotatedGreyscale);

		im_reduce2			=	Reduce2(im_dataC2);	
	}
	else if(theapp->camsPresent)
	{
		//m_imageProcessed2.pData	=	pframe->m_pcamera->cam2ImageCropped;
	//	im_reduce2			=	Reduce2(pframe->m_pcamera->cam2ImageCropped);
		im_reduce2 = Reduce2(im_dataC2);

		if(theapp->waistAvailable)
		{
			//		WAIST INSPECTION SECTION
			ScaleImageDownBySubsampling(pframe->m_pcamera->cam2ImageFull,
									currentCamWidth, currentCamHeight, bytesPerPixel, FullImageScaleDownFactor,
									cam2ImageFullScaled);

			Rotate90Right(cam2ImageFullScaled,
						FullImageScaledWidth, FullImageScaledHeight, bytesPerPixel,
						cam2ImageFullScaledRotated);
		
			ConvertFromBGRUToGreyscale(cam2ImageFullScaledRotated, 
								   FullImageScaledRotatedWidth,
								   FullImageScaledRotatedHeight, 
								   cam2ImageFullScaledRotatedGreyscale);
		}
	}

	CVisRGBAByteImage cam2Large(FullImageScaledRotatedWidth, FullImageScaledRotatedHeight, 1,-1, cam2ImageFullScaledRotated);
	imageC2Large = cam2Large;	//paint image to screen

	/////////////////
	//Image Preprocessing
	int widthImg = MainDisplayImageWidth/3;
	int heighImg = MainDisplayImageHeight/3;

	CVisRGBAByteImage	image2(widthImg,	heighImg,	1,	-1,	im_reduce2);	imageC2			=	image2;
	CVisByteImage		imagebw(widthImg,	heighImg,	1,	-1,	im_cambw2);		imageBW[nCam]	=	imagebw;
	CVisByteImage		imagesat(widthImg,	heighImg,	1,	-1,	img2SatDiv3);	imageSat[nCam]	=	imagesat;
	CVisByteImage		imagesa2(widthImg,	heighImg,	1,	-1,	img2Sa2Div3);	imageSa2[nCam]	=	imagesa2;
	CVisByteImage		imageMage(widthImg,	heighImg,	1,	-1,	img2MagDiv3);	imageMag[nCam]	=	imageMage;
	CVisByteImage		imageYell(widthImg,	heighImg,	1,	-1,	img2YelDiv3);	imageYel[nCam]	=	imageYell;
	CVisByteImage		imageCyan(widthImg,	heighImg,	1,	-1,	img2CyaDiv3);	imageCya[nCam]	=	imageCyan;
	CVisByteImage		imagebwB(widthImg,	heighImg,	1,	-1,	im_cam2Bbw);	imageBWB[nCam]	=	imagebwB;
	CVisByteImage		imagebwG(widthImg,	heighImg,	1,	-1,	im_cam2Gbw);	imageBWG[nCam]	=	imagebwG;
	CVisByteImage		imagebwR(widthImg,	heighImg,	1,	-1,	im_cam2Rbw);	imageBWR[nCam]	=	imagebwR;

	///////////////////////////////////////////////////
	int filterSelPatt =	theapp->jobinfo[pframe->CurrentJobNum].filterSelPatt;

	if(filterSelPatt==1)		{img2ForPattMatchDiv3 = im_cambw2;}
	else if(filterSelPatt==2)	{img2ForPattMatchDiv3 = img2SatDiv3;}
	else if(filterSelPatt==3)	{img2ForPattMatchDiv3 = img2MagDiv3;}
	else if(filterSelPatt==4)	{img2ForPattMatchDiv3 = img2YelDiv3;}
	else if(filterSelPatt==5)	{img2ForPattMatchDiv3 = img2CyaDiv3;}
	else if(filterSelPatt==6)	{img2ForPattMatchDiv3 = im_cam2Rbw;}
	else if(filterSelPatt==7)	{img2ForPattMatchDiv3 = im_cam2Gbw;}
	else if(filterSelPatt==8)	{img2ForPattMatchDiv3 = im_cam2Bbw;}

	edgeImgCreationC2(img2ForPattMatchDiv3, 3, nCam);

	CVisByteImage		imageEdgH(widthImg,	heighImg,	1,	-1,	im_cam2EdgH);	imageSobH[nCam]		=	imageEdgH;
	CVisByteImage		imageEdgV(widthImg,	heighImg,	1,	-1, im_cam2EdgV);	imageSobV[nCam]		=	imageEdgV;
	CVisByteImage		imagebin(widthImg,	heighImg,	1,	-1, im_cam2Bin);	imageBin[nCam]		=	imagebin;
	
	///////////////////////////////////////////////////

	edgeImgBinC2(im_cam2Rbw, im_cam2Gbw, im_cam2Bbw, 3, nCam);

	CVisByteImage		imgbinEdg(widthImg,	heighImg,	1,	-1,	byteBin2Edges);	imageBinEdges[nCam]	=	imgbinEdg;

	///////////////////////////////////////////////////
	 //*** INSPECTION CAMERA 2
	labelPos[nCam]	=	Inspect2(im_reduce2,im_cambw2);
/*
	if(theapp->inspctn[12].enable)
	{
		//###Adding new function for Clover Prune to find the label transition
		//FindingLabelTransition_Cam2(im_reduce2,2,3);
		
		int imageWidth = (4*currentCamHeight)/3; 
		int imageHeight = currentCamWidth/3;

		int searchXOffSet = (theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound*4);
		int searchYOffSet = theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound;
		int searchWidth = ((theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound-theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound)*4);
		int searchHeight = (theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound-theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);

		int RTolerance = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtRTolerance;
		int GTolerance = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtGTolerance;
		int BTolerance = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtBTolerance;

		int numberOfPixelsToCheck = 5;

		int taughtR = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtR;
		int taughtG = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtG;
		int taughtB = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtB;

		FindLabelTransition(im_reduce2, imageWidth, imageHeight,
							searchXOffSet, searchYOffSet, 
							searchWidth, searchHeight,
							taughtR, taughtG, taughtB,
							RTolerance, GTolerance, BTolerance,
							numberOfPixelsToCheck,
							vector_points2);

		int windowSize = 5;
		int deltaBetweenWindows = 3;
		int heightDeltaThreshold = 10;
		SinglePoint failingXCoordinate;
		int heightDeltaFound = 0;
		int cam_no=2;

		FindBadLabelMating(vector_points2, windowSize, deltaBetweenWindows, 
						   heightDeltaThreshold, failingXCoordinate, heightDeltaFound,cam_no);

		label_mating_results[cam_no].x=heightDeltaFound;		
	}
*/

	/////////////////
	cameraDone[nCam][pframe->countC2]	= true;	//Tracking cam performance

	/////////////////

	QueryPerformanceCounter(&m_lnC2EndI0); nDiff=0; timeMicroSecondC2I0= 0;			//Time

	if( m_lnC2StartI0.QuadPart !=0 && m_lnC2EndI0.QuadPart != 0)
	{
		timeMicroSecondC2I0	= (double)(m_lnC2FreqI0.QuadPart/1000); 
		nDiff			= m_lnC2EndI0.QuadPart-m_lnC2StartI0.QuadPart; 
		timeMicroSecondC2I0	= nDiff/timeMicroSecondC2I0;
	}
}

void CXAxisView::Display3(BYTE *image)
{
	QueryPerformanceFrequency(&m_lnC3FreqI0);
	QueryPerformanceCounter(&m_lnC3StartI0); //Timing

	int nCam = 3;

	if(first)
	{
//		QueryPerformanceFrequency(&Frequency); QueryPerformanceCounter(&timeStart);
		first=false;
		pframe->countC0.store(pframe->countC3);		
		SetTimer(1,theapp->inspDelay,NULL);
	}

	int currentCamWidth = theapp->camWidth[nCam];
	int currentCamHeight = theapp->camHeight[nCam];

	int newWidth = MainDisplayImageHeight;
	int newHeight = MainDisplayImageWidth;

	int offsetX = MainCameraDisplayXOffset;
	int offsetY = (currentCamWidth == MainDisplayImageHeight) ? MainCameraDisplayYOffset : CroppedImageYOffset;

	im_dataC3 = pframe->m_pcamera->m_imageProcessed[2][pframe->m_pcamera->readNext[2].load()]->GetData();

	/* HAS TO BE FIXED BECAUSE OF DIFFERENT SIZED SAVED IMAGES RELATED TO SWITCHING BETWEEN CAMERA SETTINGS (WAIST INSPECTION) - JUSTIN MARTIN */
	DuplicateImage(im_dataC3, im_dataC3FullRes,
		currentCamWidth, currentCamHeight, bytesPerPixel);

	CropCameraImage(im_dataC3FullRes, im_dataC3,
		currentCamWidth, currentCamHeight,
		newWidth, newHeight, offsetX, offsetY,
		bytesPerPixel);


	if(gotPic3 && (theapp->enabPicsToShow|| !theapp->camsPresent))
	{
		ScaleImageDownBySubsampling(im_dataC3FullRes,
									currentCamWidth, currentCamHeight,bytesPerPixel,
									FullImageScaleDownFactor,
									cam3ImageFullScaled);

		Rotate270Right(cam3ImageFullScaled,
						FullImageScaledWidth, FullImageScaledHeight, bytesPerPixel,
						cam3ImageFullScaledRotated);
		
		ConvertFromBGRUToGreyscale(cam3ImageFullScaledRotated, 
								   FullImageScaledRotatedWidth,
								   FullImageScaledRotatedHeight, 
								   cam3ImageFullScaledRotatedGreyscale);

		im_reduce3			=	Reduce3(im_dataC3);	
	}
	else if(theapp->camsPresent)
	{
		//m_imageProcessed3.pData	=	pframe->m_pcamera->cam3ImageCropped;
//		im_reduce3			=	Reduce3(pframe->m_pcamera->cam3ImageCropped);
		im_reduce3 = Reduce3(im_dataC3);

		if(theapp->waistAvailable)
		{
			//		WAIST INSPECTION SECTION
			ScaleImageDownBySubsampling(pframe->m_pcamera->cam3ImageFull,
									currentCamWidth, currentCamHeight, bytesPerPixel, FullImageScaleDownFactor,
									cam3ImageFullScaled);

			Rotate270Right(cam3ImageFullScaled,
						FullImageScaledWidth, FullImageScaledHeight, bytesPerPixel,
						cam3ImageFullScaledRotated);
		
			ConvertFromBGRUToGreyscale(cam3ImageFullScaledRotated, 
								   FullImageScaledRotatedWidth,
								   FullImageScaledRotatedHeight, 
								   cam3ImageFullScaledRotatedGreyscale);
		}
	}

	CVisRGBAByteImage cam3Large(FullImageScaledRotatedWidth, FullImageScaledRotatedHeight, 1,-1, cam3ImageFullScaledRotated);
	imageC3Large = cam3Large;	//paint image to screen

	/////////////////
	//Image Preprocessing
	int widthImg = MainDisplayImageWidth/3;
	int heighImg = MainDisplayImageHeight/3;

	CVisRGBAByteImage	image3(widthImg,	heighImg,	1,	-1,	im_reduce3);	imageC3			=	image3;
	CVisByteImage		imagebw(widthImg,	heighImg,	1,	-1,	im_cambw3);		imageBW[nCam]	=	imagebw;
	CVisByteImage		imagesat(widthImg,	heighImg,	1,	-1,	img3SatDiv3);	imageSat[nCam]	=	imagesat;
	CVisByteImage		imagesa2(widthImg,	heighImg,	1,	-1,	img3Sa2Div3);	imageSa2[nCam]	=	imagesa2;
	CVisByteImage		imageMage(widthImg,	heighImg,	1,	-1,	img3MagDiv3);	imageMag[nCam]	=	imageMage;
	CVisByteImage		imageYell(widthImg,	heighImg,	1,	-1,	img3YelDiv3);	imageYel[nCam]	=	imageYell;
	CVisByteImage		imageCyan(widthImg,	heighImg,	1,	-1,	img3CyaDiv3);	imageCya[nCam]	=	imageCyan;
	CVisByteImage		imagebwB(widthImg,	heighImg,	1,	-1,	im_cam3Bbw);	imageBWB[nCam]	=	imagebwB;
	CVisByteImage		imagebwG(widthImg,	heighImg,	1,	-1,	im_cam3Gbw);	imageBWG[nCam]	=	imagebwG;
	CVisByteImage		imagebwR(widthImg,	heighImg,	1,	-1,	im_cam3Rbw);	imageBWR[nCam]	=	imagebwR;

	///////////////////////////////////////////////////
	int filterSelPatt =	theapp->jobinfo[pframe->CurrentJobNum].filterSelPatt;

	if(filterSelPatt==1)		{img3ForPattMatchDiv3 = im_cambw3;}
	else if(filterSelPatt==2)	{img3ForPattMatchDiv3 = img3SatDiv3;}
	else if(filterSelPatt==3)	{img3ForPattMatchDiv3 = img3MagDiv3;}
	else if(filterSelPatt==4)	{img3ForPattMatchDiv3 = img3YelDiv3;}
	else if(filterSelPatt==5)	{img3ForPattMatchDiv3 = img3CyaDiv3;}
	else if(filterSelPatt==6)	{img3ForPattMatchDiv3 = im_cam3Rbw;}
	else if(filterSelPatt==7)	{img3ForPattMatchDiv3 = im_cam3Gbw;}
	else if(filterSelPatt==8)	{img3ForPattMatchDiv3 = im_cam3Bbw;}

	edgeImgCreationC3(img3ForPattMatchDiv3, 3, nCam);
	
	CVisByteImage		imageEdgH(widthImg,	heighImg,	1,	-1, im_cam3EdgH);	imageSobH[nCam]	=	imageEdgH;
	CVisByteImage		imageEdgV(widthImg,	heighImg,	1,	-1, im_cam3EdgV);	imageSobV[nCam]	=	imageEdgV;
	CVisByteImage		imagebin(widthImg,	heighImg,	1,	-1, im_cam3Bin);	imageBin[nCam]	=	imagebin;

	///////////////////////////////////////////////////

	edgeImgBinC3(im_cam3Rbw, im_cam3Gbw, im_cam3Bbw, 3, nCam);

	CVisByteImage		imgbinEdg(widthImg,	heighImg,	1,	-1,	byteBin3Edges);	imageBinEdges[nCam]	=	imgbinEdg;

	///////////////////////////////////////////////////

 	//*** INSPECTION CAMERA 3
	labelPos[nCam]	=	Inspect3(im_reduce3,im_cambw3);
/*
	if(theapp->inspctn[12].enable)
	{
		//###Adding new function for Clover Prune to find the label transition
	//	FindingLabelTransition_Cam3(im_reduce3,3,3);
		int imageWidth = (4*currentCamHeight)/3; 
		int imageHeight = currentCamWidth/3;

		int searchXOffSet = (theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound*4);
		int searchYOffSet = theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound;
		int searchWidth = ((theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound-theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound)*4);
		int searchHeight = (theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound-theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);

		int RTolerance = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtRTolerance;
		int GTolerance = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtGTolerance;
		int BTolerance = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtBTolerance;

		int numberOfPixelsToCheck = 5;

		int taughtR = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtR;
		int taughtG = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtG;
		int taughtB = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtB;

		FindLabelTransition(im_reduce3, imageWidth, imageHeight,
							searchXOffSet, searchYOffSet, 
							searchWidth, searchHeight,
							taughtR, taughtG, taughtB,
							RTolerance, GTolerance, BTolerance,
							numberOfPixelsToCheck,
							vector_points3);

		int windowSize = 5;
		int deltaBetweenWindows = 3;
		int heightDeltaThreshold = 10;
		SinglePoint failingXCoordinate;
		int heightDeltaFound = 0;
		int cam_no=3;

		FindBadLabelMating(vector_points3, windowSize, deltaBetweenWindows, 
						   heightDeltaThreshold, failingXCoordinate, heightDeltaFound,cam_no);

		label_mating_results[cam_no].x=heightDeltaFound;
	}
*/
	cameraDone[nCam][pframe->countC3]	= true;	//Tracking cam performance

	/////////////////

	QueryPerformanceCounter(&m_lnC3EndI0); nDiff=0; timeMicroSecondC3I0= 0;			//Time

	if( m_lnC3StartI0.QuadPart !=0 && m_lnC3EndI0.QuadPart != 0)
	{
		timeMicroSecondC3I0	= (double)(m_lnC3FreqI0.QuadPart/1000); 
		nDiff			= m_lnC3EndI0.QuadPart-m_lnC3StartI0.QuadPart; 
		timeMicroSecondC3I0	= nDiff/timeMicroSecondC3I0;
	}
}

void CXAxisView::Display4(BYTE *image)
{
	bool debugging = false;
	QueryPerformanceFrequency(&m_lnC4FreqI0); QueryPerformanceCounter(&m_lnC4StartI0); //Timing

	int nCam = 4;

	if(first)
	{
//		QueryPerformanceFrequency(&Frequency); QueryPerformanceCounter(&timeStart);
		first=false;
		pframe->countC0.store(pframe->countC4);		
		SetTimer(1,theapp->inspDelay,NULL);
	}

	int currentCamWidth = theapp->camWidth[nCam];
	int currentCamHeight = theapp->camHeight[nCam];

	int newWidth = MainDisplayImageHeight;
	int newHeight = MainDisplayImageWidth;

	int offsetX = MainCameraDisplayXOffset;
	int offsetY = (currentCamWidth == MainDisplayImageHeight) ? MainCameraDisplayYOffset : CroppedImageYOffset;

	im_dataC4 = pframe->m_pcamera->m_imageProcessed[3][pframe->m_pcamera->readNext[3].load()]->GetData();

	/* HAS TO BE FIXED BECAUSE OF DIFFERENT SIZED SAVED IMAGES RELATED TO SWITCHING BETWEEN CAMERA SETTINGS (WAIST INSPECTION) - JUSTIN MARTIN */
	DuplicateImage(im_dataC4, im_dataC4FullRes, currentCamWidth, currentCamHeight, bytesPerPixel);

	CropCameraImage(im_dataC4FullRes, im_dataC4,
		currentCamWidth, currentCamHeight,
		newWidth, newHeight, offsetX, offsetY,
		bytesPerPixel);

	if(gotPic4 && (theapp->enabPicsToShow || !theapp->camsPresent))
	{

		ScaleImageDownBySubsampling(im_dataC4FullRes,
									currentCamWidth, currentCamHeight,bytesPerPixel,
									FullImageScaleDownFactor,
									cam4ImageFullScaled);

		Rotate90Right(cam4ImageFullScaled,
						FullImageScaledWidth, FullImageScaledHeight, bytesPerPixel,
						cam4ImageFullScaledRotated);
		
		ConvertFromBGRUToGreyscale(cam4ImageFullScaledRotated, 
								   FullImageScaledRotatedWidth,
								   FullImageScaledRotatedHeight, 
								   cam4ImageFullScaledRotatedGreyscale);

		im_reduce4			=	Reduce4(im_dataC4);	
	}
	else if(theapp->camsPresent)
	{
		//m_imageProcessed4.pData	=	pframe->m_pcamera->cam4ImageCropped;
//		im_reduce4			=	Reduce4(pframe->m_pcamera->cam4ImageCropped);
		im_reduce4 = Reduce4(im_dataC4);

		if(theapp->waistAvailable)
		{
			//		WAIST INSPECTION SECTION
			ScaleImageDownBySubsampling(pframe->m_pcamera->cam4ImageFull,
									currentCamWidth, currentCamHeight, bytesPerPixel, FullImageScaleDownFactor,
									cam4ImageFullScaled);

			Rotate90Right(cam4ImageFullScaled,
						FullImageScaledWidth, FullImageScaledHeight, bytesPerPixel,
						cam4ImageFullScaledRotated);
		
			ConvertFromBGRUToGreyscale(cam4ImageFullScaledRotated, 
								   FullImageScaledRotatedWidth,
								   FullImageScaledRotatedHeight, 
								   cam4ImageFullScaledRotatedGreyscale);
		}
	}

	CVisRGBAByteImage cam4Large(FullImageScaledRotatedWidth, FullImageScaledRotatedHeight, 1,-1, cam4ImageFullScaledRotated);
	imageC4Large = cam4Large;	//paint image to screen

	/////////////////
	//Image Preprocessing
	int widthImg = MainDisplayImageWidth/3;
	int heighImg = MainDisplayImageHeight/3;

	CVisRGBAByteImage	image4(widthImg,	heighImg,	1,	-1,	im_reduce4);	imageC4			=	image4;
	CVisByteImage		imagebw(widthImg,	heighImg,	1,	-1,	im_cambw4);		imageBW[nCam]	=	imagebw;
	CVisByteImage		imagesat(widthImg,	heighImg,	1,	-1,	img4SatDiv3);	imageSat[nCam]	=	imagesat;
	CVisByteImage		imagesa2(widthImg,	heighImg,	1,	-1,	img4Sa2Div3);	imageSa2[nCam]	=	imagesa2;
	CVisByteImage		imageMage(widthImg,	heighImg,	1,	-1,	img4MagDiv3);	imageMag[nCam]	=	imageMage;
	CVisByteImage		imageYell(widthImg,	heighImg,	1,	-1,	img4YelDiv3);	imageYel[nCam]	=	imageYell;
	CVisByteImage		imageCyan(widthImg,	heighImg,	1,	-1,	img4CyaDiv3);	imageCya[nCam]	=	imageCyan;
	CVisByteImage		imagebwB(widthImg,	heighImg,	1,	-1,	im_cam4Bbw);	imageBWB[nCam]	=	imagebwB;
	CVisByteImage		imagebwG(widthImg,	heighImg,	1,	-1,	im_cam4Gbw);	imageBWG[nCam]	=	imagebwG;
	CVisByteImage		imagebwR(widthImg,	heighImg,	1,	-1,	im_cam4Rbw);	imageBWR[nCam]	=	imagebwR;

	///////////////////////////////////////////////////
	int filterSelPatt =	theapp->jobinfo[pframe->CurrentJobNum].filterSelPatt;

	if(filterSelPatt==1)		{img4ForPattMatchDiv3 = im_cambw4;}
	else if(filterSelPatt==2)	{img4ForPattMatchDiv3 = img4SatDiv3;}
	else if(filterSelPatt==3)	{img4ForPattMatchDiv3 = img4MagDiv3;}
	else if(filterSelPatt==4)	{img4ForPattMatchDiv3 = img4YelDiv3;}
	else if(filterSelPatt==5)	{img4ForPattMatchDiv3 = img4CyaDiv3;}
	else if(filterSelPatt==6)	{img4ForPattMatchDiv3 = im_cam4Rbw;}
	else if(filterSelPatt==7)	{img4ForPattMatchDiv3 = im_cam4Gbw;}
	else if(filterSelPatt==8)	{img4ForPattMatchDiv3 = im_cam4Bbw;}

	edgeImgCreationC4(img4ForPattMatchDiv3, 3, nCam);
	
	CVisByteImage		imageEdgH(widthImg,	heighImg,	1,	-1, im_cam4EdgH);	imageSobH[nCam]	=	imageEdgH;
	CVisByteImage		imageEdgV(widthImg,	heighImg,	1,	-1, im_cam4EdgV);	imageSobV[nCam]	=	imageEdgV;
	CVisByteImage		imagebin(widthImg,	heighImg,	1,	-1,  im_cam4Bin);	imageBin[nCam]	=	imagebin;

	///////////////////////////////////////////////////

	edgeImgBinC4(im_cam4Rbw, im_cam4Gbw, im_cam4Bbw, 3, nCam);

	CVisByteImage		imgbinEdg(widthImg,	heighImg,	1,	-1,  byteBin4Edges);imageBinEdges[nCam]	=	imgbinEdg;

	///////////////////////////////////////////////////
	 //*** INSPECTION CAMERA 4
	labelPos[nCam]	=	Inspect4(im_reduce4,im_cambw4);

	/////////////////

	//only allow execution on debugging == true otherwise extra calls produced to save
	if(debugging)
	{
		SaveColCustom1(im_reduce4,widthImg,heighImg);
	}
/*
	if(theapp->inspctn[12].enable)
	{
		//###Adding new function for Clover Prune to find the label transition
		//FindingLabelTransition_Cam4(im_reduce4,4,3);
		int imageWidth = (4*currentCamHeight)/3; 
		int imageHeight = currentCamWidth/3;

		int searchXOffSet = (theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound*4);
		int searchYOffSet = theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound;
		int searchWidth = ((theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound-theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound)*4);
		int searchHeight = (theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound-theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);

		int RTolerance = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtRTolerance;
		int GTolerance = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtGTolerance;
		int BTolerance = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtBTolerance;

		int numberOfPixelsToCheck = 5;

		int taughtR = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtR;
		int taughtG = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtG;
		int taughtB = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtB;

		FindLabelTransition(im_reduce4, imageWidth, imageHeight,
							searchXOffSet, searchYOffSet, 
							searchWidth, searchHeight,
							taughtR, taughtG, taughtB,
							RTolerance, GTolerance, BTolerance,
							numberOfPixelsToCheck,
							vector_points4);

		int windowSize = 5;
		int deltaBetweenWindows = 3;
		int heightDeltaThreshold = 10;
		SinglePoint failingXCoordinate;
		int heightDeltaFound = 0;
		int cam_no=4;

		FindBadLabelMating(vector_points4, windowSize, deltaBetweenWindows, 
						   heightDeltaThreshold, failingXCoordinate, heightDeltaFound,cam_no);

		label_mating_results[cam_no].x=heightDeltaFound;
	}
*/
	cameraDone[nCam][pframe->countC4]	= true;	//Tracking cam performance

	/////////////////

	QueryPerformanceCounter(&m_lnC4EndI0); nDiff=0; timeMicroSecondC4I0= 0;			//Time

	if( m_lnC4StartI0.QuadPart !=0 && m_lnC4EndI0.QuadPart != 0)
	{
		timeMicroSecondC4I0	= (double)(m_lnC4FreqI0.QuadPart/1000); 
		nDiff			= m_lnC4EndI0.QuadPart-m_lnC4StartI0.QuadPart; 
		timeMicroSecondC4I0	= nDiff/timeMicroSecondC4I0;
	}
}

void CXAxisView::Display5(BYTE *image)
{
	QueryPerformanceFrequency(&m_lnC5FreqI0); QueryPerformanceCounter(&m_lnC5StartI0); //Timing

	int nCam = 5;

	if(first)
	{
//		QueryPerformanceFrequency(&Frequency); QueryPerformanceCounter(&timeStart);
		first=false;
		pframe->countC0.store(pframe->countC5);		
		SetTimer(1,theapp->inspDelay,NULL);
	}
	

	if(gotPic5 && (theapp->enabPicsToShow || !theapp->camsPresent))
	{	im_reduce5			=	Reduce5(im_dataC5);	}
	else if(theapp->camsPresent)
	{
//		m_imageProcessed[4]	=	pframe->m_pcamera->m_imageProcessed[4];
		im_reduce5			=	Reduce5(pframe->m_pcamera->m_imageProcessed[4][pframe->m_pcamera->readNext[4]]->GetData());
	}
	
	int heigh = MainDisplayImageWidth/2; 
	int width = MainDisplayImageHeight/2;

	CVisRGBAByteImage	image5(width,	heigh,	1,	-1,	im_reduce5);	imageC5			=	image5;
	CVisByteImage		imagebw(width,	heigh,	1,	-1,	im_cambw5);		imageBW[nCam]	=	imagebw;
	CVisByteImage		imagebwB(width, heigh,	1,	-1,	im_cam5Bbw);	imageBWB[nCam]	=	imagebwB;
	CVisByteImage		imagebwG(width, heigh,	1,	-1,	im_cam5Gbw);	imageBWG[nCam]	=	imagebwG;
	CVisByteImage		imagebwR(width, heigh,	1,	-1,	im_cam5Rbw);	imageBWR[nCam]	=	imagebwR;

	///////////////////////////////////////////////////
		
	//*** INSPECTIONS WITH CAMERA 5
	if(theapp->cam5Enable)
	{Inspect5(im_reduce5,im_cambw5);}

	/////////////////

	cameraDone[nCam][pframe->countC5]	= true;	//Tracking cam performance

	/////////////////

	QueryPerformanceCounter(&m_lnC5EndI0); nDiff=0; timeMicroSecondC5I0= 0;			//Time

	if( m_lnC5StartI0.QuadPart !=0 && m_lnC5EndI0.QuadPart != 0)
	{
		timeMicroSecondC5I0	= (double)(m_lnC5FreqI0.QuadPart/1000); 
		nDiff			= m_lnC5EndI0.QuadPart-m_lnC5StartI0.QuadPart; 
		timeMicroSecondC5I0	= nDiff/timeMicroSecondC5I0;
	}
}

void CXAxisView::OnDraw(CDC* pDC)
{
	//CDocument* pDoc = GetDocument();
	CClientDC dc(this);	
	CBrush Red, Blue, RedLine, Colorw, Gray, Black;
	CRect rect, a;

	COLORREF ycolor		= RGB(255,255,0);
	COLORREF greencolor	= RGB(0,255,0);
	COLORREF dgreencolor= RGB(0,190,0);
	COLORREF redcolor	= RGB(255,0,0);
	COLORREF bluecolor	= RGB(0,0,150);
	COLORREF lbluecolor	= RGB(168,218,242);
	COLORREF graycolor	= RGB(190,190,190);
	COLORREF blackcolor = RGB(0,0,0);
	COLORREF gray		= RGB(100,100,100);
	COLORREF wcolor		= RGB(255,255,255);
	COLORREF guidecolor = RGB(0,0,192);
	COLORREF guidecentercol = RGB(192,0,0);
	COLORREF aquacolor	= RGB(0,255,255);
	COLORREF corr		= graycolor;
	COLORREF corrGoodF	= redcolor;
	COLORREF corrGoodF1 = bluecolor;
	COLORREF corrGoodF2	= aquacolor;

	Black.CreateSolidBrush(blackcolor);
	Colorw.CreateSolidBrush(wcolor);
	CString m1,m2, m3, m4;
	Gray.CreateSolidBrush(RGB(192,192,192)); 
	Red.CreateSolidBrush(RGB(255,0,0)); 
	Blue.CreateSolidBrush(RGB(0,0,255));
	
	CPen BluePen(PS_SOLID, 1, RGB(0,0,250));
	CPen LGrayPen(PS_SOLID, 1, RGB(200,200,200));
	CPen LRedPen(PS_SOLID, 1, RGB(230,30,80));

	CPen FRedPen(PS_SOLID, 2, RGB(255,0,0));
	CPen RedDash(PS_DASH, 1, RGB(255,0,0));
	CPen BlackPen(PS_SOLID, 1, RGB(0,0,0));
	CPen GreenPen(PS_SOLID, 1, RGB(0,255,0));
	CPen FGreenPen(PS_SOLID, 3, RGB(0,255,0));
	CPen LGreenPen(PS_SOLID, 1, RGB(0,255,0));
	//CPen YellowPen(PS_DASH, 1, RGB(255,255,0));
	CPen WhitePen(PS_SOLID, 2, RGB(255,255,255));
	//CPen AquaPen(PS_SOLID, 4, RGB(0,255,255));
	//CPen GrayPen(PS_SOLID, 1, RGB(100,100,100));

	LOGPEN AquaPen;	
	AquaPen.lopnStyle = PS_SOLID;
	AquaPen.lopnWidth=CPoint(1, 105);	
	AquaPen.lopnColor = RGB(0, 255, 255);

	LOGPEN AquaPenDash;	
	AquaPenDash.lopnStyle = PS_DASH;
	AquaPenDash.lopnWidth=CPoint(1, 105);	
	AquaPenDash.lopnColor = RGB(0, 255, 255);
	
	LOGPEN AquaPen2;	
	AquaPen2.lopnStyle = PS_SOLID;
	AquaPen2.lopnWidth=CPoint(1, 105);	
	AquaPen2.lopnColor = RGB(0, 255, 255);

	LOGPEN YellowPen; 
	YellowPen.lopnStyle = PS_SOLID;	
	YellowPen.lopnWidth=CPoint(2, 105);	
	YellowPen.lopnColor = RGB(255, 255, 0);
	
	LOGPEN GrayPen;
	GrayPen.lopnStyle = PS_SOLID;	
	GrayPen.lopnWidth=CPoint(1, 105);	
	GrayPen.lopnColor = RGB(100, 100, 100);

	CPen grayPen;	grayPen.CreatePenIndirect(&GrayPen);
	CPen borderI10;	borderI10.CreatePenIndirect(&AquaPen);
	CPen resultI9;	resultI9.CreatePenIndirect(&GrayPen);
	CPen borderMidd;borderMidd.CreatePenIndirect(&AquaPen2);
	CPen ShiftTop;	ShiftTop.CreatePenIndirect(&AquaPen);
	CPen ShiftBot;	ShiftBot.CreatePenIndirect(&AquaPenDash);

	///guide pen
	CPen GuidePen(PS_DASH, 1, guidecolor);
	CPen GuidePenCenter(PS_DASH,1,guidecentercol);

	int PhotoBoxHeight = 360;

	if(enter_OnDraw)
	{
		//	TRACE("OnDraw 0x%X \n",AfxGetThread()->m_nThreadID);

		enter_OnDraw=false;
		int count=0;
		int xOffset=146;//43;
	
		GetClientRect(rect);
		pDC->SetBkMode(TRANSPARENT);
		pDC->SelectObject(&GrayPen);
		pDC->SetTextColor(wcolor);
	
		_tempCtr++;
		//if(_tempCtr>=2)
		//{
		//	pDC->FillRect(new CRect(1,1,580,380),&Black);
		//	_tempCtr=0;
		//}
		//else
		//{
			pDC->FillRect(rect, &Black);
		//}
		pDC->SetWindowOrg(-5,-10);

		int ncam =1;
		int szHalf = theapp->sizepattBy12/2;
		int xpos, ypos;
		int fct = 3;
		CString txtWidth; 

		int bottleStyle = theapp->jobinfo[pframe->CurrentJobNum].bottleStyle;

		bool showSetupI5 = theapp->jobinfo[pframe->CurrentJobNum].showSetupI5;
		bool showSetupI7 = theapp->jobinfo[pframe->CurrentJobNum].showSetupI7;
		bool showSetupI8 = theapp->jobinfo[pframe->CurrentJobNum].showSetupI8;

		int fact = 3;

		//**************cam1*****************************
		if(!theapp->showLargeImages && !theapp->showLargeImages2)
		{
			if(pframe->showMagentaImg){DisplayImage(imageMag[ncam], pDC);}
			else if(pframe->showCyanImg){DisplayImage(imageCya[ncam], pDC);}
			else if(pframe->showYellowImg){DisplayImage(imageYel[ncam], pDC);}
			else if(pframe->showSaturatImg){DisplayImage(imageSat[ncam], pDC);}
			else if(pframe->showSatura2Img){DisplayImage(imageSa2[ncam], pDC);}
			else if(pframe->showBWImg){DisplayImage(imageBW[ncam], pDC);}
			else if(pframe->showBWRedImg){DisplayImage(imageBWR[ncam], pDC);}
			else if(pframe->showBWGreenImg){DisplayImage(imageBWG[ncam], pDC);}
			else if(pframe->showBWBlueImg){DisplayImage(imageBWB[ncam], pDC);}
			else if(pframe->showEdgeHImg){DisplayImage(imageSobH[ncam], pDC);}
			else if(pframe->showEdgeVImg){DisplayImage(imageSobV[ncam], pDC);}
			else if(pframe->showBinImg){DisplayImage(imageBin[ncam], pDC);}
			else if(pframe->showEdgFinalImg){DisplayImage(imageBinEdges[ncam], pDC);}
			else if(pframe->showBinImgMetho5){DisplayImage(imageBinMeth5[ncam], pDC);}
			else{DisplayImage(imageC, pDC);}

			if(theapp->showGuide)
			{
				////commented out rectangle for cap area
				//pDC->Draw3dRect(28, 0, 78,30,guidecolor,guidecolor);

				//feature request for adding in guide lines
				DrawGuides(pDC,&GuidePen,&GuidePenCenter,xOffset,PhotoBoxHeight);				
			}

			////////////////////////////
			if(theapp->jobinfo[pframe->CurrentJobNum].showMiddles)
			{
				//Middle of the bottle
				pDC->SelectObject(&WhitePen);
				int taughtMiddleRefY = theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]/fct;
				pDC->MoveTo(0, taughtMiddleRefY);	pDC->LineTo(150,taughtMiddleRefY);

				//////////////////
				//Middle with Edge Detection
				pDC->SelectObject(&borderI10);
				pDC->MoveTo(bottleXL[ncam]/fct, bottleYL[ncam]/fct-5); pDC->LineTo(bottleXL[ncam]/fct,	bottleYL[ncam]/fct+5);
				pDC->MoveTo(bottleXR[ncam]/fct, bottleYR[ncam]/fct-5); pDC->LineTo(bottleXR[ncam]/fct,	bottleYR[ncam]/fct+5);

				pDC->SelectObject(&borderMidd);
				pDC->MoveTo(xMidd[ncam]/fct,	bottleYL[ncam]/fct-10); pDC->LineTo(xMidd[ncam]/fct,	bottleYL[ncam]/fct+10);
			}

			////////////////////////////
			//orienArea
			if(Modify)
			{ 	
				if(theapp->jobinfo[pframe->CurrentJobNum].showEdges && bottleStyle==10)
				{
					/////////////////
					int w = theapp->jobinfo[pframe->CurrentJobNum].shiftWidth/3;
					pDC->Draw3dRect(xShift[ncam]/3, yShift[ncam]/3, w, hShift[ncam]/3, graycolor, graycolor);
					/////////////////

					pDC->SelectObject(&ShiftTop);
					int ylimTop = theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]/3;
					pDC->MoveTo(0, ylimTop);	pDC->LineTo(150,ylimTop);

					pDC->SelectObject(&ShiftBot);
					int ylimBot = theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]/3;
					pDC->MoveTo(0, ylimBot);	pDC->LineTo(150,ylimBot);
	
					pDC->SelectObject(&ShiftTop);
					int xlimIni =  theapp->jobinfo[pframe->CurrentJobNum].shiftHorIniLim/3;
					pDC->MoveTo(xlimIni, ylimTop);	pDC->LineTo(xlimIni,ylimBot);

					pDC->SelectObject(&ShiftBot);
					int xlimEnd =  theapp->jobinfo[pframe->CurrentJobNum].shiftHorEndLim/3;
					pDC->MoveTo(xlimEnd, ylimTop);	pDC->LineTo(xlimEnd,ylimBot);

					/////////////////////////////////
					pDC->SelectObject(&ShiftTop);
					pDC->MoveTo(xLBorder[ncam]/3, (ylimTop+ylimBot)/2);	pDC->LineTo(xRBorder[ncam]/3,(ylimTop+ylimBot)/2);

					/////////////////////////////////

					pDC->SetTextColor(aquacolor);
					//txtWidth.Format("%i",widthShift[ncam]);
					txtWidth.Format("[%i-%i]",minValOpenArea[ncam], maxValOpenArea[ncam]);
					pDC->TextOut(xLShift[ncam]/3 + 2, yLShift[ncam]/3 -20, txtWidth);
					//txtWidth.Format("Min%i",minValOpenArea[ncam]);
					//pDC->TextOut(xLShift[ncam]/3 + 5, yLShift[ncam]/3 -40, txtWidth);

					SinglePoint tmpSingPtL;	SinglePoint tmpSingPtR;

					//Middle found
					pDC->SelectObject(&ShiftTop);
					pDC->MoveTo(xLShift[ncam]/3, yLShift[ncam]/3 - 5);	pDC->LineTo(xLShift[ncam]/3, yLShift[ncam]/3 + 5);
					pDC->MoveTo(xRShift[ncam]/3, yRShift[ncam]/3 - 5);	pDC->LineTo(xRShift[ncam]/3, yRShift[ncam]/3 + 5);
					pDC->MoveTo(xLShift[ncam]/3, yLShift[ncam]/3);		pDC->LineTo(xRShift[ncam]/3,yRShift[ncam]/3);

					//Points for Skewnes
					if(qtyShiftLeft[ncam]>0)
					{
						for(int i=1; i<qtyShiftLeft[ncam]; i++)
						{
							tmpSingPtL = xLShiftPts[ncam][i];
							pDC->Draw3dRect(tmpSingPtL.x/3, tmpSingPtL.y/3, 2, 2, redcolor,		redcolor);
						}
					}

					if(qtyShiftRigh[ncam]>0)
					{
						for(int i=1; i<qtyShiftRigh[ncam]; i++)
						{
							tmpSingPtR = xRShiftPts[ncam][i];
							pDC->Draw3dRect(tmpSingPtR.x/3, tmpSingPtR.y/3, 2, 2, greencolor,	greencolor);
						}
					}

					pDC->MoveTo(xL1[ncam]/fact, yL1[ncam]);	pDC->LineTo(xL2[ncam]/fact, yL2[ncam]);
					pDC->MoveTo(xR1[ncam]/fact, yR1[ncam]);	pDC->LineTo(xR2[ncam]/fact, yR2[ncam]);
				}

				if(showSetupI7)
				{
					//Top & Bottom search area limits
					pDC->SelectObject(&LRedPen);

					int ylim = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt;    
					pDC->MoveTo(0, ylim);	pDC->LineTo(150,ylim);
		
					pDC->SelectObject(&RedDash);
					ylim = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt;
					pDC->MoveTo(0, ylim);	pDC->LineTo(150,ylim);

					//// New set of Correlation LEFT, MIDDLE, RIGHT
					if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2)
					{
						if(bestcamPattBy6==ncam)	
						{
							xpos=posPattBy6.x/fct;	ypos=posPattBy6.y/fct;
							pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4,corrGoodF, corrGoodF);
				
							pDC->SetTextColor(redcolor);
							pDC->TextOut(xpos-szHalf*4+2,ypos-szHalf*4, txtBestPatt);
						}
					}
					else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3)
					{
						int w	=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/3;
						int h	=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/3;
						xpos	=	labelPos[ncam].x;
						ypos	=	labelPos[ncam].y; 

						if(nPaint[ncam]>0)
						{
							for(int i=0; i<nPaint[ncam]; i++)
							{pDC->Draw3dRect(xPaint[ncam][i]/3,yPaint[ncam][i]/3, 2, 2,greencolor, greencolor);}
						}

						pDC->SetTextColor(aquacolor);//redcolor

						CString txtavgBlock; txtavgBlock.Format("%i", avgBlock[ncam]);
						pDC->TextOut(xpos+5,ypos+50, txtavgBlock);

						//CString txtEdgBlock; txtEdgBlock.Format("%i", edgeBlock[ncam]);
						//pDC->TextOut(xpos+5,ypos+80, txtEdgBlock);

						pDC->Draw3dRect(xpos,ypos, w, h,corr, corr);

						if(ncam==camToCheck){pDC->Draw3dRect(xpos,ypos, w, h,corrGoodF, corrGoodF);}
					}
					else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4)
					{
						if(ncam==camToCheck)	
						{
							xpos	=	labelBox.x; ypos	=	labelBox.y; 
							pDC->Draw3dRect(xpos,ypos, 15, 15,corrGoodF1, corrGoodF1);
						}
					}
					else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5)
					{
						int tmpXt = bxMethod5_Xt[ncam]/fct; 
						int tmpYt = theapp->jobinfo[pframe->CurrentJobNum].boxYt/fct;
						int tmpWt = theapp->jobinfo[pframe->CurrentJobNum].boxWt/fct;
						int tmpHt = theapp->jobinfo[pframe->CurrentJobNum].boxHt/fct;

						int tmpXb = bxMethod5_Xb[ncam]/fct; 
						int tmpYb = theapp->jobinfo[pframe->CurrentJobNum].boxYb/fct;
						int tmpWb = theapp->jobinfo[pframe->CurrentJobNum].boxWt/fct;
						int tmpHb = theapp->jobinfo[pframe->CurrentJobNum].boxHb/fct;

						pDC->Draw3dRect(tmpXt, tmpYt, tmpWt, tmpHt, greencolor,	greencolor);
						pDC->Draw3dRect(tmpXb, tmpYb, tmpWb, tmpHb, redcolor,	redcolor);
			
						///////////Results
						int w	=	theapp->jobinfo[pframe->CurrentJobNum].widtMet5/3;
						int h	=	theapp->jobinfo[pframe->CurrentJobNum].heigMet5/3;

						pDC->Draw3dRect(labelPos[ncam].x/3,		labelPos[ncam].y/3 - h/2, w, h, ycolor, ycolor);

						pDC->Draw3dRect(labelPos[ncam].x/3,		labelPos[ncam].y/3-10, 2, 20,wcolor,blackcolor);
						pDC->Draw3dRect(labelPos[ncam].x/3-5,	labelPos[ncam].y/3, 10, 2, wcolor, blackcolor);
					}
					else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6)
					{
						if(bestcamPattBy6==ncam)	
						{
							xpos=posPattBy6.x/fct;	ypos=posPattBy6.y/fct;
							pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4,corrGoodF, corrGoodF);

							pDC->SetTextColor(redcolor);
							pDC->TextOut(xpos-szHalf*4+2,ypos-szHalf*4, txtBestPatt);
						}
					}
					/*
					if(bestcamPattBy12==ncam)	
					{
						xpos=posPattBy12.x/fct;	ypos=posPattBy12.y/fct;
						pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4,corrGoodF, corrGoodF);

						pDC->SetTextColor(redcolor);
						pDC->TextOut(xpos-szHalf*4+2,ypos-szHalf*4, txtBestPatt);
					}
					else 
					{
						if(camLeft==ncam){xpos=pattLeftPosBy6[ncam].x/fct; ypos=pattLeftPosBy6[ncam].y/fct;}
						if(camMidd==ncam){xpos=pattMiddPosBy6[ncam].x/fct; ypos=pattMiddPosBy6[ncam].y/fct;}
						if(camRigh==ncam){xpos=pattRighPosBy6[ncam].x/fct; ypos=pattRighPosBy6[ncam].y/fct;}

						if(camLeft==ncam || camMidd==ncam || camRigh==ncam)
						{pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4, corr, corr);}
					}*/
				}

				if(cam==1 && showSetupI7)
				{
					pDC->Draw3dRect(labelBox.x,labelBox.y,10,10,greencolor,greencolor);

					pDC->Draw3dRect(labelPos[cam].x,	labelPos[cam].y-12, 2, 24,wcolor,blackcolor);
					pDC->Draw3dRect(labelPos[cam].x-5,	labelPos[cam].y, 10, 2,wcolor,blackcolor);
	
					if(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle==3)	//wolverine
					{
						pDC->Draw3dRect(46, theapp->boltOffset2-theapp->boltOffset, 40, theapp->boltOffset,wcolor,blackcolor);
						int y;
						for(y=0; y<=4; y++)
						{pDC->Draw3dRect(ledgePoint[y].x,ledgePoint[y].y,3,3,greencolor,greencolor);}

						if(theapp->DoNewLedge)
						{
							for(y=6; y<=24; y++)
							{pDC->Draw3dRect(ledgePoint[y].x,ledgePoint[y].y,2,2,lbluecolor,lbluecolor);}
						}
					}
				}

				if(showSetupI5)
				{
					int yposI5 = theapp->jobinfo[pframe->CurrentJobNum].orienArea;
					pDC->SetTextColor(greencolor); 	pDC->TextOut(xdistPatt, yposI5-15,"I5");

					pDC->Draw3dRect(xdistPatt, yposI5, (MainDisplayImageWidth/3)-2*xdistPatt, 26, greencolor, greencolor);
				}

				if(showSetupI8)
				{
					//pDC->Draw3dRect(30,theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset, 70, 15,greencolor,greencolor);
					pDC->Draw3dRect(xNoLbl[ncam],yNoLbl[ncam], wNoLbl[ncam], hNoLbl[ncam],greencolor,greencolor);

					pDC->SetTextColor(greencolor); 	
					CString txtAvg; txtAvg.Format("%i", avgNoLabel[ncam]);
					pDC->TextOut(30, theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset+10, txtAvg);
				}
			}

			//----- Show cam1 label mating setup -----------------------------------------------------------------------------------------------------------------------------------------------------
			if(theapp->jobinfo[pframe->CurrentJobNum].showLabelMating)
			{
				//Write result to screen.
				//	CString Result11Cam1String;
				//	Result11Cam1String.Format(_T("%.3f"), Result11Cam1);//
				//	pDC->SetTextColor(aquacolor);
				//	pDC->TextOut(10, 10, Result11Cam1String);

				//----------------------------------------------------------------------------------------------------------------------------------------------------------
				//--------------------------- Edge Search Area

				pDC->SelectObject(&LGreenPen);	
				//Top bound
				pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);  
				pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);	

				//Bottom bound
				pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);
				pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);		

				//Left bound
				pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);  
				pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);		

				//Right bound
				pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);  
				pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);	

				//----------------------------------------------------------------------------------------------------------------------------------------------------------
				//--------------------------- Color Teach Box
				pDC->SelectObject(&LRedPen);	
				//Top bound
				pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorTopBound);  
				pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorTopBound);	

				//Bottom bound
				pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorBottomBound);
				pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorBottomBound);		

				//Left bound
				pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorTopBound);  
				pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorBottomBound);		

				//Right bound
				pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorTopBound);  
				pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorBottomBound);
			}

			//#######
	/*
			if(vector_points1.size() > 0  && theapp->inspctn[12].enable)
			{
				for(int i=0;i<vector_points1.size();i++)
				{
					pDC->Draw3dRect(vector_points1[i].x,vector_points1[i].y,2,2,lbluecolor,lbluecolor);
				}
			}
	*/

			//#########
			//	Splice Tab
			if(theapp->labelOpen)
			{
				int fct =3;
				int xsplice = bottleXspliceIni[ncam]/fct;
				int ysplice = theapp->jobinfo[pframe->CurrentJobNum].ySplice[1]/fct;
				int wsplice = theapp->jobinfo[pframe->CurrentJobNum].wSplice/fct;
				int hsplice = theapp->jobinfo[pframe->CurrentJobNum].hSplice/fct;
				int hsplicesmBx = theapp->jobinfo[pframe->CurrentJobNum].hSpliceSmBx/fct;

				pDC->Draw3dRect(xsplice, ysplice, wsplice, hsplice, graycolor, graycolor);
				pDC->Draw3dRect(xsplice+4, yposSplice[ncam]/fct, wsplice-8, hsplicesmBx, greencolor, greencolor);
			}

			//	Stitch Tab
			if(theapp->tearOpen)
			{
				int fct =3;
				int	stitX = stitchX[ncam]/fct;	int stitY = stitchY[ncam]/fct;
				int stitW = stitchW/fct;		int stitH = stitchH/fct;
				pDC->Draw3dRect(stitX, stitY, stitW, stitH, graycolor, graycolor);
			}

			//**************cam2*****************************
			ncam =2; 
			pDC->SetWindowOrg(-xOffset-3,-10);
			if(pframe->showMagentaImg){DisplayImage(imageMag[ncam], pDC);}
			else if(pframe->showCyanImg){DisplayImage(imageCya[ncam], pDC);}
			else if(pframe->showYellowImg){DisplayImage(imageYel[ncam], pDC);}
			else if(pframe->showSaturatImg){DisplayImage(imageSat[ncam], pDC);}
			else if(pframe->showSatura2Img){DisplayImage(imageSa2[ncam], pDC);}
			else if(pframe->showBWImg){DisplayImage(imageBW[ncam], pDC);}
			else if(pframe->showBWRedImg){DisplayImage(imageBWR[ncam], pDC);}
			else if(pframe->showBWGreenImg){DisplayImage(imageBWG[ncam], pDC);}
			else if(pframe->showBWBlueImg){DisplayImage(imageBWB[ncam], pDC);}
			else if(pframe->showEdgeHImg){DisplayImage(imageSobH[ncam], pDC);}
			else if(pframe->showEdgeVImg){DisplayImage(imageSobV[ncam], pDC);}
			else if(pframe->showBinImg){DisplayImage(imageBin[ncam], pDC);}
			else if(pframe->showEdgFinalImg){DisplayImage(imageBinEdges[ncam], pDC);}
			else if(pframe->showBinImgMetho5){DisplayImage(imageBinMeth5[ncam], pDC);}
			else{DisplayImage(imageC2, pDC);}

			if(theapp->showGuide)
			{
				////commented out rectangle for cap area
				//pDC->Draw3dRect(28, 0, 78,30,guidecolor,guidecolor);
				//feature request for adding in guide lines
				DrawGuides(pDC,&GuidePen,&GuidePenCenter,xOffset,PhotoBoxHeight);
			}

		

			////////////////////////////

			if(theapp->jobinfo[pframe->CurrentJobNum].showMiddles)
			{
				//Middle of the bottle
				pDC->SelectObject(&WhitePen);
				int taughtMiddleRefY = theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]/fct;
				pDC->MoveTo(0, taughtMiddleRefY);	pDC->LineTo(150,taughtMiddleRefY);

				//////////////////
				//Middle with Edge Detection
				pDC->SelectObject(&borderI10);
				pDC->MoveTo(bottleXL[ncam]/fct, bottleYL[ncam]/fct-5); pDC->LineTo(bottleXL[ncam]/fct, bottleYL[ncam]/fct+5);
				pDC->MoveTo(bottleXR[ncam]/fct, bottleYR[ncam]/fct-5); pDC->LineTo(bottleXR[ncam]/fct, bottleYR[ncam]/fct+5);

				pDC->SelectObject(&borderMidd);
				pDC->MoveTo(xMidd[ncam]/fct,	bottleYL[ncam]/fct-5); pDC->LineTo(xMidd[ncam]/fct,		bottleYL[ncam]/fct+5);
			}

			////////////////////////////
			if( Modify)
			{ 
				if(theapp->jobinfo[pframe->CurrentJobNum].showEdges && bottleStyle==10)
				{
					/////////////////
					int w = theapp->jobinfo[pframe->CurrentJobNum].shiftWidth/3;
					pDC->Draw3dRect(xShift[ncam]/3, yShift[ncam]/3, w, hShift[ncam]/3, graycolor, graycolor);
					/////////////////

					pDC->SelectObject(&ShiftTop);
					int ylimTop = theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]/3;
					pDC->MoveTo(0, ylimTop);	pDC->LineTo(150,ylimTop);

					pDC->SelectObject(&ShiftBot);
					int ylimBot = theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]/3;
					pDC->MoveTo(0, ylimBot);	pDC->LineTo(150,ylimBot);

					pDC->SelectObject(&ShiftTop);
					int xlimIni =  theapp->jobinfo[pframe->CurrentJobNum].shiftHorIniLim/3;
					pDC->MoveTo(xlimIni, ylimTop);	pDC->LineTo(xlimIni,ylimBot);

					pDC->SelectObject(&ShiftBot);
					int xlimEnd =  theapp->jobinfo[pframe->CurrentJobNum].shiftHorEndLim/3;
					pDC->MoveTo(xlimEnd, ylimTop);	pDC->LineTo(xlimEnd,ylimBot);

					/////////////////////////////////
					pDC->SelectObject(&ShiftTop);
					pDC->MoveTo(xLBorder[ncam]/3, (ylimTop+ylimBot)/2);	pDC->LineTo(xRBorder[ncam]/3,(ylimTop+ylimBot)/2);
					/////////////////////////////////

					pDC->SetTextColor(aquacolor);
					//txtWidth.Format("%i",widthShift[ncam]);
					txtWidth.Format("[%i-%i]",minValOpenArea[ncam], maxValOpenArea[ncam]);
					pDC->TextOut(xLShift[ncam]/3 + 2, yLShift[ncam]/3 -20, txtWidth);
					//txtWidth.Format("Min%i",minValOpenArea[ncam]);
					//pDC->TextOut(xLShift[ncam]/3 + 5, yLShift[ncam]/3 -40, txtWidth);

					SinglePoint tmpSingPtL;	SinglePoint tmpSingPtR;

					//Middle found
					pDC->SelectObject(&ShiftTop);
					pDC->MoveTo(xLShift[ncam]/3, yLShift[ncam]/3 - 5);	pDC->LineTo(xLShift[ncam]/3, yLShift[ncam]/3 + 5);
					pDC->MoveTo(xRShift[ncam]/3, yRShift[ncam]/3 - 5);	pDC->LineTo(xRShift[ncam]/3, yRShift[ncam]/3 + 5);
					pDC->MoveTo(xLShift[ncam]/3, yLShift[ncam]/3);		pDC->LineTo(xRShift[ncam]/3,yRShift[ncam]/3);

					//Points for Skewnes
					if(qtyShiftLeft[ncam]>0)
					{
						for(int i=1; i<qtyShiftLeft[ncam]; i++)
						{
							tmpSingPtL = xLShiftPts[ncam][i];
							pDC->Draw3dRect(tmpSingPtL.x/3, tmpSingPtL.y/3, 2, 2, redcolor,		redcolor);
					}
				}

				if(qtyShiftRigh[ncam]>0)
				{
					for(int i=1; i<qtyShiftRigh[ncam]; i++)
					{
						tmpSingPtR = xRShiftPts[ncam][i];
						pDC->Draw3dRect(tmpSingPtR.x/3, tmpSingPtR.y/3, 2, 2, greencolor,	greencolor);
					}
				}

				pDC->MoveTo(xL1[ncam]/fact, yL1[ncam]);	pDC->LineTo(xL2[ncam]/fact, yL2[ncam]);
				pDC->MoveTo(xR1[ncam]/fact, yR1[ncam]);	pDC->LineTo(xR2[ncam]/fact, yR2[ncam]);
			}

			if(showSetupI5)
			{
				//pDC->Draw3dRect(8, theapp->jobinfo[pframe->CurrentJobNum].orienArea, (MainDisplayImageWidth/3)-16,26,dgreencolor,dgreencolor);
				int yposI5 = theapp->jobinfo[pframe->CurrentJobNum].orienArea;
				pDC->SetTextColor(greencolor); 	pDC->TextOut(xdistPatt, yposI5-15,"I5");
				pDC->Draw3dRect(xdistPatt, yposI5, (MainDisplayImageWidth/3)-2*xdistPatt, 26, greencolor, greencolor);
			}

			if(showSetupI7 && cam==2)
			{
				pDC->Draw3dRect(labelBox.x,labelBox.y,10,10,greencolor,greencolor);

				pDC->Draw3dRect(labelPos[cam].x,	labelPos[cam].y-10, 2, 20,wcolor,blackcolor);
				pDC->Draw3dRect(labelPos[cam].x-5,	labelPos[cam].y, 10, 2,wcolor,blackcolor);

				if(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle==3)	//wolverine
				{
					pDC->Draw3dRect(46, theapp->boltOffset2-theapp->boltOffset, 40, theapp->boltOffset,wcolor,blackcolor);
			
					int y;
					for(y=0; y<=4; y++)
					{pDC->Draw3dRect(ledgePoint[y].x,ledgePoint[y].y,3,3,greencolor,greencolor);}
			
					if(theapp->DoNewLedge)
					{
						for(y=6; y<=24; y++)
						{pDC->Draw3dRect(ledgePoint[y].x,ledgePoint[y].y,2,2,lbluecolor,lbluecolor);}
					}
				}
			}

			if(showSetupI7)
			{
				//// New set of Correlation LEFT, MIDDLE, RIGHT
				if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2)
				{
					if(bestcamPattBy6==ncam)	
					{
						xpos=posPattBy6.x/fct;	ypos=posPattBy6.y/fct;
						pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4,corrGoodF, corrGoodF);

						pDC->SetTextColor(redcolor);
						pDC->TextOut(xpos-szHalf*4+2,ypos-szHalf*4, txtBestPatt);
					}
				}
				else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3)
				{
					int w	=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/3;
					int h	=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/3;
					xpos	=	labelPos[ncam].x;
					ypos	=	labelPos[ncam].y; 

					if(nPaint[ncam]>0)
					{
						for(int i=0; i<nPaint[ncam]; i++)
						{pDC->Draw3dRect(xPaint[ncam][i]/3,yPaint[ncam][i]/3, 2, 2,greencolor, greencolor);}
					}

					pDC->SetTextColor(aquacolor);//redcolor

					CString txtavgBlock; txtavgBlock.Format("%i", avgBlock[ncam]);
					pDC->TextOut(xpos+5,ypos+50, txtavgBlock);

					//CString txtEdgBlock; txtEdgBlock.Format("%i", edgeBlock[ncam]);
					//pDC->TextOut(xpos+5,ypos+80, txtEdgBlock);

					pDC->Draw3dRect(xpos,ypos, w, h,corr, corr);

					if(ncam==camToCheck){pDC->Draw3dRect(xpos,ypos, w, h,corrGoodF, corrGoodF);}
				}
				else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4)
				{
					if(ncam==camToCheck)	
					{
						xpos	=	labelBox.x; ypos =	labelBox.y; 

						pDC->Draw3dRect(xpos,ypos, 15, 15,corrGoodF1, corrGoodF1);
					}
				}
				else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5)
				{
					int tmpXt = bxMethod5_Xt[ncam]/fct; 
					int tmpYt = theapp->jobinfo[pframe->CurrentJobNum].boxYt/fct;
					int tmpWt = theapp->jobinfo[pframe->CurrentJobNum].boxWt/fct;
					int tmpHt = theapp->jobinfo[pframe->CurrentJobNum].boxHt/fct;

					int tmpXb = bxMethod5_Xb[ncam]/fct; 
					int tmpYb = theapp->jobinfo[pframe->CurrentJobNum].boxYb/fct;
					int tmpWb = theapp->jobinfo[pframe->CurrentJobNum].boxWt/fct;
					int tmpHb = theapp->jobinfo[pframe->CurrentJobNum].boxHb/fct;

					pDC->Draw3dRect(tmpXt, tmpYt, tmpWt, tmpHt, greencolor,	greencolor);
					pDC->Draw3dRect(tmpXb, tmpYb, tmpWb, tmpHb, redcolor,	redcolor);

					///////////Results
					int w	=	theapp->jobinfo[pframe->CurrentJobNum].widtMet5/3;
					int h	=	theapp->jobinfo[pframe->CurrentJobNum].heigMet5/3;

					pDC->Draw3dRect(labelPos[ncam].x/3,		labelPos[ncam].y/3 - h/2, w, h, ycolor, ycolor);

					pDC->Draw3dRect(labelPos[ncam].x/3,		labelPos[ncam].y/3-10, 2, 20,wcolor,blackcolor);
					pDC->Draw3dRect(labelPos[ncam].x/3-5,	labelPos[ncam].y/3, 10, 2, wcolor, blackcolor);
				}
				else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6)
				{
					if(bestcamPattBy6==ncam)	
					{
						xpos=posPattBy6.x/fct;	ypos=posPattBy6.y/fct;
						pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4,corrGoodF, corrGoodF);

						pDC->SetTextColor(redcolor);
						pDC->TextOut(xpos-szHalf*4+2,ypos-szHalf*4, txtBestPatt);
					}
				}
				/*
				if(bestcamPattBy12==ncam)	
				{
					xpos=posPattBy12.x/fct;	ypos=posPattBy12.y/fct;
					pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4,corrGoodF, corrGoodF);

					pDC->SetTextColor(redcolor);
					pDC->TextOut(xpos-szHalf*4+2,ypos-szHalf*4, txtBestPatt);
				}
				else 
				{
					if(camLeft==ncam){xpos=pattLeftPosBy6[ncam].x/fct; ypos=pattLeftPosBy6[ncam].y/fct;}
					if(camMidd==ncam){xpos=pattMiddPosBy6[ncam].x/fct; ypos=pattMiddPosBy6[ncam].y/fct;}
					if(camRigh==ncam){xpos=pattRighPosBy6[ncam].x/fct; ypos=pattRighPosBy6[ncam].y/fct;}

					if(camLeft==ncam || camMidd==ncam || camRigh==ncam)
					{pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4, corr, corr);}
				}*/
				///////////////////////
			}

			if(showSetupI8)
			{
				//pDC->Draw3dRect(30,theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset, 70, 15,greencolor,greencolor);
				pDC->Draw3dRect(xNoLbl[ncam],yNoLbl[ncam], wNoLbl[ncam], hNoLbl[ncam],greencolor,greencolor);

				pDC->SetTextColor(greencolor); 	
				CString txtAvg; txtAvg.Format("%i", avgNoLabel[ncam]);
				pDC->TextOut(30, theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset+10, txtAvg);
			}
		}

		//#######
		//----- Show cam2 label mating setup -----------------------------------------------------------------------------------------------------------------------------------------------------
		if(theapp->jobinfo[pframe->CurrentJobNum].showLabelMating)
		{
			//Write result to screen.
			//	CString Result11Cam1String;
			//	Result11Cam1String.Format(_T("%.3f"), Result11Cam1);//
			//	pDC->SetTextColor(aquacolor);
			//	pDC->TextOut(10, 10, Result11Cam1String);
		
			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			pDC->SelectObject(&LGreenPen);	
			//Top bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);

			//Bottom bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);

			//Left bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);

			//Right bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);	
		}


	/*	
		if(vector_points2.size() > 0 && theapp->inspctn[12].enable)
		{
			for(int i=0;i<vector_points2.size();i++)
			{
				pDC->Draw3dRect(vector_points2[i].x,vector_points2[i].y,2,2,lbluecolor,lbluecolor);
			}
		}
	*/

		//#########
		//	Splice Tab
		if(theapp->labelOpen)
		{
			int fct =3;
			int xsplice = bottleXspliceIni[ncam]/fct;
			int ysplice = theapp->jobinfo[pframe->CurrentJobNum].ySplice[1]/fct;
			int wsplice = theapp->jobinfo[pframe->CurrentJobNum].wSplice/fct;
			int hsplice = theapp->jobinfo[pframe->CurrentJobNum].hSplice/fct;
			int hsplicesmBx = theapp->jobinfo[pframe->CurrentJobNum].hSpliceSmBx/fct;

			pDC->Draw3dRect(xsplice, ysplice, wsplice, hsplice, graycolor, graycolor);
			pDC->Draw3dRect(xsplice+4, yposSplice[ncam]/fct, wsplice-8, hsplicesmBx, greencolor, greencolor);
		}
	
		//	Stitch Tab
		if(theapp->tearOpen)
		{
			int fct =3;
			int stitX = stitchX[ncam]/fct;	int stitY = stitchY[ncam]/fct;
			int stitW = stitchW/fct;		int stitH = stitchH/fct;
			pDC->Draw3dRect(stitX, stitY, stitW, stitH, graycolor, graycolor);
		}

		//**************cam3*****************************
		ncam = 3;
		pDC->SetWindowOrg((-xOffset*2)-3,-10);
		if(pframe->showMagentaImg){DisplayImage(imageMag[ncam], pDC);}
		else if(pframe->showCyanImg){DisplayImage(imageCya[ncam], pDC);}
		else if(pframe->showYellowImg){DisplayImage(imageYel[ncam], pDC);}
		else if(pframe->showSaturatImg){DisplayImage(imageSat[ncam], pDC);}
		else if(pframe->showSatura2Img){DisplayImage(imageSa2[ncam], pDC);}
		else if(pframe->showBWImg){DisplayImage(imageBW[ncam], pDC);}
		else if(pframe->showBWRedImg){DisplayImage(imageBWR[ncam], pDC);}
		else if(pframe->showBWGreenImg){DisplayImage(imageBWG[ncam], pDC);}
		else if(pframe->showBWBlueImg){DisplayImage(imageBWB[ncam], pDC);}
		else if(pframe->showEdgeHImg){DisplayImage(imageSobH[ncam], pDC);}
		else if(pframe->showEdgeVImg){DisplayImage(imageSobV[ncam], pDC);}
		else if(pframe->showBinImg){DisplayImage(imageBin[ncam], pDC);}
		else if(pframe->showEdgFinalImg){DisplayImage(imageBinEdges[ncam], pDC);}
		else if(pframe->showBinImgMetho5){DisplayImage(imageBinMeth5[ncam], pDC);}
		else{DisplayImage(imageC3, pDC);}

		////////////////////////////
		if(theapp->jobinfo[pframe->CurrentJobNum].showMiddles)
		{
			//Middle of the bottle
			pDC->SelectObject(&WhitePen);
			int taughtMiddleRefY = theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]/fct;
			pDC->MoveTo(0, taughtMiddleRefY);	pDC->LineTo(150,taughtMiddleRefY);

			//////////////////
			//Middle with Edge Detection
			pDC->SelectObject(&borderI10);
			pDC->MoveTo(bottleXL[ncam]/fct, bottleYL[ncam]/fct-5); pDC->LineTo(bottleXL[ncam]/fct, bottleYL[ncam]/fct+5);
			pDC->MoveTo(bottleXR[ncam]/fct, bottleYR[ncam]/fct-5); pDC->LineTo(bottleXR[ncam]/fct, bottleYR[ncam]/fct+5);

			pDC->SelectObject(&borderMidd);
			pDC->MoveTo(xMidd[ncam]/fct,	bottleYL[ncam]/fct-5); pDC->LineTo(xMidd[ncam]/fct,		bottleYL[ncam]/fct+5);
		}

		////////////////////////////
		if(theapp->showGuide)	
		{
			////commented out rectangle for cap area
			//pDC->Draw3dRect(28, 0, 78,30,guidecolor,guidecolor);

			//feature request for adding in guide lines
			DrawGuides(pDC,&GuidePen,&GuidePenCenter,xOffset,PhotoBoxHeight);
		}
		

		if(Modify)
		{ 
			if(theapp->jobinfo[pframe->CurrentJobNum].showEdges && bottleStyle==10)
			{
				/////////////////
				int w = theapp->jobinfo[pframe->CurrentJobNum].shiftWidth/3;
				pDC->Draw3dRect(xShift[ncam]/3, yShift[ncam]/3, w, hShift[ncam]/3, graycolor, graycolor);
				/////////////////

				pDC->SelectObject(&ShiftTop);
				int ylimTop = theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]/3;
				pDC->MoveTo(0, ylimTop);	pDC->LineTo(150,ylimTop);

				pDC->SelectObject(&ShiftBot);
				int ylimBot = theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]/3;
				pDC->MoveTo(0, ylimBot);	pDC->LineTo(150,ylimBot);

				pDC->SelectObject(&ShiftTop);
				int xlimIni =  theapp->jobinfo[pframe->CurrentJobNum].shiftHorIniLim/3;
				pDC->MoveTo(xlimIni, ylimTop);	pDC->LineTo(xlimIni,ylimBot);
	
				pDC->SelectObject(&ShiftBot);
				int xlimEnd =  theapp->jobinfo[pframe->CurrentJobNum].shiftHorEndLim/3;
				pDC->MoveTo(xlimEnd, ylimTop);	pDC->LineTo(xlimEnd,ylimBot);

				/////////////////////////////////
				pDC->SelectObject(&ShiftTop);
				pDC->MoveTo(xLBorder[ncam]/3, (ylimTop+ylimBot)/2);	pDC->LineTo(xRBorder[ncam]/3,(ylimTop+ylimBot)/2);
				/////////////////////////////////

				pDC->SetTextColor(aquacolor);
				//txtWidth.Format("%i",widthShift[ncam]);
				txtWidth.Format("[%i-%i]",minValOpenArea[ncam], maxValOpenArea[ncam]);
				pDC->TextOut(xLShift[ncam]/3 + 2, yLShift[ncam]/3 -20, txtWidth);
				//txtWidth.Format("Min%i",minValOpenArea[ncam]);
				//pDC->TextOut(xLShift[ncam]/3 + 5, yLShift[ncam]/3 -40, txtWidth);

				SinglePoint tmpSingPtL;	SinglePoint tmpSingPtR;

				//Middle found
				pDC->SelectObject(&ShiftTop);
				pDC->MoveTo(xLShift[ncam]/3, yLShift[ncam]/3 - 5);	pDC->LineTo(xLShift[ncam]/3, yLShift[ncam]/3 + 5);
				pDC->MoveTo(xRShift[ncam]/3, yRShift[ncam]/3 - 5);	pDC->LineTo(xRShift[ncam]/3, yRShift[ncam]/3 + 5);
				pDC->MoveTo(xLShift[ncam]/3, yLShift[ncam]/3);		pDC->LineTo(xRShift[ncam]/3,yRShift[ncam]/3);

				//Points for Skewnes
				if(qtyShiftLeft[ncam]>0)
				{
					for(int i=1; i<qtyShiftLeft[ncam]; i++)
					{
						tmpSingPtL = xLShiftPts[ncam][i];
						pDC->Draw3dRect(tmpSingPtL.x/3, tmpSingPtL.y/3, 2, 2, redcolor,		redcolor);
					}
				}

				if(qtyShiftRigh[ncam]>0)
				{
					for(int i=1; i<qtyShiftRigh[ncam]; i++)
					{
						tmpSingPtR = xRShiftPts[ncam][i];
						pDC->Draw3dRect(tmpSingPtR.x/3, tmpSingPtR.y/3, 2, 2, greencolor,	greencolor);
					}
				}

				pDC->MoveTo(xL1[ncam]/fact, yL1[ncam]);	pDC->LineTo(xL2[ncam]/fact, yL2[ncam]);
				pDC->MoveTo(xR1[ncam]/fact, yR1[ncam]);	pDC->LineTo(xR2[ncam]/fact, yR2[ncam]);
			}

			if(showSetupI5)
			{
				//pDC->Draw3dRect(8, theapp->jobinfo[pframe->CurrentJobNum].orienArea, (MainDisplayImageWidth/3)-16,26,dgreencolor,dgreencolor);
				int yposI5 = theapp->jobinfo[pframe->CurrentJobNum].orienArea;
				pDC->SetTextColor(greencolor); 	pDC->TextOut(xdistPatt, yposI5-15,"I5");
				pDC->Draw3dRect(xdistPatt, yposI5, (MainDisplayImageWidth/3)-2*xdistPatt, 26, greencolor, greencolor);
			}

			if(showSetupI7)
			{
				//// New set of Correlation LEFT, MIDDLE, RIGHT
				if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2)
				{
					if(bestcamPattBy6==ncam)	
					{
						xpos=posPattBy6.x/fct;	ypos=posPattBy6.y/fct;
						pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4,corrGoodF, corrGoodF);

						pDC->SetTextColor(redcolor);
						pDC->TextOut(xpos-szHalf*4+2,ypos-szHalf*4, txtBestPatt);
					}
				}
				else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3)
				{
					int w	=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/3;
					int h	=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/3;
					xpos	=	labelPos[ncam].x;
					ypos	=	labelPos[ncam].y; 

					if(nPaint[ncam]>0)
					{
						for(int i=0; i<nPaint[ncam]; i++)
						{pDC->Draw3dRect(xPaint[ncam][i]/3,yPaint[ncam][i]/3, 2, 2,greencolor, greencolor);}
					}

					pDC->SetTextColor(aquacolor);//redcolor

					CString txtavgBlock; txtavgBlock.Format("%i", avgBlock[ncam]);
					pDC->TextOut(xpos+5,ypos+50, txtavgBlock);

					//CString txtEdgBlock; txtEdgBlock.Format("%i", edgeBlock[ncam]);
					//pDC->TextOut(xpos+5,ypos+80, txtEdgBlock);

					pDC->Draw3dRect(xpos,ypos, w, h,corr, corr);

					if(ncam==camToCheck){pDC->Draw3dRect(xpos,ypos, w, h,corrGoodF, corrGoodF);}
				}
				else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4)
				{
					if(ncam==camToCheck)	
					{
						xpos	=	labelBox.x; 	ypos	=	labelBox.y; 

						pDC->Draw3dRect(xpos,ypos, 15, 15,corrGoodF1, corrGoodF1);
					}
				}
				else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5)
				{
					int tmpXt = bxMethod5_Xt[ncam]/fct; 
					int tmpYt = theapp->jobinfo[pframe->CurrentJobNum].boxYt/fct;
					int tmpWt = theapp->jobinfo[pframe->CurrentJobNum].boxWt/fct;
					int tmpHt = theapp->jobinfo[pframe->CurrentJobNum].boxHt/fct;

					int tmpXb = bxMethod5_Xb[ncam]/fct; 
					int tmpYb = theapp->jobinfo[pframe->CurrentJobNum].boxYb/fct;
					int tmpWb = theapp->jobinfo[pframe->CurrentJobNum].boxWt/fct;
					int tmpHb = theapp->jobinfo[pframe->CurrentJobNum].boxHb/fct;

					pDC->Draw3dRect(tmpXt, tmpYt, tmpWt, tmpHt, greencolor,	greencolor);
					pDC->Draw3dRect(tmpXb, tmpYb, tmpWb, tmpHb, redcolor,	redcolor);

					///////////Results
					int w	=	theapp->jobinfo[pframe->CurrentJobNum].widtMet5/3;
					int h	=	theapp->jobinfo[pframe->CurrentJobNum].heigMet5/3;

					pDC->Draw3dRect(labelPos[ncam].x/3,		labelPos[ncam].y/3 - h/2, w, h, ycolor, ycolor);

					pDC->Draw3dRect(labelPos[ncam].x/3,		labelPos[ncam].y/3-10, 2, 20,wcolor,blackcolor);
					pDC->Draw3dRect(labelPos[ncam].x/3-5,	labelPos[ncam].y/3, 10, 2, wcolor, blackcolor);
				}
				else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6)
				{
					if(bestcamPattBy6==ncam)	
					{
						xpos=posPattBy6.x/fct;	ypos=posPattBy6.y/fct;
						pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4,corrGoodF, corrGoodF);

						pDC->SetTextColor(redcolor);
						pDC->TextOut(xpos-szHalf*4+2,ypos-szHalf*4, txtBestPatt);
					}
				}
				/*
				if(bestcamPattBy12==ncam)	
				{
					xpos=posPattBy12.x/fct;	ypos=posPattBy12.y/fct;
					pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4,corrGoodF, corrGoodF);

					pDC->SetTextColor(redcolor);
					pDC->TextOut(xpos-szHalf*4+2,ypos-szHalf*4, txtBestPatt);
				}
				else 
				{
					if(camLeft==ncam){xpos=pattLeftPosBy6[ncam].x/fct; ypos=pattLeftPosBy6[ncam].y/fct;}
					if(camMidd==ncam){xpos=pattMiddPosBy6[ncam].x/fct; ypos=pattMiddPosBy6[ncam].y/fct;}
					if(camRigh==ncam){xpos=pattRighPosBy6[ncam].x/fct; ypos=pattRighPosBy6[ncam].y/fct;}

					if(camLeft==ncam || camMidd==ncam || camRigh==ncam)
					{pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4, corr, corr);}
				}*/
				///////////////////////
			}

			if(showSetupI7 && cam==3)
			{
				bool debugging=false;

				if(labelBox.x != labelPos[cam].x)
				{bool stophere = true;}

				pDC->Draw3dRect(labelBox.x,labelBox.y,10,10,greencolor,greencolor);
				pDC->Draw3dRect(labelPos[cam].x,	labelPos[cam].y-12, 2, 24,wcolor,blackcolor);
				pDC->Draw3dRect(labelPos[cam].x-5,	labelPos[cam].y, 10, 2,wcolor,blackcolor);

				if(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle==3)	//wolverine
				{
					pDC->Draw3dRect(46, theapp->boltOffset2-theapp->boltOffset, 40, theapp->boltOffset,wcolor,blackcolor);

					int y;
					for(y=0; y<=4; y++)
					{pDC->Draw3dRect(ledgePoint[y].x,ledgePoint[y].y,3,3,greencolor,greencolor);}
		
					if(theapp->DoNewLedge)
					{
						for(y=6; y<=24; y++)
						{pDC->Draw3dRect(ledgePoint[y].x,ledgePoint[y].y,2,2,lbluecolor,lbluecolor);}
					}
				}
			}

			if(showSetupI8)
			{
				//pDC->Draw3dRect(30,theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset, 70, 15,greencolor,greencolor);
				pDC->Draw3dRect(xNoLbl[ncam],yNoLbl[ncam], wNoLbl[ncam], hNoLbl[ncam],greencolor,greencolor);

				pDC->SetTextColor(greencolor); 	
				CString txtAvg; txtAvg.Format("%i", avgNoLabel[ncam]);
				pDC->TextOut(30, theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset+10, txtAvg);
			}
		}

		//----- Show cam3 label mating setup -----------------------------------------------------------------------------------------------------------------------------------------------------
		if(theapp->jobinfo[pframe->CurrentJobNum].showLabelMating)
		{
			//Write result to screen.
			//	CString Result11Cam1String;
			//	Result11Cam1String.Format(_T("%.3f"), Result11Cam1);//
			//	pDC->SetTextColor(aquacolor);
			//	pDC->TextOut(10, 10, Result11Cam1String);
		
			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			pDC->SelectObject(&LGreenPen);	
			//Top bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);	

			//Bottom bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);		

			//Left bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);		

			//Right bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);
		}

		//#######
/*
		if(vector_points3.size() > 0 && theapp->inspctn[12].enable)
		{
			for(int i=0;i<vector_points3.size();i++)
			{
				pDC->Draw3dRect(vector_points3[i].x,vector_points3[i].y,2,2,lbluecolor,lbluecolor);
			}
		}

  */
		//#########	
		//	Splice Tab
		if(theapp->labelOpen)
		{
			int fct =3;
			int xsplice = bottleXspliceIni[ncam]/fct;
			int ysplice = theapp->jobinfo[pframe->CurrentJobNum].ySplice[1]/fct;
			int wsplice = theapp->jobinfo[pframe->CurrentJobNum].wSplice/fct;
			int hsplice = theapp->jobinfo[pframe->CurrentJobNum].hSplice/fct;
			int hsplicesmBx = theapp->jobinfo[pframe->CurrentJobNum].hSpliceSmBx/fct;

			pDC->Draw3dRect(xsplice, ysplice, wsplice, hsplice, graycolor, graycolor);
			pDC->Draw3dRect(xsplice+4, yposSplice[ncam]/fct, wsplice-8, hsplicesmBx, greencolor, greencolor);
		}

		//	Stitch Tab
		if(theapp->tearOpen)
		{
			int fct =3;
			int stitX = stitchX[ncam]/fct;	int stitY = stitchY[ncam]/fct;
			int stitW = stitchW/fct;		int stitH = stitchH/fct;
			pDC->Draw3dRect(stitX, stitY, stitW, stitH, graycolor, graycolor);
		}

		//**************cam4*****************************	
		ncam = 4;
		pDC->SetWindowOrg(-xOffset*3,-10);
		if(pframe->showMagentaImg){DisplayImage(imageMag[ncam], pDC);}
		else if(pframe->showCyanImg){DisplayImage(imageCya[ncam], pDC);}
		else if(pframe->showYellowImg){DisplayImage(imageYel[ncam], pDC);}
		else if(pframe->showSaturatImg){DisplayImage(imageSat[ncam], pDC);}
		else if(pframe->showSatura2Img){DisplayImage(imageSa2[ncam], pDC);}
		else if(pframe->showBWImg){DisplayImage(imageBW[ncam], pDC);}
		else if(pframe->showBWRedImg){DisplayImage(imageBWR[ncam], pDC);}
		else if(pframe->showBWGreenImg){DisplayImage(imageBWG[ncam], pDC);}
		else if(pframe->showBWBlueImg){DisplayImage(imageBWB[ncam], pDC);}
		else if(pframe->showEdgeHImg){DisplayImage(imageSobH[ncam], pDC);}
		else if(pframe->showEdgeVImg){DisplayImage(imageSobV[ncam], pDC);}
		else if(pframe->showBinImg){DisplayImage(imageBin[ncam], pDC);}
		else if(pframe->showEdgFinalImg){DisplayImage(imageBinEdges[ncam], pDC);}
		else if(pframe->showBinImgMetho5){DisplayImage(imageBinMeth5[ncam], pDC);}
		else{DisplayImage(imageC4, pDC);}

		if(theapp->showGuide)	
		{

			////commented out rectangle for cap area
			//pDC->Draw3dRect(28, 0, 78,30,guidecolor,guidecolor);
			
			//feature request for adding in guide lines
			DrawGuides(pDC,&GuidePen,&GuidePenCenter,xOffset,PhotoBoxHeight);
		}

		////////////////////////////
		if(theapp->jobinfo[pframe->CurrentJobNum].showMiddles)
		{
			//Middle of the bottle
			pDC->SelectObject(&WhitePen);
			int taughtMiddleRefY = theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]/fct;
			pDC->MoveTo(0, taughtMiddleRefY);	pDC->LineTo(150,taughtMiddleRefY);

			//////////////////
			//Middle with Edge Detection
			pDC->SelectObject(&borderI10);
			pDC->MoveTo(bottleXL[ncam]/fct, bottleYL[ncam]/fct-5); pDC->LineTo(bottleXL[ncam]/fct, bottleYL[ncam]/fct+5);
			pDC->MoveTo(bottleXR[ncam]/fct, bottleYR[ncam]/fct-5); pDC->LineTo(bottleXR[ncam]/fct, bottleYR[ncam]/fct+5);

			pDC->SelectObject(&borderMidd);
			pDC->MoveTo(xMidd[ncam]/fct,	bottleYL[ncam]/fct-5); pDC->LineTo(xMidd[ncam]/fct,		bottleYL[ncam]/fct+5);
		}

		////////////////////////////
		if(Modify)
		{ 
			if(theapp->jobinfo[pframe->CurrentJobNum].showEdges && bottleStyle==10)
			{
				/////////////////
				int w = theapp->jobinfo[pframe->CurrentJobNum].shiftWidth/3;
				pDC->Draw3dRect(xShift[ncam]/3, yShift[ncam]/3, w, hShift[ncam]/3, graycolor, graycolor);

				/////////////////
				pDC->SelectObject(&ShiftTop);
				int ylimTop = theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]/3;
				pDC->MoveTo(0, ylimTop);	pDC->LineTo(150,ylimTop);

				pDC->SelectObject(&ShiftBot);
				int ylimBot = theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]/3;
				pDC->MoveTo(0, ylimBot);	pDC->LineTo(150,ylimBot);

				pDC->SelectObject(&ShiftTop);
				int xlimIni =  theapp->jobinfo[pframe->CurrentJobNum].shiftHorIniLim/3;
				pDC->MoveTo(xlimIni, ylimTop);	pDC->LineTo(xlimIni,ylimBot);
		
				pDC->SelectObject(&ShiftBot);
				int xlimEnd =  theapp->jobinfo[pframe->CurrentJobNum].shiftHorEndLim/3;
				pDC->MoveTo(xlimEnd, ylimTop);	pDC->LineTo(xlimEnd,ylimBot);

				/////////////////////////////////
				pDC->SelectObject(&ShiftTop);
				pDC->MoveTo(xLBorder[ncam]/3, (ylimTop+ylimBot)/2);	pDC->LineTo(xRBorder[ncam]/3,(ylimTop+ylimBot)/2);

				/////////////////////////////////
				pDC->SetTextColor(aquacolor);
				//txtWidth.Format("%i",widthShift[ncam]);
				txtWidth.Format("[%i-%i]",minValOpenArea[ncam], maxValOpenArea[ncam]);
				pDC->TextOut(xLShift[ncam]/3 + 2, yLShift[ncam]/3 -20, txtWidth);
				//txtWidth.Format("Min%i",minValOpenArea[ncam]);
				//pDC->TextOut(xLShift[ncam]/3 + 5, yLShift[ncam]/3 -40, txtWidth);

				SinglePoint tmpSingPtL;	SinglePoint tmpSingPtR;

				//Middle found
				pDC->SelectObject(&ShiftTop);
				pDC->MoveTo(xLShift[ncam]/3, yLShift[ncam]/3 - 5);	pDC->LineTo(xLShift[ncam]/3, yLShift[ncam]/3 + 5);
				pDC->MoveTo(xRShift[ncam]/3, yRShift[ncam]/3 - 5);	pDC->LineTo(xRShift[ncam]/3, yRShift[ncam]/3 + 5);
				pDC->MoveTo(xLShift[ncam]/3, yLShift[ncam]/3);		pDC->LineTo(xRShift[ncam]/3,yRShift[ncam]/3);

				//Points for Skewnes
				if(qtyShiftLeft[ncam]>0)
				{
					for(int i=1; i<qtyShiftLeft[ncam]; i++)
					{
						tmpSingPtL = xLShiftPts[ncam][i];
						pDC->Draw3dRect(tmpSingPtL.x/3, tmpSingPtL.y/3, 2, 2, redcolor,		redcolor);
					}
				}

				if(qtyShiftRigh[ncam]>0)
				{
					for(int i=1; i<qtyShiftRigh[ncam]; i++)
					{
						tmpSingPtR = xRShiftPts[ncam][i];
						pDC->Draw3dRect(tmpSingPtR.x/3, tmpSingPtR.y/3, 2, 2, greencolor,	greencolor);
					}
				}

				pDC->MoveTo(xL1[ncam]/fact, yL1[ncam]);	pDC->LineTo(xL2[ncam]/fact, yL2[ncam]);
				pDC->MoveTo(xR1[ncam]/fact, yR1[ncam]);	pDC->LineTo(xR2[ncam]/fact, yR2[ncam]);
			}

			if(showSetupI7)
			{
				////New set of Correlation LEFT, MIDDLE, RIGHT
				if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod2)	
				{
					if(bestcamPattBy6==ncam)	
					{
						xpos=posPattBy6.x/fct;	ypos=posPattBy6.y/fct;
						pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4,corrGoodF, corrGoodF);

						pDC->SetTextColor(redcolor);
						pDC->TextOut(xpos-szHalf*4+2,ypos-szHalf*4, txtBestPatt);
					}
				}
				else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod3)
				{
					int w	=	theapp->jobinfo[pframe->CurrentJobNum].widtMet3/3;
					int h	=	theapp->jobinfo[pframe->CurrentJobNum].heigMet3/3;
					xpos	=	labelPos[ncam].x;
					ypos	=	labelPos[ncam].y; 

					if(nPaint[ncam]>0)
					{
						for(int i=0; i<nPaint[ncam]; i++)
						{pDC->Draw3dRect(xPaint[ncam][i]/3,yPaint[ncam][i]/3, 2, 2,greencolor, greencolor);}
					}

					pDC->SetTextColor(aquacolor);//redcolor

					CString txtavgBlock; txtavgBlock.Format("%i", avgBlock[ncam]);
					pDC->TextOut(xpos+5,ypos+50, txtavgBlock);

					//CString txtEdgBlock; txtEdgBlock.Format("%i", edgeBlock[ncam]);
					//pDC->TextOut(xpos+5,ypos+80, txtEdgBlock);

					//CString txtQtyDkBlock; txtQtyDkBlock.Format("Qty%i", nPaint[ncam]);
					//pDC->TextOut(xpos+5,ypos+120, txtQtyDkBlock);

					pDC->Draw3dRect(xpos,ypos, w, h,corr, corr);

					if(ncam==camToCheck){pDC->Draw3dRect(xpos,ypos, w, h,corrGoodF, corrGoodF);}
				}
				else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod4)
				{
					if(ncam==camToCheck)	
					{
						xpos	=	labelBox.x; 	ypos =	labelBox.y; 
						pDC->Draw3dRect(xpos,ypos, 15, 15,corrGoodF1, corrGoodF1);
					}
				}
				else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod5)
				{
					int tmpXt = bxMethod5_Xt[ncam]/fct; 
					int tmpYt = theapp->jobinfo[pframe->CurrentJobNum].boxYt/fct;
					int tmpWt = theapp->jobinfo[pframe->CurrentJobNum].boxWt/fct;
					int tmpHt = theapp->jobinfo[pframe->CurrentJobNum].boxHt/fct;

					int tmpXb = bxMethod5_Xb[ncam]/fct; 
					int tmpYb = theapp->jobinfo[pframe->CurrentJobNum].boxYb/fct;
					int tmpWb = theapp->jobinfo[pframe->CurrentJobNum].boxWt/fct;
					int tmpHb = theapp->jobinfo[pframe->CurrentJobNum].boxHb/fct;

					pDC->Draw3dRect(tmpXt, tmpYt, tmpWt, tmpHt, greencolor,	greencolor);
					pDC->Draw3dRect(tmpXb, tmpYb, tmpWb, tmpHb, redcolor,	redcolor);

					///////////Results
					int w	=	theapp->jobinfo[pframe->CurrentJobNum].widtMet5/3;
					int h	=	theapp->jobinfo[pframe->CurrentJobNum].heigMet5/3;

					pDC->Draw3dRect(labelPos[ncam].x/3,		labelPos[ncam].y/3 - h/2, w, h, ycolor, ycolor);

					pDC->Draw3dRect(labelPos[ncam].x/3,		labelPos[ncam].y/3-10, 2, 20,wcolor,blackcolor);
					pDC->Draw3dRect(labelPos[ncam].x/3-5,	labelPos[ncam].y/3, 10, 2, wcolor, blackcolor);
				}

				else if(theapp->jobinfo[pframe->CurrentJobNum].labelIntegrityUseMethod6)
				{
					if(bestcamPattBy6==ncam)	
					{
						xpos=posPattBy6.x/fct;	ypos=posPattBy6.y/fct;
						pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4,corrGoodF, corrGoodF);

						pDC->SetTextColor(redcolor);
						pDC->TextOut(xpos-szHalf*4+2,ypos-szHalf*4, txtBestPatt);
					}
				}
				/*
				if(bestcamPattBy12==ncam)	
				{
					xpos=posPattBy12.x/fct;	ypos=posPattBy12.y/fct;
					pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4,corrGoodF, corrGoodF);

					pDC->SetTextColor(redcolor);
					pDC->TextOut(xpos-szHalf*4+2,ypos-szHalf*4, txtBestPatt);
				}
				else 
				{
					if(camLeft==ncam){xpos=pattLeftPosBy6[ncam].x/fct; ypos=pattLeftPosBy6[ncam].y/fct;}
					if(camMidd==ncam){xpos=pattMiddPosBy6[ncam].x/fct; ypos=pattMiddPosBy6[ncam].y/fct;}
					if(camRigh==ncam){xpos=pattRighPosBy6[ncam].x/fct; ypos=pattRighPosBy6[ncam].y/fct;}

					if(camLeft==ncam || camMidd==ncam || camRigh==ncam)
					{pDC->Draw3dRect(xpos-szHalf*4,ypos-szHalf*4, 2*szHalf*4, 2*szHalf*4, corr, corr);}
				}*/
				///////////////////////
			}

			if(showSetupI5)
			{
				//pDC->Draw3dRect(8, theapp->jobinfo[pframe->CurrentJobNum].orienArea, (MainDisplayImageWidth/3)-16,26,dgreencolor,dgreencolor);
				int yposI5 = theapp->jobinfo[pframe->CurrentJobNum].orienArea;
				pDC->SetTextColor(greencolor); 	pDC->TextOut(xdistPatt, yposI5-15,"I5");
				pDC->Draw3dRect(xdistPatt, yposI5, (MainDisplayImageWidth/3)-2*xdistPatt, 26, greencolor, greencolor);
			}
		
			if(showSetupI8)
			{
				//pDC->Draw3dRect(30,theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset, 70, 15,greencolor,greencolor);
				pDC->Draw3dRect(xNoLbl[ncam],yNoLbl[ncam], wNoLbl[ncam], hNoLbl[ncam],greencolor,greencolor);

				pDC->SetTextColor(greencolor); 	
				CString txtAvg; txtAvg.Format("%i", avgNoLabel[ncam]);
				pDC->TextOut(30, theapp->jobinfo[pframe->CurrentJobNum].noLabelOffset+10, txtAvg);
			}
		
			if(showSetupI7 && cam==4)
			{
				pDC->Draw3dRect(labelBox.x,labelBox.y,10,10,greencolor,greencolor);
				pDC->Draw3dRect(labelPos[cam].x,	labelPos[cam].y-10, 2, 20,wcolor,blackcolor);
				pDC->Draw3dRect(labelPos[cam].x-5,	labelPos[cam].y, 10, 2,wcolor,blackcolor);

				if(theapp->jobinfo[pframe->CurrentJobNum].bottleStyle==3)	//wolverine
				{
					pDC->Draw3dRect(46, theapp->boltOffset2-theapp->boltOffset, 40, theapp->boltOffset,wcolor,blackcolor);
				
					int y;
					for(y=0; y<=4; y++)
					{pDC->Draw3dRect(ledgePoint[y].x,ledgePoint[y].y,3,3,greencolor,greencolor);}
				
					if(theapp->DoNewLedge)
					{
						for(y=6; y<=24; y++)
						{pDC->Draw3dRect(ledgePoint[y].x,ledgePoint[y].y,2,2,lbluecolor,lbluecolor);}
					}
				}
			}
		}

		//----- Show cam2 label mating setup -----------------------------------------------------------------------------------------------------------------------------------------------------
		if(theapp->jobinfo[pframe->CurrentJobNum].showLabelMating)
		{
			//Write result to screen.
			//	CString Result11Cam1String;
			//	Result11Cam1String.Format(_T("%.3f"), Result11Cam1);//
			//	pDC->SetTextColor(aquacolor);
			//	pDC->TextOut(10, 10, Result11Cam1String);
		
			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			pDC->SelectObject(&LGreenPen);	
			//Top bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);	

			//Bottom bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);		

			//Left bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);		

			//Right bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound, theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound);		
		}
	
		//#######

	/*
		if(vector_points4.size() > 0 && theapp->inspctn[12].enable)
		{
			for(int i=0;i<vector_points4.size();i++)
			{			
				pDC->Draw3dRect(vector_points4[i].x,vector_points4[i].y,2,2,lbluecolor,lbluecolor);
			}
		}
	*/

		//#########
		//	Splice Tab
		if(theapp->labelOpen)
		{
			int fct =3;
			int xsplice = bottleXspliceIni[ncam]/fct;
			int ysplice = theapp->jobinfo[pframe->CurrentJobNum].ySplice[1]/fct;
			int wsplice = theapp->jobinfo[pframe->CurrentJobNum].wSplice/fct;
			int hsplice = theapp->jobinfo[pframe->CurrentJobNum].hSplice/fct;
			int hsplicesmBx = theapp->jobinfo[pframe->CurrentJobNum].hSpliceSmBx/fct;

			pDC->Draw3dRect(xsplice, ysplice, wsplice, hsplice, graycolor, graycolor);
			pDC->Draw3dRect(xsplice+4, yposSplice[ncam]/fct, wsplice-8, hsplicesmBx, greencolor, greencolor);
		}

		//	Stitch Tab
		if(theapp->tearOpen)
		{
			int fct =3;
			int stitX = stitchX[ncam]/fct;	int stitY = stitchY[ncam]/fct;
			int stitW = stitchW/fct;		int stitH = stitchH/fct;
			pDC->Draw3dRect(stitX, stitY, stitW, stitH, graycolor, graycolor);
		}

		//***********************************************	
		bool showStLigh		=	theapp->jobinfo[pframe->CurrentJobNum].showStLigh;
		bool showStDark		=	theapp->jobinfo[pframe->CurrentJobNum].showStDark;
		bool showStEdges	=	theapp->jobinfo[pframe->CurrentJobNum].showStEdges;
		bool showStFinal	=	theapp->jobinfo[pframe->CurrentJobNum].showStFinal;
		bool showStbw		=	theapp->jobinfo[pframe->CurrentJobNum].showStbw;

		CRect stitchedRect1; //just to avoid empty stitched images
		CRect stitchedRect2; //just to avoid empty stitched images

		if(theapp->tearOpen)
		{
			pDC->SetWindowOrg(-100,0);

			if(showStLigh)
			{
				stitchedRect1 = imageStitchedLigh.Rect();
				if(stitchedRect1.bottom!=0 && stitchedRect1.right!=0){DisplayImage(imageStitchedLigh, pDC);}

				//////////
				pDC->SetWindowOrg(-100,-190);

				stitchedRect2 = imageStitchedLigh.Rect();
				if(stitchedRect2.bottom!=0 && stitchedRect2.right!=0){DisplayImage(imageStitchedFilLigh, pDC);}
			}

			if(showStDark)
			{
				stitchedRect1 = imageStitchedDark.Rect();
				if(stitchedRect1.bottom!=0 && stitchedRect1.right!=0){DisplayImage(imageStitchedDark, pDC);}

				//////////
				pDC->SetWindowOrg(-100,-190);

				stitchedRect2 = imageStitchedFilDark.Rect();
				if(stitchedRect2.bottom!=0 && stitchedRect2.right!=0){DisplayImage(imageStitchedFilDark, pDC);}
			}

			if(showStEdges)
			{
				stitchedRect1 = imageStitchedEdg.Rect();
				if(stitchedRect1.bottom!=0 && stitchedRect1.right!=0){DisplayImage(imageStitchedEdg, pDC);}

				//////////
				pDC->SetWindowOrg(-100,-190);

				stitchedRect2 = imageStitchedFilEdg.Rect();
				if(stitchedRect2.bottom!=0 && stitchedRect2.right!=0){DisplayImage(imageStitchedFilEdg, pDC);}
			}

			if(showStFinal)
			{
				stitchedRect1 = imageStitchedFilFinal.Rect();
				if(stitchedRect1.bottom!=0 && stitchedRect1.right!=0){DisplayImage(imageStitchedFilFinal, pDC);}
			}

			if(showStbw)
			{
				stitchedRect1 = imageStitchedbw.Rect();
				if(stitchedRect1.bottom!=0 && stitchedRect1.right!=0){DisplayImage(imageStitchedbw, pDC);}
			}

			//if(showStbw || showStEdg)
			//if(	showStLigh || showStDark || 
			//	showStEdges || showStFinal || showStbw)
			if(showStFinal)
			{
				//int x = xPattInStitched/fact;
				//int y = yPattInStitched/fact;

				//pDC->Draw3dRect(x, y, 2, 2, redcolor, redcolor);

				//int xSeam = seamRect.x/fact; int ySeam = seamRect.y/fact;
				//int wSeam = seamRect.w/fact; int hSeam = seamRect.h/fact;

				//pDC->Draw3dRect(xSeam, ySeam, wSeam, hSeam, lbluecolor, lbluecolor);

				////////////
				pDC->SelectObject(&LRedPen);

				int stitchH = theapp->jobinfo[pframe->CurrentJobNum].stitchH/fact;
				int xini, xend;

				for(int i=1; i<=4; i++)
				{
					xini = xAvoidIni[i]/fact; 
					pDC->MoveTo(xini, 0);	pDC->LineTo(xini, stitchH);

					xend = xAvoidEnd[i]/fact;
					pDC->MoveTo(xend, 0);	pDC->LineTo(xend, stitchH);

					pDC->MoveTo(xini, 0);	pDC->LineTo(xend, stitchH);
					pDC->MoveTo(xend, 0);	pDC->LineTo(xini, stitchH);
				}
			
				//for(int v=0; v<=25; v++)
				//{pDC->Draw3dRect(seamPoint[v].x,seamPoint[v].y,2,2,redcolor,redcolor);}
			}
		}

		//**************cam5*****************************
		if(theapp->cam5Enable && theapp->showCam5)
		{
			ncam = 5;

			pDC->SetWindowOrg(0,-160);
			if(pframe->showCapBWImg){DisplayImage(imageBW[ncam], pDC);}// //showBWImg
			else if(pframe->showCapBWRedImg){DisplayImage(imageBWR[ncam], pDC);}
			else if(pframe->showCapBWGreenImg){DisplayImage(imageBWG[ncam], pDC);}
			else if(pframe->showCapBWBlueImg){DisplayImage(imageBWB[ncam], pDC);}
			else{DisplayImage(imageC5, pDC);}

			//Show references for center
			////////////////////////////

			pDC->SetTextColor(graycolor);
			pDC->MoveTo(0, 90);
			pDC->LineTo(16, 90);  //cap width lines left
			pDC->LineTo(10, 85);
			pDC->LineTo(10, 95);
			pDC->LineTo(16, 90);

			pDC->MoveTo(11, 86);
			pDC->LineTo(11, 94);
			pDC->MoveTo(12, 87);
			pDC->LineTo(12, 93);
			pDC->MoveTo(13, 88);
			pDC->LineTo(13, 92);
			pDC->MoveTo(14, 89);
			pDC->LineTo(14, 91);
	
			pDC->TextOut(0,54,"Photo");
			pDC->TextOut(0,64,"-eye");
/*
			pDC->TextOut(430,10,"Max Height");
			pDC->TextOut(420,35,"Try to adjust");
			pDC->TextOut(420,50,"the motor ");
			pDC->TextOut(420,65,"height, so ");
			pDC->TextOut(420,80,"the cap falls");
			pDC->TextOut(420,95,"below the");
			pDC->TextOut(420,110,"red line");
*/

			//commented out text readout per Bob's request
			pDC->Draw3dRect(10,27,415,1,redcolor,redcolor);//right side  was 16 0n 85

			pDC->SelectObject(&grayPen);

			
			if(theapp->showGuide)
			{
			///test adding new line here
				DrawCapGuides(pDC,&GuidePen,&GuidePenCenter,90);
			///end test
			}
			else
			{
				pDC->MoveTo(0, theapp->jobinfo[pframe->CurrentJobNum].neckBarY);  //cap width lines left
				pDC->LineTo(150, theapp->jobinfo[pframe->CurrentJobNum].neckBarY);	
				
				pDC->MoveTo((theapp->jobinfo[pframe->CurrentJobNum].vertBarX), 5);  //cap width lines left
				pDC->LineTo((theapp->jobinfo[pframe->CurrentJobNum].vertBarX), 600);

				// Cap L - R results
				if(theapp->inspctn[1].enable)
				{
					pDC->Draw3dRect(CapLSide.x, CapLSide.y, 4,4, greencolor,greencolor);			//Left	Side
					pDC->Draw3dRect(CapRSide.x, CapRSide.y, 4,4, greencolor,greencolor);			//Right	Side
					pDC->Draw3dRect(CapLTop.x,	CapLTop.y,	4,4, greencolor,greencolor);			//Left	Top
					pDC->Draw3dRect(CapRTop.x,	CapRTop.y,	4,4, greencolor,greencolor);			//Right Top
				}

				// Cap Middle results
				if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)
				{
					pDC->SelectObject(&GreenPen);

					int sportPosY = theapp->jobinfo[pframe->CurrentJobNum].sportPos;
				
					pDC->MoveTo(CapLTop.x-5, sportPosY-5);
					pDC->LineTo(CapRTop.x+5, sportPosY-5);
					pDC->LineTo(CapRTop.x+5, sportPosY+5);
					pDC->LineTo(CapLTop.x-5, sportPosY+5);
					pDC->LineTo(CapLTop.x-5, sportPosY-5);
				
					pDC->Draw3dRect(sportBand.x, sportPosY, 3, 6, greencolor,greencolor);
					pDC->Draw3dRect(sportBand.x1,sportPosY, 3, 6, greencolor,greencolor);
				}
				else
				{
					pDC->Draw3dRect(MidTop.x, MidTop.y,4,4,greencolor,greencolor);}

					pDC->Draw3dRect(	NeckLLip.x+80, 
							NeckLLip.y-theapp->jobinfo[pframe->CurrentJobNum].capColorOffset,
							60,
							theapp->jobinfo[pframe->CurrentJobNum].capColorSize,
							greencolor,greencolor);

					if(neckDefectToShow && leftLipDefect)
					{pDC->Draw3dRect(NeckLLip.x-5,NeckLLip.y,20,3,redcolor,redcolor);}			//Left Side
					else
					{pDC->Draw3dRect(NeckLLip.x-5,NeckLLip.y,20,3,greencolor,greencolor);}		//Left Side

					if(neckDefectToShow && leftLipDefect==false)
					{pDC->Draw3dRect(NeckRLip.x-5,NeckRLip.y,20,3,redcolor,redcolor);}			//Right Side
					else
					{pDC->Draw3dRect(NeckRLip.x-5,NeckRLip.y,20,3,greencolor,greencolor);}		//Right Side
				}
			}

		}

		//**************Large Images*****************************
		if(theapp->showLargeImages)
		{
			//**************cam1 Large Image*****************************
			pDC->SetWindowOrg(0,-10); // -160
			DisplayImage(imageC1Large, pDC);

			//Write result to screen.
			CString Result11Cam1String;
			Result11Cam1String.Format(_T("%.3f"), Result11Cam1);//
			pDC->SetTextColor(aquacolor);
			pDC->TextOut(10, 10, Result11Cam1String);

			//	Write Label if selected
			if(theapp->Cam1SelectedForWaist)
			{
				pDC->SetTextColor(greencolor);
				pDC->TextOut(10, 40, _T("SEL"));
			}

			//Upper		
			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			pDC->SelectObject(&LRedPen);	
			//Top bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperTopBound);	

			//Bottom bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperBottomBound);
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperBottomBound);		
		
			//Left bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperBottomBound);		
	
			//Right bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperBottomBound);	

			//Waist
			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			pDC->SelectObject(&GreenPen);
			//Top bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementTopBound);	
	
			//Bottom bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementBottomBound);
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementBottomBound);		
		
			//Left bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementBottomBound);		
	
			//Right bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementBottomBound);	

			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			//Waist Width
			//pDC->SelectObject(&LRedPen);
			//pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayStartX, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayY);  
			//pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayEndX, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayY) ;
	
			SinglePoint tmp;
			/*
			if(cam1CoordinatesOfContainerLeftTopEdgePointsRaw.size() > 0)
			{		
				for(int n=0; n<cam1CoordinatesOfContainerLeftTopEdgePointsRaw.size(); n++)
				{
					tmp = cam1CoordinatesOfContainerLeftTopEdgePointsRaw[n];
					pDC->Draw3dRect(tmp.x, 
						tmp.y,
						2,2,redcolor,redcolor);
				}
			}
			*/

			if(cam1CoordinatesOfContainerLeftTopEdgePointsFilled.size() > 0)
			{		
				for(int n=0; n<cam1CoordinatesOfContainerLeftTopEdgePointsFilled.size(); n++)
				{
					tmp = cam1CoordinatesOfContainerLeftTopEdgePointsFilled[n];
					pDC->Draw3dRect(tmp.x, 
						tmp.y,
						2,2,greencolor,greencolor);
				}
			}

			if(cam1CoordinatesOfContainerLeftWaistEdgePointsFilled.size() > 0)
			{		
				for(int n=0; n<cam1CoordinatesOfContainerLeftWaistEdgePointsFilled.size(); n++)
				{
					tmp = cam1CoordinatesOfContainerLeftWaistEdgePointsFilled[n];
					pDC->Draw3dRect(tmp.x, 
						tmp.y,
						2,2,greencolor,greencolor);
				}
			}

			//**************cam4 Large Image*****************************
			pDC->SetWindowOrg(-322,-10); // -160
			DisplayImage(imageC4Large, pDC);

			//Write result to screen.
			CString Result11Cam4String;
			Result11Cam4String.Format(_T("%.3f"), Result11Cam4);//
			pDC->SetTextColor(aquacolor);
			pDC->TextOut(200, 10, Result11Cam4String);

			//	Write Label if selected
			if(!theapp->Cam1SelectedForWaist)
			{
				pDC->SetTextColor(greencolor);
				pDC->TextOut(200, 40, _T("SEL"));
			}

			//Upper		
			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			pDC->SelectObject(&LRedPen);
			//Top bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperTopBound);	
	
			//Bottom bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperBottomBound);
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperBottomBound);		
		
			//Left bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperBottomBound);		
	
			//Right bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperBottomBound);	

			//Waist
			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			pDC->SelectObject(&GreenPen);
			//Top bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementTopBound);	
	
			//Bottom bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementBottomBound);
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementBottomBound);		
		
			//Left bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementBottomBound);		
	
			//Right bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementBottomBound);	

			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			//Waist Width
			//pDC->SelectObject(&LRedPen);
			//pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayStartX, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayY);  
			//pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayEndX, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayY) ;
	
			if(cam4CoordinatesOfContainerLeftTopEdgePointsFilled.size() > 0)
			{		
				for(int n=0; n<cam4CoordinatesOfContainerLeftTopEdgePointsFilled.size(); n++)
				{
					tmp = cam4CoordinatesOfContainerLeftTopEdgePointsFilled[n];
					pDC->Draw3dRect(tmp.x, 
						tmp.y,
						2,2,greencolor,greencolor);
				}
			}

			if(cam4CoordinatesOfContainerLeftWaistEdgePointsFilled.size() > 0)
			{		
				for(int n=0; n<cam4CoordinatesOfContainerLeftWaistEdgePointsFilled.size(); n++)
				{
					tmp = cam4CoordinatesOfContainerLeftWaistEdgePointsFilled[n];
					pDC->Draw3dRect(tmp.x, 
						tmp.y,
						2,2,greencolor,greencolor);
				}
			}

			//Selection
			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			pDC->SetWindowOrg(0,-10); // -160
			pDC->SelectObject(&GreenPen);
		
			//Top bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionTopBound);	
	
			//Bottom bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionBottomBound);
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionBottomBound);		
		
			//Left bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionBottomBound);		
	
			//Right bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionBottomBound);	
		}

		//**************cam2 Large Image*****************************
		if(theapp->showLargeImages2)
		{
			pDC->SetWindowOrg(0,-10); // -160
			DisplayImage(imageC2Large, pDC);

			//Write result to screen.
			CString Result11Cam2String;
			Result11Cam2String.Format(_T("%.3f"), Result11Cam2);
			pDC->SetTextColor(aquacolor);
			pDC->TextOut(10, 10, Result11Cam2String);

			//	Write Label if selected
			if(theapp->Cam2SelectedForWaist)
			{
				pDC->SetTextColor(greencolor);
				pDC->TextOut(10, 40, _T("SEL"));
			}

			//Upper		
			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			pDC->SelectObject(&LRedPen);	
			//Top bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperTopBound);	
	
			//Bottom bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperBottomBound);
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperBottomBound);		
		
			//Left bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperBottomBound);		
	
			//Right bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperBottomBound);	

			//Waist
			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			pDC->SelectObject(&GreenPen);
			//Top bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementTopBound);	

			//Bottom bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementBottomBound);
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementBottomBound);		

			//Left bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementBottomBound);		
	
			//Right bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementBottomBound);	

			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			//Waist Width
			//pDC->SelectObject(&LRedPen);
			//pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayStartX, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayY);  
			//pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayEndX, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayY) ;

			SinglePoint tmp;
			/*
			if(cam1CoordinatesOfContainerLeftTopEdgePointsRaw.size() > 0)
			{		
				for(int n=0; n<cam1CoordinatesOfContainerLeftTopEdgePointsRaw.size(); n++)
				{
					tmp = cam1CoordinatesOfContainerLeftTopEdgePointsRaw[n];
					pDC->Draw3dRect(tmp.x, 
						tmp.y,
						2,2,redcolor,redcolor);
				}
			}
			*/

			if(cam2CoordinatesOfContainerLeftTopEdgePointsFilled.size() > 0)
			{		
				for(int n=0; n<cam2CoordinatesOfContainerLeftTopEdgePointsFilled.size(); n++)
				{
					tmp = cam2CoordinatesOfContainerLeftTopEdgePointsFilled[n];
					pDC->Draw3dRect(tmp.x, 
						tmp.y,
						2,2,greencolor,greencolor);
				}
			}

			if(cam2CoordinatesOfContainerLeftWaistEdgePointsFilled.size() > 0)
			{		
				for(int n=0; n<cam2CoordinatesOfContainerLeftWaistEdgePointsFilled.size(); n++)
				{
					tmp = cam2CoordinatesOfContainerLeftWaistEdgePointsFilled[n];
					pDC->Draw3dRect(tmp.x, 
						tmp.y,
						2,2,greencolor,greencolor);
				}
			}

			//**************cam3 Large Image*****************************
			pDC->SetWindowOrg(-322,-10); // -160
			DisplayImage(imageC3Large, pDC);

			//Write result to screen.
			CString Result11Cam3String;
			Result11Cam3String.Format(_T("%.3f"), Result11Cam3);//
			pDC->SetTextColor(aquacolor);
			pDC->TextOut(200, 10, Result11Cam3String);

			//	Write Label if selected
			if(!theapp->Cam2SelectedForWaist)
			{
				pDC->SetTextColor(greencolor);
				pDC->TextOut(200, 40, _T("SEL"));
			}

			//Upper		
			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			pDC->SelectObject(&LRedPen);
			//Top bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperTopBound);	
	
			//Bottom bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperBottomBound);
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperBottomBound);		

			//Left bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperBottomBound);		
	
			//Right bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperBottomBound);	

			//Waist
			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			pDC->SelectObject(&GreenPen);
			//Top bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementTopBound);	

			//Bottom bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementBottomBound);
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementBottomBound);		
		
			//Left bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementBottomBound);		
	
			//Right bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementBottomBound);	

			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			//Waist Width
			//pDC->SelectObject(&LRedPen);
			//pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayStartX, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayY);  
			//pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayEndX, theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayY) ;
		
			if(cam3CoordinatesOfContainerLeftTopEdgePointsFilled.size() > 0)
			{		
				for(int n=0; n<cam3CoordinatesOfContainerLeftTopEdgePointsFilled.size(); n++)
				{
					tmp = cam3CoordinatesOfContainerLeftTopEdgePointsFilled[n];
					pDC->Draw3dRect(tmp.x, 
						tmp.y,
						2,2,greencolor,greencolor);
				}
			}

			if(cam3CoordinatesOfContainerLeftWaistEdgePointsFilled.size() > 0)
			{		
				for(int n=0; n<cam3CoordinatesOfContainerLeftWaistEdgePointsFilled.size(); n++)
				{
					tmp = cam3CoordinatesOfContainerLeftWaistEdgePointsFilled[n];
					pDC->Draw3dRect(tmp.x, 
						tmp.y,
						2,2,greencolor,greencolor);
				}
			}

			//Selection
			//----------------------------------------------------------------------------------------------------------------------------------------------------------
			pDC->SetWindowOrg(0,-10); // -160
			pDC->SelectObject(&GreenPen);

			//Top bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionTopBound);	

			//Bottom bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionBottomBound);
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionBottomBound);		

			//Left bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionLeftBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionBottomBound);		
	
			//Right bound
			pDC->MoveTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionTopBound);  
			pDC->LineTo(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionRightBound, theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionBottomBound);	
		}

		pDC->SetWindowOrg(0,0);

		xOffset=0;

		if(theapp->rejectAllKrones)
		{
			CRect b;
			b.SetRect(100,100,300,200);
			pDC->SelectObject(&Red);
			pDC->FillRect(b,&Red);
			pDC->TextOut(zeroLine+130 ,150,"Reject From Labeler");
		}

		enter_OnDraw=true;
	}
	else
	{
		pDC->FillRect(rect, &Black);
	}

	pframe->m_pview->UpdateDisplay();
	pframe->m_psplice->UpdateDisplay2();
	
	//////////
	//trying to update cap edges found for left and right
	pframe->m_pcapSetup1->UpdateDisplay2();
	//////////
	pframe->m_pmod->UpdateDisplay2();
}

void CXAxisView::OnLoadPatternX_method6(int Job, int pattType)
{
	CVisByteImage image;
//	int pxWidth;  //pixel width
//	int pxHeight; //pixel height
//	BYTE pxValue; //pixel value

	int		szPatt6	=	theapp->sizepattBy6;
	int		szPatt3W=	theapp->sizepattBy3_2W;
	int		szPatt3H=	theapp->sizepattBy3_2H;

	bool	success	=	false;

	CString c;	c=itoa(Job,buf,10);
		
	////////////

	//char	buf[10];
	//char	currentpath[60];
	char	currentpathTxt2[60];
	char	currentpathTxt3[60];
	char	currentpathBmp2[60];
	char	currentpathBmp3[60];
	char	contgypathTxt2[60];
	char	contgypathTxt3[60];

	char	*pFileName = "c:\\silgandata\\pat\\";//drive
	CString diffError = "The label pattern diff scales needs to be taught first";
	BYTE	*pPatDivBy6;
	BYTE	*pPatDivBy3;

	strcpy(currentpathTxt2,pFileName);	strcat(currentpathTxt2,c);
	strcpy(currentpathBmp2,pFileName);	strcat(currentpathBmp2,c);
	strcpy(contgypathTxt2,pFileName);	strcat(contgypathTxt2,c);

	strcpy(currentpathTxt3,pFileName);	strcat(currentpathTxt3,c);
	strcpy(currentpathBmp3,pFileName);	strcat(currentpathBmp3,c);
	strcpy(contgypathTxt3,pFileName);	strcat(contgypathTxt3,c);

	///replacing if statements with switch statement
	//if(pattType==1)	
	//{
	//	strcat(currentpathTxt2,"\\M6imgBy6L.txt"); 
	//	strcat(currentpathTxt3,"\\M6imgBy3L.txt");
	//}
	//if(pattType==2)	
	//{
	//	strcat(currentpathTxt2,"\\M6imgBy6M.txt"); 
	//	strcat(currentpathTxt3,"\\M6imgBy3M.txt");
	//}
	//if(pattType==3)	
	//{
	//	strcat(currentpathTxt2,"\\M6imgBy6R.txt"); 
	//	strcat(currentpathTxt3,"\\M6imgBy3R.txt");
	//}

	switch(pattType)
	{
		case 1:
		{
			strcat(currentpathTxt2,"\\M6imgBy3L.txt");
			strcat(currentpathBmp2,"\\M6patBy3L.bmp");
			strcat(contgypathTxt2,"\\imgBy3L.txt");
			pPatDivBy3 = bytePattBy3_method6_1;

			strcat(currentpathTxt3,"\\M6imgBy6L.txt");
			strcat(currentpathBmp3,"\\M6patBy6L.bmp");
			strcat(contgypathTxt3,"\\imgBy6L.txt");
			pPatDivBy6 = bytePattBy6_method6_1;
			break;
		}
		case 2:
		{
			strcat(currentpathTxt2,"\\M6imgBy3M.txt"); 
			strcat(currentpathBmp2,"\\M6patBy3M.bmp"); 
			strcat(contgypathTxt2,"\\imgBy3M.txt");
			pPatDivBy3 = bytePattBy3_method6;

			strcat(currentpathTxt3,"\\M6imgBy6M.txt");
			strcat(currentpathBmp3,"\\M6patBy6M.bmp");
			strcat(contgypathTxt3,"\\imgBy6M.txt");
			pPatDivBy6 = bytePattBy6_method6;
			break;
		}
		case 3:
		{
			//copied this from OnLoadPatternX_method6_num3 (for code consolidation)
			strcat(currentpathTxt2,"\\M6imgBy3R_num3.txt");
			strcat(currentpathBmp2,"\\M6patBy3R_num3.bmp");
			strcat(contgypathTxt2,"\\imgBy3R.txt");
			pPatDivBy3 = bytePattBy3_method6_num3;

			strcat(currentpathTxt3,"\\M6imgBy6R_num3.txt");
			strcat(currentpathBmp3,"\\M6patBy6R_num3.bmp");
			strcat(contgypathTxt3,"\\imgBy6R.txt");
			pPatDivBy6 = bytePattBy6_method6_num3;
			break;
		}
		default:
		{
			//error if here
			pPatDivBy3 = NULL;
			pPatDivBy6 = NULL;
		}
	}

	/////////////////////

	CString N2; 
	char	bufDig2[2];
	int		digit2;

	CFileException ce;
	
	if(pPatDivBy3 != NULL)
	{
		//try loading the bmp first then the text file as a backup
		try
		{
			//image.ReadFile(currentpathBmp2);
			//pxWidth = image.Width();
			//pxHeight = image.Height();

			CVisByteImage imgDivBy3(theapp->sizepattBy3_2W, theapp->sizepattBy3_2H, 1, -1, pPatDivBy3);
			//CVisByteImage imgDivBy3(theapp->sizepattBy3_2W, theapp->sizepattBy3_2H, 1, -1, bytePattBy3[pattType-1]);
			imgDivBy3.ReadFile(currentpathBmp2);

			pframe->JobNoGood=false;
		}
		catch (CVisError e)  // "..." is bad - we could leak an exception object.
		{
			success=ImageFile.Open(currentpathTxt2, CFile::modeRead, &ce) > 0;

			if(success!=0)
			{
				for(int g=0; g<szPatt3W*szPatt3H; g++)
				{
					ImageFile.Read(bufDig2,3);
					N2	 =	bufDig2[0];
					N2	+=	bufDig2[1];
					N2	+=	bufDig2[2];
					digit2=	atoi(N2);
					
					N2	=	"";
					//if(pattType == 2)
					//{
					//	*(bytePattBy6_method6+g)=digit2;
					//}
					//else
					//{
					//	*(bytePattBy6_method6_1+g)=digit2;
					//}

					*(pPatDivBy3+g) = digit2;
					//*(bytePattBy3[pattType]+g) = digit2;
				}

				ImageFile.Close();
				pframe->JobNoGood=false;
			}
			else
			{
				//adding contingency to ensure that imgBy6 is used
				success=ImageFile.Open(contgypathTxt2, CFile::modeRead, &ce) > 0;

				if(success!=0)
				{
					for(int g=0; g<szPatt3W*szPatt3H; g++)
					{
						ImageFile.Read(bufDig2,3);
						N2	 =	bufDig2[0];
						N2	+=	bufDig2[1];
						N2	+=	bufDig2[2];
						digit2=	atoi(N2);
						
						N2	=	"";
						//if(pattType == 2)
						//{
						//	*(bytePattBy6_method6+g)=digit2;
						//}
						//else
						//{
						//	*(bytePattBy6_method6_1+g)=digit2;
						//}

						*(pPatDivBy3+g) = digit2;
						//*(bytePattBy3[pattType]+g) = digit2;
					}

					ImageFile.Close();
					pframe->JobNoGood=false;
				}
				else
				{

					AfxMessageBox(diffError + "\n" + contgypathTxt2);
					wehaveTempX=false;
				}
			}
		}
	}
	////////////////////

	CString N3; 
	char	bufDig3[2];
	int		digit3;

//try loading the bmp first then the text file as a backup
	if(pPatDivBy6 != NULL)
	{
		try
		{
			//CVisByteImage imgDivBy6(theapp->sizepattBy6, theapp->sizepattBy6, 1, -1, bytePattBy6[pattType-1]);
			CVisByteImage imgDivBy6(theapp->sizepattBy6, theapp->sizepattBy6, 1, -1, pPatDivBy6);
			imgDivBy6.ReadFile(currentpathBmp3);

			pframe->JobNoGood=false;
		}
		catch (CVisError e)  // "..." is bad - we could leak an exception object.
		{
			success=ImageFile.Open(currentpathTxt3, CFile::modeRead, &ce) > 0;

			if(success!=0)
			{
				for(int g=0; g<szPatt6*szPatt6; g++)
				{
					ImageFile.Read(bufDig3,3);
					N3	 =	bufDig3[0];
					N3	+=	bufDig3[1];
					N3	+=	bufDig3[2];
					digit3=	atoi(N3);
					
					N3	=	"";

					//if(pattType == 2)
					//{
					//	*(bytePattBy3_method6+g)=digit3;
					//}
					//else
					//{
					//	*(bytePattBy3_method6_1+g)=digit3;
					//}
					*(pPatDivBy6+g) = digit3;
					//*(bytePattBy3[pattType]+g) = digit3;
				}

				ImageFile.Close();
				pframe->JobNoGood=false;
			}
			else
			{
				//adding contingency to ensure that imgBy6 is used
				success=ImageFile.Open(contgypathTxt3, CFile::modeRead, &ce) > 0;

				if(success!=0)
				{
					for(int g=0; g<szPatt6*szPatt6; g++)
					{
						ImageFile.Read(bufDig3,3);
						N3	 =	bufDig3[0];
						N3	+=	bufDig3[1];
						N3	+=	bufDig3[2];
						digit3=	atoi(N3);
						
						N3	=	"";

						*(pPatDivBy6+g) = digit3;
					}

					ImageFile.Close();
					pframe->JobNoGood=false;
				}
				else
				{

					AfxMessageBox(diffError + "\n" + contgypathTxt3);
					wehaveTempX=false;
				}
			}
		}
	}
	/////////////////////

	pPatDivBy6 = NULL;
	pPatDivBy3 = NULL;

//	bool debugging=false;
//	if(debugging)	{SaveBWCustom1(bytePattBy6[pattType], szPatt6,	szPatt6);}
//	if(debugging)	{SaveBWCustom1(bytePattBy3[pattType], szPatt3W, szPatt3H);}
}

void CXAxisView::FindingLabelTransition_Cam4(BYTE *Image, int nCam, int factor)//factor is always 3 in the 51R85
{
	int newLengthBWDiv3		=	(4*CroppedCameraHeight)/factor;

	int newHeightBWDiv3		=	  CroppedCameraWidth/factor;

	int starty= 200 ;// Arbitrary start line
	int endy=50 ;
	int startx=0;
	int endx= newLengthBWDiv3  - 30;
	int pixB1=0,pixG1=0,pixR1=0,pixB2=0,pixG2=0,pixR2=0,pixB3=0,pixG3=0,pixR3=0;
	SinglePoint point;
	int threshold=40;
	int avg_R,avg_B,avg_G;

	vector_points4.clear();
		
	for(int k=startx; k<endx ; k+=4)
	{
		for(int i=starty; i> endy ; i--)
		{
			//Reprents B,G & R of pix1
			pixB1	= *((Image + k  +	newLengthBWDiv3*(i)));
			pixG1	= *((Image + k  +1+	newLengthBWDiv3*(i)));
			pixR1	= *((Image + k  +2+	newLengthBWDiv3*(i)));
		
			//Reprents B,G & R of pix2
			pixB2	= *((Image + k  +	newLengthBWDiv3*(i-1)));
			pixG2	= *((Image + k  +1+	newLengthBWDiv3*(i-1)));
			pixR2	= *((Image + k  +2+	newLengthBWDiv3*(i-1)));

			//Reprents B,G & R of pix3
			pixB3	= *((Image + k  +	newLengthBWDiv3*(i-2)));
			pixG3	= *((Image + k  +1+	newLengthBWDiv3*(i-2)));
			pixR3	= *((Image + k  +2+	newLengthBWDiv3*(i-2)));

			avg_R=(int)(pixR1+pixR2+pixR3)/3;
			avg_G=(int)(pixG1+pixG2+pixG3)/3;
			avg_B=(int)(pixB1+pixB2+pixB3)/3;

			if( pixB1 <threshold && pixG1 <threshold && pixR1 <threshold && (pixR1 > pixG1) && (pixR1 > (pixB1)) &&
				      pixB2 <threshold && pixG2 <threshold && pixR2 <threshold && (pixR2 > pixG2) && (pixR2 > (pixB2)) &&
					        pixB3 <threshold && pixG3 <threshold && pixR3 <threshold && (pixR3 > pixG3) && (pixR3 > (pixB3)) 
					 && (( abs(avg_R-avg_G)+ abs(avg_R-avg_B))>15)				
				)
			{
				point.x=(k)/4;
				point.y= i;  //storing co ord in native resolution
				vector_points4.push_back(point);

				break;
			}
		}
	}
}

void CXAxisView::FindingLabelTransition_Cam3(BYTE *Image, int nCam, int factor)//factor is always 3 in the 51R85
{

	
	int newLengthBWDiv3		=	(4*CroppedCameraHeight)/factor;

	int newHeightBWDiv3		=	  CroppedCameraWidth/factor;

	int starty= 200 ;// Arbitrary start line
	int endy=50 ;
	int startx=0;
	int endx= newLengthBWDiv3  - 30;
	int pixB1=0,pixG1=0,pixR1=0,pixB2=0,pixG2=0,pixR2=0,pixB3=0,pixG3=0,pixR3=0;
	SinglePoint point;
	int threshold=40;
	int avg_R,avg_B,avg_G;

	vector_points3.clear();

	for(int k=startx; k<endx ; k+=4)
	{
		for(int i=starty; i> endy ; i--)
		{
			//Reprents B,G & R of pix1
			pixB1	= *((Image + k  +	newLengthBWDiv3*(i)));
			pixG1	= *((Image + k  +1+	newLengthBWDiv3*(i)));
			pixR1	= *((Image + k  +2+	newLengthBWDiv3*(i)));

			//Reprents B,G & R of pix2
			pixB2	= *((Image + k  +	newLengthBWDiv3*(i-1)));
			pixG2	= *((Image + k  +1+	newLengthBWDiv3*(i-1)));
			pixR2	= *((Image + k  +2+	newLengthBWDiv3*(i-1)));

			//Reprents B,G & R of pix3
			pixB3	= *((Image + k  +	newLengthBWDiv3*(i-2)));
			pixG3	= *((Image + k  +1+	newLengthBWDiv3*(i-2)));
			pixR3	= *((Image + k  +2+	newLengthBWDiv3*(i-2)));

			avg_R=(int)(pixR1+pixR2+pixR3)/3;
			avg_G=(int)(pixG1+pixG2+pixG3)/3;
			avg_B=(int)(pixB1+pixB2+pixB3)/3;

			if( pixB1 <threshold && pixG1 <threshold && pixR1 <threshold && (pixR1 > pixG1) && (pixR1 > (pixB1)) &&
			      pixB2 <threshold && pixG2 <threshold && pixR2 <threshold && (pixR2 > pixG2) && (pixR2 > (pixB2)) &&
			        pixB3 <threshold && pixG3 <threshold && pixR3 <threshold && (pixR3 > pixG3) && (pixR3 > (pixB3)) 
					 && (( abs(avg_R-avg_G)+ abs(avg_R-avg_B))>15)				
				)
			{
				point.x=(k)/4;
				point.y= i;  //storing co ord in native resolution
				vector_points3.push_back(point);

				break;
			}
		}
	}
}

void CXAxisView::FindingLabelTransition_Cam2(BYTE *Image, int nCam, int factor)//factor is always 3 in the 51R85
{
	int newLengthBWDiv3		=	(4*CroppedCameraHeight)/factor;

	int newHeightBWDiv3		=	  CroppedCameraWidth/factor;

	int starty= 200 ;// Arbitrary start line
	int endy=50 ;
	int startx=0;
	int endx= newLengthBWDiv3  - 30;
	int pixB1=0,pixG1=0,pixR1=0,pixB2=0,pixG2=0,pixR2=0,pixB3=0,pixG3=0,pixR3=0;
	SinglePoint point;
	int threshold=40;
	int avg_R,avg_B,avg_G;

	vector_points2.clear();

	for(int k=startx; k<endx ; k+=4)
	{
		for(int i=starty; i> endy ; i--)
		{
			//Reprents B,G & R of pix1
			pixB1	= *((Image + k  +	newLengthBWDiv3*(i)));
			pixG1	= *((Image + k  +1+	newLengthBWDiv3*(i)));
			pixR1	= *((Image + k  +2+	newLengthBWDiv3*(i)));

			//Reprents B,G & R of pix2
			pixB2	= *((Image + k  +	newLengthBWDiv3*(i-1)));
			pixG2	= *((Image + k  +1+	newLengthBWDiv3*(i-1)));
			pixR2	= *((Image + k  +2+	newLengthBWDiv3*(i-1)));

			//Reprents B,G & R of pix3
			pixB3	= *((Image + k  +	newLengthBWDiv3*(i-2)));
			pixG3	= *((Image + k  +1+	newLengthBWDiv3*(i-2)));
			pixR3	= *((Image + k  +2+	newLengthBWDiv3*(i-2)));

			avg_R=(int)(pixR1+pixR2+pixR3)/3;
			avg_G=(int)(pixG1+pixG2+pixG3)/3;
			avg_B=(int)(pixB1+pixB2+pixB3)/3;	

			if( pixB1 <threshold && pixG1 <threshold && pixR1 <threshold && (pixR1 > pixG1) && (pixR1 > (pixB1)) &&
			      pixB2 <threshold && pixG2 <threshold && pixR2 <threshold && (pixR2 > pixG2) && (pixR2 > (pixB2)) &&
			        pixB3 <threshold && pixG3 <threshold && pixR3 <threshold && (pixR3 > pixG3) && (pixR3 > (pixB3)) 
					 && (( abs(avg_R-avg_G)+ abs(avg_R-avg_B))>15)			
				)
			{
				point.x=(k)/4;
				point.y= i;  //storing co ord in native resolution
				vector_points2.push_back(point);

				break;
			}
		}
	}
}

void CXAxisView::FindingLabelTransition_Cam1(BYTE *Image, int nCam, int factor)//factor is always 3 in the 51R85
{
	//Going Bottom to Top
/*
	
	int newLengthBWDiv3		=	(4*CroppedCameraHeight)/factor;

	int newHeightBWDiv3		=	  CroppedCameraWidth/factor;

	int starty= 200 ;// Arbitrary start line
	int endy=50 ;
	int startx=0;
	int endx= newLengthBWDiv3  - 30;
	int pixB1=0,pixG1=0,pixR1=0,pixB2=0,pixG2=0,pixR2=0,pixB3=0,pixG3=0,pixR3=0;
	SinglePoint point;
	int threshold=30;
	int avg_R,avg_B,avg_G;
	vector_points1.clear();

		
	for(int k=startx; k<endx ; k+=4)
	{

		for(int i=starty; i> endy ; i--)
		{

			//Reprents B,G & R of pix1
			pixB1	= *((Image + k  +	newLengthBWDiv3*(i)));
			pixG1	= *((Image + k  +1+	newLengthBWDiv3*(i)));
			pixR1	= *((Image + k  +2+	newLengthBWDiv3*(i)));
		
			//Reprents B,G & R of pix2
			pixB2	= *((Image + k  +	newLengthBWDiv3*(i-1)));
			pixG2	= *((Image + k  +1+	newLengthBWDiv3*(i-1)));
			pixR2	= *((Image + k  +2+	newLengthBWDiv3*(i-1)));


			//Reprents B,G & R of pix3
			pixB3	= *((Image + k  +	newLengthBWDiv3*(i-2)));
			pixG3	= *((Image + k  +1+	newLengthBWDiv3*(i-2)));
			pixR3	= *((Image + k  +2+	newLengthBWDiv3*(i-2)));

			avg_R=(int)(pixR1+pixR2+pixR3)/3;
			avg_G=(int)(pixG1+pixG2+pixG3)/3;
			avg_B=(int)(pixB1+pixB2+pixB3)/3;

			if( pixB1 <threshold && pixG1 <threshold && pixR1 <threshold && (pixR1 > pixG1) && (pixR1 > pixB1) &&
					  pixB2 <threshold && pixG2 <threshold && pixR2 <threshold && (pixR2 > pixG2) && (pixR2 > pixB2) &&
						    pixB3 <threshold && pixG3 <threshold && pixR3 <threshold && (pixR3 > pixG3) && (pixR3 > pixB3) 
						//	 && (( abs(avg_R-avg_G)+ abs(avg_R-avg_B))>15)
							)
							
			{
				point.x=(k)/4;
				point.y= i;  //storing co ord in native resolution
				vector_points1.push_back(point);

				break;
			}
		}
	}
	*/

	//Going Top to Bottom

	int newLengthBWDiv3		=	(4*CroppedCameraHeight)/factor;

	int newHeightBWDiv3		=	  CroppedCameraWidth/factor;

	int starty= 200 ;// Arbitrary start line
	int endy=50 ;
	int startx=0;
	int endx= newLengthBWDiv3  - 30;
	int pixB1=0,pixG1=0,pixR1=0,pixB2=0,pixG2=0,pixR2=0,pixB3=0,pixG3=0,pixR3=0;
	SinglePoint point;
	int threshold=30;
	int avg_R,avg_B,avg_G;
	vector_points1.clear();

	for(int k=startx; k<endx ; k+=4)
	{
		for(int i=starty; i> endy ; i--)
		{
			//Reprents B,G & R of pix1
			pixB1	= *((Image + k  +	newLengthBWDiv3*(i)));
			pixG1	= *((Image + k  +1+	newLengthBWDiv3*(i)));
			pixR1	= *((Image + k  +2+	newLengthBWDiv3*(i)));
		
			//Reprents B,G & R of pix2
			pixB2	= *((Image + k  +	newLengthBWDiv3*(i-1)));
			pixG2	= *((Image + k  +1+	newLengthBWDiv3*(i-1)));
			pixR2	= *((Image + k  +2+	newLengthBWDiv3*(i-1)));


			//Reprents B,G & R of pix3
			pixB3	= *((Image + k  +	newLengthBWDiv3*(i-2)));
			pixG3	= *((Image + k  +1+	newLengthBWDiv3*(i-2)));
			pixR3	= *((Image + k  +2+	newLengthBWDiv3*(i-2)));

			avg_R=(int)(pixR1+pixR2+pixR3)/3;
			avg_G=(int)(pixG1+pixG2+pixG3)/3;
			avg_B=(int)(pixB1+pixB2+pixB3)/3;

			if( pixB1 <threshold && pixG1 <threshold && pixR1 <threshold && (pixR1 > pixG1) && (pixR1 > pixB1) &&
				  pixB2 <threshold && pixG2 <threshold && pixR2 <threshold && (pixR2 > pixG2) && (pixR2 > pixB2) &&
				    pixB3 <threshold && pixG3 <threshold && pixR3 <threshold && (pixR3 > pixG3) && (pixR3 > pixB3) 
					//	 && (( abs(avg_R-avg_G)+ abs(avg_R-avg_B))>15)
				)
			{
				point.x=(k)/4;
				point.y= i;  //storing co ord in native resolution
				vector_points1.push_back(point);

				break;
			}
		}
	}
}

void CXAxisView::FindBadLabelMating(std::vector<SinglePoint> &vectorPointsToUse,
									int windowSize, int deltaBetweenWindows, int heightDeltaThreshold,
									SinglePoint &failingCoordinate, int &heightDeltaFound,int nCam)
{
	int numberOfPoints = vectorPointsToUse.size();
	double window1Avg = 0;
	double window2Avg = 0;
	int tempHeightDeltaFound = 0;
	heightDeltaFound = 0;

	//label_mating_results[ncam];

	int tempY = 0;

	for(int i = 0; (i + 2*windowSize + deltaBetweenWindows) < numberOfPoints-(2*windowSize + deltaBetweenWindows) ; i++)
	{
		window1Avg = 0;
		window2Avg = 0;
		//TRACE("---- \n");
		for(int window1I = i; window1I< i+windowSize ; window1I++)
		{
			tempY= vectorPointsToUse[window1I].y;
			//TRACE("Window 1 Y %i \n",vector_points4[window1I].y);
			window1Avg += vectorPointsToUse[window1I].y/windowSize;
		}

		for(int window2I = i+windowSize+deltaBetweenWindows; window2I< i+2*windowSize+deltaBetweenWindows; window2I++)
		{
			window2Avg += vectorPointsToUse[window2I].y/windowSize;
				//TRACE("Window 2 Y %i \n",vectorPointsToUse[window2I].y);
		}

		tempHeightDeltaFound = abs(window1Avg-window2Avg);
			//TRACE("Diff  %i \n",tempHeightDeltaFound);

		if(tempHeightDeltaFound > heightDeltaFound)
		{
			heightDeltaFound = tempHeightDeltaFound;
			if(heightDeltaFound >= heightDeltaThreshold)
			{
				failingCoordinate = vectorPointsToUse[i + windowSize +deltaBetweenWindows/2];
			
				break;
			}
		}
	}

	///Writing points  to a text file
	CString currentpathTxt="C:\\Values.txt";
	CFile file_temp;
	//int digit_x;
	int digit_y=0;
	CString szDigit3;
	
	int size=vectorPointsToUse.size();

	try{
	file_temp.Open(currentpathTxt, CFile::modeWrite | CFile::modeCreate);
	}
	catch(...)
	{
		ASSERT("Failed to Create File");
	}

	int i1;
	for(i1=0;i1<size;i1++)
	{
		szDigit3.Format("%3i %3i \n",vectorPointsToUse[i1].x,vectorPointsToUse[i1].y);
		file_temp.Write(szDigit3,szDigit3.GetLength());
	}
	file_temp.Close();

	///#############
	 
	// BEST FIT
	float avg_x=0,avg_y=0;

	for(  i1=0;i1<size;i1++)
	{
		avg_x+=vectorPointsToUse[i1].x;
		avg_y+=vectorPointsToUse[i1].y;
	}

	avg_x/=size;
	avg_y/=size;

//	TRACE(" AVG_X %f  AVG_Y: %f \n",avg_x,avg_y);

	std::vector<int> XminusAvgx;
	std::vector<int> YminusAvgy;
	std::vector<int> XminusAvgxTIMESYminusAvgy;
	std::vector<int> XminusAvgxSq;

	float Sum_XminusAvgxTIMESYminusAvgy=0;
	float Sum_XminusAvgxSq=0;

	for(  i1=0;i1<size;i1++)
	{
		XminusAvgx.push_back(vectorPointsToUse[i1].x - avg_x);
		YminusAvgy.push_back(vectorPointsToUse[i1].y - avg_y);
		XminusAvgxTIMESYminusAvgy.push_back(XminusAvgx.at(i1)* YminusAvgy.at(i1));
		XminusAvgxSq.push_back(XminusAvgx.at(i1)*XminusAvgx.at(i1));

		Sum_XminusAvgxTIMESYminusAvgy+=XminusAvgxTIMESYminusAvgy.at(i1);
		Sum_XminusAvgxSq+=XminusAvgxSq.at(i1);
	}

	float slope_m=0;
	slope_m=Sum_XminusAvgxTIMESYminusAvgy/Sum_XminusAvgxSq;

	TRACE(" Cam %i  Slope %f\n",nCam ,slope_m);

	//#######################
}

void CXAxisView::TeachLabelMatingColorCam1()
{
	//theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtR;

	int imageWidth = (4*CroppedCameraHeight)/3; 
	int imageHeight = (CroppedCameraWidth)/3;
	int xOffSet = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorLeftBound*4;
	int yOffSet = theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorBottomBound;
	int teachWidth = ((theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorRightBound-theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorLeftBound)*4);
	int teachHeight = (theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorBottomBound-theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorTopBound);

	TeachLabelMatingColor(im_reduce1, imageWidth, imageHeight,
			   xOffSet, yOffSet, teachWidth, teachHeight,
			   theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtR,
			   theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtG,
			   theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtB);
}

void CXAxisView::TeachLabelMatingColor(BYTE *image, int imageWidth, int imageHeight,
									   int xOffSet, int yOffSet, int teachWidth, int teachHeight,
									   int &taughtR, int &taughtG, int &taughtB)
{
	taughtR = 0;
	taughtG = 0;
	taughtB = 0;
	int pixelCount = 0;

	double tempR = 0;
	double tempG = 0;
	double tempB = 0;

	for(int x=xOffSet; x<xOffSet+teachWidth; x+=4)
	{
		for(int y=yOffSet; y<yOffSet+teachHeight; y++)
		{
			tempB += *( image + x + 2 + imageWidth*y );
			tempG += *( image + x + 1 + imageWidth*y );
			tempR += *( image + x + 0 + imageWidth*y );

			pixelCount++;
		}
	}

	taughtR = tempR/pixelCount;
	taughtG = tempG/pixelCount;
	taughtB = tempB/pixelCount;
}

void CXAxisView::FindLabelTransition(BYTE* image, int imageWidth, int imageHeight,
									 int searchXOffSet, int searchYOffSet, 
									 int searchWidth, int searchHeight,
									 int taughtR, int taughtG, int taughtB,
									 int RTolerance, int GTolerance, int BTolerance,
									 int numberOfPixelsToCheck,
									 std::vector<SinglePoint> &foundPoints)
{
	double BAvg=0, GAvg=0, RAvg=0;
	foundPoints.clear();
	SinglePoint point;
	/*	
	for(int x=searchXOffSet; x<searchXOffSet+searchWidth; x+=4)
	{
		for(int y=searchYOffSet; y> searchYOffSet-searchHeight; y--)
		{
			BAvg = 0;
			GAvg = 0;
			RAvg = 0;
			for(int yOffSet=0; yOffSet < numberOfPixelsToCheck; yOffSet++)
			{
				//Reprents B,G & R of pixel
				BAvg += *(image + x + 0 + imageWidth*(y-yOffSet) );
				GAvg += *(image + x + 1 + imageWidth*(y-yOffSet) );
				RAvg += *(image + x + 2 + imageWidth*(y-yOffSet) );
			}

			BAvg = BAvg/numberOfPixelsToCheck;
			GAvg = BAvg/numberOfPixelsToCheck;
			RAvg = BAvg/numberOfPixelsToCheck;

			if( BAvg > (taughtB-BTolerance) && BAvg < (taughtB+BTolerance) &&
				GAvg > (taughtG-GTolerance) && GAvg < (taughtG+GTolerance) &&
				RAvg > (taughtR-RTolerance) && RAvg < (taughtR+RTolerance))
			{
				point.x=(x)/4;
				point.y= y;  //storing coord in native resolution
				foundPoints.push_back(point);

				break;
			}
		}
	}
	*/

	for(int x=searchXOffSet; x<searchXOffSet+searchWidth; x+=4)
	{
		for(int y=searchYOffSet; y> searchYOffSet-searchHeight; y--)
		{
			BAvg = 0;
			GAvg = 0;
			RAvg = 0;

			for(int yOffSet=0; yOffSet < numberOfPixelsToCheck; yOffSet++)
			{
				//Reprents B,G & R of pixel
				BAvg += *(image + x + 0 + imageWidth*(y-yOffSet) );
				GAvg += *(image + x + 1 + imageWidth*(y-yOffSet) );
				RAvg += *(image + x + 2 + imageWidth*(y-yOffSet) );
			}

			BAvg = BAvg/numberOfPixelsToCheck;
			GAvg = BAvg/numberOfPixelsToCheck;
			RAvg = BAvg/numberOfPixelsToCheck;

			if( BAvg <30)
			{
				point.x=(x)/4;
				point.y= y;  //storing coord in native resolution
				foundPoints.push_back(point);

				break;
			}
		}
	}
}

void CXAxisView::OnLoadPatternX_method6_num3(int Job, int pattType)
{
	CVisByteImage image;
	int pxWidth;  //pixel width
	int pxHeight; //pixel height
	BYTE pxValue; //pixel value

	int		szPatt6	=	theapp->sizepattBy6;
	int		szPatt3W=	theapp->sizepattBy3_2W;
	int		szPatt3H=	theapp->sizepattBy3_2H;

	bool	success	=	false;

	char	buf[10];
	//char	currentpath[60];
	char	currentpathTxt2[60];
	char	currentpathTxt3[60];
	char	currentpathBmp2[60];
	char	currentpathBmp3[60];

	CString c;	c=itoa(Job,buf,10);

	////////////


	char	*pFileName = "c:\\silgandata\\pat\\";//drive
	CString diffError = "The label pattern diff scales needs to be taught first";

	strcpy(currentpathTxt2,pFileName);	strcat(currentpathTxt2,c);
	strcpy(currentpathBmp2,pFileName);	strcat(currentpathBmp2,c);
	strcpy(currentpathTxt3,pFileName);	strcat(currentpathTxt3,c);
	strcpy(currentpathBmp3,pFileName);	strcat(currentpathBmp3,c);

	///replacing if statements with switch
	//if(pattType==1)	
	//{
	//	strcat(currentpathTxt2,"\\M6imgBy6L_num3.txt"); 
	//	strcat(currentpathTxt3,"\\M6imgBy3L_num3.txt");
	//}
	//if(pattType==2)	
	//{
	//	strcat(currentpathTxt2,"\\M6imgBy6M_num3.txt"); 
	//	strcat(currentpathTxt3,"\\M6imgBy3M_num3.txt");
	//}

	//if(pattType==3)	
	//{
	//	strcat(currentpathTxt2,"\\M6imgBy6R_num3.txt");  // these 2 files should get saved
	//	strcat(currentpathTxt3,"\\M6imgBy3R_num3.txt");
	//}


	switch(pattType)
	{
		case 1:
		{
			strcat(currentpathTxt2,"\\M6imgBy6L_num3.txt");
			strcat(currentpathBmp2,"\\M6patBy6L_num3.bmp");

			strcat(currentpathTxt3,"\\M6imgBy3L_num3.txt");
			strcat(currentpathBmp3,"\\M6patBy3L_num3.bmp");
			break;
		}
		case 2:
		{
			strcat(currentpathTxt2,"\\M6imgBy6M_num3.txt"); 
			strcat(currentpathBmp2,"\\M6patBy6M_num3.bmp"); 

			strcat(currentpathTxt3,"\\M6imgBy3M_num3.txt");
			strcat(currentpathBmp3,"\\M6patBy3M_num3.bmp");
			break;
		}
		case 3:
		{
			strcat(currentpathTxt2,"\\M6imgBy6R_num3.txt");
			strcat(currentpathBmp2,"\\M6patBy6R_num3.bmp");

			strcat(currentpathTxt3,"\\M6imgBy3R_num3.txt");
			strcat(currentpathBmp3,"\\M6patBy3R_num3.bmp");
			break;
		}
		default:
		{
			strcat(currentpathTxt2,"\\M6imgBy6_num3.txt");  //error if here
			strcat(currentpathBmp2,"\\M6patBy6_num3.bmp");

			strcat(currentpathTxt3,"\\M6imgBy3_num3.txt");
			strcat(currentpathBmp3,"\\M6patBy3_num3.bmp");
		}
	}

	/////////////////////

	CString N2; 
	char	bufDig2[2];
	int		digit2;

	CFileException ce;

	//try loading the bmp first then the text file as a backup
	try
	{
		image.ReadFile(currentpathBmp2);
		pxWidth = image.Width();
		pxHeight = image.Height();
		
		//treat bitmap image as single channel

		for(int col=0; col<pxWidth; col++)
		{
			for(int  row=0; row<pxHeight; row++)
			{
				pxValue = image.Pixel(col,row); //assuming only one band since image needs to be grayscale

				*(bytePattBy6_method6_num3+col+row*pxWidth)=pxValue;
			}
		}

		pframe->JobNoGood=false;
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		success=ImageFile.Open(currentpathTxt2, CFile::modeRead, &ce) > 0;

		if(success!=0)
		{
			for(int g=0; g<szPatt6*szPatt6; g++)
			{
				ImageFile.Read(bufDig2,3);
				N2	 =	bufDig2[0];
				N2	+=	bufDig2[1];
				N2	+=	bufDig2[2];
				digit2=	atoi(N2);
				
				N2	=	"";
				*(bytePattBy6_method6_num3+g)=digit2;
			}

			ImageFile.Close();
			pframe->JobNoGood=false;
		}
		else
		{
			AfxMessageBox(diffError + "\n" + currentpathTxt2);
			wehaveTempX=false;
		}
	}
	////////////////////

	CString N3; 
	char	bufDig3[2];
	int		digit3;

	//try loading the bmp first then the text file as a backup
	try
	{
		image.ReadFile(currentpathBmp2);
		pxWidth = image.Width();
		pxHeight = image.Height();
		
		//treat bitmap image as single channel

		for(int col=0; col<pxWidth; col++)
		{
			for(int  row=0; row<pxHeight; row++)
			{
				pxValue = image.Pixel(col,row); //assuming only one band since image needs to be grayscale

				*(bytePattBy3_method6_num3+col+row*pxWidth)=pxValue;
			}
		}

		pframe->JobNoGood=false;
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		success=ImageFile.Open(currentpathTxt3, CFile::modeRead, &ce) > 0;

		if(success!=0)
		{
			for(int g=0; g<szPatt3W*szPatt3H; g++)
			{
				ImageFile.Read(bufDig3,3);
				N3	 =	bufDig3[0];
				N3	+=	bufDig3[1];
				N3	+=	bufDig3[2];
				digit3=	atoi(N3);
				
				N3	=	"";
				*(bytePattBy3_method6_num3+g)=digit3;
			}

			ImageFile.Close();
			pframe->JobNoGood=false;
		}
		else
		{
			AfxMessageBox(diffError + "\n" + currentpathTxt3);
			wehaveTempX=false;
		}
	}
}

void CXAxisView::LearnPattX_method6_num3(BYTE* imgInby6,BYTE* imgInby3, int posPattX1, int posPattY1, int cam, int pattsel)
{
	int xpos			=	0;		
	int ypos			=	0;
	int xposTmp			=	0;	
	int yposTmp			=	0;

	int LineLengthTmp	=	theapp->sizepattBy12;
	int szpatt12		=	theapp->sizepattBy12;
	int szpatt6			=	theapp->sizepattBy6;
	int LineLength;
	int LineHeight;

	bool debugging		=	false;

	int x1, y1;

	////////////////////

	LineLength		=	widthBy6;
	LineHeight		=	heightBy6;
	LineLengthTmp	=	theapp->sizepattBy6;

	if(debugging)	{SaveBWCustom1(imgInby6, LineLength, LineHeight);}

	x1	=	posPattX1/6;
	y1	=	posPattY1/6;

	xpos	= 0;
	ypos	= 0;
	xposTmp	= 0;
	yposTmp	= 0;

	for(ypos=y1-szpatt6/2; ypos<y1+szpatt6/2; ypos++)
	{
		for(xpos=x1-szpatt6/2;xpos<x1+szpatt6/2;xpos++)
		{	
			if(xpos>0 && xpos<LineLength && ypos>0 && ypos<LineHeight)
			{
				*(bytePattBy6_method6_num3 + xposTmp + LineLengthTmp*yposTmp)= *(imgInby6 + xpos + LineLength*ypos);
				xposTmp++;
			}
		}

		xposTmp=0; yposTmp++;
	}

	////////////////////

	LineLength		=	widthBy3;
	LineHeight		=	heightBy3;
	int szpatt3W	=	theapp->sizepattBy3_2W;
	int szpatt3H	=	theapp->sizepattBy3_2H;
	LineLengthTmp	=	szpatt3W;

	if(debugging)	{SaveBWCustom1(imgInby3, LineLength, LineHeight);}

	x1	=	posPattX1/3;
	y1	=	posPattY1/3;

	xpos = 0; ypos = 0;	xposTmp	= 0; yposTmp = 0;

	for(ypos=y1-szpatt3H/2; ypos<y1+szpatt3H/2; ypos++)
	{
		for(xpos=x1-szpatt3W/2;xpos<x1+szpatt3W/2;xpos++)
		{	
			if(xpos>0 && xpos<LineLength && ypos>0 && ypos<LineHeight)
			{
				*(bytePattBy3_method6_num3 + xposTmp + LineLengthTmp*yposTmp)= *(imgInby3 + xpos + LineLength*ypos);
				xposTmp++;
			}
		}

		xposTmp=0; yposTmp++;
	}

	////////////////////
	
	if(debugging)	{SaveBWCustom1(bytePattBy6[pattsel],	szpatt6, szpatt6);}
	if(debugging)	{SaveBWCustom1(bytePattBy3[pattsel],	szpatt3W, szpatt3H);}

	SaveTemplateX_method6_num3(bytePattBy6_method6_num3, bytePattBy3_method6_num3, cam, pattsel);

	UpdateData(false);
	InvalidateRect(NULL,false);
}

void CXAxisView::SaveTemplateX_method6_num3(BYTE *im_sr2, BYTE *im_sr3, int ncam, int pattsel)
{
	//pattsel should always be 3 when this routine is called

	char* pFileName = "c:\\silgandata\\pat\\";//drive
	CString c;

	c =itoa(pframe->CurrentJobNum, buf, 10);

	strcpy(currentpathTxt,pFileName);	strcat(currentpathTxt,c);
	strcpy(currentpathBmp,pFileName);	strcat(currentpathBmp,c);

	strcpy(currentpathTxt2,pFileName);	strcat(currentpathTxt2,c);
	strcpy(currentpathBmp2,pFileName);	strcat(currentpathBmp2,c);

	strcpy(currentpathTxt3,pFileName);	strcat(currentpathTxt3,c);
	strcpy(currentpathBmp3,pFileName);	strcat(currentpathBmp3,c);

	///replacing if statements with switch
	//if(pattsel==1)
	//{
		//strcat(currentpathTxt2,"\\M6imgBy6L_num3.txt");
	//	strcat(currentpathBmp2,"\\M6patBy6L_num3.bmp");  //M6: Method 6 
		//strcat(currentpathTxt3,"\\M6imgBy3L_num3.txt");
	//	strcat(currentpathBmp3,"\\M6patBy3L_num3.bmp");
	//}
	//else if(pattsel==2)
	//{
		//strcat(currentpathTxt2,"\\M6imgBy6M_num3.txt");
	//	strcat(currentpathBmp2,"\\M6patBy6M_num3.bmp");
		//strcat(currentpathTxt3,"\\M6imgBy3M_num3.txt");
	//	strcat(currentpathBmp3,"\\M6patBy3M_num3.bmp");
	//}
	//else if(pattsel==3)
	//{
		//strcat(currentpathTxt2,"\\M6imgBy6R_num3.txt");
	//	strcat(currentpathBmp2,"\\M6patBy6R_num3.bmp");
		//strcat(currentpathTxt3,"\\M6imgBy3R_num3.txt");
	//	strcat(currentpathBmp3,"\\M6patBy3R_num3.bmp");
	//}

	switch(pattsel)
	{
		case 1:
		{
			strcat(currentpathTxt2,"\\M6imgBy6L_num3.txt");  //M6: Method 6 
			strcat(currentpathBmp2,"\\M6patBy6L_num3.bmp");

			strcat(currentpathTxt3,"\\M6imgBy3L_num3.txt");
			strcat(currentpathBmp3,"\\M6patBy3L_num3.bmp");
			break;
		}
		case 2:
		{
			strcat(currentpathTxt2,"\\M6imgBy6M_num3.txt");
			strcat(currentpathBmp2,"\\M6patBy6M_num3.bmp");

			strcat(currentpathTxt3,"\\M6imgBy3M_num3.txt");
			strcat(currentpathBmp3,"\\M6patBy3M_num3.bmp");
			break;
		}
		case 3:
		{
			strcat(currentpathTxt2,"\\M6imgBy6R_num3.txt");
			strcat(currentpathBmp2,"\\M6patBy6R_num3.bmp");

			strcat(currentpathTxt3,"\\M6imgBy3R_num3.txt");
			strcat(currentpathBmp3,"\\M6patBy3R_num3.bmp");
			break;
		}
		default:
		{
			strcat(currentpathTxt2,"\\M6imgBy6_num3.txt"); //removing letter after 6 and 3 to signify error
			strcat(currentpathBmp2,"\\M6patBy6_num3.bmp");

			strcat(currentpathTxt3,"\\M6imgBy3_num3.txt");
			strcat(currentpathBmp3,"\\M6patBy3_num3.bmp");
		}
	}

	int szpatt12	=	theapp->sizepattBy12;
	int szpatt6		=	theapp->sizepattBy6;
	int szpatt3W	=	theapp->sizepattBy3_2W;
	int szpatt3H	=	theapp->sizepattBy3_2H;

	//////////////////

	//commenting out  text file writing
	ImageFile.Open(currentpathTxt2, CFile::modeWrite | CFile::modeCreate);
	
	CString szDigit2;	int digit2;
	int g;
	for(g=0; g<szpatt6*szpatt6; g++)
	{
		digit2=*(im_sr2+g);
		szDigit2.Format("%3i",digit2);
		ImageFile.Write(szDigit2,szDigit2.GetLength());
	}

	ImageFile.Close();
	///////////////////

	CVisByteImage image2(szpatt6,szpatt6,1,-1,im_sr2);
	imagepat2=image2;

	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpathBmp2;
		imagepat2.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{AfxMessageBox("The pat file could not be saved.");}// Warn the user.

	///////////////////

	//comment out text file writing
	ImageFile.Open(currentpathTxt3, CFile::modeWrite | CFile::modeCreate);

	CString szDigit3;	int digit3;

	for(g=0; g<szpatt3W*szpatt3H; g++)
	{
		digit3=*(im_sr3+g);
		szDigit3.Format("%3i",digit3);
		ImageFile.Write(szDigit3,szDigit3.GetLength());
	}

	ImageFile.Close();
	///////////////////

	CVisByteImage image3(szpatt3W,szpatt3H,1,-1,im_sr3);
	imagepat3=image3;

	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;
		filedescriptorT.filename = currentpathBmp3;
		imagepat3.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{AfxMessageBox("The pat file could not be saved.");}// Warn the user.
}

BOOL CXAxisView::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	
	return TRUE;
}

void CXAxisView::ComposeCam1WaistInspection()
{
	QueryPerformanceFrequency(&m_lnC1FreqI11);
	QueryPerformanceCounter(&m_lnC1StartI11);//Timing

	//int fact = 3;
	int verticalSearchLength = theapp->jobinfo[pframe->CurrentJobNum].cam1WaistVerticalSearchLength;
	int horizontalSearchLength = theapp->jobinfo[pframe->CurrentJobNum].cam1WaistHorizontalSearchLength;
	int edgeThreshold = theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeThreshold;
	bool leftEdge = true;
	int imageWidth = FullImageScaledRotatedWidth; 
	int imageHeight = FullImageScaledRotatedHeight;

	int topBound = theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementTopBound;
	int bottomBound = theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementBottomBound;
	int leftBound = theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementLeftBound;
	int rightBound = theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementRightBound;

	int cam1WaistWidthThreshold = theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthThreshold;

/*
	cam1CoordinatesOfContainerLeftTopEdgePointsRaw.clear();

	SinglePoint tempSinglePoint;

	for(int y =topBound; y < bottomBound; y++)
	{
		tempSinglePoint.x = 30;
		tempSinglePoint.y = y;
		cam1CoordinatesOfContainerLeftTopEdgePointsRaw.push_back(tempSinglePoint);
	}
*/
//	SaveBWCustom1(cam1ImageFullScaledRotatedGreyscale, imageWidth, imageHeight);

	InitializeSinglePointCoordinates(topBound, bottomBound, imageWidth-1, cam1CoordinatesOfContainerLeftTopEdgePointsRaw);

	FindEdge(cam1ImageFullScaledRotatedGreyscale, imageWidth, imageHeight,
			 topBound, bottomBound,
			 leftBound, rightBound,
			 verticalSearchLength, horizontalSearchLength, 
			 edgeThreshold,
			 leftEdge,
			 cam1CoordinatesOfContainerLeftTopEdgePointsRaw);

	int avgWindowSize = 5;				//theapp->jobinfo[pframe->CurrentJobNum].cam1WaistAvgWindowSize;
	int edgeCoordinateDeviation = 15;	//theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeCoordinateDeviation;
	bool fillTopToBottom = true;

	FillEdge(cam1CoordinatesOfContainerLeftTopEdgePointsRaw, 
			 avgWindowSize, edgeCoordinateDeviation, fillTopToBottom,
			 cam1CoordinatesOfContainerLeftTopEdgePointsFilled);

	leftEdge = true;
	topBound = theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperTopBound;
	bottomBound = theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperBottomBound;
	leftBound = theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperLeftBound;
	rightBound = theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperRightBound;

	InitializeSinglePointCoordinates(topBound, bottomBound, 0, cam1CoordinatesOfContainerLeftWaistEdgePointsRaw);

	FindEdge(cam1ImageFullScaledRotatedGreyscale, imageWidth, imageHeight,
			 topBound, bottomBound,
			 leftBound, rightBound,
			 verticalSearchLength, horizontalSearchLength, 
			 edgeThreshold,
			 leftEdge,
			 cam1CoordinatesOfContainerLeftWaistEdgePointsRaw);

	FillEdge(cam1CoordinatesOfContainerLeftWaistEdgePointsRaw, 
			 avgWindowSize, edgeCoordinateDeviation, fillTopToBottom,
			 cam1CoordinatesOfContainerLeftWaistEdgePointsFilled);
	
	int averageOfTopVector = FindAverageXValue(cam1CoordinatesOfContainerLeftTopEdgePointsFilled);
	int averageOfWaistVector  = FindAverageXValue(cam1CoordinatesOfContainerLeftWaistEdgePointsFilled);

//	FindDeltas(cam1CoordinatesOfContainerLeftTopEdgePointsFilled,
//			   cam1CoordinatesOfContainerRightEdgePointsFilled,
//			   cam1WaistDeltas);

	Result11Cam1 = averageOfTopVector-averageOfWaistVector;	//CountWaistWidthAboveThreshold(cam1WaistDeltas, cam1WaistWidthThreshold);

	QueryPerformanceCounter(&m_lnC1EndI11); 
	nDiff=0; 
	dMicroSecondC1I11= 0; //Time

	if( m_lnC1StartI11.QuadPart !=0 && m_lnC1EndI11.QuadPart != 0)
	{
		dMicroSecondC1I11	= (double)(m_lnC1FreqI11.QuadPart/1000); 
		nDiff				= m_lnC1EndI11.QuadPart-m_lnC1StartI11.QuadPart; 
		dMicroSecondC1I11	= nDiff/dMicroSecondC1I11;
	}
/**/
}

void CXAxisView::ComposeCam2WaistInspection()
{
	//QueryPerformanceFrequency(&m_lnC1FreqI11);
	//QueryPerformanceCounter(&m_lnC1StartI11);//Timing

	//int fact = 3;
	int verticalSearchLength = theapp->jobinfo[pframe->CurrentJobNum].cam2WaistVerticalSearchLength;
	int horizontalSearchLength = theapp->jobinfo[pframe->CurrentJobNum].cam2WaistHorizontalSearchLength;
	int edgeThreshold = theapp->jobinfo[pframe->CurrentJobNum].cam2WaistEdgeThreshold;
	bool leftEdge = true;
	int imageWidth = FullImageScaledRotatedWidth; 
	int imageHeight = FullImageScaledRotatedHeight;

	int topBound = theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementTopBound;
	int bottomBound = theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementBottomBound;
	int leftBound = theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementLeftBound;
	int rightBound = theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementRightBound;

	int cam1WaistWidthThreshold = theapp->jobinfo[pframe->CurrentJobNum].cam2WaistWidthThreshold;

//	SaveBWCustom1(cam1ImageFullScaledRotatedGreyscale, imageWidth, imageHeight);

	InitializeSinglePointCoordinates(topBound, bottomBound, imageWidth-1, cam2CoordinatesOfContainerLeftTopEdgePointsRaw);

	FindEdge(cam2ImageFullScaledRotatedGreyscale, imageWidth, imageHeight,
			 topBound, bottomBound,
			 leftBound, rightBound,
			 verticalSearchLength, horizontalSearchLength, 
			 edgeThreshold,
			 leftEdge,
			 cam2CoordinatesOfContainerLeftTopEdgePointsRaw);

	int avgWindowSize = 5;				//theapp->jobinfo[pframe->CurrentJobNum].cam1WaistAvgWindowSize;
	int edgeCoordinateDeviation = 15;	//theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeCoordinateDeviation;
	bool fillTopToBottom = true;

	FillEdge(cam2CoordinatesOfContainerLeftTopEdgePointsRaw, 
			 avgWindowSize, edgeCoordinateDeviation, fillTopToBottom,
			 cam2CoordinatesOfContainerLeftTopEdgePointsFilled);

	leftEdge = true;
	topBound = theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperTopBound;
	bottomBound = theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperBottomBound;
	leftBound = theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperLeftBound;
	rightBound = theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperRightBound;

	InitializeSinglePointCoordinates(topBound, bottomBound, 0, cam2CoordinatesOfContainerLeftWaistEdgePointsRaw);
	
	FindEdge(cam2ImageFullScaledRotatedGreyscale, imageWidth, imageHeight,
			 topBound, bottomBound,
			 leftBound, rightBound,
			 verticalSearchLength, horizontalSearchLength, 
			 edgeThreshold,
			 leftEdge,
			 cam2CoordinatesOfContainerLeftWaistEdgePointsRaw);

	FillEdge(cam2CoordinatesOfContainerLeftWaistEdgePointsRaw, 
			 avgWindowSize, edgeCoordinateDeviation, fillTopToBottom,
			 cam2CoordinatesOfContainerLeftWaistEdgePointsFilled);
	
	int averageOfTopVector = FindAverageXValue(cam2CoordinatesOfContainerLeftTopEdgePointsFilled);
	int averageOfWaistVector  = FindAverageXValue(cam2CoordinatesOfContainerLeftWaistEdgePointsFilled);

//	FindDeltas(cam1CoordinatesOfContainerLeftTopEdgePointsFilled,
//			   cam1CoordinatesOfContainerRightEdgePointsFilled,
//			   cam1WaistDeltas);

	Result11Cam2 = averageOfTopVector-averageOfWaistVector;	//CountWaistWidthAboveThreshold(cam1WaistDeltas, cam1WaistWidthThreshold);

//	QueryPerformanceCounter(&m_lnC1EndI11); 
//	nDiff=0; 
//	dMicroSecondC1I11= 0; //Time

//	if( m_lnC1StartI11.QuadPart !=0 && m_lnC1EndI11.QuadPart != 0)
//	{
//		dMicroSecondC1I11	= (double)(m_lnC1FreqI11.QuadPart/1000); 
//		nDiff				= m_lnC1EndI11.QuadPart-m_lnC1StartI11.QuadPart; 
//		dMicroSecondC1I11	= nDiff/dMicroSecondC1I11;
//	}
}

void CXAxisView::ComposeCam3WaistInspection()
{
	//QueryPerformanceFrequency(&m_lnC1FreqI11);
	//QueryPerformanceCounter(&m_lnC1StartI11);//Timing

	//int fact = 3;
	int verticalSearchLength = theapp->jobinfo[pframe->CurrentJobNum].cam3WaistVerticalSearchLength;
	int horizontalSearchLength = theapp->jobinfo[pframe->CurrentJobNum].cam3WaistHorizontalSearchLength;
	int edgeThreshold = theapp->jobinfo[pframe->CurrentJobNum].cam3WaistEdgeThreshold;
	bool leftEdge = false;
	int imageWidth = FullImageScaledRotatedWidth; 
	int imageHeight = FullImageScaledRotatedHeight;

	int topBound = theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementTopBound;
	int bottomBound = theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementBottomBound;
	int leftBound = theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementLeftBound;
	int rightBound = theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementRightBound;

	int cam1WaistWidthThreshold = theapp->jobinfo[pframe->CurrentJobNum].cam3WaistWidthThreshold;

//	SaveBWCustom1(cam1ImageFullScaledRotatedGreyscale, imageWidth, imageHeight);

	InitializeSinglePointCoordinates(topBound, bottomBound, 0, cam3CoordinatesOfContainerLeftTopEdgePointsRaw);

	FindEdge(cam3ImageFullScaledRotatedGreyscale, imageWidth, imageHeight,
			 topBound, bottomBound,
			 leftBound, rightBound,
			 verticalSearchLength, horizontalSearchLength, 
			 edgeThreshold,
			 leftEdge,
			 cam3CoordinatesOfContainerLeftTopEdgePointsRaw);

	int avgWindowSize = 5;				//theapp->jobinfo[pframe->CurrentJobNum].cam1WaistAvgWindowSize;
	int edgeCoordinateDeviation = 15;	//theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeCoordinateDeviation;
	bool fillTopToBottom = true;

	FillEdge(cam3CoordinatesOfContainerLeftTopEdgePointsRaw, 
			 avgWindowSize, edgeCoordinateDeviation, fillTopToBottom,
			 cam3CoordinatesOfContainerLeftTopEdgePointsFilled);

	leftEdge = false;
	topBound = theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperTopBound;
	bottomBound = theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperBottomBound;
	leftBound = theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperLeftBound;
	rightBound = theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperRightBound;

	InitializeSinglePointCoordinates(topBound, bottomBound, imageWidth-1, cam3CoordinatesOfContainerLeftWaistEdgePointsRaw);
	
	FindEdge(cam3ImageFullScaledRotatedGreyscale, imageWidth, imageHeight,
			 topBound, bottomBound,
			 leftBound, rightBound,
			 verticalSearchLength, horizontalSearchLength, 
			 edgeThreshold,
			 leftEdge,
			 cam3CoordinatesOfContainerLeftWaistEdgePointsRaw);

	FillEdge(cam3CoordinatesOfContainerLeftWaistEdgePointsRaw, 
			 avgWindowSize, edgeCoordinateDeviation, fillTopToBottom,
			 cam3CoordinatesOfContainerLeftWaistEdgePointsFilled);
	
	int averageOfTopVector = FindAverageXValue(cam3CoordinatesOfContainerLeftTopEdgePointsFilled);
	int averageOfWaistVector  = FindAverageXValue(cam3CoordinatesOfContainerLeftWaistEdgePointsFilled);

//	FindDeltas(cam1CoordinatesOfContainerLeftTopEdgePointsFilled,
//			   cam1CoordinatesOfContainerRightEdgePointsFilled,
//			   cam1WaistDeltas);

	Result11Cam3 = averageOfWaistVector- averageOfTopVector;	//CountWaistWidthAboveThreshold(cam1WaistDeltas, cam1WaistWidthThreshold);

//	QueryPerformanceCounter(&m_lnC1EndI11); 
//	nDiff=0; 
//	dMicroSecondC1I11= 0; //Time

//	if( m_lnC1StartI11.QuadPart !=0 && m_lnC1EndI11.QuadPart != 0)
//	{
//		dMicroSecondC1I11	= (double)(m_lnC1FreqI11.QuadPart/1000); 
//		nDiff				= m_lnC1EndI11.QuadPart-m_lnC1StartI11.QuadPart; 
//		dMicroSecondC1I11	= nDiff/dMicroSecondC1I11;
//	}
}

void CXAxisView::ComposeCam4WaistInspection()
{
	bool debugging = false;
//	QueryPerformanceFrequency(&m_lnC1FreqI11);
//	QueryPerformanceCounter(&m_lnC1StartI11);//Timing

	//int fact = 3;
	int verticalSearchLength = theapp->jobinfo[pframe->CurrentJobNum].cam4WaistVerticalSearchLength;
	int horizontalSearchLength = theapp->jobinfo[pframe->CurrentJobNum].cam4WaistHorizontalSearchLength;
	int edgeThreshold = theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeThreshold;
	bool leftEdge = false;
	int imageWidth = FullImageScaledRotatedWidth; 
	int imageHeight = FullImageScaledRotatedHeight;

	int topBound = theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementTopBound;
	int bottomBound = theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementBottomBound;
	int leftBound = theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementLeftBound;
	int rightBound = theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementRightBound;

	int cam4WaistWidthThreshold = theapp->jobinfo[pframe->CurrentJobNum].cam4WaistWidthThreshold;
/*
	cam1CoordinatesOfContainerLeftTopEdgePointsRaw.clear();

	SinglePoint tempSinglePoint;

	for(int y =topBound; y < bottomBound; y++)
	{
		tempSinglePoint.x = 30;
		tempSinglePoint.y = y;
		cam1CoordinatesOfContainerLeftTopEdgePointsRaw.push_back(tempSinglePoint);
	}
*/

	//only allow execution on debugging == true otherwise extra calls produced to save
	if(debugging)
	{
		SaveBWCustom1(cam4ImageFullScaledRotatedGreyscale, imageWidth, imageHeight);
	}

	InitializeSinglePointCoordinates(topBound, bottomBound, 0, cam4CoordinatesOfContainerLeftTopEdgePointsRaw);

	FindEdge(cam4ImageFullScaledRotatedGreyscale, imageWidth, imageHeight,
			 topBound, bottomBound,
			 leftBound, rightBound,
			 verticalSearchLength, horizontalSearchLength, 
			 edgeThreshold,
			 leftEdge,
			 cam4CoordinatesOfContainerLeftTopEdgePointsRaw);

	int avgWindowSize = 5;				//theapp->jobinfo[pframe->CurrentJobNum].cam4WaistAvgWindowSize;
	int edgeCoordinateDeviation = 15;	//theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeCoordinateDeviation;
	bool fillTopToBottom = true;

	FillEdge(cam4CoordinatesOfContainerLeftTopEdgePointsRaw, 
			 avgWindowSize, edgeCoordinateDeviation, fillTopToBottom,
			 cam4CoordinatesOfContainerLeftTopEdgePointsFilled);

	leftEdge = false;
	topBound = theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperTopBound;
	bottomBound = theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperBottomBound;
	leftBound = theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperLeftBound;
	rightBound = theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperRightBound;

	InitializeSinglePointCoordinates(topBound, bottomBound, imageWidth-1, cam4CoordinatesOfContainerLeftWaistEdgePointsRaw);
	
	FindEdge(cam4ImageFullScaledRotatedGreyscale, imageWidth, imageHeight,
			 topBound, bottomBound,
			 leftBound, rightBound,
			 verticalSearchLength, horizontalSearchLength, 
			 edgeThreshold,
			 leftEdge,
			 cam4CoordinatesOfContainerLeftWaistEdgePointsRaw);

	FillEdge(cam4CoordinatesOfContainerLeftWaistEdgePointsRaw, 
			 avgWindowSize, edgeCoordinateDeviation, fillTopToBottom,
			 cam4CoordinatesOfContainerLeftWaistEdgePointsFilled);
	
	int averageOfTopVector = FindAverageXValue(cam4CoordinatesOfContainerLeftTopEdgePointsFilled);
	int averageOfWaistVector  = FindAverageXValue(cam4CoordinatesOfContainerLeftWaistEdgePointsFilled);

//	FindDeltas(cam1CoordinatesOfContainerLeftTopEdgePointsFilled,
//			   cam1CoordinatesOfContainerRightEdgePointsFilled,
//			   cam1WaistDeltas);

	Result11Cam4 = averageOfWaistVector-averageOfTopVector;	//CountWaistWidthAboveThreshold(cam1WaistDeltas, cam1WaistWidthThreshold);

//	QueryPerformanceCounter(&m_lnC1EndI11); 
//	nDiff=0; 
//	dMicroSecondC1I11= 0; //Time

//	if( m_lnC1StartI11.QuadPart !=0 && m_lnC1EndI11.QuadPart != 0)
//	{
//		dMicroSecondC1I11	= (double)(m_lnC1FreqI11.QuadPart/1000); 
//		nDiff				= m_lnC1EndI11.QuadPart-m_lnC1StartI11.QuadPart; 
//		dMicroSecondC1I11	= nDiff/dMicroSecondC1I11;
//	}

/**/
}

void CXAxisView::FindEdge(BYTE *img, int imageWidth, int imageHeight,
						  int topBound, int bottomBound,
						  int leftBound, int rightBound,
						  int verticalSearchLength, int horizontalSearchLength, 
						  int edgeThreshold,
						  int searchLeftToRight,
						  std::vector<SinglePoint> &coordinatesOfEdgePoints)
{
	//coordinatesOfEdgePoints.clear();					//Resulting edge coordinates.

	//Check if input parameters are valid;
	bool inputParametersValid = true;

	if(img == NULL)
	{
		inputParametersValid = false;
	}
	if(imageWidth <= 0 || imageHeight <= 0)
	{
		inputParametersValid = false;
	}

	if(topBound < verticalSearchLength || topBound >= bottomBound)
	{
		inputParametersValid = false;
	}
	if(bottomBound > (imageHeight-1)-verticalSearchLength )
	{
		inputParametersValid = false;
	}

	if(leftBound < horizontalSearchLength || leftBound >= rightBound)
	{
		inputParametersValid = false;
	}
	if(rightBound > (imageWidth-1)-horizontalSearchLength )
	{
		inputParametersValid = false;
	}

/*
	if(searchLeftToRight)
	{
		if(leftBound < 0 || leftBound >= rightBound)
		{
			inputParametersValid = false;
		}
		if( rightBound > (imageWidth-1)-horizontalSearchLength )
		{
			inputParametersValid = false;
		}
	}
	else
	{
		if(leftBound < horizontalSearchLength || leftBound >= rightBound)
		{
			inputParametersValid = false;
		}
		if( rightBound > (imageWidth-1) )
		{
			inputParametersValid = false;
		}
	}
	
*/

	if(verticalSearchLength < 1 || verticalSearchLength > 20)
	{
		inputParametersValid = false;
	} 
	if(horizontalSearchLength < 1 || horizontalSearchLength > 20)
	{
		inputParametersValid = false;
	}

	if(inputParametersValid)
	{
		int verticalNumberOfPixelsToCheck = (bottomBound - topBound)+1;
/*
		coordinatesOfEdgePoints.clear();	

		SinglePoint temSinglePoint;
		temSinglePoint.x = 0;								
		for(int i = 0; i < verticalNumberOfPixelsToCheck; i++)
		{
			temSinglePoint.y = topBound+i;
			coordinatesOfEdgePoints.push_back(temSinglePoint);
		}
*/
		int verticalStartCoordinate = topBound;
		int verticalEndCoordinate = bottomBound;
		int verticalIncrement = 1;

		int horizontalStartCoordinate;
		int horizontalEndCoordinate;
		int horizontalIncrement;

		if(searchLeftToRight)
		{
			horizontalStartCoordinate = leftBound;
			horizontalEndCoordinate = rightBound;
			horizontalIncrement = 1;
		}
		else
		{
			horizontalStartCoordinate = rightBound;
			horizontalEndCoordinate = leftBound;
			horizontalIncrement = -1;
		}

		double basePixelsAvg = 0;									//Average value of base pixel and its vertical neighbors.
		double offsetPixelsAvg = 0;									//Average value of currently inspected pixel and its vertical neighbors.
		int edgeVectorIndex = 0;
		int xBeingChecked = 0;
		int absValue = 0;
		bool edgeFound = false;

		bool isVerticalAverage = true;

		for(int y = verticalStartCoordinate; y <= verticalEndCoordinate; y += verticalIncrement)
		{
			edgeFound = false;
			for(int x = horizontalStartCoordinate; x >= leftBound && x <= rightBound; x += horizontalIncrement)
			{
				//Compute vertical average for horizontal base pixel. Takes verticalSearchLength
				//before and after the current pixel.
				basePixelsAvg = AveragePixels( img, imageWidth, imageHeight, verticalSearchLength, y, x, isVerticalAverage);
				
				//Go to next horizontal offset.
				for(int xOffsetIndex = 1*horizontalIncrement; xOffsetIndex*horizontalIncrement <= horizontalSearchLength; xOffsetIndex += horizontalIncrement)
				{
					//Compute vertical average for horizontally offset pixel.
					xBeingChecked = x + xOffsetIndex;
					offsetPixelsAvg = AveragePixels( img, imageWidth, imageHeight, verticalSearchLength, y, xBeingChecked, isVerticalAverage);
					//Compute difference between base pixel and offset pixel.
					absValue = abs(basePixelsAvg - offsetPixelsAvg);

					//If difference is large enough consider edge found and stop searching.
					if(absValue > edgeThreshold)
					{
						SinglePoint tempSinglePoint;
						tempSinglePoint.x = xBeingChecked;
						tempSinglePoint.y = y;

						edgeVectorIndex = y - topBound;
						if(tempSinglePoint.x >= 0 && tempSinglePoint.x <imageWidth &&
						   tempSinglePoint.y >= 0 && tempSinglePoint.y < imageHeight &&
						   edgeVectorIndex >=0 && edgeVectorIndex < coordinatesOfEdgePoints.size())  
						{
							coordinatesOfEdgePoints[edgeVectorIndex] = tempSinglePoint;
						}
						edgeFound = true;
						break;
					}
				}

				if(edgeFound)
				{
					break;
				}
			}
		}
	}
}

int CXAxisView::AveragePixels(BYTE *img, int imageWidth, int imageHeight,
							   int averagingWindowLength, 
							   int averagingCenterYCoordinate, int averagingCenterXCoordinate,
							   bool isVerticalAverage)
{
    int pixelAvg = 0;
	int xCoordinate = 0;
	int yCoordinate = 0;
    for(int offSetIndex=-averagingWindowLength; offSetIndex <= averagingWindowLength; offSetIndex++)
	{
		xCoordinate = averagingCenterXCoordinate+offSetIndex*(!isVerticalAverage);
		yCoordinate = averagingCenterYCoordinate+offSetIndex*isVerticalAverage; 

		if(xCoordinate >= 0 && xCoordinate <imageWidth &&
		   yCoordinate >= 0 && yCoordinate < imageHeight) 
		{
			pixelAvg = pixelAvg+img[xCoordinate + yCoordinate * imageWidth];
		}
		else
		{
			pixelAvg = 0;
			break;
		}
	}
    pixelAvg = pixelAvg/((2*averagingWindowLength) + 1);

	return pixelAvg;
}

void CXAxisView::FillEdge(const std::vector<SinglePoint> &edgeCoordinates, 
						  int avgWindowSize, int edgeCoordinateDeviation,
						  bool fillTopToBottom,
						  std::vector<SinglePoint> &edgeCoordinatesFilled)
{
	std::deque<SinglePoint> coordinatesQueue;	//Used to store running average values.

	//Initialize filled edge coordinates with 0 x values, y values are copied over from the edge coordinates vector.
	edgeCoordinatesFilled.clear();

	if(edgeCoordinates.size()>0)
	{
		SinglePoint tempSinglePoint;
		tempSinglePoint.x = 0;
		for(int i = 0; i < edgeCoordinates.size(); i++)
		{
			tempSinglePoint.y = edgeCoordinates[i].y;
			edgeCoordinatesFilled.push_back(tempSinglePoint);
		}

		int fillStartingIndex;
		int fillEndingIndex;
		int fillIncrement;

		if(fillTopToBottom)
		{
			fillStartingIndex = 0;
			fillEndingIndex = edgeCoordinatesFilled.size()-1;
			fillIncrement = 1;
		}
		else
		{
			fillStartingIndex = edgeCoordinatesFilled.size()-1;
			fillEndingIndex = 0;
			fillIncrement = -1;
		}

		//Find x coordinates of top segment
		int initialAverage = 0;
		int edgeVectorIndex;
		for(edgeVectorIndex = fillStartingIndex;  edgeVectorIndex < edgeCoordinates.size() && edgeVectorIndex != (fillStartingIndex+avgWindowSize*fillIncrement); edgeVectorIndex = edgeVectorIndex+fillIncrement)
		{
			initialAverage = initialAverage + edgeCoordinates[edgeVectorIndex].x;
		}
		initialAverage = initialAverage/avgWindowSize;

		int initialBetterAverage = 0;
		int numberOfInitialGoodPoints = 0;
		for(edgeVectorIndex = fillStartingIndex;  edgeVectorIndex< edgeCoordinates.size() && edgeVectorIndex != (fillEndingIndex+avgWindowSize*fillIncrement); edgeVectorIndex = edgeVectorIndex+fillIncrement)
		{
			if( abs(edgeCoordinates[edgeVectorIndex].x - initialAverage) < edgeCoordinateDeviation)
			{
				initialBetterAverage = initialBetterAverage + edgeCoordinates[edgeVectorIndex].x;  
				numberOfInitialGoodPoints++;
			}
		}

		if(numberOfInitialGoodPoints >0)
		{
			initialBetterAverage = initialBetterAverage/numberOfInitialGoodPoints;
		}
		else
		{
			initialBetterAverage = initialAverage;
		}

		for(edgeVectorIndex = fillStartingIndex; edgeVectorIndex < edgeCoordinates.size() && edgeVectorIndex != (fillEndingIndex+avgWindowSize*fillIncrement); edgeVectorIndex = edgeVectorIndex+fillIncrement)
		{
			tempSinglePoint.x = initialBetterAverage;
			tempSinglePoint.y = edgeCoordinates[edgeVectorIndex].y;
			coordinatesQueue.push_back(tempSinglePoint);
		}

		//Remove outliers
		for (edgeVectorIndex = fillStartingIndex;  edgeVectorIndex < edgeCoordinates.size() && edgeVectorIndex != (fillEndingIndex+fillIncrement); edgeVectorIndex = edgeVectorIndex+fillIncrement)
		{
			int coordinateAverage = 0;
			for(int coordinatesQueueIndex =0; coordinatesQueueIndex <= coordinatesQueue.size()-1; coordinatesQueueIndex++)
			{
				coordinateAverage = coordinateAverage + coordinatesQueue[coordinatesQueueIndex].x;
			}
			coordinateAverage = coordinateAverage/(coordinatesQueue.size());

			if( (edgeCoordinates[edgeVectorIndex].x - coordinateAverage) > edgeCoordinateDeviation)
			{
				tempSinglePoint.x = coordinateAverage;
				tempSinglePoint.y = edgeCoordinates[edgeVectorIndex].y;

				edgeCoordinatesFilled[edgeVectorIndex] = tempSinglePoint;
				coordinatesQueue.pop_front();
				coordinatesQueue.push_back(tempSinglePoint);
			}
			else if( (edgeCoordinates[edgeVectorIndex].x - coordinateAverage) < -edgeCoordinateDeviation)
			{
				tempSinglePoint.x = coordinateAverage;
				tempSinglePoint.y = edgeCoordinates[edgeVectorIndex].y;

				edgeCoordinatesFilled[edgeVectorIndex] = tempSinglePoint;
				coordinatesQueue.pop_front();
				coordinatesQueue.push_back(tempSinglePoint);
			}
			else
			{
				tempSinglePoint.x = edgeCoordinates[edgeVectorIndex].x;
				tempSinglePoint.y = edgeCoordinates[edgeVectorIndex].y;

				edgeCoordinatesFilled[edgeVectorIndex] = tempSinglePoint;
				coordinatesQueue.pop_front();
				coordinatesQueue.push_back(tempSinglePoint);
			}

			//If edge was never found assign average edge x coordinate.
			if( edgeCoordinates[edgeVectorIndex].x == 0)
			{
				tempSinglePoint.x = coordinateAverage;
				tempSinglePoint.y = edgeCoordinates[edgeVectorIndex].y;

				edgeCoordinatesFilled[edgeVectorIndex] = tempSinglePoint;
				coordinatesQueue.pop_front();
				coordinatesQueue.push_back(tempSinglePoint);
			}
		}
	}
}

void CXAxisView::FindDeltas(const std::vector<SinglePoint> &leftEdgeCoordinates,
						    const std::vector<SinglePoint> &rightEdgeCoordinates,
						    std::vector<int> &edgeCoordinateDeltas)
{
	edgeCoordinateDeltas.clear();
	int delta = 0;

	int smallerArraySize = min(leftEdgeCoordinates.size(), rightEdgeCoordinates.size() );
	//if(leftEdgeCoordinates.size() == rightEdgeCoordinates.size())
	//{
		for(int i = 0; i < smallerArraySize; i++)
		{
			delta = (leftEdgeCoordinates[i].x- rightEdgeCoordinates[i].x);//abs
			edgeCoordinateDeltas.push_back(delta);
		}
	//}
}

int CXAxisView::CountWaistWidthAboveThreshold(const std::vector<int> &edgeCoordinateDeltas, int threshold)
{
	int count = 0;

	if(threshold > 0)
	{
		for(int i = 0; i < edgeCoordinateDeltas.size(); i++)
		{
			if(edgeCoordinateDeltas[i] >= threshold)
			{
				count++;
			}
		}
	}

	return count;
}

int CXAxisView::FindAverageXValue(const std::vector<SinglePoint> &coordinates)
{
	double average = 0;

	for(int i = 0; i < coordinates.size(); i++)
	{
		average = average + coordinates[i].x;
	}

	average = average / coordinates.size();

	return average;
}


void CXAxisView::InitializeSinglePointCoordinates(int startY, int endY, int defualtX,
												  std::vector<SinglePoint> &outSinglePointCoordinates)
{
	outSinglePointCoordinates.clear();	

	int verticalNumberOfPixelsToCheck = (endY - startY)+1;

	SinglePoint temSinglePoint;
	temSinglePoint.x = defualtX;								
	for(int i = 0; i < verticalNumberOfPixelsToCheck; i++)
	{
		temSinglePoint.y = startY+i;
		outSinglePointCoordinates.push_back(temSinglePoint);
	}
}

void CXAxisView::DisplayImage(CVisImageBase& image, CDC* pDC)
{
	CVisImageBase& refimage = image;
	assert(refimage.IsValid());
	refimage.DisplayInHdc(*pDC);
}

///start of guide lines feature request
///A. Gawlik
void CXAxisView::DrawGuides(CDC *displayContext,CPen* pen, CPen* pen_center,int separation, int height)
{
	int xStart,xMid,xEnd,yStart,yLinePos,yMid,yEnd,numGuidesX=5,pxBuffer=10,numGuidesY=5;
	CPen* pLastPen = NULL;
	CPoint origin;
	//float frac = float(ncams-cam)/float(ncams-cam-1);

	
	//store display context last pen and select new guide pen
	pLastPen = displayContext->SelectObject(pen);
	origin = displayContext->GetWindowOrg();

	//calculate guide lines positions
	xStart = -((-origin.x)%separation); // identifying the x-coordinate of the origin, perform modulo division on the additive inverse and then return the addititive inverse of the result for an offset
	xEnd = separation+xStart;

	yStart = origin.y+pxBuffer;
	//two times the px buffer to take into acount what was added to yStart
	yEnd = height+yStart-2*pxBuffer;


	for(unsigned x_frac=0; x_frac<numGuidesX; x_frac++)
	{
		xMid = int((float(x_frac+1)*float(xEnd-xStart)/float(numGuidesX+1))+xStart);

		//draw vertical lines across image
		displayContext->MoveTo(xMid,yStart);
		displayContext->LineTo(xMid,yEnd);
	}

	//draw line 10 px above bottom of images
	yLinePos = yStart+height - 3*pxBuffer; //yEnd pushed up by pxBuffer
	displayContext->MoveTo(xStart,yLinePos);
	displayContext->LineTo(xEnd,yLinePos);

	for(unsigned y_frac=0; y_frac<numGuidesY; y_frac++)
	{
		yMid = int((float(y_frac+1)*float(yLinePos-yStart)/float(numGuidesY+1))+yStart);

		//draw vertical lines across image
		displayContext->MoveTo(xStart,yMid);
		displayContext->LineTo(xEnd,yMid);
	}
	

	//draw center lines
	displayContext->SelectObject(pen_center);
	xMid=int(float(xEnd+xStart)/2.0f);
	displayContext->MoveTo(xMid,yStart);
	displayContext->LineTo(xMid,yEnd);

	yMid=int(float(yLinePos+yStart)/2.0f);
	displayContext->MoveTo(xStart,yMid);
	displayContext->LineTo(xEnd,yMid);

	//return the previous pen color
	displayContext->SelectObject(pLastPen);
	pLastPen = NULL;
}

void CXAxisView::DrawCapGuides(CDC* displayContext,CPen* pen, CPen* pen_center, int photo_eye_level)
{
	CPen* pLastPen = NULL;
	CPoint origin;
	float scale;
	int middleY,middleX;
	int widthsCap[] = {250,300,400};
	int numWidths = 3;
	RECT rectBounds;

	pLastPen = displayContext->SelectObject(pen_center); //store last pen
	origin = displayContext->GetWindowOrg();

	scale = -CroppedCameraHeight/origin.y;
	rectBounds.left= 0;
	rectBounds.top = 0;
	rectBounds.right = (long)(CroppedCameraWidth/scale);
	rectBounds.bottom = (long)(CroppedCameraHeight/scale);

	middleX = (int)((rectBounds.left+rectBounds.right)/2);
	middleY = (int)((rectBounds.top+rectBounds.bottom)/2);
	//drawing the line that continues across from the photo eye
	displayContext->MoveTo(rectBounds.left,photo_eye_level);
	displayContext->LineTo(rectBounds.right,photo_eye_level);

	//draw each pair of lines as appropriate and draw line down middle of image
	displayContext->MoveTo(middleX,rectBounds.top);
	displayContext->LineTo(middleX,rectBounds.bottom);

	displayContext->SelectObject(pen);

	for(int cap = 0; cap < numWidths; cap++)
	{
		displayContext->MoveTo(middleX-widthsCap[cap]/scale,rectBounds.top);
		displayContext->LineTo(middleX-widthsCap[cap]/scale,rectBounds.bottom);

		displayContext->MoveTo(middleX+widthsCap[cap]/scale,rectBounds.top);
		displayContext->LineTo(middleX+widthsCap[cap]/scale,rectBounds.bottom);
	}

	//reinstate last pen
	displayContext->SelectObject(pLastPen);
	pLastPen=NULL;
}

int CXAxisView::FindCockedLabel1(BYTE *image, int ncam,int factor )
{
	using namespace std;

//	int ncam =1;

	int LineLength			=	theapp->camHeight[ncam]/factor;
	int Height				=	theapp->camWidth[ncam]/factor;



	int pixa,pixb,pixc,pixd,pixe,pixf;
	int d1;
	pixa=pixb=pixc=pixd=pixe=pixf=0;

	std::vector<int> myvector;
	myvector.clear();

	//initi the array which will hold the co ordinates of the resultant points satisfying the condition
	int i;
	for(i=0;i<100;i++) 	cockedLabel1[i].x=cockedLabel1[i].y=0;  

	int count=0;//just to count how many points were picked up
	int resultant_diff=0;

	//   threshold /sensitivity
	int threshold=theapp->jobinfo[pframe->CurrentJobNum].sen_Cocked_Label;


	//Creating the bounds for the traversal
	int xstart=(xMidd[ncam]/factor)-40;
	int xend  =(xMidd[ncam]/factor)+40;
/*
	int ystart=theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam];
	int yend;
	if(ystart-50 <Height) {yend=ystart+50;}
	else yend= Height-20;
*/
	int ystart=theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam];
	int yend;
	if(ystart-50 > 0) {yend=ystart-50;}
	else yend= 20;

	for(int x=xstart;x<xend;x++)
	{
	 
		for(int y=ystart;y>yend;y--)		 //	for(int y=ystart;y<yend;y++)
		{
				pixa=*(image+x-1+(LineLength * (y)));  	
				pixb=*(image+x  +(LineLength * (y)));    
				pixc=*(image+x+1+(LineLength * (y)));  

				pixd=*(image+x-1+(LineLength * (y-2)));
				pixe=*(image+x  +(LineLength * (y-2)));
				pixf=*(image+x+1+(LineLength * (y-2))); //y+2

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));

				if( (d1 > threshold)  )
				{		
						pixa=*(image+x  +(LineLength * (y-4)));  	
						pixb=*(image+x  +(LineLength * (y-5)));   
				
						if(pixa <50 && pixb <50)    //Verifying whether we actually are at the real edge  of label and bottle
						{
							myvector.push_back(y);

							cockedLabel1[count].x=x;cockedLabel1[count].y=y;
							count++;
							y=yend; //forced break out 
						}
				}

		}

	}

	if((myvector.size() >0) )
	{

		sort(myvector.begin(),myvector.end());

		resultant_diff=(abs(myvector.at(0)-myvector.at(myvector.size()-1)));


	}

	/////%%%%%%%%%%% Writing the points onto a text file

				//Writing values to a text file to test 
	/*
		CFile myFile;

		bool success = myFile.Open("C:\\SilganData\\Cam1_coord.txt", CFile::modeWrite | CFile::modeCreate);
		CString tmpSt;

		tmpSt.Format(" \r\n ");
		tmpSt.Format(" \r\n ");
		myFile.Write(tmpSt, tmpSt.GetLength());
			
		for( i= 0;i<count ;i++)
		{
					
						
				tmpSt.Format("X=%i ,Y=%i \r\n ",cockedLabel1[i].x,cockedLabel1[i].y );

					
				myFile.Write(tmpSt, tmpSt.GetLength());

		}
				
		

		
			
		myFile.Close();
*/
////%%%%%%%%%%%%%%


//##VK --adding an extra bit of code to calculate the slope 

	float m_b,m_a;

	// calculate the averages of arrays x and y
     float xa = 0, ya = 0;
	 int n=count; 


//	 x_pt3=y_pt3=x_pt4=y_pt4=0; //for lower

	for ( i = 0; i < n; i++)
	{
			xa += cockedLabel1[i].x;
			ya += cockedLabel1[i].y;
	}

	if(n>0)
	{ 
		xa /= n;    //Average of all X co ordinates
		ya /= n;    //Avg of Y co ord
	}

	// calculate auxiliary sums
	float xx = 0, yy = 0, xy = 0;

	for ( i = 0; i < n; i++) 
	{
			float tmpx = cockedLabel1[i].x - xa, tmpy = cockedLabel1[i].y - ya;
			xx += tmpx * tmpx;
			yy += tmpy * tmpy;
	    	xy += tmpx * tmpy;
	}

	// calculate regression line parameters
 
	// make sure slope is not infinite
	// assert(fabs(xx) != 0);

	if( xx >0)
	{
 					m_b = xy / xx; //slope 
					m_a = ya - m_b * xa; // intercept
					float m_coeff = (fabs(yy) == 0) ? 1 : xy / sqrt(xx * yy); // regression co eff
					
				
				//	slopel=m_b;
				//	intrcpl=m_a;
					m_b=abs((int)(m_b*1000));   //For better resolution
				
	}

	TRACE("Slope1 = %f      ",m_b);


//#############################



	//$$$$$$$$$$$$$$  VK -- Doing the linear regression again 
/*
slope b = r * Sy/Sx ;

r=Summation(xy)


int XY,X2,Y2 
*/









	//$$$$$$$$$$$$$$$$


//return resultant_diff;
return m_b;

}


int CXAxisView::FindCockedLabel2(BYTE *image, int ncam,int factor )
{
	using namespace std;

//	int ncam =1;

	int LineLength			=	theapp->camHeight[ncam]/factor;
	int Height				=	theapp->camWidth[ncam]/factor;

//	OnSaveBW(image,1);

	int pixa,pixb,pixc,pixd,pixe,pixf;
	int d1;
	pixa=pixb=pixc=pixd=pixe=pixf=0;

	std::vector<int> myvector;
	myvector.clear();

	//initi the array which will hold the co ordinates of the resultant points satisfying the condition
	int i;
	for(i=0;i<100;i++) 	cockedLabel2[i].x=cockedLabel2[i].y=0;  

	int count=0;//just to count how many points were picked up
	int resultant_diff=0;

	//   threshold /sensitivity
	int threshold=theapp->jobinfo[pframe->CurrentJobNum].sen_Cocked_Label;


	//Creating the bounds for the traversal
	int xstart=(xMidd[ncam]/factor)-40;
	int xend  =(xMidd[ncam]/factor)+40;
/*
	int ystart=theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam];
	int yend;
	if(ystart-50 <Height) {yend=ystart+50;}
	else yend= Height-20;
*/
	int ystart=theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam];
	int yend;
	if(ystart-50 > 0) {yend=ystart-50;}
	else yend= 20;


	for(int x=xstart;x<xend;x++)
	{
	 
		for(int y=ystart;y>yend;y--)   //for(int y=ystart;y<yend;y++)
		{
				pixa=*(image+x-1+(LineLength * (y)));  	
				pixb=*(image+x  +(LineLength * (y)));    
				pixc=*(image+x+1+(LineLength * (y)));  

				pixd=*(image+x-1+(LineLength * (y-2)));
				pixe=*(image+x  +(LineLength * (y-2)));
				pixf=*(image+x+1+(LineLength * (y-2)));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));

				if( (d1 > threshold)  )
				{		
						pixa=*(image+x  +(LineLength * (y-4)));   //(y+4	
						pixb=*(image+x  +(LineLength * (y-5)));   //(y+5
					
						if(pixa <50 && pixb <50)    //Verifying whether we actually are at the real edge  of label and bottle
						{
							myvector.push_back(y);

							cockedLabel2[count].x=x;cockedLabel2[count].y=y;
							count++;
							y=yend; //forced break out 
						}
				}

		}

	}

	if((myvector.size() >0) )
	{

		sort(myvector.begin(),myvector.end());

		resultant_diff=(abs(myvector.at(0)-myvector.at(myvector.size()-1)));


	}

	/////%%%%%%%%%%% Writing the points onto a text file
/*
				//Writing values to a text file to test 
	
		CFile myFile;

		bool success = myFile.Open("C:\\SilganData\\Cam2_coord.txt", CFile::modeWrite | CFile::modeCreate);
		CString tmpSt;

		tmpSt.Format(" \r\n ");
		tmpSt.Format(" \r\n ");
		myFile.Write(tmpSt, tmpSt.GetLength());
			
		for( i= 0;i<count ;i++)
				{
					
						
						tmpSt.Format("X=%i ,Y=%i \r\n ",cockedLabel2[i].x,cockedLabel2[i].y );

					
						myFile.Write(tmpSt, tmpSt.GetLength());

				}
				
		

		
			
			myFile.Close();
*/
////%%%%%%%%%%%%%%



	//##VK --adding an extra bit of code to calculate the slope 

	float m_b,m_a;

	// calculate the averages of arrays x and y
     float xa = 0, ya = 0;
	 int n=count; 


//	 x_pt3=y_pt3=x_pt4=y_pt4=0; //for lower

	for ( i = 0; i < n; i++)
	{
			xa += cockedLabel2[i].x;
			ya += cockedLabel2[i].y;
	}

	if(n>0)
	{
		xa /= n;
		ya /= n;
	}

	// calculate auxiliary sums
	float xx = 0, yy = 0, xy = 0;

	for ( i = 0; i < n; i++) 
	{
			float tmpx = cockedLabel2[i].x - xa, tmpy = cockedLabel2[i].y - ya;
			xx += tmpx * tmpx;
			yy += tmpy * tmpy;
	    	xy += tmpx * tmpy;
	}

	// calculate regression line parameters
 
	// make sure slope is not infinite
	// assert(fabs(xx) != 0);

	if( xx >0)
	{
 					m_b = xy / xx; //slope 
					m_a = ya - m_b * xa; // intercept
					float m_coeff = (fabs(yy) == 0) ? 1 : xy / sqrt(xx * yy); // regression co eff
					
				
				//	slopel=m_b;
				//	intrcpl=m_a;
					m_b=abs((int)(m_b*1000));
				
	}

	TRACE("     Slope2 = %f ",m_b);


//#############################

return m_b;

}



int CXAxisView::FindCockedLabel3(BYTE *image, int ncam,int factor )
{
	using namespace std;

//	int ncam =1;

	int LineLength			=	theapp->camHeight[ncam]/factor;
	int Height				=	theapp->camWidth[ncam]/factor;

//	OnSaveBW(image,1);

	int pixa,pixb,pixc,pixd,pixe,pixf;
	int d1;
	pixa=pixb=pixc=pixd=pixe=pixf=0;

	std::vector<int> myvector;
	myvector.clear();

	//initi the array which will hold the co ordinates of the resultant points satisfying the condition
	int i;
	for(i=0;i<100;i++) 	cockedLabel3[i].x=cockedLabel3[i].y=0;

	int count=0;//just to count how many points were picked up
	int resultant_diff=0;

	//   threshold /sensitivity
	int threshold=theapp->jobinfo[pframe->CurrentJobNum].sen_Cocked_Label;


	//Creating the bounds for the traversal
	int xstart=(xMidd[ncam]/factor)-40;
	int xend  =(xMidd[ncam]/factor)+40;
/*
	int ystart=theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam];
	int yend;
	if(ystart-50 <Height) {yend=ystart+50;}
	else yend= Height-20;
*/
	int ystart=theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam];
	int yend;
	if(ystart-50 > 0) {yend=ystart-50;}
	else yend= 20;

	for(int x=xstart;x<xend;x++)
	{
	 
		for(int y=ystart;y>yend;y--)
		{
				pixa=*(image+x-1+(LineLength * (y)));  	
				pixb=*(image+x  +(LineLength * (y)));    
				pixc=*(image+x+1+(LineLength * (y)));  

				pixd=*(image+x-1+(LineLength * (y-2)));
				pixe=*(image+x  +(LineLength * (y-2)));
				pixf=*(image+x+1+(LineLength * (y-2)));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));

				if( (d1 > threshold)  )
				{		
						pixa=*(image+x  +(LineLength * (y-4)));  	
						pixb=*(image+x  +(LineLength * (y-5)));   
					
						if(pixa <50 && pixb<50)    //Verifying whether we actually are at the real edge  of label and bottle
						{
							myvector.push_back(y);

							cockedLabel3[count].x=x;cockedLabel3[count].y=y;
							count++;
							y=yend; //forced break out 
						}
				}

		}

	}

	if((myvector.size() >0) )
	{

		sort(myvector.begin(),myvector.end());

		resultant_diff=(abs(myvector.at(0)-myvector.at(myvector.size()-1)));


	}
/////%%%%%%%%%%% Writing the points onto a text file

				//Writing values to a text file to test 
	/*
		CFile myFile;

		bool success = myFile.Open("C:\\SilganData\\Cam3_coord.txt", CFile::modeWrite | CFile::modeCreate);
		CString tmpSt;

		tmpSt.Format(" \r\n ");
		tmpSt.Format(" \r\n ");
		myFile.Write(tmpSt, tmpSt.GetLength());
			
		for( i= 0;i<count ;i++)
				{
					
						
						tmpSt.Format("X=%i ,Y=%i \r\n ",cockedLabel3[i].x,cockedLabel3[i].y );

					
						myFile.Write(tmpSt, tmpSt.GetLength());

				}
				
		

		
			
			myFile.Close();
*/
////%%%%%%%%%%%%%%

	//##VK --adding an extra bit of code to calculate the slope 

	float m_b,m_a;

	// calculate the averages of arrays x and y
     float xa = 0, ya = 0;
	 int n=count; 


//	 x_pt3=y_pt3=x_pt4=y_pt4=0; //for lower

	for ( i = 0; i < n; i++)
	{
			xa += cockedLabel3[i].x;
			ya += cockedLabel3[i].y;
	}

	if(n>0)
	{
		xa /= n;
		ya /= n;
	}

	// calculate auxiliary sums
	float xx = 0, yy = 0, xy = 0;

	for ( i = 0; i < n; i++) 
	{
			float tmpx = cockedLabel3[i].x - xa, tmpy = cockedLabel3[i].y - ya;
			xx += tmpx * tmpx;
			yy += tmpy * tmpy;
	    	xy += tmpx * tmpy;
	}

	// calculate regression line parameters
 
	// make sure slope is not infinite
	// assert(fabs(xx) != 0);

	if( xx >0)
	{
 					m_b = xy / xx; //slope 
					m_a = ya - m_b * xa; // intercept
					float m_coeff = (fabs(yy) == 0) ? 1 : xy / sqrt(xx * yy); // regression co eff
					
				
				//	slopel=m_b;
				//	intrcpl=m_a;
					m_b=abs((int)(m_b*1000));
				
	}

	TRACE("      Slope3 = %f ",m_b);


//#############################


//return resultant_diff;
return m_b;


}


int CXAxisView::FindCockedLabel4(BYTE *image, int ncam,int factor )
{
	using namespace std;

//	int ncam =1;

	int LineLength			=	theapp->camHeight[ncam]/factor;
	int Height				=	theapp->camWidth[ncam]/factor;

//	OnSaveBW(image,1);

	int pixa,pixb,pixc,pixd,pixe,pixf;
	int d1;
	pixa=pixb=pixc=pixd=pixe=pixf=0;

	std::vector<int> myvector;
	myvector.clear();

	//initi the array which will hold the co ordinates of the resultant points satisfying the condition
	int i;
	for(i=0;i<100;i++) 	cockedLabel4[i].x=cockedLabel4[i].y=0;

	int count=0;//just to count how many points were picked up
	int resultant_diff=0;

	//   threshold /sensitivity
	int threshold=theapp->jobinfo[pframe->CurrentJobNum].sen_Cocked_Label;


	//Creating the bounds for the traversal
	int xstart=(xMidd[ncam]/factor)-40;
	int xend  =(xMidd[ncam]/factor)+40;
/*
	int ystart=theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam];
	int yend;
	if(ystart-50 <Height) {yend=ystart+50;}
	else yend= Height-20;
*/
	int ystart=theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam];
	int yend;
	if(ystart-50 > 0) {yend=ystart-50;}
	else yend= 20;

	for(int x=xstart;x<xend;x++)
	{
	 
		for(int y=ystart;y>yend;y--)
		{
				pixa=*(image+x-1+(LineLength * (y)));  	
				pixb=*(image+x  +(LineLength * (y)));    
				pixc=*(image+x+1+(LineLength * (y)));  

				pixd=*(image+x-1+(LineLength * (y-2)));
				pixe=*(image+x  +(LineLength * (y-2)));
				pixf=*(image+x+1+(LineLength * (y-2)));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));

				if( (d1 > threshold)  )
				{		
						pixa=*(image+x  +(LineLength * (y-4)));  	
						pixb=*(image+x  +(LineLength * (y-5)));   
					
						if(pixa<50 && pixb<50)    //Verifying whether we actually are at the real edge  of label and bottle
						{
							myvector.push_back(y);

							cockedLabel4[count].x=x;cockedLabel4[count].y=y;
							count++;
							y=yend; //forced break out 
						}
				}

		}

	}

	if((myvector.size() >0) )
	{

		sort(myvector.begin(),myvector.end());

		resultant_diff=(abs(myvector.at(0)-myvector.at(myvector.size()-1)));


	}


	/////%%%%%%%%%%% Writing the points onto a text file

				//Writing values to a text file to test 
	/*
		CFile myFile;

		bool success = myFile.Open("C:\\SilganData\\Cam4_coord.txt", CFile::modeWrite | CFile::modeCreate);
		CString tmpSt;

		tmpSt.Format(" \r\n ");
		tmpSt.Format(" \r\n ");
		myFile.Write(tmpSt, tmpSt.GetLength());
			
		for( i= 0;i<count ;i++)
				{
					
						
						tmpSt.Format("X=%i ,Y=%i \r\n ",cockedLabel4[i].x,cockedLabel4[i].y );

					
						myFile.Write(tmpSt, tmpSt.GetLength());

				}
				
		

		
			
			myFile.Close();
*/
////%%%%%%%%%%%%%%



	//##VK --adding an extra bit of code to calculate the slope 

	float m_b,m_a;

	// calculate the averages of arrays x and y
     float xa = 0, ya = 0;
	 int n=count; 


//	 x_pt3=y_pt3=x_pt4=y_pt4=0; //for lower

	for ( i = 0; i < n; i++)
	{
			xa += cockedLabel4[i].x;
			ya += cockedLabel4[i].y;
	}

	if(n>0)
	{
		xa /= n;
		ya /= n;
	}

	// calculate auxiliary sums
	float xx = 0, yy = 0, xy = 0;

	for ( i = 0; i < n; i++) 
	{
			float tmpx = cockedLabel4[i].x - xa, tmpy = cockedLabel4[i].y - ya;
			xx += tmpx * tmpx;
			yy += tmpy * tmpy;
	    	xy += tmpx * tmpy;
	}

	// calculate regression line parameters
 
	// make sure slope is not infinite
	// assert(fabs(xx) != 0);

	if( xx >0)
	{
 					m_b = xy / xx; //slope 
					m_a = ya - m_b * xa; // intercept
					float m_coeff = (fabs(yy) == 0) ? 1 : xy / sqrt(xx * yy); // regression co eff
					
				
				//	slopel=m_b;
				//	intrcpl=m_a;
				
				m_b=abs((int)(m_b*1000));
				
	}

	TRACE("   Slope4 = %f \n",m_b);


//#############################

//return resultant_diff;
return m_b;

}

