// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "Bottle.h"

#include "MainFrm.h"
#include "Camera.h"

#include "serial.h"
#include "job.h"
#include "xaxisview.h"
#include "setup.h"
#include "insp.h"
#include "bottleview.h"
#include "yaxisview.h"
#include "pics.h"

#include "blowoff.h"
#include "bottledown.h"
#include "Data.h"
#include "view.h"
#include "cap.h"
#include "capSetup1.h"
#include "security.h"

#include "light.h"
#include "motor.h"
#include "modify.h"

#include "movehead.h"
#include "photoeye.h"
#include "digio.h"
#include "tech.h"
#include "advanced.h"
#include "splitter.h"

#include "PicRecal.h"
#include "Usb.h"
#include "WaistInspection.h"
#include "LabelMatingSetup.h"
#include "SleeverHeadData.h"
#include "SysConfig.h"
#include "WhiteBalance.h"

#include <iomanip>
#include <exception>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_TIMER()
	ON_COMMAND(ID_SERIAL, OnSerial)
	ON_COMMAND(ID_JOBCHANGE, OnJobchange)
	ON_WM_CLOSE()
	ON_COMMAND(ID_SETUP2, OnSetup2)
	ON_COMMAND(ID_PICS, OnPics)
	ON_COMMAND(ID_EJECT, OnEject)
	ON_COMMAND(ID_SECURITY, OnSecurity)
	ON_COMMAND(ID_LIGHT, OnLight)
	ON_COMMAND(ID_MOTOR, OnMotor)
	ON_COMMAND(ID_PHOTOEYE, OnPhotoeye)
	ON_COMMAND(ID_DIGIO, OnDigio)
	ON_COMMAND(ID_DATA, OnData)
	ON_COMMAND(ID_SETUP4, OnSetup4)
	ON_COMMAND(ID_LOGOUT, OnLogout)
	ON_COMMAND(IDC_SHOWGUIDE, OnShowguide)
	ON_COMMAND(ID_ADVANCED, OnAdvanced)
	ON_COMMAND(ID_RECALLDEFECTIVE, OnRecalldefective)
	ON_COMMAND(IDC_DOWNBOTTLE, OnDownbottle)
	ON_COMMAND(IDC_85CONFIG, OnSystemConfig)
	ON_COMMAND(ID_CAMERASETTINGS, OnCamerasettings)
	ON_COMMAND(ID_ADJUSTWHTBAL, OnAdjustWhtBal)
	ON_WM_CREATE()
	ON_COMMAND(ID_USB, OnUsb)
	ON_WM_CREATE()
	ON_COMMAND(ID_DATA_HEADTRACKERDATA, OnDataHeadtrackerdata)
	ON_COMMAND(ID_DATA, OnData)
	//}}AFX_MSG_MAP

	ON_MESSAGE(WM_NEWJOB,NewJob)
	ON_MESSAGE(WM_INSPECT,Inspect)
	ON_COMMAND(ID_HELP, CMDIFrameWnd::OnHelpIndex)
	ON_MESSAGE(WM_TRIGGER,Trigger)
	ON_MESSAGE(WM_RELOAD,ReLoad)
	ON_MESSAGE(WM_SETLIGHTLEVEL,SetLightLevel)
	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

using namespace std;

CMainFrame::CMainFrame()
{
	CRect r;
    r.SetRect(0, 0, 800, 600); 
     //pWnd = AfxGetMainWnd();   
    Create(NULL, 
    "Bottle Inspection System",
    WS_VISIBLE, //| WS_OVERLAPPEDWINDOW,
    r, 
    this,
    MAKEINTRESOURCE(IDR_MAINFRAME));// MAKEINTRESOURCE(IDR_MAINFRAME)

	m_pform=NULL;
	CurrentJobNum=0;
	SetTimer(3,50,NULL);//Init()
}

CMainFrame::~CMainFrame()
{
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

void CMainFrame::Init()
{	
	theapp=(CBottleApp*)AfxGetApp();
	theapp->pframeSub=this;

	countC0 = countC1 = countC2 = countC3 = countC4 =  countC5 = 1;
	notFinished = 0;
	sameImg = 0;
	
	missPic1 = missPic2 = missPic3 = missPic4 = false;

	_counterTemp=TEMP_COUNTER=0;

	pParent=GetParent(); 
	
	if(theapp->serPresent)
	{
		m_pserial=new Serial();
		try
		{
			m_pserial->Create(IDD_SERIAL, pParent);
			m_pserial->ControlChars=0;
		}
		catch(std::exception& e)
		{
			TRACE(e.what());
			
//			theapp->serPresent = false;
			delete m_pserial;
			m_pserial = NULL;
		}
	}
	
	m_pwhtbal=new CWhiteBalance();
	m_pwhtbal->Create(IDD_WHTBAL, pParent);
	
	m_pusb=new Usb();
	m_pusb->Create(IDD_USB, pParent);

	m_sysconfigform = new CSysConfig();
	m_sysconfigform->Create(IDD_SYSCONFIG, pParent);

	theapp->rejectAllKrones=false;

	if(theapp->camsPresent)
	{
		m_pcamera=new CCamera();
		m_pcamera->Create(IDD_CAMERA, pParent);
		m_pcamera->StartCams();
	}

	SSecretMenu = false;

	CopyDone=false;
	conRejectLatch=false;
	IOReject=false;
	IORejects=0;
	IODone=true;
	IODebounce=false;
	IORejectLimit=theapp->SavedIORejects;
	ConRejectLimit=theapp->SavedConRejects;
	ConRejectLimit2=theapp->SavedConRejects2;
	conreject=0;
	PicsOpen=false;
	Faulted=false;
	theapp->Total=0;
	theapp->labelOpen=false;
	Rejects=0;
	EjectRate=0;
	Freeze=false;
	CountEnable=false;
	debounce=false;
	WasSetup=false;
	InspOpen=false;
	JobNoGood=false;
	Safe=false;
	OnLine=false;
	LockUp=0;
	Condition1=0;
	Condition2=0;
	reloading=false;
	BottleRate=0;
	OldBottleCount=0;
	TriggerCount=OldTriggerCount=NewTriggerCount=0;
	gotNext=0;
	testData=false;
	timesThru=0;
	got1=0;
	got2=0;
	got3=0;
	got4=0;
	everyOther=false;
	toggle=false;

	/////Sleever Tracker init

	_sleeverHeadNumber=1;
	_counterSleever=1;
	_inspectionNumber=0;
	memset(arrayA,0,sizeof(arrayA));
	memset(_arrayLocal,0,sizeof(_arrayLocal));
	

	isValidated=false;
	sleeverHeadUnlocked=true;
	_PLCdecision=0;

//	AfxBeginThread( TestThreading, this );
	//////////////
	
	picNum=1;
	picDefNum=1;
	pictoShow=1;

	//setSecurityFlagsOFF(theapp->SecOff);
	setSecurityFlagsOFF(false); //ignore "SecurityEnable"/"SecOff" flags and always have security active

	seqComp = 0;

	TeachPicC1=false;

	ToggleCheckStatus=true;
	Stop=false;
	
	WaitNext=false;
	
	if(theapp->serPresent)
	{
		m_pserial->SendChar(0,410);
		PhotoEyeEnable(true);
	}
	
	SetTimer(11,10000,NULL);	//timer for bottles/min and freeze status
	SetTimer(12,1000,NULL);		//get motor position from PLC
	//SetTimer(24,100,NULL);	//com
	
	//SetTimer(27,10000,NULL);	//heartbeat
	//SetTimer(8,100,NULL);		//com
	//SetTimer(1,240000,NULL);	//four minutes

	SetTimer(43, 1000, NULL);
	
	m_pCrntFormat = NULL;
	m_pCrntMode = NULL;
	m_pCrntFrate = NULL;
	m_pTrigger = NULL;
	SetTimer(26,2000,NULL);//enable eject

	GetJob(0);

	showMagentaImg	=false;
	showCyanImg		=false;
	showYellowImg	=false;
	showSaturatImg	=false;
	showSatura2Img	=false;
	showBWImg		=false;
	showBWRedImg	=false;
	showBWGreenImg	=false;
	showBWBlueImg	=false;

	showCapBWImg		=false;
	showCapBWRedImg		=false;
	showCapBWGreenImg	=false;
	showCapBWBlueImg	=false;

	
	showEdgeHImg	=false;
	showEdgeVImg	=false;
	showBinImg		=false;
	showEdgFinalImg	=false;
	showBinImgMetho5=false;

	countC1 = countC2 = countC3 = countC4 =   countC5 = 1;

	gotPicC1 = false;
	gotPicC2 = false;
	gotPicC3 = false;
	gotPicC4 = false;
	gotPicC5 = false;


	/////////////////////////////////////////////
	
	imgShown = 0;

	//to manipulate Screen Saver
//	screenSaverTimeOut = GetScreenSaverTimeout();//seconds
//	SetTimer(42, screenSaverTimeOut*1000, NULL);
//	flagScreenSaver = false;

	/////////////////////////////////////////////

}

//Open White Balance Dialog Box
void CMainFrame::OnAdjustWhtBal() 
{	
//	SSecretMenu=false;	//	Resets upon entry
//	Security sec;
//	sec.DoModal();

	if(SSecretMenu==true)
	{	
		m_pwhtbal->ShowWindow(SW_SHOW);
	}
	else
	{
		Security sec; sec.DoModal();
		if(SSecretMenu)
		{
			m_pwhtbal->ShowWindow(SW_SHOW);
		}
	}
}

bool CMainFrame::SetWaistEnable(bool waistEnab)
{
	bool changed = false;
	if(theapp->waistAvailable != waistEnab)
	{
		theapp->WriteProfileInt("waistEnabled", "Current", waistEnab ? 1 : 0);
		changed = true;

		CString widthStr;
		CString heightStr;

		//	Push CamSize Changes to Registry
		int width = waistEnab ? FullImageWidth : CroppedImageWidth;
		int height = waistEnab ? FullImageHeight : CroppedImageHeight;

		for(int i = 1; i < 5; i++)
		{
			heightStr.Format("camHeight%d",i);
			widthStr.Format("camWidth%d",i);

			theapp->WriteProfileInt(heightStr, "Current", width);
			theapp->WriteProfileInt(widthStr, "Current", height);
		}

		heightStr.Format("camHeight%d",5);
		widthStr.Format("camWidth%d",5);

		theapp->WriteProfileInt(heightStr, "Current", CroppedImageWidth);
		theapp->WriteProfileInt(widthStr, "Current", CroppedImageHeight);
	}
	return changed;
}

bool CMainFrame::SetTrackerEnable(bool trackerEnab)
{
	bool changed = false;
	if(theapp->sleeverEnable != trackerEnab)
	{
		//	Update UI
		theapp->WriteProfileInt("sleeverEnable", "Current", trackerEnab ? 1 : 0);
		changed = true;
	}
	return changed;
}

bool CMainFrame::SetWelchsEnable(bool welchsEnab)
{
	bool changed = false;
	if(theapp->useDiff51R85Hardware != welchsEnab)
	{
		//	Update UI
		theapp->WriteProfileInt("useDiff51R85Hardware", "Current", welchsEnab ? 1 : 0);
		changed = true;
	}
	return changed;
}

bool CMainFrame::SetAlignmentEnable(bool alignmentEnab)
{
	bool changed = false;
	if(theapp->alignAvailable != alignmentEnab)
	{
		//	Update UI
		theapp->WriteProfileInt("alignmentEnabled", "Current", alignmentEnab ? 1 : 0);
		changed = true;
	}
	return changed;
}

bool CMainFrame::SetMachineType(MachineType machType)
{
	bool changed = false;
	if(theapp->cam5Enable != (machType == 0 ? false : true))
	{
		//	Update UI
		theapp->WriteProfileInt("cam5enable", "Current", machType == 0 ? false : true);				//	The only machine codes available at the moment are 0 and 1
		changed = true;
	}
	return changed;
}

void CMainFrame::OnTimer(UINT nIDEvent) 
{
	switch(nIDEvent)
	{
	
		case 1:
		{
			if(theapp->resetFromPLC)
			{ResetTotals2(); theapp->resetFromPLC=false;}
			
			if(theapp->Totalx> oldTotal)
			{
				SetTimer(30,100,NULL);
				dataReady=true;
				oldTotal=theapp->Totalx;
			}
			break;}
		case 2:
		{
			Faulted=true;
			KillTimer(8);
			m_pview->m_status="Comm to PLC Lost";
			GotFault();

			break;}
		case 3:
		{	KillTimer(3); Init();
			break;}
		case 4:
		{	KillTimer(4);
			break;}
		case 5:
		{	KillTimer(5);
			m_pxaxis->InvalidateRect(NULL,false);
			break;}
		case 6:
		{
			break;}
		case 7:
		{	KillTimer(7);
			break;}
		case 8:
		{	KillTimer(8);
			break;}
		case 9:
		{	KillTimer(9);
			break;}
		case 10:
		{	KillTimer(10); Stop=true;
			break;}
		case 11:
		{	//look every 10 secs
			BottleRate=float(theapp->Total-OldBottleCount)*6;
			OldBottleCount=theapp->Total;
			break;}
		case 12:
		{	if(theapp->serPresent)	{MotPos=m_pserial->MotorPos;}
			KillTimer(12);
			break;}
		case 13:
		{	KillTimer(13);
			setSecurityFlagsOFF(false);
			Safe=false;
			//m_pyaxis->m_tabmenu.SetCurSel(0);
			m_pyaxis->m_tabmenu.SetCurFocus(0);
			theapp->lock=true;
			break;}
		case 14:
		{	debounce=false; KillTimer(14);
			break;}

		case 15:
		{	KillTimer(15);
			break;}
		case 21:
		{	int Value=0; int Function=0; int CntrBits=0;
		
			if(theapp->serPresent)		{m_pserial->ControlChars=Value+Function+CntrBits;}
			
 			KillTimer(21);
			break;}
		case 22:
		{	KillTimer(22);
			break;}
		case 23:
		{	SendMessage(WM_TRIGGER,NULL,NULL); 
			KillTimer(23);
			break;}
		case 24:
		{	KillTimer(24);
			break;}
		case 26:
		{	if(theapp->serPresent)		{m_pserial->SendChar(0,110);} 
			KillTimer(26);
			break;}
		case 27:
		{	
			if(theapp->serPresent)
			{
			//if(m_pserial->Temperature>=theapp->tempLimit)
			//{
			//	m_pserial->SendChar(0,470);
			//	m_pview->m_status="Over Temperature Limit!!!";
			//}

			m_pserial->SendChar(0,50);
			}
			break;}
		case 28:
		{	KillTimer(28);
			break;}
		case 29:
		{	KillTimer(29);
			break;}
		case 30:
		{	UpdatePLCData();
			if(!dataReady){KillTimer(30);}
			break;}
		case 31:
		{	KillTimer(31); UpdatePLC();
			break;}
		case 35:
		{	KillTimer(35);

			int Value		= theapp->SavedEncDur;
			int Function	= 90;

			if(theapp->serPresent)		{m_pserial->SendChar(Value,Function);}
			break;}
		case 36:
		{	KillTimer(36);

			int Value		= theapp->SavedEncDist;
			int Function	= 80;

			if(theapp->serPresent)		{m_pserial->SendChar(Value,Function);}
			break;}
		case 37:
		{	KillTimer(37);

			int Value		= 0;
			int Function	= 0;
		
		
			if(theapp->serPresent)
			{
				//PhotoEye direction
				//if(theapp->SavedPhotoeye)	{Function = 220;}	//Bottle flow is from the right
				//else							{Function = 230;}	//Left
				if(theapp->SavedPhotoeye)	{Function = 230;}	//Bottle flow is from the right
				else							{Function = 220;}	//Left


				m_pserial->SendChar(Value,Function);
			}
		
			break;
		}

		case 38:
		{	KillTimer(38);

			if(theapp->serPresent)
			{
				int Value		= theapp->SavedDelay;
				int Function	= 70;

				m_pserial->SendChar(Value,Function);
			} //Snap delay
			break;
		}

		case 39:
		{	KillTimer(39);

			if(theapp->serPresent)
			{
				int Value		= theapp->SavedDeadBand;
				int Function	= 120;
				m_pserial->SendChar(Value,Function);
			} //Cap width
			break;}

		case 40:
		{	KillTimer(40);
		
			if(theapp->serPresent)
			{
				int Value=0; int Function;
				
				if(theapp->SavedBottleSensor==0)
				{
					Function=180;	//Disable Down Bottle
					m_pserial->SendChar(Value,Function);
				}

				if(theapp->SavedBottleSensor==1)
				{
					Function=170;	//Enable Down Bottle
					m_pserial->SendChar(Value,Function);
				}
			}
			break;}

		case 41:
		{
			KillTimer(41);
		
			if(theapp->serPresent)
			{
				int Value		= theapp->SavedDBOffset;
				int Function	= 610;
				
				m_pserial->SendChar(Value,Function);
			}
			break;
		}

		case 42:
		{
			flagScreenSaver = true;
			break;
		}

		case 43:
		{
			KillTimer(43);
		
			bool useRing = theapp->jobinfo[CurrentJobNum].useRing;
			
			if(theapp->serPresent)	{m_pserial->SendChar(0, useRing ? 270 : 260);}
			break;
		}

		case 44:
		{
			break;
		}
	
		default:
		{
		break;
		}

		
	
	}

	CMDIFrameWnd::OnTimer(nIDEvent);


	/*
///#######
	if (nIDEvent==1) 
	{	
		if(theapp->resetFromPLC)
		{ResetTotals2(); theapp->resetFromPLC=false;}
		
		if(theapp->Totalx> oldTotal)
		{
			SetTimer(30,100,NULL);
			dataReady=true;
			oldTotal=theapp->Totalx;
		}
	}

	if (nIDEvent==2) 
	{
		Faulted=true;
		KillTimer(8);
		m_pview->m_status="Comm to PLC Lost";
		GotFault();
	}
	
	if (nIDEvent==3)	{KillTimer(3); Init();}	
	if (nIDEvent==4) 	{KillTimer(4);}
	if (nIDEvent==5)	{KillTimer(5); m_pxaxis->InvalidateRect(NULL,false);}
	if (nIDEvent==7) 	{KillTimer(7);}
	if (nIDEvent==8) 	{KillTimer(8);}
	if (nIDEvent==9) 	{KillTimer(9);}
	if (nIDEvent==10) 	{KillTimer(10); Stop=true;}
	
	if (nIDEvent==11) 
	{
		//look every 10 secs
		BottleRate=float(theapp->Total-OldBottleCount)*6;
		OldBottleCount=theapp->Total;
	}

	if (nIDEvent==12)	
	{
		if(theapp->serPresent)	{MotPos=m_pserial->MotorPos;}
		KillTimer(12);
	}

	if (nIDEvent==13) 
	{
		KillTimer(13);
		setSecurityFlagsOFF(false);

		Safe=false;

		//m_pyaxis->m_tabmenu.SetCurSel(0);
		m_pyaxis->m_tabmenu.SetCurFocus(0);
							

		theapp->lock=true;
	}

	if (nIDEvent==14)	{debounce=false; KillTimer(14);}

	if (nIDEvent==15) 	{KillTimer(15);}

	if (nIDEvent==21) 
	{
		int Value=0; int Function=0; int CntrBits=0;
		
		if(theapp->serPresent)		{m_pserial->ControlChars=Value+Function+CntrBits;}
		
 		KillTimer(21);
	}

	if (nIDEvent==22)		{KillTimer(22);}

	if (nIDEvent==23)		{SendMessage(WM_TRIGGER,NULL,NULL); KillTimer(23);}

	if (nIDEvent==24)		{KillTimer(24);	}

	if (nIDEvent==26) 	
	{
		if(theapp->serPresent)		{m_pserial->SendChar(0,110);} 
		KillTimer(26);
	}

	if (nIDEvent==27) 
	{	
		if(theapp->serPresent)
		{
			//if(m_pserial->Temperature>=theapp->tempLimit)
			//{
			//	m_pserial->SendChar(0,470);
			//	m_pview->m_status="Over Temperature Limit!!!";
			//}

			m_pserial->SendChar(0,50);
		}
	}

	if (nIDEvent==28)		{KillTimer(28);}
	if (nIDEvent==29)		{KillTimer(29);}
	
	if (nIDEvent==30) 
	{
		UpdatePLCData();
		if(!dataReady){KillTimer(30);}
	}

	if (nIDEvent==31)		{KillTimer(31); UpdatePLC();}

	/////////////////

	if(nIDEvent==35) 
	{
		KillTimer(35);

		int Value		= theapp->SavedEncDur;
		int Function	= 90;

		if(theapp->serPresent)		{m_pserial->SendChar(Value,Function);}
	}
 
	if(nIDEvent==36) 
	{
		KillTimer(36);

		int Value		= theapp->SavedEncDist;
		int Function	= 80;

		if(theapp->serPresent)		{m_pserial->SendChar(Value,Function);}
	}
 
	if(nIDEvent==37) 
	{
		KillTimer(37);

		int Value		= 0;
		int Function	= 0;
	
//	if(theapp->serPresent)
//		{
			//PhotoEye direction
//			if(theapp->SavedPhotoeye)	{Function = 220;}	//Bottle flow is from the right
//			else							{Function = 230;}	//Left

//			if(theapp->serPresent)			{m_pserial->SendChar(Value,Function);}
//		}
	
		
	}
 
	if(nIDEvent==38) 
	{
		KillTimer(38);

		if(theapp->serPresent)
		{
			int Value		= theapp->SavedDelay;
			int Function	= 70;

			m_pserial->SendChar(Value,Function);
		} //Snap delay
	}

	if(nIDEvent==39) 
	{
		KillTimer(39);

		if(theapp->serPresent)
		{
			int Value		= theapp->SavedDeadBand;
			int Function	= 120;
			m_pserial->SendChar(Value,Function);
		} //Cap width
	}

	if(nIDEvent==40) 
	{
		KillTimer(40);
		
		if(theapp->serPresent)
		{
			int Value=0; int Function;
			
			if(theapp->SavedBottleSensor==0)
			{
				Function=180;	//Disable Down Bottle
				m_pserial->SendChar(Value,Function);
			}

			if(theapp->SavedBottleSensor==1)
			{
				Function=170;	//Enable Down Bottle
				m_pserial->SendChar(Value,Function);
			}
		}
	}

	if(nIDEvent==41) 
	{
		KillTimer(41);
		
		if(theapp->serPresent)
		{
			int Value		= theapp->SavedDBOffset;
			int Function	= 610;
			
			m_pserial->SendChar(Value,Function);
		}
	}

	if(nIDEvent==42) 
	{
		flagScreenSaver = true;
	}

	if(nIDEvent==43) 
	{
		KillTimer(43);
		
		bool useRing = theapp->jobinfo[CurrentJobNum].useRing;
		
		if(theapp->serPresent)	{m_pserial->SendChar(0, useRing ? 270 : 260);}
	}

	if(nIDEvent==44) 
	{
	//	m_pxaxis->InvalidateRect(NULL, true);
	}

	/////////////////

	CMDIFrameWnd::OnTimer(nIDEvent);
	*/
}


void CMainFrame::OnSerial() 
{
	if(theapp->serPresent)	{m_pserial->ShowWindow(SW_SHOW); m_pserial->UpdateData(false);}
}

LONG CMainFrame::Trigger(UINT, LONG)//manual button
{
	if(theapp->enabPicsToShow || !theapp->camsPresent)	
	{
		//don't forget to increment the counter for pictoShow
		m_pxaxis->LoadImagePic(pictoShow);
		
		if(PicsOpen==false)
		{
			if(Freeze==false)	{m_pxaxis->InvalidateRect(NULL,false);}
			else
			{
				if(FreezeReset) {m_pxaxis->InvalidateRect(NULL,false);}
				
				if(m_pxaxis->BadCap || m_pxaxis->BadCap2 ) 
				{ m_pxaxis->InvalidateRect(NULL,false); FreezeReset=false;}
				else{ m_pview->UpdateDisplay2(); }
			}
		}

	}
	else
	{
		if(theapp->serPresent)
		{
			m_pserial->SendChar(0,20);
		}
		else
		{
			int numcams = theapp->cam5Enable ? 5 : 4;
			for (int i = 0; i < numcams; i++)
				m_pcamera->cam_e[i]->FireSoftwareTrigger();
		}
	}

	if(m_pxaxis->LearnDone)	{theapp->DidTriggerOnce=true;}
	
	return true;
}

LONG CMainFrame::NewJob(UINT, LONG)
{
	CountEnable=false;

	return true;
}

LONG CMainFrame::SetLightLevel(UINT level, LONG level2)
{
	int Value=level; int Function=0; int CntrBits=0;

	if(level2==1)
	{
		Function=22;
		Function=Function *10;
		CntrBits=0;
		
		if(theapp->serPresent)	{m_pserial->SendChar(Value,Function);}
	}
	else
	{
		Function=22;
		Function=Function *10;
		CntrBits=0;
	
		if(theapp->serPresent)	{m_pserial->SendChar(Value,Function);}
	}

	return true;
}

LONG CMainFrame::ReLoad(UINT mp, LONG)
{
	
 
	m_pxaxis->OnLoad3(CurrentJobNum);

	if(theapp->cam5Enable)
	{
		m_pxaxis->OnLoadCap(CurrentJobNum, true);
		m_pxaxis->OnLoadCap(CurrentJobNum, false);
	}
	m_pxaxis->wehaveTempX=true;

	//For method 6
	if(theapp->jobinfo[CurrentJobNum].labelIntegrityUseMethod6)
	{
		m_pxaxis->OnLoadPatternX_method6(CurrentJobNum, 1);
		m_pxaxis->OnLoadPatternX_method6(CurrentJobNum, 2);
		m_pxaxis->OnLoadPatternX_method6(CurrentJobNum, 3); //consolidating this method with OnLoadPatternX_method6_num3
		//m_pxaxis->OnLoadPatternX_method6_num3(CurrentJobNum, 3);
	}
	else if(theapp->jobinfo[CurrentJobNum].labelIntegrityUseMethod2)
	{
			m_pxaxis->OnLoadPatternX_method6(CurrentJobNum, 1);
	}
	else if(theapp->jobinfo[CurrentJobNum].labelIntegrityUseMethod1)
	{
			m_pxaxis->OnLoad2(CurrentJobNum);
	}
	else{
		m_pxaxis->OnLoadPatternX(CurrentJobNum, 1); //Left Pattern
	}

	//m_pxaxis->OnLoadPatternX(CurrentJobNum, 2); //Middle Pattern
	//m_pxaxis->OnLoadPatternX(CurrentJobNum, 3); //Right Pattern

	JobNoGood = false; //we dont need to rely in this loading images

	if(!JobNoGood)
	{
		reloading=true;
		ResetTotals2();
		m_pview->m_status="Job "+m_pview->m_currentjob+ " has been Reloaded";	
		WasSetup=true;
		InspOpen=false;
		CountEnable=true;
		m_pmod->UpdateDisplay();
	
		if(theapp->showMot)
		{MoveHead movehead; movehead.DoModal();}
		
		PhotoEyeEnable(true);
		reloading=false;
	}

	JobNoGood=false;
	
	return true;
}

void CMainFrame::ResetTotals()
{
	theapp->Total=0;
	Rejects=0;
	
	m_pxaxis->e1Cnt=0;
	m_pxaxis->e2Cnt=0;
	m_pxaxis->e3Cnt=0;
	m_pxaxis->e4Cnt=0;
	m_pxaxis->e5Cnt=0;
	m_pxaxis->e6Cnt=0;
	m_pxaxis->e7Cnt=0;
	m_pxaxis->e8Cnt=0;
	m_pxaxis->e9Cnt=0;
	m_pxaxis->e10Cnt=0;
	m_pxaxis->e11Cnt=0;
	m_pxaxis->e12Cnt=0;

	m_pxaxis->E1Cnt=0;
	m_pxaxis->E2Cnt=0;
	m_pxaxis->E3Cnt=0;
	m_pxaxis->E4Cnt=0;
	m_pxaxis->E5Cnt=0;
	m_pxaxis->E6Cnt=0;
	m_pxaxis->E7Cnt=0;
	m_pxaxis->E8Cnt=0;
	m_pxaxis->E9Cnt=0;
	m_pxaxis->E10Cnt=0;
	m_pxaxis->E11Cnt=0;
	m_pxaxis->E12Cnt=0;

	m_pxaxis->Result1r=0;
	m_pxaxis->Result1l=0;
	m_pxaxis->Result2=0;
	m_pxaxis->Result3=0;
	m_pxaxis->Result4=0;
	m_pxaxis->Result5=0;
	m_pxaxis->Result6=0;
	m_pxaxis->Result7=0;
	m_pxaxis->Result8=0;
	m_pxaxis->Result9=0;
	m_pxaxis->Result10=0;
	m_pxaxis->Result11=0;
	m_pxaxis->Result12=0;

	theapp->outh0s=0;
	theapp->outh1s=0;
	theapp->outh2s=0;
	theapp->outh3s=0;
	theapp->outh4s=0;
	theapp->outh5s=0;
	theapp->outh6s=0;
	theapp->outh1ms=0;
	theapp->outh2ms=0;
	theapp->outh3ms=0;
	theapp->outh4ms=0;
	theapp->outh5ms=0;
	theapp->outh6ms=0;
	theapp->outh7s=0;
	theapp->outh7ms=0;

	countC0 = countC1 = countC2 = countC3 = countC4 = countC5 = 1;
	notFinished = 0;
	sameImg = 0;

	for(int i=0; i<190; i++)
	{
//		m_pxaxis->cameraDone[1][i] = true;
//		m_pxaxis->cameraDone[2][i] = true;
//		m_pxaxis->cameraDone[3][i] = true;
//		m_pxaxis->cameraDone[4][i] = true;

		m_pxaxis->cameraDone[1][i] = false;
		m_pxaxis->cameraDone[2][i] = false;
		m_pxaxis->cameraDone[3][i] = false;
		m_pxaxis->cameraDone[4][i] = false;
		m_pxaxis->cameraDone[5][i] = false;
	}

	/////////////////

	//To reset counters in the PLC
	int Value=0; 
	int Function=0; 
	int CntrBits=0;

	Function=590;
	CntrBits=0;
	
	if(theapp->serPresent)	{m_pserial->SendChar(Value,Function);}

	/////////////////

	EjectRate=0;
	BottleRate=0;
	OldBottleCount=0;
	m_pxaxis->StartLockOut=true;
	m_pxaxis->TimesThru=0;
	m_pview->UpdateDisplay();
}

void CMainFrame::ResetTotals2()
{
	theapp->Totalx=0;
	oldTotal=0;
	Rejectsx=0;

	theapp->out0=0;
	theapp->out1=0;
	theapp->out2=0;
	theapp->out3=0;
	theapp->out4=0;
	theapp->out5=0;
	theapp->out6=0;
	theapp->out7=0;
	theapp->out8=0;
	theapp->out9=0;
	theapp->out10=0;
	theapp->out11=0;
	theapp->out12=0;
	theapp->out13=0;
	theapp->out14=0;
	theapp->out15=0;
	theapp->out16=0;
	theapp->out17=0;
	theapp->out18=0;
	theapp->out19=0;

	theapp->out0=0;
	theapp->out1m=0;
	theapp->out2m=0;
	theapp->out3m=0;
	theapp->out4m=0;
	theapp->out5m=0;
	theapp->out6m=0;
	theapp->out7m=0;
	theapp->out8m=0;
	theapp->out9m=0;
	theapp->out10m=0;
	theapp->out11m=0;
	theapp->out12m=0;
	theapp->out13m=0;
	theapp->out14m=0;
	theapp->out15m=0;
	theapp->out16m=0;
	theapp->out17m=0;
	theapp->out18m=0;
	theapp->out19m=0;

	theapp->outh0=0;
	theapp->outh1=0;
	theapp->outh2=0;
	theapp->outh3=0;
	theapp->outh4=0;
	theapp->outh5=0;
	theapp->outh6=0;
	theapp->outh1m=0;
	theapp->outh2m=0;
	theapp->outh3m=0;
	theapp->outh4m=0;
	theapp->outh5m=0;
	theapp->outh6m=0;
	theapp->outh7=0;
	theapp->outh7m=0;

	theapp->outh0s=0;
	theapp->outh1s=0;
	theapp->outh2s=0;
	theapp->outh3s=0;
	theapp->outh4s=0;
	theapp->outh5s=0;
	theapp->outh6s=0;
	theapp->outh1ms=0;
	theapp->outh2ms=0;
	theapp->outh3ms=0;
	theapp->outh4ms=0;
	theapp->outh5ms=0;
	theapp->outh6ms=0;
	theapp->outh7s=0;
	theapp->outh7ms=0;

	m_pxaxis->E1Cntx=0;
	m_pxaxis->E2Cntx=0;
	m_pxaxis->E3Cntx=0;
	m_pxaxis->E4Cntx=0;
	m_pxaxis->E5Cntx=0;
	m_pxaxis->E6Cntx=0;
	m_pxaxis->E7Cntx=0;
	m_pxaxis->E8Cntx=0;
	m_pxaxis->E9Cntx=0;
	m_pxaxis->E10Cntx=0;
	m_pxaxis->E11Cntx=0;
	
	m_pview->UpdateDisplay();
}


LONG CMainFrame::Inspect(UINT, LONG)//from photoeye
{
	TriggerCount+=1;
	
	if(TriggerCount>=32764)	{TriggerCount=0;}

	///////////////////////////////////
	if(gotPicC1 != true || gotPicC2 != true  || gotPicC3 != true || gotPicC4 != true || gotPicC5 != true)
	{
		CString str = "";
		
		//One or more cameras were not received at the time of inspecting the product
		if(!gotPicC1)	{str += "C1";}
		if(!gotPicC2)	{str += "C2";}
		if(!gotPicC3)	{str += "C3";}
		if(!gotPicC4)	{str += "C4";}
		if(!gotPicC5)	{str += "C5";}

		//m_pview->m_status=CString(str);
	}

	//set everything to false again ready for the next picture
	gotPicC1 = false;
	gotPicC2 = false;
	gotPicC3 = false;
	gotPicC4 = false;
	gotPicC5 = false;

	///////////////////////////////////

	if(!Stop)
	{
		if(WasSetup )//&& InspOpen==false
		{
			theapp->Total+=1;
			theapp->picNum+=1;
			
			theapp->Totalx+=1;
			theapp->Total24+=1;

			SetTimer(1,10000,NULL);
			KillTimer(30);
			timesThru=0;

			////two minutes  120000  one minute

			if(theapp->rejectAllKrones)	
			{
				m_pxaxis->BadCap=true;
			}

			if(m_pxaxis->BadCap || m_pxaxis->BadCap2)
			{
				if(OnLine)
				{
					//KillTimer(28);//kill heartbeat
					//SetTimer(29,40,NULL); //restart heartbeat after 40 millisec

					Rejects+=1;
					Rejectsx+=1;
					theapp->Rejects24+=1;

					if(theapp->Total-savedtotal==1) {conreject+=1;}else{conreject=0;}

					savedtotal=theapp->Total;
					
					if(m_pxaxis->BadCap2)
					{
						if( (conreject>=ConRejectLimit && ConRejectLimit>0) || everyOther)
						{
							everyOther=false;
							conreject=0;
							m_pview->m_status="Consecutive or Every Other Reject";
							
							if(theapp->serPresent)	{m_pserial->SendChar(0,620);} //con reject and reject 
						}
						else
						{
							if(theapp->serPresent)	{m_pserial->SendChar(0,420);}
						}// just reject
					}
					else//BadCap2
					{
						if(theapp->capEjectorInstalled)
						{
							if(theapp->serPresent){m_pserial->SendChar(0,630);}
						}// reject cap rejector
						else
						{
							if(theapp->serPresent){m_pserial->SendChar(0,420);}
						}// just reject
					}
				}
			}
			else
			{
				if(theapp->serPresent){m_pserial->SendChar(0,790);}
			}// just reject

			//###### Adding the call to update the Sleever array 

			//PopulateArray();

			//######

			if(OnLine && theapp->SavedConRejects2 >=1)
			{
				if(toggle)
				{
					toggle=false;
					if(m_pxaxis->BadCap || m_pxaxis->BadCap2)	{got1+=1;}	else {got1=0;}
					if(!m_pxaxis->BadCap && !m_pxaxis->BadCap2) {got2+=1;}	else {got2=0;}
				}
				else
				{
					toggle=true;

					if(m_pxaxis->BadCap || m_pxaxis->BadCap2)	{got3+=1;}	else {got3=0;}
					if(!m_pxaxis->BadCap && !m_pxaxis->BadCap2) {got4+=1;}	else {got4=0;}
				}

				if( (got2>=theapp->SavedConRejects2 && got3>=theapp->SavedConRejects2) || (got1>=theapp->SavedConRejects2 && got4>=theapp->SavedConRejects2) )
				{
					m_pview->m_status="Every Other Reject";
					everyOther=true; 
				}
			}
			
			m_pview2->UpdateDisplay();
		}

		else
		{
			if(!InspOpen)
			{
				m_pview->m_status="You must teach job first";
				m_pview->UpdateData(false);
			}
		}

		if(theapp->Total!=0) {EjectRate=(float(Rejects)/float(theapp->Total))*100;}
	}

	if(InspOpen)
	{
		if(WaitNext)
		{
			gotNext+=1;
			
			if(gotNext==2)
			{
				gotNext=0;
				WaitNext=false; 
				m_pinsp->InvalidateRect(NULL,false);
				PhotoEyeEnable(false);
			}
		}

		int stoppoint=true;
	}

	if(IOReject)
	{
		m_pview->m_status="External Reject Activated";
		m_pview->UpdateDisplay();
		IORejects+=1;

		if(IORejects>=IORejectLimit)
		{IORejects=0; IODone=true; IOReject=false;}
	}
	else {IORejects=0;}
	
	if(theapp->capOpen) {m_pcap->UpdateDisplay();}

	//if(theapp->serPresent)	{	m_pserial->ShowResults(true);	}

	m_pview->UpdateDisplay2();
	return true;
}

void CMainFrame::KillContinuousTrigger()
{
	m_pview->KillTimer(1);
}

void CMainFrame::OnClose() 
{
	SaveJob(CurrentJobNum);//	_crtBreakAlloc = 733;

	if(SJob)
	{	
		int result=AfxMessageBox("Do you want to close the application?", MB_YESNO);

		//int result=AfxMessageBox("Do you want to close the application?", MB_YESNO | MB_ICONQUESTION);
		//int result=AfxMessageBox("Do you want to close the application?", MB_OK);
		//int result=AfxMessageBox("Do you want to close the application?");//NO
		
		if(IDYES!=result) return;
		theapp->_CheckGPIOReg=false;
		m_pcamera->m_bRestart = false;
		m_pcamera->m_bContinueGrabThread = false;		//Stop grab threads.

		if(!theapp->commProblem)
		{
			if(theapp->serPresent)
			{
				m_pserial->SendChar(0,410);
				m_pserial->SendChar(0,310);
			}
		}

		Sleep(1);

		KillTimer(24);
////////////////

		m_pview->KillTimer(1); // kill any continuous trigger

		if(!theapp->commProblem) 
		{
			if(theapp->serPresent)
			{
			//	m_pserial->SendChar(0,490);
			//	PhotoEyeEnable(false);
				m_pserial->SendChar(0,310);
			}
		}

/////////////////////
	
		if(theapp->camsPresent)	{m_pcamera->StopCams();}

		Stop=true;

		Sleep(250);

		if(theapp->serPresent)
		{
			m_pserial->ShutDown();
			m_pserial->DestroyWindow(); 
			delete m_pserial;
		}
		
		theapp->serPresent=false;

		if(theapp->camsPresent)
		{
			m_pcamera->DestroyWindow(); 
			delete m_pcamera;
		}

		m_pxaxis->KillTimer(1);

		/////////////////
	
		free(m_pxaxis->im_trouble);
		free(m_pxaxis->im_patl2);
		free(m_pxaxis->im_color2);
		free(m_pxaxis->im_patr2x);
		free(m_pxaxis->im_patr2);
		free(m_pxaxis->im_pat);
		free(m_pxaxis->im_pat3);
		free(m_pxaxis->im_thresh);

		/////////////////////////

		free(m_pxaxis->im_reduce1);
		free(m_pxaxis->im_reduce2);
		free(m_pxaxis->im_reduce3);
		free(m_pxaxis->im_reduce4);
		free(m_pxaxis->im_reduce5);

		free(m_pxaxis->im_cambw1);
		free(m_pxaxis->im_cambw2);
		free(m_pxaxis->im_cambw3);
		free(m_pxaxis->im_cambw4);
		free(m_pxaxis->im_cambw5);
		free(m_pxaxis->im_cam5Bbw);
		free(m_pxaxis->im_cam5Gbw);
		free(m_pxaxis->im_cam5Rbw);

		free(m_pxaxis->im_matchl);
		free(m_pxaxis->im_matchr);
		free(m_pxaxis->im_match2);
		free(m_pxaxis->im_mat);
		free(m_pxaxis->im_mat2);
		free(m_pxaxis->im_match3);
		free(m_pxaxis->im_mat3);
		free(m_pxaxis->imgBuf0);
		free(m_pxaxis->im_patshow);

		free(m_pxaxis->imgBuf0bw);
		free(m_pxaxis->imgBuf5bw);

		free(m_pxaxis->imgStitchedLigh);
		free(m_pxaxis->imgStitchedDark);
		free(m_pxaxis->imgStitchedEdg);

		free(m_pxaxis->imgStitchedFilLigh);
		free(m_pxaxis->imgStitchedFilDark);
		free(m_pxaxis->imgStitchedFilEdg);
		free(m_pxaxis->imgStitchedFilFinal);
		free(m_pxaxis->imgStitchedBW);

		//free(m_pxaxis->m_imageProcessed.pData);
		//free(m_pxaxis->m_imageProcessed2.pData);
		//free(m_pxaxis->m_imageProcessed3.pData);
		//free(m_pxaxis->m_imageProcessed4.pData);
		//free(m_pxaxis->m_imageProcessed5.pData);

		//		free(m_pxaxis->imgBuf3RGB_cam1);
		//		free(m_pxaxis->imgBuf3RGB_cam2);
		//		free(m_pxaxis->imgBuf3RGB_cam3);
		//		free(m_pxaxis->imgBuf3RGB_cam4);
		//		free(m_pxaxis->imgBuf3RGB_cam5);

		free(m_pxaxis->img1SatDiv3);
		free(m_pxaxis->img2SatDiv3);
		free(m_pxaxis->img3SatDiv3);
		free(m_pxaxis->img4SatDiv3);

		free(m_pxaxis->img1Sa2Div3);
		free(m_pxaxis->img2Sa2Div3);
		free(m_pxaxis->img3Sa2Div3);
		free(m_pxaxis->img4Sa2Div3);

		free(m_pxaxis->img1SatDiv6);
		free(m_pxaxis->img2SatDiv6);
		free(m_pxaxis->img3SatDiv6);
		free(m_pxaxis->img4SatDiv6);

		free(m_pxaxis->img1MagDiv3);
		free(m_pxaxis->img2MagDiv3);
		free(m_pxaxis->img3MagDiv3);
		free(m_pxaxis->img4MagDiv3);

		free(m_pxaxis->img1YelDiv3);
		free(m_pxaxis->img2YelDiv3);
		free(m_pxaxis->img3YelDiv3);
		free(m_pxaxis->img4YelDiv3);

		free(m_pxaxis->img1CyaDiv3);
		free(m_pxaxis->img2CyaDiv3);
		free(m_pxaxis->img3CyaDiv3);
		free(m_pxaxis->img4CyaDiv3);

		free(m_pxaxis->im_cam1Bbw);
		free(m_pxaxis->im_cam2Bbw);
		free(m_pxaxis->im_cam3Bbw);
		free(m_pxaxis->im_cam4Bbw);

		free(m_pxaxis->im_cam1Gbw);
		free(m_pxaxis->im_cam2Gbw);		
		free(m_pxaxis->im_cam3Gbw);
		free(m_pxaxis->im_cam4Gbw);		
		
		free(m_pxaxis->im_cam1Rbw);
		free(m_pxaxis->im_cam2Rbw);
		free(m_pxaxis->im_cam3Rbw);
		free(m_pxaxis->im_cam4Rbw);
			
		free(m_pxaxis->im_cam1bwDiv6);
		free(m_pxaxis->im_cam2bwDiv6);
		free(m_pxaxis->im_cam3bwDiv6);
		free(m_pxaxis->im_cam4bwDiv6);

		free(m_pxaxis->img1MagDiv6);
		free(m_pxaxis->img2MagDiv6);
		free(m_pxaxis->img3MagDiv6);
		free(m_pxaxis->img4MagDiv6);

		free(m_pxaxis->img1YelDiv6);
		free(m_pxaxis->img2YelDiv6);
		free(m_pxaxis->img3YelDiv6);
		free(m_pxaxis->img4YelDiv6);
		
		free(m_pxaxis->img1CyaDiv6);
		free(m_pxaxis->img2CyaDiv6);
		free(m_pxaxis->img3CyaDiv6);
		free(m_pxaxis->img4CyaDiv6);

		free(m_pxaxis->im_cam1RbwDiv6);
		free(m_pxaxis->im_cam2RbwDiv6);
		free(m_pxaxis->im_cam3RbwDiv6);
		free(m_pxaxis->im_cam4RbwDiv6);	
		
		free(m_pxaxis->im_cam1GbwDiv6);
		free(m_pxaxis->im_cam2GbwDiv6);
		free(m_pxaxis->im_cam3GbwDiv6);
		free(m_pxaxis->im_cam4GbwDiv6);

		free(m_pxaxis->im_cam1BbwDiv6);
		free(m_pxaxis->im_cam2BbwDiv6);
		free(m_pxaxis->im_cam3BbwDiv6);
		free(m_pxaxis->im_cam4BbwDiv6);



/*		free(m_pxaxis->img_cam1Ligh);
		free(m_pxaxis->img_cam2Ligh);
		free(m_pxaxis->img_cam3Ligh);
		free(m_pxaxis->img_cam4Ligh);

		free(m_pxaxis->img_cam1Dark);
		free(m_pxaxis->img_cam2Dark);
		free(m_pxaxis->img_cam3Dark);
		free(m_pxaxis->img_cam4Dark);

		free(m_pxaxis->img_cam1Edge);
		free(m_pxaxis->img_cam2Edge);
		free(m_pxaxis->img_cam3Edge);
		free(m_pxaxis->img_cam4Edge);


		free(m_pxaxis->im1LighSeq);
		free(m_pxaxis->im2LighSeq);
		free(m_pxaxis->im3LighSeq);
		free(m_pxaxis->im4LighSeq);

		free(m_pxaxis->im1DarkSeq);
		free(m_pxaxis->im2DarkSeq);
		free(m_pxaxis->im3DarkSeq);
		free(m_pxaxis->im4DarkSeq);

		free(m_pxaxis->im1EdgeSeq);
		free(m_pxaxis->im2EdgeSeq);
		free(m_pxaxis->im3EdgeSeq);
		free(m_pxaxis->im4EdgeSeq);

		free(m_pxaxis->img1ForNoLabelDiv3);
		free(m_pxaxis->img2ForNoLabelDiv3);
		free(m_pxaxis->img3ForNoLabelDiv3);
		free(m_pxaxis->img4ForNoLabelDiv3);

		free(m_pxaxis->img1MiddBottle);
		free(m_pxaxis->img2MiddBottle);
		free(m_pxaxis->img3MiddBottle);
		free(m_pxaxis->img4MiddBottle);

  /////////////////////


		free(m_pxaxis->img1ForSpliceDiv3);
		free(m_pxaxis->img2ForSpliceDiv3);
		free(m_pxaxis->img3ForSpliceDiv3);
		free(m_pxaxis->img4ForSpliceDiv3);

		free(m_pxaxis->img1ForShiftedDiv3);
		free(m_pxaxis->img2ForShiftedDiv3);
		free(m_pxaxis->img3ForShiftedDiv3);
		free(m_pxaxis->img4ForShiftedDiv3);

		free(m_pxaxis->img1ForShiftedDiv6);
		free(m_pxaxis->img2ForShiftedDiv6);
		free(m_pxaxis->img3ForShiftedDiv6);
		free(m_pxaxis->img4ForShiftedDiv6);

		free(m_pxaxis->img1ForPattMatchDiv3);
		free(m_pxaxis->img2ForPattMatchDiv3);
		free(m_pxaxis->img3ForPattMatchDiv3);
		free(m_pxaxis->img4ForPattMatchDiv3);

		free(m_pxaxis->img1ForPattMatchDiv6);
		free(m_pxaxis->img2ForPattMatchDiv6);
		free(m_pxaxis->img3ForPattMatchDiv6);
		free(m_pxaxis->img4ForPattMatchDiv6);

*/

		free(m_pxaxis->img1Edg);
		free(m_pxaxis->img2Edg);
		free(m_pxaxis->img3Edg);
		free(m_pxaxis->img4Edg);



		free(m_pxaxis->im_cam1EdgH);
		free(m_pxaxis->im_cam2EdgH);
		free(m_pxaxis->im_cam3EdgH);
		free(m_pxaxis->im_cam4EdgH);

		free(m_pxaxis->im_cam1EdgV);
		free(m_pxaxis->im_cam2EdgV);
		free(m_pxaxis->im_cam3EdgV);
		free(m_pxaxis->im_cam4EdgV);

		free(m_pxaxis->im_cam1Bin);
		free(m_pxaxis->im_cam2Bin);
		free(m_pxaxis->im_cam3Bin);
		free(m_pxaxis->im_cam4Bin);

		free(m_pxaxis->byteBin1Edges);
		free(m_pxaxis->byteBin2Edges);
		free(m_pxaxis->byteBin3Edges);
		free(m_pxaxis->byteBin4Edges);

		free(m_pxaxis->byteBinMet5C1);
		free(m_pxaxis->byteBinMet5C2);
		free(m_pxaxis->byteBinMet5C3);
		free(m_pxaxis->byteBinMet5C4);

		for(int i=1; i<=3; i++)
		{
			free(m_pxaxis->bytePattBy6[i]);
			free(m_pxaxis->bytePattBy3[i]);
		}

		free(m_pxaxis->bytePattBy6_method6_1);
		free(m_pxaxis->bytePattBy3_method6_1);
		
		free(m_pxaxis->bytePattBy6_method6);
		free(m_pxaxis->bytePattBy3_method6);
		
		free(m_pxaxis->bytePattBy6_method6_num3);
		free(m_pxaxis->bytePattBy3_method6_num3);
		
//		free(m_pxaxis->widthby3);
//		free(m_pxaxis->heighby3);

//		free(m_pxaxis->widthby6);
//		free(m_pxaxis->heighby6);

	
	/*	for(i=1; i<=4; i++)
		{
			m_pxaxis->imageYel[i].Deallocate();
			m_pxaxis->imageMag[i].Deallocate();
			m_pxaxis->imageCya[i].Deallocate();
			m_pxaxis->imageBW[i].Deallocate();
			m_pxaxis->imageBWB[i].Deallocate();
			m_pxaxis->imageBWG[i].Deallocate();
			m_pxaxis->imageBWR[i].Deallocate();
			m_pxaxis->imageSat[i].Deallocate();
			m_pxaxis->imageSobH[i].Deallocate();
			m_pxaxis->imageSobV[i].Deallocate();
			m_pxaxis->imageBin[i].Deallocate();
			m_pxaxis->imageBinEdges[i].Deallocate();
		}

*/
	
/////////////////

 //       _CrtSetDbgFlag (_CRTDBG_ALLOC_MEM_DF |_CRTDBG_LEAK_CHECK_DF);
   //     _CrtSetReportMode ( _CRT_ERROR, _CRTDBG_MODE_DEBUG);
	//	_CrtDumpMemoryLeaks();

		CMDIFrameWnd::OnClose();
	}
	else
	{
		AfxMessageBox("You need to use the Password!!!!");
		Security sec;
		sec.DoModal();

		if(SJob)
		{	
			m_pcamera->m_bRestart = false;
			m_pcamera->m_bContinueGrabThread = false;		//Stop grab threads.

			KillTimer(24);

			if(!theapp->commProblem)
			{
				if(theapp->serPresent)	
				{m_pserial->SendChar(0,410);}
			}
			
			Sleep(1);

			if(!theapp->commProblem) 
			{
				if(theapp->serPresent)
				{
					m_pserial->SendChar(0,490);
					PhotoEyeEnable(false);
				}
			}

			if(theapp->camsPresent)
			{
				m_pcamera->StopCams();
			}
				
			Stop=true;

			_CrtSetDbgFlag (_CRTDBG_ALLOC_MEM_DF |_CRTDBG_LEAK_CHECK_DF);
			_CrtSetReportMode ( _CRT_ERROR, _CRTDBG_MODE_DEBUG);
			_CrtDumpMemoryLeaks();

			CMDIFrameWnd::OnClose();
		}
	}


}

void CMainFrame::OnSetup2() 
{
	if(SSetup)		{Insp insp; insp.DoModal();}
	else				
	{
		Security sec; sec.DoModal();
	
		if(SSetup)	{Insp insp; insp.DoModal();}
	}
}

void CMainFrame::GotFault()
{
	Faulted=true;

	if(theapp->serPresent)	{m_pserial->SendChar(0,410);}//red light
	
	m_pview->UpdateDisplay();
}

void CMainFrame::OnPics() 
{
	if(SModify)		{Pics pics; pics.DoModal();}
	else
	{
		Security sec; sec.DoModal();
		if(SModify)	{Pics pics; pics.DoModal();}
	}
}

void CMainFrame::OnEject() 
{
	if(SModify)		{Blowoff blowoff; blowoff.DoModal();}
	else
	{
		Security sec; sec.DoModal();
		
		if(SModify)	{Blowoff blowoff; blowoff.DoModal();}
	}
}

void CMainFrame::OnCapwidth() 
{
	//CWidth width;
	//width.DoModal();	
}

void CMainFrame::OnSecurity() {Security sec; sec.DoModal();}

void CMainFrame::OnLight() 
{
	if(SModify)		{Light light; light.DoModal();}
	else
	{
		Security sec;	sec.DoModal();

		if(SModify)	{Light light; light.DoModal();}
	}
}

void CMainFrame::OnMotor() 
{
	if(SModify)		{Motor motor; motor.DoModal();}
	else
	{
		Security sec; sec.DoModal();
		if(SModify)	{Motor motor; motor.DoModal();}
	}
}


void CMainFrame::OnJobchange() 
{
	if(SJob)		{Job job; job.DoModal();}
	else
	{
		Security sec; sec.DoModal();
		if(SJob)	{Job job; job.DoModal();}
	}
}

void CMainFrame::UpdatePLC()
{
	///Additions
	SetTimer(35,500,NULL);	//kick off duration
	SetTimer(36,700,NULL);	//kick off distance
	SetTimer(37,800,NULL);	//photoeye Direction
	SetTimer(38,1000,NULL);	//snap delay
	SetTimer(39,1150,NULL);	//cap width
	SetTimer(40,1300,NULL);	//Down Bottle
	SetTimer(41,1450,NULL);	//Down Bottle Size
	SetTimer(43,1600,NULL);	//Side lights disable or enable
}

void CMainFrame::CheckStatus()
{
	if(ToggleCheckStatus) 
	{
		if(theapp->serPresent)	{Condition1=m_pserial->StatusByte;}
		
		OldTriggerCount=TriggerCount;
		ToggleCheckStatus=false;
	}
	else
	{
		if(theapp->serPresent)	{Condition2=m_pserial->StatusByte;}

		ToggleCheckStatus=true;
		NewTriggerCount=TriggerCount;

		if(Condition1 != Condition2)
		{
			if(OldTriggerCount==NewTriggerCount) {LockUp+=1;}
		}
	}
}

void CMainFrame::OnPhotoeye() 
{
	
	if(SSetup)	{Photoeye photo; photo.DoModal();}
	else
	{
		Security sec; sec.DoModal();
		if(SSetup){Photoeye photo; photo.DoModal();}
	}
}


void CMainFrame::PhotoEyeEnable(bool enabled)
{
	if(theapp->serPresent)	{m_pserial->SendChar(0, enabled ? 300 : 310);}
}

void CMainFrame::OnDigio() 
{
	if(SSetup)	{DigIO digio; digio.DoModal();}
	else				{Security sec;	sec.DoModal();}
}


void CMainFrame::OnData() 
{
	Data data; data.DoModal();
}


void CMainFrame::GetJob(int JobNum)
{ 
	theapp->ReadRegistery(JobNum); 
}

void CMainFrame::SaveJob(int jobNum)
{
	theapp=(CBottleApp*)AfxGetApp();

	// Write each record in a separate Registry folder

	char inspnum[10];
	char folderName[255];
	int curInsp=1;
	
	sprintf(folderName, "JobArray\\Job%03d", jobNum);
	theapp->WriteProfileString(folderName, "jobname", theapp->jobinfo[jobNum].jobname);

//  ----------------------------------- Label Mating -----------------------------------
	theapp->WriteProfileInt(folderName,	"showLabelMating",theapp->jobinfo[jobNum].showLabelMating);

	theapp->WriteProfileInt(folderName, "labelMatingTeachColorLeftBound", theapp->jobinfo[jobNum].labelMatingTeachColorLeftBound);
	theapp->WriteProfileInt(folderName, "labelMatingTeachColorRightBound", theapp->jobinfo[jobNum].labelMatingTeachColorRightBound);
	theapp->WriteProfileInt(folderName, "labelMatingTeachColorTopBound", theapp->jobinfo[jobNum].labelMatingTeachColorTopBound);
	theapp->WriteProfileInt(folderName, "labelMatingTeachColorBottomBound", theapp->jobinfo[jobNum].labelMatingTeachColorBottomBound);

	theapp->WriteProfileInt(folderName, "labelMatingLeftBound", theapp->jobinfo[jobNum].labelMatingLeftBound);
	theapp->WriteProfileInt(folderName, "labelMatingRightBound", theapp->jobinfo[jobNum].labelMatingRightBound);
	theapp->WriteProfileInt(folderName, "labelMatingTopBound", theapp->jobinfo[jobNum].labelMatingTopBound);
	theapp->WriteProfileInt(folderName, "labelMatingBottomBound", theapp->jobinfo[jobNum].labelMatingBottomBound);

	theapp->WriteProfileInt(folderName, "labelMatingTaughtR", theapp->jobinfo[jobNum].labelMatingTaughtR);
	theapp->WriteProfileInt(folderName, "labelMatingTaughtG", theapp->jobinfo[jobNum].labelMatingTaughtG);
	theapp->WriteProfileInt(folderName, "labelMatingTaughtB", theapp->jobinfo[jobNum].labelMatingTaughtB);


	theapp->WriteProfileInt(folderName, "labelMatingTaughtRTolerance", theapp->jobinfo[jobNum].labelMatingTaughtRTolerance);
	theapp->WriteProfileInt(folderName, "labelMatingTaughtGTolerance", theapp->jobinfo[jobNum].labelMatingTaughtGTolerance);
	theapp->WriteProfileInt(folderName, "labelMatingTaughtBTolerance", theapp->jobinfo[jobNum].labelMatingTaughtBTolerance);

	//------ Waist measurement cam1 -------------------------
	//Selection box
//	theapp->WriteProfileInt(folderName,"cam1WaistSelectionLeftBound", theapp->jobinfo[jobNum].cam1WaistSelectionLeftBound);
//	theapp->WriteProfileInt(folderName,"cam1WaistSelectionRightBound", theapp->jobinfo[jobNum].cam1WaistSelectionRightBound);
//	theapp->WriteProfileInt(folderName,"cam1WaistSelectionTopBound", theapp->jobinfo[jobNum].cam1WaistSelectionTopBound);
//	theapp->WriteProfileInt(folderName,"cam1WaistSelectionBottomBound", theapp->jobinfo[jobNum].cam1WaistSelectionBottomBound);


	theapp->WriteProfileInt(folderName, "cam1WaistMeasurementUpperLeftBound", theapp->jobinfo[jobNum].cam1WaistMeasurementUpperLeftBound);
	theapp->WriteProfileInt(folderName, "cam1WaistMeasurementUpperRightBound", theapp->jobinfo[jobNum].cam1WaistMeasurementUpperRightBound);	
	theapp->WriteProfileInt(folderName, "cam1WaistMeasurementUpperTopBound", theapp->jobinfo[jobNum].cam1WaistMeasurementUpperTopBound);
	theapp->WriteProfileInt(folderName, "cam1WaistMeasurementUpperBottomBound", theapp->jobinfo[jobNum].cam1WaistMeasurementUpperBottomBound);

	theapp->WriteProfileInt(folderName, "cam1WaistMeasurementLeftBound", theapp->jobinfo[jobNum].cam1WaistMeasurementLeftBound);
	theapp->WriteProfileInt(folderName, "cam1WaistMeasurementRightBound", theapp->jobinfo[jobNum].cam1WaistMeasurementRightBound);	
	theapp->WriteProfileInt(folderName, "cam1WaistMeasurementTopBound", theapp->jobinfo[jobNum].cam1WaistMeasurementTopBound);
	theapp->WriteProfileInt(folderName, "cam1WaistMeasurementBottomBound", theapp->jobinfo[jobNum].cam1WaistMeasurementBottomBound);

	theapp->WriteProfileInt(folderName, "cam1WaistEdgeThreshold", theapp->jobinfo[jobNum].cam1WaistEdgeThreshold);
	theapp->WriteProfileInt(folderName, "cam1WaistVerticalSearchLength", theapp->jobinfo[jobNum].cam1WaistVerticalSearchLength);	
	theapp->WriteProfileInt(folderName, "cam1WaistHorizontalSearchLength", theapp->jobinfo[jobNum].cam1WaistHorizontalSearchLength);

	theapp->WriteProfileInt(folderName, "cam1WaistAvgWindowSize", theapp->jobinfo[jobNum].cam1WaistAvgWindowSize);
	theapp->WriteProfileInt(folderName, "cam1WaistEdgeCoordinateDeviation", theapp->jobinfo[jobNum].cam1WaistEdgeCoordinateDeviation);	
	theapp->WriteProfileInt(folderName, "cam1WaistfillTopToBottom", theapp->jobinfo[jobNum].cam1WaistfillTopToBottom);

	theapp->WriteProfileInt(folderName, "cam1WaistWidthOverlayStartX", theapp->jobinfo[jobNum].cam1WaistWidthOverlayStartX);
	theapp->WriteProfileInt(folderName, "cam1WaistWidthOverlayEndX", theapp->jobinfo[jobNum].cam1WaistWidthOverlayEndX);
	theapp->WriteProfileInt(folderName, "cam1WaistWidthOverlayY", theapp->jobinfo[jobNum].cam1WaistWidthOverlayY);

	//--------------------------------------------------


	//------ Waist measurement cam4 -------------------------
	theapp->WriteProfileInt(folderName, "cam4WaistMeasurementUpperLeftBound", theapp->jobinfo[jobNum].cam4WaistMeasurementUpperLeftBound);
	theapp->WriteProfileInt(folderName, "cam4WaistMeasurementUpperRightBound", theapp->jobinfo[jobNum].cam4WaistMeasurementUpperRightBound);	
	theapp->WriteProfileInt(folderName, "cam4WaistMeasurementUpperTopBound", theapp->jobinfo[jobNum].cam4WaistMeasurementUpperTopBound);
	theapp->WriteProfileInt(folderName, "cam4WaistMeasurementUpperBottomBound", theapp->jobinfo[jobNum].cam4WaistMeasurementUpperBottomBound);

	theapp->WriteProfileInt(folderName, "cam4WaistMeasurementLeftBound", theapp->jobinfo[jobNum].cam4WaistMeasurementLeftBound);
	theapp->WriteProfileInt(folderName, "cam4WaistMeasurementRightBound", theapp->jobinfo[jobNum].cam4WaistMeasurementRightBound);	
	theapp->WriteProfileInt(folderName, "cam4WaistMeasurementTopBound", theapp->jobinfo[jobNum].cam4WaistMeasurementTopBound);
	theapp->WriteProfileInt(folderName, "cam4WaistMeasurementBottomBound", theapp->jobinfo[jobNum].cam4WaistMeasurementBottomBound);

	theapp->WriteProfileInt(folderName, "cam4WaistEdgeThreshold", theapp->jobinfo[jobNum].cam4WaistEdgeThreshold);
	theapp->WriteProfileInt(folderName, "cam4WaistVerticalSearchLength", theapp->jobinfo[jobNum].cam4WaistVerticalSearchLength);	
	theapp->WriteProfileInt(folderName, "cam4WaistHorizontalSearchLength", theapp->jobinfo[jobNum].cam4WaistHorizontalSearchLength);

	theapp->WriteProfileInt(folderName, "cam4WaistAvgWindowSize", theapp->jobinfo[jobNum].cam4WaistAvgWindowSize);
	theapp->WriteProfileInt(folderName, "cam4WaistEdgeCoordinateDeviation", theapp->jobinfo[jobNum].cam4WaistEdgeCoordinateDeviation);	
	theapp->WriteProfileInt(folderName, "cam4WaistfillTopToBottom", theapp->jobinfo[jobNum].cam4WaistfillTopToBottom);

	theapp->WriteProfileInt(folderName, "cam4WaistWidthOverlayStartX", theapp->jobinfo[jobNum].cam4WaistWidthOverlayStartX);
	theapp->WriteProfileInt(folderName, "cam4WaistWidthOverlayEndX", theapp->jobinfo[jobNum].cam4WaistWidthOverlayEndX);
	theapp->WriteProfileInt(folderName, "cam4WaistWidthOverlayY", theapp->jobinfo[jobNum].cam4WaistWidthOverlayY);

	//------ Waist measurement cam2 -------------------------
	theapp->WriteProfileInt(folderName, "cam2WaistMeasurementUpperLeftBound", theapp->jobinfo[jobNum].cam2WaistMeasurementUpperLeftBound);
	theapp->WriteProfileInt(folderName, "cam2WaistMeasurementUpperRightBound", theapp->jobinfo[jobNum].cam2WaistMeasurementUpperRightBound);	
	theapp->WriteProfileInt(folderName, "cam2WaistMeasurementUpperTopBound", theapp->jobinfo[jobNum].cam2WaistMeasurementUpperTopBound);
	theapp->WriteProfileInt(folderName, "cam2WaistMeasurementUpperBottomBound", theapp->jobinfo[jobNum].cam2WaistMeasurementUpperBottomBound);

	theapp->WriteProfileInt(folderName, "cam2WaistMeasurementLeftBound", theapp->jobinfo[jobNum].cam2WaistMeasurementLeftBound);
	theapp->WriteProfileInt(folderName, "cam2WaistMeasurementRightBound", theapp->jobinfo[jobNum].cam2WaistMeasurementRightBound);	
	theapp->WriteProfileInt(folderName, "cam2WaistMeasurementTopBound", theapp->jobinfo[jobNum].cam2WaistMeasurementTopBound);
	theapp->WriteProfileInt(folderName, "cam2WaistMeasurementBottomBound", theapp->jobinfo[jobNum].cam2WaistMeasurementBottomBound);

	theapp->WriteProfileInt(folderName, "cam2WaistEdgeThreshold", theapp->jobinfo[jobNum].cam2WaistEdgeThreshold);
	theapp->WriteProfileInt(folderName, "cam2WaistVerticalSearchLength", theapp->jobinfo[jobNum].cam2WaistVerticalSearchLength);	
	theapp->WriteProfileInt(folderName, "cam2WaistHorizontalSearchLength", theapp->jobinfo[jobNum].cam2WaistHorizontalSearchLength);

	theapp->WriteProfileInt(folderName, "cam2WaistAvgWindowSize", theapp->jobinfo[jobNum].cam2WaistAvgWindowSize);
	theapp->WriteProfileInt(folderName, "cam2WaistEdgeCoordinateDeviation", theapp->jobinfo[jobNum].cam2WaistEdgeCoordinateDeviation);	
	theapp->WriteProfileInt(folderName, "cam2WaistfillTopToBottom", theapp->jobinfo[jobNum].cam2WaistfillTopToBottom);

	theapp->WriteProfileInt(folderName, "cam2WaistWidthOverlayStartX", theapp->jobinfo[jobNum].cam2WaistWidthOverlayStartX);
	theapp->WriteProfileInt(folderName, "cam2WaistWidthOverlayEndX", theapp->jobinfo[jobNum].cam2WaistWidthOverlayEndX);
	theapp->WriteProfileInt(folderName, "cam2WaistWidthOverlayY", theapp->jobinfo[jobNum].cam2WaistWidthOverlayY);

	//------ Waist measurement cam3 -------------------------
	theapp->WriteProfileInt(folderName, "cam3WaistMeasurementUpperLeftBound", theapp->jobinfo[jobNum].cam3WaistMeasurementUpperLeftBound);
	theapp->WriteProfileInt(folderName, "cam3WaistMeasurementUpperRightBound", theapp->jobinfo[jobNum].cam3WaistMeasurementUpperRightBound);	
	theapp->WriteProfileInt(folderName, "cam3WaistMeasurementUpperTopBound", theapp->jobinfo[jobNum].cam3WaistMeasurementUpperTopBound);
	theapp->WriteProfileInt(folderName, "cam3WaistMeasurementUpperBottomBound", theapp->jobinfo[jobNum].cam3WaistMeasurementUpperBottomBound);

	theapp->WriteProfileInt(folderName, "cam3WaistMeasurementLeftBound", theapp->jobinfo[jobNum].cam3WaistMeasurementLeftBound);
	theapp->WriteProfileInt(folderName, "cam3WaistMeasurementRightBound", theapp->jobinfo[jobNum].cam3WaistMeasurementRightBound);	
	theapp->WriteProfileInt(folderName, "cam3WaistMeasurementTopBound", theapp->jobinfo[jobNum].cam3WaistMeasurementTopBound);
	theapp->WriteProfileInt(folderName, "cam3WaistMeasurementBottomBound", theapp->jobinfo[jobNum].cam3WaistMeasurementBottomBound);

	theapp->WriteProfileInt(folderName, "cam3WaistEdgeThreshold", theapp->jobinfo[jobNum].cam3WaistEdgeThreshold);
	theapp->WriteProfileInt(folderName, "cam3WaistVerticalSearchLength", theapp->jobinfo[jobNum].cam3WaistVerticalSearchLength);	
	theapp->WriteProfileInt(folderName, "cam3WaistHorizontalSearchLength", theapp->jobinfo[jobNum].cam3WaistHorizontalSearchLength);

	theapp->WriteProfileInt(folderName, "cam3WaistAvgWindowSize", theapp->jobinfo[jobNum].cam3WaistAvgWindowSize);
	theapp->WriteProfileInt(folderName, "cam3WaistEdgeCoordinateDeviation", theapp->jobinfo[jobNum].cam3WaistEdgeCoordinateDeviation);	
	theapp->WriteProfileInt(folderName, "cam3WaistfillTopToBottom", theapp->jobinfo[jobNum].cam3WaistfillTopToBottom);

	theapp->WriteProfileInt(folderName, "cam3WaistWidthOverlayStartX", theapp->jobinfo[jobNum].cam3WaistWidthOverlayStartX);
	theapp->WriteProfileInt(folderName, "cam3WaistWidthOverlayEndX", theapp->jobinfo[jobNum].cam3WaistWidthOverlayEndX);
	theapp->WriteProfileInt(folderName, "cam3WaistWidthOverlayY", theapp->jobinfo[jobNum].cam3WaistWidthOverlayY);

	//--------------------------------------------------

	theapp->WriteProfileInt(folderName, "usering", theapp->jobinfo[jobNum].useRing);
	
	theapp->WriteProfileInt(folderName, "taught",theapp->jobinfo[jobNum].taught);

	theapp->WriteProfileInt(folderName, "tgYpattern",theapp->jobinfo[jobNum].tgYpattern);

	//Transition label Integrity method
	theapp->WriteProfileInt(folderName,	"labelIntegrityUseMethod1",theapp->jobinfo[jobNum].labelIntegrityUseMethod1);
	theapp->WriteProfileInt(folderName,	"labelIntegrityUseMethod2",theapp->jobinfo[jobNum].labelIntegrityUseMethod2);
	theapp->WriteProfileInt(folderName,	"labelIntegrityUseMethod3",theapp->jobinfo[jobNum].labelIntegrityUseMethod3);
	theapp->WriteProfileInt(folderName,	"labelIntegrityUseMethod4",theapp->jobinfo[jobNum].labelIntegrityUseMethod4);
	theapp->WriteProfileInt(folderName,	"labelIntegrityUseMethod5",theapp->jobinfo[jobNum].labelIntegrityUseMethod5);
	theapp->WriteProfileInt(folderName,	"labelIntegrityUseMethod6",theapp->jobinfo[jobNum].labelIntegrityUseMethod6);

	//Edge Transition
	theapp->WriteProfileInt(folderName,	"SpliceLookFor",theapp->jobinfo[jobNum].SpliceLookFor);

	//Edge Transition
	theapp->WriteProfileInt(folderName,	"EdgeTransition",theapp->jobinfo[jobNum].EdgeTransition);

	//Pattern Selection
	theapp->WriteProfileInt(folderName,	"filterSelPatt",theapp->jobinfo[jobNum].filterSelPatt);

	//Pattern Cap Selection
	theapp->WriteProfileInt(folderName,	"filterSelPattCap",theapp->jobinfo[jobNum].filterSelPattCap);

	

	//Selection - No Label
	theapp->WriteProfileInt(folderName,	"filterSelNolabel",theapp->jobinfo[jobNum].filterSelNolabel);

	//Shifted Selection
	theapp->WriteProfileInt(folderName,	"filterSelShifted",theapp->jobinfo[jobNum].filterSelShifted);

	//Selection - No Label
	theapp->WriteProfileInt(folderName,	"filterSelSplice",theapp->jobinfo[jobNum].filterSelSplice);

	//Boxes for Method5
	theapp->WriteProfileInt(folderName,	"boxYt",	theapp->jobinfo[jobNum].boxYt);
	theapp->WriteProfileInt(folderName,	"boxWt",	theapp->jobinfo[jobNum].boxWt);
	theapp->WriteProfileInt(folderName,	"boxHt",	theapp->jobinfo[jobNum].boxHt);

	theapp->WriteProfileInt(folderName,	"boxYb",	theapp->jobinfo[jobNum].boxYb);
	theapp->WriteProfileInt(folderName,	"boxWb",	theapp->jobinfo[jobNum].boxWb);
	theapp->WriteProfileInt(folderName,	"boxHb",	theapp->jobinfo[jobNum].boxHb);

	//Seam Variables
	theapp->WriteProfileInt(folderName,	"seamW",	theapp->jobinfo[jobNum].seamW);
	theapp->WriteProfileInt(folderName,	"seamH",	theapp->jobinfo[jobNum].seamH);
	theapp->WriteProfileInt(folderName,	"seamOffX",	theapp->jobinfo[jobNum].seamOffX);
	theapp->WriteProfileInt(folderName,	"seamOffY",	theapp->jobinfo[jobNum].seamOffY);

	//Areas to Avoid
	theapp->WriteProfileInt(folderName,	"avoidOffX",theapp->jobinfo[jobNum].avoidOffX);

	//Stitching Variables
	theapp->WriteProfileInt(folderName,	"stitchY1",theapp->jobinfo[jobNum].stitchY[1]);
	theapp->WriteProfileInt(folderName,	"stitchY2",theapp->jobinfo[jobNum].stitchY[2]);
	theapp->WriteProfileInt(folderName,	"stitchY3",theapp->jobinfo[jobNum].stitchY[3]);
	theapp->WriteProfileInt(folderName,	"stitchY4",theapp->jobinfo[jobNum].stitchY[4]);
	theapp->WriteProfileInt(folderName,	"stitchW",theapp->jobinfo[jobNum].stitchW);
	theapp->WriteProfileInt(folderName,	"stitchH",theapp->jobinfo[jobNum].stitchH);

	theapp->WriteProfileInt(folderName,	"showStLigh",theapp->jobinfo[jobNum].showStLigh);
	theapp->WriteProfileInt(folderName,	"showStDark",theapp->jobinfo[jobNum].showStDark);
	theapp->WriteProfileInt(folderName,	"showStEdges",theapp->jobinfo[jobNum].showStEdges);
	theapp->WriteProfileInt(folderName,	"showStFinal",theapp->jobinfo[jobNum].showStFinal);

	theapp->WriteProfileInt(folderName,	"senEdges",theapp->jobinfo[jobNum].senEdges);
	theapp->WriteProfileInt(folderName,	"stLighSen",theapp->jobinfo[jobNum].stLighSen);
	theapp->WriteProfileInt(folderName,	"stDarkSen",theapp->jobinfo[jobNum].stDarkSen);
	theapp->WriteProfileInt(folderName,	"stEdgeSen",theapp->jobinfo[jobNum].stEdgeSen);

	theapp->WriteProfileInt(folderName,	"stLighImg",theapp->jobinfo[jobNum].stLighImg);
	theapp->WriteProfileInt(folderName,	"stDarkImg",theapp->jobinfo[jobNum].stDarkImg);
	theapp->WriteProfileInt(folderName,	"stEdgeImg",theapp->jobinfo[jobNum].stEdgeImg);

	theapp->WriteProfileInt(folderName,	"useStLigh",theapp->jobinfo[jobNum].useStLigh);
	theapp->WriteProfileInt(folderName,	"useStDark",theapp->jobinfo[jobNum].useStDark);
	theapp->WriteProfileInt(folderName,	"useStEdge",theapp->jobinfo[jobNum].useStEdge);

	theapp->WriteProfileInt(folderName,	"showStLigh",theapp->jobinfo[jobNum].showStLigh);
	theapp->WriteProfileInt(folderName,	"showStDark",theapp->jobinfo[jobNum].showStDark);
	theapp->WriteProfileInt(folderName,	"showStEdges",theapp->jobinfo[jobNum].showStEdges);
	theapp->WriteProfileInt(folderName,	"showStFinal",theapp->jobinfo[jobNum].showStFinal);
	theapp->WriteProfileInt(folderName,	"showStbw",theapp->jobinfo[jobNum].showStbw);

	theapp->WriteProfileInt(folderName,	"lightLevel1",theapp->jobinfo[jobNum].lightLevel1);
	theapp->WriteProfileInt(folderName,	"lightLevel2",theapp->jobinfo[jobNum].lightLevel2);
	theapp->WriteProfileInt(folderName,	"lightLevel3",theapp->jobinfo[jobNum].lightLevel3);
	theapp->WriteProfileInt(folderName,	"lightLevel4",theapp->jobinfo[jobNum].lightLevel4);
	theapp->WriteProfileInt(folderName,	"lightLevel5",theapp->jobinfo[jobNum].lightLevel5);

	theapp->WriteProfileInt(folderName, "vertBarX", theapp->jobinfo[jobNum].vertBarX);
	theapp->WriteProfileInt(folderName, "midBarY", theapp->jobinfo[jobNum].midBarY);
	theapp->WriteProfileInt(folderName, "neckBarY", theapp->jobinfo[jobNum].neckBarY);

	theapp->WriteProfileInt(folderName, "method5_Rt", theapp->jobinfo[jobNum].method5_Rt);
	theapp->WriteProfileInt(folderName, "method5_Gt", theapp->jobinfo[jobNum].method5_Gt);
	theapp->WriteProfileInt(folderName, "method5_Bt", theapp->jobinfo[jobNum].method5_Bt);
	theapp->WriteProfileInt(folderName, "method5_Ht", theapp->jobinfo[jobNum].method5_Ht);

	theapp->WriteProfileInt(folderName, "method5_Rb", theapp->jobinfo[jobNum].method5_Rb);
	theapp->WriteProfileInt(folderName, "method5_Gb", theapp->jobinfo[jobNum].method5_Gb);
	theapp->WriteProfileInt(folderName, "method5_Bb", theapp->jobinfo[jobNum].method5_Bb);
	theapp->WriteProfileInt(folderName, "method5_Hb", theapp->jobinfo[jobNum].method5_Hb);

	//** Cam Parameters
	theapp->WriteProfileInt("camLeft1",	"Current", theapp->camLeft[1]);
	theapp->WriteProfileInt("camLeft2",	"Current", theapp->camLeft[2]);
	theapp->WriteProfileInt("camLeft3",	"Current", theapp->camLeft[3]);
	theapp->WriteProfileInt("camLeft4",	"Current", theapp->camLeft[4]);
	theapp->WriteProfileInt("camLeft5",	"Current", theapp->camLeft[5]);

	theapp->WriteProfileInt("camTop1",	"Current", theapp->camTop[1]);
	theapp->WriteProfileInt("camTop2",	"Current", theapp->camTop[2]);
	theapp->WriteProfileInt("camTop3",	"Current", theapp->camTop[3]);
	theapp->WriteProfileInt("camTop4",	"Current", theapp->camTop[4]);
	theapp->WriteProfileInt("camTop5",	"Current", theapp->camTop[5]);

	theapp->WriteProfileInt("camHeight1","Current", theapp->camHeight[1]);
	theapp->WriteProfileInt("camHeight2","Current", theapp->camHeight[2]);
	theapp->WriteProfileInt("camHeight3","Current", theapp->camHeight[3]);
	theapp->WriteProfileInt("camHeight4","Current", theapp->camHeight[4]);
	theapp->WriteProfileInt("camHeight5","Current", theapp->camHeight[5]);

	theapp->WriteProfileInt("camWidth1","Current", theapp->camWidth[1]);
	theapp->WriteProfileInt("camWidth2","Current", theapp->camWidth[2]);
	theapp->WriteProfileInt("camWidth3","Current", theapp->camWidth[3]);
	theapp->WriteProfileInt("camWidth4","Current", theapp->camWidth[4]);
	theapp->WriteProfileInt("camWidth5","Current", theapp->camWidth[5]);
	
	theapp->WriteProfileInt(folderName, "lDist", theapp->jobinfo[jobNum].lDist);
	
	theapp->WriteProfileInt(folderName, "motPos", theapp->jobinfo[jobNum].motPos);

	theapp->WriteProfileInt(folderName, "notchsen", theapp->jobinfo[jobNum].notchSen);		

	theapp->WriteProfileInt(folderName, "labelsen", theapp->jobinfo[jobNum].labelSen);		

	theapp->WriteProfileInt(folderName, "coloroffset2", theapp->jobinfo[jobNum].colorOffset2);
	theapp->WriteProfileInt(folderName, "wtype", theapp->jobinfo[jobNum].type);
	theapp->WriteProfileInt(folderName, "colortargr", theapp->jobinfo[jobNum].colorTargR);
	theapp->WriteProfileInt(folderName, "colortargg", theapp->jobinfo[jobNum].colorTargG);
	theapp->WriteProfileInt(folderName, "colortargb", theapp->jobinfo[jobNum].colorTargB);

	theapp->WriteProfileInt(folderName, "colortargr2", theapp->jobinfo[jobNum].colorTargR2);
	theapp->WriteProfileInt(folderName, "colortargg2", theapp->jobinfo[jobNum].colorTargG2);
	theapp->WriteProfileInt(folderName, "colortargb2", theapp->jobinfo[jobNum].colorTargB2);
	theapp->WriteProfileInt(folderName, "lizardfilter", theapp->jobinfo[jobNum].lizardFilter);
	theapp->WriteProfileInt(folderName, "colorfilter", theapp->jobinfo[jobNum].colorFilter);
	theapp->WriteProfileInt(folderName, "colorfilter2", theapp->jobinfo[jobNum].colorFilter2);
	theapp->WriteProfileInt(folderName, "bottlecolor", theapp->jobinfo[jobNum].bottleColor);
	theapp->WriteProfileInt(folderName, "prodname", theapp->jobinfo[jobNum].prodName);
	theapp->WriteProfileInt(folderName, "nolabelsen", theapp->jobinfo[jobNum].noLabelSen);
	theapp->WriteProfileInt(folderName, "shiftedLabSen", theapp->jobinfo[jobNum].shiftedLabSen);
	
	theapp->WriteProfileInt(folderName, "EdgeLabSen", theapp->jobinfo[jobNum].EdgeLabSen);

	theapp->WriteProfileInt(folderName, "sensMet3", theapp->jobinfo[jobNum].sensMet3);
	theapp->WriteProfileInt(folderName, "sensEdge", theapp->jobinfo[jobNum].sensEdge);

	theapp->WriteProfileInt(folderName, "heigMet3", theapp->jobinfo[jobNum].heigMet3);
	theapp->WriteProfileInt(folderName, "widtMet3", theapp->jobinfo[jobNum].widtMet3);

	theapp->WriteProfileInt(folderName, "heigMet5", theapp->jobinfo[jobNum].heigMet5);
	theapp->WriteProfileInt(folderName, "widtMet5", theapp->jobinfo[jobNum].widtMet5);
	
	theapp->WriteProfileInt(folderName, "nolabeloffset", theapp->jobinfo[jobNum].noLabelOffset);

	theapp->WriteProfileInt(folderName, "noLabelOffsetX1", theapp->jobinfo[jobNum].noLabelOffsetX[1]);
	theapp->WriteProfileInt(folderName, "noLabelOffsetX2", theapp->jobinfo[jobNum].noLabelOffsetX[2]);
	theapp->WriteProfileInt(folderName, "noLabelOffsetX3", theapp->jobinfo[jobNum].noLabelOffsetX[3]);
	theapp->WriteProfileInt(folderName, "noLabelOffsetX4", theapp->jobinfo[jobNum].noLabelOffsetX[4]);
	
	theapp->WriteProfileInt(folderName, "noLabelOffsetW", theapp->jobinfo[jobNum].noLabelOffsetW);
	theapp->WriteProfileInt(folderName, "noLabelOffsetH", theapp->jobinfo[jobNum].noLabelOffsetH);

	theapp->WriteProfileInt(folderName, "rotatepos", theapp->jobinfo[jobNum].rotatePos);
	theapp->WriteProfileInt(folderName, "insideout", theapp->jobinfo[jobNum].insideOut);

	theapp->WriteProfileInt(folderName, "neckStrength", theapp->jobinfo[jobNum].neckStrength);

	theapp->WriteProfileInt(folderName, "capstrength", theapp->jobinfo[jobNum].capStrength);
	theapp->WriteProfileInt(folderName, "capstrength2", theapp->jobinfo[jobNum].capStrength2);
	theapp->WriteProfileInt(folderName, "mainbodysen", theapp->jobinfo[jobNum].mainBodySen);

	theapp->WriteProfileInt(folderName, "orienarea", theapp->jobinfo[jobNum].orienArea);
	theapp->WriteProfileInt(folderName, "necksen", theapp->jobinfo[jobNum].neckSen);
	theapp->WriteProfileInt(folderName, "neckpos", theapp->jobinfo[jobNum].neckPos);
	theapp->WriteProfileInt(folderName, "underlipenable", theapp->jobinfo[jobNum].underLipEnable);
	theapp->WriteProfileInt(folderName, "neckInOut", theapp->jobinfo[jobNum].neckInOut);
	theapp->WriteProfileInt(folderName, "neckInOut2", theapp->jobinfo[jobNum].neckInOut2);

	theapp->WriteProfileInt(folderName, "greenfilter",theapp->jobinfo[jobNum].greenFilter);
	theapp->WriteProfileInt(folderName, "redfilter",theapp->jobinfo[jobNum].redFilter);
	theapp->WriteProfileInt(folderName, "bluefilter",theapp->jobinfo[jobNum].blueFilter);
	theapp->WriteProfileInt(folderName, "findwhite",theapp->jobinfo[jobNum].findWhite);
	theapp->WriteProfileInt(folderName, "fillsize",theapp->jobinfo[jobNum].fillSize);
	theapp->WriteProfileInt(folderName, "filloffset",theapp->jobinfo[jobNum].fillOffset);
	theapp->WriteProfileInt(folderName, "capcoloroffset",theapp->jobinfo[jobNum].capColorOffset);
	theapp->WriteProfileInt(folderName, "capcolorsize",theapp->jobinfo[jobNum].capColorSize);
	theapp->WriteProfileInt(folderName, "bottlestyle",theapp->jobinfo[jobNum].bottleStyle);
	theapp->WriteProfileInt(folderName, "ellipsesen",theapp->jobinfo[jobNum].ellipseSen);
	theapp->WriteProfileInt(folderName, "labelcolorrectx",theapp->jobinfo[jobNum].labelColorRectX);
	theapp->WriteProfileInt(folderName, "labelcolorrecty",theapp->jobinfo[jobNum].labelColorRectY);

	theapp->WriteProfileInt(folderName, "rotoffset",theapp->jobinfo[jobNum].rotOffset);
	theapp->WriteProfileInt(folderName, "sportsen",theapp->jobinfo[jobNum].sportSen);
	theapp->WriteProfileInt(folderName, "sportpos",theapp->jobinfo[jobNum].sportPos);
	theapp->WriteProfileInt(folderName, "sport",theapp->jobinfo[jobNum].sport);

	//Sensitivity to find middle on the bottle
	theapp->WriteProfileInt(folderName, "senMidd",theapp->jobinfo[jobNum].senMidd);
	theapp->WriteProfileInt(folderName, "MiddImg",theapp->jobinfo[jobNum].MiddImg);

	//to teach Middle of the Bottles
	theapp->WriteProfileInt(folderName,	"taughtMiddle",theapp->jobinfo[jobNum].taughtMiddle);

	theapp->WriteProfileInt(folderName,	"RefForMiddleIsLeft1",theapp->jobinfo[jobNum].RefForMiddleIsLeft[1]);
	theapp->WriteProfileInt(folderName,	"RefForMiddleIsLeft2",theapp->jobinfo[jobNum].RefForMiddleIsLeft[2]);
	theapp->WriteProfileInt(folderName,	"RefForMiddleIsLeft3",theapp->jobinfo[jobNum].RefForMiddleIsLeft[3]);
	theapp->WriteProfileInt(folderName,	"RefForMiddleIsLeft4",theapp->jobinfo[jobNum].RefForMiddleIsLeft[4]);

	theapp->WriteProfileInt(folderName,	"taughtMiddleRefW1",theapp->jobinfo[jobNum].taughtMiddleRefW[1]);
	theapp->WriteProfileInt(folderName,	"taughtMiddleRefW2",theapp->jobinfo[jobNum].taughtMiddleRefW[2]);
	theapp->WriteProfileInt(folderName,	"taughtMiddleRefW3",theapp->jobinfo[jobNum].taughtMiddleRefW[3]);
	theapp->WriteProfileInt(folderName,	"taughtMiddleRefW4",theapp->jobinfo[jobNum].taughtMiddleRefW[4]);

	theapp->WriteProfileInt(folderName,	"taughtMiddleRefY1",theapp->jobinfo[jobNum].taughtMiddleRefY[1]);
	theapp->WriteProfileInt(folderName,	"taughtMiddleRefY2",theapp->jobinfo[jobNum].taughtMiddleRefY[2]);
	theapp->WriteProfileInt(folderName,	"taughtMiddleRefY3",theapp->jobinfo[jobNum].taughtMiddleRefY[3]);
	theapp->WriteProfileInt(folderName,	"taughtMiddleRefY4",theapp->jobinfo[jobNum].taughtMiddleRefY[4]);

	theapp->WriteProfileInt(folderName,	"middlesM1",theapp->jobinfo[jobNum].middlesM[1]);
	theapp->WriteProfileInt(folderName,	"middlesM2",theapp->jobinfo[jobNum].middlesM[2]);
	theapp->WriteProfileInt(folderName,	"middlesM3",theapp->jobinfo[jobNum].middlesM[3]);
	theapp->WriteProfileInt(folderName,	"middlesM4",theapp->jobinfo[jobNum].middlesM[4]);

	theapp->WriteProfileInt(folderName,	"middlesBarX1",theapp->jobinfo[jobNum].middlesBarX[1]);
	theapp->WriteProfileInt(folderName,	"middlesBarX2",theapp->jobinfo[jobNum].middlesBarX[2]);
	theapp->WriteProfileInt(folderName,	"middlesBarX3",theapp->jobinfo[jobNum].middlesBarX[3]);
	theapp->WriteProfileInt(folderName,	"middlesBarX4",theapp->jobinfo[jobNum].middlesBarX[4]);

	theapp->WriteProfileInt(folderName,	"middlesBarY1",theapp->jobinfo[jobNum].middlesBarY[1]);
	theapp->WriteProfileInt(folderName,	"middlesBarY2",theapp->jobinfo[jobNum].middlesBarY[2]);
	theapp->WriteProfileInt(folderName,	"middlesBarY3",theapp->jobinfo[jobNum].middlesBarY[3]);
	theapp->WriteProfileInt(folderName,	"middlesBarY4",theapp->jobinfo[jobNum].middlesBarY[4]);

	theapp->WriteProfileInt(folderName,	"showC5inPictures",theapp->jobinfo[jobNum].showC5inPictures);

	theapp->WriteProfileInt(folderName,	"ytopLimPatt",theapp->jobinfo[jobNum].ytopLimPatt);	
	theapp->WriteProfileInt(folderName,	"ybotLimPatt",theapp->jobinfo[jobNum].ybotLimPatt);	

	theapp->WriteProfileInt(folderName,	"shiftTopLim1",theapp->jobinfo[jobNum].shiftTopLim[1]);	
	theapp->WriteProfileInt(folderName,	"shiftBotLim1",theapp->jobinfo[jobNum].shiftBotLim[1]);	
	theapp->WriteProfileInt(folderName,	"shiftTopLim2",theapp->jobinfo[jobNum].shiftTopLim[2]);	
	theapp->WriteProfileInt(folderName,	"shiftBotLim2",theapp->jobinfo[jobNum].shiftBotLim[2]);	
	theapp->WriteProfileInt(folderName,	"shiftTopLim3",theapp->jobinfo[jobNum].shiftTopLim[3]);	
	theapp->WriteProfileInt(folderName,	"shiftBotLim3",theapp->jobinfo[jobNum].shiftBotLim[3]);	
	theapp->WriteProfileInt(folderName,	"shiftTopLim4",theapp->jobinfo[jobNum].shiftTopLim[4]);	
	theapp->WriteProfileInt(folderName,	"shiftBotLim4",theapp->jobinfo[jobNum].shiftBotLim[4]);	

	theapp->WriteProfileInt(folderName,	"shiftHorIniLim",theapp->jobinfo[jobNum].shiftHorIniLim);	
	theapp->WriteProfileInt(folderName,	"shiftHorEndLim",theapp->jobinfo[jobNum].shiftHorEndLim);	

	theapp->WriteProfileInt(folderName,	"shiftWidth",theapp->jobinfo[jobNum].shiftWidth);	
	
	theapp->WriteProfileInt(folderName,	"findMiddles",theapp->jobinfo[jobNum].findMiddles);
	theapp->WriteProfileInt(folderName,	"showMiddles",theapp->jobinfo[jobNum].showMiddles);
	theapp->WriteProfileInt(folderName,	"showEdges",	theapp->jobinfo[jobNum].showEdges);
	

	theapp->WriteProfileInt(folderName,	"showSetupI5",theapp->jobinfo[jobNum].showSetupI5);
	theapp->WriteProfileInt(folderName,	"showSetupI6",theapp->jobinfo[jobNum].showSetupI6);
	theapp->WriteProfileInt(folderName,	"showSetupI7",theapp->jobinfo[jobNum].showSetupI7);
	theapp->WriteProfileInt(folderName,	"showSetupI8",theapp->jobinfo[jobNum].showSetupI8);
	theapp->WriteProfileInt(folderName,	"showSetupI9",theapp->jobinfo[jobNum].showSetupI9);
	theapp->WriteProfileInt(folderName,	"showSetupI10",theapp->jobinfo[jobNum].showSetupI10);

	///////////////
	
	theapp->WriteProfileInt(folderName,	"ysplice1",theapp->jobinfo[jobNum].ySplice[1]);
	theapp->WriteProfileInt(folderName,	"ysplice2",theapp->jobinfo[jobNum].ySplice[2]);
	theapp->WriteProfileInt(folderName,	"ysplice3",theapp->jobinfo[jobNum].ySplice[3]);
	theapp->WriteProfileInt(folderName,	"ysplice4",theapp->jobinfo[jobNum].ySplice[4]);
	theapp->WriteProfileInt(folderName,	"wSplice",theapp->jobinfo[jobNum].wSplice);
	theapp->WriteProfileInt(folderName,	"hSplice",theapp->jobinfo[jobNum].hSplice);
	theapp->WriteProfileInt(folderName,	"hSpliceSmBx",theapp->jobinfo[jobNum].hSpliceSmBx);

	theapp->WriteProfileInt(folderName,	"spliceVar",theapp->jobinfo[jobNum].spliceVar);
	
	///////////////

	theapp->WriteProfileInt(folderName,	"tgXpatt1",theapp->jobinfo[jobNum].tgXpatt[1]);
	theapp->WriteProfileInt(folderName,	"tgYpatt1",theapp->jobinfo[jobNum].tgYpatt[1]);
	theapp->WriteProfileInt(folderName,	"tgXpatt2",theapp->jobinfo[jobNum].tgXpatt[2]);
	theapp->WriteProfileInt(folderName,	"tgYpatt2",theapp->jobinfo[jobNum].tgYpatt[2]);
	theapp->WriteProfileInt(folderName,	"tgXpatt3",theapp->jobinfo[jobNum].tgXpatt[3]);
	theapp->WriteProfileInt(folderName,	"tgYpatt3",theapp->jobinfo[jobNum].tgYpatt[3]);

	theapp->WriteProfileInt(folderName,	"meth6_patt1_x",theapp->jobinfo[jobNum].meth6_patt1_x);
	theapp->WriteProfileInt(folderName,	"meth6_patt1_y",theapp->jobinfo[jobNum].meth6_patt1_y);

	theapp->WriteProfileInt(folderName,	"meth6_patt2_x",theapp->jobinfo[jobNum].meth6_patt2_x);
	theapp->WriteProfileInt(folderName,	"meth6_patt2_y",theapp->jobinfo[jobNum].meth6_patt2_y);

	theapp->WriteProfileInt(folderName,	"meth6_patt3_x",theapp->jobinfo[jobNum].meth6_patt3_x);
	theapp->WriteProfileInt(folderName,	"meth6_patt3_y",theapp->jobinfo[jobNum].meth6_patt3_y);

	///////////////

	theapp->jobinfo[jobNum].totinsp=12;

	for (int curInspctn=1; curInspctn<=theapp->jobinfo[jobNum].totinsp; curInspctn++)//theapp->jobinfo[jobnum].totinsp
	{
		sprintf(inspnum, "\\Insp%03d", curInspctn);
		strcat(folderName,inspnum);

		theapp->WriteProfileString(folderName,	"name", theapp->inspctn[curInspctn].name);
		theapp->WriteProfileInt(folderName,		"enable", theapp->inspctn[curInspctn].enable);

		theapp->WriteProfileInt(folderName,		"lmin", theapp->inspctn[curInspctn].lmin);
		theapp->WriteProfileInt(folderName,		"lmax", theapp->inspctn[curInspctn].lmax);

		theapp->WriteProfileInt(folderName,		"lminB", theapp->inspctn[curInspctn].lminB);
		theapp->WriteProfileInt(folderName,		"lmaxB", theapp->inspctn[curInspctn].lmaxB);

		theapp->WriteProfileInt(folderName,		"lminC", theapp->inspctn[curInspctn].lminC);
		theapp->WriteProfileInt(folderName,		"lmaxC", theapp->inspctn[curInspctn].lmaxC);
	}
	
	if(jobNum!=0)
	{
		theapp->WriteProfileInt("EncDist",		"Current", theapp->SavedEncDist);
		theapp->WriteProfileInt("EncDur",		"Current", theapp->SavedEncDur);
		theapp->WriteProfileInt("Eject",		"Current", theapp->SavedEjectDisable);
		theapp->WriteProfileInt("SavedDelay",	"Current", theapp->SavedDelay);
		theapp->WriteProfileInt("Photo",		"Current", theapp->SavedPhotoeye);
		theapp->WriteProfileInt("saveddeadband","Current", theapp->SavedDeadBand);
	
		theapp->WriteProfileInt("EncSim",		"Current", theapp->SavedEncSim);
		theapp->WriteProfileInt("Photo",		"Current", theapp->SavedPhotoeye);
		theapp->WriteProfileInt("IORejects",	"Current", theapp->SavedIORejects);
		theapp->WriteProfileInt("ConRejects",	"Current", theapp->SavedConRejects);
		theapp->WriteProfileInt("ConRejects2",	"Current", theapp->SavedConRejects2);
	}

	theapp->WriteProfileString("Pass",			"Current", theapp->SavedPassWord);
	theapp->WriteProfileString("PassOP",		"Current", theapp->SavedOPPassWord);
	theapp->WriteProfileString("PassMAN",		"Current", theapp->SavedMANPassWord);
	theapp->WriteProfileString("PassSUP",		"Current", theapp->SavedSUPPassWord);
	theapp->WriteProfileInt("trialradius",		"Current", theapp->trialRadius);
	theapp->WriteProfileInt("heartbeat",		"Current", theapp->enableHeartBeat);
	theapp->WriteProfileInt("deadband",			"Current", theapp->SavedDeadBand);

	theapp->WriteProfileInt("camser1",			"Current", theapp->CamSer1);
	theapp->WriteProfileInt("camser2",			"Current", theapp->CamSer2);
	theapp->WriteProfileInt("camser3",			"Current", theapp->CamSer3);
	theapp->WriteProfileInt("camser4",			"Current", theapp->CamSer4);
	theapp->WriteProfileInt("camser5",			"Current", theapp->CamSer5);
	theapp->WriteProfileInt("cam1offset",		"Current", theapp->cam1Offset);
	theapp->WriteProfileInt("cam2offset",		"Current", theapp->cam2Offset);
	theapp->WriteProfileInt("cam3offset",		"Current", theapp->cam3Offset);
	theapp->WriteProfileInt("cam4offset",		"Current", theapp->cam4Offset);
	theapp->WriteProfileInt("savedzero",		"Current", theapp->savedZero);

	int Shutter=theapp->savedShutter*10;
	theapp->WriteProfileInt("shutter",			"Current", Shutter);

	int Shutter2=theapp->savedShutter2*10;
	theapp->WriteProfileInt("shutter2",			"Current", Shutter2);

	theapp->WriteProfileInt("securityEnable",	"Current", theapp->securityEnable);


	theapp->WriteProfileInt("inspdelay",		"Current", theapp->inspDelay);
	theapp->WriteProfileInt("24hour",			"Current", theapp->hourClock);

	theapp->WriteProfileInt("Jobs",				"Current", theapp->SavedTotalJobs);
	
	theapp->WriteProfileInt("xlighton",			"Current", theapp->XLightOn);
	theapp->WriteProfileInt("boltoffset",		"Current", theapp->boltOffset);
	theapp->WriteProfileInt("boltoffset2",		"Current", theapp->boltOffset2);		
	theapp->WriteProfileInt("donewledge",		"Current", theapp->DoNewLedge);	
	theapp->WriteProfileInt("capejectorinstalled", "Current", theapp->capEjectorInstalled);	
	theapp->WriteProfileInt("labelthresh",		"Current", theapp->jobinfo[jobNum].labelThresh);		//added to mirror CBottleApp::ReadRegistery
	theapp->WriteProfileInt("profile",			"Current", theapp->jobinfo[jobNum].profile);		//added to mirror CBottleApp::ReadRegistery
	theapp->WriteProfileInt("showmot",			"Current", theapp->showMot);		
	theapp->WriteProfileInt("decCutEnable",		"Current", theapp->jobinfo[jobNum].decCutEnable);		//added to mirror CBottleApp::ReadRegistery
	theapp->WriteProfileInt("intThresh",		"Current", theapp->jobinfo[jobNum].intThresh);		//added to mirror CBottleApp::ReadRegistery
	theapp->WriteProfileInt("convratio",		"Current", theapp->convRatio);
	theapp->WriteProfileInt("bottlesensor",		"Current", theapp->SavedBottleSensor);
	theapp->WriteProfileInt("dboffset",			"Current", theapp->SavedDBOffset);

	theapp->WriteProfileInt("bandwidth",		"Current", theapp->bandwidth);
	theapp->WriteProfileInt("bandwidth5",		"Current", theapp->bandwidth5);
	
	theapp->WriteProfileInt("lightDuration",	"Current", theapp->lightDuration);
	theapp->WriteProfileInt("ringDuration",		"Current", theapp->ringDuration);

	theapp->WriteProfileInt("holdtime1",		"Current", theapp->holdTime1);
	theapp->WriteProfileInt("enabPicsToShow",	"Current", theapp->enabPicsToShow);

	theapp->WriteProfileInt("passNotFinished", "Current", theapp->passNotFinished);

	theapp->WriteProfileInt("templimit",		"Current", theapp->tempLimit);
	theapp->WriteProfileInt("tempSelect",		"Current", theapp->tempSelect);

	theapp->WriteProfileInt("whitebalance1Red",		"Current", theapp->whitebalance1Red);
	theapp->WriteProfileInt("whitebalance2Red",		"Current", theapp->whitebalance2Red);
	theapp->WriteProfileInt("whitebalance3Red",		"Current", theapp->whitebalance3Red);
	theapp->WriteProfileInt("whitebalance4Red",		"Current", theapp->whitebalance4Red);
	theapp->WriteProfileInt("whitebalance5Red",		"Current", theapp->whitebalance5Red);
	theapp->WriteProfileInt("whitebalance1Blue",	"Current", theapp->whitebalance1Blue);
	theapp->WriteProfileInt("whitebalance2Blue",	"Current", theapp->whitebalance2Blue);
	theapp->WriteProfileInt("whitebalance3Blue",	"Current", theapp->whitebalance3Blue);
	theapp->WriteProfileInt("whitebalance4Blue",	"Current", theapp->whitebalance4Blue);
	theapp->WriteProfileInt("whitebalance5Blue",	"Current", theapp->whitebalance5Blue);


	///////////
}


void CMainFrame::CleanUp()
{


}


void CMainFrame::OnSetup4() 
{
	if(SModify)	{SDelay sdelay; sdelay.DoModal();}
	else				{Security sec; sec.DoModal();}
}


void CMainFrame::OnLogout() 
{ SetTimer(13,1,NULL); }

/*void CMainFrame::OnEncoder() 
{
	if(SSetup)	{ Tech tech; tech.DoModal(); }
	else				{ Security sec; sec.DoModal();}
}*/

void CMainFrame::UpdatePLCData()
{
	int value=0;
	int function=0;
	int EjectRate2=0;

	timesThru+=1;

	if(timesThru>=29)	{timesThru=1;}
	
	switch(timesThru)
	{
	case 1:	//Total
		value=theapp->Totalx;	function=650;
		if(theapp->serPresent) {m_pserial->SendChar(value,function);}
		theapp->Totalx=0;
		oldTotal=0;
		break;

	case 2:	//Rejects
		value=Rejectsx;			function=660;
		if(theapp->serPresent) {m_pserial->SendChar(value,function);}
		Rejectsx=0;
		break;

	case 3:	//
		value=0;				function=690;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		m_pxaxis->E4Cntx=0;
		break;

	case 4:	//I5 Label Height
		value=m_pxaxis->E5Cntx;	function=700;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		m_pxaxis->E5Cntx=0;
		break;

	case 5:	//I6 Rotate
		value=m_pxaxis->E6Cntx;	function=710;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		m_pxaxis->E6Cntx=0;
		break;

	case 6:	//I7 Label Integrity
		value=m_pxaxis->E7Cntx;	function=750;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		m_pxaxis->E7Cntx=0;
		break;

	case 7:	//I8 No Label
		value=m_pxaxis->E8Cntx;	function=740;
		if(theapp->serPresent) {m_pserial->SendChar(value,function);}
		m_pxaxis->E8Cntx=0;
		break;	

	case 8:	//I9 Splice
		value=m_pxaxis->E9Cntx;	function=730;
		if(theapp->serPresent) {m_pserial->SendChar(value,function);}
		m_pxaxis->E9Cntx=0;
		break;
		
	case 9: //I10 Spare
		value=m_pxaxis->E10Cntx;function=760;
		if(theapp->serPresent) {m_pserial->SendChar(value,function);}
		m_pxaxis->E10Cntx=0;
		break;	

	case 10://I4 Neck Integrity
		value=m_pxaxis->E4Cntx;	function=720;
		if(theapp->serPresent) {m_pserial->SendChar(value,function);}
		break;	

	case 11://I1 and I2 Cap height
		value=m_pxaxis->E1Cntx+m_pxaxis->E2Cntx; function=670;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		m_pxaxis->E1Cntx=m_pxaxis->E2Cntx=0;
		break;

	case 12://I3 Cap color
		value=m_pxaxis->E3Cntx;	function=680;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		m_pxaxis->E3Cntx=0;
		break;

	case 13:
		value=theapp->outh0;	function=920;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		theapp->outh0=0;
		break;
	
	case 14:
		//value=theapp->outh6;	function=800;
		//if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		//theapp->outh6=0;
		break;

	case 15:
		value=theapp->outh5;	function=810;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		theapp->outh5=0;
		break;
	
	case 16:
		value=theapp->outh4;	function=820;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		theapp->outh4=0;
		break;
	
	case 17:
		value=theapp->outh3;	function=830;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		theapp->outh3=0;
		break;
	
	case 18:
		value=theapp->outh2;	function=840;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		theapp->outh2=0;
		break;
	
	case 19:
		value=theapp->outh1;	function=850;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		theapp->outh1=0;
		break;
	
	case 20:
		value=theapp->outh1m;	function=860;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		theapp->outh1m=0;
		break;
	
	case 21:
		value=theapp->outh2m;	function=870;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		theapp->outh2m=0;
		break;
	
	case 22:
		value=theapp->outh3m;	function=880;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		theapp->outh3m=0;
		break;
	
	case 23:
		value=theapp->outh4m;	function=890;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		theapp->outh4m=0;
		break;

	case 24:
		value=theapp->outh5m;	function=900;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		theapp->outh5m=0;
		break;
	
	case 25:
		value=theapp->outh6m;	function=910;
		if(theapp->serPresent)	{m_pserial->SendChar(value,function);}
		theapp->outh6m=0;
		break;

	case 26:
		value=theapp->outh7;	function=930;
		if(theapp->serPresent) {m_pserial->SendChar(value,function);}
		theapp->outh7=0;
		break;

	case 27:
		value=theapp->outh7m;	function=940;
		if(theapp->serPresent) {m_pserial->SendChar(value,function);}
		theapp->outh7m=0;
		break;

	case 28:
		dataReady=false;

		CTime thetime = CTime::GetCurrentTime();

		if( (thetime.GetHour()==theapp->hourClock && !CopyDone) || testData)
		{
			CopyDone=true;
			testData=false;
			//s="e:\\" + s + ".xls";
			//int filenum=theapp->SavedDataFile+=1;
			CTime thetime = CTime::GetCurrentTime();
			char* CurrentName="24Hrdata";

			CString s = thetime.Format( "%m%d%Y" );
			CString d = CurrentName;
			//CString d4;

			char* pFileName = "c:\\silgandata\\";//drive	
			char currentpath[60];

			strcpy(currentpath,pFileName);
			strcat(currentpath,d);
			strcat(currentpath,s);
			
			strcat(currentpath,".txt");

			DataFile.Open(currentpath, CFile::modeWrite | CFile::modeCreate);

			m_r1.Format("%3i",	m_pxaxis->E1Cnt24);//CapHeight
			m_r2.Format(" %3i",	m_pxaxis->E2Cnt24);
			m_r3.Format(" %3i",	m_pxaxis->E3Cnt24);	
			m_r4.Format(" %3i",	m_pxaxis->E4Cnt24);
			m_r5.Format(" %3i",	m_pxaxis->E5Cnt24);
			m_r6.Format(" %3i",	m_pxaxis->E6Cnt24);
			m_r7.Format(" %3i",	m_pxaxis->E7Cnt24);
			m_r8.Format(" %3i",	m_pxaxis->E8Cnt24);
			m_r9.Format(" %3i", m_pxaxis->E9Cnt24);
			m_r10.Format(" %3i",m_pxaxis->E10Cnt24);
			m_r11.Format(" %8i",theapp->Total24);
			m_r15.Format(" %5i",theapp->Rejects24);

			DataFile.Write("CapRL",5);
			DataFile.Write(m_r1,m_r1.GetLength());
			DataFile.Write(", ",1);

			DataFile.Write("CapM",4);
			DataFile.Write(m_r2,m_r2.GetLength());
			DataFile.Write(", ",1);

			DataFile.Write("CapColor",8);
			DataFile.Write(m_r3,m_r3.GetLength());
			DataFile.Write(", ",1);

			DataFile.Write("Neck", 4);
			DataFile.Write(m_r4,m_r4.GetLength());
			DataFile.Write(", ",1);

			DataFile.Write("LabelHeight",11);
			DataFile.Write(m_r5,m_r5.GetLength());
			DataFile.Write(", ",1);

			DataFile.Write("Rotate",6);
			DataFile.Write(m_r6,m_r6.GetLength());
			DataFile.Write(", ",1);

			DataFile.Write("LabelIntegrity",14);
			DataFile.Write(m_r7,m_r7.GetLength());
			DataFile.Write(", ",1);

			DataFile.Write("NoLabel",7);
			DataFile.Write(m_r8,m_r8.GetLength());
			DataFile.Write(", ",1);

			DataFile.Write("Splice",6);
			DataFile.Write(m_r9,m_r9.GetLength());
			DataFile.Write(", ",1);

			DataFile.Write("xxx",3);
			DataFile.Write(m_r10,m_r10.GetLength());
			DataFile.Write(", ",1);

			DataFile.Write("Total",5);
			DataFile.Write(m_r11,m_r11.GetLength());
			DataFile.Write(", ",1);

			DataFile.Write("Rejects",7);
			DataFile.Write(m_r15,m_r15.GetLength());
			DataFile.Write(", ",1);
			
			DataFile.Close();

			m_pxaxis->E1Cnt24	=	0;
			m_pxaxis->E2Cnt24	=	0;
			m_pxaxis->E3Cnt24	=	0;
			m_pxaxis->E4Cnt24	=	0;
			m_pxaxis->E5Cnt24	=	0;
			m_pxaxis->E6Cnt24	=	0;
			m_pxaxis->E7Cnt24	=	0;
			m_pxaxis->E8Cnt24	=	0;
			m_pxaxis->E9Cnt24	=	0;
			m_pxaxis->E10Cnt24	=	0;

			theapp->Total24=0;
			theapp->Rejects24=0;
		}

		if(thetime.GetHour()!=theapp->hourClock)	{CopyDone=false;}

		break;
	}
}


/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers


void CMainFrame::OnShowguide() 
{
	theapp->showGuide = !theapp->showGuide;
	m_pxaxis->InvalidateRect(NULL,false);
}

void CMainFrame::OnAdvanced() 
{
	if(SAdvanced)		{Advanced advanced;	advanced.DoModal();}
	else
	{
		Security sec; sec.DoModal();
		if(SAdvanced)	{Advanced advanced;	advanced.DoModal();}
	}
}

void CMainFrame::OnRecalldefective() 
{
	PicRecal picrecall;
	picrecall.DoModal();
}

void CMainFrame::OnSystemConfig() 
{
//	SSecretMenu = false;
//	Security sec;
//	sec.DoModal();

	if(SSecretMenu)
	{
		m_sysconfigform->SetParams((MachineType)theapp->cam5Enable, theapp->sleeverEnable, theapp->waistAvailable, theapp->alignAvailable, theapp->useDiff51R85Hardware);
		m_sysconfigform->ShowWindow(SW_SHOW);
	}
	else
	{
		Security sec; sec.DoModal();

		if(SSecretMenu)
		{
			m_sysconfigform->SetParams((MachineType)theapp->cam5Enable, theapp->sleeverEnable, theapp->waistAvailable, theapp->alignAvailable, theapp->useDiff51R85Hardware);
			m_sysconfigform->ShowWindow(SW_SHOW);
		}
	}
}

void CMainFrame::OnDownbottle() 
{
	if(SDownBottle)		{BottleDown bottledown;	bottledown.DoModal();}
	else
	{
		Security sec; sec.DoModal();
		if(SDownBottle)	{BottleDown bottledown;	bottledown.DoModal();}
	}
}

void CMainFrame::OnCamerasettings() 
{
//	SSecretMenu = false;
//	Security sec;
//	sec.DoModal();

	if(SSecretMenu && SCam && theapp->camsPresent)
	{
		m_pcamera->ShowWindow(SW_SHOW);
		m_pcamera->UpdateDisplay();
	}
	else
	{
		if(!theapp->camsPresent)
		{
			AfxMessageBox("No cameras detected in system");
		}
		else
		{
			Security sec; sec.DoModal();

			if(SSecretMenu && SCam)
			{
				m_pcamera->ShowWindow(SW_SHOW);
				m_pcamera->UpdateDisplay();
			}
		}
	}
	/*
	if(SCam)
	{	m_pcamera->ShowWindow(SW_SHOW);
		m_pcamera->UpdateDisplay();
	}
	else			{Security sec; sec.DoModal();}
	*/
}


//with false all dialog boxes will call for Security password
//with true  all dialog boxes won't ask for Security password
void CMainFrame::setSecurityFlagsOFF(bool status)
{
	theapp->SavedAdvanced=SAdvanced=status;
	theapp->SavedDownBottle=SDownBottle=status;
	theapp->SavedCam=SCam=status;
	theapp->SavedSetup=SSetup=status;
	theapp->SavedLimits=SLimits=status;
	theapp->SavedEject=SEject=status;
	theapp->SavedModify=SModify=status;
	theapp->SavedJob=SJob=status;
	theapp->SavedTech=STech=status;
	SSecretMenu=false;  //the secret menu is the exception as it is part of root access
}




void CMainFrame::ScreenSaverCheckAndRefresh()
{
	if(flagScreenSaver)
	{
		//check if the screensaver is running
		bool screenSaverIsON = GetScreenSaverRunning();

		if(screenSaverIsON)
		{
			screenSaverTimeOut = GetScreenSaverTimeout();
			StopScreenSaver();
			KillTimer(42);
			SetTimer(42, screenSaverTimeOut*1000, NULL);
			
			flagScreenSaver = false;
		}
	}
}

int CMainFrame::GetScreenSaverTimeout()
{
	int SPI_GETSCREENSAVERTIMEOUT = 14;
	int value = 600;
	::SystemParametersInfo(SPI_GETSCREENSAVERTIMEOUT, 0, &value, 0); //value in seconds
	
	return value;
}

bool CMainFrame::GetScreenSaverRunning()
{
	bool isRunning =false;
//	int SPI_GETSCREENSAVERRUNNING = 114;
	::SystemParametersInfo(SPI_GETSCREENSAVERRUNNING, 0, &isRunning, 0);

	return isRunning;
}

void CMainFrame::StopScreenSaver()
{
	//MLUCAS JULY26 just to make the screen saver to work
	//requires to modify the stdafx.h file, had to add this line
	//#define _WIN32_WINNT 0x0501	
	INPUT ip;

	ip.type = INPUT_KEYBOARD;
	ip.ki.wScan = 0;
	ip.ki.time = 0;
	ip.ki.dwExtraInfo = 0;

	//Press the "A" key
	ip.ki.wVk = 0x41; //virtual-key code for the "a" key
	ip.ki.dwFlags = 0; //0 for key press
	::SendInput(1, &ip, sizeof(::INPUT));
	
	//Release the "A" key
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	::SendInput(1, &ip, sizeof(::INPUT));
}

void CMainFrame::OnUsb() 
{
	if(!theapp->lock)
	{
		m_pusb->ShowWindow(SW_SHOW);
	}
	else
	{
		Security sec;
		sec.DoModal();

		if(!theapp->lock)
		{
			m_pusb->ShowWindow(SW_SHOW);
		}
	}
}

void CMainFrame::OnDataHeadtrackerdata() 
{
	SleeverHeadData sleever;
	sleever.DoModal();
	
}

//Local 2D array "_arrayLocal" only holds inspection results for the most recent 40 bottle iteration.

void CMainFrame::PopulateArray()
{
	if((_sleeverHeadNumber == 1) && (_counterSleever ==40))  //implies previous iteration was GOOD
	{
		//##Update the master 2D array & Reset the local 2D array
			for(int i=0;i<42;i++)
			{
				for(int j=0;j<12;j++)
				{
				arrayA[i][j]+=_arrayLocal[i][j];
				
				}
			}
		
			memset( _arrayLocal,0,sizeof( _arrayLocal));
		//##


			std::fstream myFile;

			 //Writing to Text file
			myFile.open("C:\\SleeverData.txt", ios::in| ios::out|ios::trunc);

			if(myFile.is_open())
			{
		 		myFile.flush();
				CString	szDigit2;
				int digit2;

				myFile<<"\t  ";
					 

				for(int a=1;a<11;a++)
				{
					myFile<<setw(5);
					szDigit2.Format("%s%d","I",a);
					string str(szDigit2);	  
					myFile<<str;
				}
				 myFile<<"\n\n";

	 			for( int i=1;i<41;i++)	
				{
			 
					myFile<<"Head";
					myFile<<setw(3);
					szDigit2.Format("%d", i);
					string str(szDigit2);	  
					myFile<<str;

					for(int j=1;j<11;j++)
					{	
						digit2=arrayA[i][j];
						myFile<<setw(5);
						szDigit2.Format("%i",digit2);
						string str2(szDigit2);	  
						myFile<<str2;

					}
				 		  
					myFile<<"\n";
				}
			
 				myFile.flush();
				myFile.close();

			}

			// Reset count to 1 
			_counterSleever =1;		
	}

	
	else if((_sleeverHeadNumber == 1) && (_counterSleever !=40))
	{
		//Reset 2D Array
		//	memset( arrayA,0,sizeof( arrayA));
		memset( _arrayLocal,0,sizeof( _arrayLocal));

		// Reset counter to 1 
		_counterSleever =1;

		//_arrayLocal
	}

	if(_sleeverHeadNumber<42)
	{
		//This part gets always executed
		for( int i=1;i<11;i++)  //looping throught all the inspection results
		{
			if(!m_pxaxis->inspResultOK[i])
			{
				//	 arrayA[_sleeverHeadNumber][i]+=1;  // assign 1 to sleever head numbers that have failed atleast 1 inspection
				 _arrayLocal[_sleeverHeadNumber][i]+=1;  // assign 1 to sleever head numbers that have failed atleast 1 inspection
			}
		}
	}	
}
