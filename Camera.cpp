// Camera.cpp : implementation file
//

#include "stdafx.h"
#include "Bottle.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "xaxisview.h"

#include <FlyCapture2.h>
#include "status.h"
#include "Camera.h"
#include "mainfrm.h"
#include "Serial.h"
#include "windows.h"
#include <processthreadsapi.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define _MIN_BGR_BUFFER_SIZE  640 * 480 * 4		//Minimum image size RGB and transparency
#define _MIN_BGR_COLS  640						//Minimum image size RGB and transparency
#define _MIN_BGR_ROWS  480						//Minimum image size RGB and transparency

/////////////////////////////////////////////////////////////////////////////
// CCamera dialog

#define _DEFAULT_WINDOW_X 1024
#define _DEFAULT_WINDOW_Y 768
// Grab this many images, then quit.
//
#define IMAGES 5

//unsigned long cam1SerialNumber=7420151;//wrong trop2
//unsigned long cam2SerialNumber=7420145;//trop2 
//unsigned long cam3SerialNumber=7380563;//trop2
//unsigned long cam4SerialNumber=7380559;//trop2

//unsigned long cam1SerialNumber=7420163;//trop1 3
//unsigned long cam3SerialNumber=7420148;//trop1 1
//unsigned long cam4SerialNumber=7380463;//trop1 2
//unsigned long cam2SerialNumber=7380573;//trop1 4

//unsigned long cam1SerialNumber=7420165;//real trop2
//unsigned long cam3SerialNumber=7420150;//1
//unsigned long cam4SerialNumber=7420157;//2
//unsigned long cam2SerialNumber=7380565;//4

//unsigned long cam1SerialNumber=7510528;//test 3
//unsigned long cam3SerialNumber=7510600;//test 1
//unsigned long cam4SerialNumber=7510606;//test 2
//unsigned long cam2SerialNumber=7510598;//test 4


// Set the grab timeout to this many milliseconds.
//
#define TIMEOUT 5000

// Software trigger the camera instead of using an external hardware trigger
//
#define SOFTWARE_TRIGGER_CAMERA

// By default, use the PGR-specific SOFT_ASYNC_TRIGGER register 0x102C to
// generate the software trigger. Comment this out to use the DCAM 1.31 
// SOFTWARE_TRIGGER register 0x62C as the software trigger (note: this requires
// a DCAM 1.31-compliant camera that implements this functionality).
//
#define USE_SOFT_ASYNC_TRIGGER

#define _MAX_BGR_BUFFER_SIZE  FullImageWidth * FullImageHeight * bytesPerPixel

#define ALLOW_CAMERA_SELECT true
// The minimum size of the BGR image buffer.
//
#define _MIN_BGR_BUFFER_SIZE  CroppedImageWidth * CroppedImageHeight * bytesPerPixel
// Register defines
// 
#define INITIALIZE         0x000
#define TRIGGER_INQ        0x530
#define CAMERA_POWER       0x610
#define SOFTWARE_TRIGGER   0x62C
#define SOFT_ASYNC_TRIGGER 0x102C

unsigned char**   g_arpBuffers[ _MAX_CAMERAS ];

// Error handling macro.
//
#define _CHECK( error, function, retval ) \
{ \
   if ( error != FLYCAPTURE_OK ) \
   { \
      CString  csMessage; \
      csMessage.Format( \
         "%s reported \"%s\"", \
         function, \
         flycaptureErrorToString( error ) ); \
      AfxMessageBox( csMessage, error ); \
      \
      return retval;\
   } \
} \

//=============================================================================
// Function Definitions
//=============================================================================
//FlyCapture2::Error checkSoftwareTriggerPresence( FlyCaptureContext  m_flyCaptureContext, unsigned int uiRegister )
//{
//   FlyCapture2::Error   error;
//   unsigned long     ulValue;
//   
//   switch( uiRegister )
//   {
//      case SOFT_ASYNC_TRIGGER:
//         error = flycaptureGetCameraRegister( 
//            m_flyCaptureContext, SOFT_ASYNC_TRIGGER, &ulValue );
//         //_CHECK( error, "flycaptureGetCameraRegister()", 1);
//
//         //
//         // Check the Presence_Inq field of the register; bit 0 == 1 indicates
//         // presence of this feature.
//         //
//         if( ( ulValue & 0x80000000 ) == 0x80000000 )
//         {return PGRERROR_OK;}
//         else
//         {return PGRERROR_INVALID_PARAMETER;}
//
//      case SOFTWARE_TRIGGER:
//         error = flycaptureGetCameraRegister( 
//            m_flyCaptureContext, TRIGGER_INQ, &ulValue );
//         //_CHECK( "flycaptureGetCameraRegister()", error );
//
//         //
//         // Check the Software_Trigger_Inq field of the register; bit 15 == 1 
//         // indicates presence of this feature.
//         //
//         if( (ulValue & 0x10000) == 0x10000 )
//         {return PGRERROR_OK;}
//         else
//         {return PGRERROR_NOT_IMPLEMENTED;}
//
//      default:
//         return PGRERROR_INVALID_PARAMETER;
//   }
//}
//
//FlyCapture2::Error checkTriggerReady( FlyCaptureContext m_flyCaptureContext )
//{
//	FlyCapture2::Error   error;
//
//   unsigned long     ulValue;
//
//   // 
//   // Do our check to make sure the camera is ready to be triggered
//   // by looking at bits 30-31. Any value other than 1 indicates
//   // the camera is not ready to be triggered.
//   //
//   error = flycaptureGetCameraRegister( 
//      m_flyCaptureContext, SOFT_ASYNC_TRIGGER, &ulValue );
//   //_CHECK( "flycaptureGetCameraRegister()", error );
//
//   while( ulValue != 0x80000001 )
//   {
//      error = flycaptureGetCameraRegister( 
//         m_flyCaptureContext, SOFT_ASYNC_TRIGGER, &ulValue );
//      //_CHECK( "flycaptureGetCameraRegister()", error );
//   }
//
//   return PGRERROR_OK;
//}
// Buffers per camera.
//
#define _BUFFERS 5
int g_uiCameras=5;
//
// Maximum cameras on the bus. 
// (the maximum devices allowed on a 1394 bus is 64).
//
#define _MAX_IMAGE_SIZE 1600 * 1200 * 4
//=============================================================================
// Function Definitions
//=============================================================================
//
// allocateBuffers()
//
// This function allocates a series of image buffers for every camera on the 
// bus.  These images are later deallocated with a call to deallocateBuffers().
//
void allocateBuffers()
{
   for( unsigned iCamera = 0; iCamera < g_uiCameras; iCamera++ )
   {
      g_arpBuffers[ iCamera ] = new unsigned char*[ _BUFFERS ];
      
      for( unsigned i = 0; i < _BUFFERS; i++ )
      {g_arpBuffers[ iCamera ][ i ] = new unsigned char[ _MAX_IMAGE_SIZE ];}
   }
}

CCamera::CCamera(CWnd* pParent /*=NULL*/)
	: CDialog(CCamera::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCamera)
	m_camTop1 = _T("");
	m_camTop2 = _T("");
	m_camTop3 = _T("");
	m_camTop4 = _T("");
	m_bandwidth = _T("");
	m_camWidth1 = _T("");
	m_camHeigh1 = _T("");
	m_camTop5 = _T("");
	m_camWidth5 = _T("");
	m_camHeigh5 = _T("");
	m_camLeft1 = _T("");
	m_camLeft2 = _T("");
	m_camLeft3 = _T("");
	m_camLeft4 = _T("");
	m_camLeft5 = _T("");
	m_bandwidth5 = _T("");
	//}}AFX_DATA_INIT

	ipaddresses[0].octets[0] = ipaddresses[1].octets[0] = ipaddresses[2].octets[0] = ipaddresses[3].octets[0] = ipaddresses[4].octets[0] = 192;
	ipaddresses[0].octets[1] = ipaddresses[1].octets[1] = ipaddresses[2].octets[1] = ipaddresses[3].octets[1] = ipaddresses[4].octets[1] = 168;

	ipaddresses[0].octets[2] = 2;
	ipaddresses[1].octets[2] = 3;
	ipaddresses[2].octets[2] = 4;
	ipaddresses[3].octets[2] = 5;
	ipaddresses[4].octets[2] = 6;

	ipaddresses[0].octets[3] = ipaddresses[1].octets[3] = ipaddresses[2].octets[3] = ipaddresses[3].octets[3] = ipaddresses[4].octets[3] = 1;
	grabThread1 = NULL;
	grabThread2 = NULL;
	grabThread3 = NULL;
	grabThread4 = NULL;
	grabThread5 = NULL;

	for (int i = 0; i < EXPECTED_CAMS; i++)
	{
		readNext[i] = 0;
		writeNext[i] = 1;
	}

	signal1 = CreateSemaphore(NULL, 0, 1, "SIGNAL1");
	signal2 = CreateSemaphore(NULL, 0, 1, "SIGNAL2");
	signal3 = CreateSemaphore(NULL, 0, 1, "SIGNAL3");
	signal4 = CreateSemaphore(NULL, 0, 1, "SIGNAL4");
	signal5 = CreateSemaphore(NULL, 0, 1, "SIGNAL5");
	filestream.open("c:\\out\\log.txt");
	if (filestream.is_open())
		filestream.write("\n", 1);

}


void CCamera::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCamera)
	DDX_Text(pDX, IDC_CAMTOP1, m_camLeft1);
	DDX_Text(pDX, IDC_CAMTOP2, m_camLeft2);
	DDX_Text(pDX, IDC_CAMTOP3, m_camLeft3);
	DDX_Text(pDX, IDC_CAMTOP4, m_camLeft4);
	DDX_Text(pDX, IDC_CAMLEFT1, m_camTop1);
	DDX_Text(pDX, IDC_CAMLEFT2, m_camTop2);
	DDX_Text(pDX, IDC_CAMLEFT3, m_camTop3);
	DDX_Text(pDX, IDC_CAMLEFT4, m_camTop4);
	DDX_Text(pDX, IDC_CAMLEFT5, m_camLeft5);
	DDX_Text(pDX, IDC_CAMTOP5, m_camTop5);
	DDX_Text(pDX, IDC_BANDWIDTH, m_bandwidth);
	DDX_Text(pDX, IDC_CAMWIDTH1, m_camWidth1);
	DDX_Text(pDX, IDC_CAMHEIGHT1, m_camHeigh1);
	DDX_Text(pDX, IDC_CAMWIDTH5, m_camWidth5);
	DDX_Text(pDX, IDC_CAMHEIGHT5, m_camHeigh5);
	DDX_Text(pDX, IDC_BANDWIDTH5, m_bandwidth5);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCamera, CDialog)
	//{{AFX_MSG_MAP(CCamera)
	ON_WM_CLOSE()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CAMTOPUP1, OnCamtopup1)
	ON_BN_CLICKED(IDC_CAMTOPDW1, OnCamtopdw1)
	ON_BN_CLICKED(IDC_CAMTOPUP2, OnCamtopup2)
	ON_BN_CLICKED(IDC_CAMTOPUP3, OnCamtopup3)
	ON_BN_CLICKED(IDC_CAMTOPUP4, OnCamtopup4)
	ON_BN_CLICKED(IDC_CAMTOPDW2, OnCamtopdw2)
	ON_BN_CLICKED(IDC_CAMTOPDW3, OnCamtopdw3)
	ON_BN_CLICKED(IDC_CAMTOPDW4, OnCamtopdw4)
	ON_BN_CLICKED(IDC_RESTART, OnRestart)
	ON_BN_CLICKED(IDC_BANDWIDTHUP1, OnBandwidthup1)
	ON_BN_CLICKED(IDC_CAMWIDTHUP1, OnCamwidthup1)
	ON_BN_CLICKED(IDC_CAMWIDTHDW1, OnCamwidthdw1)
	ON_BN_CLICKED(IDC_CAMHEIGHTUP1, OnCamheightup1)
	ON_BN_CLICKED(IDC_CAMHEIGHTDW1, OnCamheightdw1)
	ON_BN_CLICKED(IDC_BANDWIDTHDW1, OnBandwidthdw1)
	ON_BN_CLICKED(IDC_CAMTOPUP5, OnCamtopup5)
	ON_BN_CLICKED(IDC_CAMTOPDW5, OnCamtopdw5)
	ON_BN_CLICKED(IDC_CAMWIDTHUP5, OnCamwidthup5)
	ON_BN_CLICKED(IDC_CAMWIDTHDW5, OnCamwidthdw5)
	ON_BN_CLICKED(IDC_CAMHEIGHTUP5, OnCamheightup5)
	ON_BN_CLICKED(IDC_CAMHEIGHTDW5, OnCamheightdw5)
	ON_BN_CLICKED(IDC_CAMLEFTUP1, OnCamleftup1)
	ON_BN_CLICKED(IDC_CAMLEFTDW1, OnCamleftdw1)
	ON_BN_CLICKED(IDC_CAMLEFTUP2, OnCamleftup2)
	ON_BN_CLICKED(IDC_CAMLEFTUP3, OnCamleftup3)
	ON_BN_CLICKED(IDC_CAMLEFTUP4, OnCamleftup4)
	ON_BN_CLICKED(IDC_CAMLEFTDW2, OnCamleftdw2)
	ON_BN_CLICKED(IDC_CAMLEFTDW3, OnCamleftdw3)
	ON_BN_CLICKED(IDC_CAMLEFTDW4, OnCamleftdw4)
	ON_BN_CLICKED(IDC_CAMLEFTUP5, OnCamleftup5)
	ON_BN_CLICKED(IDC_CAMLEFTDW5, OnCamleftdw5)
	ON_BN_CLICKED(IDC_BANDWIDTHUP5, OnBandwidthup5)
	ON_BN_CLICKED(IDC_BANDWIDTHDW5, OnBandwidthdw5)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCamera message handlers

BOOL CCamera::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pcamera=this;
	theapp=(CBottleApp*)AfxGetApp();

	frameRate=15.0;

	cam1OK=false;
	cam2OK=false;
	cam3OK=false;
	cam4OK=false;
	cam5OK=false;

	//resWidth = 12;
	resWidth = 8; // Point Grey does not like 12 only 8

	shutterSpeed[0]=theapp->savedShutter;
	shutterSpeed[1]=theapp->savedShutter;
	shutterSpeed[2]=theapp->savedShutter;
	shutterSpeed[3]=theapp->savedShutter;
	shutterSpeed[4]=theapp->savedShutter2;

	gain[0]=17.0;
	gain[1]=17.0;
	gain[2]=17.0;
	gain[3]=17.0;
	gain[4]=17.0;

	LockOut=false;
	LockOut2 = false;
	InitializeCameras();

	if(!theapp->cam5Enable)
	{
		DisableCam5Controls();
		cam_e[4] = nullptr;
	}
	else
	{
		cam_e[4] = &cam_e5;
	}

	cam_e[0] = &cam_e1;
	cam_e[1] = &cam_e2;
	cam_e[2] = &cam_e3;
	cam_e[3] = &cam_e4;


	//	Disable Width and Height Control until further notice
	DisableAdjustableHtWd();

	UpdateDisplay();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


FlyCapture2::Error CCamera::SetImageSettings(int index)
{
	GigEImageSettings		imgSettings;
	index++;
	imgSettings.offsetX = theapp->camLeft[index].load();
	imgSettings.offsetY = theapp->camTop[index].load();
	imgSettings.height = theapp->camHeight[index].load();
	imgSettings.width = theapp->camWidth[index].load();

	CString str;
	str.Format("Camera %d Set left %d, top %d width %d height %d", index, imgSettings.offsetX, imgSettings.offsetY, imgSettings.width, imgSettings.height);
	index--;
	imgSettings.pixelFormat = PIXEL_FORMAT_RAW8; //PIXEL_FORMAT_MONO8//PIXEL_FORMAT_422YUV8//PIXEL_FORMAT_RAW8
	FlyCapture2::Error error = cam_e[index]->SetGigEImageSettings(&imgSettings);
	return error;
}


void CCamera::initBitmapStruct( int iCols, int iRows )
{
   BITMAPINFOHEADER* pheader = &m_bitmapInfo.bmiHeader;
   
   // Initialize permanent data in the bitmapinfo header.
   pheader->biSize          = sizeof( BITMAPINFOHEADER );
   pheader->biPlanes        = 1;
   pheader->biCompression   = BI_RGB;
   pheader->biXPelsPerMeter = 100;
   pheader->biYPelsPerMeter = 100;
   pheader->biClrUsed       = 0;
   pheader->biClrImportant  = 0;
   
   // Set a default window size.
   pheader->biWidth    = iCols;
   pheader->biHeight   = -iRows;
   pheader->biBitCount = 32;
   
   m_bitmapInfo.bmiHeader.biSizeImage = 
      pheader->biWidth * pheader->biHeight * ( pheader->biBitCount / 8 );
}

void CCamera::DisableCam5Controls()
{
	GetDlgItem(IDC_CAMTOPUP5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMTOPDW5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMLEFTUP5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMLEFTDW5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BANDWIDTHUP5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BANDWIDTHDW5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMTOP5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMLEFT5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BANDWIDTH5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAM5GROUPBOX)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAM5LEFTLBL)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAM5TOPLBL)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAM5BANDLBL)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAM5DEFLLBL)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAM5DEFTLBL)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAM5DEFBLBL)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAM5W)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAM5H)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_DEFAULTCAM5W)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_DEFAULTCAM5H)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMWIDTHUP5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMWIDTHDW5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMHEIGHTUP5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMHEIGHTDW5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMWIDTH5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMHEIGHT5)->ShowWindow(SW_HIDE);
}

void CCamera::DisableAdjustableHtWd()
{
	//	Need to set dialog height and width at some point
	GetDlgItem(IDC_CAMWIDTH1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMHEIGHT1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMWIDTH5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMHEIGHT5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMWIDTHUP1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMWIDTHDW1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMHEIGHTUP1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMHEIGHTDW1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMWIDTHUP5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMWIDTHDW5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMHEIGHTUP5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMHEIGHTDW5)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMWS)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAMHS)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAM5W)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAM5H)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_DEFAULTCAMWS)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_DEFAULTCAMHS)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_DEFAULTCAM5W)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_DEFAULTCAM5H)->ShowWindow(SW_HIDE);
}

void CCamera::manbusCallback( void* pparam, int iMessage, unsigned long ulParam )
{
   CCamera* pDoc = (CCamera*)pparam;
   
   //ASSERT( pDoc->m_flyCaptureContext != NULL );
   
   const unsigned long ulSerialNumber = ulParam;
   
   CString csMsg;

   //if(!pDoc->LockOut)
   //{

	   //pDoc->LockOut=true;
	//	pDoc->theapp->lostCam+=1;
		//pDoc->StopCams();
	  
		//pDoc->SetTimer(1,1,NULL);
		//pDoc->SetTimer(2,10000,NULL);//10 seconds

   //}
   
   switch( iMessage )
   {
		case 2:
			if(!pDoc->LockOut)
			{
				pDoc->LockOut=true;
				pDoc->theapp->lostCam+=1;
				pDoc->StopCams();
			}
     
			break; 
		
		case 1:
			
			pDoc->SetTimer(1,1,NULL);
			pDoc->SetTimer(2,10000,NULL);//10 seconds

			break;

		case 3:
			pDoc->SetTimer(1,1,NULL);
			pDoc->SetTimer(2,10000,NULL);//10 seconds
			break;
   }
      
  // case FLYCAPTURE_MESSAGE_DEVICE_ARRIVAL:
	  //AfxMessageBox("FLYCAPTURE_MESSAGE_DEVICE_ARRIVAL");
	  
//pDoc->InitializeCameras();
	  //pDoc->ReStartCams();
	 // break;
      
   //case FLYCAPTURE_MESSAGE_DEVICE_REMOVAL:

	//	AfxMessageBox("FLYCAPTURE_MESSAGE_DEVICE_REMOVAL");
      //if( ulSerialNumber == pDoc->m_cameraInfo.SerialNumber )
      //{
         //
         // The camera we are currently using has been unplugged.
         //
         //pDoc->m_bContinueGrabThread = false;
  	     
  	     //::PostMessage(AfxGetApp()->m_pMainWnd->GetSafeHwnd(), FLYCAPTURE_WINDOWS_MESSAGE_DEVICE_REMOVAL, NULL, NULL);
         //AfxMessageBox("FLYCAPTURE_WINDOWS_MESSAGE_DEVICE_REMOVAL");
         //
         // Bring up the camera selection dialog again.
         //
  	     //::PostMessage(AfxGetApp()->m_pMainWnd->GetSafeHwnd(), WM_COMMAND, ID_FILE_NEW, NULL);
     // }
      //else
      //{
  	     //::PostMessage(AfxGetApp()->m_pMainWnd->GetSafeHwnd(), FLYCAPTURE_WINDOWS_MESSAGE_DEVICE_REMOVAL, NULL, NULL);
			//AfxMessageBox("FLYCAPTURE_WINDOWS_MESSAGE_DEVICE_REMOVAL");
      //}
      //break;
      
   //default:

	//AfxMessageBox("Camera connection problem");
      //PGR_ERROR_MESSAGE2( 
         //"CFlyCapDoc::busCallback(): Software version mismatch! "
         //"Please reinstall and recompile.",
         //CRITICAL_MESSAGES );
      //ASSERT( FALSE );
   //}
}


void CCamera::busCallback( void* pparam, int iMessage, unsigned long ulParam )
{
   CCamera* pDoc = (CCamera*)pparam;
   
   //ASSERT( pDoc->m_flyCaptureContext != NULL );
   
   const unsigned long ulSerialNumber = ulParam;
   
   CString csMsg;

   //if(!pDoc->LockOut)
   //{

	   //pDoc->LockOut=true;
	//	pDoc->theapp->lostCam+=1;
		//pDoc->StopCams();
	  
		//pDoc->SetTimer(1,1,NULL);
		//pDoc->SetTimer(2,10000,NULL);//10 seconds

   //}
   
   switch( iMessage )
   {
		case 2:
			if(!pDoc->LockOut)
			{
				pDoc->LockOut=true;
				pDoc->theapp->lostCam+=1;
				pDoc->StopCams();
			}
     
			break; 
		
		case 1:
			
			pDoc->SetTimer(1,1,NULL);
			pDoc->SetTimer(2,10000,NULL);//10 seconds

			break;

		case 3:
			pDoc->SetTimer(1,1,NULL);
			pDoc->SetTimer(2,10000,NULL);//10 seconds
			break;
   }
      

}

void CCamera::StopCams()
{
	m_bContinueGrabThread = false;

	Sleep(100);
}

/*
//	Returns true if Camera started successfully
bool CCamera::StartCamera(int camNo, long camSerNo, FlyCaptureInfoEx& camInfo)
{
	FlyCaptureError   error;
	m_bContinueGrabThread=true;
	int contextNo = camNo - 1;
	float bandwidth = camNo == 5 ? bandwidth5 : bandwidth;
	float sSpeed = camNo == 5 ? shutterSpeed5 : shutterSpeed;
	bool camStarted = false;

	for(initTry=1; initTry<8; initTry++)
	{
		error = flycaptureCreateContext( &g_arContext[ contextNo ] );
		error = flycaptureInitializeFromSerialNumber( g_arContext[ contextNo ], camSerNo );

		// Reset the camera to default factory settings by asserting bit 0
		error = flycaptureSetCameraRegister( g_arContext[ contextNo ], INITIALIZE, 0x80000000 );
		
		//##Sleever Tracker#####
		if(camNo == 5 && theapp->sleeverEnable && !restart)
		{
			//Setting GPIO#2 to Input mode  
			unsigned long _gpioState=0; //GPIO Pin2
			error =flycaptureGetCameraRegister( g_arContext[ contextNo ], 0X1130, &_gpioState );

			if(error != FLYCAPTURE_OK)	{AfxMessageBox("GetReg");}

			_gpioState=0x80000001;
			error = flycaptureSetCameraRegister( g_arContext[ contextNo ], 0X1130, _gpioState );

			if(error !=FLYCAPTURE_OK)	{AfxMessageBox("SetReg");}

			unsigned long _frameInfo=0; //Setting the register 0x12f8 with FrameNo & GPIO status set
			error =flycaptureGetCameraRegister( g_arContext[ contextNo ], 0X12f8, &_frameInfo );
			_frameInfo=0x80000141;
			error = flycaptureSetCameraRegister( g_arContext[ contextNo ], 0X12f8, _frameInfo );
			if(error !=FLYCAPTURE_OK)
			{AfxMessageBox("Embedded Image Info Setting Failed");}
		}
		//##Sleever Tracker########
																					//OnePush //OnOff //Auto //Value
		error = flycaptureSetCameraAbsPropertyEx(g_arContext[ contextNo ],FLYCAPTURE_SHUTTER,false,true,false,shutterSpeed );
		error = flycaptureSetCameraProperty(g_arContext[ contextNo ],FLYCAPTURE_SHARPNESS,false,true,500 );
		error = flycaptureSetCameraAbsPropertyEx(g_arContext[ contextNo ],FLYCAPTURE_AUTO_EXPOSURE,false,true,false,1.0 );
		error = flycaptureSetCameraAbsPropertyEx(g_arContext[ contextNo ],FLYCAPTURE_SATURATION,false,true,false, 100 );
		error = flycaptureSetCameraAbsPropertyEx(g_arContext[ contextNo ],FLYCAPTURE_GAIN,false,true,false,gain );
		error = flycaptureSetCameraPropertyEx(g_arContext[ contextNo ],FLYCAPTURE_WHITE_BALANCE,false,true,false,626,691);//630,500 );						Cam 1
		error = flycaptureSetCameraPropertyEx(g_arContext[ 4 ],FLYCAPTURE_WHITE_BALANCE,false,true,false,650,500);//theapp->redBal ,theapp->blueBal  );		Cam 5
		//error = flycaptureSetCameraProperty(g_arContext[ contextNo ],FLYCAPTURE_FRAME_RATE,false,true,frameRate );
		error = flycaptureGetCameraInfo( g_arContext[ contextNo ], &camInfo );	//	Camera Info???????????
   
		error = flycaptureStartCustomImage(
		g_arContext[ contextNo ],
		0,//unsigned int uiMode,
		theapp->camLeft[camNo],
		theapp->camTop[camNo],
		theapp->camWidth[camNo],
		theapp->camHeight[camNo],
		bandwidth,//float fBandwidth,
		FLYCAPTURE_RAW8);//FlyCapturePixelFormat format )
   
		cam=camNo;
		if(!restart)
		{
			Status status;
			status.DoModal();
		}

		if(error!=FLYCAPTURE_OK)
		{
			//AfxMessageBox("failed to start camera 1");
			theapp->camsPresent=false;
		}
		else
		{
			initTry=10;
			theapp->camsPresent=true;
			camStarted = true;
		}
	}

	return camStarted;
}
*/

void CCamera::StartCams1to5(bool restart)
{
	//CameraGUIError    guierror;
	m_bContinueGrabThread=true;

	unsigned long cam1SerialNumber=theapp->CamSer1;//8100339;//test 4
	unsigned long cam2SerialNumber=theapp->CamSer2;//8210086;//test 1
	unsigned long cam3SerialNumber=theapp->CamSer3;//8100333;//test 3
	unsigned long cam4SerialNumber=theapp->CamSer4;//8210085;//test 2
	unsigned long cam5SerialNumber=theapp->CamSer5;//8210117;//test 5

	//	In the future this will need to be reworked to accomodate various widths and heights
	m_cams1to4Width = CroppedImageWidth;
	m_cams1to4Height = CroppedImageHeight;
	m_cams1to4XOffset = CroppedImageXOffset;
	m_cams1to4YOffset = CroppedImageYOffset;

	theapp->camHeight[5] = CroppedImageHeight;
	theapp->camWidth[5] = CroppedImageWidth;

	if(theapp->waistAvailable)
	{
		m_cams1to4Width = FullImageWidth;
		m_cams1to4Height = FullImageHeight;
		m_cams1to4YOffset = FullImageYOffset;
	}

	theapp->camHeight[0] =
	theapp->camHeight[1] =
	theapp->camHeight[2] = 
	theapp->camHeight[3] = 
	theapp->camHeight[4] = m_cams1to4Height;

	theapp->camWidth[0] =
	theapp->camWidth[1] =
	theapp->camWidth[2] = 
	theapp->camWidth[3] = 
	theapp->camWidth[4] = m_cams1to4Width;

	//	Check offsets to see if within range
	SetOffsets();

	CameraInfo		camInfo[128];
	unsigned int	numCamInfo = 128;
	unsigned int	numCameras;
	GigEImageSettingsInfo	imgSettingsInfo;
	GigEImageSettings		imgSettings;
	busMgr = new BusManager();


	FlyCapture2::Error error;

	//TODO: Maybe not needed
	/*int m_iCameraBusIndex = -1;   

	initBitmapStruct( _DEFAULT_WINDOW_X, _DEFAULT_WINDOW_Y );
	resizeProcessedImage1( _MIN_BGR_BUFFER_SIZE );
	resizeProcessedImage2( _MIN_BGR_BUFFER_SIZE );
	resizeProcessedImage3( _MIN_BGR_BUFFER_SIZE );
	resizeProcessedImage4( _MIN_BGR_BUFFER_SIZE );

	memset( m_imageProcessed1.pData, 0x0, _MIN_BGR_BUFFER_SIZE );
	memset( m_imageProcessed2.pData, 0x0, _MIN_BGR_BUFFER_SIZE );
	memset( m_imageProcessed3.pData, 0x0, _MIN_BGR_BUFFER_SIZE );
    memset( m_imageProcessed4.pData, 0x0, _MIN_BGR_BUFFER_SIZE );

	if(theapp->cam5Enable) 
	{
		resizeProcessedImage5( _MIN_BGR_BUFFER_SIZE );
		memset( m_imageProcessed5.pData, 0x0, _MIN_BGR_BUFFER_SIZE );
	}
*/

	error = FlyCapture2::BusManager::DiscoverGigECameras(camInfo, &numCamInfo);

	error = busMgr->GetNumOfCameras(&numCameras);

	if (error == PGRERROR_OK)
	{
		handles[0] = CreateEvent(0, true, false, "Event 1");
		handles[1] = CreateEvent(0, true, false, "Event 2");
		handles[2] = CreateEvent(0, true, false, "Event 3");
		handles[3] = CreateEvent(0, true, false, "Event 4");
		handles[4] = CreateEvent(0, true, false, "Event 5");

		m_bRestart = true;
		m_bContinueGrabThread = true;
		int index = 0;

		grabThread1 = new std::thread(threadGrabImage, this);
	//	SetThreadDescription((HANDLE)grabThread1->native_handle(), L"Grab Thread 1");
		SetThreadPriority((HANDLE)grabThread1->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.
		inspectThread1 = new std::thread(threadInspect1, this);
		//SetThreadDescription((HANDLE)inspectThread1->native_handle(), L"Inspect Thread 1");
		SetThreadPriority((HANDLE)inspectThread1->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.
		Status status;	status.DoModal();
		grabThread1->detach();
		inspectThread1->detach();
		cam++;

		grabThread2 = new std::thread(threadGrabImage2, this);
	//	SetThreadDescription((HANDLE)grabThread2->native_handle(), L"Grab Thread 2");
		SetThreadPriority((void*)grabThread2->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.
		inspectThread2 = new std::thread(threadInspect2, this);
	//	SetThreadDescription((HANDLE)inspectThread2->native_handle(), L"Inspect Thread 2");
		SetThreadPriority((HANDLE)inspectThread2->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.
		status;	status.DoModal();
		grabThread2->detach();
		inspectThread2->detach();
		cam++;

		grabThread3 = new std::thread(threadGrabImage3, this);
	//	SetThreadDescription((HANDLE)grabThread3->native_handle(), L"Grab Thread 3");
		SetThreadPriority((HANDLE)grabThread3->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.
		inspectThread3 = new std::thread(threadInspect3, this);
	//	SetThreadDescription((HANDLE)inspectThread3->native_handle(), L"Inspect Thread 3");
		SetThreadPriority((HANDLE)inspectThread3->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.

		status;	status.DoModal();
		grabThread3->detach();
		inspectThread3->detach();
		cam++;

		grabThread4 = new std::thread(threadGrabImage4, this);
		//	SetThreadDescription((HANDLE)grabThread4->native_handle(), L"Grab Thread 4");
		SetThreadPriority((HANDLE)grabThread4->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.
		inspectThread4 = new std::thread(threadInspect4, this);
		//	SetThreadDescription((HANDLE)inspectThread4->native_handle(), L"Inspect Thread 4");
		SetThreadPriority((HANDLE)inspectThread4->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.
		status;	status.DoModal();
		grabThread4->detach();
		inspectThread4->detach();
		cam++;

		if (theapp->cam5Enable)
		{
			grabThread5 = new std::thread(threadGrabImage5, this);
			//	SetThreadDescription((HANDLE)grabThread5->native_handle(), L"Grab Thread 5");
			SetThreadPriority((HANDLE)grabThread5->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.
			inspectThread5 = new std::thread(threadInspect5, this);
			//	SetThreadDescr                                                                                                                                                                                                    iption((HANDLE)inspectThread5->native_handle(), L"Inspect Thread 5");
			SetThreadPriority((HANDLE)inspectThread5->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.
			status;	status.DoModal();
			cam++;
			grabThread5->detach();
			inspectThread5->detach();
		}
	}
}

FlyCapture2::Error CCamera::SetTriggerMode(FlyCapture2::GigECamera* cam, bool value)
{
	TriggerMode triggerMode;
	FlyCapture2::Error error = cam->GetTriggerMode(&triggerMode);

	if (error != PGRERROR_OK)
	{
		error.PrintErrorTrace(); return error;
	}

	// Set camera to trigger mode
//	triggerMode.onOff		=	false;
	triggerMode.onOff = value;
	triggerMode.polarity = 1; // rising edge
	triggerMode.source = 2; //2-External, 7 Internal
	TriggerDelay delay;

	cam->GetTriggerDelay(&delay);
	delay.onOff = false;
	delay.absValue = 0.0f;
	cam->SetTriggerDelay(&delay);


	error = cam->SetTriggerMode(&triggerMode);

	if (error != PGRERROR_OK)
	{
		error.PrintErrorTrace();	AfxMessageBox("Failed to SetTriggerMode()"); return error;
	}
	return error;
}

FlyCapture2::Error CCamera::SetCameraPropertyAuto(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, bool autoOn)
{
	FlyCapture2::Error   error;
	Property pProp;
	pProp.present = true;
	pProp.onOff = true;
	pProp.autoManualMode = autoOn;
	pProp.type = type;
	error = cam->SetProperty(&pProp, false);
	return error;

}
FlyCapture2::Error CCamera::SetCameraProperty(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, float value)
{
	FlyCapture2::Error   error;
	Property pProp;
	pProp.present = true;
	pProp.absControl = true;
	pProp.onePush = false;
	pProp.onOff = true;
	pProp.autoManualMode = false;
	pProp.type = type;
	pProp.absValue = value;
	error = cam->SetProperty(&pProp, false);
	return error;
}

FlyCapture2::Error CCamera::SetCameraPropertyNoAbs(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, float value)
{
	FlyCapture2::Error   error;
	Property pProp;
	pProp.present = true;
	pProp.absControl = false;
	pProp.onePush = false;
	pProp.onOff = true;
	pProp.autoManualMode = false;
	pProp.type = type;
	pProp.absValue = value;
	error = cam->SetProperty(&pProp, false);
	return error;
}

FlyCapture2::Error CCamera::SetCameraProperty(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, unsigned int valueA, unsigned int valueB)
{
	FlyCapture2::Error   error;
	Property pProp;
	pProp.present = 1;
	pProp.absControl = false;
	pProp.onePush = false;
	pProp.onOff = true;
	pProp.autoManualMode = false;
	pProp.type = type;
	pProp.valueA = valueA;
	pProp.valueB = valueB;
	error = cam->SetProperty(&pProp, false);
	return error;
}

FlyCapture2::Error CCamera::GetCameraProperty(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, int &valueA, int &valueB)
{
	FlyCapture2::Error   error;
	Property pProp;
	pProp.type = type;
	pProp.onePush = false;
	pProp.onOff = true;
	error = cam->GetProperty(&pProp);
	valueA = pProp.valueA;
	valueB = pProp.valueB;
	return error;
}

FlyCapture2::Error CCamera::CameraSetup(int index, FlyCapture2::BusManager* busMgr, int& camOK)
{
	FlyCapture2::Error error;
	PGRGuid guid;
	InterfaceType interfaceType;
	const unsigned int k_camInitialize = INITIALIZE;
	const unsigned int k_regVal = 0x80000000;

	cam = index + 1;

	error = busMgr->GetCameraFromIPAddress(ipaddresses[index], &guid);
	if (error != PGRERROR_OK) { camOK = 3;  error.PrintErrorTrace(); }

	//getting type of camera's interface
	error = busMgr->GetInterfaceTypeFromGuid(&guid, &interfaceType);
	if (error != PGRERROR_OK) { camOK = 4; error.PrintErrorTrace(); }

	if (interfaceType == INTERFACE_GIGE)
	{
		error = cam_e[index]->Connect(&guid);
		if (error != PGRERROR_OK) { camOK = 5; error.PrintErrorTrace(); }
		error = cam_e[index]->WriteRegister(k_camInitialize, k_regVal);
		if (error != PGRERROR_OK) { camOK = 5; error.PrintErrorTrace(); }

		//Set register 954 to 1 to kill camera's heartbeat
		cam_e[index]->WriteGVCPRegister(0x954, 0x1, false);

		CameraInfo camInfo;
		error = cam_e[index]->GetCameraInfo(&camInfo);
		if (error != PGRERROR_OK) { camOK = 6; error.PrintErrorTrace(); }

		unsigned int numStreamChannels = 0;
		error = cam_e[index]->GetNumStreamChannels(&numStreamChannels);
		if (error != PGRERROR_OK) { camOK = 7; error.PrintErrorTrace(); }

		for (unsigned int i = 0; i < numStreamChannels; i++)
		{
			GigEStreamChannel streamChannel;
			error = cam_e[index]->GetGigEStreamChannelInfo(i, &streamChannel);

			if (error != PGRERROR_OK) { camOK = 8; error.PrintErrorTrace(); }
		}

		imageSettings.lock();
		error = SetCameraProperty(cam_e[index], GAIN, gain[index]);
		if (error != PGRERROR_OK) { camOK = 9; error.PrintErrorTrace(); 
		AfxMessageBox("Failed to Set GAIN"); }

		error = SetCameraProperty(cam_e[index], SHUTTER, shutterSpeed[index]);
		if (error != PGRERROR_OK) { camOK = 10; error.PrintErrorTrace(); 
		AfxMessageBox("Failed to Set SHUTTERSPEED"); }
		imageSettings.unlock();

		error = SetCameraProperty(cam_e[index], BRIGHTNESS, 1.0);
		if (error != PGRERROR_OK) { camOK = 11; error.PrintErrorTrace(); AfxMessageBox("Failed to Set BRIGHTNESS"); }

		error = SetCameraProperty(cam_e[index], HUE, 1.5);
		if (error != PGRERROR_OK) { camOK = 12; error.PrintErrorTrace(); AfxMessageBox("Failed to Set HUE"); }

		error = SetCameraProperty(cam_e[index], SATURATION, 100);
		if (error != PGRERROR_OK) { camOK = 13; error.PrintErrorTrace(); AfxMessageBox("Failed to Set SATURATION"); }

		error = SetCameraPropertyNoAbs(cam_e[index], SHARPNESS, 500);
		if (error != PGRERROR_OK) { camOK = 14; error.PrintErrorTrace(); AfxMessageBox("Failed to Set SHARPNESS"); }

		error = SetCameraProperty(cam_e[index], AUTO_EXPOSURE, 1.0);
		if (error != PGRERROR_OK) { camOK = 15; error.PrintErrorTrace(); AfxMessageBox("Failed to Set AUTO_EXPOSURE"); }

		if (index == 0)
		{
			error = SetCameraProperty(cam_e[index], WHITE_BALANCE, theapp->whitebalance1Red, theapp->whitebalance1Blue);
		}
		else if (index == 1)
		{
			error = SetCameraProperty(cam_e[index], WHITE_BALANCE, theapp->whitebalance2Red, theapp->whitebalance2Blue);
		}
		else if (index == 2)
		{
			error = SetCameraProperty(cam_e[index], WHITE_BALANCE, theapp->whitebalance3Red, theapp->whitebalance3Blue);
		}


		//int wbr, wbb;
		//error = GetCameraProperty(cam_e[index], WHITE_BALANCE, wbr, wbb);
		//if (index == 0)
		//{
		//	theapp->whitebalance1Red.store(wbr);
		//	theapp->whitebalance1Blue.store(wbb);
		//}
		//else if (index == 1)
		//{
		//	theapp->whitebalance2Red.store(wbr);
		//	theapp->whitebalance2Blue.store(wbb);
		//}
		//else if (index == 2)
		//{
		//	theapp->whitebalance3Red.store(wbr);
		//	theapp->whitebalance3Blue.store(wbb);
		//}
		if (error != PGRERROR_OK) { camOK = 16; error.PrintErrorTrace(); AfxMessageBox("Failed to Set WHITE_BALANCE"); }
	}
	return error;
}


void CCamera::StartImageGrabThreads()
{
	m_bContinueGrabThread = true;

	if(cam1OK){thread1=AfxBeginThread( threadGrabImage, this ); SetThreadPriority(threadGrabImage, THREAD_PRIORITY_HIGHEST);}
	if(cam2OK){thread2=AfxBeginThread( threadGrabImage2, this );SetThreadPriority(threadGrabImage2, THREAD_PRIORITY_HIGHEST);}
	if(cam3OK){thread3=AfxBeginThread( threadGrabImage3, this );SetThreadPriority(threadGrabImage3, THREAD_PRIORITY_HIGHEST);}
	if(cam4OK){thread4=AfxBeginThread( threadGrabImage4, this );SetThreadPriority(threadGrabImage4, THREAD_PRIORITY_HIGHEST);}
	if(theapp->cam5Enable && cam5OK){thread5=AfxBeginThread( threadGrabImage5, this );SetThreadPriority(threadGrabImage5, THREAD_PRIORITY_HIGHEST);}

	if(!cam1OK || !cam2OK || !cam3OK || !cam4OK )
	{theapp->camsPresent=false; m_bContinueGrabThread = false;}
}

int CCamera::StartCams()
{
	StartCams1to5(false);
	return TRUE;
}

int CCamera::ReStartCams()
{
	m_bContinueGrabThread = true;

	return TRUE;
}

void CCamera::SetOffsets()
{
	int MAX_CAM_WIDTH = FullImageWidth;
	int MAX_CAM_HEIGHT = FullImageHeight;

	for(int i = 1; i <= 5; i++)
	{
		if((theapp->camLeft[i] + theapp->camWidth[i]) > MAX_CAM_WIDTH)
		{
			theapp->camLeft[i] = 0;
		}

		if((theapp->camTop[i] + theapp->camHeight[i]) > MAX_CAM_HEIGHT)
		{
			theapp->camTop[i] = (MAX_CAM_HEIGHT - theapp->camHeight[i]);
		}
	}
}

UINT CCamera::doGrabLoop()
{

	int camOK = 0;
	FlyCapture2::Error error;
	for (int i = 1; i < 6; i++)
	{
		initTry = i;
		error = CameraSetup(0, busMgr, camOK);
		if (error == PGRERROR_OK)
		{
		//	error = SetImageSettings(0);
			if (error == PGRERROR_OK)
			{
				break;
			}
		}
	}

	SetTriggerMode(cam_e[0]);
	doContinousGrab(cam_e[0], theapp->pframeSub->gotPicC1, theapp->pframeSub->countC1, m_iProcessedBufferSize[0], 0);
	return 0;
}

//Call back function from point gray camera driver. Called when state of firewire bus changes, such as camera being disconnected,
//bus reset, image grabbed, register read/written.
//void CCamera::busCallback( void* pparam, int iMessage, unsigned long ulParam )
void CCamera::busRemovalCallback(void* pparam, int serial)
{
	CCamera* pDoc = (CCamera*)pparam;
	if (pDoc->LockOut == false)
	{
		pDoc->LockOut = true;
		pDoc->theapp->lostCam += 1;
		pDoc->StopCams();
	}
}


UINT CCamera::threadInspect1(void* pparam)
{
	CCamera* pNewCamera = (CCamera*)pparam;
	pNewCamera->doInspect1();
	return 0;
}

UINT CCamera::threadInspect2(void* pparam)
{
	CCamera* pNewCamera = (CCamera*)pparam;
	pNewCamera->doInspect2();
	return 0;
}

UINT CCamera::threadInspect3(void* pparam)
{
	CCamera* pNewCamera = (CCamera*)pparam;
	pNewCamera->doInspect3();
	return 0;
}

UINT CCamera::threadInspect4(void* pparam)
{
	CCamera* pNewCamera = (CCamera*)pparam;
	pNewCamera->doInspect4();
	return 0;
}

UINT CCamera::threadInspect5(void* pparam)
{
	CCamera* pNewCamera = (CCamera*)pparam;
	pNewCamera->doInspect5();
	return 0;
}


void CCamera::doInspect1()
{
	while (m_bContinueGrabThread.load())
	{
		WaitForSingleObject(signal1, INFINITE);
		theapp->pframeSub->m_pxaxis->Display(NULL);
	}
}

void CCamera::doInspect2()
{
	while (m_bContinueGrabThread.load())
	{
		WaitForSingleObject(signal2, INFINITE);
		theapp->pframeSub->m_pxaxis->Display2(NULL);
	}
}
void CCamera::doInspect3()
{
	while (m_bContinueGrabThread.load())
	{
		WaitForSingleObject(signal3, INFINITE);
		theapp->pframeSub->m_pxaxis->Display3(NULL);
	}
}

void CCamera::doInspect4()
{
	while (m_bContinueGrabThread.load())
	{
		WaitForSingleObject(signal4, INFINITE);
		theapp->pframeSub->m_pxaxis->Display4(NULL);
	}
}


void CCamera::doInspect5()
{
	while (m_bContinueGrabThread.load())
	{
		WaitForSingleObject(signal5, INFINITE);
		theapp->pframeSub->m_pxaxis->Display5(NULL);
	}
}




FlyCapture2::Error CCamera::doContinousGrab(FlyCapture2::GigECamera* cam, bool &gotPic, std::atomic<int>& count, int& bufferSize, int index)
{
	FlyCapture2::Error error;
	Image rawImage;
	int camOK = 0;
	FC2Config config;
	HANDLE* signal;
	char str[100];
	unsigned int expectedFrameNumber = 0, frameNumber = 0;
	bool skip;

	while (m_bRestart.load())
	{
		imageSettings.lock();

		m_iProcessedBufferSize[index] = 0;
		memset(&m_imageProcessed[index][0], 0x0, sizeof(m_imageProcessed[index][0]));
		memset(&m_imageProcessed[index][1], 0x0, sizeof(m_imageProcessed[index][1]));

		if (index == 0)
			m_imageProcessed[index][0] = m_imageProcessed[index][1] = &m_imageProcessed1;
		else if (index == 1)
			m_imageProcessed[index][0] = m_imageProcessed[index][1] = &m_imageProcessed2;
		else if (index == 2)
			m_imageProcessed[index][0] = m_imageProcessed[index][1] = &m_imageProcessed3;
		else if(index == 3)
			m_imageProcessed[index][0] = m_imageProcessed[index][1] = &m_imageProcessed4;
		else
			m_imageProcessed[index][0] = m_imageProcessed[index][1] = &m_imageProcessed5;


		initBitmapStruct(_DEFAULT_WINDOW_X, _DEFAULT_WINDOW_Y);
		resizeProcessedImage(_MIN_BGR_ROWS, _MIN_BGR_COLS, m_imageProcessed[index][0], m_iProcessedBufferSize[index]);
		resizeProcessedImage(_MIN_BGR_ROWS, _MIN_BGR_COLS, m_imageProcessed[index][1], m_iProcessedBufferSize[index]);

		SetImageSettings(index);
		Mode giGeImgMode = MODE_0; //MODE_0:1288x694 //MODE_1:644x482
		error = cam->SetGigEImagingMode(giGeImgMode);

		if (error != PGRERROR_OK) { camOK = 19; error.PrintErrorTrace(); /*AfxMessageBox("Failed %d", camOK);*/ }

		//*******************************************************************
		error = SetCameraProperty(cam_e[index], GAIN, gain[index]);
		if (error != PGRERROR_OK) { camOK = 9; error.PrintErrorTrace(); 
		AfxMessageBox("Failed to Set GAIN"); }

		error = SetCameraProperty(cam_e[index], SHUTTER, shutterSpeed[index]);
		if (error != PGRERROR_OK) { camOK = 10; error.PrintErrorTrace(); 
		AfxMessageBox("Failed to Set SHUTTERSPEED"); }

		TriggerMode triggerMode;
		error = cam->GetTriggerMode(&triggerMode);

		if (error != PGRERROR_OK)
		{
			error.PrintErrorTrace(); 
			imageSettings.unlock();
			return error;
		}

		SetTriggerMode(cam, true);
		if (error != PGRERROR_OK) { error.PrintErrorTrace();	AfxMessageBox("Failed to SetTriggerMode()"); return error; }


		if (index == 0)
		{
			SetCameraProperty(cam, WHITE_BALANCE, this->theapp->whitebalance1Red.load(), theapp->whitebalance1Blue.load());
			signal = &signal1;
		}
		else if (index == 1)
		{
			SetCameraProperty(cam, WHITE_BALANCE, theapp->whitebalance2Red.load(), theapp->whitebalance2Blue.load());
			signal = &signal2;
		}
		else if (index == 2)
		{
			SetCameraProperty(cam, WHITE_BALANCE, theapp->whitebalance3Red.load(), theapp->whitebalance3Blue.load());
			signal = &signal3;
		}
		else if (index == 3)
		{
			SetCameraProperty(cam, WHITE_BALANCE, theapp->whitebalance4Red.load(), theapp->whitebalance4Blue.load());
			signal = &signal4;
		}
		else if (index == 4)
		{
			SetCameraProperty(cam, WHITE_BALANCE, theapp->whitebalance5Red.load(), theapp->whitebalance5Blue.load());
			signal = &signal5;
		}



		error = cam->GetConfiguration(&config);
		if (error != PGRERROR_OK) { error.PrintErrorTrace(); return error; }

		// Set the grab timeout to INFINITE
		config.grabTimeout = -1;
//		config.grabMode = BUFFER_FRAMES;
//		config.numBuffers = 20;
	
	// Set the camera configuration
		error = cam->SetConfiguration(&config);
		if (error != PGRERROR_OK) { error.PrintErrorTrace(); AfxMessageBox("Failed to SetConfiguration()"); return error; }

		// Set the bus notification callback so the user-level app can know when
		// the camera is unplugged, etc.
		CallbackHandle handle;
		error = busMgr->RegisterCallback((BusEventCallback)busRemovalCallback, BusCallbackType::REMOVAL, 0, &handle);
		if (error != PGRERROR_OK) AfxMessageBox("failed to create bus callback");

		imageSettings.unlock();
		error = cam->StartCapture();

		if (camOK == 0) { theapp->camsPresent = true; }
		else { theapp->camsPresent = false; }

		SetEvent(handles[index]);

		while (m_bContinueGrabThread.load())
		{
			//			WaitForMultipleObjects(3, handles, true, INFINITE);
						//////////////////////////////		
						// Grab the raw image.
						//
			memset(str, '\0', 100);
			sprintf(str, "Before grab from cam %d to write to buffer %d count %d\n", index, writeNext[index].load(), count.load());
	//		filestream.write(str, strlen(str));
			filestream.flush();
			
			if (expectedFrameNumber == 0 || frameNumber == expectedFrameNumber)
			{
				memset(str, '\0', 100);
				sprintf(str, "Cam %d Frame OK.  Expected %d got %d\n", index, expectedFrameNumber, frameNumber);
				filestream.write(str, strlen(str));
				filestream.flush();

			}
			else if (frameNumber < expectedFrameNumber)
			{
				memset(str, '\0', 100);
				sprintf(str, "Cam %d Frame TOO LOW!  Expected %d got %d\n", index, expectedFrameNumber, frameNumber);
				filestream.write(str, strlen(str));
				filestream.flush();
			}
			else if (frameNumber > expectedFrameNumber)
			{
				memset(str, '\0', 100);
				sprintf(str, "Cam %d SKIPPED FRAME!  Expected %d got %d\n", index, expectedFrameNumber, frameNumber);
				filestream.write(str, strlen(str));
				filestream.flush();
				skip = true;
				Sleep(30);

			}
			else
			{
				memset(str, '\0', 100);
				sprintf(str, "Weird error on cam %d\n", index);
				filestream.write(str, strlen(str));
				filestream.flush();

			}
			expectedFrameNumber++;


			error = cam->RetrieveBuffer(&rawImage);
			frameNumber = rawImage.GetMetadata().embeddedFrameCounter;
			if (expectedFrameNumber == 1)
				expectedFrameNumber = frameNumber;
			memset(str, '\0', 100);
			sprintf(str, "Grab complete from cam %d time %u frame %d\n", index, rawImage.GetMetadata().embeddedTimeStamp, frameNumber);
			filestream.write(str, strlen(str));
			filestream.flush();

			//			ResetEvent(handles[index]);
			if (!m_bContinueGrabThread.load())
			{
				break;       
			}

			switch (error.GetType())
			{
			case PGRERROR_ISOCH_NOT_STARTED:
				//
				// This is an expected error case if we are changing modes or 
				// frame rates in another thread (ie, PGRFlycaptureGUI.dll).  Wait
				// a while and then try again.
				//
				TRACE(
					"flycaptureGrabImage2() returned an error (\"%s\") "
					"(expected case.)\n",
					error.GetDescription());

				//AfxMessageBox( "Grab2 camera 1 returned an error" );
				Sleep(50);
				continue;
				break;
			}

			// Do post processing on the image.
			gotPic = true;
			count++;

			if (count > 180 || count < 0)
			{
				count = 1;
			}


			//set it to false
			theapp->pframeSub->m_pxaxis->cameraDone[index][count] = false;
			error = rawImage.Convert(PIXEL_FORMAT_BGRU, m_imageProcessed[index][writeNext[index]]);
			
			if (error != PGRERROR_OK)
			{
				if (error == PGRERROR_FAILED)
				{
					error = rawImage.Convert(PIXEL_FORMAT_BGRU, m_imageProcessed[index][writeNext[index]]);
					if (error != PGRERROR_OK) {
						gotPic = false;
						continue;
					}
				}			//AfxMessageBox("failed to convert image camera 1");
				//OutputDebugString(imProc->GetData());
			}
			///////////////////////////////
			//ML- TO KEEP TRACK OF CAMERA PERFORMANCE
			if (count > 180 || count < 0)
			{
				count = 1;
			}

			//set it to false
			theapp->pframeSub->m_pxaxis->cameraDone[index][count] = false;

			writeNext[index] = writeNext[index] ? 0 : 1;
			readNext[index] = readNext[index] ? 0 : 1;
			memset(str, 0, 100);
			sprintf(str, "Signaling Display from cam %d wrote to buffer %d count %d\n", index, writeNext[index].load(), count.load());
		//	filestream.write(str, strlen(str));
			filestream.flush();

			ReleaseSemaphore(*signal, 1, 0);

			//		SetEvent(handles[index]);
		}//while( m_bContinueGrabThread.load() )

		cam->StopCapture();

	//	SetEvent(eventThreadDone);

	}//while( m_bRestart )
	return error;
}


UINT CCamera::doGrabLoop2()
{
	int camOK = 0;
	int index = 1;
	FlyCapture2::Error error;
	for (int i = 1; i < 6; i++)
	{
		initTry = i;
		error = CameraSetup(index, busMgr, camOK);
		if (error == PGRERROR_OK)
		{
		//	error = SetImageSettings(index);
			if (error == PGRERROR_OK)
			{
				break;
			}
		}
	}

	SetTriggerMode(cam_e[index]);
	doContinousGrab(cam_e[index], theapp->pframeSub->gotPicC2, theapp->pframeSub->countC2, m_iProcessedBufferSize[1], 1);
	return 0;
}

UINT CCamera::doGrabLoop3()
{
	int camOK = 0;
	int index = 2;
	FlyCapture2::Error error;
	for (int i = 1; i < 6; i++)
	{
		initTry = i;
		error = CameraSetup(index, busMgr, camOK);
		if (error == PGRERROR_OK)
		{
		//	error = SetImageSettings(index);
			if (error == PGRERROR_OK)
			{
				break;
			}
		}
	}

	SetTriggerMode(cam_e[index]);
	doContinousGrab(cam_e[index], theapp->pframeSub->gotPicC3, theapp->pframeSub->countC3, m_iProcessedBufferSize[2], 2);
	return 0;
}


UINT CCamera::doGrabLoop4()
{
	int camOK = 0;
	int index = 3;
	FlyCapture2::Error error;
	for (int i = 1; i < 6; i++)
	{
		initTry = i;
		error = CameraSetup(index, busMgr, camOK);
		if (error == PGRERROR_OK)
		{
		//	error = SetImageSettings(index);
			if (error == PGRERROR_OK)
			{
				break;
			}
		}
	}

	SetTriggerMode(cam_e[index]);
	doContinousGrab(cam_e[index], theapp->pframeSub->gotPicC4, theapp->pframeSub->countC4, m_iProcessedBufferSize[3], 3);
	return 0;
}


UINT CCamera::doGrabLoop5()
{
	int camOK = 0;
	int index = 4;
	FlyCapture2::Error error;
	for (int i = 1; i < 6; i++)
	{
		initTry = i;
		error = CameraSetup(index, busMgr, camOK);
		if (error == PGRERROR_OK)
		{
			//error = SetImageSettings(index);
			if (error == PGRERROR_OK)
			{
				break;
			}
		}
	}

	SetTriggerMode(cam_e[index]);
	doContinousGrab(cam_e[index], theapp->pframeSub->gotPicC4, theapp->pframeSub->countC5, m_iProcessedBufferSize[4], 4);
	return 0;
}

void CCamera::DuplicateFlyCaptureBGRUImage(FlyCapture2::Image &image, BYTE *duplicatedImage)
{
	int imageSizeOrg = image.GetCols()*image.GetRows()*bytesPerPixel;
	memcpy (duplicatedImage, image.GetData(), imageSizeOrg );
}


void CCamera::CropCameraImage(FlyCapture2::Image &image, BYTE *croppedImageBuffer,
							  int croppedImageWidth, int croppedImageHeight,
							  int croppedImageXOffset, int croppedImageYOffset)
{
	int imageWidthOrg = image.GetCols();
	int imageStrideNew = croppedImageWidth*bytesPerPixel;

	for(int y = 0; y < croppedImageHeight; y++ )
    {
		memcpy (croppedImageBuffer + (y * croppedImageWidth * bytesPerPixel),
				image.GetData() + ((y + croppedImageYOffset) * imageWidthOrg * bytesPerPixel) + (croppedImageXOffset * bytesPerPixel),
				imageStrideNew);
    }
}


UINT CCamera::threadGrabImage( void* pparam )
{
   CCamera* pNewCamera= (CCamera*) pparam;

   UINT uiRetval = pNewCamera->doGrabLoop(); 
   
   if( uiRetval != 0 )
   {
      CString csMessage;
      csMessage.Format(
         "The grab thread has encountered a problem and had to terminate." );
      AfxMessageBox( csMessage, MB_ICONSTOP );

      //// Signal that the thread has died.
      //SetEvent( pNewCamera->m_heventThreadDone );      
   }

   return uiRetval;
}


UINT CCamera::threadGrabImage2( void* pparam )
{
   CCamera* pNewCamera= (CCamera*) pparam;

   UINT uiRetval = pNewCamera->doGrabLoop2(); 
   
   if( uiRetval != 0 )
   {
      CString csMessage;
      csMessage.Format("The grab thread has encountered a problem and had to terminate.");
      AfxMessageBox( csMessage, MB_ICONSTOP );

      //// Signal that the thread has died.
      //SetEvent( pNewCamera->m_heventThreadDone2 );      
   }

   return uiRetval;
}

UINT CCamera::threadGrabImage3( void* pparam )
{
   CCamera* pNewCamera= (CCamera*) pparam;

   UINT uiRetval = pNewCamera->doGrabLoop3(); 
   
   if( uiRetval != 0 )
   {
      CString csMessage;
      csMessage.Format("The grab thread has encountered a problem and had to terminate.");
      AfxMessageBox( csMessage, MB_ICONSTOP );

  /*     Signal that the thread has died.
      SetEvent( pNewCamera->m_heventThreadDone3 );      
 */  }

   return uiRetval;
}

UINT CCamera::threadGrabImage4( void* pparam )
{
   CCamera* pNewCamera= (CCamera*) pparam;

   UINT uiRetval = pNewCamera->doGrabLoop4(); 
   
   if( uiRetval != 0 )
   {
      CString csMessage;
      csMessage.Format("The grab thread has encountered a problem and had to terminate.");
      AfxMessageBox( csMessage, MB_ICONSTOP );

      //// Signal that the thread has died.
      //SetEvent( pNewCamera->m_heventThreadDone4);      
   }

   return uiRetval;
}

UINT CCamera::threadGrabImage5( void* pparam )
{
   CCamera* pNewCamera= (CCamera*) pparam;

   UINT uiRetval = 0;
   pNewCamera->doGrabLoop5(); 
   
   if( uiRetval != 0 )
   {
      CString csMessage;
      csMessage.Format("The grab thread has encountered a problem and had to terminate.");
      AfxMessageBox( csMessage, MB_ICONSTOP );

   //   // Signal that the thread has died.
   //   SetEvent( pNewCamera->m_heventThreadDone5 );      
   }

   return uiRetval;
}

void CCamera::resizeProcessedImage(int rows, int cols, FlyCapture2::Image* im, int& bufferSize)
{
	//No padding was used in old version... assuming stride == cols

	if (rows * cols * 4 < _MIN_BGR_BUFFER_SIZE)			//New size cannot be smaller than minimum
	{
		cols = min(_MIN_BGR_COLS, cols);
		rows = min(_MIN_BGR_ROWS, rows);
	}


	if (rows * cols > bufferSize)		//New size cannot be smaller than previous size.
	{
		im->SetDimensions(rows, cols, cols, PixelFormat::PIXEL_FORMAT_BGRU, im->GetBayerTileFormat());
		bufferSize = rows * cols * 4;								//Save the new buffer size.
	}
}

void CCamera::ModifyCams()
{
	unsigned long     ulSerialNumber = 0;
	INT_PTR		    ipDialogStatus;

	unsigned int      uiCamerasOnBus = 64;
	int m_iCameraBusIndex = -1;


	m_bContinueGrabThread = false;				//Stop image grab threads.
	Sleep(100);
	m_bContinueGrabThread = true;				//Stop image grab threads.

}

void CCamera::OnGmore() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();

	if(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1<=24.75)
	{
		gain[0] +=0.25;
		theapp->jobinfo[pframe->CurrentJobNum].lightLevel1= gain[0];
		error = SetCameraProperty(cam_e[0], GAIN, gain[0]);
	}
	imageSettings.unlock();

	UpdateData(false);
}

void CCamera::OnGless() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	gain[0]-=0.25;
	theapp->jobinfo[pframe->CurrentJobNum].lightLevel1= gain[0];
	error = SetCameraProperty(cam_e[0], GAIN, gain[0]);
	imageSettings.unlock();
	UpdateData(false);
}
	
void CCamera::OnSsmore1() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	shutterSpeed[0]+=0.1;
	error = SetCameraProperty(cam_e[0], SHUTTER, shutterSpeed[0]);
	imageSettings.unlock();

	UpdateData(false);
}

void CCamera::OnSsless1() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	shutterSpeed[0]-=0.1;
	error = SetCameraProperty(cam_e[0], SHUTTER, shutterSpeed[0]);
	imageSettings.unlock();

	UpdateData(false);
}

void CCamera::OnSsmore2() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	shutterSpeed[1] +=0.1;
	error = SetCameraProperty(cam_e[1], SHUTTER, shutterSpeed[1]);
	imageSettings.unlock();

	UpdateData(false);
}

void CCamera::OnSsless2() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	shutterSpeed[1]-=0.1;
	error = SetCameraProperty(cam_e[1], SHUTTER, shutterSpeed[1]);
	imageSettings.unlock();

	UpdateData(false);
}

void CCamera::OnSsmore3() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	shutterSpeed[2]+=0.1;
	error = SetCameraProperty(cam_e[2], SHUTTER, shutterSpeed[2]);
	imageSettings.unlock();

	UpdateData(false);
}

void CCamera::OnSsless3() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	shutterSpeed[2]-=0.1;
	error = SetCameraProperty(cam_e[2], SHUTTER, shutterSpeed[2]);
	imageSettings.unlock();

	UpdateData(false);
}

void CCamera::OnSsmore4() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	shutterSpeed[3]+=0.1;
	error = SetCameraProperty(cam_e[3], SHUTTER, shutterSpeed[3]);
	imageSettings.unlock();

	UpdateData(false);
}

void CCamera::OnSsless4() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	shutterSpeed[3]-=0.1;
	error = SetCameraProperty(cam_e[3], SHUTTER, shutterSpeed[3]);
	imageSettings.unlock();
	UpdateData(false);
}

void CCamera::OnSsmore5() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	shutterSpeed[4]+=0.1;
	error = SetCameraProperty(cam_e[4], SHUTTER, shutterSpeed[4]);
	imageSettings.unlock();

	UpdateData(false);
}

void CCamera::OnSsless5() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	shutterSpeed[4]-=0.1;
	error = SetCameraProperty(cam_e[4], SHUTTER, shutterSpeed[4]);
	imageSettings.unlock();
	UpdateData(false);
}

void CCamera::OnGmore2() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();

	if(theapp->jobinfo[pframe->CurrentJobNum].lightLevel2<=24.75)
	{
		gain[1]+=0.25;
		theapp->jobinfo[pframe->CurrentJobNum].lightLevel2=gain[1];
		error = SetCameraProperty(cam_e[1], GAIN, gain[1]);
	}
	imageSettings.unlock();

	UpdateData(false);
}

void CCamera::OnGless2() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	gain[1]-=0.25;
	theapp->jobinfo[pframe->CurrentJobNum].lightLevel2=gain[1];
	error = SetCameraProperty(cam_e[1], GAIN, gain[1]);
	imageSettings.unlock();

	UpdateData(false);
}

void CCamera::OnGmore3() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();

	if(theapp->jobinfo[pframe->CurrentJobNum].lightLevel3<=24.75)
	{
		gain[2]+=0.25;
		theapp->jobinfo[pframe->CurrentJobNum].lightLevel3=gain[2];
		error = SetCameraProperty(cam_e[2], GAIN, gain[2]);
	}
	imageSettings.unlock();

	UpdateData(false);
}

void CCamera::OnGless3() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	gain[2]-=0.25;
	theapp->jobinfo[pframe->CurrentJobNum].lightLevel3=gain[2];
	error = SetCameraProperty(cam_e[2], GAIN, gain[2]);

	imageSettings.unlock();
	UpdateData(false);
}

void CCamera::OnGmore4() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();

	if(theapp->jobinfo[pframe->CurrentJobNum].lightLevel4<=24.75)
	{
		gain[3]+=0.25;
		theapp->jobinfo[pframe->CurrentJobNum].lightLevel4=gain[3];
		error = SetCameraProperty(cam_e[3], GAIN, gain[3]);
	}

	imageSettings.unlock();
	UpdateData(false);
}

void CCamera::OnGless4() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	gain[3]-=0.25;
	theapp->jobinfo[pframe->CurrentJobNum].lightLevel4=gain[3];
	error = SetCameraProperty(cam_e[3], GAIN, gain[3]);
	imageSettings.unlock();

	UpdateData(false);
}

void CCamera::OnGmore5() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();

	if(theapp->jobinfo[pframe->CurrentJobNum].lightLevel5<=24.75)
	{
		gain[4]+=0.25;
		theapp->jobinfo[pframe->CurrentJobNum].lightLevel5=gain[4];
		error = SetCameraProperty(cam_e[4], GAIN, gain[4]);
	}

	imageSettings.unlock();
	UpdateData(false);
}

void CCamera::OnGless5() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	gain[4]-=0.25;
	theapp->jobinfo[pframe->CurrentJobNum].lightLevel5=gain[4];
	error = SetCameraProperty(cam_e[4], GAIN, gain[4]);
	imageSettings.unlock();

	UpdateData(false);
}

void CCamera::InitializeCameras()
{
	//m_heventThreadDone	   = CreateEvent(NULL, FALSE, FALSE, NULL);
	//m_heventThreadDone2	   = CreateEvent(NULL, FALSE, FALSE, NULL);
	//m_heventThreadDone3	   = CreateEvent(NULL, FALSE, FALSE, NULL);
	//m_heventThreadDone4	   = CreateEvent(NULL, FALSE, FALSE, NULL);
	//
	//if(theapp->cam5Enable)
	//{
	//	m_heventThreadDone5= CreateEvent( NULL, FALSE, FALSE, NULL );
	//}

	m_iProcessedBufferSize[0]  = 0;
	m_iProcessedBufferSize[1] = 0;
	m_iProcessedBufferSize[2] = 0;
	m_iProcessedBufferSize[3] = 0;
	m_iProcessedBufferSize[4] = 0;
	

	int tempCheck1 =  sizeof( m_imageProcessed1 );

	/*memset( &m_imageProcessed1, 0x0, sizeof( m_imageProcessed1 ));
	memset( &m_imageProcessed2, 0x0, sizeof( m_imageProcessed2 ));
	memset( &m_imageProcessed3, 0x0, sizeof( m_imageProcessed3 ));
	memset( &m_imageProcessed4, 0x0, sizeof( m_imageProcessed4 ));
	memset( &m_imageProcessed5, 0x0, sizeof( m_imageProcessed5 ));
	*/
	m_loopmode = FREE_RUNNING;
}

BOOL CCamera::DestroyWindow() 
{
	int numcams = theapp->cam5Enable ? 5 : 4;
	for (int i = 0; i < numcams; i++)
	{
		m_imageProcessed[i][0]->SetData(0, m_imageProcessed[i][0]->GetDataSize());
		m_imageProcessed[i][1]->SetData(0, m_imageProcessed[i][1]->GetDataSize());
	}
	delete busMgr;

	if (theapp->cam5Enable)
	{
		delete grabThread5;
		delete inspectThread5;
	}
	delete grabThread1;
	delete grabThread2;
	delete grabThread3;
	delete grabThread4;
	delete inspectThread1;
	delete inspectThread2;
	delete inspectThread3;
	delete inspectThread4;

	filestream.flush();
	filestream.close();


	return CDialog::DestroyWindow();
}

void CCamera::SetLightLevel(double cam1Gain, double cam2Gain, double cam3Gain, double cam4Gain, double cam5Gain)
{
	FlyCapture2::Error   error;
	imageSettings.lock();

	gain[0]=cam1Gain;
	gain[1] =cam2Gain;
	gain[2] =cam3Gain;
	gain[3] =cam4Gain;
	gain[4] =cam5Gain;

	error = SetCameraProperty(cam_e[0], GAIN, cam1Gain);
	error = SetCameraProperty(cam_e[1], GAIN, cam2Gain);
	error = SetCameraProperty(cam_e[2], GAIN, cam3Gain);
	error = SetCameraProperty(cam_e[3], GAIN, cam4Gain);

	if(theapp->cam5Enable)	{error = SetCameraProperty(cam_e[4], GAIN, cam5Gain);}
	imageSettings.unlock();
}

//void CCamera::Restart()
//{
//	FlyCaptureError   error;
//
//	error = flycaptureUnlockAll(g_arContext[0]);
//	error = flycaptureUnlockAll(g_arContext[1]);
//	error = flycaptureUnlockAll(g_arContext[2]);
//	error = flycaptureUnlockAll(g_arContext[3]);
//	if(theapp->cam5Enable)	{error = flycaptureUnlockAll(g_arContext[4]);}
//}

void CCamera::OnClose() 
{
	CDialog::OnClose();
}

void CCamera::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{
		KillTimer(1); 
		if (LockOut2 == false)
		{
			//InitializeCameras();
			ReStartCams();
		}
		else
		{
			SetTimer(1, 1000, NULL);
			SetTimer(6, 500, NULL);
		}
	}

	if (nIDEvent == 1)  //Used in combination with timers 3 and 4 to restart cameras.
	{
		KillTimer(1);
		if (LockOut2 == false)
		{
			//InitializeCameras();
			ReStartCams();
		}
		else
		{
			SetTimer(1, 1000, NULL);
			SetTimer(6, 500, NULL);
		}
	}
	if (nIDEvent == 2)
	{
		KillTimer(2); LockOut = false;
	}

	if (nIDEvent == 3)
	{
		KillTimer(3); pframe->SaveJob(pframe->CurrentJobNum);
	}

	if (nIDEvent == 4)
	{
		KillTimer(4); pframe->SendMessage(WM_TRIGGER, NULL, NULL);
	}



	if (nIDEvent == 5)  //Used in combination with timers 1 and 4 to restart cameras.
	{
		KillTimer(5);

		LockOut2 = true;
		StopCams();


		SetTimer(1, 1000, NULL);
	}

	if (nIDEvent == 6)  //Used in combination with timers 1 and 3 to restart cameras.
	{
		KillTimer(6);
		LockOut2 = false;
	}
	

	CDialog::OnTimer(nIDEvent);
}

/*
void CCamera::MoreRed()
{
	FlyCaptureError   error;
	error = flycaptureSetCameraPropertyEx(g_arContext[4],FLYCAPTURE_WHITE_BALANCE,false,true,false,theapp->redBal,theapp->blueBal);
	
	if(error!=FLYCAPTURE_OK)
	{AfxMessageBox("failed to set white balance");}
}

void CCamera::LessRed()
{
	FlyCaptureError   error;
	error = flycaptureSetCameraPropertyEx(g_arContext[4],FLYCAPTURE_WHITE_BALANCE,false,true,false,theapp->redBal,theapp->blueBal );

	if(error!=FLYCAPTURE_OK)
	{AfxMessageBox("failed to set white balance");}
}

void CCamera::ChangeBlue()
{
	FlyCaptureError   error;
	error = flycaptureSetCameraPropertyEx(g_arContext[4],FLYCAPTURE_WHITE_BALANCE,false,true,false,theapp->redBal,theapp->blueBal  );
	
	if(error!=FLYCAPTURE_OK)
	{AfxMessageBox("failed to set white balance");}
}
*/

void CCamera::OnOK() 
{
	// TODO: Add extra validation here	
//	pframe->SSecretMenu = false;
	CDialog::OnOK();
}

void CCamera::UpdateValues()
{
	SetTimer(3,300,NULL);
	UpdateDisplay();
}

void CCamera::UpdateDisplay()
{
	m_bandwidth5.Format("%.0f",theapp->bandwidth5);
	m_bandwidth.Format("%.0f",theapp->bandwidth);

	m_camLeft1.Format("%3i",theapp->camLeft[1].load());
	m_camLeft2.Format("%3i",theapp->camLeft[2].load());
	m_camLeft3.Format("%3i",theapp->camLeft[3].load());
	m_camLeft4.Format("%3i",theapp->camLeft[4].load());
	m_camLeft5.Format("%3i",theapp->camLeft[5].load());

	m_camTop1.Format("%3i",theapp->camTop[1].load());
	m_camTop2.Format("%3i",theapp->camTop[2].load());
	m_camTop3.Format("%3i",theapp->camTop[3].load());
	m_camTop4.Format("%3i",theapp->camTop[4].load());
	m_camTop5.Format("%3i",theapp->camTop[5].load());
	
	/*	Disabled until further notice
	m_camWidth1.Format("%i",theapp->camWidth[1]);
	m_camHeigh1.Format("%i",theapp->camHeight[1]);
	m_camWidth5.Format("%i",theapp->camWidth[5]);
	m_camHeigh5.Format("%i",theapp->camHeight[5]);
	*/

	UpdateData(false);
}

void CCamera::OnRestart() 
{
	if(!LockOut)
	{
		LockOut=true;
		theapp->lostCam+=1;
		StopCams();
	}

	SetTimer(1,1,NULL);
	SetTimer(2,10000,NULL);//10 seconds
	SetTimer(4,11000,NULL);//10 seconds
}

void CCamera::OnBandwidthup1() 
{
	theapp->bandwidth+=2;

	if(theapp->bandwidth>=99)
	theapp->bandwidth=99;

	UpdateValues();	
}

void CCamera::OnBandwidthdw1() 
{
	theapp->bandwidth-=2;

	if(theapp->bandwidth<=30)
	theapp->bandwidth=30;

	UpdateValues();		
}

void CCamera::OnBandwidthup5() 
{
	theapp->bandwidth5+=2;

	if(theapp->bandwidth5>=99)
	theapp->bandwidth5=99;

	UpdateValues();		
}

void CCamera::OnBandwidthdw5() 
{
	theapp->bandwidth5-=2;

	if(theapp->bandwidth5<=62)
	theapp->bandwidth5=62;

	UpdateValues();			
}

void CCamera::CamLeftUp(int camNo)
{
	if((theapp->camTop[camNo].load() + CroppedImageHeight + 2) <= FullImageHeight)
	{
		theapp->camTop[camNo].store(theapp->camTop[camNo].load() + 2);

		//	This should change camera settings only if waist is not enabled
		if(!theapp->waistAvailable)
		{
			UpdateCameraPosition(camNo);
		}

		UpdateValues();
	}
}

void CCamera::CamLeftDown(int camNo)
{
	if((theapp->camTop[camNo].load() - 2) >= 0)
	{
		theapp->camTop[camNo].store(theapp->camTop[camNo].load() - 2);
		//	This should change camera settings only if waist is not enabled
		if(!theapp->waistAvailable)
		{
			UpdateCameraPosition(camNo);
		}

		UpdateValues();	
	}
}

void CCamera::CamTopUp(int camNo)
{
	if((theapp->camLeft[camNo].load() + CroppedImageWidth + 2) <= FullImageWidth)
	{
		theapp->camLeft[camNo].store(theapp->camLeft[camNo].load() + 2);;

		//	This should change camera settings only if waist is not enabled
		if(!theapp->waistAvailable)
		{
			UpdateCameraPosition(camNo);
		}

		UpdateValues();	
	}
}

void CCamera::CamTopDown(int camNo)
{
	if((theapp->camLeft[camNo].load() - 2) >= 0)
	{
		theapp->camLeft[camNo].store(theapp->camLeft[camNo].load() - 2);

		//	This should change camera settings only if waist is not enabled
		if(!theapp->waistAvailable)
		{
			UpdateCameraPosition(camNo);
		}
	
		UpdateValues();	
	}
}

void CCamera::UpdateCameraPosition(int camNo)
{ 
	SetTimer(5,1000,NULL);


//	//	flyCaptureStop causes the threads to terminates because of the m_bContinueGrabThread value
//	FlyCapture2::Error error = flycaptureStop(g_arContext[camNo - 1]);
//	if(error == PGRERROR_OK)
//	{
//		float bwidth = camNo == 5 ? theapp->bandwidth5 : theapp->bandwidth;
//		error = flycaptureStartCustomImage(
//					g_arContext[camNo - 1],
//					0,
//					theapp->camLeft[camNo],
//					theapp->camTop[camNo],
//					theapp->camWidth[camNo],
//					theapp->camHeight[camNo],
//					bwidth,
//					FLYCAPTURE_RAW8);
//		
//		if(error!= PGRERROR_OK)
//		{
//			AfxMessageBox("Invalid Camera Settings");
//		}
//		else
//		{
//			//	Assumes the threads stopped
//			StartImageGrabThreads();
//		}
//	}
}

//	These cameras move in the same direction
void CCamera::OnCamtopup1(){ CamTopDown(1);}
void CCamera::OnCamtopdw1() { CamTopUp(1);}
void CCamera::OnCamtopup3(){ CamTopDown(3);}
void CCamera::OnCamtopdw3() { CamTopUp(3);}

//	These cameras move in the same direction
void CCamera::OnCamtopup2(){ CamTopUp(2);}
void CCamera::OnCamtopdw2() { CamTopDown(2);}
void CCamera::OnCamtopup4(){ CamTopUp(4);}
void CCamera::OnCamtopdw4() { CamTopDown(4);}

//	These cameras move in the same direction
void CCamera::OnCamleftup2() { CamLeftUp(2);}
void CCamera::OnCamleftdw2() { CamLeftDown(2);}
void CCamera::OnCamleftup4() { CamLeftUp(4);}
void CCamera::OnCamleftdw4() { CamLeftDown(4);}

//	These cameras move in the same direction
void CCamera::OnCamleftup1() { CamLeftDown(1);}
void CCamera::OnCamleftdw1() { CamLeftUp(1);}
void CCamera::OnCamleftup3() { CamLeftDown(3);}
void CCamera::OnCamleftdw3() { CamLeftUp(3);}

void CCamera::OnCamleftup5() { CamTopDown(5);}
void CCamera::OnCamleftdw5() { CamTopUp(5);}
void CCamera::OnCamtopup5() { CamLeftUp(5);}
void CCamera::OnCamtopdw5() { CamLeftDown(5);}

void CCamera::OnCamwidthup1() 
{
	/*	Check operation if we decide to use it
	theapp->camWidth[1]+=resWidth;
	theapp->camWidth[2]+=resWidth;
	theapp->camWidth[3]+=resWidth;
	theapp->camWidth[4]+=resWidth;

	if(	theapp->camLeft[1]+theapp->camWidth[1]>=1024	||
		theapp->camLeft[2]+theapp->camWidth[2]>=1024	||
		theapp->camLeft[3]+theapp->camWidth[3]>=1024	||
		theapp->camLeft[4]+theapp->camWidth[4]>=1024)
	{
		//theapp->camWidth[1]-=resWidth; theapp->camWidth[2]-=resWidth;
		//theapp->camWidth[3]-=resWidth; theapp->camWidth[4]-=resWidth;

		theapp->camWidth[1]=1024; theapp->camWidth[2]=1024;
		theapp->camWidth[3]=1024; theapp->camWidth[4]=1024;
	}

	//int remainder = theapp->camWidth[1]%12;
	int remainder = theapp->camWidth[1]%4;
	
	if(remainder != 0)
	{
		theapp->camWidth[1]-= remainder;

		theapp->camWidth[2] = theapp->camWidth[1];
		theapp->camWidth[3] = theapp->camWidth[1];
		theapp->camWidth[4] = theapp->camWidth[1];
	}

	UpdateValues();	
	*/
}

void CCamera::OnCamwidthdw1() 
{
	/*	Check operation if we decide to use it
	theapp->camWidth[1]-=resWidth;
	theapp->camWidth[2]-=resWidth;
	theapp->camWidth[3]-=resWidth;
	theapp->camWidth[4]-=resWidth;

	if(	theapp->camWidth[1]<408	|| theapp->camWidth[2]<408	||
		theapp->camWidth[3]<408	|| theapp->camWidth[4]<408 )
	{
		theapp->camWidth[1]+=resWidth; theapp->camWidth[2]+=resWidth;
		theapp->camWidth[3]+=resWidth; theapp->camWidth[4]+=resWidth;
	}

	//int remainder = theapp->camWidth[1]%12;
	int remainder = theapp->camWidth[1]%4;
	
	if(remainder != 0)
	{
		theapp->camWidth[1]-= remainder;

		theapp->camWidth[2] = theapp->camWidth[1];
		theapp->camWidth[3] = theapp->camWidth[1];
		theapp->camWidth[4] = theapp->camWidth[1];
	}

	UpdateValues();	
	*/
}

void CCamera::OnCamheightup1() 
{
	/*	Check operation if we decide to use it
	theapp->camHeight[1]+=12;
	theapp->camHeight[2]+=12;
	theapp->camHeight[3]+=12;
	theapp->camHeight[4]+=12;

	int rest = 	theapp->camHeight[1]%12;

	if (rest!=0)
	{
		theapp->camHeight[1]-=rest;
		theapp->camHeight[2]-=rest;
		theapp->camHeight[3]-=rest;
		theapp->camHeight[4]-=rest;
	}


	if(	theapp->camTop[1]+theapp->camHeight[1]>766	||
		theapp->camTop[2]+theapp->camHeight[2]>766	||
		theapp->camTop[3]+theapp->camHeight[3]>766	||
		theapp->camTop[4]+theapp->camHeight[4]>766)
	{
		theapp->camHeight[1]-=12;
		theapp->camHeight[2]-=12;
		theapp->camHeight[3]-=12;
		theapp->camHeight[4]-=12;
	}

	UpdateValues();	
	*/
}

void CCamera::OnCamheightdw1() 
{
	/*	Check operation if we decide to use it
	theapp->camHeight[1]-=12;
	theapp->camHeight[2]-=12;
	theapp->camHeight[3]-=12;
	theapp->camHeight[4]-=12;

	int rest = 	theapp->camHeight[1]%12;

	if (rest!=0)
	{
		theapp->camHeight[1]-=rest;
		theapp->camHeight[2]-=rest;
		theapp->camHeight[3]-=rest;
		theapp->camHeight[4]-=rest;
	}

	if(	theapp->camHeight[1]<408 || theapp->camHeight[2]<408 ||
		theapp->camHeight[3]<408 ||	theapp->camHeight[4]<408 )
	{
		theapp->camHeight[1]+=12;
		theapp->camHeight[2]+=12;
		theapp->camHeight[3]+=12;
		theapp->camHeight[4]+=12;
	}

	UpdateValues();
	*/
}

void CCamera::OnCamwidthup5() 
{
	/*	Check operation if we decide to use it
	theapp->camWidth[5]+=resWidth;

	if(	theapp->camLeft[5]+theapp->camWidth[5]>=1024)
	{theapp->camWidth[5]=1024;}

	//int remainder = theapp->camWidth[5]%8;
	int remainder = theapp->camWidth[5]%4;
	
	if(remainder != 0)
	{theapp->camWidth[5]-= remainder;}

	UpdateValues();	
	*/
}

void CCamera::OnCamwidthdw5() 
{
	/*	Check operation if we decide to use it
	theapp->camWidth[5]-=resWidth;

	if(	theapp->camWidth[5]<408)
	{theapp->camWidth[5]+=resWidth;}

	//int remainder = theapp->camWidth[5]%8;
	int remainder = theapp->camWidth[5]%4;
	
	if(remainder != 0)
	{theapp->camWidth[5]-= remainder;}

	UpdateValues();	
	*/
}

void CCamera::OnCamheightup5()
{
	/*	Check operation if we decide to use it
	theapp->camHeight[camNo]+=12;

	int rest = 	theapp->camHeight[camNo]%12;

	if (rest!=0)
	{theapp->camHeight[camNo]-=rest;}

	if(	theapp->camTop[camNo]+theapp->camHeight[5]>FullImageHeight)
	{theapp->camHeight[camNo]-=12;}

	UpdateValues();	
	*/
}

void CCamera::OnCamheightdw5()
{
	/*	Check operation if we decide to use it
	theapp->camHeight[camNo]-=12;

	int rest = 	theapp->camHeight[camNo]%12;

	if (rest!=0)
	{theapp->camHeight[camNo]-=rest;}

	if(	theapp->camHeight[camNo]<408)
	{theapp->camHeight[camNo]+=12;}

	UpdateValues();
	*/
}

UINT CCamera::threadCheckGPIOcontrolReg( void* pparam )
{
   CCamera* pNewCamera= (CCamera*) pparam;

   while(((CBottleApp*)AfxGetApp())->_CheckGPIOReg)
   {
   UINT uiRetval = pNewCamera->CheckGPIOControlReg(); 
   
   if( uiRetval != 0 )
   {
          
      // Signal that the thread has died.
     // SetEvent( pNewCamera->m_heventThreadDone5 );      
   }
   }
   return 0;
}

UINT CCamera:: CheckGPIOControlReg(   )
{
	unsigned long _gpioControlReg=0;
	unsigned long _maskingNo=1;
	//unsigned long result;
/*
	flycaptureGetCameraRegister( g_arContext[ 4 ], 0x1130, &_gpioControlReg );

	result=	(_gpioControlReg & _maskingNo);

	if(result ==1){  
	//	TRACE("%d    \n",_gpioControlReg);//result );
		_count_1=1;

		pframe->_sleeverHeadNumber=1;
	//	TRACE("GPIO2 0x%X \n",AfxGetThread()->m_nThreadID);

		Sleep(1500);
	}
	else //if (( AfxGetMainWnd()) != NULL)
	{
	_count_1=0;
	}
*/
	return 0;
}
