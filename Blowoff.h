#if !defined(AFX_BLOWOFF_H__A36BA62E_8876_4EAF_AF38_DFC3A086319C__INCLUDED_)
#define AFX_BLOWOFF_H__A36BA62E_8876_4EAF_AF38_DFC3A086319C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Blowoff.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Blowoff dialog


class Blowoff : public CDialog
{
	friend class CMainFrame;
	friend class CBottleApp;
	// Construction
public:
	CMainFrame* pframe;
	CBottleApp* theapp;

	void Enable(bool off);
	Blowoff(CWnd* pParent = NULL);   // standard constructor

	long Value;
	int Function;
	int CntrBits;

// Dialog Data
	//{{AFX_DATA(Blowoff)
	enum { IDD = IDD_BLOWOFF };
	CString	m_dist;
	CString	m_dur;
	CString	m_enable;
	CString	m_edithold;
	//}}AFX_DATA
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Blowoff)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Blowoff)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnMore();
	afx_msg void OnLess();
	afx_msg void OnM2();
	afx_msg void OnL2();
	afx_msg void OnMore2();
	afx_msg void OnLess2();
	afx_msg void OnEnable();
	virtual void OnOK();
	afx_msg void OnRaisfingdelaymore();
	afx_msg void OnRaisfingdelayless();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BLOWOFF_H__A36BA62E_8876_4EAF_AF38_DFC3A086319C__INCLUDED_)
