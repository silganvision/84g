#if !defined(AFX_DATA_H__71112D45_C961_4286_9C19_AFFD9C633EC3__INCLUDED_)
#define AFX_DATA_H__71112D45_C961_4286_9C19_AFFD9C633EC3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Data.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Data dialog
class Data : public CDialog
{
	friend class CMainFrame;
	friend class CBottleApp;
	// Construction
public:
	Data(CWnd* pParent = NULL);   // standard constructor
	void WriteData();
	void CreateNewFile();
	CBottleApp *theapp;
	CMainFrame* pframe;

	CFile DataFile;
// Dialog Data
	//{{AFX_DATA(Data)
	enum { IDD = IDD_DATA };
	CString	m_r1;
	CString	m_r10;
	CString	m_r2;
	CString	m_r3;
	CString	m_r4;
	CString	m_r5;
	CString	m_r6;
	CString	m_r7;
	CString	m_r8;
	CString	m_r9;
	CString	m_r15;
	CString	m_24hrtime;
	CString	m_r11;
	CString	m_out0;
	CString	m_out1;
	CString	m_out10;
	CString	m_out11;
	CString	m_out12;
	CString	m_out13;
	CString	m_out14;
	CString	m_out15;
	CString	m_out16;
	CString	m_out2;
	CString	m_out3;
	CString	m_out4;
	CString	m_out5;
	CString	m_out6;
	CString	m_out7;
	CString	m_out8;
	CString	m_out9;
	CString	m_out17;
	CString	m_outh0;
	CString	m_outh1;
	CString	m_outh2;
	CString	m_outh3;
	CString	m_outh4;
	CString	m_outh5;
	CString	m_outh6;
	CString	m_outh7;
	CString	m_outhm1;
	CString	m_outhm2;
	CString	m_outhm3;
	CString	m_outhm4;
	CString	m_outhm5;
	CString	m_outhm6;
	CString	m_outhm7;
	CString	m_outm1;
	CString	m_outm10;
	CString	m_outm11;
	CString	m_outm12;
	CString	m_outm13;
	CString	m_outm14;
	CString	m_outm15;
	CString	m_outm16;
	CString	m_outm17;
	CString	m_outm2;
	CString	m_outm3;
	CString	m_outm4;
	CString	m_outm5;
	CString	m_outm6;
	CString	m_outm7;
	CString	m_outm8;
	CString	m_outm9;
	CString	m_r12;
	CString	m_out18;
	CString	m_out19;
	CString	m_outm18;
	CString	m_outm19;
	CString	m_insp1;
	CString	m_insp2;
	CString	m_insp3;
	CString	m_insp4;
	CString	m_insp5;
	CString	m_insp6;
	CString	m_insp7;
	CString	m_insp8;
	CString	m_insp9;
	CString	m_insp10;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Data)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Data)
	virtual BOOL OnInitDialog();
	afx_msg void OnPrint();
	virtual void OnOK();
	afx_msg void OnButtonearly();
	afx_msg void OnButtonlate();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATA_H__71112D45_C961_4286_9C19_AFFD9C633EC3__INCLUDED_)
