// Modify.cpp : implementation file
//

#include "stdafx.h"
#include "Bottle.h"
#include "Modify.h"
#include "mainfrm.h"
#include "xaxisview.h"
#include "labelcolor.h"
#include "neck.h"
#include "serial.h"
#include "MiddleSetup.h"
#include "ShiftedLabel.h"
//#include "LabelMatingSetup.h"

#include "LabIntegSetup.h"
#include "NoLabelSetup.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Modify dialog


Modify::Modify(CWnd* pParent /*=NULL*/)
	: CDialog(Modify::IDD, pParent)
{
	//{{AFX_DATA_INIT(Modify)
	m_capneck = _T("");
	m_capmiddle = _T("");
	m_captop = _T("");
	m_edite = _T("");
	m_capradius = _T("");
	m_editdec = _T("");
	m_editcode = _T("");
	m_editsize2 = _T("");
	m_editbody = _T("");
	m_editopos = _T("");
	m_editosen = _T("");
	m_pos5 = _T("");
	m_editint = _T("");
	m_pos13 = _T("");
	m_picNum = _T("");
	m_corrLeftC1 = _T("");
	m_corrLeftC2 = _T("");
	m_corrLeftC3 = _T("");
	m_corrLeftC4 = _T("");
	m_corrLef2C1 = _T("");
	m_corrLef2C2 = _T("");
	m_corrLef2C3 = _T("");
	m_corrLef2C4 = _T("");
	m_modl6 = _T("");
	//}}AFX_DATA_INIT
}


void Modify::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Modify)
	DDX_Text(pDX, IDC_EDITOPOS, m_editopos);
	DDX_Text(pDX, IDC_EDITOSEN, m_editosen);
	DDX_Text(pDX, IDC_POS5, m_pos5);
	DDX_Text(pDX, IDC_EDITINT, m_editint);
	DDX_Text(pDX, IDC_POS13, m_pos13);
	DDX_Text(pDX, IDC_PICTOSHOW, m_picNum);
	DDX_Text(pDX, IDC_CORRLEFC1, m_corrLeftC1);
	DDX_Text(pDX, IDC_CORRLEFC2, m_corrLeftC2);
	DDX_Text(pDX, IDC_CORRLEFC3, m_corrLeftC3);
	DDX_Text(pDX, IDC_CORRLEFC4, m_corrLeftC4);
	DDX_Text(pDX, IDC_CORRLEF2C1, m_corrLef2C1);
	DDX_Text(pDX, IDC_CORRLEF2C2, m_corrLef2C2);
	DDX_Text(pDX, IDC_CORRLEF2C3, m_corrLef2C3);
	DDX_Text(pDX, IDC_CORRLEF2C4, m_corrLef2C4);
	DDX_Text(pDX, IDC_FINEMOD, m_fine);
	DDX_Text(pDX, IDC_MODL6, m_modl6);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Modify, CDialog)
	//{{AFX_MSG_MAP(Modify)
	ON_BN_CLICKED(IDC_MUP, OnMup)
	ON_BN_CLICKED(IDC_MDN, OnMdn)
	ON_BN_CLICKED(IDC_NUP, OnNup)
	ON_BN_CLICKED(IDC_NDN, OnNdn)
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_OSENMORE, OnOsenmore)
	ON_BN_CLICKED(IDC_OSENLESS, OnOsenless)
	ON_BN_CLICKED(IDC_OPOSUP, OnOposup)
	ON_BN_CLICKED(IDC_OPOSDN, OnOposdn)
	ON_BN_CLICKED(IDC_M5, OnM5)
	ON_BN_CLICKED(IDC_s5, Ons5)
	ON_BN_CLICKED(IDC_FINEMOD, OnFinemod)
	ON_BN_CLICKED(IDC_INTUP, OnIntup)
	ON_BN_CLICKED(IDC_INTDN, OnIntdn)
	ON_BN_CLICKED(IDC_TOGGLE, OnToggle)
	ON_BN_CLICKED(IDC_S13, OnS13)
	ON_BN_CLICKED(IDC_M13, OnM13)
	ON_BN_CLICKED(IDC_ENAPICSTOSHOW, OnEnapicstoshow)
	ON_BN_CLICKED(IDC_PICTOSHOWUP, OnPictoshowup)
	ON_BN_CLICKED(IDC_PICTOSHOWDN, OnPictoshowdn)
	ON_BN_CLICKED(IDC_SHOWSETUPI5, OnShowsetupi5)
	ON_BN_CLICKED(IDC_SHOWSETUPI8, OnShowsetupi8)
	ON_BN_CLICKED(IDC_FINDMIDDLES, OnFindmiddles)
	ON_BN_CLICKED(IDC_SHOWMIDDLES, OnShowmiddles)
	ON_BN_CLICKED(IDC_MIDDLESETUP, OnMiddlesetup)
	ON_BN_CLICKED(IDC_SHOWMAGENTA, OnShowmagenta)
	ON_BN_CLICKED(IDC_SHOWCYAN, OnShowcyan)
	ON_BN_CLICKED(IDC_SHOWYELLOW, OnShowyellow)
	ON_BN_CLICKED(IDC_SHOWSATURATION, OnShowsaturation)
	ON_BN_CLICKED(IDC_SHOWBW, OnShowbw)
	ON_BN_CLICKED(IDC_SHOWRED, OnShowred)
	ON_BN_CLICKED(IDC_SHIFTEDSETUP, OnShiftedsetup)
	ON_BN_CLICKED(IDC_SHOWEDGES, OnShowedges)
	ON_BN_CLICKED(IDC_SHOWSETUPI7, OnShowsetupi7)
	ON_BN_CLICKED(IDC_LABELINTSET, OnLabelintset)
	ON_BN_CLICKED(IDC_SHOWGREEN, OnShowgreen)
	ON_BN_CLICKED(IDC_SHOWBLUE, OnShowblue)
	ON_BN_CLICKED(IDC_NOLABELSETUP, OnNolabelsetup)
	ON_BN_CLICKED(IDC_SHOWEDGEH, OnShowedgeh)
	ON_BN_CLICKED(IDC_SHOWEDGEV, OnShowedgev)
	ON_BN_CLICKED(IDC_SHOWBIN, OnShowbin)
	ON_BN_CLICKED(IDC_SHOWEDGEFINAL, OnShowedgefinal)
	ON_BN_CLICKED(IDC_TEACH, OnTeach)
	ON_BN_CLICKED(IDC_SHOWSATUR2, OnShowsatur2)
	ON_BN_CLICKED(IDC_SHOWBIN1, OnShowbin1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Modify message handlers

BOOL Modify::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pmod=this;
	theapp=(CBottleApp*)AfxGetApp();

	m_fine.Format("%s","Slow"); 
	res=1;

	newTarget=false;

	if(theapp->DoNewLedge)	{CheckDlgButton(IDC_TOGGLE,1);}
	else							{CheckDlgButton(IDC_TOGGLE,0);}

	if(theapp->enabPicsToShow){CheckDlgButton(IDC_ENAPICSTOSHOW,1);}
	else							{CheckDlgButton(IDC_ENAPICSTOSHOW,0);}

	UpdateDisplay();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Modify::UpdateDisplay()
{
	if( theapp->enabPicsToShow)
	{CheckDlgButton(IDC_ENAPICSTOSHOW,1);}
	else
	{CheckDlgButton(IDC_ENAPICSTOSHOW,0);}

	m_capmiddle.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].midBarY);
	m_capneck.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckBarY);
	m_captop.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].vertBarX);

	m_editint.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].orienArea);

	m_editosen.Format("%3i",theapp->boltOffset);
	m_modl6.Format("I6 %s",theapp->inspctn[6].name);


	
	m_editopos.Format("%3i",theapp->boltOffset2);

	m_pos5.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].ellipseSen);
	m_pos13.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].rotOffset/2);

	m_picNum.Format("%i", pframe->pictoShow);

	if(theapp->enabPicsToShow){CheckDlgButton(IDC_ENAPICSTOSHOW,1);}
	else							{CheckDlgButton(IDC_ENAPICSTOSHOW,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].showSetupI5)
	{
		CheckDlgButton(IDC_SHOWSETUPI5,1);
	}
	else							
	{
		CheckDlgButton(IDC_SHOWSETUPI5,0);
	}

	if(theapp->jobinfo[pframe->CurrentJobNum].showSetupI7)
									{CheckDlgButton(IDC_SHOWSETUPI7,1);}
	else							{CheckDlgButton(IDC_SHOWSETUPI7,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].showSetupI8)
									{CheckDlgButton(IDC_SHOWSETUPI8,1);}
	else							{CheckDlgButton(IDC_SHOWSETUPI8,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].showMiddles)
									{CheckDlgButton(IDC_SHOWMIDDLES,1);}
	else							{CheckDlgButton(IDC_SHOWMIDDLES,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].findMiddles)
									{CheckDlgButton(IDC_FINDMIDDLES,1);}
	else							{CheckDlgButton(IDC_FINDMIDDLES,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].showEdges)
									{CheckDlgButton(IDC_SHOWEDGES,1); }
	else							{CheckDlgButton(IDC_SHOWEDGES,0); }

	if(theapp->jobinfo[pframe->CurrentJobNum].showLabelMating == true)
	{
		CheckDlgButton(IDC_SHOW_LABEL_MATING,1);
	}
	else							
	{
		CheckDlgButton(IDC_SHOW_LABEL_MATING,0);
	}

	//Showing images
	
	if(pframe->showMagentaImg){CheckDlgButton(IDC_SHOWMAGENTA,1);}
	else							{CheckDlgButton(IDC_SHOWMAGENTA,0);}

	if(pframe->showCyanImg)	{CheckDlgButton(IDC_SHOWCYAN,1);}
	else							{CheckDlgButton(IDC_SHOWCYAN,0);}

	if(pframe->showYellowImg)	{CheckDlgButton(IDC_SHOWYELLOW,1);}
	else							{CheckDlgButton(IDC_SHOWYELLOW,0);}

	if(pframe->showSaturatImg){CheckDlgButton(IDC_SHOWSATURATION,1);}
	else							{CheckDlgButton(IDC_SHOWSATURATION,0);}

	if(pframe->showSatura2Img){CheckDlgButton(IDC_SHOWSATUR2,1);}
	else							{CheckDlgButton(IDC_SHOWSATUR2,0);}

	if(pframe->showBWImg)		{CheckDlgButton(IDC_SHOWBW,1);} 
	else							{CheckDlgButton(IDC_SHOWBW,0);} 

	if(pframe->showBWRedImg)	{CheckDlgButton(IDC_SHOWRED,1);} 
	else							{CheckDlgButton(IDC_SHOWRED,0);} 

	if(pframe->showBWGreenImg){CheckDlgButton(IDC_SHOWGREEN,1);}
	else							{CheckDlgButton(IDC_SHOWGREEN,0);}
		
	if(pframe->showBWBlueImg)	{CheckDlgButton(IDC_SHOWBLUE,1);}
	else							{CheckDlgButton(IDC_SHOWBLUE,0);}

	if(pframe->showEdgeHImg)	{CheckDlgButton(IDC_SHOWEDGEH,1);}
	else							{CheckDlgButton(IDC_SHOWEDGEH,0);}

	if(pframe->showEdgeVImg)	{CheckDlgButton(IDC_SHOWEDGEV,1);}
	else							{CheckDlgButton(IDC_SHOWEDGEV,0);}

	if(pframe->showBinImg)	{CheckDlgButton(IDC_SHOWBIN,1);}
	else							{CheckDlgButton(IDC_SHOWBIN,0);}

	if(pframe->showEdgFinalImg)	{CheckDlgButton(IDC_SHOWEDGEFINAL,1);}
	else								{CheckDlgButton(IDC_SHOWEDGEFINAL,0);}

	if(pframe->showBinImgMetho5)	{CheckDlgButton(IDC_SHOWBIN1,1);}
	else								{CheckDlgButton(IDC_SHOWBIN1,0);}

	////////////////

	UpdateData(false);

	if(newTarget)
	{ 
		newTarget=false; 
		UpdateData(false);
		InvalidateRect(NULL,false);
	}
}

void Modify::OnMup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].midBarY-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].midBarY<=0) 
	{theapp->jobinfo[pframe->CurrentJobNum].midBarY=0;}
	
	m_capmiddle.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].midBarY);

	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Modify::OnMdn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].midBarY+=res;
	
	if(pframe->m_pxaxis->MidBar.y>=480) 
	{theapp->jobinfo[pframe->CurrentJobNum].midBarY=480;}
	
	m_capmiddle.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].midBarY);

	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Modify::OnNup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckBarY-=res;
	m_capneck.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckBarY);

	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Modify::OnNdn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckBarY+=res;
	m_capneck.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckBarY);

	SetTimer(1,3000,NULL);
	UpdateData(false);
}


void Modify::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{KillTimer(1); pframe->SaveJob(pframe->CurrentJobNum);}

	CDialog::OnTimer(nIDEvent);
}



void Modify::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	CBrush Saved;
}


void Modify::OnOsenmore() 
{
	theapp->boltOffset+=1;
	m_editosen.Format("%3i",theapp->boltOffset);
	pframe->m_pxaxis->InvalidateRect(NULL,false);
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Modify::OnOsenless() 
{
	theapp->boltOffset-=1;
	m_editosen.Format("%3i",theapp->boltOffset);
	pframe->m_pxaxis->InvalidateRect(NULL,false);
	SetTimer(1,3000,NULL);
	UpdateData(false);
}


void Modify::OnOposup() 
{
	theapp->boltOffset2-=1;
	m_editopos.Format("%3i",theapp->boltOffset2);
	pframe->m_pxaxis->InvalidateRect(NULL,false);
	SetTimer(1,3000,NULL);
	UpdateData(false);	
}

void Modify::OnOposdn() 
{
	theapp->boltOffset2+=1;
	m_editopos.Format("%3i",theapp->boltOffset2);
	pframe->m_pxaxis->InvalidateRect(NULL,false);
	SetTimer(1,3000,NULL);
	UpdateData(false);	
}

void Modify::OnM5() 
{
	theapp->jobinfo[pframe->CurrentJobNum].ellipseSen+=1;
	m_pos5.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].ellipseSen);
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Modify::Ons5() 
{
	theapp->jobinfo[pframe->CurrentJobNum].ellipseSen-=1;
	m_pos5.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].ellipseSen);
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Modify::OnFinemod() 
{
	if(res==1)	{m_fine.Format("%s","Fast"); CheckDlgButton(IDC_FINE,1); res=10;}
	else		{m_fine.Format("%s","Slow"); CheckDlgButton(IDC_FINE,0); res=1;}
	
	UpdateData(false);	
}

void Modify::OnIntup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].orienArea-=res;
	m_editint.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].orienArea);
	pframe->m_pxaxis->InvalidateRect(NULL,false);
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Modify::OnIntdn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].orienArea+=res;
	m_editint.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].orienArea);
	pframe->m_pxaxis->InvalidateRect(NULL,false);
	SetTimer(1,3000,NULL);
	UpdateData(false);	
}


void Modify::OnToggle() 
{
	if(theapp->DoNewLedge)	{theapp->DoNewLedge=false;CheckDlgButton(IDC_TOGGLE,0);}
	else							{theapp->DoNewLedge=true;CheckDlgButton(IDC_TOGGLE,1);}

	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Modify::OnS13() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rotOffset-=2;
	m_pos13.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].rotOffset/2);
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Modify::OnM13() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rotOffset+=2;
	m_pos13.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].rotOffset/2);
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Modify::OnEnapicstoshow() 
{
	if( theapp->enabPicsToShow)
	{CheckDlgButton(IDC_ENAPICSTOSHOW,0);	theapp->enabPicsToShow=false;}
	else
	{CheckDlgButton(IDC_ENAPICSTOSHOW,1);	theapp->enabPicsToShow=true;}
		
	UpdateData(false);	
}

void Modify::OnPictoshowup() 
{
	pframe->pictoShow++;	

	if(pframe->pictoShow>=49)	{pframe->pictoShow=49;}

	UpdateDisplay();
}

void Modify::OnPictoshowdn() 
{
	pframe->pictoShow--;	

	if(pframe->pictoShow<=1)	{pframe->pictoShow=1;}
	
	UpdateDisplay();
}

void Modify::OnShowsetupi5() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].showSetupI5)
	{
		CheckDlgButton(IDC_SHOWSETUPI5,0); 
		theapp->jobinfo[pframe->CurrentJobNum].showSetupI5=false;
	}
	else
	{
		CheckDlgButton(IDC_SHOWSETUPI5,1);
		theapp->jobinfo[pframe->CurrentJobNum].showSetupI5=true;
	}
		
	UpdateValues();	
}

void Modify::OnShowsetupi8() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].showSetupI8)
	{
		CheckDlgButton(IDC_SHOWSETUPI8,0); 
		theapp->jobinfo[pframe->CurrentJobNum].showSetupI8=false;
	}
	else
	{
		CheckDlgButton(IDC_SHOWSETUPI8,1);
		theapp->jobinfo[pframe->CurrentJobNum].showSetupI8=true;
	}
		
	UpdateValues();	
}

void Modify::UpdateValues()
{
	//////////////////////
	pframe->imgShown = 0;

	//Memory of current filter shown
	if(pframe->showBWImg)			{pframe->imgShown = 1;}
	if(pframe->showSaturatImg)	{pframe->imgShown = 2;}
	if(pframe->showMagentaImg)	{pframe->imgShown = 3;}
	if(pframe->showYellowImg)		{pframe->imgShown = 4;}
	if(pframe->showCyanImg)		{pframe->imgShown = 5;}
	if(pframe->showBWRedImg)		{pframe->imgShown = 6;}
	if(pframe->showBWGreenImg)	{pframe->imgShown = 7;}
	if(pframe->showBWBlueImg)		{pframe->imgShown = 8;}
	if(pframe->showSatura2Img)	{pframe->imgShown = 9;}
	//////////////////////
	
	SetTimer(1,300,NULL);
	UpdateDisplay();

	pframe->m_pxaxis->InvalidateRect(NULL,false);
	pframe->m_pxaxis->UpdateData(false);
}

void Modify::UpdateDisplay2()
{
	m_corrLeftC1.Format("%3.0f", pframe->m_pxaxis->pattLeftPosBy6[1].c);
	m_corrLeftC2.Format("%3.0f", pframe->m_pxaxis->pattLeftPosBy6[2].c);
	m_corrLeftC3.Format("%3.0f", pframe->m_pxaxis->pattLeftPosBy6[3].c);
	m_corrLeftC4.Format("%3.0f", pframe->m_pxaxis->pattLeftPosBy6[4].c);

//	m_corrMidC1.Format("%3.0f", pframe->m_pxaxis->pattMiddPosBy6[1].c);
//	m_corrMidC2.Format("%3.0f", pframe->m_pxaxis->pattMiddPosBy6[2].c);
//	m_corrMidC3.Format("%3.0f", pframe->m_pxaxis->pattMiddPosBy6[3].c);
//	m_corrMidC4.Format("%3.0f", pframe->m_pxaxis->pattMiddPosBy6[4].c);

//	m_corrRigC1.Format("%3.0f", pframe->m_pxaxis->pattRighPosBy6[1].c);
//	m_corrRigC2.Format("%3.0f", pframe->m_pxaxis->pattRighPosBy6[2].c);
//	m_corrRigC3.Format("%3.0f", pframe->m_pxaxis->pattRighPosBy6[3].c);
//	m_corrRigC4.Format("%3.0f", pframe->m_pxaxis->pattRighPosBy6[4].c);

	m_corrLef2C1.Format("%3.0f", pframe->m_pxaxis->pattLeftPosBy3[1].c);
	m_corrLef2C2.Format("%3.0f", pframe->m_pxaxis->pattLeftPosBy3[2].c);
	m_corrLef2C3.Format("%3.0f", pframe->m_pxaxis->pattLeftPosBy3[3].c);
	m_corrLef2C4.Format("%3.0f", pframe->m_pxaxis->pattLeftPosBy3[4].c);

/*
	float maxCorrLeft=0;
	float maxCorrMidd=0;
	float maxCorrRigh=0;
	float maxCorrelation=0;

	for(int i=1; i<=4; i++)
	{
		if(pframe->m_pxaxis->pattLeftPosBy6[i].c>maxCorrLeft)
		{maxCorrLeft=pframe->m_pxaxis->pattLeftPosBy6[i].c;}

		if(pframe->m_pxaxis->pattMiddPosBy6[i].c>maxCorrMidd)
		{maxCorrMidd=pframe->m_pxaxis->pattMiddPosBy6[i].c;}

		if(pframe->m_pxaxis->pattRighPosBy6[i].c>maxCorrRigh)
		{maxCorrRigh=pframe->m_pxaxis->pattRighPosBy6[i].c;}
	}

	if(maxCorrLeft>maxCorrelation)	{maxCorrelation=maxCorrLeft;}
	if(maxCorrMidd>maxCorrelation)	{maxCorrelation=maxCorrMidd;}
	if(maxCorrRigh>maxCorrelation)	{maxCorrelation=maxCorrRigh;}

	m_corrLef.Format("%3.0f",	maxCorrLeft);
	m_corrMidd.Format("%3.0f",	maxCorrMidd);
	m_corrRig.Format("%3.0f",	maxCorrRigh);
	m_corrBest.Format("%3.0f",	maxCorrelation);

	//////////////
	float maxCorrelation2=0;

	m_corrLef2.Format("%3.0f",	pframe->m_pxaxis->pattHighResLeft.c);
	m_corrMidd2.Format("%3.0f",	pframe->m_pxaxis->pattHighResMidd.c);
	m_corrRig2.Format("%3.0f",	pframe->m_pxaxis->pattHighResRigh.c);

	if(pframe->m_pxaxis->pattHighResLeft.c>maxCorrelation2) 
	{maxCorrelation2 = pframe->m_pxaxis->pattHighResLeft.c;}

	if(pframe->m_pxaxis->pattHighResMidd.c>maxCorrelation2) 
	{maxCorrelation2 = pframe->m_pxaxis->pattHighResMidd.c;}

	if(pframe->m_pxaxis->pattHighResRigh.c>maxCorrelation2) 
	{maxCorrelation2 = pframe->m_pxaxis->pattHighResRigh.c;}

	m_corrBest2.Format("%3.0f",	maxCorrelation2);

	//////////////
	float corrLeft3 = (pframe->m_pxaxis->pattHighResLeft.c+maxCorrLeft)/2;
	float corrMidd3 = (pframe->m_pxaxis->pattHighResMidd.c+maxCorrMidd)/2;
	float corrRigh3 = (pframe->m_pxaxis->pattHighResRigh.c+maxCorrRigh)/2;
	
	float maxCorrelation3 = 0;

	if(corrLeft3>maxCorrelation3)	{maxCorrelation=corrLeft3;}
	if(corrMidd3>maxCorrelation3)	{maxCorrelation=corrMidd3;}
	if(corrRigh3>maxCorrelation3)	{maxCorrelation=corrRigh3;}

	m_corrLef3.Format("%3.0f",	corrLeft3);
	m_corrMidd3.Format("%3.0f",	corrMidd3);
	m_corrRig3.Format("%3.0f",	corrRigh3);
	m_corrBest3.Format("%3.0f",	maxCorrelation3);
*/
	UpdateData(false);
}

void Modify::OnFindmiddles() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].findMiddles)
	{
		CheckDlgButton(IDC_FINDMIDDLES,0); 
		theapp->jobinfo[pframe->CurrentJobNum].findMiddles=false;
	}
	else
	{
		CheckDlgButton(IDC_FINDMIDDLES,1);
		theapp->jobinfo[pframe->CurrentJobNum].findMiddles=true;
	}
		
	UpdateValues();			
}

void Modify::OnShowmiddles() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].showMiddles)
	{
		CheckDlgButton(IDC_SHOWMIDDLES,0); 
		theapp->jobinfo[pframe->CurrentJobNum].showMiddles=false;
	}
	else
	{
		CheckDlgButton(IDC_SHOWMIDDLES,1);
		theapp->jobinfo[pframe->CurrentJobNum].showMiddles=true;
	}
		
	UpdateValues();		
}

void Modify::OnMiddlesetup() 
{
	MiddleSetup middleSetup;
	middleSetup.DoModal();	
}

void Modify::OnShowmagenta() 
{
	bool showSta = pframe->showMagentaImg;

	if(showSta)	{showSta=false;}
	else				{setAllToFalse(); showSta=true;}

	pframe->showMagentaImg = showSta;
		
	UpdateValues();	
}

void Modify::OnShowcyan() 
{
	bool showSta = pframe->showCyanImg;

	if(showSta)	{showSta=false;}
	else				{setAllToFalse(); showSta=true;}

	pframe->showCyanImg = showSta;
		
	UpdateValues();	
}

void Modify::OnShowyellow() 
{
	bool showSta = pframe->showYellowImg;

	if(showSta)	{showSta=false;}
	else				{setAllToFalse(); showSta=true;}

	pframe->showYellowImg = showSta;
		
	UpdateValues();	
}

void Modify::OnShowsaturation() 
{
	bool showSta = pframe->showSaturatImg;

	if(showSta)	{showSta=false;}
	else				{setAllToFalse(); showSta=true;}

	pframe->showSaturatImg = showSta;
		
	UpdateValues();		
}

void Modify::OnShowsatur2() 
{
	bool showSta = pframe->showSatura2Img;

	if(showSta)	{showSta=false;}
	else				{setAllToFalse(); showSta=true;}

	pframe->showSatura2Img = showSta;
		
	UpdateValues();			
}


void Modify::OnShowbw() 
{
	bool showSta = pframe->showBWImg;

	if(showSta)	{showSta=false;}
	else				{setAllToFalse(); showSta=true;}

	pframe->showBWImg = showSta;
		
	UpdateValues();		
}


void Modify::OnShowred() 
{
	bool showSta = pframe->showBWRedImg;

	if(showSta)	{showSta=false;}
	else				{setAllToFalse(); showSta=true;}

	pframe->showBWRedImg = showSta;
		
	UpdateValues();	
}


void Modify::OnShiftedsetup() 
{
	ShiftedLabel shiftedLabel;
	shiftedLabel.DoModal();	
}

void Modify::OnShowedges() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].showEdges)
	{
		CheckDlgButton(IDC_SHOWEDGES,0); 
		theapp->jobinfo[pframe->CurrentJobNum].showEdges=false;
	}
	else
	{
		CheckDlgButton(IDC_SHOWEDGES,1);
		theapp->jobinfo[pframe->CurrentJobNum].showEdges=true;
	}
		
	UpdateValues();			
}

void Modify::OnShowsetupi7() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].showSetupI7)
	{
		CheckDlgButton(IDC_SHOWSETUPI7,0); 
		theapp->jobinfo[pframe->CurrentJobNum].showSetupI7=false;
	}
	else
	{
		CheckDlgButton(IDC_SHOWSETUPI7,1);
		theapp->jobinfo[pframe->CurrentJobNum].showSetupI7=true;
	}
		
	UpdateValues();		
}



void Modify::OnLabelintset() 
{
	LabIntegSetup labIntegSetup;
	labIntegSetup.DoModal();	
}


void Modify::OnShowgreen() 
{
	bool showSta = pframe->showBWGreenImg;

	if(showSta)	{showSta=false;}
	else				{setAllToFalse(); showSta=true;}

	pframe->showBWGreenImg = showSta;
		
	UpdateValues();	
}

void Modify::OnShowblue() 
{
	bool showSta = pframe->showBWBlueImg;

	if(showSta)	{showSta=false;}
	else				{setAllToFalse(); showSta=true;}

	pframe->showBWBlueImg = showSta;
		
	UpdateValues();				
}

void Modify::OnNolabelsetup() 
{
	NoLabelSetup noLabelSetup;
	noLabelSetup.DoModal();	
}

void Modify::setAllToFalse()
{
	pframe->showCyanImg		=	false;
	pframe->showMagentaImg	=	false;
	pframe->showYellowImg	=	false;
	pframe->showSaturatImg	=	false;
	pframe->showSatura2Img	=	false;
	pframe->showBWImg		=	false;
	pframe->showBWRedImg	=	false;
	pframe->showBWGreenImg	=	false;
	pframe->showBWBlueImg	=	false;
	pframe->showEdgeHImg	=	false;
	pframe->showEdgeVImg	=	false;
	pframe->showBinImg		=	false;
	pframe->showEdgFinalImg	=	false;
	pframe->showBinImgMetho5=	false;
}

void Modify::OnShowedgeh() 
{
	bool showSta = pframe->showEdgeHImg;

	if(showSta)	{showSta=false;}
	else				{setAllToFalse(); showSta=true;}

	pframe->showEdgeHImg = showSta;
		
	UpdateValues();			
}

void Modify::OnShowedgev() 
{
	bool showSta = pframe->showEdgeVImg;

	if(showSta)	{showSta=false;}
	else				{setAllToFalse(); showSta=true;}

	pframe->showEdgeVImg = showSta;
		
	UpdateValues();			
}

void Modify::OnShowbin() 
{
	bool showSta = pframe->showBinImg;

	if(showSta)	{showSta=false;}
	else				{setAllToFalse(); showSta=true;}

	pframe->showBinImg = showSta;
		
	UpdateValues();			
}

void Modify::OnShowedgefinal() 
{
	bool showSta = pframe->showEdgFinalImg;

	if(showSta)	{showSta=false;}
	else				{setAllToFalse(); showSta=true;}

	pframe->showEdgFinalImg = showSta;
		
	UpdateValues();				
}

void Modify::OnShowbin1() 
{
	bool showSta = pframe->showBinImgMetho5;

	if(showSta)	{showSta=false;}
	else				{setAllToFalse(); showSta=true;}

	pframe->showBinImgMetho5 = showSta;
		
	UpdateValues();					
}

BOOL Modify::DestroyWindow() 
{
	m_fine.Empty();
	m_enable.Empty();
	m_capneck.Empty();
	m_capmiddle.Empty();
	m_captop.Empty();
	m_edite.Empty();
	m_capradius.Empty();
	m_editdec.Empty();
	m_editcode.Empty();
	m_editsize2.Empty();


	m_editbody.Empty();
	m_editopos.Empty();
	m_editosen.Empty();
	m_pos1.Empty();
	m_pos2.Empty();
	m_pos3.Empty();
	m_pos4.Empty();
	m_pos5.Empty();
	m_pos6.Empty();
	m_pos7.Empty();
	m_pos8.Empty();
	m_editint.Empty();
	m_pos10.Empty();
	m_pos11.Empty();
	m_pos9.Empty();
	m_pos13.Empty();

	m_picNum.Empty();
	m_corrLeftC1.Empty();
	m_corrLeftC2.Empty();
	m_corrLeftC3.Empty();
	m_corrLeftC4.Empty();
	m_corrLef2C1.Empty();
	m_corrLef2C2.Empty();
	m_corrLef2C3.Empty();
	m_corrLef2C4.Empty();
	

	m_fine.FreeExtra();
	m_enable.FreeExtra();
	m_capneck.FreeExtra();
	m_capmiddle.FreeExtra();
	m_captop.FreeExtra();
	m_edite.FreeExtra();
	m_capradius.FreeExtra();
	m_editdec.FreeExtra();
	m_editcode.FreeExtra();
	m_editsize2.FreeExtra();

	m_editbody.FreeExtra();
	m_editopos.FreeExtra();
	m_editosen.FreeExtra();
	m_pos1.FreeExtra();
	m_pos2.FreeExtra();
	m_pos3.FreeExtra();
	m_pos4.FreeExtra();
	m_pos5.FreeExtra();
	m_pos6.FreeExtra();
	m_pos7.FreeExtra();
	m_pos8.FreeExtra();
	m_editint.FreeExtra();
	m_pos10.FreeExtra();
	m_pos11.FreeExtra();
	m_pos9.FreeExtra();
	m_pos13.FreeExtra();

	m_picNum.FreeExtra();
	m_corrLeftC1.FreeExtra();
	m_corrLeftC2.FreeExtra();
	m_corrLeftC3.FreeExtra();
	m_corrLeftC4.FreeExtra();
	m_corrLef2C1.FreeExtra();
	m_corrLef2C2.FreeExtra();
	m_corrLef2C3.FreeExtra();
	m_corrLef2C4.FreeExtra();

	return CDialog::DestroyWindow();
}

void Modify::OnTeach() 
{

	if(pframe->CurrentJobNum>=1 && pframe->CurrentJobNum<=theapp->SavedTotalJobs)
	{
		pframe->TeachPicC1=true; 
		theapp->jobinfo[pframe->CurrentJobNum].taught=true;
	}
	else {AfxMessageBox("You must load a job first!");}		

	
}


