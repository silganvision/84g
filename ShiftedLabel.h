#if !defined(AFX_SHIFTEDLABEL_H__4653434F_B32F_450C_83D9_FE56094C2F19__INCLUDED_)
#define AFX_SHIFTEDLABEL_H__4653434F_B32F_450C_83D9_FE56094C2F19__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ShiftedLabel.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// ShiftedLabel dialog

class ShiftedLabel : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
	// Construction
public:

	CBottleApp	*theapp;
	CMainFrame	*pframe;

	int		res;
	int		resTh;

	int filterSelShifted;

	void UpdateDisplay();
	void UpdateValues();
	ShiftedLabel(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ShiftedLabel)
	enum { IDD = IDD_SHIFTEDLABEL };
	CButton	m_fine;
	CString	m_shiftedLabTh;
	CString	m_shiftBotLim1;
	CString	m_shiftTopLim1;
	CString	m_shiftBotLim2;
	CString	m_shiftBotLim3;
	CString	m_shiftBotLim4;
	CString	m_shiftTopLim2;
	CString	m_shiftTopLim3;
	CString	m_shiftTopLim4;
	CString	m_edgeSen;
	CString	m_shiftWidth;
	CString	m_horIni;
	CString	m_horEnd;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ShiftedLabel)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ShiftedLabel)
	afx_msg void OnFilter1();
	afx_msg void OnFilter2();
	afx_msg void OnFilter3();
	afx_msg void OnFilter4();
	afx_msg void OnFilter5();
	afx_msg void OnFilter6();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnShifttoplimup();
	afx_msg void OnShifttoplimdw();
	afx_msg void OnShiftbotlimup();
	afx_msg void OnShiftbotlimdw();
	afx_msg void OnFinemod();
	afx_msg void OnShiftbotthup();
	afx_msg void OnShiftbotthdw();
	afx_msg void OnShifttoplimup2();
	afx_msg void OnShifttoplimup3();
	afx_msg void OnShifttoplimup4();
	afx_msg void OnShifttoplimdw2();
	afx_msg void OnShifttoplimdw3();
	afx_msg void OnShifttoplimdw4();
	afx_msg void OnShiftbotlimup2();
	afx_msg void OnShiftbotlimup3();
	afx_msg void OnShiftbotlimup4();
	afx_msg void OnShiftbotlimdw2();
	afx_msg void OnShiftbotlimdw3();
	afx_msg void OnShiftbotlimdw4();
	afx_msg void OnShiftbotthup2();
	afx_msg void OnShiftbotthdw2();
	afx_msg void OnShiftwidthup();
	afx_msg void OnShiftwidthdw();
	afx_msg void OnShiftinilimrigh();
	afx_msg void OnShiftinilimleft();
	afx_msg void OnShiftendlimrigh();
	afx_msg void OnShiftendlimleft();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHIFTEDLABEL_H__4653434F_B32F_450C_83D9_FE56094C2F19__INCLUDED_)
