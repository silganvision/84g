#if !defined(AFX_LABINTEGSETUP_H__E0F2F475_A465_427E_A997_40B09FDFDB2D__INCLUDED_)
#define AFX_LABINTEGSETUP_H__E0F2F475_A465_427E_A997_40B09FDFDB2D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LabIntegSetup.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// LabIntegSetup dialog

class LabIntegSetup : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
	// Construction
public:
	void UpdateValues2();
	void UpdateValues();
	CBottleApp *theapp;
	CMainFrame* pframe;

	LabIntegSetup(CWnd* pParent = NULL);   // standard constructor
	int res;
	void UpdateDisplay();

// Dialog Data
	//{{AFX_DATA(LabIntegSetup)
	enum { IDD = IDD_LABELINTSETUP };
	CButton	m_fine;
	CString	m_topLimPatt;
	CString	m_botLimPatt;
	CString	m_sensMet3;
	CString	m_heightMethod3;
	CString	m_widthMethod3;
	CString	m_sensEdgeMet3;
	CString	m_Method5_Wt;
	CString	m_Method5_Ht;
	CString	m_Method5_Hb;
	CString	m_Method5_Wb;
	CString	m_Method5_Yt;
	CString	m_Method5_Yb;
	CString	m_heighMethod5;
	CString	m_widthMethod5;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(LabIntegSetup)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(LabIntegSetup)
	virtual void OnOK();
	afx_msg void OnSensmet3up();
	afx_msg void OnSensmet3dw();
	afx_msg void OnToplimpattup();
	afx_msg void OnToplimpattdw();
	afx_msg void OnBotlimpattup();
	afx_msg void OnBotlimpattdw();
	afx_msg void OnFinemod();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnHeightmet3up();
	afx_msg void OnHeightmet3dw();
	afx_msg void OnWidthmet3up();
	afx_msg void OnWidthmet3dw();
	afx_msg void OnSensedgmet3up();
	afx_msg void OnSensedgmet3dw();
	afx_msg void OnTopheightup1();
	afx_msg void OnTopheightdw1();
	afx_msg void OnTopwidthup1();
	afx_msg void OnTopwidthdw1();
	afx_msg void OnBotheightup1();
	afx_msg void OnBotheightdw1();
	afx_msg void OnBotwidthup1();
	afx_msg void OnBotwidthdw1();
	afx_msg void OnTopyposup1();
	afx_msg void OnTopyposdw1();
	afx_msg void OnBotyposup1();
	afx_msg void OnBotyposdw1();
	afx_msg void OnHeightmet5up1();
	afx_msg void OnHeightmet5dw1();
	afx_msg void OnWidthmet5up1();
	afx_msg void OnWidthmet5dw1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LABINTEGSETUP_H__E0F2F475_A465_427E_A997_40B09FDFDB2D__INCLUDED_)
