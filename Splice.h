#if !defined(AFX_SPLICE_H__DA2E6EA1_2EA5_44F1_8389_0F8FB11D2DC5__INCLUDED_)
#define AFX_SPLICE_H__DA2E6EA1_2EA5_44F1_8389_0F8FB11D2DC5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Splice.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Splice dialog

class Splice : public CDialog
{
	friend class CMainFrame;
	friend class CBottleApp;
// Construction
public:
	int		resVariance;

	void UpdateDisplay2();
	bool	stepSlow;
	int		resBox;
	int		lengthCam;
	int		heightCam;

	void UpdateValues();
	void UpdateDisplay();
	Splice(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Splice)
	enum { IDD = IDD_SPLICE };
	CButton	m_step;
	CString	m_resSplice1;
	CString	m_resSplice2;
	CString	m_resSplice3;
	CString	m_resSplice4;
	CString	m_ysplice1;
	CString	m_ysplice2;
	CString	m_ysplice3;
	CString	m_ysplice4;
	CString	m_wsplice;
	CString	m_hsplice;
	CString	m_hsplicesmBx;
	CString	m_picNum;
	CString	m_spVariance1;
	CString	m_spVariance2;
	CString	m_spVariance3;
	CString	m_spVariance4;
	CString	m_spVariance;
	//}}AFX_DATA
	CMainFrame* pframe;
	CBottleApp* theapp;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Splice)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Splice)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnYspliceup1();
	afx_msg void OnYsplicedw1();
	afx_msg void OnYspliceup2();
	afx_msg void OnYsplicedw2();
	afx_msg void OnYspliceup3();
	afx_msg void OnYsplicedw3();
	afx_msg void OnYspliceup4();
	afx_msg void OnYsplicedw4();
	afx_msg void OnWspliceup();
	afx_msg void OnWsplicedw();
	afx_msg void OnHspliceup();
	afx_msg void OnHsplicedw();
	afx_msg void OnHsmallboxup();
	afx_msg void OnHsmallboxdw();
	afx_msg void OnPictoshowup();
	afx_msg void OnPictoshowdn();
	afx_msg void OnEnapicstoshow();
	afx_msg void OnUseimgsplice1();
	afx_msg void OnUseimgsplice2();
	afx_msg void OnUseimgsplice3();
	afx_msg void OnUseimgsplice4();
	afx_msg void OnUseimgsplice5();
	afx_msg void OnUseimgsplice6();
	afx_msg void OnUseimgsplice7();
	afx_msg void OnUseimgsplice8();
	afx_msg void OnVariancup();
	afx_msg void OnVariancdw();
	afx_msg void OnStep();
	afx_msg void OnUseimgsplice9();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SPLICE_H__DA2E6EA1_2EA5_44F1_8389_0F8FB11D2DC5__INCLUDED_)
