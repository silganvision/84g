// Security.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "Security.h"
#include "mainfrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Security dialog


Security::Security(CWnd* pParent /*=NULL*/)
	: CDialog(Security::IDD, pParent)
{
	//{{AFX_DATA_INIT(Security)
	m_editpass = _T("");
	m_manpass = _T("");
	m_oppass = _T("");
	m_suppass = _T("");
	//}}AFX_DATA_INIT
}


void Security::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Security)
	DDX_Control(pDX, IDC_EDITPASS, m_editpass2);
	DDX_Text(pDX, IDC_EDITPASS, m_editpass);
	DDX_Text(pDX, IDC_MANPASS, m_manpass);
	DDX_Text(pDX, IDC_OPPASS, m_oppass);
	DDX_Text(pDX, IDC_SUPPASS, m_suppass);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Security, CDialog)
	//{{AFX_MSG_MAP(Security)
	ON_BN_CLICKED(IDC_B1, OnB1)
	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDC_B2, OnB2)
	ON_BN_CLICKED(IDC_B3, OnB3)
	ON_BN_CLICKED(IDC_B4, OnB4)
	ON_BN_CLICKED(IDC_B5, OnB5)
	ON_BN_CLICKED(IDC_B6, OnB6)
	ON_BN_CLICKED(IDC_B7, OnB7)
	ON_BN_CLICKED(IDC_B8, OnB8)
	ON_BN_CLICKED(IDC_B9, OnB9)
	ON_BN_CLICKED(IDC_RSETUP, OnRsetup)
	ON_BN_CLICKED(IDC_RLIMITS, OnRlimits)
	ON_BN_CLICKED(IDC_REJECT, OnReject)
	ON_BN_CLICKED(IDC_RMODIFY, OnRmodify)
	ON_BN_CLICKED(IDC_B0, OnB0)
	ON_BN_CLICKED(IDC_TECH, OnTech)
	ON_BN_CLICKED(IDC_RJOB, OnRjob)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHANGEOP, OnChangeop)
	ON_BN_CLICKED(IDC_CHANGESUP, OnChangesup)
	ON_BN_CLICKED(IDC_CHANGEMAN, OnChangeman)
	ON_BN_CLICKED(IDC_BYPASS, OnBypass)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Security message handlers

BOOL Security::OnInitDialog() 
{
	CDialog::OnInitDialog();
	pframe		=	(CMainFrame*)AfxGetMainWnd();
	pframe->m_psec=this;
	theapp		=	(CBottleApp*)AfxGetApp();

	PassWord	=	"";
	PassWordX	=	"";
	UpdateData(false);
	Safe		=	false;
	Safe1		=	false;
	Safe2		=	false;
	pframe->SSecretMenu = false;

	bypassTimer	=	false;

	GetDlgItem(IDC_CHANGERECT)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CURRENTRECT)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_OPPASS)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SUPPASS)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_MANPASS)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHANGEOP)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHANGESUP)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHANGEMAN)->ShowWindow(SW_HIDE);

	CheckDlgButton(IDC_RMODIFY,1);
	CheckDlgButton(IDC_RJOB,1);
	CheckDlgButton(IDC_TECH,1);
	CheckDlgButton(IDC_REJECT,1);
	CheckDlgButton(IDC_RSETUP,1);
	CheckDlgButton(IDC_RLIMITS,1);
	
	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Security::OnB0() 
{
	PassWord=PassWord+"0";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
}

void Security::OnB1() 
{
	PassWord=PassWord+"1";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
}

void Security::OnAccept() 
{
	theapp=(CBottleApp*)AfxGetApp();

	if(PassWord=="60515" || PassWord=="114031" )
	{
		Safe=true;
		Safe1=true;
		Safe2=true;

		GetDlgItem(IDC_CHANGERECT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CURRENTRECT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_OPPASS)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SUPPASS)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_MANPASS)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHANGEOP)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHANGESUP)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHANGEMAN)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_ACCEPT)->ShowWindow(SW_HIDE);

		CheckDlgButton(IDC_RMODIFY,0);
		CheckDlgButton(IDC_RJOB,0);
		CheckDlgButton(IDC_TECH,0);
		CheckDlgButton(IDC_REJECT,0);
		CheckDlgButton(IDC_RSETUP,0);
		CheckDlgButton(IDC_RLIMITS,0);
		
		theapp->SavedAdvanced	=true;	pframe->SAdvanced=true;
		theapp->SavedDownBottle	=true;	pframe->SDownBottle=true;


		theapp->SavedCam	=pframe->SCam	=true; 
		theapp->SavedJob	=pframe->SJob	=true; 
		theapp->SavedLimits	=pframe->SLimits=true;
		theapp->SavedModify	=pframe->SModify=true;
		theapp->SavedSetup	=pframe->SSetup	=true; 
		theapp->SavedEject	=pframe->SEject	=true;
		theapp->SavedTech	=pframe->STech	=true;

		m_oppass.Format("%s",theapp->SavedOPPassWord);
		m_suppass.Format("%s",theapp->SavedSUPPassWord);
		m_manpass.Format("%s",theapp->SavedMANPassWord);
		m_editpass.Format("%s","Accepted");

		pframe->SSecretMenu=true;
		pframe->SCam=true;

		if(PassWord=="114031")
		{
			bypassTimer=true;
		}
	}
	//else if(PassWord==_T(theapp->SavedMANPassWord) || PassWord=="223344" )
	else if(PassWord==_T(theapp->SavedMANPassWord) )
	{
		Safe=true;
		Safe1=true;
		Safe2=true;

		GetDlgItem(IDC_CHANGERECT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CURRENTRECT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_OPPASS)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SUPPASS)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_MANPASS)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHANGEOP)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHANGESUP)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHANGEMAN)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_ACCEPT)->ShowWindow(SW_HIDE);

		CheckDlgButton(IDC_RMODIFY,0);
		CheckDlgButton(IDC_RJOB,0);
		//CheckDlgButton(IDC_TECH,0);
		CheckDlgButton(IDC_REJECT,0);
		CheckDlgButton(IDC_RSETUP,0);
		CheckDlgButton(IDC_RLIMITS,0);

		//theapp->SavedAdvanced	=	pframe->SAdvanced	=true;
		theapp->SavedDownBottle	=	pframe->SDownBottle	=true;
		theapp->SavedJob		=	pframe->SJob		=true;
		theapp->SavedCam		=	pframe->SCam		=true; 	
		theapp->SavedLimits		=	pframe->SLimits		=true;
		theapp->SavedModify		=	pframe->SModify		=true;
		theapp->SavedSetup		=	pframe->SSetup		=true; 
		theapp->SavedEject		=	pframe->SEject		=true;
		//theapp->SavedTech		=	pframe->STech		=true;

		m_oppass.Format("%s",theapp->SavedOPPassWord);
		m_suppass.Format("%s",theapp->SavedSUPPassWord);
		m_manpass.Format("%s",theapp->SavedMANPassWord);
		m_editpass.Format("%s","Accepted");
	}

	else if(PassWord==_T(theapp->SavedSUPPassWord) )
	{
		CheckDlgButton(IDC_RJOB,0);
		CheckDlgButton(IDC_RMODIFY,0);
		CheckDlgButton(IDC_RLIMITS,0);

		theapp->SavedJob=pframe->SJob=true;
		theapp->SavedLimits=pframe->SLimits=true;
		theapp->SavedModify=pframe->SModify=true;
		m_editpass.Format("%s","Accepted");
	}
	else if(PassWord==_T(theapp->SavedOPPassWord) )
	{
		CheckDlgButton(IDC_RJOB,0);
		
		theapp->SavedJob=pframe->SJob=true;
		m_editpass.Format("%s","Accepted");
	}
	else if(PassWord=="1010" )
	{
		theapp->lock=false;
		m_editpass.Format("%s","Unlocked");
	}
	//else if(PassWord==_T("60085") )		//Secret menu password.
	//{
	//	pframe->SSecretMenu=true;
	//}
	else
	{
		Safe=false;
		Safe1=false;
		Safe2=false;
		pframe->SSecretMenu=false;
		pframe->SCam=false;

		m_editpass.Format("%s","Rejected");
		Beep(100,100);
	}
	
	m_editpass12="";
	PassWord="";
	PassWordX="";
	UpdateData(false);
}

void Security::OnB2() 
{
	PassWord=PassWord+"2";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
}

void Security::OnB3() 
{	
	PassWord=PassWord+"3";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
}

void Security::OnB4() 
{
	PassWord=PassWord+"4";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
}

void Security::OnB5() 
{
	PassWord=PassWord+"5";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
}

void Security::OnB6() 
{
	PassWord=PassWord+"6";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
}

void Security::OnB7() 
{
	PassWord=PassWord+"7";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
}

void Security::OnB8() 
{
	PassWord=PassWord+"8";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
}

void Security::OnB9() 
{
	PassWord=PassWord+"9";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
}

void Security::OnRsetup() 
{
		
}

void Security::OnRlimits() 
{
	
}

void Security::OnReject() 
{
	
}

void Security::OnRmodify() 
{
	Safe=true;
	Safe1=true;
	Safe2=true;

	GetDlgItem(IDC_CHANGERECT)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_CURRENTRECT)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_OPPASS)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_SUPPASS)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_MANPASS)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_CHANGEOP)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_CHANGESUP)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_CHANGEMAN)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_ACCEPT)->ShowWindow(SW_HIDE);

	CheckDlgButton(IDC_RMODIFY,0);
	CheckDlgButton(IDC_RJOB,0);
	CheckDlgButton(IDC_TECH,0);
	CheckDlgButton(IDC_REJECT,0);
	CheckDlgButton(IDC_RSETUP,0);
	CheckDlgButton(IDC_RLIMITS,0);

	theapp->SavedJob=pframe->SJob=true; 
	theapp->SavedLimits=pframe->SLimits=true;
	theapp->SavedModify=pframe->SModify=true;
	theapp->SavedSetup=pframe->SSetup=true; 
	theapp->SavedEject=pframe->SEject=true;
	theapp->SavedTech=pframe->STech=true;

	m_oppass.Format("%s",theapp->SavedOPPassWord);
	m_suppass.Format("%s",theapp->SavedSUPPassWord);
	m_manpass.Format("%s",theapp->SavedMANPassWord);
	m_editpass.Format("%s","Accepted");
	CDialog::OnOK();	
}


void Security::OnTech() 
{

}


void Security::OnRjob() 
{
	
}


void Security::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)		{KillTimer(1);}
	
	CDialog::OnTimer(nIDEvent);
}

void Security::OnOK() 
{
//	if(theapp->securityEnable)
//	{
		if(!bypassTimer)
		{pframe->SetTimer(13,600000,NULL);}//ten minutes	
//	}

	CDialog::OnOK();
}

void Security::OnChangeop() 
{
	if( (m_editpass12.Right(1)=="0")||(m_editpass12.Right(1)=="1")|| 
		(m_editpass12.Right(1)=="2")||(m_editpass12.Right(1)=="3")|| 
		(m_editpass12.Right(1)=="4")||(m_editpass12.Right(1)=="5")|| 
		(m_editpass12.Right(1)=="6")||(m_editpass12.Right(1)=="7")|| 
		(m_editpass12.Right(1)=="8")||(m_editpass12.Right(1)=="9") ) 
	{
		theapp->SavedOPPassWord=PassWord=m_editpass12;
		m_editpass.Format("%s","New password accepted!");
	}
	else
	{
		m_editpass.Format("%s","Enter a valid password!");
		Beep(1000,100);
	}

	m_oppass.Format("%s",PassWord);
	m_editpass12="";
	PassWord="";
	PassWordX="";

	UpdateData(false);
}

void Security::OnChangesup() 
{
	if( (m_editpass12.Right(1)=="0")|| (m_editpass12.Right(1)=="1")|| 
		(m_editpass12.Right(1)=="2")|| (m_editpass12.Right(1)=="3")|| 
		(m_editpass12.Right(1)=="4")|| (m_editpass12.Right(1)=="5")|| 
		(m_editpass12.Right(1)=="6")|| (m_editpass12.Right(1)=="7")|| 
		(m_editpass12.Right(1)=="8")|| (m_editpass12.Right(1)=="9") ) 
	{
		theapp->SavedSUPPassWord=PassWord=m_editpass12;
		m_editpass.Format("%s","New password accepted!");
	}
	else
	{
		m_editpass.Format("%s","Enter a valid password!");
		Beep(1000,100);
	}

	m_suppass.Format("%s",PassWord);
	m_editpass12="";
	PassWord="";
	PassWordX="";

	UpdateData(false);
}

void Security::OnChangeman() 
{
	if( (m_editpass12.Right(1)=="0")||(m_editpass12.Right(1)=="1")|| 
		(m_editpass12.Right(1)=="2")||(m_editpass12.Right(1)=="3")|| 
		(m_editpass12.Right(1)=="4")||(m_editpass12.Right(1)=="5")|| 
		(m_editpass12.Right(1)=="6")||(m_editpass12.Right(1)=="7")|| 
		(m_editpass12.Right(1)=="8")||(m_editpass12.Right(1)=="9") ) 
	{
		theapp->SavedMANPassWord=PassWord=m_editpass12;
		m_editpass.Format("%s","New password accepted!");
	}
	else
	{
		m_editpass.Format("%s","Enter a valid password!");
		Beep(1000,100);
	}

	m_manpass.Format("%s",PassWord);
	m_editpass12="";
	PassWord="";
	PassWordX="";

	UpdateData(false);
}

void Security::OnBypass() 
{
	
	
}
