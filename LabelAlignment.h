#if !defined(AFX_LABELALIGNMENT_H__BE29D386_6656_4F50_9916_571B9A75065A__INCLUDED_)
#define AFX_LABELALIGNMENT_H__BE29D386_6656_4F50_9916_571B9A75065A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LabelAlignment.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// LabelAlignment dialog

class LabelAlignment : public CDialog
{
// Construction
public:
	void UpdateDisplay();
	LabelAlignment(CWnd* pParent = NULL);   // standard constructor

//	CMainFrame* pframe;
//	CBottleApp* theapp;
// Dialog Data
	//{{AFX_DATA(LabelAlignment)
	enum { IDD = IDD_LabelAlignment };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(LabelAlignment)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(LabelAlignment)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LABELALIGNMENT_H__BE29D386_6656_4F50_9916_571B9A75065A__INCLUDED_)
