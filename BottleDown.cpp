// BottleDown.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "BottleDown.h"
#include "Mainfrm.h"
#include "serial.h"
#include "bottleview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BottleDown dialog

BottleDown::BottleDown(CWnd* pParent /*=NULL*/)
	: CDialog(BottleDown::IDD, pParent)
{
	//{{AFX_DATA_INIT(BottleDown)
	m_dbedit = _T("");
	//}}AFX_DATA_INIT
}

void BottleDown::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BottleDown)
	DDX_Text(pDX, IDC_BOTTLEDOWN, m_enable);
	DDX_Text(pDX, IDC_DBEDIT, m_dbedit);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(BottleDown, CDialog)
	//{{AFX_MSG_MAP(BottleDown)
	ON_BN_CLICKED(IDC_BOTTLEDOWN, OnBotdn)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_DBMORE, OnDbmore)
	ON_BN_CLICKED(IDC_DBLESS, OnDbless)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BottleDown message handlers

BOOL BottleDown::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pbotdn=this;
	theapp=(CBottleApp*)AfxGetApp();

	if(theapp->SavedBottleSensor==0)
	{
		CheckDlgButton(IDC_BOTTLEDOWN,0); 
		m_enable.Format("%s","Sensor is disabled");
	}
	else
	{
		CheckDlgButton(IDC_BOTTLEDOWN,1);
		m_enable.Format("%s","Sensor is enabled");
	}

//	UpdateData(false);

	Value		= 0;	
	Function	= 0;

	m_dbedit.Format("%3i",theapp->SavedDBOffset);
	
	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void BottleDown::OnBotdn() 
{
	pframe=(CMainFrame*)AfxGetMainWnd();

	if(theapp->SavedBottleSensor==1)
	{
		CheckDlgButton(IDC_BOTTLEDOWN,0); 
		m_enable.Format("%s","Sensor is disabled");
		theapp->SavedBottleSensor=0;
		Value=0;
		Function=180;	//Disable Down Bottle
		pframe->m_pserial->SendChar(Value,Function);
	}
	else
	{
		CheckDlgButton(IDC_BOTTLEDOWN,1);
		m_enable.Format("%s","Sensor is enabled");
		theapp->SavedBottleSensor=1;
		Value=0;
		Function=170;	//Enable Down Bottle
		pframe->m_pserial->SendChar(Value,Function);
	}

	UpdateData(false);
}

void BottleDown::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{
		KillTimer(1);

		Value=0;
		Function=0;
		CntrBits=0;

		pframe->m_pserial->ControlChars=Value+Function+CntrBits;
	}
	
	CDialog::OnTimer(nIDEvent);
}

void BottleDown::OnDbmore() 
{
	Value=theapp->SavedDBOffset+=2;

	//if(theapp->SavedDBOffset<=0)theapp->SavedDBOffset=0;
	m_dbedit.Format("%3i",theapp->SavedDBOffset);

	Value=theapp->SavedDBOffset;
	Function=610;

	pframe->m_pserial->SendChar(Value,Function);
	UpdateData(false);
}

void BottleDown::OnDbless() 
{
	Value=theapp->SavedDBOffset-=2;

	//if(theapp->SavedDBOffset<=0)theapp->SavedDBOffset=0;
	m_dbedit.Format("%3i",theapp->SavedDBOffset);

	Value=theapp->SavedDBOffset;
	Function=610;

	pframe->m_pserial->SendChar(Value,Function);
	UpdateData(false);
}
