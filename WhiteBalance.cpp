// WhiteBalance.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "WhiteBalance.h"
#include "serial.h"
#include "MainFrm.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CWhiteBalance::CWhiteBalance(CWnd* pParent /*=NULL*/)
	: CDialog(CWhiteBalance::IDD, pParent)
{
	SelectedCamera = 1;
	LinkedCameras = 0;
	whtbalinc = 1;
	m_sCam = _T("Camera 1");
	m_sLinkCam1 = _T("C2");
	m_sLinkCam2 = _T("C3");
	m_whitebalanceRedText = _T("");
	m_whitebalanceBlueText = _T("");
	//{{AFX_DATA_INIT(CCamera)
	//}}AFX_DATA_INIT
}


BEGIN_MESSAGE_MAP(CWhiteBalance, CDialog)
	//{{AFX_MSG_MAP(CCamera)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_SELCAM1, Cam1RadioSelect)
	ON_BN_CLICKED(IDC_SELCAM2, Cam2RadioSelect)
	ON_BN_CLICKED(IDC_SELCAM3, Cam3RadioSelect)
	ON_BN_CLICKED(IDC_SELCAM4, Cam4RadioSelect)
	ON_BN_CLICKED(IDC_SELCAM5, Cam5RadioSelect)
	ON_BN_CLICKED(IDC_LINKCAM1CHKBOX, OnLinkCam1)
	ON_BN_CLICKED(IDC_LINKCAM2CHKBOX, OnLinkCam2)
	ON_BN_CLICKED(IDC_WBREDUP, OnRedUp)
	ON_BN_CLICKED(IDC_WBREDDOWN, OnRedDown)
	ON_BN_CLICKED(IDC_WBBLUEUP, OnBlueUp)
	ON_BN_CLICKED(IDC_WBBLUEDOWN, OnBlueDown)
	ON_BN_CLICKED(IDC_UPDATE, OnUpdateLinked)
	ON_BN_CLICKED(IDAUTOWB, OnAutoWB)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER4, OnReleasedcaptureRedSlider)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER10, OnReleasedcaptureBlueSlider)
	ON_BN_CLICKED(IDOK, OnClose)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCamera message handlers

BOOL CWhiteBalance::OnInitDialog()
{
	CDialog::OnInitDialog();

	pframe = (CMainFrame*)AfxGetMainWnd();
	theapp = (CBottleApp*)AfxGetApp();

	Cam1RadioSelect();
	ClearLinks();
	UpdateData(false);

	if (!theapp->cam5Enable)
	{
		GetDlgItem(IDC_SELCAM5)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_LINKCAM4CHKBOX)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CAMLINK4)->ShowWindow(SW_HIDE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

//Clears all camera related recourses. Called when program closes.
BOOL CWhiteBalance::DestroyWindow()
{
	return CDialog::DestroyWindow();
}

void CWhiteBalance::SetAutoOff(int index)
{
	int wbr, wbb;

	pframe->m_pcamera->SetCameraPropertyAuto(pframe->m_pcamera->cam_e[index], WHITE_BALANCE, false);
	Sleep(100);
	pframe->m_pcamera->GetCameraProperty(pframe->m_pcamera->cam_e[index], WHITE_BALANCE, wbr, wbb);
	UpdateWBParameters(wbr, wbb);

}

void CWhiteBalance::OnTimer(UINT nIDEvent)
{
	if (nIDEvent == 6)  //
	{
		if (IsCamera1Selected())
			SetAutoOff(0);
		if (IsCamera2Selected())
			SetAutoOff(1);
		if (IsCamera3Selected())
			SetAutoOff(2);

		pframe->m_pcamera->m_bContinueGrabThread = true;
		Sleep(100);
		pframe->SendMessage(WM_TRIGGER, NULL, NULL);
		if (m_whitebalanceAuto)
		{
			m_auto.SetCheck(0);
			m_whitebalanceAuto = false;
		}
		KillTimer(6);
	}

	CDialog::OnTimer(nIDEvent);
}

void CWhiteBalance::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWhiteBalance)
	DDX_Text(pDX, IDC_CAMERALABEL, m_sCam);
	DDX_Text(pDX, IDC_CAMLINK1, m_sLinkCam1);
	DDX_Text(pDX, IDC_CAMLINK2, m_sLinkCam2);
	DDX_Text(pDX, IDC_WBRED, m_whitebalanceRedText);
	DDX_Text(pDX, IDC_WBBLUE, m_whitebalanceBlueText);
	DDX_Control(pDX, IDC_SLIDER4, m_redSlider);
	DDX_Control(pDX, IDC_SLIDER10, m_blueSlider);
	DDX_Control(pDX, IDAUTOWB, m_auto);
	//}}AFX_DATA_MAP
}

void CWhiteBalance::OnClose()
{
	pframe->SSecretMenu = false;
	ShowWindow(SW_HIDE);
	//CDialog::OnClose();
}

//	Select Camera 1
void CWhiteBalance::Cam1RadioSelect()
{
	SelectedCamera = Cam1Selected;
	CheckDlgButton(IDC_SELCAM1, 1);
	CheckDlgButton(IDC_SELCAM2, 0);
	CheckDlgButton(IDC_SELCAM3, 0);
	CheckDlgButton(IDC_SELCAM4, 0);
	CheckDlgButton(IDC_SELCAM5, 0);
	m_sCam = _T("Camera 1");
	m_sLinkCam1 = _T("C2");
	m_sLinkCam2 = _T("C3");
	m_sLinkCam3 = _T("C4");
	m_sLinkCam4 = _T("C5");
	ClearLinks();
	LoadCamera1Values();
	UpdateData(false);
}

//	Select Camera 2
void CWhiteBalance::Cam2RadioSelect()
{
	SelectedCamera = Cam2Selected;
	CheckDlgButton(IDC_SELCAM1, 0);
	CheckDlgButton(IDC_SELCAM2, 1);
	CheckDlgButton(IDC_SELCAM3, 0);
	CheckDlgButton(IDC_SELCAM4, 0);
	CheckDlgButton(IDC_SELCAM5, 0);
	m_sCam = _T("Camera 2");
	m_sLinkCam1 = _T("C1");
	m_sLinkCam2 = _T("C3");
	m_sLinkCam3 = _T("C4");
	m_sLinkCam4 = _T("C5");
	ClearLinks();
	LoadCamera2Values();
	UpdateData(false);
}

//	Select Camera 3
void CWhiteBalance::Cam3RadioSelect()
{
	SelectedCamera = Cam3Selected;
	CheckDlgButton(IDC_SELCAM1, 0);
	CheckDlgButton(IDC_SELCAM2, 0);
	CheckDlgButton(IDC_SELCAM3, 1);
	CheckDlgButton(IDC_SELCAM4, 0);
	CheckDlgButton(IDC_SELCAM5, 0);
	m_sCam = _T("Camera 3");
	m_sLinkCam1 = _T("C1");
	m_sLinkCam2 = _T("C2");
	m_sLinkCam3 = _T("C4");
	m_sLinkCam4 = _T("C5");
	ClearLinks();
	LoadCamera3Values();
	UpdateData(false);
}


//	Select Camera 4
void CWhiteBalance::Cam4RadioSelect()
{
	SelectedCamera = Cam4Selected;
	CheckDlgButton(IDC_SELCAM1, 0);
	CheckDlgButton(IDC_SELCAM2, 0);
	CheckDlgButton(IDC_SELCAM3, 0);
	CheckDlgButton(IDC_SELCAM4, 1);
	CheckDlgButton(IDC_SELCAM5, 0);
	m_sCam = _T("Camera 4");
	m_sLinkCam1 = _T("C1");
	m_sLinkCam2 = _T("C2");
	m_sLinkCam3 = _T("C3");
	m_sLinkCam4 = _T("C5");
	ClearLinks();
	LoadCamera4Values();
	UpdateData(false);
}


//	Select Camera 5
void CWhiteBalance::Cam5RadioSelect()
{
	SelectedCamera = Cam5Selected;
	CheckDlgButton(IDC_SELCAM1, 0);
	CheckDlgButton(IDC_SELCAM2, 0);
	CheckDlgButton(IDC_SELCAM3, 0);
	CheckDlgButton(IDC_SELCAM4, 1);
	CheckDlgButton(IDC_SELCAM5, 0);
	m_sCam = _T("Camera 5");
	m_sLinkCam1 = _T("C1");
	m_sLinkCam2 = _T("C2");
	m_sLinkCam3 = _T("C3");
	m_sLinkCam4 = _T("C4");
	ClearLinks();
	LoadCamera5Values();
	UpdateData(false);
}

void CWhiteBalance::OnLinkCam1()
{
	bool isC1 = m_sLinkCam1.Compare(_T("C1")) == 0;

	if (isC1)
	{
		if ((LinkedCameras & Cam1Selected) == Cam1Selected)
		{
			LinkedCameras -= Cam1Selected;
			CheckDlgButton(IDC_LINKCAM1CHKBOX, 0);
		}
		else
		{
			LinkedCameras += Cam1Selected;
			CheckDlgButton(IDC_LINKCAM1CHKBOX, 1);
		}
	}
	else
	{
		if ((LinkedCameras & Cam2Selected) == Cam2Selected)
		{
			LinkedCameras -= Cam2Selected;
			CheckDlgButton(IDC_LINKCAM1CHKBOX, 0);
		}
		else
		{
			LinkedCameras += Cam2Selected;
			CheckDlgButton(IDC_LINKCAM1CHKBOX, 1);
		}
	}

	UpdateData(false);
}

void CWhiteBalance::OnLinkCam2()
{
	bool isC2 = m_sLinkCam2.Compare(_T("C2")) == 0;

	if (isC2)
	{
		if ((LinkedCameras & Cam2Selected) == Cam2Selected)
		{
			LinkedCameras -= Cam2Selected;
			CheckDlgButton(IDC_LINKCAM2CHKBOX, 0);
		}
		else
		{
			LinkedCameras += Cam2Selected;
			CheckDlgButton(IDC_LINKCAM2CHKBOX, 1);
		}
	}
	else
	{
		if ((LinkedCameras & Cam3Selected) == Cam3Selected)
		{
			LinkedCameras -= Cam3Selected;
			CheckDlgButton(IDC_LINKCAM2CHKBOX, 0);
		}
		else
		{
			LinkedCameras += Cam3Selected;
			CheckDlgButton(IDC_LINKCAM2CHKBOX, 1);
		}
	}

	UpdateData(false);
}

void CWhiteBalance::ClearLinks()
{
	LinkedCameras = NoCamSelected;
	CheckDlgButton(IDC_LINKCAM1CHKBOX, 0);
	CheckDlgButton(IDC_LINKCAM2CHKBOX, 0);
}

void CWhiteBalance::LoadCamera1Values()
{
	m_whitebalanceRed = theapp->whitebalance1Red.load();
	m_whitebalanceBlue = theapp->whitebalance1Blue.load();
	m_auto.SetCheck(m_whitebalanceAuto);

	//Initialize slider for camera1.
	m_redSlider.SetRange(0, 1023);
	m_redSlider.SetTicFreq(32);
	m_redSlider.SetPos(m_whitebalanceRed);

	//Initialize slider for camera1.
	m_blueSlider.SetRange(0, 1023);
	m_blueSlider.SetTicFreq(32);
	m_blueSlider.SetPos(m_whitebalanceBlue);

	m_whitebalanceRedText.Format("%4i", m_whitebalanceRed);
	m_whitebalanceBlueText.Format("%4i", m_whitebalanceBlue);
	UpdateData(false);
}

void CWhiteBalance::LoadCamera2Values()
{
	m_whitebalanceRed = theapp->whitebalance2Red;
	m_whitebalanceBlue = theapp->whitebalance2Blue;
	m_auto.SetCheck(m_whitebalanceAuto);

	//Initialize slider for camera2.
	m_redSlider.SetRange(0, 1023);
	m_redSlider.SetTicFreq(32);
	m_redSlider.SetPos(m_whitebalanceRed);

	//Initialize slider for camera2.
	m_blueSlider.SetRange(0, 1023);
	m_blueSlider.SetTicFreq(32);
	m_blueSlider.SetPos(m_whitebalanceBlue);

	m_whitebalanceRedText.Format("%4i", m_whitebalanceRed);
	m_whitebalanceBlueText.Format("%4i", m_whitebalanceBlue);
	UpdateData(false);
}

void CWhiteBalance::LoadCamera3Values()
{
	m_whitebalanceRed = theapp->whitebalance3Red;
	m_whitebalanceBlue = theapp->whitebalance3Blue;
	m_auto.SetCheck(m_whitebalanceAuto);

	//Initialize slider for camera3.
	m_redSlider.SetRange(0, 1023);
	m_redSlider.SetTicFreq(32);
	m_redSlider.SetPos(m_whitebalanceRed);

	//Initialize slider for camera3.
	m_blueSlider.SetRange(0, 1023);
	m_blueSlider.SetTicFreq(32);
	m_blueSlider.SetPos(m_whitebalanceBlue);

	m_whitebalanceRedText.Format("%4i", m_whitebalanceRed);
	m_whitebalanceBlueText.Format("%4i", m_whitebalanceBlue);
	UpdateData(false);
}


void CWhiteBalance::LoadCamera4Values()
{
	m_whitebalanceRed = theapp->whitebalance4Red;
	m_whitebalanceBlue = theapp->whitebalance4Blue;
	m_auto.SetCheck(m_whitebalanceAuto);

	//Initialize slider for camera4.
	m_redSlider.SetRange(0, 1023);
	m_redSlider.SetTicFreq(32);
	m_redSlider.SetPos(m_whitebalanceRed);

	//Initialize slider for camera4.
	m_blueSlider.SetRange(0, 1023);
	m_blueSlider.SetTicFreq(32);
	m_blueSlider.SetPos(m_whitebalanceBlue);

	m_whitebalanceRedText.Format("%4i", m_whitebalanceRed);
	m_whitebalanceBlueText.Format("%4i", m_whitebalanceBlue);
	UpdateData(false);
}


void CWhiteBalance::LoadCamera5Values()
{
	m_whitebalanceRed = theapp->whitebalance5Red;
	m_whitebalanceBlue = theapp->whitebalance5Blue;
	m_auto.SetCheck(m_whitebalanceAuto);

	//Initialize slider for camera4.
	m_redSlider.SetRange(0, 1023);
	m_redSlider.SetTicFreq(32);
	m_redSlider.SetPos(m_whitebalanceRed);

	//Initialize slider for camera4.
	m_blueSlider.SetRange(0, 1023);
	m_blueSlider.SetTicFreq(32);
	m_blueSlider.SetPos(m_whitebalanceBlue);

	m_whitebalanceRedText.Format("%4i", m_whitebalanceRed);
	m_whitebalanceBlueText.Format("%4i", m_whitebalanceBlue);
	UpdateData(false);
}


void CWhiteBalance::OnReleasedcaptureRedSlider(NMHDR* pNMHDR, LRESULT* pResult)
{
	if (m_whitebalanceAuto)
	{
		m_redSlider.SetPos(m_whitebalanceRed);
		return;
	}
	m_whitebalanceRed = m_redSlider.GetPos();		//Get thumb position.
	UpdateWhiteBalanceRed(m_whitebalanceRed);
	UpdateData(false);
}

void CWhiteBalance::OnReleasedcaptureBlueSlider(NMHDR* pNMHDR, LRESULT* pResult)
{
	if (m_whitebalanceAuto)
	{
		m_blueSlider.SetPos(m_whitebalanceBlue);
		return;
	}
	m_whitebalanceBlue = m_blueSlider.GetPos();		//Get thumb position.
	UpdateWhiteBalanceBlue(m_whitebalanceBlue);
	UpdateData(false);
}

void CWhiteBalance::OnRedUp()
{
	if (m_whitebalanceAuto)
	{
		m_redSlider.SetPos(m_whitebalanceRed);
		return;
	}
	if (m_whitebalanceRed < 1023)
	{
		m_whitebalanceRed += whtbalinc;
		UpdateWhiteBalanceRed(m_whitebalanceRed);
		UpdateData(false);
	}
}

void CWhiteBalance::OnRedDown()
{
	if (m_whitebalanceAuto)
	{
		m_redSlider.SetPos(m_whitebalanceRed);
		return;
	}
	if (m_whitebalanceRed > 0)
	{
		m_whitebalanceRed -= whtbalinc;
		UpdateWhiteBalanceRed(m_whitebalanceRed);
		UpdateData(false);
	}
}

void CWhiteBalance::OnBlueUp()
{
	if (m_whitebalanceAuto)
	{
		m_blueSlider.SetPos(m_whitebalanceBlue);
		return;
	}
	if (m_whitebalanceBlue < 1023)
	{
		m_whitebalanceBlue += whtbalinc;
		UpdateWhiteBalanceBlue(m_whitebalanceBlue);
		UpdateData(false);
	}
}

void CWhiteBalance::OnBlueDown()
{
	if (m_whitebalanceAuto)
	{
		m_blueSlider.SetPos(m_whitebalanceBlue);
		return;
	}
	if (m_whitebalanceBlue > 0)
	{
		m_whitebalanceBlue -= whtbalinc;
		UpdateWhiteBalanceBlue(m_whitebalanceBlue);
		UpdateData(false);
	}
}

bool CWhiteBalance::IsCamera1Selected()
{
	int camerasToUpdate = SelectedCamera | LinkedCameras;
	if ((camerasToUpdate & Cam1Selected) == Cam1Selected)
	{
		return true;
	}
	return false;
}

bool CWhiteBalance::IsCamera2Selected()
{
	int camerasToUpdate = SelectedCamera | LinkedCameras;
	if ((camerasToUpdate & Cam2Selected) == Cam2Selected)
	{
		return true;
	}
	return false;
}

bool CWhiteBalance::IsCamera3Selected()
{
	int camerasToUpdate = SelectedCamera | LinkedCameras;
	if ((camerasToUpdate & Cam3Selected) == Cam3Selected)
	{
		return true;
	}
	return false;
}

bool CWhiteBalance::IsCamera4Selected()
{
	int camerasToUpdate = SelectedCamera | LinkedCameras;
	if ((camerasToUpdate & Cam4Selected) == Cam4Selected)
	{
		return true;
	}
	return false;
}

bool CWhiteBalance::IsCamera5Selected()
{
	int camerasToUpdate = SelectedCamera | LinkedCameras;
	if ((camerasToUpdate & Cam4Selected) == Cam4Selected)
	{
		return true;
	}
	return false;
}

void CWhiteBalance::OnAutoWB()
{
	int wbred, wbblue;

	if (IsCamera1Selected())
		DoAutoWB(0);
	if (IsCamera2Selected())
		DoAutoWB(1);
	if (IsCamera3Selected())
		DoAutoWB(2);
	if (IsCamera4Selected())
		DoAutoWB(3);
	if (IsCamera5Selected())
		DoAutoWB(4);

	SetTimer(6, 4000, NULL); //Allow Cam to Auto correct Wb for 4 sec,then set cam to Ext trigger & Update UI

}

void CWhiteBalance::DoAutoWB(int cameraToUpdate)
{
	m_whitebalanceAuto = m_auto.GetCheck();

	if (m_whitebalanceAuto)
	{
		pframe->m_pcamera->m_bContinueGrabThread = false;
		pframe->m_pserial->SendChar(0, 600);
		Sleep(100);
		pframe->m_pcamera->SetTriggerMode(pframe->m_pcamera->cam_e[cameraToUpdate], false);
		pframe->m_pcamera->SetCameraPropertyAuto(pframe->m_pcamera->cam_e[cameraToUpdate], WHITE_BALANCE, true);
	}
}


void CWhiteBalance::UpdateWhiteBalanceRed(int whitebalanceRed)
{
	FlyCapture2::Error   error;

	pframe->m_pcamera->m_bContinueGrabThread = false;
	int camerasToUpdate = SelectedCamera | LinkedCameras;
	if ((camerasToUpdate & Cam1Selected) == Cam1Selected)
	{
		theapp->whitebalance1Red = whitebalanceRed;
		pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[0], WHITE_BALANCE, whitebalanceRed, m_whitebalanceBlue);
	}

	if ((camerasToUpdate & Cam2Selected) == Cam2Selected)
	{
		theapp->whitebalance2Red = whitebalanceRed;
		pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[1], WHITE_BALANCE, whitebalanceRed, m_whitebalanceBlue);
	}

	if ((camerasToUpdate & Cam3Selected) == Cam3Selected)
	{
		theapp->whitebalance3Red = whitebalanceRed;
		pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[2], WHITE_BALANCE, whitebalanceRed, m_whitebalanceBlue);
	}

	if ((camerasToUpdate & Cam4Selected) == Cam4Selected)
	{
		theapp->whitebalance4Red = whitebalanceRed;
		pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[3], WHITE_BALANCE, whitebalanceRed, m_whitebalanceBlue);
	}

	if (theapp->cam5Enable)
	{
		if ((camerasToUpdate & Cam5Selected) == Cam5Selected)
		{
			theapp->whitebalance5Red = whitebalanceRed;
			pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[4], WHITE_BALANCE, whitebalanceRed, m_whitebalanceBlue);
		}
	}

	m_whitebalanceRed = whitebalanceRed;
	m_whitebalanceRedText.Format("%4i", whitebalanceRed);
	m_redSlider.SetPos(whitebalanceRed);
	Sleep(100);
	pframe->m_pcamera->m_bContinueGrabThread = true;

	pframe->SendMessage(WM_TRIGGER, NULL, NULL);
}

void CWhiteBalance::UpdateWhiteBalanceBlue(int whitebalanceBlue)
{
	FlyCapture2::Error   error;

	pframe->m_pcamera->m_bContinueGrabThread = false;
	int camerasToUpdate = SelectedCamera | LinkedCameras;
	if ((camerasToUpdate & Cam1Selected) == Cam1Selected)
	{
		theapp->whitebalance1Blue = whitebalanceBlue;
		pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[0], WHITE_BALANCE, m_whitebalanceRed, whitebalanceBlue);
	}
	if ((camerasToUpdate & Cam2Selected) == Cam2Selected)
	{
		theapp->whitebalance2Blue = whitebalanceBlue;
		pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[1], WHITE_BALANCE, m_whitebalanceRed, whitebalanceBlue);
	}

	if ((camerasToUpdate & Cam3Selected) == Cam3Selected)
	{
		theapp->whitebalance3Blue = whitebalanceBlue;
		pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[2], WHITE_BALANCE, m_whitebalanceRed, whitebalanceBlue);
	}


	if ((camerasToUpdate & Cam4Selected) == Cam4Selected)
	{
		theapp->whitebalance4Blue = whitebalanceBlue;
		pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[3], WHITE_BALANCE, m_whitebalanceRed, whitebalanceBlue);
	}

	if (theapp->cam5Enable)
	{
		if ((camerasToUpdate & Cam5Selected) == Cam5Selected)
		{
			theapp->whitebalance5Blue = whitebalanceBlue;
			pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[4], WHITE_BALANCE, m_whitebalanceRed, whitebalanceBlue);
		}
	}

	m_whitebalanceBlue = whitebalanceBlue;
	m_whitebalanceBlueText.Format("%4i", whitebalanceBlue);
	m_blueSlider.SetPos(whitebalanceBlue);
	Sleep(100);
	pframe->m_pcamera->m_bContinueGrabThread = true;
	pframe->SendMessage(WM_TRIGGER, NULL, NULL);
}



void CWhiteBalance::OnUpdateLinked()
{
	UpdateWhiteBalanceBlue(m_whitebalanceBlue);
	UpdateWhiteBalanceRed(m_whitebalanceRed);
	UpdateData(false);
}

void CWhiteBalance::UpdateWBParameters(int wbred, int wbblue)
{
	m_whitebalanceRed = wbred;
	m_whitebalanceBlue = wbblue;

	m_redSlider.SetPos(m_whitebalanceRed);
	m_blueSlider.SetPos(m_whitebalanceBlue);

	m_whitebalanceRedText.Format("%4i", m_whitebalanceRed);
	m_whitebalanceBlueText.Format("%4i", m_whitebalanceBlue);

	InvalidateRect(NULL, false);
	UpdateData(false);
}