// Database.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Database.h"
#include "mainfrm.h"

#include "job.h"

#include "fstream.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

char* pfilecurrent = "c:\\silgandata\\pat\\";//drive
char* pfileload = "c:\\silgandata\\load\\";//drive
char* pfilesave = "c:\\silgandata\\save\\";//drive
/////////////////////////////////////////////////////////////////////////////
// Database dialog


Database::Database(CWnd* pParent /*=NULL*/)
	: CDialog(Database::IDD, pParent)
{
	//{{AFX_DATA_INIT(Database)
	//}}AFX_DATA_INIT
}


void Database::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Database)
	DDX_Control(pDX, IDC_LISTSAVE, m_savelist);
	DDX_Control(pDX, IDC_LISTLOAD, m_loadlist);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Database, CDialog)
	//{{AFX_MSG_MAP(Database)
	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDC_LOAD2, OnLoad2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Database message handlers

BOOL Database::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pdatabase=this;
	theapp=(CCapApp*)AfxGetApp();

	D1=theapp->SavedD1Name;
	ShowData();
	//D2="No DataBase";
	m_savelist.InsertString(0,D1);
	m_loadlist.InsertString(0,D2);
	//ShowData();
	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Database::ShowData()
{
	CString C1, N1;
	char File[60];
	char File2[60];

	char pbuf[1];
	int Num=0;
	LoadOK=true;
	
	strcpy(File,pfileload);
	strcat(File,"DateFile");
	
	strcat(File,".txt");

	strcpy(File2,pfileload);
	strcat(File2,"DateFile2");
	
	strcat(File2,".txt");

	//DataFile.Close();

	//int filenum=open( File, ios::nocreate, filebuf::sh_read );// filebuf::openprot
	//if(filenum==-1) {D2="No DataBase" ; LoadOK=false; }
	//close(filenum);

	if(LoadOK) 
	{
		DataFile.Open(File, CFile::modeRead);
	
		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];

		int toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		D2=N1;
		N1="";
	}
}

void Database::MovePic(int index)
{
	int skip=false;
	char SourceFile[60];
	char indexchar[4];
	itoa(index,indexchar,10);

	strcpy(SourceFile,pfilecurrent);
	strcat(SourceFile,indexchar);
	strcat(SourceFile,"\\");
	strcat(SourceFile,indexchar);
	strcat(SourceFile,".bmp");

	char DestinationFile[60];

	strcpy(DestinationFile,pfilesave);
	strcat(DestinationFile,indexchar);
	strcat(DestinationFile,".bmp");

	CVisByteImage image;
	try
	{
		image.ReadFile(SourceFile);
		imageMem.Allocate(image.Rect());
		image.CopyPixelsTo(imageMem);

	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		skip=true;
		//AfxMessageBox("The Source file specified could not be opened. No saved Database?");
	}
	
	//CVisByteImage image(640,480,1,-1,im_sr);
	//imageS=image;
	if(!skip)
	{
		try
		{
			SVisFileDescriptor filedescriptorT;
			ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
			filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
			filedescriptorT.jpeg_quality = 0;

			filedescriptorT.filename = DestinationFile;

			imageMem.WriteFile(filedescriptorT);
		}
		catch (...)  // "..." is bad - we could leak an exception object.
		{
			// Warn the user.
			AfxMessageBox("The file could not be saved. ");

			
		}
	}

}

void Database::LoadPic(int index)
{
	char SourceFile[60];
	char indexchar[4];
	itoa(index,indexchar,10);

	strcpy(SourceFile,pfileload);
	
	strcat(SourceFile,indexchar);
	strcat(SourceFile,".bmp");

	char DestinationFile[60];

	strcpy(DestinationFile,pfilecurrent);
	strcat(DestinationFile,indexchar);
	strcat(DestinationFile,"\\");
	strcat(DestinationFile,indexchar);
	strcat(DestinationFile,".bmp");

	CVisByteImage image;
	try
	{
		image.ReadFile(SourceFile);
		imageMem.Allocate(image.Rect());
		image.CopyPixelsTo(imageMem);

	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		AfxMessageBox("The Source file specified could not be opened. No saved Database?");
	}
	
	//CVisByteImage image(640,480,1,-1,im_sr);
	//imageS=image;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;

		filedescriptorT.filename = DestinationFile;

		imageMem.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Warn the user.
		AfxMessageBox("The file could not be saved. ");

		
	}

}

void Database::OnAccept() 
{
	
	CTime thetime = CTime::GetCurrentTime();
	CString S1;
	
	int mth=thetime.GetMonth();
	int day=thetime.GetDay();
	int yr=thetime.GetYear();
	
	itoa(mth,month,10);
	itoa(day,dayofweek,10);
	itoa(yr,year,10);
	strcpy(cp,pfilesave);
		
	strcpy(currentpath2,"Db");
	//strcat(currentpath2,index);
	strcat(currentpath2,"_");
	strcat(currentpath2,month);
	strcat(currentpath2,"_");
	strcat(currentpath2,dayofweek);
	strcat(currentpath2,"_");
	strcat(currentpath2,year);

	strcat(cp,"DateFile");
	strcat(cp,".txt");

	D1=theapp->SavedD1Name=currentpath2;
		
	DataFile.Open(cp, CFile::modeWrite | CFile::modeCreate);
	DataFile.Write(D1,D1.GetLength());
	DataFile.Write("~",1);
	DataFile.Close();


	strcpy(cp2,pfilesave);
	strcat(cp2,"DateFile2");
	strcat(cp2,".txt");
	DataFile2.Open(cp2, CFile::modeWrite | CFile::modeCreate);
	DataFile2.Close();

	for(int r=1; r<=10; r++)
	{
		itoa(r,index,10);

		strcpy(cp,pfilesave);
		
		strcpy(currentpath2,"Db");
		strcat(currentpath2,index);
		strcat(cp,currentpath2);
		strcat(cp,".txt");
		
		DataFile.Open(cp, CFile::modeWrite | CFile::modeCreate);
		m_savelist.ResetContent();
		//CString S1;
		//S1.Format("%3.2f",theapp->SavedJ1Name);

		DataFile.Write(theapp->SavedJ1Name,theapp->SavedJ1Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ2Name,theapp->SavedJ2Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ3Name,theapp->SavedJ3Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ4Name,theapp->SavedJ4Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ5Name,theapp->SavedJ5Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ6Name,theapp->SavedJ6Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ7Name,theapp->SavedJ7Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ8Name,theapp->SavedJ8Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ9Name,theapp->SavedJ9Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ10Name,theapp->SavedJ10Name.GetLength());
		DataFile.Write("~",1);

		switch(r)
		{
		case 1:

		
			S1.Format("%i",theapp->SavedJ1a);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ1b);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1c);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1d);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1e);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ1f);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1g);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1h);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1i);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ1j);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1k);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1l);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1b1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ1c1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1d1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1e1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1f1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ1g1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1h1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1i1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1L1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ1L1x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1L2);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1L2x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1L3);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ1L3x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1L4);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1L4x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1L5);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ1L5x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1L8);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1L8x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1L6);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ1L6x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1L7);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1L7x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1Taught);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ1MotPos);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1Xtra);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1Type);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1Black);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ1Profile);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		break;

	case 2:
		
			S1.Format("%i",theapp->SavedJ2a);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ2b);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2c);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2d);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2e);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ2f);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2g);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2h);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2i);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ2j);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2k);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2l);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2b1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ2c1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2d1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2e1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2f1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ2g1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2h1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2i1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2L1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ2L1x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2L2);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2L2x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2L3);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ2L3x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2L4);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2L4x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2L5);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ2L5x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2L8);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2L8x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2L6);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ2L6x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2L7);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2L7x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2Taught);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ2MotPos);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2Xtra);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2Type);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ2Black);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

S1.Format("%i",theapp->SavedJ2Profile);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		break;

	case 3:
		
			S1.Format("%i",theapp->SavedJ3a);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ3b);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3c);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3d);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3e);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ3f);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3g);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3h);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3i);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ3j);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3k);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3l);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3b1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ3c1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3d1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3e1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3f1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ3g1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3h1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3i1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3L1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ3L1x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3L2);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3L2x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3L3);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ3L3x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3L4);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3L4x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3L5);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ3L5x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3L8);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3L8x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3L6);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ3L6x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3L7);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3L7x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3Taught);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ3MotPos);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3Xtra);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3Type);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ3Black);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

S1.Format("%i",theapp->SavedJ3Profile);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		break;

case 4:
		
			S1.Format("%i",theapp->SavedJ4a);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ4b);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4c);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4d);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4e);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ4f);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4g);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4h);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4i);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ4j);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4k);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4l);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4b1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ4c1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4d1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4e1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4f1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ4g1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4h1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4i1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4L1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ4L1x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4L2);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4L2x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4L3);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ4L3x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4L4);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4L4x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4L5);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ4L5x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4L8);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4L8x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4L6);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ4L6x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4L7);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4L7x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4Taught);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ4MotPos);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4Xtra);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4Type);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ4Black);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

S1.Format("%i",theapp->SavedJ4Profile);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		break;

case 5:
		
			S1.Format("%i",theapp->SavedJ5a);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ5b);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5c);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5d);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5e);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ5f);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5g);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5h);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5i);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ5j);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5k);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5l);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5b1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ5c1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5d1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5e1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5f1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ5g1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5h1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5i1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5L1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ5L1x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5L2);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5L2x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5L3);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ5L3x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5L4);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5L4x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5L5);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ5L5x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5L8);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5L8x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5L6);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ5L6x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5L7);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5L7x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5Taught);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ5MotPos);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5Xtra);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5Type);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ5Black);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

S1.Format("%i",theapp->SavedJ5Profile);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		break;

case 6:
		
			S1.Format("%i",theapp->SavedJ6a);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ6b);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6c);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6d);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6e);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ6f);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6g);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6h);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6i);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ6j);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6k);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6l);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6b1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ6c1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6d1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6e1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6f1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ6g1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6h1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6i1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6L1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ6L1x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6L2);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6L2x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6L3);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ6L3x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6L4);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6L4x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6L5);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ6L5x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6L8);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6L8x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6L6);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ6L6x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6L7);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6L7x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6Taught);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ6MotPos);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6Xtra);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6Type);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ6Black);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

S1.Format("%i",theapp->SavedJ6Profile);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		break;

case 7:
		
			S1.Format("%i",theapp->SavedJ7a);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ7b);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7c);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7d);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7e);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ7f);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7g);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7h);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7i);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ7j);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7k);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7l);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7b1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ7c1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7d1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7e1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7f1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ7g1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7h1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7i1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7L1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ7L1x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7L2);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7L2x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7L3);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ7L3x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7L4);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7L4x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7L5);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ7L5x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7L8);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7L8x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7L6);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ7L6x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7L7);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7L7x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7Taught);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ7MotPos);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7Xtra);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7Type);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ7Black);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

S1.Format("%i",theapp->SavedJ7Profile);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		break;

case 8:
		
			S1.Format("%i",theapp->SavedJ8a);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ8b);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8c);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8d);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8e);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ8f);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8g);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8h);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8i);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ8j);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8k);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8l);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8b1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ8c1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8d1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8e1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8f1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ8g1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8h1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8i1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8L1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ8L1x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8L2);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8L2x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8L3);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ8L3x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8L4);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8L4x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8L5);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ8L5x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8L8);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8L8x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8L6);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ8L6x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8L7);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8L7x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8Taught);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ8MotPos);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8Xtra);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8Type);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ8Black);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

S1.Format("%i",theapp->SavedJ8Profile);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		break;

case 9:
		
			S1.Format("%i",theapp->SavedJ9a);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ9b);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9c);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9d);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9e);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ9f);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9g);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9h);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9i);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ9j);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9k);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9l);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9b1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ9c1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9d1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9e1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9f1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ9g1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9h1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9i1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9L1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ9L1x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9L2);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9L2x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9L3);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ9L3x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9L4);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9L4x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9L5);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ9L5x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9L8);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9L8x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9L6);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ9L6x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9L7);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9L7x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9Taught);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ9MotPos);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9Xtra);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9Type);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ9Black);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

S1.Format("%i",theapp->SavedJ9Profile);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		break;

case 10:
		
			S1.Format("%i",theapp->SavedJ10a);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ10b);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10c);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10d);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10e);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ10f);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10g);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10h);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10i);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ10j);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10k);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10l);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10b1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ10c1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10d1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10e1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10f1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ10g1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10h1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10i1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10L1);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ10L1x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10L2);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10L2x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10L3);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ10L3x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10L4);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10L4x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10L5);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ10L5x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10L8);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10L8x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10L6);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ10L6x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10L7);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10L7x);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10Taught);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		
			S1.Format("%i",theapp->SavedJ10MotPos);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10Xtra);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10Type);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10Black);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);

			S1.Format("%i",theapp->SavedJ10Profile);
			DataFile.Write(S1,S1.GetLength());
			DataFile.Write("~",1);
		break;
		}//switch
		
		DataFile.Close();
		
		switch(r)
		{
			case 1:
				MovePic(1);
			break;

			case 2:
				MovePic(2);
			break;

			case 3:
				MovePic(3);
			break;

			case 4:
				MovePic(4);
			break;

			case 5:
				MovePic(5);
			break;

			case 6:
				MovePic(6);
			break;

			case 7:
				MovePic(7);
			break;

			case 8:
				MovePic(8);
			break;

			case 9:
				MovePic(9);
			break;

			case 10:
				MovePic(10);
			break;
		}
	}

	m_savelist.InsertString(0,D1);
	theapp->SavedD1Name=D1;
	UpdateData(false);		
}

void Database::OnLoad() 
{
	
	CString C1, N1, Name;
	Name="Name";
	char File[60];
	char pbuf[1];
	int Num=0;
	bool fileOK=true;
	
	for(int r=1; r<=10; r++)
	{

		itoa(r,index,10);
		strcpy(File,pfileload);
		strcat(File,"DB");
		strcat(File,index);
	//strcat(File,CurrentSpot);
		strcat(File,".txt");

//		int filenum=open( File, ios::nocreate , filebuf::openprot );
//		if(filenum==-1) {AfxMessageBox("Can not find database file!") ; fileOK=false; }
//		close(filenum);

		if(fileOK) 
		{
			DataFile.Open(File, CFile::modeRead);
		//searchto=nIndex*130;

//************search to correct job****************************
		//DataFile.Seek( 1, CFile::begin );

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];

		int toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ1Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ2Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ3Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ4Name=N1;
		N1="";
		
		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ5Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ6Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ7Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ8Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ9Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ10Name=N1;
		N1="";
		
		
		switch(r)
		{
		case 1:

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1a=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1b=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1c=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1d=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1e=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1f=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1g=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1h=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1i=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1j=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1k=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1l=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1b1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1c1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1d1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1e1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1f1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1g1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1h1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1i1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L1x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L2=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L2x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L3=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L3x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L4=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L4x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L5=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L5x=atoi(N1);
			N1="";
			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L8=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L8x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L6=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L6x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L7=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1L7x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1Taught=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1MotPos=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1Xtra=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1Type=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1Black=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ1Profile=atoi(N1);
			N1="";
			break;

		case 2:

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2a=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2b=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2c=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2d=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2e=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2f=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2g=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2h=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2i=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2j=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2k=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2l=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2b1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2c1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2d1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2e1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2f1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2g1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2h1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2i1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L1x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L2=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L2x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L3=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L3x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L4=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L4x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L5=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L5x=atoi(N1);
			N1="";
			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L8=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L8x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L6=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L6x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L7=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2L7x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2Taught=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2MotPos=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2Xtra=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2Type=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2Black=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ2Profile=atoi(N1);
			N1="";
			break;

	case 3:

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3a=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3b=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3c=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3d=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3e=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3f=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3g=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3h=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3i=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3j=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3k=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3l=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3b1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3c1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3d1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3e1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3f1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3g1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3h1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3i1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L1x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L2=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L2x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L3=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L3x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L4=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L4x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L5=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L5x=atoi(N1);
			N1="";
			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L8=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L8x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L6=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L6x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L7=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3L7x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3Taught=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3MotPos=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3Xtra=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3Type=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3Black=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ3Profile=atoi(N1);
			N1="";
			break;

	case 4:

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4a=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4b=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4c=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4d=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4e=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4f=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4g=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4h=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4i=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4j=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4k=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4l=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4b1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4c1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4d1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4e1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4f1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4g1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4h1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4i1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L1x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L2=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L2x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L3=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L3x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L4=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L4x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L5=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L5x=atoi(N1);
			N1="";
			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L8=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L8x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L6=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L6x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L7=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4L7x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4Taught=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4MotPos=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4Xtra=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4Type=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4Black=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ4Profile=atoi(N1);
			N1="";
			break;

	case 5:

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5a=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5b=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5c=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5d=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5e=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5f=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5g=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5h=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5i=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5j=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5k=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5l=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5b1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5c1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5d1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5e1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5f1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5g1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5h1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5i1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L1x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L2=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L2x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L3=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L3x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L4=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L4x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L5=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L5x=atoi(N1);
			N1="";
			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L8=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L8x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L6=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L6x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L7=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5L7x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5Taught=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5MotPos=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5Xtra=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5Type=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5Black=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ5Profile=atoi(N1);
			N1="";
			break;

	case 6:

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6a=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6b=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6c=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6d=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6e=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6f=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6g=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6h=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6i=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6j=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6k=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6l=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6b1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6c1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6d1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6e1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6f1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6g1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6h1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6i1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L1x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L2=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L2x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L3=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L3x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L4=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L4x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L5=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L5x=atoi(N1);
			N1="";
			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L8=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L8x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L6=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L6x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L7=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6L7x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6Taught=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6MotPos=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6Xtra=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6Type=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6Black=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ6Profile=atoi(N1);
			N1="";
			break;

	case 7:

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7a=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7b=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7c=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7d=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7e=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7f=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7g=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7h=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7i=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7j=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7k=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7l=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7b1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7c1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7d1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7e1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7f1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7g1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7h1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7i1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L1x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L2=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L2x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L3=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L3x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L4=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L4x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L5=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L5x=atoi(N1);
			N1="";
			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L8=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L8x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L6=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L6x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L7=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7L7x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7Taught=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7MotPos=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7Xtra=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7Type=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7Black=atoi(N1);
			N1="";

DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ7Profile=atoi(N1);
			N1="";
			break;

	case 8:

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8a=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8b=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8c=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8d=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8e=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8f=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8g=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8h=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8i=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8j=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8k=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8l=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8b1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8c1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8d1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8e1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8f1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8g1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8h1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8i1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L1x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L2=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L2x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L3=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L3x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L4=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L4x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L5=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L5x=atoi(N1);
			N1="";
			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L8=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L8x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L6=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L6x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L7=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8L7x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8Taught=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8MotPos=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8Xtra=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8Type=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8Black=atoi(N1);
			N1="";

DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ8Profile=atoi(N1);
			N1="";
			break;

	case 9:

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9a=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9b=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9c=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9d=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9e=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9f=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9g=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9h=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9i=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9j=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9k=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9l=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9b1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9c1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9d1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9e1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9f1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9g1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9h1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9i1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L1x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L2=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L2x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L3=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L3x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L4=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L4x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L5=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L5x=atoi(N1);
			N1="";
			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L8=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L8x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L6=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L6x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L7=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9L7x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9Taught=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9MotPos=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9Xtra=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9Type=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9Black=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ9Profile=atoi(N1);
			N1="";
			break;

	case 10:

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10a=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10b=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10c=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10d=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10e=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10f=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10g=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10h=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10i=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10j=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10k=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10l=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10b1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10c1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10d1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10e1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10f1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10g1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10h1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10i1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L1=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L1x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L2=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L2x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L3=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L3x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L4=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L4x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L5=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L5x=atoi(N1);
			N1="";
			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L8=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L8x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L6=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L6x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L7=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10L7x=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10Taught=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10MotPos=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10Xtra=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10Type=atoi(N1);
			N1="";

			DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10Black=atoi(N1);
			N1="";

DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10Profile=atoi(N1);
			N1="";
			break;

		}

		DataFile.Close();
		}
		if(LoadOK)
		{
			switch(r)
			{
				case 1:
					LoadPic(1);
				break;

				case 2:
					LoadPic(2);
				break;

				case 3:
					LoadPic(3);
				break;

				case 4:
					LoadPic(4);
				break;

				case 5:
					LoadPic(5);
				break;

				case 6:
					LoadPic(6);
				break;

				case 7:
					LoadPic(7);
				break;

				case 8:
					LoadPic(8);
				break;

				case 9:
					LoadPic(9);
				break;

				case 10:
					LoadPic(10);
				break;
			}
		}
	}

	
}

void Database::OnOK() 
{
	pframe->m_pjob->LoadJobs();
	
	CDialog::OnOK();
}


void Database::OnLoad2() 
{
	int result=AfxMessageBox("Do you want to write over the existing job", MB_YESNO);
	if(IDYES!=result) return;	
}
