// CockedLabel.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "CockedLabel.h"
#include "mainfrm.h"
#include "xaxisview.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCockedLabel dialog


CCockedLabel::CCockedLabel(CWnd* pParent /*=NULL*/)
	: CDialog(CCockedLabel::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCockedLabel)
	m_sens = _T("");
	m_yref1 = _T("");
	m_yref2 = _T("");
	m_yref3 = _T("");
	m_yref4 = _T("");
	//}}AFX_DATA_INIT
}


void CCockedLabel::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCockedLabel)
	DDX_Control(pDX, IDC_FINEMOD, m_fineMode);
	DDX_Text(pDX, IDC_SENMIDD, m_sens);
	DDX_Text(pDX, IDC_YREF1, m_yref1);
	DDX_Text(pDX, IDC_YREF2, m_yref2);
	DDX_Text(pDX, IDC_YREF3, m_yref3);
	DDX_Text(pDX, IDC_YREF4, m_yref4);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCockedLabel, CDialog)
	//{{AFX_MSG_MAP(CCockedLabel)
	ON_BN_CLICKED(IDC_YREFUP1, OnYrefup1)
	ON_BN_CLICKED(IDC_YREFDW1, OnYrefdw1)
	ON_BN_CLICKED(IDC_YREFUP2, OnYrefup2)
	ON_BN_CLICKED(IDC_YREFDW2, OnYrefdw2)
	ON_BN_CLICKED(IDC_YREFUP3, OnYrefup3)
	ON_BN_CLICKED(IDC_YREFDW3, OnYrefdw3)
	ON_BN_CLICKED(IDC_YREFUP4, OnYrefup4)
	ON_BN_CLICKED(IDC_YREFDW4, OnYrefdw4)
	ON_BN_CLICKED(IDC_FINEMOD, OnFinemod)
	ON_BN_CLICKED(IDC_SENMIDDUP, OnSenmiddup)
	ON_BN_CLICKED(IDC_SENMIDDDW, OnSenmidddw)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCockedLabel message handlers

void CCockedLabel::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

BOOL CCockedLabel::OnInitDialog() 
{
	CDialog::OnInitDialog();
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pCockedLabel=this;
	theapp=(CBottleApp*)AfxGetApp();

	camHeight	=	theapp->camWidth[1]; // 1024
	camWidth	=	theapp->camHeight[1]/3;  // 408
	res			=	1;
	
	int ncam = 1;
	m_yref1.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]);
	 
	ncam = 2;
	m_yref2.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]);
	ncam = 3;
	m_yref3.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]);
	ncam = 4;
	m_yref4.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]);

	m_sens.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].sen_Cocked_Label);

	UpdateData(false);		
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCockedLabel::OnYrefup1() 
{
	//theapp->jobinfo[jobNum].MidRef_cockedLabel[1];

	int ncam = 1;
	theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]=4;}

		m_yref1.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]);
		UpdateData(false);	
}

void CCockedLabel::OnYrefdw1() 
{
	int ncam = 1;
	theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]+=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]>camHeight)
	{theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]=camHeight-8;}

	m_yref1.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]);
		UpdateData(false);		
}

void CCockedLabel::OnYrefup2() 
{
	int ncam = 2;
	theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]=4;}

		m_yref2.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]);
		UpdateData(false);	
	
}

void CCockedLabel::OnYrefdw2() 
{
	int ncam = 2;
	theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]+=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]>camHeight)
	{theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]=camHeight-8;}

	m_yref2.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]);
		UpdateData(false);		
	
}

void CCockedLabel::OnYrefup3() 
{
		int ncam = 3;
	theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]=4;}

		m_yref3.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]);
		UpdateData(false);	
	
}

void CCockedLabel::OnYrefdw3() 
{
		int ncam = 3;
	theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]+=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]>camHeight)
	{theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]=camHeight-8;}

	m_yref3.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]);
		UpdateData(false);		
	
}

void CCockedLabel::OnYrefup4() 
{
		int ncam = 4;
	theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]=4;}

		m_yref4.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]);
		UpdateData(false);	
	
}

void CCockedLabel::OnYrefdw4() 
{
	int ncam = 4;
	theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]+=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]>camHeight)
	{theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]=camHeight-8;}

	m_yref4.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].MidRef_cockedLabel[ncam]);
		UpdateData(false);		
	
}

void CCockedLabel::OnFinemod() 
{
	if(res==1)	{m_fineMode.SetWindowText("Fast"); CheckDlgButton(IDC_FINEMOD,1); res=10;	}
	else		{m_fineMode.SetWindowText("Slow"); CheckDlgButton(IDC_FINEMOD,0); res=1;	}
	
	UpdateData(false);		
	
}

void CCockedLabel::OnSenmiddup() 
{
	

	theapp->jobinfo[pframe->CurrentJobNum].sen_Cocked_Label+=res;
	
//	if(theapp->jobinfo[pframe->CurrentJobNum].sen_Cocked_Label>250)
//	{theapp->jobinfo[pframe->CurrentJobNum].sen_Cocked_Label=250;}


	m_sens.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].sen_Cocked_Label);

	UpdateData(false);		
	
}

void CCockedLabel::OnSenmidddw() 
{


	theapp->jobinfo[pframe->CurrentJobNum].sen_Cocked_Label-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].sen_Cocked_Label<10)
	{theapp->jobinfo[pframe->CurrentJobNum].sen_Cocked_Label=10;}

	m_sens.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].sen_Cocked_Label);

		UpdateData(false);
}
