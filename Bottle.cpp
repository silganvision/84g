// Bottle.cpp : Defines the class behaviors for the application.
//


#include "stdafx.h"
#include "Bottle.h"

#include "MainFrm.h"
#include "xaxisview.h"
#include "ChildFrm.h"
#include "BottleDoc.h"
#include "BottleView.h"
#include "splitter.h"
//#include "Camera.h"

#include "LimitSingleInstance.h"
#include "CockedLabel.h"

#ifdef _DEBUG
#define new DEBUG_NEW

#undef THIS_FILE
static char THIS_FILE[] = __FILE__;




#endif

/////////////////////////////////////////////////////////////////////////////
// CBottleApp

BEGIN_MESSAGE_MAP(CBottleApp, CWinApp)
	//{{AFX_MSG_MAP(CBottleApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////////////////////////
//	Create Mutex to ensure that only one instance of the application is running at a time
//////////////////////////////////////////////////////////////////////////////////////////
CLimitSingleInstance instance(_T("51R85"));
/////////////////////////////////////////////////////////////////////////////
// CBottleApp construction
	//static const CLSID clsid =
//{ 0xd1efeb32, 0x270c, 0x47aa, { 0x81, 0x3c, 0xa2, 0x32, 0x5a, 0xd5, 0xc9, 0xa3 } };

CBottleApp::CBottleApp()
{
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CBottleApp object

CBottleApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CBottleApp initialization

BOOL CBottleApp::InitInstance()
{

	//	Exit if there is another running instance--------------------------------------
	if(instance.IsAnotherInstanceRunning())
		return FALSE;

//	camsPresent=true; serPresent=true;
//	camsPresent=false; serPresent=false;
	camsPresent = true; 
	serPresent = true;
#ifdef _DEBUG
	serPresent = false;
	camsPresent = false;

#endif

	//waistAvailable = false;
	showLargeImages = false;
	showLargeImages2 = false;

	Temperature = 0.0;

	Cam1SelectedForWaist = true;
	Cam2SelectedForWaist = true;

	saveOneSetCol	=	false;
	saveManySetCol	=	false;
	picNum			=	109;
	_CheckGPIOReg	=	true;

	//timedTrigger	=	60;	//60	Simulating 1000	Bott/Min BAD
	//timedTrigger	=	66;	//66	Simulating 900	Bott/Min
	//timedTrigger	=	75;	//75	Simulating 800	Bott/Min
	//timedTrigger	=	85;	//85	Simulating 700	Bott/Min
	//timedTrigger	=	100;//100	Simulating 600	Bott/Min 3/2400 BOTTLES
	//timedTrigger	=	120;//120	Simulating 500	Bott/Min OK
	//timedTrigger	=	150;//150	Simulating 400	Bott/Min OK
	timedTrigger	=	500;//200	Simulating 300	Bott/Min OK
	//timedTrigger	=	400;//400	Simulating 150	Bott/Min OK

	inspDelay		=	20;
	//cam5Enable	=	true;

	savedTotal		=	0;
	statsOpen		=	true;
	commProblem		=	true;
	Total24			=	0;

	lostCam			=	0;
	StartDelay		=	true;
	saveOneC		=	false;
	showCam5		=	false;
	capOpen			=	false;
	tearOpen		=	false;

	//bandwidth		=	62;
	lock			=	true;
	DidTriggerOnce	=	false;

	AfxEnableControlContainer();

	//Standard initialization
	//If you are not using these features and wish to reduce the size
	//of your final executable, you should remove from the following
	//the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	SetRegistryKey(_T("Silgan Closures"));
this->m_pszRegistryKey;
	waistAvailable = GetProfileInt("waistEnabled", "Current", 0) > 0;
	alignAvailable = GetProfileInt("alignmentEnabled", "Current", 0) > 0;
	sleeverEnable = GetProfileInt("sleeverEnable", "Current", 0) > 0;
	cam5Enable = GetProfileInt("cam5enable",	"Current", 1) > 0;

	int cHeight = waistAvailable ? 1288 : 1024;
	int cWidth = waistAvailable ? 964 : 408;
	int cYOffset = cWidth == 408 ? 278 : 0;
	int cXOffset = 132;

	camWidth[1] = camWidth[2]=camWidth[3]=camWidth[4]=cWidth;
	camHeight[1] = camHeight[2]=camHeight[3]=camHeight[4]=cHeight;
	camTop[1] = camTop[2] = camTop[3] = camTop[4] = cYOffset;
	camLeft[1] = camLeft[2] = camLeft[3] = camLeft[4] = cXOffset;

	camWidth[5] = cWidth;
	camHeight[5] = cHeight;
	camTop[5] = cYOffset;
	camLeft[5] = cXOffset;



	//This is to use a different hardware for the 51R85
	useDiff51R85Hardware = GetProfileInt("useDiff51R85Hardware", "Current", 0) > 0;

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.

	SavedTotalJobs=GetProfileInt("Jobs", "Current", 70);
	
	SavedTotalJobs=70;
	char folderName[255];

	for(int i=1; i<=SavedTotalJobs; i++)
	{
		sprintf(folderName, "JobArray\\Job%03d", i);
		jobinfo[i].jobname=GetProfileString(folderName, "jobname", jobinfo[i].jobname);
	}

	/*
	if(useDiff51R85Hardware) //using Welches Hardware
	{
		camLeft[1]=0; camTop[1]=180; camWidth[1]=1024; camHeight[1]=408;
		camLeft[2]=0; camTop[2]=180; camWidth[2]=1024; camHeight[2]=408;
		camLeft[3]=0; camTop[3]=180; camWidth[3]=1024; camHeight[3]=408;
		camLeft[4]=0; camTop[4]=180; camWidth[4]=1024; camHeight[4]=408;
		camLeft[5]=0; camTop[5]=192; camWidth[5]=1024; camHeight[5]=408;
	}
	else
	{
		camLeft[1]=0; camTop[1]=288; camWidth[1]=1024; camHeight[1]=408;
		camLeft[2]=0; camTop[2]=336; camWidth[2]=1024; camHeight[2]=408;
		camLeft[3]=0; camTop[3]=288; camWidth[3]=1024; camHeight[3]=408;
		camLeft[4]=0; camTop[4]=336; camWidth[4]=1024; camHeight[4]=408;
		camLeft[5]=0; camTop[5]=192; camWidth[5]=1024; camHeight[5]=408;
	}
	*/

	ReadRegistery(0);
	
	TRACE("Main Bottle Init 0x%X \n",AfxGetThread()->m_nThreadID);

	if(useDiff51R85Hardware)	//using Welches Hardware
	{
		camLeft[1]=0; camTop[1]=180; camWidth[1]=1024; camHeight[1]=408;
		camLeft[2]=0; camTop[2]=180; camWidth[2]=1024; camHeight[2]=408;
		camLeft[3]=0; camTop[3]=180; camWidth[3]=1024; camHeight[3]=408;
		camLeft[4]=0; camTop[4]=180; camWidth[4]=1024; camHeight[4]=408;
		camLeft[5]=0; camTop[5]=192; camWidth[5]=1024; camHeight[5]=408;
	}

	SecOff			=	false;
	SecOff = !securityEnable;

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(
		IDR_BOTTLETYPE,
		RUNTIME_CLASS(CBottleDoc),
		RUNTIME_CLASS(C3WaySplitterFrame), // custom MDI child frame
		RUNTIME_CLASS(CBottleView));
	AddDocTemplate(pDocTemplate);

	//m_server.ConnectTemplate(clsid, pDocTemplate, FALSE);
	// Register all OLE server factories as running.  This enables the
	//  OLE libraries to create objects from other applications.////
	//	COleTemplateServer::RegisterAll();
	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	//if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
	//	return FALSE;
	m_pMainWnd = pMainFrame;

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The main window has been initialized, so show and update it.
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();
	if(serPresent)
		pMainFrame->UpdatePLC();
	if(camsPresent)
	{

	}

	return TRUE;
}

void CBottleApp::ReadRegistery(int JobNum)
{
	CString min;
	CString max;

	this->m_pszRegistryKey;

	char folderName[255];

	sprintf(folderName, "JobArray\\Job%03d", JobNum);
	jobinfo[JobNum].jobname=GetProfileString(folderName, "jobname", jobinfo[JobNum].jobname);
	jobinfo[JobNum].taught=GetProfileInt(folderName,"taught",jobinfo[JobNum].taught) > 0;

	char inspnum[10];
//  ----------------------------------- Label Mating -----------------------------------

	jobinfo[JobNum].showLabelMating = GetProfileInt(folderName, "showLabelMating", 0) > 0;

	jobinfo[JobNum].labelMatingTeachColorLeftBound = GetProfileInt(folderName, "labelMatingTeachColorLeftBound", 70);
	jobinfo[JobNum].labelMatingTeachColorRightBound = GetProfileInt(folderName, "labelMatingTeachColorRightBound", 80);
	jobinfo[JobNum].labelMatingTeachColorTopBound = GetProfileInt(folderName, "labelMatingTeachColorTopBound", 200);
	jobinfo[JobNum].labelMatingTeachColorBottomBound = GetProfileInt(folderName, "labelMatingTeachColorBottomBound", 210);


	jobinfo[JobNum].labelMatingLeftBound = GetProfileInt(folderName, "labelMatingLeftBound", 10);
	jobinfo[JobNum].labelMatingRightBound = GetProfileInt(folderName, "labelMatingRightBound", 130);
	jobinfo[JobNum].labelMatingTopBound = GetProfileInt(folderName, "labelMatingTopBound", 200);
	jobinfo[JobNum].labelMatingBottomBound = GetProfileInt(folderName, "labelMatingBottomBound", 300);


	jobinfo[JobNum].labelMatingTaughtR = GetProfileInt(folderName,"labelMatingTaughtR", 0);
	jobinfo[JobNum].labelMatingTaughtG = GetProfileInt(folderName,"labelMatingTaughtG", 0);
	jobinfo[JobNum].labelMatingTaughtB = GetProfileInt(folderName,"labelMatingTaughtB", 0);

	jobinfo[JobNum].labelMatingTaughtRTolerance = GetProfileInt(folderName,"labelMatingTaughtRTolerance", 10);
	jobinfo[JobNum].labelMatingTaughtGTolerance = GetProfileInt(folderName,"labelMatingTaughtGTolerance", 10);
	jobinfo[JobNum].labelMatingTaughtBTolerance = GetProfileInt(folderName,"labelMatingTaughtBTolerance", 10);



//  ----------------------------------- Waist measurement cam1  -----------------------------------
//  Waist measurement 
//  Selection box
//	jobinfo[JobNum].cam1WaistSelectionLeftBound=GetProfileInt(folderName,"cam1WaistSelectionLeftBound", 0);
//	jobinfo[JobNum].cam1WaistSelectionRightBound=GetProfileInt(folderName,"cam1WaistSelectionRightBound", 259);
//	jobinfo[JobNum].cam1WaistSelectionTopBound=GetProfileInt(folderName,"cam1WaistSelectionTopBound", 334);
//	jobinfo[JobNum].cam1WaistSelectionBottomBound=GetProfileInt(folderName,"cam1WaistSelectionBottomBound", 0);


//  ----------------------------------- Waist measurement cam1  -----------------------------------
	jobinfo[JobNum].cam1WaistMeasurementUpperLeftBound=GetProfileInt(folderName,"cam1WaistMeasurementUpperLeftBound", 40);
	jobinfo[JobNum].cam1WaistMeasurementUpperRightBound=GetProfileInt(folderName,"cam1WaistMeasurementUpperRightBound", 100);
	jobinfo[JobNum].cam1WaistMeasurementUpperTopBound=GetProfileInt(folderName,"cam1WaistMeasurementUpperTopBound", 60);
	jobinfo[JobNum].cam1WaistMeasurementUpperBottomBound=GetProfileInt(folderName,"cam1WaistMeasurementUpperBottomBound", 80);

	jobinfo[JobNum].cam1WaistMeasurementLeftBound=GetProfileInt(folderName,"cam1WaistMeasurementLeftBound", 40);
	jobinfo[JobNum].cam1WaistMeasurementRightBound=GetProfileInt(folderName,"cam1WaistMeasurementRightBound", 100);
	jobinfo[JobNum].cam1WaistMeasurementTopBound=GetProfileInt(folderName,"cam1WaistMeasurementTopBound", 180);
	jobinfo[JobNum].cam1WaistMeasurementBottomBound=GetProfileInt(folderName,"cam1WaistMeasurementBottomBound", 200);

	jobinfo[JobNum].cam1WaistEdgeThreshold=GetProfileInt(folderName,"cam1WaistEdgeThreshold", 10);
	jobinfo[JobNum].cam1WaistVerticalSearchLength=GetProfileInt(folderName,"cam1WaistVerticalSearchLength", 3);
	jobinfo[JobNum].cam1WaistHorizontalSearchLength=GetProfileInt(folderName,"cam1WaistHorizontalSearchLength", 3);

	jobinfo[JobNum].cam1WaistAvgWindowSize=GetProfileInt(folderName,"cam1WaistAvgWindowSize", 3);
	jobinfo[JobNum].cam1WaistEdgeCoordinateDeviation=GetProfileInt(folderName,"cam1WaistEdgeCoordinateDeviation", 5);
	jobinfo[JobNum].cam1WaistfillTopToBottom=GetProfileInt(folderName,"cam1WaistfillTopToBottom", 1);

	jobinfo[JobNum].cam1WaistWidthOverlayStartX=GetProfileInt(folderName,"cam1WaistWidthOverlayStartX", 0); 
	jobinfo[JobNum].cam1WaistWidthOverlayEndX=GetProfileInt(folderName,"cam1WaistWidthOverlayEndX", 0);
	jobinfo[JobNum].cam1WaistWidthOverlayY=GetProfileInt(folderName,"cam1WaistWidthOverlayY", 0);

//  ----------------------------------- Waist measurement cam4  -----------------------------------
	jobinfo[JobNum].cam4WaistMeasurementUpperLeftBound=GetProfileInt(folderName,"cam4WaistMeasurementUpperLeftBound", 150);
	jobinfo[JobNum].cam4WaistMeasurementUpperRightBound=GetProfileInt(folderName,"cam4WaistMeasurementUpperRightBound", 210);
	jobinfo[JobNum].cam4WaistMeasurementUpperTopBound=GetProfileInt(folderName,"cam4WaistMeasurementUpperTopBound", 60);
	jobinfo[JobNum].cam4WaistMeasurementUpperBottomBound=GetProfileInt(folderName,"cam4WaistMeasurementUpperBottomBound", 80);

	jobinfo[JobNum].cam4WaistMeasurementLeftBound=GetProfileInt(folderName,"cam4WaistMeasurementLeftBound", 150);
	jobinfo[JobNum].cam4WaistMeasurementRightBound=GetProfileInt(folderName,"cam4WaistMeasurementRightBound", 210);
	jobinfo[JobNum].cam4WaistMeasurementTopBound=GetProfileInt(folderName,"cam4WaistMeasurementTopBound", 180);
	jobinfo[JobNum].cam4WaistMeasurementBottomBound=GetProfileInt(folderName,"cam4WaistMeasurementBottomBound", 200);

	jobinfo[JobNum].cam4WaistEdgeThreshold=GetProfileInt(folderName,"cam4WaistEdgeThreshold", 10);
	jobinfo[JobNum].cam4WaistVerticalSearchLength=GetProfileInt(folderName,"cam4WaistVerticalSearchLength", 3);
	jobinfo[JobNum].cam4WaistHorizontalSearchLength=GetProfileInt(folderName,"cam4WaistHorizontalSearchLength", 3);

	jobinfo[JobNum].cam4WaistAvgWindowSize=GetProfileInt(folderName,"cam4WaistAvgWindowSize", 3);
	jobinfo[JobNum].cam4WaistEdgeCoordinateDeviation=GetProfileInt(folderName,"cam4WaistEdgeCoordinateDeviation", 5);
	jobinfo[JobNum].cam4WaistfillTopToBottom=GetProfileInt(folderName,"cam4WaistfillTopToBottom", 1);

	jobinfo[JobNum].cam4WaistWidthOverlayStartX=GetProfileInt(folderName,"cam4WaistWidthOverlayStartX", 0); 
	jobinfo[JobNum].cam4WaistWidthOverlayEndX=GetProfileInt(folderName,"cam4WaistWidthOverlayEndX", 0);
	jobinfo[JobNum].cam4WaistWidthOverlayY=GetProfileInt(folderName,"cam4WaistWidthOverlayY", 0);

	jobinfo[JobNum].waistInspectionCameraToUse=GetProfileInt(folderName,"waistInspectionCameraToUse", 1); 


//  ----------------------------------- Waist measurement cam2  -----------------------------------
	jobinfo[JobNum].cam2WaistMeasurementUpperLeftBound=GetProfileInt(folderName,"cam2WaistMeasurementUpperLeftBound", 40);
	jobinfo[JobNum].cam2WaistMeasurementUpperRightBound=GetProfileInt(folderName,"cam2WaistMeasurementUpperRightBound", 100);
	jobinfo[JobNum].cam2WaistMeasurementUpperTopBound=GetProfileInt(folderName,"cam2WaistMeasurementUpperTopBound", 60);
	jobinfo[JobNum].cam2WaistMeasurementUpperBottomBound=GetProfileInt(folderName,"cam2WaistMeasurementUpperBottomBound", 80);

	jobinfo[JobNum].cam2WaistMeasurementLeftBound=GetProfileInt(folderName,"cam2WaistMeasurementLeftBound", 40);
	jobinfo[JobNum].cam2WaistMeasurementRightBound=GetProfileInt(folderName,"cam2WaistMeasurementRightBound", 100);
	jobinfo[JobNum].cam2WaistMeasurementTopBound=GetProfileInt(folderName,"cam2WaistMeasurementTopBound", 180);
	jobinfo[JobNum].cam2WaistMeasurementBottomBound=GetProfileInt(folderName,"cam2WaistMeasurementBottomBound", 200);

	jobinfo[JobNum].cam2WaistEdgeThreshold=GetProfileInt(folderName,"cam2WaistEdgeThreshold", 10);
	jobinfo[JobNum].cam2WaistVerticalSearchLength=GetProfileInt(folderName,"cam2WaistVerticalSearchLength", 3);
	jobinfo[JobNum].cam2WaistHorizontalSearchLength=GetProfileInt(folderName,"cam2WaistHorizontalSearchLength", 3);

	jobinfo[JobNum].cam2WaistAvgWindowSize=GetProfileInt(folderName,"cam2WaistAvgWindowSize", 3);
	jobinfo[JobNum].cam2WaistEdgeCoordinateDeviation=GetProfileInt(folderName,"cam2WaistEdgeCoordinateDeviation", 5);
	jobinfo[JobNum].cam2WaistfillTopToBottom=GetProfileInt(folderName,"cam2WaistfillTopToBottom", 1);

	jobinfo[JobNum].cam2WaistWidthOverlayStartX=GetProfileInt(folderName,"cam2WaistWidthOverlayStartX", 0); 
	jobinfo[JobNum].cam2WaistWidthOverlayEndX=GetProfileInt(folderName,"cam2WaistWidthOverlayEndX", 0);
	jobinfo[JobNum].cam2WaistWidthOverlayY=GetProfileInt(folderName,"cam2WaistWidthOverlayY", 0);

//  ----------------------------------- Waist measurement cam3  -----------------------------------
	jobinfo[JobNum].cam3WaistMeasurementUpperLeftBound=GetProfileInt(folderName,"cam3WaistMeasurementUpperLeftBound", 150);
	jobinfo[JobNum].cam3WaistMeasurementUpperRightBound=GetProfileInt(folderName,"cam3WaistMeasurementUpperRightBound", 210);
	jobinfo[JobNum].cam3WaistMeasurementUpperTopBound=GetProfileInt(folderName,"cam3WaistMeasurementUpperTopBound", 60);
	jobinfo[JobNum].cam3WaistMeasurementUpperBottomBound=GetProfileInt(folderName,"cam3WaistMeasurementUpperBottomBound", 80);

	jobinfo[JobNum].cam3WaistMeasurementLeftBound=GetProfileInt(folderName,"cam3WaistMeasurementLeftBound", 150);
	jobinfo[JobNum].cam3WaistMeasurementRightBound=GetProfileInt(folderName,"cam3WaistMeasurementRightBound", 210);
	jobinfo[JobNum].cam3WaistMeasurementTopBound=GetProfileInt(folderName,"cam3WaistMeasurementTopBound", 180);
	jobinfo[JobNum].cam3WaistMeasurementBottomBound=GetProfileInt(folderName,"cam3WaistMeasurementBottomBound", 200);

	jobinfo[JobNum].cam3WaistEdgeThreshold=GetProfileInt(folderName,"cam3WaistEdgeThreshold", 10);
	jobinfo[JobNum].cam3WaistVerticalSearchLength=GetProfileInt(folderName,"cam3WaistVerticalSearchLength", 3);
	jobinfo[JobNum].cam3WaistHorizontalSearchLength=GetProfileInt(folderName,"cam3WaistHorizontalSearchLength", 3);

	jobinfo[JobNum].cam3WaistAvgWindowSize=GetProfileInt(folderName,"cam3WaistAvgWindowSize", 3);
	jobinfo[JobNum].cam3WaistEdgeCoordinateDeviation=GetProfileInt(folderName,"cam3WaistEdgeCoordinateDeviation", 5);
	jobinfo[JobNum].cam3WaistfillTopToBottom=GetProfileInt(folderName,"cam3WaistfillTopToBottom", 1);

	jobinfo[JobNum].cam3WaistWidthOverlayStartX=GetProfileInt(folderName,"cam3WaistWidthOverlayStartX", 0); 
	jobinfo[JobNum].cam3WaistWidthOverlayEndX=GetProfileInt(folderName,"cam3WaistWidthOverlayEndX", 0);
	jobinfo[JobNum].cam3WaistWidthOverlayY=GetProfileInt(folderName,"cam3WaistWidthOverlayY", 0);

	//---------------------------------------------------------------------------------
	//Transition label Integrity method
	jobinfo[JobNum].tgYpattern=GetProfileInt(folderName,"tgYpattern", 0);

	//Transition label Integrity method
	jobinfo[JobNum].labelIntegrityUseMethod1=GetProfileInt(folderName,"labelIntegrityUseMethod1", 0) > 0;
	jobinfo[JobNum].labelIntegrityUseMethod2=GetProfileInt(folderName,"labelIntegrityUseMethod2", 0) > 0;
	jobinfo[JobNum].labelIntegrityUseMethod3=GetProfileInt(folderName,"labelIntegrityUseMethod3", 0) > 0;
	jobinfo[JobNum].labelIntegrityUseMethod4=GetProfileInt(folderName,"labelIntegrityUseMethod4", 0) > 0;
	jobinfo[JobNum].labelIntegrityUseMethod5=GetProfileInt(folderName,"labelIntegrityUseMethod5", 0) > 0;
	jobinfo[JobNum].labelIntegrityUseMethod6=GetProfileInt(folderName,"labelIntegrityUseMethod6", 0) > 0;

	//Method5 - Finding reference
	jobinfo[JobNum].boxYt=GetProfileInt(folderName,"boxYt",100);
	jobinfo[JobNum].boxWt=GetProfileInt(folderName,"boxWt",100);
	jobinfo[JobNum].boxHt=GetProfileInt(folderName,"boxHt",200);
	
	jobinfo[JobNum].boxYb=GetProfileInt(folderName,"boxYb",300);
	jobinfo[JobNum].boxWb=GetProfileInt(folderName,"boxWb",100);
	jobinfo[JobNum].boxHb=GetProfileInt(folderName,"boxHb",200);

	//Seam Variable
	jobinfo[JobNum].seamW=GetProfileInt(folderName,"seamW",60);
	jobinfo[JobNum].seamH=GetProfileInt(folderName,"seamH",150);
	jobinfo[JobNum].seamOffX=GetProfileInt(folderName,"seamOffX",150);
	jobinfo[JobNum].seamOffY=GetProfileInt(folderName,"seamOffY",10);

	jobinfo[JobNum].avoidOffX=GetProfileInt(folderName,"avoidOffX",30);

	//Stitching Variables
	jobinfo[JobNum].stitchY[1]=GetProfileInt(folderName,"stitchY1",60);
	jobinfo[JobNum].stitchY[2]=GetProfileInt(folderName,"stitchY2",60);
	jobinfo[JobNum].stitchY[3]=GetProfileInt(folderName,"stitchY3",60);
	jobinfo[JobNum].stitchY[4]=GetProfileInt(folderName,"stitchY4",60);
	jobinfo[JobNum].stitchW=GetProfileInt(folderName,"stitchW",84);
	jobinfo[JobNum].stitchH=GetProfileInt(folderName,"stitchH",100);

	jobinfo[JobNum].senEdges=GetProfileInt(folderName,"senEdges",10);

	jobinfo[JobNum].senMidd=GetProfileInt(folderName,"senMidd",150);
	jobinfo[JobNum].MiddImg=GetProfileInt(folderName,"MiddImg",1);

	jobinfo[JobNum].stDarkImg=GetProfileInt(folderName,"stDarkImg",1);
	jobinfo[JobNum].stLighImg=GetProfileInt(folderName,"stLighImg",1);
	jobinfo[JobNum].stEdgeImg=GetProfileInt(folderName,"stEdgeImg",1);

	jobinfo[JobNum].stLighSen=GetProfileInt(folderName,"stLighSen", 54);
	jobinfo[JobNum].stDarkSen=GetProfileInt(folderName,"stDarkSen", 40);
	jobinfo[JobNum].stEdgeSen=GetProfileInt(folderName,"stEdgeSen", 62);

	jobinfo[JobNum].showStLigh=GetProfileInt(folderName, "showStLigh",0) > 0;
	jobinfo[JobNum].showStDark=GetProfileInt(folderName, "showStDark",0) > 0;
	jobinfo[JobNum].showStEdges=GetProfileInt(folderName,"showStEdges",0) > 0;
	jobinfo[JobNum].showStFinal=GetProfileInt(folderName,"showStFinal",1) > 0;
	jobinfo[JobNum].showStbw=GetProfileInt(folderName,"showStbw",0) > 0;


	jobinfo[JobNum].useStLigh=GetProfileInt(folderName,"useStLigh", 1) > 0;
	jobinfo[JobNum].useStDark=GetProfileInt(folderName,"useStDark", 0) > 0;
	jobinfo[JobNum].useStEdge=GetProfileInt(folderName,"useStEdge", 0) > 0;

	jobinfo[JobNum].EdgeTransition=GetProfileInt(folderName,"EdgeTransition",0);

	jobinfo[JobNum].SpliceLookFor=GetProfileInt(folderName,"SpliceLookFor",0);

	jobinfo[JobNum].filterSelPatt=GetProfileInt(folderName,"filterSelPatt",1);

	jobinfo[JobNum].filterSelPattCap=GetProfileInt(folderName,"filterSelPattCap",1);

	jobinfo[JobNum].filterSelShifted=GetProfileInt(folderName,"filterSelShifted",1);
	jobinfo[JobNum].filterSelNolabel=GetProfileInt(folderName,"filterSelNolabel",1);

	jobinfo[JobNum].filterSelSplice=GetProfileInt(folderName,"filterSelSplice",1);

	jobinfo[JobNum].useRing=GetProfileInt(folderName,"usering", 1) > 0;


	//if(jobinfo[JobNum].taught=1;
	if(jobinfo[JobNum].taught)
	{
		jobinfo[JobNum].lightLevel1=GetProfileInt(folderName,"lightLevel1",jobinfo[JobNum].lightLevel1);
		jobinfo[JobNum].lightLevel2=GetProfileInt(folderName,"lightLevel2",jobinfo[JobNum].lightLevel2);
		jobinfo[JobNum].lightLevel3=GetProfileInt(folderName,"lightLevel3",jobinfo[JobNum].lightLevel3);
		jobinfo[JobNum].lightLevel4=GetProfileInt(folderName,"lightLevel4",jobinfo[JobNum].lightLevel4);
		jobinfo[JobNum].lightLevel5=GetProfileInt(folderName,"lightLevel5",jobinfo[JobNum].lightLevel5);

		if(jobinfo[JobNum].lightLevel1 >=25) jobinfo[JobNum].lightLevel1=25;
		if(jobinfo[JobNum].lightLevel2 >=25) jobinfo[JobNum].lightLevel2=25;
		if(jobinfo[JobNum].lightLevel3 >=25) jobinfo[JobNum].lightLevel3=25;
		if(jobinfo[JobNum].lightLevel4 >=25) jobinfo[JobNum].lightLevel4=25;
		if(jobinfo[JobNum].lightLevel5 >=25) jobinfo[JobNum].lightLevel5=25;

		//method5
		jobinfo[JobNum].method5_Rt=GetProfileInt(folderName, "method5_Rt", jobinfo[JobNum].method5_Rt);
		jobinfo[JobNum].method5_Gt=GetProfileInt(folderName, "method5_Gt", jobinfo[JobNum].method5_Gt);
		jobinfo[JobNum].method5_Bt=GetProfileInt(folderName, "method5_Bt", jobinfo[JobNum].method5_Bt);
		jobinfo[JobNum].method5_Ht=GetProfileInt(folderName, "method5_Ht", jobinfo[JobNum].method5_Ht);

		jobinfo[JobNum].method5_Rb=GetProfileInt(folderName, "method5_Rb", jobinfo[JobNum].method5_Rb);
		jobinfo[JobNum].method5_Gb=GetProfileInt(folderName, "method5_Gb", jobinfo[JobNum].method5_Gb);
		jobinfo[JobNum].method5_Bb=GetProfileInt(folderName, "method5_Bb", jobinfo[JobNum].method5_Bb);
		jobinfo[JobNum].method5_Hb=GetProfileInt(folderName, "method5_Hb", jobinfo[JobNum].method5_Hb);



		jobinfo[JobNum].vertBarX=GetProfileInt(folderName, "vertbarx", jobinfo[JobNum].vertBarX);
		jobinfo[JobNum].midBarY=GetProfileInt(folderName, "midbary", jobinfo[JobNum].midBarY);
		jobinfo[JobNum].neckBarY=GetProfileInt(folderName, "neckbary", jobinfo[JobNum].neckBarY);
		jobinfo[JobNum].lDist=GetProfileInt(folderName, "lDist", jobinfo[JobNum].lDist);

		if(jobinfo[JobNum].lDist>=100) jobinfo[JobNum].lDist=100;
		if(jobinfo[JobNum].lDist<=0) jobinfo[JobNum].lDist=0;

		jobinfo[JobNum].labelThresh=GetProfileInt(folderName, "labelthresh", jobinfo[JobNum].labelThresh);
		jobinfo[JobNum].profile=GetProfileInt(folderName, "profile", jobinfo[JobNum].profile);
		jobinfo[JobNum].motPos=GetProfileInt(folderName, "motPos", jobinfo[JobNum].motPos);
		jobinfo[JobNum].decCutEnable=GetProfileInt(folderName, "decCutEnable", jobinfo[JobNum].decCutEnable) > 0;
		jobinfo[JobNum].intThresh=GetProfileInt(folderName, "intThresh", jobinfo[JobNum].intThresh);

		jobinfo[JobNum].notchSen=GetProfileInt(folderName,"notchsen", 80);

		jobinfo[JobNum].labelSen=GetProfileInt(folderName,"labelsen", 180);
		//jobinfo[JobNum].notchOffset=GetProfileInt(folderName,"notchoffset", 50);

		jobinfo[JobNum].bottleStyle=GetProfileInt(folderName,"bottlestyle", 1);
		jobinfo[JobNum].type=GetProfileInt(folderName,"wtype", 1);
		jobinfo[JobNum].colorOffset2=GetProfileInt(folderName,"coloroffset2", 0);
		jobinfo[JobNum].lizardFilter=GetProfileInt(folderName,"lizardfilter", 0);

		jobinfo[JobNum].colorTargR=GetProfileInt(folderName,"colortargr", 0);
		jobinfo[JobNum].colorTargG=GetProfileInt(folderName,"colortargg", 0);
		jobinfo[JobNum].colorTargB=GetProfileInt(folderName,"colortargb", 0);
		savedColor = RGB(jobinfo[JobNum].colorTargR,jobinfo[JobNum].colorTargG,jobinfo[JobNum].colorTargB);


		jobinfo[JobNum].colorTargR2=GetProfileInt(folderName,"colortargr2", 0);
		jobinfo[JobNum].colorTargG2=GetProfileInt(folderName,"colortargg2", 0);
		jobinfo[JobNum].colorTargB2=GetProfileInt(folderName,"colortargb2", 0);
		savedColor2 = RGB(jobinfo[JobNum].colorTargR2,jobinfo[JobNum].colorTargG2,jobinfo[JobNum].colorTargB2);

		jobinfo[JobNum].bottleColor=GetProfileInt(folderName,"bottleColor",0);
		jobinfo[JobNum].colorFilter=GetProfileInt(folderName,"colorfilter", 40);
		jobinfo[JobNum].colorFilter2=GetProfileInt(folderName,"colorfilter2", 40);

		jobinfo[JobNum].sensEdge=GetProfileInt(folderName,"sensEdge", 50);
		jobinfo[JobNum].sensMet3=GetProfileInt(folderName,"sensMet3", 50);
		jobinfo[JobNum].heigMet3=GetProfileInt(folderName,"heigMet3", 150);
		jobinfo[JobNum].widtMet3=GetProfileInt(folderName,"widtMet3", 45);


		jobinfo[JobNum].heigMet5=GetProfileInt(folderName,"heigMet5", 12);
		jobinfo[JobNum].widtMet5=GetProfileInt(folderName,"widtMet5", 60);
	
		jobinfo[JobNum].prodName=GetProfileInt(folderName,"prodname", 1);

		jobinfo[JobNum].noLabelSen=GetProfileInt(folderName,"nolabelsen", 20);
		jobinfo[JobNum].noLabelOffset=GetProfileInt(folderName,"nolabeloffset", 1);
		jobinfo[JobNum].noLabelOffsetX[1]=GetProfileInt(folderName,"noLabelOffsetX1", 0);
		jobinfo[JobNum].noLabelOffsetX[2]=GetProfileInt(folderName,"noLabelOffsetX2", 0);
		jobinfo[JobNum].noLabelOffsetX[3]=GetProfileInt(folderName,"noLabelOffsetX3", 0);
		jobinfo[JobNum].noLabelOffsetX[4]=GetProfileInt(folderName,"noLabelOffsetX4", 0);
		jobinfo[JobNum].noLabelOffsetW=GetProfileInt(folderName,"noLabelOffsetW", 70*3);
		jobinfo[JobNum].noLabelOffsetH=GetProfileInt(folderName,"noLabelOffsetH", 15*3);

		jobinfo[JobNum].shiftedLabSen=GetProfileInt(folderName,"shiftedLabSen", 50);
		jobinfo[JobNum].EdgeLabSen=GetProfileInt(folderName,"EdgeLabSen", 16);

		jobinfo[JobNum].rotatePos=GetProfileInt(folderName,"rotatepos", 150);
		jobinfo[JobNum].insideOut=GetProfileInt(folderName,"insideout", 0) > 0;
		
		jobinfo[JobNum].neckStrength=GetProfileInt(folderName,"neckStrength", 50);

		jobinfo[JobNum].capStrength=GetProfileInt(folderName,"capstrength", 50);
		jobinfo[JobNum].capStrength2=GetProfileInt(folderName,"capstrength2", 50);
		jobinfo[JobNum].mainBodySen=GetProfileInt(folderName,"mainbodysen", 60);

		jobinfo[JobNum].orienArea=GetProfileInt(folderName,"orienarea", 0);
		jobinfo[JobNum].neckSen=GetProfileInt(folderName,"necksen", 0);
		jobinfo[JobNum].neckPos=GetProfileInt(folderName,"neckpos", 0);
		jobinfo[JobNum].underLipEnable=GetProfileInt(folderName,"underlipenable", 0) > 0;
		jobinfo[JobNum].neckInOut=GetProfileInt(folderName,"neckInOut", 0);
		jobinfo[JobNum].neckInOut2=GetProfileInt(folderName,"neckInOut2", 0);

		jobinfo[JobNum].greenFilter=GetProfileInt(folderName,"greenfilter", 50);
		jobinfo[JobNum].redFilter=GetProfileInt(folderName,"redfilter", 50);
		jobinfo[JobNum].blueFilter=GetProfileInt(folderName,"bluefilter", 50);
		jobinfo[JobNum].findWhite=GetProfileInt(folderName,"findwhite", 80);
		jobinfo[JobNum].fillOffset=GetProfileInt(folderName,"filloffset", 0);
		jobinfo[JobNum].fillSize=GetProfileInt(folderName,"fillsize", 20);
		jobinfo[JobNum].capColorSize=GetProfileInt(folderName,"capcolorsize", 20);
		jobinfo[JobNum].capColorOffset=GetProfileInt(folderName,"capcoloroffset", 20);

		jobinfo[JobNum].labelColorRectX=GetProfileInt(folderName,"labelcolorrectx", 0);
		jobinfo[JobNum].labelColorRectY=GetProfileInt(folderName,"labelcolorrecty", 0);
		jobinfo[JobNum].ellipseSen=GetProfileInt(folderName,"ellipsesen", 70);
		jobinfo[JobNum].rotOffset=GetProfileInt(folderName,"rotoffset", 8);

		jobinfo[JobNum].sportPos=GetProfileInt(folderName,"sportpos", 10);
		jobinfo[JobNum].sportSen=GetProfileInt(folderName,"sportsen", 50);
		jobinfo[JobNum].sport=GetProfileInt(folderName,"sport", 0) > 0;

		jobinfo[JobNum].taughtMiddle=GetProfileInt(folderName,"taughtMiddle",0) > 0;

		jobinfo[JobNum].RefForMiddleIsLeft[1]=GetProfileInt(folderName,"RefForMiddleIsLeft1",0) > 0;
		jobinfo[JobNum].RefForMiddleIsLeft[2]=GetProfileInt(folderName,"RefForMiddleIsLeft2",1) > 0;
		jobinfo[JobNum].RefForMiddleIsLeft[3]=GetProfileInt(folderName,"RefForMiddleIsLeft3",0) > 0;
		jobinfo[JobNum].RefForMiddleIsLeft[4]=GetProfileInt(folderName,"RefForMiddleIsLeft4",1) > 0;

		jobinfo[JobNum].taughtMiddleRefW[1]=GetProfileInt(folderName,"taughtMiddleRefW1",100);
		jobinfo[JobNum].taughtMiddleRefW[2]=GetProfileInt(folderName,"taughtMiddleRefW2",100);
		jobinfo[JobNum].taughtMiddleRefW[3]=GetProfileInt(folderName,"taughtMiddleRefW3",100);
		jobinfo[JobNum].taughtMiddleRefW[4]=GetProfileInt(folderName,"taughtMiddleRefW4",100);

		jobinfo[JobNum].taughtMiddleRefY[1]=GetProfileInt(folderName,"taughtMiddleRefY1",42);
		jobinfo[JobNum].taughtMiddleRefY[2]=GetProfileInt(folderName,"taughtMiddleRefY2",42);
		jobinfo[JobNum].taughtMiddleRefY[3]=GetProfileInt(folderName,"taughtMiddleRefY3",42);
		jobinfo[JobNum].taughtMiddleRefY[4]=GetProfileInt(folderName,"taughtMiddleRefY4",42);

		jobinfo[JobNum].middlesM[1]=GetProfileInt(folderName,"middlesM1",100);
		jobinfo[JobNum].middlesM[2]=GetProfileInt(folderName,"middlesM2",100);
		jobinfo[JobNum].middlesM[3]=GetProfileInt(folderName,"middlesM3",100);
		jobinfo[JobNum].middlesM[4]=GetProfileInt(folderName,"middlesM4",100);

		jobinfo[JobNum].middlesBarX[1]=GetProfileInt(folderName,"middlesBarX1",100);
		jobinfo[JobNum].middlesBarX[2]=GetProfileInt(folderName,"middlesBarX2",100);
		jobinfo[JobNum].middlesBarX[3]=GetProfileInt(folderName,"middlesBarX3",100);
		jobinfo[JobNum].middlesBarX[4]=GetProfileInt(folderName,"middlesBarX4",100);

		jobinfo[JobNum].middlesBarY[1]=GetProfileInt(folderName,"middlesBarY1",100);
		jobinfo[JobNum].middlesBarY[2]=GetProfileInt(folderName,"middlesBarY2",100);
		jobinfo[JobNum].middlesBarY[3]=GetProfileInt(folderName,"middlesBarY3",100);
		jobinfo[JobNum].middlesBarY[4]=GetProfileInt(folderName,"middlesBarY4",100);

		jobinfo[JobNum].tgXpatt[1]	=	GetProfileInt(folderName, "tgXpatt1", 4);
		jobinfo[JobNum].tgYpatt[1]	=	GetProfileInt(folderName, "tgYpatt1", 4);
		jobinfo[JobNum].tgXpatt[2]	=	GetProfileInt(folderName, "tgXpatt2", 4);
		jobinfo[JobNum].tgYpatt[2]	=	GetProfileInt(folderName, "tgYpatt2", 4);
		jobinfo[JobNum].tgXpatt[3]	=	GetProfileInt(folderName, "tgXpatt3", 4);
		jobinfo[JobNum].tgYpatt[3]	=	GetProfileInt(folderName, "tgYpatt3", 4);

		jobinfo[JobNum].meth6_patt1_x	=	GetProfileInt(folderName, "meth6_patt1_x", 4);
		jobinfo[JobNum].meth6_patt1_y	=	GetProfileInt(folderName, "meth6_patt1_y", 4);

		jobinfo[JobNum].meth6_patt2_x	=	GetProfileInt(folderName, "meth6_patt2_x", 4);
		jobinfo[JobNum].meth6_patt2_y	=	GetProfileInt(folderName, "meth6_patt2_y", 4);

		jobinfo[JobNum].meth6_patt3_x	=	GetProfileInt(folderName, "meth6_patt3_x", 4);
		jobinfo[JobNum].meth6_patt3_y	=	GetProfileInt(folderName, "meth6_patt3_y", 4);

		jobinfo[JobNum].showMiddles=GetProfileInt(folderName,"showMiddles",0) > 0;
		jobinfo[JobNum].findMiddles=GetProfileInt(folderName,"findMiddles",0) > 0;
		jobinfo[JobNum].showEdges=GetProfileInt(folderName,"showEdges",0) > 0;

		jobinfo[JobNum].ytopLimPatt=GetProfileInt(folderName,"ytopLimPatt",10);
		jobinfo[JobNum].ybotLimPatt=GetProfileInt(folderName,"ybotLimPatt",280);

		jobinfo[JobNum].shiftTopLim[1]=GetProfileInt(folderName,"shiftTopLim1",560);
		jobinfo[JobNum].shiftBotLim[1]=GetProfileInt(folderName,"shiftBotLim1",740);
		jobinfo[JobNum].shiftTopLim[2]=GetProfileInt(folderName,"shiftTopLim2",560);
		jobinfo[JobNum].shiftBotLim[2]=GetProfileInt(folderName,"shiftBotLim2",740);
		jobinfo[JobNum].shiftTopLim[3]=GetProfileInt(folderName,"shiftTopLim3",560);
		jobinfo[JobNum].shiftBotLim[3]=GetProfileInt(folderName,"shiftBotLim3",740);
		jobinfo[JobNum].shiftTopLim[4]=GetProfileInt(folderName,"shiftTopLim4",560);
		jobinfo[JobNum].shiftBotLim[4]=GetProfileInt(folderName,"shiftBotLim4",740);

		jobinfo[JobNum].shiftHorIniLim=GetProfileInt(folderName,"shiftHorIniLim",20);
		jobinfo[JobNum].shiftHorEndLim=GetProfileInt(folderName,"shiftHorEndLim",300);

		jobinfo[JobNum].shiftWidth=GetProfileInt(folderName,"shiftWidth",36);



		jobinfo[JobNum].showC5inPictures=GetProfileInt(folderName,"showC5inPictures",0) > 0;

		jobinfo[JobNum].showSetupI5=GetProfileInt(folderName,"showSetupI5",0) > 0;
		jobinfo[JobNum].showSetupI6=GetProfileInt(folderName,"showSetupI6",0) > 0;
		jobinfo[JobNum].showSetupI7=GetProfileInt(folderName,"showSetupI7",0) > 0;
		jobinfo[JobNum].showSetupI8=GetProfileInt(folderName,"showSetupI8",0) > 0;
		jobinfo[JobNum].showSetupI9=GetProfileInt(folderName,"showSetupI9",0) > 0;

		jobinfo[JobNum].showSetupI10=GetProfileInt(folderName,"showSetupI10",0) > 0;
		jobinfo[JobNum].showLabelMating=GetProfileInt(folderName,"showLabelMating",0) > 0;

		////////////////
		//Splice Settings
		jobinfo[JobNum].ySplice[1]	= GetProfileInt(folderName,"ysplice1",	176);
		jobinfo[JobNum].ySplice[2]	= GetProfileInt(folderName,"ysplice2",	176);
		jobinfo[JobNum].ySplice[3]	= GetProfileInt(folderName,"ysplice3",	176);
		jobinfo[JobNum].ySplice[4]	= GetProfileInt(folderName,"ysplice4",	176);
		jobinfo[JobNum].wSplice		= GetProfileInt(folderName,"wsplice",	280);
		jobinfo[JobNum].hSplice		= GetProfileInt(folderName,"hsplice",	744);
		jobinfo[JobNum].hSpliceSmBx	= GetProfileInt(folderName,"hSpliceSmBx",92);

		
		jobinfo[JobNum].spliceVar	= GetProfileInt(folderName,"spliceVar",700);



		jobinfo[JobNum].totinsp=10;

		inspctn[1].name="Cap L-R Height";

		if(jobinfo[JobNum].sport==1)			{inspctn[2].name="Cap Sport";}
		else									{inspctn[2].name="Cap Mid Height";}

		inspctn[3].name="CapColor";

		//Andrew Gawlik 2017-07-14 lip pattern will be inspection 4 regardless of bottle shape per discussion with Vivek
		//if(jobinfo[JobNum].bottleStyle==10)		{inspctn[4].name="Skewness";}
		//else									{inspctn[4].name="LipPattern";}
		inspctn[4].name="LipPattern";

		inspctn[5].name="LblHeight";

		if(jobinfo[JobNum].bottleStyle==3)		{inspctn[6].name="LblRotate";}
		else if(jobinfo[JobNum].bottleStyle==10){inspctn[6].name="ShiftLbl";}
		else									{inspctn[6].name="Spare";}

		inspctn[7].name	= "LblInteg";
		inspctn[8].name	= "NoLabel";
		inspctn[9].name	= "Splice";
		inspctn[10].name= "LblTear";
		inspctn[11].name= "Waist";

		//Andrew Gawlik 2017-07-14 skewness that was previously on I4 will be moved to I12 to take the place of alignment on square bottles
		//inspctn[12].name= "Alignment";
		if(jobinfo[JobNum].bottleStyle==10)		{inspctn[12].name="Skewness";}
		else									{inspctn[12].name="Alignment";}

		inspctn[1].type	= 2;
		inspctn[2].type	= 2;
		inspctn[3].type	= 2;
		inspctn[4].type	= 2;
		inspctn[5].type	= 2;
		inspctn[6].type	= 2;
		inspctn[7].type	= 2;
		inspctn[8].type	= 2;
		inspctn[9].type	= 2;
		inspctn[10].type= 2;

		for (int CurInspctn=1; CurInspctn<=12; CurInspctn++)
		{
			sprintf(inspnum, "\\Insp%03d", CurInspctn);
			strcat(folderName,inspnum);

			inspctn[CurInspctn].enable=GetProfileInt(folderName, "enable", 0);

			inspctn[CurInspctn].lmin=GetProfileInt(folderName, "lmin", 0);
			inspctn[CurInspctn].lmax=GetProfileInt(folderName, "lmax", 0);

			if(CurInspctn==9)
			{
				inspctn[CurInspctn].lmin=GetProfileInt(folderName, "lmin", 2);
				inspctn[CurInspctn].lmax=GetProfileInt(folderName, "lmax", 255);
			}

			inspctn[CurInspctn].lminB=GetProfileInt(folderName, "lminB", 0);
			inspctn[CurInspctn].lmaxB=GetProfileInt(folderName, "lmaxB", 0);

			inspctn[CurInspctn].lminC=GetProfileInt(folderName, "lminC", 0);
			inspctn[CurInspctn].lmaxC=GetProfileInt(folderName, "lmaxC", 0);
		}
	}
	else
	{
		jobinfo[JobNum].mainBodySen=65;
		jobinfo[JobNum].neckStrength=50;
		jobinfo[JobNum].capStrength=50;
		jobinfo[JobNum].capStrength2=50;
		jobinfo[JobNum].insideOut=0;
		jobinfo[JobNum].rotatePos=150;


		jobinfo[JobNum].prodName=1;
		jobinfo[JobNum].colorFilter=100;
		jobinfo[JobNum].colorFilter2=100;
		jobinfo[JobNum].colorOffset2=70;

		jobinfo[JobNum].labelSen=50;

		jobinfo[JobNum].notchSen=120;

		jobinfo[JobNum].intThresh=50;
		jobinfo[JobNum].decCutEnable=0;
		jobinfo[JobNum].lightLevel1=20;
		jobinfo[JobNum].lightLevel2=20;
		jobinfo[JobNum].lightLevel3=20;
		jobinfo[JobNum].lightLevel4=20;
		jobinfo[JobNum].lightLevel5=20;
		jobinfo[JobNum].vertBarX=60;
		jobinfo[JobNum].midBarY=60;
		jobinfo[JobNum].neckBarY=165;
		jobinfo[JobNum].capColorSize=12;
		jobinfo[JobNum].capColorOffset=100;
		jobinfo[JobNum].lDist=20;
		jobinfo[JobNum].labelThresh=50;
		jobinfo[JobNum].active=1;
		jobinfo[JobNum].u2Pos=100;
		jobinfo[JobNum].u2Thresh2=70;
		jobinfo[JobNum].u2Thresh1=80;
		jobinfo[JobNum].profile=-257;
		jobinfo[JobNum].sensitivity=100;
		jobinfo[JobNum].enable=1;
		jobinfo[JobNum].motPos=200;
		jobinfo[JobNum].totinsp=10;

		for (int CurInspctn=1; CurInspctn<=jobinfo[JobNum].totinsp; CurInspctn++)
		{
			sprintf(inspnum, "\\Insp%03d", CurInspctn);
			strcat(folderName,inspnum);
			inspctn[CurInspctn].lmin=0;
			inspctn[CurInspctn].lmax=0;

			if(CurInspctn==1 ){ inspctn[CurInspctn].lmin=100;inspctn[CurInspctn].lmax=130;}
			if(CurInspctn==2 ){ inspctn[CurInspctn].lmin=100;inspctn[CurInspctn].lmax=130;}
			if(CurInspctn==3 ){ inspctn[CurInspctn].lmin=0;inspctn[CurInspctn].lmax=30;}
			if(CurInspctn==4 ){inspctn[CurInspctn].lmax=100;inspctn[CurInspctn].lmin=70;}
			if(CurInspctn==5 ){ inspctn[CurInspctn].lmin=-5;inspctn[CurInspctn].lmax=5;}
			if(CurInspctn==6 ){ inspctn[CurInspctn].lmin=0;inspctn[CurInspctn].lmax=14;}
			if(CurInspctn==7 ){ inspctn[CurInspctn].lmin=70;inspctn[CurInspctn].lmax=100;}
			if(CurInspctn==8 ){ inspctn[CurInspctn].lmin=0;inspctn[CurInspctn].lmax=100;}
			if(CurInspctn==9) {inspctn[CurInspctn].lmin=180;inspctn[CurInspctn].lmax=800;}

			inspctn[CurInspctn].name=GetProfileString(folderName, "name", inspctn[CurInspctn].name);
		}

		SavedEncDist= 1000;
		SavedEncDur=50;

		SavedPassWord="60515";

		SavedIORejects=0;
		SavedConRejects=0;
	}

	SavedOPPassWord=GetProfileString("PassOP",		"Current", "123");
	SavedMANPassWord=GetProfileString("PassMAN",	"Current", "789");
	SavedSUPPassWord=GetProfileString("PassSUP",	"Current", "456");
	
	SavedEncSim=GetProfileInt("EncSim", "Current", 0);
	SavedDelay=GetProfileInt("SavedDelay", "Current", 50);
	SavedEncDist=GetProfileInt("EncDist", "Current", 1000);
	SavedEncDur=GetProfileInt("EncDur", "Current", 100);
	//SavedEjectDisable=GetProfileInt("Eject", "Current", 1);
	SavedPassWord=GetProfileString("Pass", "Current", "1301");
	SavedIORejects=GetProfileInt("IORejects", "Current", 0);
	SavedConRejects=GetProfileInt("ConRejects", "Current", 0);
	SavedConRejects2=GetProfileInt("ConRejects2", "Current", 0);
	SavedEncSim=GetProfileInt("EncSim", "Current", 0);
	SavedPhotoeye=GetProfileInt("Photo", "Current", 0);
	SavedDeadBand=GetProfileInt("deadband", "Current", 50);
	enableHeartBeat=GetProfileInt("heartbeat", "Current", 0) > 0;
	tropSystem=GetProfileInt("tropsystem", "Current", 1);
	SavedBottleSensor=GetProfileInt("bottlesensor", "Current", 0) > 0;
	SavedDBOffset=GetProfileInt("dboffset", "Current", 0);

	cam1Offset=GetProfileInt("cam1offset", "Current", 0);
	cam2Offset=GetProfileInt("cam2offset", "Current", 0);
	cam3Offset=GetProfileInt("cam3offset", "Current", 0);
	cam4Offset=GetProfileInt("cam4offset", "Current", 0);

	float Shutter;
	int temp;
	temp=GetProfileInt("shutter", "Current", 10); // was 5
	Shutter=float(temp);
	savedShutter=Shutter/10;

	float Shutter2;
	int temp2;
	temp2=GetProfileInt("shutter2",			"Current", 10); // was 5
	Shutter2=float(temp2);
	savedShutter2=Shutter2/10;

	savedZero=GetProfileInt("savedzero",	"Current", 120);

	CamSer1=GetProfileInt("camser1",		"Current", 8141140);
	CamSer2=GetProfileInt("camser2",		"Current", 8210107);
	CamSer3=GetProfileInt("camser3",		"Current", 8141053);
	CamSer4=GetProfileInt("camser4",		"Current", 8141136);
	CamSer5=GetProfileInt("camser5",		"Current", 8210118);

	securityEnable	=GetProfileInt("securityEnable","Current", 1) > 0;

	inspDelay=GetProfileInt("inspdelay",	"Current", 32);

	//inspDelay=14;
	hourClock=GetProfileInt("24hour",		"Current", 23);

	showMot=GetProfileInt("showmot",		"Current", 0) > 0;
	XLightOn=GetProfileInt("xlighton",		"Current", 0) > 0;
	boltOffset=GetProfileInt("boltoffset",	"Current", 0);
	boltOffset2=GetProfileInt("boltoffset2","Current", 0);
	DoNewLedge=GetProfileInt("donewledge",	"Current", 0) > 0;
	capEjectorInstalled=GetProfileInt("capejectorinstalled", "Current", 0) > 0;
	tempLimit=GetProfileInt("templimit",	"Current", 130);
	convRatio=GetProfileInt("convratio",	"Current", 1);

	//** Cam Parameters
	bandwidth=GetProfileInt("bandwidth","Current", 62);
	bandwidth5=GetProfileInt("bandwidth5","Current", 62);

	camLeft[1]=GetProfileInt("camLeft1",	"Current", 0);
	camLeft[2]=GetProfileInt("camLeft2",	"Current", 0);
	camLeft[3]=GetProfileInt("camLeft3",	"Current", 0);
	camLeft[4]=GetProfileInt("camLeft4",	"Current", 0);
	camLeft[5]=GetProfileInt("camLeft5",	"Current", 0);

	camTop[1]=GetProfileInt("camTop1",	"Current", 288);
	camTop[2]=GetProfileInt("camTop2",	"Current", 336);
	camTop[3]=GetProfileInt("camTop3",	"Current", 288);
	camTop[4]=GetProfileInt("camTop4",	"Current", 336);
	camTop[5]=GetProfileInt("camTop5",	"Current", 192);

//	camHeight[1]=GetProfileInt("camHeight1",	"Current", 432);
//	camHeight[2]=GetProfileInt("camHeight2",	"Current", 432);
//	camHeight[3]=GetProfileInt("camHeight3",	"Current", 432);
//	camHeight[4]=GetProfileInt("camHeight4",	"Current", 432);
//	camHeight[5]=GetProfileInt("camHeight5",	"Current", 432);

	lightDuration=GetProfileInt("lightDuration",	"Current", 12);
	ringDuration=GetProfileInt("ringDuration",	"Current", 12);
	holdTime1=GetProfileInt("holdtime1", "Current", 32);
	enabPicsToShow=GetProfileInt("enabPicsToShow", "Current", 0) > 0;

	whitebalance1Red=GetProfileInt("whitebalance1Red","Current", 626);
	whitebalance2Red=GetProfileInt("whitebalance2Red","Current", 626);
	whitebalance3Red=GetProfileInt("whitebalance3Red","Current", 626);
	whitebalance4Red=GetProfileInt("whitebalance4Red","Current", 626);
	whitebalance5Red=GetProfileInt("whitebalance5Red","Current", 650);
	whitebalance1Blue=GetProfileInt("whitebalance1Blue","Current", 691);
	whitebalance2Blue=GetProfileInt("whitebalance2Blue","Current", 691);
	whitebalance3Blue=GetProfileInt("whitebalance3Blue","Current", 691);
	whitebalance4Blue=GetProfileInt("whitebalance4Blue","Current", 691);
	whitebalance5Blue=GetProfileInt("whitebalance5Blue","Current", 500);

	passNotFinished=GetProfileInt("passNotFinished", "Current", 1) > 0;
	tempSelect			=GetProfileInt("tempSelect","Current", 2);//1:PLC 2:USB

	//////////////

	sizepattBy6		= 12;
	sizepattBy12	= 12;

	sizepattBy3_2W	= 20;//24
	sizepattBy3_2H	= 20;//24

	saveDefects = false;
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CBottleApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CBottleApp message handlers

