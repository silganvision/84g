#if !defined(AFX_SETUP_H__1C5E78C9_21B6_49D9_8B9C_E02013DADC53__INCLUDED_)
#define AFX_SETUP_H__1C5E78C9_21B6_49D9_8B9C_E02013DADC53__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Setup.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// Setup dialog

class Setup : public CDialog
{
	friend class CMainFrame;
	friend class CBottleApp;
	// Construction
public:

	long ControlChars;
	int bits;
	void CalculateChars();
	Setup(CWnd* pParent = NULL);   // standard constructor
	CMainFrame* pframe;
	CBottleApp* theapp;

// Dialog Data
	//{{AFX_DATA(Setup)
	enum { IDD = IDD_DELAY };
	CString	m_zvalue;
	CString	m_fvalue;
	//}}AFX_DATA

typedef struct  {
	int high; 
	int low; } Target;

typedef struct  {
	float x; 
	float y; } TwoPoint;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Setup)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Setup)
	virtual BOOL OnInitDialog();
	afx_msg void OnZin();
	afx_msg void OnZout();
	virtual void OnOK();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnAop();
	afx_msg void OnAcl();
	afx_msg void OnFin();
	afx_msg void OnFfar();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString s1;
	CString s2;
	CString s3;
	int i1;
	int i2;
	int i3;
	long Value;
	int Function;
	int CntrBits;
};

/////////////////////////////////////////////////////////////////////////////
// SDelay dialog

class SDelay : public CDialog
{
// Construction
public:
	void UpdatePLC();
	SDelay(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(SDelay)
	enum { IDD = IDD_DELAY };
	CString	m_sdelay;
	CString	m_editdb;
	//}}AFX_DATA
	CMainFrame* pframe;
	CBottleApp* theapp;

	long Value;
	int Function;
	int CntrBits;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SDelay)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(SDelay)
	afx_msg void OnMore();
	afx_msg void OnLess();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnMore3();
	afx_msg void OnLess3();
	afx_msg void OnMore5();
	afx_msg void OnLess4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETUP_H__1C5E78C9_21B6_49D9_8B9C_E02013DADC53__INCLUDED_)
