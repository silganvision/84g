// StitchSetup.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "StitchSetup.h"

#include "mainfrm.h"
#include "xaxisview.h"
#include "setupStLightFilter.h"
#include "setupStDarkFilter.h"
#include "setupStEdgeFilter.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// StitchSetup dialog


StitchSetup::StitchSetup(CWnd* pParent /*=NULL*/)
	: CDialog(StitchSetup::IDD, pParent)
{
	//{{AFX_DATA_INIT(StitchSetup)
	m_ypos1 = _T("");
	m_ypos2 = _T("");
	m_ypos3 = _T("");
	m_ypos4 = _T("");
	m_w1 = _T("");
	m_h1 = _T("");
	m_picNum = _T("");
	m_senEdges = _T("");
	m_seamH = _T("");
	m_seamW = _T("");
	m_seamOffX = _T("");
	m_seamOffY = _T("");
	m_avoidMid = _T("");
	//}}AFX_DATA_INIT
}


void StitchSetup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(StitchSetup)
	DDX_Control(pDX, ID_STEP, m_step);
	DDX_Text(pDX, IDC_YPOS1, m_ypos1);
	DDX_Text(pDX, IDC_YPOS2, m_ypos2);
	DDX_Text(pDX, IDC_YPOS3, m_ypos3);
	DDX_Text(pDX, IDC_YPOS4, m_ypos4);
	DDX_Text(pDX, IDC_W1, m_w1);
	DDX_Text(pDX, IDC_H1, m_h1);
	DDX_Text(pDX, IDC_PICTOSHOW, m_picNum);
	DDX_Text(pDX, IDC_SENEDGES, m_senEdges);
	DDX_Text(pDX, IDC_HEIGHTSEAM, m_seamH);
	DDX_Text(pDX, IDC_WIDTHSEAM, m_seamW);
	DDX_Text(pDX, IDC_OFFX, m_seamOffX);
	DDX_Text(pDX, IDC_OFFY, m_seamOffY);
	DDX_Text(pDX, IDC_AVOIDMIDD, m_avoidMid);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(StitchSetup, CDialog)
	//{{AFX_MSG_MAP(StitchSetup)
	ON_BN_CLICKED(IDC_YUP1, OnYup1)
	ON_BN_CLICKED(IDC_YDW1, OnYdw1)
	ON_BN_CLICKED(IDC_YUP2, OnYup2)
	ON_BN_CLICKED(IDC_YUP3, OnYup3)
	ON_BN_CLICKED(IDC_YUP4, OnYup4)
	ON_BN_CLICKED(IDC_YDW2, OnYdw2)
	ON_BN_CLICKED(IDC_YDW3, OnYdw3)
	ON_BN_CLICKED(IDC_YDW4, OnYdw4)
	ON_BN_CLICKED(IDC_WUP1, OnWup1)
	ON_BN_CLICKED(IDC_WDW1, OnWdw1)
	ON_BN_CLICKED(IDC_HUP1, OnHup1)
	ON_BN_CLICKED(IDC_HDW1, OnHdw1)
	ON_WM_TIMER()
	ON_BN_CLICKED(ID_STEP, OnStep)
	ON_BN_CLICKED(IDC_SHOWSTITBW, OnShowstitbw)
	ON_BN_CLICKED(IDC_PICTOSHOWUP2, OnPictoshowup2)
	ON_BN_CLICKED(IDC_PICTOSHOWDN, OnPictoshowdn)
	ON_BN_CLICKED(IDC_ENAPICSTOSHOW, OnEnapicstoshow)
	ON_BN_CLICKED(IDC_SHOWSTITEDGES, OnShowstitedges)
	ON_BN_CLICKED(IDC_SENEDGESUP, OnSenedgesup)
	ON_BN_CLICKED(IDC_SENEDGESDW, OnSenedgesdw)
	ON_BN_CLICKED(IDC_WIDTHSEAMUP, OnWidthseamup)
	ON_BN_CLICKED(IDC_WIDTHSEAMDW, OnWidthseamdw)
	ON_BN_CLICKED(IDC_HEIGHTSEAMUP, OnHeightseamup)
	ON_BN_CLICKED(IDC_HEIGHTSEAMDW, OnHeightseamdw)
	ON_BN_CLICKED(IDC_OFFXUP, OnOffxup)
	ON_BN_CLICKED(IDC_OFFXDW, OnOffxdw)
	ON_BN_CLICKED(IDC_OFFYUP, OnOffyup)
	ON_BN_CLICKED(IDC_OFFYDW, OnOffydw)
	ON_BN_CLICKED(IDC_FILTERLIGHT, OnFilterlight)
	ON_BN_CLICKED(IDC_FILTERDARK, OnFilterdark)
	ON_BN_CLICKED(IDC_FILTEREDGE, OnFilteredge)
	ON_BN_CLICKED(IDC_SHOWSTITLIGHT, OnShowstitlight)
	ON_BN_CLICKED(IDC_SHOWSTITDARK, OnShowstitdark)
	ON_BN_CLICKED(IDC_SHOWSTITFINAL, OnShowstitfinal)
	ON_BN_CLICKED(IDC_USEFILTERLIGHT, OnUsefilterlight)
	ON_BN_CLICKED(IDC_USEFILTERDARK, OnUsefilterdark)
	ON_BN_CLICKED(IDC_USEFILTEREDGE, OnUsefilteredge)
	ON_BN_CLICKED(IDC_AVOIDMIDDUP, OnAvoidmiddup)
	ON_BN_CLICKED(IDC_AVOIDMIDDDW, OnAvoidmidddw)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// StitchSetup message handlers


void StitchSetup::OnYup1() 
{
	int ncam=1;

	theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]-=resBox;
	if(theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]=4;}

	UpdateValues();		
}


void StitchSetup::OnYup2() 
{
	int ncam=2;

	theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]-=resBox;
	if(theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]=4;}

	UpdateValues();	
}

void StitchSetup::OnYup3() 
{
	int ncam=3;

	theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]-=resBox;
	if(theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]=4;}

	UpdateValues();	
}

void StitchSetup::OnYup4() 
{
	int ncam=4;

	theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]-=resBox;
	if(theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]=4;}

	UpdateValues();	
	
}

void StitchSetup::OnYdw1() 
{
	int ncam=1;

	theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]+=resBox;
	if( (theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]+
		 theapp->jobinfo[pframe->CurrentJobNum].stitchH)>heightCam)
	{theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]-=2*resBox;}

	UpdateValues();			
}

void StitchSetup::OnYdw2() 
{
	int ncam=2;

	theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]+=resBox;
	if( (theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]+
		 theapp->jobinfo[pframe->CurrentJobNum].stitchH)>heightCam)
	{theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]-=2*resBox;}

	UpdateValues();			
}

void StitchSetup::OnYdw3() 
{
	int ncam=3;

	theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]+=resBox;
	if( (theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]+
		 theapp->jobinfo[pframe->CurrentJobNum].stitchH)>heightCam)
	{theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]-=2*resBox;}

	UpdateValues();			
}

void StitchSetup::OnYdw4() 
{
	int ncam=4;

	theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]+=resBox;
	if( (theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]+
		 theapp->jobinfo[pframe->CurrentJobNum].stitchH)>heightCam)
	{theapp->jobinfo[pframe->CurrentJobNum].stitchY[ncam]-=2*resBox;}

	UpdateValues();			
}

void StitchSetup::OnWup1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stitchW+=resBoxWidth;

	if( theapp->jobinfo[pframe->CurrentJobNum].stitchW > lengthCam )
	{theapp->jobinfo[pframe->CurrentJobNum].stitchW-=resBoxWidth;}

	int remainder = theapp->jobinfo[pframe->CurrentJobNum].stitchW%12;

	if(remainder!=0)
	{theapp->jobinfo[pframe->CurrentJobNum].stitchW-=remainder;}

	UpdateValues();			
}

void StitchSetup::OnWdw1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stitchW-=resBoxWidth;

	if(theapp->jobinfo[pframe->CurrentJobNum].stitchW < 12 )
	{theapp->jobinfo[pframe->CurrentJobNum].stitchW=12;}

	int remainder = theapp->jobinfo[pframe->CurrentJobNum].stitchW%12;

	if(remainder!=0)
	{theapp->jobinfo[pframe->CurrentJobNum].stitchW-=remainder;}


	UpdateValues();		
}

void StitchSetup::OnHup1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stitchH+=resBox;

	if( ((theapp->jobinfo[pframe->CurrentJobNum].stitchY[1]+theapp->jobinfo[pframe->CurrentJobNum].stitchH)>heightCam) ||
		((theapp->jobinfo[pframe->CurrentJobNum].stitchY[2]+theapp->jobinfo[pframe->CurrentJobNum].stitchH)>heightCam) ||
		((theapp->jobinfo[pframe->CurrentJobNum].stitchY[3]+theapp->jobinfo[pframe->CurrentJobNum].stitchH)>heightCam) ||
		((theapp->jobinfo[pframe->CurrentJobNum].stitchY[4]+theapp->jobinfo[pframe->CurrentJobNum].stitchH)>heightCam)	)
	{theapp->jobinfo[pframe->CurrentJobNum].stitchH-=resBox;}

	UpdateValues();			
}

void StitchSetup::OnHdw1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stitchH-=resBox;
	if(theapp->jobinfo[pframe->CurrentJobNum].stitchH<4)
	{theapp->jobinfo[pframe->CurrentJobNum].stitchH=4;}

	UpdateValues();		
	
}

void StitchSetup::UpdateValues()
{
	pframe->m_pxaxis->StitchedLength = 4*theapp->jobinfo[pframe->CurrentJobNum].stitchW;
	pframe->m_pxaxis->StitchedHeight = theapp->jobinfo[pframe->CurrentJobNum].stitchH;

	pframe->m_pxaxis->calcTearCoordinates();

	SetTimer(1,300,NULL);
	UpdateDisplay();
}

void StitchSetup::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{
		KillTimer(1);
		pframe->SaveJob(pframe->CurrentJobNum);
	}
		
	CDialog::OnTimer(nIDEvent);
}

BOOL StitchSetup::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pstitchSetup=this;
	theapp=(CBottleApp*)AfxGetApp();

	//SetWindowPos(&wndTop,0,310,780,250,SWP_SHOWWINDOW | SWP_NOSIZE );

	lengthCam = MainDisplayImageWidth;
	heightCam = MainDisplayImageHeight;

	resBox = 3;
	resBoxWidth = 12;
	resSen=2;

	stepSlow=true;

	m_step.SetWindowText("Slow");

	UpdateDisplay();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void StitchSetup::OnStep() 
{
	if(stepSlow)	
	{
		stepSlow=false;m_step.SetWindowText("Fast"); 
		CheckDlgButton(ID_STEP,1); resBox=12;resBoxWidth=24;
		resSen=10;
	}
	else		
	{
		stepSlow=true;	m_step.SetWindowText("Slow"); 
		CheckDlgButton(ID_STEP,0); resBox=4; resBoxWidth=12;
		resSen=2;
	}
	
	UpdateData(false);		
}

void StitchSetup::UpdateDisplay()
{
	m_avoidMid.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].avoidOffX);
	m_seamOffX.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].seamOffX);
	m_seamOffY.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].seamOffY);

	m_seamH.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].seamH);
	m_seamW.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].seamW);

	m_senEdges.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].senEdges);

	m_w1.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].stitchW);
	m_h1.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].stitchH);

	m_ypos1.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].stitchY[1] );
	m_ypos2.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].stitchY[2] );
	m_ypos3.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].stitchY[3] );
	m_ypos4.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].stitchY[4] );

	////////////////

	if(theapp->jobinfo[pframe->CurrentJobNum].useStLigh)
									{CheckDlgButton(IDC_USEFILTERLIGHT,1);}
	else							{CheckDlgButton(IDC_USEFILTERLIGHT,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].useStDark)
									{CheckDlgButton(IDC_USEFILTERDARK,1);}
	else							{CheckDlgButton(IDC_USEFILTERDARK,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].useStEdge)
									{CheckDlgButton(IDC_USEFILTEREDGE,1);}
	else							{CheckDlgButton(IDC_USEFILTEREDGE,0);}

	////////////////

	if(theapp->jobinfo[pframe->CurrentJobNum].showStLigh)
									{CheckDlgButton(IDC_SHOWSTITLIGHT,1);}
	else							{CheckDlgButton(IDC_SHOWSTITLIGHT,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].showStDark)
									{CheckDlgButton(IDC_SHOWSTITDARK,1);}
	else							{CheckDlgButton(IDC_SHOWSTITDARK,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].showStEdges)
									{CheckDlgButton(IDC_SHOWSTITEDGES,1);}
	else							{CheckDlgButton(IDC_SHOWSTITEDGES,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].showStFinal)
									{CheckDlgButton(IDC_SHOWSTITFINAL,1);}
	else							{CheckDlgButton(IDC_SHOWSTITFINAL,0);}

	if(theapp->jobinfo[pframe->CurrentJobNum].showStbw)
									{CheckDlgButton(IDC_SHOWSTITBW,1);}
	else							{CheckDlgButton(IDC_SHOWSTITBW,0);}

	m_picNum.Format("%i", pframe->pictoShow);

	if(theapp->enabPicsToShow){CheckDlgButton(IDC_ENAPICSTOSHOW,1);}
	else							{CheckDlgButton(IDC_ENAPICSTOSHOW,0);}

	if(pframe->CurrentJobNum>0)
	{pframe->m_pxaxis->InvalidateRect(NULL,false);}

	UpdateData(false);		
}

void StitchSetup::OnPictoshowup2() 
{
	pframe->pictoShow++;	

	if(pframe->pictoShow>=49)	{pframe->pictoShow=49;}

	UpdateDisplay();
	
}

void StitchSetup::OnPictoshowdn() 
{
	pframe->pictoShow--;	

	if(pframe->pictoShow<=1)	{pframe->pictoShow=1;}
	
	UpdateDisplay();
	
}

void StitchSetup::OnEnapicstoshow() 
{
	if( theapp->enabPicsToShow)
	{CheckDlgButton(IDC_ENAPICSTOSHOW,0);	theapp->enabPicsToShow=false;}
	else
	{CheckDlgButton(IDC_ENAPICSTOSHOW,1);	theapp->enabPicsToShow=true;}
		
	UpdateData(false);	
}

void StitchSetup::OnShowstitedges() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].showStEdges)
	{
		CheckDlgButton(IDC_SHOWSTITEDGES,0); 
		theapp->jobinfo[pframe->CurrentJobNum].showStEdges=false;
	}
	else
	{
		CheckDlgButton(IDC_SHOWSTITLIGHT,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStLigh=false;

		CheckDlgButton(IDC_SHOWSTITDARK,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStDark=false;

		CheckDlgButton(IDC_SHOWSTITEDGES,1);
		theapp->jobinfo[pframe->CurrentJobNum].showStEdges=true;

		CheckDlgButton(IDC_SHOWSTITFINAL,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStFinal=false;

		CheckDlgButton(IDC_SHOWSTITBW,0); 
		theapp->jobinfo[pframe->CurrentJobNum].showStbw=false;
	}
		
	UpdateValues();	
}

void StitchSetup::OnSenedgesup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].senEdges+=resSen;

	if(theapp->jobinfo[pframe->CurrentJobNum].senEdges>=255)
	{theapp->jobinfo[pframe->CurrentJobNum].senEdges=255;}

	UpdateValues();	
}

void StitchSetup::OnSenedgesdw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].senEdges-=resSen;

	if(theapp->jobinfo[pframe->CurrentJobNum].senEdges<=1)
	{theapp->jobinfo[pframe->CurrentJobNum].senEdges=1;}

	UpdateValues();	
}

void StitchSetup::OnWidthseamup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].seamW+=resBox;

	if( theapp->jobinfo[pframe->CurrentJobNum].seamW > lengthCam )
	{theapp->jobinfo[pframe->CurrentJobNum].seamW-=resBox;}

	UpdateValues();	
}

void StitchSetup::OnWidthseamdw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].seamW-=resBox;
	if(theapp->jobinfo[pframe->CurrentJobNum].seamW < 12 )
	{theapp->jobinfo[pframe->CurrentJobNum].seamW=12;}

	UpdateValues();		
}

void StitchSetup::OnHeightseamup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].seamH+=resBox;

	//if(theapp->jobinfo[pframe->CurrentJobNum].seamH )
	//{theapp->jobinfo[pframe->CurrentJobNum].stitchH-=resBox;}

	UpdateValues();		
}

void StitchSetup::OnHeightseamdw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].seamH-=resBox;

	if( theapp->jobinfo[pframe->CurrentJobNum].seamH<12 )
	{theapp->jobinfo[pframe->CurrentJobNum].stitchH=12;}

	UpdateValues();		
}

void StitchSetup::OnOffxup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].seamOffX+=resBox;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].seamOffX>=400)
	{theapp->jobinfo[pframe->CurrentJobNum].seamOffX = 400;}

	UpdateValues();	
}

void StitchSetup::OnOffxdw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].seamOffX-=resBox;

	if(theapp->jobinfo[pframe->CurrentJobNum].seamOffX<=-400)
	{theapp->jobinfo[pframe->CurrentJobNum].seamOffX = -400;}

	UpdateValues();	
}

void StitchSetup::OnOffyup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].seamOffY+=resBox;

	UpdateValues();	
	
}

void StitchSetup::OnOffydw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].seamOffY-=resBox;

	UpdateValues();	
}



void StitchSetup::OnFilterlight() 
{
	setupStLightFilter setStLightFilter;	setStLightFilter.DoModal();
/*
	if(SAdvanced)	
	{setupStLightFilter setStLightFilter;	setStLightFilter.DoModal();}
	else
	{
		Security sec; sec.DoModal();
		if(SAdvanced)
		{setupStLightFilter setStLightFilter;	setStLightFilter.DoModal();}
	}*/
}

void StitchSetup::OnFilterdark() 
{
	setupStDarkFilter setStDarkFilter;	setStDarkFilter.DoModal();
/*
	if(SAdvanced)	
	{setupStDarkFilter setStDarkFilter;	setStDarkFilter.DoModal();}
	else
	{
		Security sec; sec.DoModal();
		if(SAdvanced)
		{setupStDarkFilter setStDarkFilter;	setStDarkFilter.DoModal();}
	}	*/
}

void StitchSetup::OnFilteredge() 
{
	setupStEdgeFilter setStEdgeFilter;	setStEdgeFilter.DoModal();

	/*
	if(SAdvanced)	
	{setupStEdgeFilter setStEdgeFilter;	setStEdgeFilter.DoModal();}
	else
	{
		Security sec; sec.DoModal();
		if(SAdvanced)
		{setupStEdgeFilter setStEdgeFilter;	setStEdgeFilter.DoModal();}
	}*/
}

void StitchSetup::OnShowstitlight() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].showStLigh)
	{
		CheckDlgButton(IDC_SHOWSTITLIGHT,0); 
		theapp->jobinfo[pframe->CurrentJobNum].showStLigh=false;
	}
	else
	{
		CheckDlgButton(IDC_SHOWSTITLIGHT,1);
		theapp->jobinfo[pframe->CurrentJobNum].showStLigh=true;

		CheckDlgButton(IDC_SHOWSTITDARK,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStDark=false;

		CheckDlgButton(IDC_SHOWSTITEDGES,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStEdges=false;

		CheckDlgButton(IDC_SHOWSTITFINAL,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStFinal=false;

		CheckDlgButton(IDC_SHOWSTITBW,0); 
		theapp->jobinfo[pframe->CurrentJobNum].showStbw=false;
	}
		
	UpdateValues();		
}

void StitchSetup::OnShowstitdark() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].showStDark)
	{
		CheckDlgButton(IDC_SHOWSTITDARK,0); 
		theapp->jobinfo[pframe->CurrentJobNum].showStDark=false;
	}
	else
	{
		CheckDlgButton(IDC_SHOWSTITLIGHT,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStLigh=false;

		CheckDlgButton(IDC_SHOWSTITDARK,1);
		theapp->jobinfo[pframe->CurrentJobNum].showStDark=true;

		CheckDlgButton(IDC_SHOWSTITEDGES,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStEdges=false;

		CheckDlgButton(IDC_SHOWSTITFINAL,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStFinal=false;

		CheckDlgButton(IDC_SHOWSTITBW,0); 
		theapp->jobinfo[pframe->CurrentJobNum].showStbw=false;
	}
		
	UpdateValues();		
}

void StitchSetup::OnShowstitfinal() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].showStFinal)
	{
		CheckDlgButton(IDC_SHOWSTITFINAL,0); 
		theapp->jobinfo[pframe->CurrentJobNum].showStFinal=false;
	}
	else
	{
		CheckDlgButton(IDC_SHOWSTITLIGHT,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStLigh=false;

		CheckDlgButton(IDC_SHOWSTITDARK,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStDark=false;

		CheckDlgButton(IDC_SHOWSTITEDGES,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStEdges=false;

		CheckDlgButton(IDC_SHOWSTITFINAL,1);
		theapp->jobinfo[pframe->CurrentJobNum].showStFinal=true;

		CheckDlgButton(IDC_SHOWSTITBW,0); 
		theapp->jobinfo[pframe->CurrentJobNum].showStbw=false;
	}
		
	UpdateValues();		
}


void StitchSetup::OnShowstitbw() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].showStbw)
	{
		CheckDlgButton(IDC_SHOWSTITBW,0); 
		theapp->jobinfo[pframe->CurrentJobNum].showStbw=false;
	}
	else
	{
		CheckDlgButton(IDC_SHOWSTITLIGHT,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStLigh=false;

		CheckDlgButton(IDC_SHOWSTITDARK,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStDark=false;

		CheckDlgButton(IDC_SHOWSTITEDGES,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStEdges=false;

		CheckDlgButton(IDC_SHOWSTITFINAL,0);
		theapp->jobinfo[pframe->CurrentJobNum].showStFinal=false;

		CheckDlgButton(IDC_SHOWSTITBW,1); 
		theapp->jobinfo[pframe->CurrentJobNum].showStbw=true;
	}
		
	UpdateValues();
}	

void StitchSetup::OnUsefilterlight() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].useStLigh)
	{
		CheckDlgButton(IDC_USEFILTERLIGHT,0); 
		theapp->jobinfo[pframe->CurrentJobNum].useStLigh=false;
	}
	else
	{
		CheckDlgButton(IDC_USEFILTERLIGHT,1);
		theapp->jobinfo[pframe->CurrentJobNum].useStLigh=true;
	}
		
	UpdateValues();			
}

void StitchSetup::OnUsefilterdark() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].useStDark)
	{
		CheckDlgButton(IDC_USEFILTERDARK,0); 
		theapp->jobinfo[pframe->CurrentJobNum].useStDark=false;
	}
	else
	{
		CheckDlgButton(IDC_USEFILTERDARK,1);
		theapp->jobinfo[pframe->CurrentJobNum].useStDark=true;
	}
		
	UpdateValues();				
}

void StitchSetup::OnUsefilteredge() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].useStEdge)
	{
		CheckDlgButton(IDC_USEFILTEREDGE,0); 
		theapp->jobinfo[pframe->CurrentJobNum].useStEdge=false;
	}
	else
	{
		CheckDlgButton(IDC_USEFILTEREDGE,1);
		theapp->jobinfo[pframe->CurrentJobNum].useStEdge=true;
	}
		
	UpdateValues();				
}

void StitchSetup::OnAvoidmiddup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].avoidOffX+=resBox;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].avoidOffX>=400)
	{theapp->jobinfo[pframe->CurrentJobNum].avoidOffX = 400;}

	UpdateValues();		
}

void StitchSetup::OnAvoidmidddw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].avoidOffX-=resBox;

	if(theapp->jobinfo[pframe->CurrentJobNum].avoidOffX<=0)
	{theapp->jobinfo[pframe->CurrentJobNum].avoidOffX = 0;}

	UpdateValues();		
}
