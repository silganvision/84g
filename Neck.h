#if !defined(AFX_NECK_H__0C379B8C_3A8D_402E_9231_50996F6B9650__INCLUDED_)
#define AFX_NECK_H__0C379B8C_3A8D_402E_9231_50996F6B9650__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Neck.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Neck dialog

class Neck : public CDialog
{
	friend class CMainFrame;
	friend class CBottleApp;
	// Construction
public:
	int res;
	Neck(CWnd* pParent = NULL);   // standard constructor

	CString m_enable;
	CBottleApp *theapp;
	CMainFrame* pframe;
// Dialog Data
	//{{AFX_DATA(Neck)
	enum { IDD = IDD_NECK };
	CString	m_editneckpos;
	CString	m_editnecksen;
	CString	m_editcappos;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Neck)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Neck)
	afx_msg void OnNeckup();
	afx_msg void OnNeckdn();
	afx_msg void OnNeckup2();
	afx_msg void OnNeckdn2();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnRadio1();
	afx_msg void OnCheck1();
	afx_msg void OnNeckout();
	afx_msg void OnNeckin();
	afx_msg void OnNeckoffup();
	afx_msg void OnNeckoffdn();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NECK_H__0C379B8C_3A8D_402E_9231_50996F6B9650__INCLUDED_)
