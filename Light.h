#if !defined(AFX_LIGHT_H__B6520F71_0F20_417D_8AFF_FC416739095B__INCLUDED_)
#define AFX_LIGHT_H__B6520F71_0F20_417D_8AFF_FC416739095B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Light.h : header file
//
//#include "pcrcam.h"
/////////////////////////////////////////////////////////////////////////////
// Light dialog

class Light : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
	// Construction
public:
	enum { NoCamera = 0, FrontCamera = 1, LeftCamera = 2, RightCamera = 4, TopCamera = 8, FifthCamera = 16, AllCameras = 31 };
	void UpdateDisplay();
	int Cams;
	void UpdatePLC();
	int OldLevel;
	int LightLevel1;
	int LightLevel2;
	int LightLevel3;
	int LightLevel4;
	int LightLevel5;
	Light(CWnd* pParent = NULL);   // standard constructor

	CBottleApp *theapp;
	CMainFrame* pframe;
	CString m_lenable;

	int Value;
	int Function;

//CICamera *cam0;
// Dialog Data
	//{{AFX_DATA(Light)
	enum { IDD = IDD_LIGHTS };
	CSliderCtrl	m_slide5;
	CSliderCtrl	m_slide4;
	CSliderCtrl	m_slide3;
	CSliderCtrl	m_slide2;
	CSliderCtrl	m_slide;
	CString	m_lightval;
	CString	m_lightDuration;
	CString	m_ringDuration;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Light)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Light)
	virtual BOOL OnInitDialog();
	afx_msg void OnLight();
	afx_msg void OnDark();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnAll();
	afx_msg void OnFront();
	afx_msg void OnLeft();
	afx_msg void OnRight();
	afx_msg void OnReleasedcaptureSlider1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedcaptureSlider2(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedcaptureSlider3(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTop();
	afx_msg void OnReleasedcaptureSlider4(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedcaptureSlider5(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCam5();
	afx_msg void OnLightdurationup();
	afx_msg void OnLightdurationdw();
	afx_msg void OnRingdurationup();
	afx_msg void OnRingdurationdw();
	afx_msg void OnExtralight();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LIGHT_H__B6520F71_0F20_417D_8AFF_FC416739095B__INCLUDED_)
