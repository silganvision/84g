// LabelMatingSetup.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "LabelMatingSetup.h"
#include "mainfrm.h"
#include "xaxisview.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// LabelMatingSetup dialog


LabelMatingSetup::LabelMatingSetup(CWnd* pParent /*=NULL*/)
	: CDialog(LabelMatingSetup::IDD, pParent)
{
	//{{AFX_DATA_INIT(LabelMatingSetup)
	m_taughtR = _T("");
	m_taughtG = _T("");
	m_taughtB = _T("");
	m_RTol = _T("");
	m_GTol = _T("");
	m_BTol = _T("");
	//}}AFX_DATA_INIT
}


void LabelMatingSetup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(LabelMatingSetup)
	DDX_Control(pDX, IDC_STEP_SIZE, m_stepSize);
	DDX_Text(pDX, IDC_TAUGHTR, m_taughtR);
	DDX_Text(pDX, IDC_TAUGHTG, m_taughtG);
	DDX_Text(pDX, IDC_TAUGHTB, m_taughtB);
	DDX_Text(pDX, IDC_TOL_R, m_RTol);
	DDX_Text(pDX, IDC_TOL_G, m_GTol);
	DDX_Text(pDX, IDC_TOL_B, m_BTol);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(LabelMatingSetup, CDialog)
	//{{AFX_MSG_MAP(LabelMatingSetup)
	ON_BN_CLICKED(IDC_STEP_SIZE, OnStepSize)
	ON_BN_CLICKED(IDC_X_LEFT, OnXLeft)
	ON_BN_CLICKED(IDC_X_RIGHT, OnXRight)
	ON_BN_CLICKED(IDC_Y_UP, OnYUp)
	ON_BN_CLICKED(IDC_Y_DOWN, OnYDown)
	ON_BN_CLICKED(IDC_WIDTH_LESS, OnWidthLess)
	ON_BN_CLICKED(IDC_WIDTH_MORE, OnWidthMore)
	ON_BN_CLICKED(IDC_HEIGHT_MORE, OnHeightMore)
	ON_BN_CLICKED(IDC_HEIGHT_LESS, OnHeightLess)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_X_LEFT2, OnXLeft2)
	ON_BN_CLICKED(IDC_X_RIGHT2, OnXRight2)
	ON_BN_CLICKED(IDC_Y_UP2, OnYUp2)
	ON_BN_CLICKED(IDC_Y_DOWN2, OnYDown2)
	ON_BN_CLICKED(IDC_WIDTH_LESS2, OnWidthLess2)
	ON_BN_CLICKED(IDC_WIDTH_MORE2, OnWidthMore2)
	ON_BN_CLICKED(IDC_HEIGHT_MORE2, OnHeightMore2)
	ON_BN_CLICKED(IDC_HEIGHT_LESS2, OnHeightLess2)
	ON_BN_CLICKED(IDC_TEACH_COLOR, OnTeachColor)
	ON_BN_CLICKED(IDC_TOL_R_UP, OnTolRUp)
	ON_BN_CLICKED(IDC_TOL_R_DOWN, OnTolRDown)
	ON_BN_CLICKED(IDC_TOL_G_UP, OnTolGUp)
	ON_BN_CLICKED(IDC_TOL_G_DOWN, OnTolGDown)
	ON_BN_CLICKED(IDC_TOL_B_UP, OnTolBUp)
	ON_BN_CLICKED(IDC_TOL_B_DOWN, OnTolBDown)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// LabelMatingSetup message handlers


BOOL LabelMatingSetup::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pLabelMatingSetup=this;
	theapp=(CBottleApp*)AfxGetApp();

	isSmallStepSize = false;

//	UpdateDisplay();
	OnStepSize();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void LabelMatingSetup::UpdateDisplay()
{
	m_RTol.Format("%4i", theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtRTolerance);
	m_GTol.Format("%4i", theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtGTolerance);
	m_BTol.Format("%4i", theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtBTolerance);

	m_taughtR.Format("%4i", theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtR);
	m_taughtG.Format("%4i", theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtG);
	m_taughtB.Format("%4i", theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtB);

	UpdateData(false);
	pframe->m_pxaxis->InvalidateRect(NULL,true);
}

void LabelMatingSetup::OnStepSize() 
{
	if(isSmallStepSize)
	{
		isSmallStepSize = false;
		pixelStepSize = largePixelStepSize;
		pixelIntensityStepSize = largePixelIntensityStepSize;
		m_stepSize.SetWindowText("Fast");
		CheckDlgButton(IDC_STEP_SIZE,1); 
	}
	else
	{
		isSmallStepSize = true;
		pixelStepSize = smallPixelStepSize;
		pixelIntensityStepSize = smallPixelIntensityStepSize;
		m_stepSize.SetWindowText("Slow");
		CheckDlgButton(IDC_STEP_SIZE,1); 
	}
	
	UpdateData(false);
}


void LabelMatingSetup::OnXLeft() 
{
//	if(theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound-pixelStepSize > camera1FullImageLeftEdge)
//	{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound-=pixelStepSize;
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound-=pixelStepSize;
		//UpdateWaistWidthOverlayValues();
		UpdateAndSaveSetupValues(); 
//	}
}

void LabelMatingSetup::OnXRight() 
{
//	if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperRightBound+pixelStepSize < camera1FullImageRightEdge)
//	{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingLeftBound+=pixelStepSize;
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound+=pixelStepSize;
		//UpdateWaistWidthOverlayValues();
		UpdateAndSaveSetupValues(); 
//	}	
}

void LabelMatingSetup::OnYUp() 
{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound-=pixelStepSize;
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound-=pixelStepSize;
		//UpdateWaistWidthOverlayValues();
		UpdateAndSaveSetupValues(); 
}

void LabelMatingSetup::OnYDown() 
{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingTopBound+=pixelStepSize;
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound+=pixelStepSize;
		//UpdateWaistWidthOverlayValues();
		UpdateAndSaveSetupValues(); 
}

void LabelMatingSetup::OnWidthLess() 
{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound-=pixelStepSize;
		UpdateAndSaveSetupValues(); 
}

void LabelMatingSetup::OnWidthMore() 
{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingRightBound+=pixelStepSize;
		UpdateAndSaveSetupValues(); 	
}

void LabelMatingSetup::OnHeightLess() 
{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound+=pixelStepSize;
		UpdateAndSaveSetupValues();
}

void LabelMatingSetup::OnHeightMore() 
{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingBottomBound-=pixelStepSize;
		UpdateAndSaveSetupValues();
}



//--------------------------------------------------------------------------------------
//---------------------------------------- Teach Color ---------------------------------
void LabelMatingSetup::OnXLeft2() 
{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorLeftBound-=pixelStepSize;
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorRightBound-=pixelStepSize;
		//UpdateWaistWidthOverlayValues();
		UpdateAndSaveSetupValues(); 	
}

void LabelMatingSetup::OnXRight2() 
{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorLeftBound+=pixelStepSize;
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorRightBound+=pixelStepSize;
		//UpdateWaistWidthOverlayValues();
		UpdateAndSaveSetupValues(); 
}

void LabelMatingSetup::OnYUp2() 
{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorTopBound-=pixelStepSize;
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorBottomBound-=pixelStepSize;
		//UpdateWaistWidthOverlayValues();
		UpdateAndSaveSetupValues(); 	
}

void LabelMatingSetup::OnYDown2() 
{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorTopBound+=pixelStepSize;
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorBottomBound+=pixelStepSize;
		//UpdateWaistWidthOverlayValues();
		UpdateAndSaveSetupValues(); 
}

void LabelMatingSetup::OnWidthLess2() 
{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorRightBound-=pixelStepSize;
		UpdateAndSaveSetupValues(); 
}

void LabelMatingSetup::OnWidthMore2() 
{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorRightBound+=pixelStepSize;
		UpdateAndSaveSetupValues(); 
}

void LabelMatingSetup::OnHeightMore2() 
{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorBottomBound-=pixelStepSize;
		UpdateAndSaveSetupValues();
}

void LabelMatingSetup::OnHeightLess2() 
{
		theapp->jobinfo[pframe->CurrentJobNum].labelMatingTeachColorBottomBound+=pixelStepSize;
		UpdateAndSaveSetupValues();	
}



//-----------------------------------------------------------------
//Tolerances

void LabelMatingSetup::OnTolRUp() 
{
	theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtRTolerance += pixelIntensityStepSize;
	UpdateDisplay();
}

void LabelMatingSetup::OnTolRDown() 
{
	theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtRTolerance -= pixelIntensityStepSize;
	UpdateDisplay();
}

void LabelMatingSetup::OnTolGUp() 
{
	theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtGTolerance += pixelIntensityStepSize;
	UpdateDisplay();
}

void LabelMatingSetup::OnTolGDown() 
{
	theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtGTolerance -= pixelIntensityStepSize;
	UpdateDisplay();
}

void LabelMatingSetup::OnTolBUp() 
{
	theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtBTolerance += pixelIntensityStepSize;
	UpdateDisplay();
}

void LabelMatingSetup::OnTolBDown() 
{
	theapp->jobinfo[pframe->CurrentJobNum].labelMatingTaughtBTolerance -= pixelIntensityStepSize;
	UpdateDisplay();
}



void LabelMatingSetup::OnTeachColor() 
{
	pframe->m_pxaxis->TeachLabelMatingColorCam1();
	theapp->jobinfo[pframe->CurrentJobNum].taught=true;
	UpdateDisplay();
}


void LabelMatingSetup::UpdateAndSaveSetupValues() 
{
	SetTimer(1,3000,NULL);

	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pxaxis->InvalidateRect(NULL, false); 	
}

void LabelMatingSetup::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1) 
	{
		KillTimer(1);
		pframe->SaveJob(pframe->CurrentJobNum);
	}

	CDialog::OnTimer(nIDEvent);
}


