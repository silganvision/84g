#if !defined(AFX_STITCHSETUP_H__988F2583_EC5E_4241_A84E_E0C71441D81C__INCLUDED_)
#define AFX_STITCHSETUP_H__988F2583_EC5E_4241_A84E_E0C71441D81C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StitchSetup.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// StitchSetup dialog

class StitchSetup : public CDialog
{
	friend class CMainFrame;
	friend class CBottleApp;
// Construction
public:
	void UpdateDisplay();

	CBottleApp *theapp;
	CMainFrame* pframe;

	int lengthCam;
	int heightCam;


	StitchSetup(CWnd* pParent = NULL);   // standard constructor
	void UpdateValues();

	bool	stepSlow;
	int		resBox;
	int		resBoxWidth;
	int		resSen;

// Dialog Data
	//{{AFX_DATA(StitchSetup)
	enum { IDD = IDD_STITCHIMAGE };
	CButton	m_step;
	CString	m_ypos1;
	CString	m_ypos2;
	CString	m_ypos3;
	CString	m_ypos4;
	CString	m_w1;
	CString	m_h1;
	CString	m_picNum;
	CString	m_senEdges;
	CString	m_seamH;
	CString	m_seamW;
	CString	m_seamOffX;
	CString	m_seamOffY;
	CString	m_avoidMid;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(StitchSetup)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(StitchSetup)
	afx_msg void OnYup1();
	afx_msg void OnYdw1();
	afx_msg void OnYup2();
	afx_msg void OnYup3();
	afx_msg void OnYup4();
	afx_msg void OnYdw2();
	afx_msg void OnYdw3();
	afx_msg void OnYdw4();
	afx_msg void OnWup1();
	afx_msg void OnWdw1();
	afx_msg void OnHup1();
	afx_msg void OnHdw1();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual BOOL OnInitDialog();
	afx_msg void OnStep();
	afx_msg void OnShowstitbw();
	afx_msg void OnPictoshowup2();
	afx_msg void OnPictoshowdn();
	afx_msg void OnEnapicstoshow();
	afx_msg void OnShowstitedges();
	afx_msg void OnSenedgesup();
	afx_msg void OnSenedgesdw();
	afx_msg void OnWidthseamup();
	afx_msg void OnWidthseamdw();
	afx_msg void OnHeightseamup();
	afx_msg void OnHeightseamdw();
	afx_msg void OnOffxup();
	afx_msg void OnOffxdw();
	afx_msg void OnOffyup();
	afx_msg void OnOffydw();
	afx_msg void OnFilterlight();
	afx_msg void OnFilterdark();
	afx_msg void OnFilteredge();
	afx_msg void OnShowstitlight();
	afx_msg void OnShowstitdark();
	afx_msg void OnShowstitfinal();
	afx_msg void OnUsefilterlight();
	afx_msg void OnUsefilterdark();
	afx_msg void OnUsefilteredge();
	afx_msg void OnAvoidmiddup();
	afx_msg void OnAvoidmidddw();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STITCHSETUP_H__988F2583_EC5E_4241_A84E_E0C71441D81C__INCLUDED_)
