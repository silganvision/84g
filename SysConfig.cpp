// SysConfig.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "mainfrm.h"
#include "SysConfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSysConfig dialog


CSysConfig::CSysConfig(CWnd* pParent)
	: CDialog(CSysConfig::IDD, pParent)
{
}

void CSysConfig::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWhiteBalance)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSysConfig, CDialog)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDOK, OnUpdateSelections)
	ON_BN_CLICKED(IDC_TRACK, OnTrackerSelect)
	ON_BN_CLICKED(IDC_WELCHS, OnWelchsSelect)
	ON_BN_CLICKED(IDC_WAISTINSP, OnWaistSelect)
	ON_BN_CLICKED(IDC_ALIGNINSP, OnAlignmentSelect)
	ON_BN_CLICKED(IDC_84MACHINE, M84RadioSelect)
	ON_BN_CLICKED(IDC_85MACHINE, M85RadioSelect)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSysConfig message handlers

BOOL CSysConfig::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();

	UpdateData(false);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//Clears all camera related recourses. Called when program closes.
BOOL CSysConfig::DestroyWindow() 
{	
	return CDialog::DestroyWindow();
}

void CSysConfig::SetParams(MachineType machNo, bool trackEnab, bool waistEnab, bool alignEnab, bool welchsEnab)
{
	CheckDlgButton(IDC_TRACK,(trackerEnabled = trackEnab) ? 1 : 0);
	CheckDlgButton(IDC_WAISTINSP,(waistEnabled = waistEnab) ? 1 : 0);
	CheckDlgButton(IDC_ALIGNINSP,(alignmentEnabled = alignEnab) ? 1 : 0);
	CheckDlgButton(IDC_WELCHS,(welchsEnabled = welchsEnab) ? 1 : 0);

	CheckDlgButton(IDC_84MACHINE,(machineNo = machNo) == 0 ? 1 : 0);
	CheckDlgButton(IDC_85MACHINE, machineNo == 1 ? 1 : 0 );
}

void CSysConfig::OnUpdateSelections() 
{
	bool changesPresent = false;
	pframe->SSecretMenu = false;

	changesPresent ^= pframe->SetWelchsEnable(welchsEnabled);
	changesPresent ^= pframe->SetTrackerEnable(trackerEnabled);
	changesPresent ^= pframe->SetWaistEnable(waistEnabled);
	changesPresent ^= pframe->SetAlignmentEnable(alignmentEnabled);
	changesPresent ^= pframe->SetMachineType((MachineType)machineNo);
	
	if(changesPresent)
		AfxMessageBox("Changes will be made with next start");

	ShowWindow(SW_HIDE);
}

//	Select 84 Machine
void CSysConfig::M84RadioSelect() 
{
	machineNo = Machine84;
	CheckDlgButton(IDC_84MACHINE,1);
	CheckDlgButton(IDC_85MACHINE,0);
	UpdateData(false);
}

//	Select 85 Machine
void CSysConfig::M85RadioSelect() 
{
	machineNo = Machine85;
	CheckDlgButton(IDC_84MACHINE,0);
	CheckDlgButton(IDC_85MACHINE,1);
	UpdateData(false);
}

//	Select Welch's Machine
void CSysConfig::OnWelchsSelect() 
{
	welchsEnabled = !welchsEnabled;
	CheckDlgButton(IDC_WELCHS, welchsEnabled ? 1 : 0);
	UpdateData(false);
}

void CSysConfig::OnTrackerSelect()
{
	trackerEnabled = !trackerEnabled;
	CheckDlgButton(IDC_TRACK, trackerEnabled ? 1 : 0);
	UpdateData(false);
}

void CSysConfig::OnAlignmentSelect()
{
	alignmentEnabled = !alignmentEnabled;
	CheckDlgButton(IDC_ALIGNINSP, alignmentEnabled ? 1 : 0);
	UpdateData(false);
}

void CSysConfig::OnWaistSelect()
{
	waistEnabled = !waistEnabled;
	CheckDlgButton(IDC_WAISTINSP, waistEnabled ? 1 : 0);
	UpdateData(false);
}
