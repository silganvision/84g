// DigIO.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "DigIO.h"
#include "mainfrm.h"
#include "serial.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DigIO dialog


DigIO::DigIO(CWnd* pParent /*=NULL*/)
	: CDialog(DigIO::IDD, pParent)
{
	//{{AFX_DATA_INIT(DigIO)
	m_editiorejects = _T("");
	m_editconrejects = _T("");
	m_editconrejects2 = _T("");
	//}}AFX_DATA_INIT
}


void DigIO::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DigIO)
	DDX_Text(pDX, IDC_EDITIOREJECTS, m_editiorejects);
	DDX_Text(pDX, IDC_EDITIOREJECTS2, m_editconrejects);
	DDX_Text(pDX, IDC_EDITIOREJECTS3, m_editconrejects2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DigIO, CDialog)
	//{{AFX_MSG_MAP(DigIO)
	ON_BN_CLICKED(IDC_BMORE, OnBmore)
	ON_BN_CLICKED(IDC_BLESS, OnBless)
	ON_BN_CLICKED(IDC_BMORE2, OnBmore2)
	ON_BN_CLICKED(IDC_BLESS2, OnBless2)
	ON_BN_CLICKED(IDC_HEARTBEAT, OnHeartbeat)
	ON_BN_CLICKED(IDC_TESTCONEJ, OnTestconej)
	ON_BN_CLICKED(IDC_TESTEJALL, OnTestejall)
	ON_BN_CLICKED(IDC_BMORE3, OnBmore3)
	ON_BN_CLICKED(IDC_BLESS3, OnBless3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DigIO message handlers

BOOL DigIO::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pdigio=this;
	theapp=(CBottleApp*)AfxGetApp();
	tog=false;
	
	m_editiorejects.Format("%3i",theapp->SavedIORejects);
	m_editconrejects.Format("%3i",theapp->SavedConRejects);

	m_editconrejects2.Format("%3i",theapp->SavedConRejects2);
	
	CheckDlgButton(IDC_HEARTBEAT, theapp->enableHeartBeat ? 1 : 0);

	UpdateData(false);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DigIO::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

void DigIO::OnBmore() 
{
	pframe->IORejectLimit=theapp->SavedIORejects+=1;
	
	if(theapp->SavedIORejects>=200)	{theapp->SavedIORejects=200;}
	if(theapp->SavedIORejects<=0)	{theapp->SavedIORejects=0;}

	m_editiorejects.Format("%3i",theapp->SavedIORejects);

	UpdateData(false);
}

void DigIO::OnBless() 
{
	pframe->IORejectLimit=theapp->SavedIORejects-=1;
	
	if(theapp->SavedIORejects>=200)	{theapp->SavedIORejects=200;}
	if(theapp->SavedIORejects<=0)	{theapp->SavedIORejects=0;}
	
	m_editiorejects.Format("%3i",theapp->SavedIORejects);
	UpdateData(false);
}

void DigIO::OnBmore2() 
{
	pframe->ConRejectLimit=theapp->SavedConRejects+=1;
	
	if(theapp->SavedConRejects>=200)	{theapp->SavedConRejects=200;}
	if(theapp->SavedConRejects<=0)		{theapp->SavedConRejects=0;}

	m_editconrejects.Format("%3i",theapp->SavedConRejects);

	UpdateData(false);
}

void DigIO::OnBless2() 
{
	pframe->ConRejectLimit=theapp->SavedConRejects-=1;

	if(theapp->SavedConRejects>=200)	{theapp->SavedConRejects=200;}
	if(theapp->SavedConRejects<=0)		{theapp->SavedConRejects=0;}
	
	m_editconrejects.Format("%3i",theapp->SavedConRejects);

	UpdateData(false);
}

void DigIO::OnHeartbeat() 
{
	if( theapp->enableHeartBeat==true )
	{
		CheckDlgButton(IDC_HEARTBEAT,0); 
		theapp->enableHeartBeat=false;
		//pframe->m_pserial->SendChar(550); //con reject and reject 
	}
	else
	{ 
		CheckDlgButton(IDC_HEARTBEAT,1);
		theapp->enableHeartBeat=true; 
		//pframe->m_pserial->SendChar(540); //con reject and reject 
	}

	UpdateData(false);
}

void DigIO::OnTestconej() 
{
	if(theapp->serPresent)	{pframe->m_pserial->SendChar(0,620);}
}

void DigIO::OnTestejall() 
{
	if(tog)
	{
		theapp->rejectAllKrones=false;
		pframe->m_pxaxis->InvalidateRect(NULL,false);
		tog=false;
	}
	else
	{
		theapp->rejectAllKrones=true;
		pframe->m_pxaxis->InvalidateRect(NULL,false);
		tog=true;
	}
}

void DigIO::OnBmore3() 
{
	pframe->ConRejectLimit2=theapp->SavedConRejects2+=1;
	
	if(theapp->SavedConRejects2>=200)	{theapp->SavedConRejects2=200;}
	if(theapp->SavedConRejects2<=0)		{theapp->SavedConRejects2=0;}
	
	m_editconrejects2.Format("%3i",theapp->SavedConRejects2);
	
	UpdateData(false);
}

void DigIO::OnBless3() 
{
	pframe->ConRejectLimit2=theapp->SavedConRejects2-=1;
	
	if(theapp->SavedConRejects2>=200)	{theapp->SavedConRejects2=200;}
	if(theapp->SavedConRejects2<=0)		{theapp->SavedConRejects2=0;}
	
	m_editconrejects2.Format("%3i",theapp->SavedConRejects2);

	UpdateData(false);
}
