#ifndef GLOBALS_H
#define GLOBALS_H

#include <FlyCapture2.h>
#include "GigECamera.h"

#define _MAX_CAMERAS 5
extern unsigned char**   g_arpBuffers[ _MAX_CAMERAS ];
const int EXPECTED_CAMS = 5;

#endif