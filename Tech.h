#if !defined(AFX_TECH_H__E7CD203B_496C_4D45_80F3_91D68E1C964A__INCLUDED_)
#define AFX_TECH_H__E7CD203B_496C_4D45_80F3_91D68E1C964A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Tech.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Tech dialog

class Tech : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
	// Construction
public:
	Tech(CWnd* pParent = NULL);   // standard constructor

	CMainFrame* pframe;
	CBottleApp* theapp;
	CString m_enable;
	

	long Value;
	int Function;
	int CntrBits;
// Dialog Data
	//{{AFX_DATA(Tech)
	enum { IDD = IDD_HIDDEN };
	
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Tech)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Tech)
	virtual BOOL OnInitDialog();

	afx_msg void OnEncsim();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnPlcpos();
	
	virtual void OnOK();
	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TECH_H__E7CD203B_496C_4D45_80F3_91D68E1C964A__INCLUDED_)
