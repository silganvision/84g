// YAxisView.cpp : implementation file
//

#include "stdafx.h"
#include "Bottle.h"
#include "YAxisView.h"
#include "XAxisView.h"
#include "View.h"
#include "Modify.h"
#include "Lim.h"
#include "cap.h"
#include "splice.h"
#include "StitchSetup.h"
#include "CapSetup1.h"
#include "LabelMatingSetup.h"
#include "WaistInspection.h"
#include "WaistInspection2.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//HIFCGRAB GrabID1;
/////////////////////////////////////////////////////////////////////////////
// CYAxisView

IMPLEMENT_DYNCREATE(CYAxisView, CView)

CYAxisView::CYAxisView()
{
}

CYAxisView::~CYAxisView()
{
}


BEGIN_MESSAGE_MAP(CYAxisView, CView)
	//{{AFX_MSG_MAP(CYAxisView)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CYAxisView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pyaxis=this;
	theapp=(CBottleApp*)AfxGetApp();
	
	toggle		=	true;

	RECT rect;
	rect.left	=	0;
	rect.right	=	630;
	rect.bottom	=	140;
	rect.top	=	0;

	m_tabmenu.Create(TCS_TABS |  TCS_RIGHTJUSTIFY  | WS_CHILD | WS_VISIBLE, //| TCS_FIXEDWIDTH 
	rect, this, 0x1006);
	m_tabmenu.SetMinTabWidth(60);
	//m_tabmenu.SetPadding(10);

	//	Because of dynamic tabs, we have to manage the indexes	-	J.Martin
	int tabIndex = 0;
	STATSTAB = MODTAB = LIMTAB = CAPTAB = SPLICETAB = TEARTAB = ALIGNTAB = WAISTTAB = WAIST2TAB = -1;

	TC_ITEM TabCtrlItem;
	TabCtrlItem.mask	= TCIF_TEXT;

	TabCtrlItem.pszText	= "Statistics";
	m_tabmenu.InsertItem(STATSTAB = tabIndex++, &TabCtrlItem);

	TabCtrlItem.pszText = "Modify Label";
	m_tabmenu.InsertItem(MODTAB = tabIndex++, &TabCtrlItem);
	
	TabCtrlItem.pszText = "Limits";
	m_tabmenu.InsertItem(LIMTAB = tabIndex++, &TabCtrlItem);
	
	if(theapp->cam5Enable)
	{
		TabCtrlItem.pszText = "Cap Insp.";
		m_tabmenu.InsertItem(CAPTAB = tabIndex++, &TabCtrlItem);
	}

	TabCtrlItem.pszText = "Splice";
	m_tabmenu.InsertItem(SPLICETAB = tabIndex++, &TabCtrlItem);

	TabCtrlItem.pszText = "Tear";
	m_tabmenu.InsertItem(TEARTAB = tabIndex++, &TabCtrlItem);

	if(theapp->alignAvailable)
	{
		TabCtrlItem.pszText = "Alignment";
		m_tabmenu.InsertItem(ALIGNTAB = tabIndex++, &TabCtrlItem);
	}

	//Waist measurement was for Vitaliy's Gatorade project.Currently Diasbled
	if(theapp->waistAvailable)
	{
		TabCtrlItem.pszText = "Waist";
		m_tabmenu.InsertItem(WAISTTAB = tabIndex++, &TabCtrlItem);

		TabCtrlItem.pszText = "Waist2";
		m_tabmenu.InsertItem(WAIST2TAB = tabIndex++, &TabCtrlItem);
	}

	CWnd* pParent=GetParent(); 

	m_pstats	=	new View(); 
	m_pstats->Create(IDD_VIEW,this);

	m_pstats->ShowWindow(SW_SHOW);
	pframe->m_pview2->UpdateDisplay();

	m_pmod		=	new Modify(); 
	m_pmod->Create(IDD_MODIFY,this);

	m_plimit	=	new Lim(); 
	m_plimit->Create(IDD_LIM,this);

	m_pcap		=	new CCap(); 
	m_pcap->Create(IDD_CAP,this);

	m_pcap		=	new CCap(); 
	m_pcap->Create(IDD_CAP,this);

	m_psplice	=	new Splice(); 
	m_psplice->Create(IDD_SPLICE,this);

	m_pstitchSetup =new StitchSetup(); 
	m_pstitchSetup->Create(IDD_STITCHIMAGE,this);

	m_pLabelMatingSetup= new LabelMatingSetup(); 
	m_pLabelMatingSetup->Create(IDD_LABEL_MATING_SETUP,this);

	m_pWaistInspection = new WaistInspection(); 
	m_pWaistInspection->Create(IDD_WAIST_INSPECTION,this);

	m_pWaistInspection2 = new WaistInspection2(); 
	m_pWaistInspection2->Create(IDD_WAIST_INSPECTION2,this);

	//////////////Just weird
	m_pcapSetup1 = new CCapSetup1(); 
	m_pcapSetup1->Create(IDD_CAPSETUP1,this);


}

/////////////////////////////////////////////////////////////////////////////
// CYAxisView drawing

void CYAxisView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	CClientDC ColorRect(this);
	CString m_1,m_2,m_3,m_4;
	CBrush Colorw, Colorr;

	COLORREF sqrred,sqrwhite;//,Gray, Red;
	CPen GrayPen(PS_SOLID, 1, RGB(200,200,200));
	CPen BluePen(PS_SOLID, 1, RGB(0,0,255));

	CPen LGreenPen(PS_SOLID, 1, RGB(150,255,150));
	CRect a;
	
	COLORREF bluecolor = RGB(0,0,255);
	
	sqrred=RGB(200,0,0);
	sqrwhite=RGB(255,255,255);
	a.SetRect(60,0,600,90);
	Colorr.CreateSolidBrush(sqrred);
	Colorw.CreateSolidBrush(sqrwhite);
	
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(RGB(0,0,0));
}

/////////////////////////////////////////////////////////////////////////////
// CYAxisView diagnostics

#ifdef _DEBUG
void CYAxisView::AssertValid() const
{
	CView::AssertValid();
}

void CYAxisView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CYAxisView message handlers





void CYAxisView::OnDestroy() 
{
	CView::OnDestroy();
	
	m_pstats->DestroyWindow(); 
	m_pmod->DestroyWindow(); 
	m_plimit->DestroyWindow(); 
	m_pcap->DestroyWindow(); 
	m_psplice->DestroyWindow(); 
	m_pstitchSetup->DestroyWindow(); 
	m_pWaistInspection->DestroyWindow();
	m_pLabelMatingSetup->DestroyWindow();

	delete m_pstats;
	delete m_pmod;
	delete m_plimit;
	delete m_pcap;
	delete m_psplice;
	delete m_pstitchSetup;
	delete m_pWaistInspection;
	delete m_pLabelMatingSetup;
}

BOOL CYAxisView::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo) 
{
	int index	=	m_tabmenu.GetCurSel();
	
	/*	Removed switch to allow dynamic switching of tab indices
	switch(index)
	{
		case 0:

			m_pmod->ShowWindow(SW_HIDE);
			m_plimit->ShowWindow(SW_HIDE);
			m_pcap->ShowWindow(SW_HIDE);
			m_pstats->ShowWindow(SW_SHOW);
			m_psplice->ShowWindow(SW_HIDE);
			m_pstitchSetup->ShowWindow(SW_HIDE);
			m_pLabelMatingSetup->ShowWindow(SW_HIDE);
			m_pWaistInspection->ShowWindow(SW_HIDE);
			m_pWaistInspection2->ShowWindow(SW_HIDE);

			theapp->statsOpen			=	true;
			theapp->labelOpen			=	false;
			pframe->m_pxaxis->Modify	=	false;
			//theapp->showCam5			=	false;
			//theapp->capOpen				=	false;
			theapp->tearOpen			=	false;
			theapp->showLargeImages = false;
			theapp->showLargeImages2 = false;
			theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;


			pframe->m_pview2->UpdateDisplay();
			pframe->m_pxaxis->InvalidateRect(NULL, false);

			UpdateData(false);
			break;

		case 1:
		
			if(pframe->SModify)
			{
				m_pstats->ShowWindow(SW_HIDE);
				m_plimit->ShowWindow(SW_HIDE);
				m_pcap->ShowWindow(SW_HIDE);
				m_pmod->ShowWindow(SW_SHOW);
				m_psplice->ShowWindow(SW_HIDE);
				m_pstitchSetup->ShowWindow(SW_HIDE);
				m_pLabelMatingSetup->ShowWindow(SW_HIDE);
				m_pWaistInspection->ShowWindow(SW_HIDE);
				m_pWaistInspection2->ShowWindow(SW_HIDE);

				pframe->m_pxaxis->Modify=	true;
				theapp->statsOpen		=	false;
				theapp->showCam5		=	false;
				theapp->capOpen			=	false;
				theapp->labelOpen		=	false;
				theapp->tearOpen		=	false;
				theapp->showLargeImages = false;
				theapp->showLargeImages2 = false;
				theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;

				pframe->m_pmod->UpdateDisplay();
				pframe->m_pxaxis->InvalidateRect(NULL,false);
			}
			else
			{
				m_tabmenu.SetCurSel(0);
				AfxMessageBox("Password Protected");
			}

			UpdateData(false);
			break;

		case 2:
		
			if(pframe->SLimits)
			{
				m_pmod->ShowWindow(SW_HIDE);
				m_pstats->ShowWindow(SW_HIDE);
				m_pcap->ShowWindow(SW_HIDE);
				m_plimit->ShowWindow(SW_SHOW);
				m_psplice->ShowWindow(SW_HIDE);
				m_pstitchSetup->ShowWindow(SW_HIDE);
				m_pLabelMatingSetup->ShowWindow(SW_HIDE);
				m_pWaistInspection->ShowWindow(SW_HIDE);
				m_pWaistInspection2->ShowWindow(SW_HIDE);

				theapp->statsOpen		=	false;
				pframe->m_pxaxis->Modify=	false;
				theapp->showCam5		=	false;
				theapp->capOpen			=	false;
				theapp->labelOpen		=	false;
				theapp->tearOpen		=	false;
				theapp->showLargeImages = false;
				theapp->showLargeImages2 = false;
				theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;


				pframe->m_plimit->UpdateDisplay();
				pframe->m_pxaxis->InvalidateRect(NULL,false);
			}
			else
			{
				m_tabmenu.SetCurSel(0);
				AfxMessageBox("Password Protected");
			}

			UpdateData(false);
			break;	
		
		case 3:

			if(pframe->SModify)
			{
				theapp=(CBottleApp*)AfxGetApp();
				m_pmod->ShowWindow(SW_HIDE);
				m_pstats->ShowWindow(SW_HIDE);
				m_pcap->ShowWindow(SW_SHOW);
				m_plimit->ShowWindow(SW_HIDE);
				m_psplice->ShowWindow(SW_HIDE);
				m_pstitchSetup->ShowWindow(SW_HIDE);
				m_pLabelMatingSetup->ShowWindow(SW_HIDE);
				m_pWaistInspection->ShowWindow(SW_HIDE);
				m_pWaistInspection2->ShowWindow(SW_HIDE);

				theapp->showCam5		=	true;
				theapp->statsOpen		=	false;
				pframe->m_pxaxis->Modify=	false;
				theapp->capOpen			=	true;
				theapp->labelOpen		=	false;
				theapp->tearOpen		=	false;
				theapp->showLargeImages = false;
				theapp->showLargeImages2 = false;
				theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;

				pframe->m_pcap->UpdateDisplay();
			}
			else
			{
				m_tabmenu.SetCurSel(0);
				AfxMessageBox("Password Protected");
			}

			UpdateData(false);
			break;

		case 4:
		
			if(pframe->SModify)
			{
				//pframe->m_pxaxis->Modify=true;
				m_pstats->ShowWindow(SW_HIDE);
				m_plimit->ShowWindow(SW_HIDE);
				m_pcap->ShowWindow(SW_HIDE);
				m_pmod->ShowWindow(SW_HIDE);
				m_psplice->ShowWindow(SW_SHOW);
				m_pstitchSetup->ShowWindow(SW_HIDE);
				m_pLabelMatingSetup->ShowWindow(SW_HIDE);
				m_pWaistInspection->ShowWindow(SW_HIDE);
				m_pWaistInspection2->ShowWindow(SW_HIDE);

				theapp->statsOpen		=	false;
				theapp->showCam5		=	false;
				theapp->capOpen			=	false;
				pframe->m_pxaxis->Modify=	false;
				theapp->labelOpen		=	true;
				theapp->tearOpen		=	false;
				theapp->showLargeImages = false;
				theapp->showLargeImages2 = false;
				theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;

				pframe->m_psplice->UpdateDisplay();
				pframe->m_pxaxis->InvalidateRect(NULL,false);
			}
			else
			{
				m_tabmenu.SetCurSel(0);
				AfxMessageBox("Password Protected");
			}

			UpdateData(false);
			break;

		case 5:
		
			if(pframe->SModify)
			{
				//pframe->m_pxaxis->Modify=true;
				m_pstats->ShowWindow(SW_HIDE);
				m_plimit->ShowWindow(SW_HIDE);
				m_pcap->ShowWindow(SW_HIDE);
				m_pmod->ShowWindow(SW_HIDE);
				m_psplice->ShowWindow(SW_HIDE);
				m_pstitchSetup->ShowWindow(SW_SHOW);
				m_pLabelMatingSetup->ShowWindow(SW_HIDE);
				m_pWaistInspection->ShowWindow(SW_HIDE);
				m_pWaistInspection2->ShowWindow(SW_HIDE);

				theapp->statsOpen		=	false;
				theapp->showCam5		=	false;
				theapp->capOpen			=	false;
				theapp->labelOpen		=	false;
				pframe->m_pxaxis->Modify=	false;
				theapp->tearOpen		=	true;
				theapp->showLargeImages = false;
				theapp->showLargeImages2 = false;
				theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;

				pframe->m_pstitchSetup->UpdateDisplay();
				pframe->m_pxaxis->InvalidateRect(NULL,false);
			}
			else
			{
				m_tabmenu.SetCurSel(0);
				AfxMessageBox("Password Protected");
			}

			UpdateData(false);
			break;
		case 6:
		
			if(pframe->SModify)
			{
				//pframe->m_pxaxis->Modify=true;
				m_pstats->ShowWindow(SW_HIDE);
				m_plimit->ShowWindow(SW_HIDE);
				m_pcap->ShowWindow(SW_HIDE);
				m_pmod->ShowWindow(SW_HIDE);
				m_psplice->ShowWindow(SW_HIDE);
				m_pstitchSetup->ShowWindow(SW_HIDE);
				m_pLabelMatingSetup->ShowWindow(SW_SHOW);
				m_pWaistInspection->ShowWindow(SW_HIDE);
				m_pWaistInspection2->ShowWindow(SW_HIDE);
			
				theapp->statsOpen		=	false;
				theapp->showCam5		=	false;
				theapp->capOpen			=	false;
				theapp->labelOpen		=	false;
				pframe->m_pxaxis->Modify=	false;
				theapp->tearOpen		=	false;
				theapp->showLargeImages =	false;
				theapp->showLargeImages2 = false;
				theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=true;

				pframe->m_pLabelMatingSetup->UpdateDisplay();
				pframe->m_pxaxis->InvalidateRect(NULL,false);			
			}
			else
			{
				m_tabmenu.SetCurSel(0);
				AfxMessageBox("Password Protected");
			}

			break;
		case 7:
		
			if(pframe->SModify)
			{
				//pframe->m_pxaxis->Modify=true;
				m_pstats->ShowWindow(SW_HIDE);
				m_plimit->ShowWindow(SW_HIDE);
				m_pcap->ShowWindow(SW_HIDE);
				m_pmod->ShowWindow(SW_HIDE);
				m_psplice->ShowWindow(SW_HIDE);
				m_pstitchSetup->ShowWindow(SW_HIDE);
				m_pLabelMatingSetup->ShowWindow(SW_HIDE);
				m_pWaistInspection->ShowWindow(SW_SHOW);
				m_pWaistInspection2->ShowWindow(SW_HIDE);

				theapp->statsOpen		=	false;
				theapp->showCam5		=	false;
				theapp->capOpen			=	false;
				theapp->labelOpen		=	false;
				pframe->m_pxaxis->Modify=	false;
				theapp->tearOpen		=	false;
				theapp->showLargeImages =	true;
				theapp->showLargeImages2 = false;
				theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;

				pframe->m_pWaistInspection->UpdateDisplay();
				pframe->m_pxaxis->InvalidateRect(NULL,false);			
			}
			else
			{
				m_tabmenu.SetCurSel(0);
				AfxMessageBox("Password Protected");
			}

			break;
		case 8:
		
			if(pframe->SModify)
			{
				//pframe->m_pxaxis->Modify=true;
				m_pstats->ShowWindow(SW_HIDE);
				m_plimit->ShowWindow(SW_HIDE);
				m_pcap->ShowWindow(SW_HIDE);
				m_pmod->ShowWindow(SW_HIDE);
				m_psplice->ShowWindow(SW_HIDE);
				m_pstitchSetup->ShowWindow(SW_HIDE);
				m_pLabelMatingSetup->ShowWindow(SW_HIDE);
				m_pWaistInspection->ShowWindow(SW_HIDE);
				m_pWaistInspection2->ShowWindow(SW_SHOW);

				theapp->statsOpen		=	false;
				theapp->showCam5		=	false;
				theapp->capOpen			=	false;
				theapp->labelOpen		=	false;
				pframe->m_pxaxis->Modify=	false;
				theapp->tearOpen		=	false;
				theapp->showLargeImages = false;
				theapp->showLargeImages2 = true;
				theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;

				pframe->m_pWaistInspection2->UpdateDisplay();
				pframe->m_pxaxis->InvalidateRect(NULL,false);
			}
			else
			{
				m_tabmenu.SetCurSel(0);
				AfxMessageBox("Password Protected");
			}

			break;
	}//switch
	*/
	
	if(index == STATSTAB)
	{
		m_pmod->ShowWindow(SW_HIDE);
		m_plimit->ShowWindow(SW_HIDE);
		m_pcap->ShowWindow(SW_HIDE);
		m_pstats->ShowWindow(SW_SHOW);
		m_psplice->ShowWindow(SW_HIDE);
		m_pstitchSetup->ShowWindow(SW_HIDE);
		m_pLabelMatingSetup->ShowWindow(SW_HIDE);
		m_pWaistInspection->ShowWindow(SW_HIDE);
		m_pWaistInspection2->ShowWindow(SW_HIDE);

		theapp->statsOpen			=	true;
		theapp->labelOpen			=	false;
		pframe->m_pxaxis->Modify	=	false;
		//theapp->showCam5			=	false;
		//theapp->capOpen				=	false;
		theapp->tearOpen			=	false;
		theapp->showLargeImages = false;
		theapp->showLargeImages2 = false;
		theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;

		pframe->m_pview2->UpdateDisplay();
		pframe->m_pxaxis->InvalidateRect(NULL, false);

		UpdateData(false);
	}
	else if(index == MODTAB)
	{
		if(pframe->SModify)
		{
			m_pstats->ShowWindow(SW_HIDE);
			m_plimit->ShowWindow(SW_HIDE);
			m_pcap->ShowWindow(SW_HIDE);
			m_pmod->ShowWindow(SW_SHOW);
			m_psplice->ShowWindow(SW_HIDE);
			m_pstitchSetup->ShowWindow(SW_HIDE);
			m_pLabelMatingSetup->ShowWindow(SW_HIDE);
			m_pWaistInspection->ShowWindow(SW_HIDE);
			m_pWaistInspection2->ShowWindow(SW_HIDE);

			pframe->m_pxaxis->Modify=	true;
			theapp->statsOpen		=	false;
			theapp->showCam5		=	false;
			theapp->capOpen			=	false;
			theapp->labelOpen		=	false;
			theapp->tearOpen		=	false;
			theapp->showLargeImages = false;
			theapp->showLargeImages2 = false;
			theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;

			pframe->m_pmod->UpdateDisplay();
			pframe->m_pxaxis->InvalidateRect(NULL,true);
		 
	 
		}
		else
		{
			m_tabmenu.SetCurSel(0);
			AfxMessageBox("Password Protected");
		}

		UpdateData(false);
	}
	else if(index == LIMTAB)
	{
		if(pframe->SLimits)
		{
			m_pmod->ShowWindow(SW_HIDE);
			m_pstats->ShowWindow(SW_HIDE);
			m_pcap->ShowWindow(SW_HIDE);
			m_plimit->ShowWindow(SW_SHOW);
			m_psplice->ShowWindow(SW_HIDE);
			m_pstitchSetup->ShowWindow(SW_HIDE);
			m_pLabelMatingSetup->ShowWindow(SW_HIDE);
			m_pWaistInspection->ShowWindow(SW_HIDE);
			m_pWaistInspection2->ShowWindow(SW_HIDE);

			theapp->statsOpen		=	false;
			pframe->m_pxaxis->Modify=	false;
			theapp->showCam5		=	false;
			theapp->capOpen			=	false;
			theapp->labelOpen		=	false;
			theapp->tearOpen		=	false;
			theapp->showLargeImages = false;
			theapp->showLargeImages2 = false;
			theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;

			pframe->m_plimit->UpdateDisplay();
			pframe->m_pxaxis->InvalidateRect(NULL,false);
		}
		else
		{
			m_tabmenu.SetCurSel(0);
			AfxMessageBox("Password Protected");
		}

		UpdateData(false);
	}
	else if(index == CAPTAB)
	{
		if(pframe->SModify)
		{
			theapp=(CBottleApp*)AfxGetApp();
			m_pmod->ShowWindow(SW_HIDE);
			m_pstats->ShowWindow(SW_HIDE);
			m_pcap->ShowWindow(SW_SHOW);
			m_plimit->ShowWindow(SW_HIDE);
			m_psplice->ShowWindow(SW_HIDE);
			m_pstitchSetup->ShowWindow(SW_HIDE);
			m_pLabelMatingSetup->ShowWindow(SW_HIDE);
			m_pWaistInspection->ShowWindow(SW_HIDE);
			m_pWaistInspection2->ShowWindow(SW_HIDE);

			theapp->showCam5		=	true;
			theapp->statsOpen		=	false;
			pframe->m_pxaxis->Modify=	false;
			theapp->capOpen			=	true;
			theapp->labelOpen		=	false;
			theapp->tearOpen		=	false;
			theapp->showLargeImages = false;
			theapp->showLargeImages2 = false;
			theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;

			pframe->m_pcap->UpdateDisplay();
		}
		else
		{
			m_tabmenu.SetCurSel(0);
			AfxMessageBox("Password Protected");
		}

		UpdateData(false);
	}
	else if(index == SPLICETAB)
	{
		if(pframe->SModify)
		{
			//pframe->m_pxaxis->Modify=true;
			m_pstats->ShowWindow(SW_HIDE);
			m_plimit->ShowWindow(SW_HIDE);
			m_pcap->ShowWindow(SW_HIDE);
			m_pmod->ShowWindow(SW_HIDE);
			m_psplice->ShowWindow(SW_SHOW);
			m_pstitchSetup->ShowWindow(SW_HIDE);
			m_pLabelMatingSetup->ShowWindow(SW_HIDE);
			m_pWaistInspection->ShowWindow(SW_HIDE);
			m_pWaistInspection2->ShowWindow(SW_HIDE);

			theapp->statsOpen		=	false;
			theapp->showCam5		=	false;
			theapp->capOpen			=	false;
			pframe->m_pxaxis->Modify=	false;
			theapp->labelOpen		=	true;
			theapp->tearOpen		=	false;
			theapp->showLargeImages = false;
			theapp->showLargeImages2 = false;
			theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;

			pframe->m_psplice->UpdateDisplay();
			pframe->m_pxaxis->InvalidateRect(NULL,false);
		}
		else
		{
			m_tabmenu.SetCurSel(0);
			AfxMessageBox("Password Protected");
		}

		UpdateData(false);
	}
	else if(index == TEARTAB)
	{
		if(pframe->SModify)
		{
			//pframe->m_pxaxis->Modify=true;
			m_pstats->ShowWindow(SW_HIDE);
			m_plimit->ShowWindow(SW_HIDE);
			m_pcap->ShowWindow(SW_HIDE);
			m_pmod->ShowWindow(SW_HIDE);
			m_psplice->ShowWindow(SW_HIDE);
			m_pstitchSetup->ShowWindow(SW_SHOW);
			m_pLabelMatingSetup->ShowWindow(SW_HIDE);
			m_pWaistInspection->ShowWindow(SW_HIDE);
			m_pWaistInspection2->ShowWindow(SW_HIDE);

			theapp->statsOpen		=	false;
			theapp->showCam5		=	false;
			theapp->capOpen			=	false;
			theapp->labelOpen		=	false;
			pframe->m_pxaxis->Modify=	false;
			theapp->tearOpen		=	true;
			theapp->showLargeImages = false;
			theapp->showLargeImages2 = false;
			theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;

			pframe->m_pstitchSetup->UpdateDisplay();
			pframe->m_pxaxis->InvalidateRect(NULL,false);
		}
		else
		{
			m_tabmenu.SetCurSel(0);
			AfxMessageBox("Password Protected");
		}

		UpdateData(false);
	}
	else if(index == ALIGNTAB)
	{
		if(pframe->SModify)
		{
			//pframe->m_pxaxis->Modify=true;
			m_pstats->ShowWindow(SW_HIDE);
			m_plimit->ShowWindow(SW_HIDE);
			m_pcap->ShowWindow(SW_HIDE);
			m_pmod->ShowWindow(SW_HIDE);
			m_psplice->ShowWindow(SW_HIDE);
			m_pstitchSetup->ShowWindow(SW_HIDE);
			m_pLabelMatingSetup->ShowWindow(SW_SHOW);
			m_pWaistInspection->ShowWindow(SW_HIDE);
			m_pWaistInspection2->ShowWindow(SW_HIDE);
			
			theapp->statsOpen		=	false;
			theapp->showCam5		=	false;
			theapp->capOpen			=	false;
			theapp->labelOpen		=	false;
			pframe->m_pxaxis->Modify=	false;
			theapp->tearOpen		=	false;
			theapp->showLargeImages =	false;
			theapp->showLargeImages2 = false;
			theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=true;

			pframe->m_pLabelMatingSetup->UpdateDisplay();
			pframe->m_pxaxis->InvalidateRect(NULL,false);			
		}
		else
		{
			m_tabmenu.SetCurSel(0);
			AfxMessageBox("Password Protected");
		}

		UpdateData(false);
	}
	else if(index == WAISTTAB)
	{
		if(pframe->SModify)
		{
			//pframe->m_pxaxis->Modify=true;
			m_pstats->ShowWindow(SW_HIDE);
			m_plimit->ShowWindow(SW_HIDE);
			m_pcap->ShowWindow(SW_HIDE);
			m_pmod->ShowWindow(SW_HIDE);
			m_psplice->ShowWindow(SW_HIDE);
			m_pstitchSetup->ShowWindow(SW_HIDE);
			m_pLabelMatingSetup->ShowWindow(SW_HIDE);
			m_pWaistInspection->ShowWindow(SW_SHOW);
			m_pWaistInspection2->ShowWindow(SW_HIDE);
			
			theapp->statsOpen		=	false;
			theapp->showCam5		=	false;
			theapp->capOpen			=	false;
			theapp->labelOpen		=	false;
			pframe->m_pxaxis->Modify=	false;
			theapp->tearOpen		=	false;
			theapp->showLargeImages =	true;
			theapp->showLargeImages2 = false;
			theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;

			pframe->m_pWaistInspection->UpdateDisplay();
			pframe->m_pxaxis->InvalidateRect(NULL,false);			
		}
		else
		{
			m_tabmenu.SetCurSel(0);
			AfxMessageBox("Password Protected");
		}

		UpdateData(false);
	}
	else if(index == WAIST2TAB)
	{
		if(pframe->SModify)
		{
			//pframe->m_pxaxis->Modify=true;
			m_pstats->ShowWindow(SW_HIDE);
			m_plimit->ShowWindow(SW_HIDE);
			m_pcap->ShowWindow(SW_HIDE);
			m_pmod->ShowWindow(SW_HIDE);
			m_psplice->ShowWindow(SW_HIDE);
			m_pstitchSetup->ShowWindow(SW_HIDE);
			m_pLabelMatingSetup->ShowWindow(SW_HIDE);
			m_pWaistInspection->ShowWindow(SW_HIDE);
			m_pWaistInspection2->ShowWindow(SW_SHOW);

			theapp->statsOpen		=	false;
			theapp->showCam5		=	false;
			theapp->capOpen			=	false;
			theapp->labelOpen		=	false;
			pframe->m_pxaxis->Modify=	false;
			theapp->tearOpen		=	false;
			theapp->showLargeImages = false;
			theapp->showLargeImages2 = true;
			theapp->jobinfo[pframe->CurrentJobNum].showLabelMating=false;

			pframe->m_pWaistInspection2->UpdateDisplay();
			pframe->m_pxaxis->InvalidateRect(NULL,false);
		}
		else
		{
			m_tabmenu.SetCurSel(0);
			AfxMessageBox("Password Protected");
		}

		UpdateData(false);
	}

	return CView::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}
