#if !defined(AFX_WAISTINSPECTION2_H__787F2A18_45DE_4C4B_8009_09234EDB7064__INCLUDED_)
#define AFX_WAISTINSPECTION2_H__787F2A18_45DE_4C4B_8009_09234EDB7064__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WaistInspection2.h : header file
//

#define FullImageLeftEdge 0
#define FullImageRightEdge FullImageScaledRotatedWidth-1
#define FullImageTopEdge 0
#define FullImageBottomEdge FullImageScaledRotatedHeight-1

#define thresholdMin 1
#define thresholdMax 255

#define verticalSearchLengthMin 1
#define verticalSearchLengthMax 30

#define horizontalSearchLengthMin 1
#define horizontalSearchLengthMax 30

#define avgWindowSize1Min 1
#define avgWindowSize1Max 30

#define deviationMin 2
#define deviationMax 50

#define waistWidthThresholdMin 1
#define waistWidthThresholdMax FullImageScaledRotatedWidth

#define smallPixelStepSize 1
#define largePixelStepSize 10

#define smallPixelIntensityStepSize 1
#define largePixelIntensityStepSize 5

/////////////////////////////////////////////////////////////////////////////
// WaistInspection2 dialog

class WaistInspection2 : public CDialog
{
// Construction
public:
	WaistInspection2(CWnd* pParent = NULL);   // standard constructor

	CBottleApp *theapp;
	CMainFrame* pframe;

	int pixelStepSize;
	int pixelIntensityStepSize;
	bool isSmallStepSize;
	bool isCam2Settings;

	void UpdateWaistWidthOverlayValues();
	void UpdateAndSaveSetupValues();
	void UpdateDisplay();

// Dialog Data
	//{{AFX_DATA(WaistInspection2)
	enum { IDD = IDD_WAIST_INSPECTION2 };
	CButton	m_stepSize;
	CButton	m_cam1cam2;
	CString	m_horizontalWindowLength1;
	CString	m_threshold1;
	CString	m_verticalWindowLength1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(WaistInspection2)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(WaistInspection2)
	afx_msg void OnStepSize();
	afx_msg void OnCam1UpperXLeft();
	afx_msg void OnCam1UpperXRight();
	afx_msg void OnCam1UpperYUp();
	afx_msg void OnCam1UpperYDown();
	afx_msg void OnCam1WidthUpperLess();
	afx_msg void OnCam1WidthUpperMore();
	afx_msg void OnCam1HeightUpperMore();
	afx_msg void OnCam1HeightUpperLess();
	afx_msg void OnCam1XLeft();
	afx_msg void OnCam1XRight();
	afx_msg void OnCam1YUp();
	afx_msg void OnCam1YDown();
	afx_msg void OnCam1WidthLess();
	afx_msg void OnCam1WidthMore();
	afx_msg void OnCam1HeightMore();
	afx_msg void OnCam1HeightLess();
	afx_msg void OnCam1threshold1Down();
	afx_msg void OnCam1threshold1Up();
	afx_msg void OnCam1vlength1Down();
	afx_msg void OnCam1vlength1Up();
	afx_msg void OnCam1hlength1Down();
	afx_msg void OnCam1hlength1Up();
	afx_msg void OnCam1cam4();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};









//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WAISTINSPECTION2_H__787F2A18_45DE_4C4B_8009_09234EDB7064__INCLUDED_)
