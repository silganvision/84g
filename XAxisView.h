#if !defined(AFX_XAXISVIEW_H__00A332A9_BC77_4C8D_B269_55255414C830__INCLUDED_)
#define AFX_XAXISVIEW_H__00A332A9_BC77_4C8D_B269_55255414C830__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// XAxisView.h : header file
//
#include "VisCore.h"
#include "Globals.h"



typedef struct  {
	int R; 
	int G; 
	int B;}ColorRGB;

typedef struct  {
	int x; 
	int y; } SinglePoint;

typedef struct  {
	int x; 
	int y; 
	float c; } SpecialPoint;


typedef struct  {
	int p1; 
	int p2; 
	int p3; 
	int p4; 
	int p5; 
	int p6; 
	int p7; 
	int p8;  } EightPoint;

typedef struct  {
	float x; 
	float y; } SinglePointF;

typedef struct  {
	int x; 
	int y; 
	int x1; 
	int y1; } TwoPoint;

typedef struct  {
	int x; 
	int y; 
	int x1; 
	int y1; } DoublePoint;

typedef struct  {
	int x; 
	int y; 
	int w; 
	int h; } appRect;

struct SingPtXAsc
{
	bool operator()(const SinglePoint & a, const SinglePoint & b)const
	{return a.x < b.x;}
};


typedef struct linearRegData{
	linearRegData():avgx(0),avgy(0),incl(0),slop(0),
		angIncRad(0), angIncDeg(0), angSloRad(0), angSloDeg(0)
	{}
	float avgx;
	float avgy;
	float incl;
	float slop;
	float angIncRad; float angIncDeg;
	float angSloRad; float angSloDeg;
}linearRegData;


//----------------------------------------------------------------------------------------------
#define bytesPerPixel 4

#define camera1CroppedImageWidth 1024
#define camera1CroppedImageHeight 408
#define camera1CroppedImageXOffset 132
#define camera1croppedImageYOffset 278

#define camera1FullImageWidth 1288
#define camera1FullImageHeight 964 
#define camera1FullImageXoffset 0
#define camera1FullImageYoffset 0

#define camera1FullImageScaleDownFactor 3

#define camera1FullImageScaledWidth camera1FullImageWidth/camera1FullImageScaleDownFactor
#define camera1FullImageScaledHeight camera1FullImageHeight/camera1FullImageScaleDownFactor

#define camera1FullImageScaledRotatedWidth camera1FullImageScaledHeight
#define camera1FullImageScaledRotatedHeight camera1FullImageScaledWidth

//----------------------------------------------------------------------------------------------

#define camera4CroppedImageWidth 1024
#define camera4CroppedImageHeight 408
#define camera4CroppedImageXOffset 132
#define camera4croppedImageYOffset 278

#define camera4FullImageWidth 1288
#define camera4FullImageHeight 964 
#define camera4FullImageXoffset 0
#define camera4FullImageYoffset 0

#define camera4FullImageScaleDownFactor 3

#define camera4FullImageScaledWidth camera1FullImageWidth/camera1FullImageScaleDownFactor
#define camera4FullImageScaledHeight camera1FullImageHeight/camera1FullImageScaleDownFactor

#define camera4FullImageScaledRotatedWidth camera1FullImageScaledHeight
#define camera4FullImageScaledRotatedHeight camera1FullImageScaledWidth

//-------------------------------------------------------------------------------------------------

#define camera2CroppedImageWidth 1024
#define camera2CroppedImageHeight 408
#define camera2CroppedImageXOffset 132
#define camera2croppedImageYOffset 278

#define camera2FullImageWidth 1288
#define camera2FullImageHeight 964 
#define camera2FullImageXoffset 0
#define camera2FullImageYoffset 0

#define camera2FullImageScaleDownFactor 3

#define camera2FullImageScaledWidth camera2FullImageWidth/camera2FullImageScaleDownFactor
#define camera2FullImageScaledHeight camera2FullImageHeight/camera2FullImageScaleDownFactor

#define camera2FullImageScaledRotatedWidth camera2FullImageScaledHeight
#define camera2FullImageScaledRotatedHeight camera2FullImageScaledWidth

//-------------------------------------------------------------------------------------------------
#define camera3CroppedImageWidth 1024
#define camera3CroppedImageHeight 408
#define camera3CroppedImageXOffset 132
#define camera3croppedImageYOffset 278

#define camera3FullImageWidth 1288
#define camera3FullImageHeight 964 
#define camera3FullImageXoffset 0
#define camera3FullImageYoffset 0

#define camera3FullImageScaleDownFactor 3

#define camera3FullImageScaledWidth camera3FullImageWidth/camera3FullImageScaleDownFactor
#define camera3FullImageScaledHeight camera3FullImageHeight/camera3FullImageScaleDownFactor

#define camera3FullImageScaledRotatedWidth camera3FullImageScaledHeight
#define camera3FullImageScaledRotatedHeight camera3FullImageScaledWidth

#define MainDisplayImageHeight 1024
#define MainDisplayImageWidth 408

#define MainDisplayBWImageSize MainDisplayImageHeight*MainDisplayImageWidth
#define MainDisplayColorImageSize MainDisplayImageHeight*MainDisplayImageWidth*bytesPerPixel

#define WaistDisplayImageHeight 1288
#define WaistDisplayImageWidth 964

#define WaistDisplayColorImageSize WaistDisplayImageHeight*WaistDisplayImageWidth*bytesPerPixel

#define CroppedCameraHeight 408
#define CroppedCameraWidth 1024



#define MainCameraDisplayYOffset 0
#define MainCameraDisplayXOffset 0

#define CapInspImageDisplayHeight 408
#define CapInspImageDisplayWidth 1024

#define CroppedImageYOffset 288
#define CroppedImageNoYOffset 0

#define FullImageScaleDownFactor 3

#define FullImageScaledWidth WaistDisplayImageHeight/FullImageScaleDownFactor
#define FullImageScaledHeight WaistDisplayImageWidth/FullImageScaleDownFactor

#define FullImageScaledRotatedWidth FullImageScaledHeight
#define FullImageScaledRotatedHeight FullImageScaledWidth
/////////////////////////////////////////////////////////////////////////////
// CXAxisView view

class CXAxisView : public CView
{
	friend class CBottleApp;
	friend class CMainFrame;
	friend class CCamera;
protected:
	CXAxisView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CXAxisView)

// Attributes

public:

	int currEdgeL;
	int currEdgeR;
	int currEdgeM;

//-----------------------------------------------------------------------------------------------------------------------------------
	unsigned char cam1ImageFullScaled[camera1FullImageScaledWidth*camera1FullImageScaledHeight*bytesPerPixel];
	unsigned char cam1ImageFullScaledRotated[camera1FullImageScaledRotatedWidth*camera1FullImageScaledRotatedHeight*bytesPerPixel];
	unsigned char cam1ImageFullScaledRotatedGreyscale[camera1FullImageScaledRotatedWidth*camera1FullImageScaledRotatedHeight];

	std::vector<SinglePoint> cam1CoordinatesOfContainerLeftTopEdgePointsRaw;
	std::vector<SinglePoint> cam1CoordinatesOfContainerLeftTopEdgePointsFilled;

	std::vector<SinglePoint> cam1CoordinatesOfContainerLeftWaistEdgePointsRaw;
	std::vector<SinglePoint> cam1CoordinatesOfContainerLeftWaistEdgePointsFilled;

	std::vector<int> cam1WaistDeltas;

//-----------------------------------------------------------------------------------------------------------------------------------
	unsigned char cam4ImageFullScaled[camera1FullImageScaledWidth*camera1FullImageScaledHeight*bytesPerPixel];
	unsigned char cam4ImageFullScaledRotated[camera1FullImageScaledRotatedWidth*camera1FullImageScaledRotatedHeight*bytesPerPixel];
	unsigned char cam4ImageFullScaledRotatedGreyscale[camera1FullImageScaledRotatedWidth*camera1FullImageScaledRotatedHeight];

	std::vector<SinglePoint> cam4CoordinatesOfContainerLeftTopEdgePointsRaw;
	std::vector<SinglePoint> cam4CoordinatesOfContainerLeftTopEdgePointsFilled;

	std::vector<SinglePoint> cam4CoordinatesOfContainerLeftWaistEdgePointsRaw;
	std::vector<SinglePoint> cam4CoordinatesOfContainerLeftWaistEdgePointsFilled;

	std::vector<int> cam4WaistDeltas;

//-----------------------------------------------------------------------------------------------------------------------------------
	unsigned char cam2ImageFullScaled[camera2FullImageScaledWidth*camera2FullImageScaledHeight*bytesPerPixel];
	unsigned char cam2ImageFullScaledRotated[camera2FullImageScaledRotatedWidth*camera2FullImageScaledRotatedHeight*bytesPerPixel];
	unsigned char cam2ImageFullScaledRotatedGreyscale[camera2FullImageScaledRotatedWidth*camera2FullImageScaledRotatedHeight];

	std::vector<SinglePoint> cam2CoordinatesOfContainerLeftTopEdgePointsRaw;
	std::vector<SinglePoint> cam2CoordinatesOfContainerLeftTopEdgePointsFilled;

	std::vector<SinglePoint> cam2CoordinatesOfContainerLeftWaistEdgePointsRaw;
	std::vector<SinglePoint> cam2CoordinatesOfContainerLeftWaistEdgePointsFilled;

	std::vector<int> cam2WaistDeltas;

//-----------------------------------------------------------------------------------------------------------------------------------
	unsigned char cam3ImageFullScaled[camera3FullImageScaledWidth*camera3FullImageScaledHeight*bytesPerPixel];
	unsigned char cam3ImageFullScaledRotated[camera3FullImageScaledRotatedWidth*camera3FullImageScaledRotatedHeight*bytesPerPixel];
	unsigned char cam3ImageFullScaledRotatedGreyscale[camera3FullImageScaledRotatedWidth*camera3FullImageScaledRotatedHeight];

	std::vector<SinglePoint> cam3CoordinatesOfContainerLeftTopEdgePointsRaw;
	std::vector<SinglePoint> cam3CoordinatesOfContainerLeftTopEdgePointsFilled;

	std::vector<SinglePoint> cam3CoordinatesOfContainerLeftWaistEdgePointsRaw;
	std::vector<SinglePoint> cam3CoordinatesOfContainerLeftWaistEdgePointsFilled;

	std::vector<int> cam3WaistDeltas;
//-----------------------------------------------------------------------------------------------------------------------------------


	__int64 nDiff0;
	double spanElapsed0;		//Used to hold time in seconds of how long XAxisView.Inspect() takes.
	LARGE_INTEGER  timeStart0,	//Used to hold time at which XAxisView.Inspect() starts.
				   timeEnd0,	//Used to hold time at which XAxisView.Inspect() ends.
				   Frequency0;	//Used to convert processor cycles to seconds time measurements.


//-----------------------------------------------------------------------------------------------------------------------------------

	void Rotate90Right(const BYTE *image, int imageWidth, int imageHeight, int bytesPPixel, BYTE *rotatedImage);
	void Rotate270Right(const BYTE *image, int imageWidth, int imageHeight, int bytesPPixel, BYTE *rotatedImage);
	void ScaleImageDownBySubsampling(const BYTE *image, int imageWidth, int imageHeight, int bytesPPixel, int scaleFactor, BYTE *scaledImage);
	void ScaleImageDownByAveraging(const BYTE *image, int imageWidth, int imageHeight, int bytesPPixel, int scaleFactor, BYTE *scaledImage);
	
	void ConvertFromBGRUToGreyscale(const BYTE *image, int imageWidth, int imageHeight, BYTE *GreyscaleImage);

	void ComposeCam1WaistInspection();
	void ComposeCam2WaistInspection();
	void ComposeCam3WaistInspection();
	void ComposeCam4WaistInspection();

	void FillEdge(const std::vector<SinglePoint> &edgeCoordinates, 
				  int avgWindowSize, int edgeCoordinateDeviation,
				  bool fillTopToBottom,
				  std::vector<SinglePoint> &edgeCoordinatesFilled);


	void FindEdge(BYTE *img, int imageWidth, int imageHeight,
				  int verticalTopBound, int verticalBottomBound,
				  int horizontalLeftBound, int horizontalRightBound,
				  int verticalSearchLength, int horizontalSearchLength, 
				  int edgeThreshold,
				  int searchLeftToRight,
				  std::vector<SinglePoint> &outCoordinatesOfEdgePoints);


	int AveragePixels(BYTE *img, int imageWidth, int imageHeight,
					   int averagingWindowLength,
					   int averagingCenterYCoordinate, int averagingCenterXCoordinate,
					   bool isVerticalAverage);

	void AllocateAndCopyImage(int index);

	int FindAverageXValue(const std::vector<SinglePoint> &coordinates);

	void FindDeltas(const std::vector<SinglePoint> &leftEdgeCoordinates,
				    const std::vector<SinglePoint> &rightEdgeCoordinates,
				    std::vector<int> &edgeCoordinateDeltas);

	int CountWaistWidthAboveThreshold(const std::vector<int> &edgeCoordinateDeltas, int threshold);

	void InitializeSinglePointCoordinates(int startY, int endY, int defualtX,
										  std::vector<SinglePoint> &outSinglePointCoordinates);

//-----------------------------------------------------------------------------------------------------------------------------------

	int histMethod5[260];
	int histMeth5[8][260];;
	int	transLabel[8][1050];
	int	transLabelX[8][1050];
	int	transLabelY[8][1050];

	TwoPoint FindLabelEdge_Method5(BYTE *imgIn, int nCam, int fact, int limYt, int limYb,int limX, int limW);
	
	TwoPoint FindLabelEdge_Method5C1(BYTE *imgIn, int nCam, int fact, int limYt, int limYb,int limX, int limW);
	TwoPoint FindLabelEdge_Method5C2(BYTE *imgIn, int nCam, int fact, int limYt, int limYb,int limX, int limW);
	TwoPoint FindLabelEdge_Method5C3(BYTE *imgIn, int nCam, int fact, int limYt, int limYb,int limX, int limW);
	TwoPoint FindLabelEdge_Method5C4(BYTE *imgIn, int nCam, int fact, int limYt, int limYb,int limX, int limW);

	void createHistAndBinarize(	BYTE *imgR1, BYTE *imgG1, BYTE *imgB1,
								BYTE *imgR2, BYTE *imgG2, BYTE *imgB2,
								BYTE *imgR3, BYTE *imgG3, BYTE *imgB3,
								BYTE *imgR4, BYTE *imgG4, BYTE *imgB4,
								BYTE *imgIn1, BYTE *imgIn2, BYTE *imgIn3, BYTE *imgIn4,
								BYTE *imgBin1,BYTE *imgBin2,BYTE *imgBin3,BYTE *imgBin4,
								int tX, int tY, int tH,
								int bX, int bY, int bH,
								int W,	int fact);


	void createHistAndBinarizeC1(	BYTE *imgR1, BYTE *imgG1, BYTE *imgB1,
									BYTE *imgIn1,
									BYTE *imgBin1,
									int nCam,
									int tX, int tY, int tH,
									int bX, int bY, int bH,
									int W,	int fact);


	void createHistAndBinarizeC2(	BYTE *imgR1, BYTE *imgG1, BYTE *imgB1,
									BYTE *imgIn1,
									BYTE *imgBin1,
									int nCam,
									int tX, int tY, int tH,
									int bX, int bY, int bH,
									int W,	int fact);

	void createHistAndBinarizeC3(	BYTE *imgR1, BYTE *imgG1, BYTE *imgB1,
									BYTE *imgIn1,
									BYTE *imgBin1,
									int nCam,
									int tX, int tY, int tH,
									int bX, int bY, int bH,
									int W,	int fact);

	void createHistAndBinarizeC4(	BYTE *imgR1, BYTE *imgG1, BYTE *imgB1,
									BYTE *imgIn1,
									BYTE *imgBin1,
									int nCam,
									int tX, int tY, int tH,
									int bX, int bY, int bH,
									int W,	int fact);
	int bxMethod5_Xt[8]; 
	int bxMethod5_Xb[8]; 

	void method5_CalcRGBvalues(	BYTE *img1R, BYTE *img1G, BYTE *img1B, 
								BYTE *img2R, BYTE *img2G, BYTE *img2B, 
								BYTE *img3R, BYTE *img3G, BYTE *img3B,
								BYTE *img4R, BYTE *img4G, BYTE *img4B,
								int tX, int tY, int tH,
								int bX, int bY, int bH,
								int W,	int fact,
								int *pRt, int *pGt, int *pBt,	float *pHt,
								int *pRb, int *pGb, int *pBb,	float *pHb);

	int varianceSplice[8];
	int sdeviatiSplice[8];

	int xLBorder[8];
	int xRBorder[8];


	void SaveTempPic(BYTE *im_sr, int res);

	void SaveTemplateX(BYTE *im_sr2, BYTE *im_sr3, int ncam, int pattsel);
	char	currentpathTxt[60];	char	currentpathBmp[60];
	char	currentpathTxt2[60];char	currentpathBmp2[60];
	char	currentpathTxt3[60];char	currentpathBmp3[60];
	CVisByteImage	imagepat2;
	CVisByteImage	imagepat3;
	void LearnPattX(BYTE *imgInby6, BYTE *imgInby3, int posPattX1, int posPattY1, int cam, int pattsel);

	int		widthBy3;
	int		heightBy3;	
	
	int		widthBy6;
	int		heightBy6;

	bool gotPic1;
	bool gotPic2;
	bool gotPic3;
	bool gotPic4;
	bool gotPic5;

	int	xShift[8];
	int yShift[8];
	int hShift[8];

	SpecialPoint checkNoLabel1(int nCam, int fac, BYTE *imgIn, int x0, int y0, int x1, int h0, int w1, int h1);
	SpecialPoint checkNoLabel2(int nCam, int fac, BYTE *imgIn, int x0, int y0, int x1, int h0, int w1, int h1);
	SpecialPoint checkNoLabel3(int nCam, int fac, BYTE *imgIn, int x0, int y0, int x1, int h0, int w1, int h1);
	SpecialPoint checkNoLabel4(int nCam, int fac, BYTE *imgIn, int x0, int y0, int x1, int h0, int w1, int h1);

	int histTransition[8][280];
	int	histcntTotalTrans[8];

	int pixValTotal[280];
	int pixCntTotal;

	int nPaint[8];	
	int xPaint[8][10000];
	int yPaint[8][10000];

	int		nhisPix[8];
	int		pixVal[8][300];
	bool	imgREqual[8][50];

	int		capLeftVal;
	int		capRighVal;

	//Tracking if cameras are done or not at 
	//the time of showing image
	bool	cameraDone[8][200]; 
	int		camDoneCter[8];

	//////////////////////////////////////
	
	linearRegData linearRegY(std::vector<SinglePoint> points);
	void verAxisPtsRemOutliers(std::vector<SinglePoint> &v);

	//Tear Inspection... areas to avoid
	int xAvoidIni[8];
	int xAvoidEnd[8];

	//Calculating Skewness
	int xL1[8];	int yL1[8];
	int xL2[8];	int yL2[8];
	float inclinL[8];
	float angIncDegL[8];

	int xR1[8];	int yR1[8];
	int xR2[8];	int yR2[8];
	float inclinR[8];
	float angIncDegR[8];

	//////////////////////

	//No Label parameters
	int xNoLbl[8];
	int yNoLbl[8];
	int wNoLbl[8];
	int hNoLbl[8];
	//////////////////////

	int	avgValOpenArea[8];
	int	minValOpenArea[8];
	int	maxValOpenArea[8];

	int shiftedMidd[8];
	int shiftedMiddWPoints[8];

	int	avgNoLabel[8];
	/////////////////
	int	avgBlock[8];
	int edgeBlock[8];

	/////////////////

	appRect seamRect;
	float edgeProp;

	int maxShift;
	int minShift;

	int maxShiftWPoints;

	int maxWidthShift;
	int minWidthShift;

	//Shifted Label... Jeremy request
	std::vector<SinglePoint> xLShiftPts[8];
	std::vector<SinglePoint> xRShiftPts[8];
	
	int qtyShiftLeft[8];
	int qtyShiftRigh[8];

	int xLShift[8];	int yLShift[8];
	int xRShift[8];	int yRShift[8];
	int widthShift[8];
	int middlShift[8];
	int	middlShiftWPoints[8];

	
	

	BYTE*	img1MiddBottle;
	BYTE*	img2MiddBottle;
	BYTE*	img3MiddBottle;
	BYTE*	img4MiddBottle;


	BYTE*	im_reduce1;
	BYTE*	im_reduce2;
	BYTE*	im_reduce3;
	BYTE*	im_reduce4;
	BYTE*	im_reduce5;
	
	BYTE*	im_cambw1; 
	BYTE*	im_cambw2; 
	BYTE*	im_cambw3;	
	BYTE*	im_cambw4;
	BYTE*	im_cambw5;
	BYTE*	im_cam5Bbw;
	BYTE*	im_cam5Gbw;
	BYTE*	im_cam5Rbw;


	BYTE*	im_cam1Bbw;
	BYTE*	im_cam2Bbw;
	BYTE*	im_cam3Bbw;
	BYTE*	im_cam4Bbw;

	BYTE*	im_cam1Gbw;
	BYTE*	im_cam2Gbw;
	BYTE*	im_cam3Gbw;
	BYTE*	im_cam4Gbw;

	BYTE*	im_cam1Rbw;
	BYTE*	im_cam2Rbw;
	BYTE*	im_cam3Rbw;
	BYTE*	im_cam4Rbw;

	
	BYTE*	img1MagDiv3;
	BYTE*	img2MagDiv3;
	BYTE*	img3MagDiv3;
	BYTE*	img4MagDiv3;
	
	BYTE*	img1YelDiv3;
	BYTE*	img2YelDiv3;
	BYTE*	img3YelDiv3;
	BYTE*	img4YelDiv3;




	BYTE*	img1CyaDiv3;
	BYTE*	img2CyaDiv3;
	BYTE*	img3CyaDiv3;
	BYTE*	img4CyaDiv3;


	BYTE*	img1SatDiv3;
	BYTE*	img2SatDiv3;
	BYTE*	img3SatDiv3;
	BYTE*	img4SatDiv3;

	BYTE*	img1Sa2Div3;
	BYTE*	img2Sa2Div3;
	BYTE*	img3Sa2Div3;
	BYTE*	img4Sa2Div3;

	
	BYTE*	img1ForNoLabelDiv3;
	BYTE*	img2ForNoLabelDiv3;
	BYTE*	img3ForNoLabelDiv3;
	BYTE*	img4ForNoLabelDiv3;

	BYTE*	img1ForSpliceDiv3;
	BYTE*	img2ForSpliceDiv3;
	BYTE*	img3ForSpliceDiv3;
	BYTE*	img4ForSpliceDiv3;

	BYTE*	img1ForPattMatchDiv3;
	BYTE*	img2ForPattMatchDiv3;
	BYTE*	img3ForPattMatchDiv3;
	BYTE*	img4ForPattMatchDiv3;

	BYTE*	byte5ForPattMatchDiv3;
	
	BYTE*	img1ForShiftedDiv3;
	BYTE*	img2ForShiftedDiv3;
	BYTE*	img3ForShiftedDiv3;
	BYTE*	img4ForShiftedDiv3;


	BYTE*	im_cam1bwDiv6;
	BYTE*	im_cam2bwDiv6;
	BYTE*	im_cam3bwDiv6;
	BYTE*	im_cam4bwDiv6;

	BYTE*	im_cam1RbwDiv6;
	BYTE*	im_cam2RbwDiv6;
	BYTE*	im_cam3RbwDiv6;
	BYTE*	im_cam4RbwDiv6;

	BYTE*	im_cam1GbwDiv6;
	BYTE*	im_cam2GbwDiv6;
	BYTE*	im_cam3GbwDiv6;
	BYTE*	im_cam4GbwDiv6;

	BYTE*	im_cam1BbwDiv6;
	BYTE*	im_cam2BbwDiv6;
	BYTE*	im_cam3BbwDiv6;
	BYTE*	im_cam4BbwDiv6;
	
	BYTE*	img1MagDiv6;
	BYTE*	img2MagDiv6;
	BYTE*	img3MagDiv6;
	BYTE*	img4MagDiv6;
	
	BYTE*	img1YelDiv6;
	BYTE*	img2YelDiv6;
	BYTE*	img3YelDiv6;
	BYTE*	img4YelDiv6;

	BYTE*	img1CyaDiv6;
	BYTE*	img2CyaDiv6;
	BYTE*	img3CyaDiv6;
	BYTE*	img4CyaDiv6;
	
	BYTE*	img1SatDiv6;
	BYTE*	img2SatDiv6;
	BYTE*	img3SatDiv6;
	BYTE*	img4SatDiv6;
	
	BYTE*	img1ForPattMatchDiv6;
	BYTE*	img2ForPattMatchDiv6;
	BYTE*	img3ForPattMatchDiv6;
	BYTE*	img4ForPattMatchDiv6;
	
	BYTE*	img1ForShiftedDiv6;
	BYTE*	img2ForShiftedDiv6;
	BYTE*	img3ForShiftedDiv6;
	BYTE*	img4ForShiftedDiv6;

	BYTE*	im1LighSeq;
	BYTE*	im2LighSeq;
	BYTE*	im3LighSeq;
	BYTE*	im4LighSeq;

	BYTE*	im1DarkSeq;
	BYTE*	im2DarkSeq;
	BYTE*	im3DarkSeq;
	BYTE*	im4DarkSeq;

	BYTE*	im1EdgeSeq;
	BYTE*	im2EdgeSeq;
	BYTE*	im3EdgeSeq;
	BYTE*	im4EdgeSeq;


	BYTE*	im_cam1EdgH;
	BYTE*	im_cam2EdgH;
	BYTE*	im_cam3EdgH;
	BYTE*	im_cam4EdgH;

	BYTE*	im_cam1EdgV;
	BYTE*	im_cam2EdgV;
	BYTE*	im_cam3EdgV;
	BYTE*	im_cam4EdgV;

	BYTE*	im_cam1Bin;
	BYTE*	im_cam2Bin;
	BYTE*	im_cam3Bin;
	BYTE*	im_cam4Bin;
	
	BYTE*	byteBinMet5C1;
	BYTE*	byteBinMet5C2;
	BYTE*	byteBinMet5C3;
	BYTE*	byteBinMet5C4;


	BYTE*	byteBin1Edges;
	BYTE*	byteBin2Edges;
	BYTE*	byteBin3Edges;
	BYTE*	byteBin4Edges;

	BYTE*	img1Edg;
	BYTE*	img2Edg;
	BYTE*	img3Edg;
	BYTE*	img4Edg;

	BYTE*	im_patl2;
	BYTE*	im_patshow;
	BYTE*	im_patr2x;
	BYTE*	im_patr2;
	BYTE*	im_pat;
	
	BYTE*	im_matchl;
	BYTE*	im_matchr;
	BYTE*	im_match2;
	BYTE*	im_mat;
	BYTE*	im_mat2;
	BYTE*	im_match3;
	BYTE*	im_mat3;
	BYTE*	im_pat3;

	BYTE*	im_trouble;
	BYTE*	im_color2;

	BYTE*	im_thresh;

	BYTE*	im_dataC;	
	BYTE*	im_dataC2;
	BYTE*	im_dataC3;
	BYTE*	im_dataC4;
	BYTE*	im_dataC5;

	BYTE	im_dataCFullRes[WaistDisplayColorImageSize];	
	BYTE	im_dataC2FullRes[WaistDisplayColorImageSize];
	BYTE	im_dataC3FullRes[WaistDisplayColorImageSize];
	BYTE	im_dataC4FullRes[WaistDisplayColorImageSize];
	BYTE	im_dataC5FullRes[WaistDisplayColorImageSize];

	BYTE	im_dataCCropped[MainDisplayColorImageSize];	
	BYTE	im_dataC2Cropped[MainDisplayColorImageSize];
	BYTE	im_dataC3Cropped[MainDisplayColorImageSize];
	BYTE	im_dataC4Cropped[MainDisplayColorImageSize];
	BYTE	im_dataC5Cropped[MainDisplayColorImageSize];

	BYTE*	imgBuf0;
	BYTE*	imgBuf0bw;
	BYTE*	imgBuf5bw;

	BYTE*	img1LargeBuff;
	BYTE*	img2LargeBuff;
	BYTE*	img3LargeBuff;
	BYTE*	img4LargeBuff;
	//////////////

	BYTE*			imgStitchedFilLigh;
	BYTE*			imgStitchedFilDark;
	BYTE*			imgStitchedFilEdg;
	BYTE*			imgStitchedFilFinal;
	BYTE*			imgStitchedBW;

	CVisByteImage	imageStitchedFilLigh;
	CVisByteImage	imageStitchedFilDark;
	CVisByteImage	imageStitchedFilEdg;
	CVisByteImage	imageStitchedFilFinal;
	CVisByteImage	imageStitchedbw;

	//////////////

	BYTE*			imgStitchedLigh;
	BYTE*			imgStitchedDark;
	BYTE*			imgStitchedEdg;

	CVisByteImage	imageStitchedLigh;
	CVisByteImage	imageStitchedDark;
	CVisByteImage	imageStitchedEdg;

	//////////

	//Xtra patterns
	BYTE*	bytePattBy6[5];
	BYTE*	bytePattBy3[5];

	BYTE* bytePattBy6_method6_1;
	BYTE* bytePattBy3_method6_1;

	BYTE* bytePattBy6_method6;
	BYTE* bytePattBy3_method6;

	BYTE* bytePattBy6_method6_num3;
	BYTE* bytePattBy3_method6_num3;

	//Images for Stitching
	BYTE* img_cam1Ligh;
	BYTE* img_cam2Ligh;
	BYTE* img_cam3Ligh;
	BYTE* img_cam4Ligh;

	BYTE* img_cam1Dark;
	BYTE* img_cam2Dark;
	BYTE* img_cam3Dark;
	BYTE* img_cam4Dark;

	BYTE* img_cam1Edge;
	BYTE* img_cam2Edge;
	BYTE* img_cam3Edge;
	BYTE* img_cam4Edge;

	//FlyCaptureImage		m_imageProcessed;
	//FlyCaptureImage		m_imageProcessed2;
	//FlyCaptureImage		m_imageProcessed3;
	//FlyCaptureImage		m_imageProcessed4;
	//FlyCaptureImage		m_imageProcessed5;

	CVisByteImage		imagepat;
	CVisByteImage		imageS;

	CVisRGBAByteImage	imageFile1;
	CVisRGBAByteImage	imageFile2;
	CVisRGBAByteImage	imageFile3;
	CVisRGBAByteImage	imageFile4;
	CVisRGBAByteImage	imageFile5;
	CVisRGBAByteImage	imageCS;
	
	CVisRGBAByteImage	imageC;
	CVisRGBAByteImage	imageC2;
	CVisRGBAByteImage	imageC3;
	CVisRGBAByteImage	imageC4;
	CVisRGBAByteImage	imageC5;
	

	CVisRGBAByteImage	imageC1Large;
	CVisRGBAByteImage	imageC2Large;
	CVisRGBAByteImage	imageC3Large;
	CVisRGBAByteImage	imageC4Large;
	////////////

	CVisByteImage	imageBW[8];
	CVisByteImage	imageYel[8]; 
	CVisByteImage	imageMag[8];
	CVisByteImage	imageCya[8];
	CVisByteImage	imageSat[8];
	CVisByteImage	imageSa2[8];
	CVisByteImage	imageBWB[8];
	CVisByteImage	imageBWR[8];
	CVisByteImage	imageBWG[8];
	CVisByteImage	imageSobH[8];
	CVisByteImage	imageSobV[8];

	CVisByteImage	imageBin[8];
	CVisByteImage	imageBinEdges[8];
	CVisByteImage	imageBinMeth5[8];
	

	////////////

	int xPattInStitched;
	int yPattInStitched;

	float corrLeft;
	float corrMidd;
	float corrRigh;

	int StitchedLength;
	int StitchedHeight;

	int stitchX[8];
	int stitchY[8];
	int stitchW;
	int stitchH;

	//////////////

	int camLeft, camMidd, camRigh;


	int Stitch4ImgX(int wBox, int hBox, 
		BYTE *im_Ligh1, BYTE *im_Dark1, BYTE *im_Edge1, int xBox1, int yBox1, 
		BYTE *im_Ligh2, BYTE *im_Dark2, BYTE *im_Edge2, int xBox2, int yBox2, 
		BYTE *im_Ligh3, BYTE *im_Dark3, BYTE *im_Edge3, int xBox3, int yBox3, 
		BYTE *im_Ligh4, BYTE *im_Dark4, BYTE *im_Edge4, int xBox4, int yBox4, 
		int camPatt, TwoPoint pattPos,  
		bool takecam1, bool takecam2, bool takecam3, bool takecam4, int fact);

	SpecialPoint pattHighResLeft;
	SpecialPoint pattHighResMidd;
	SpecialPoint pattHighResRigh;

	SpecialPoint posPattBy6; float bestcorPattBy6;	int bestcamPattBy6;		int bestPattTypBy6;

	CString txtBestPatt;

	CBottleApp	*theapp;
	CMainFrame	*pframe;
	CFile CapImage;

	SpecialPoint pattLeftPosBy6[5];
	SpecialPoint pattMiddPosBy6[5];
	SpecialPoint pattRighPosBy6[5];

	SpecialPoint pattLeftPosBy3[5];

	SpecialPoint pattLeftPosBy3_method6_1[5];
	SpecialPoint pattLeftPosBy6_method6_1[5];

	SpecialPoint pattLeftPosBy3_method6[5];
	SpecialPoint pattLeftPosBy6_method6[5];

	SpecialPoint pattLeftPosBy3_method6_num3[5];
	SpecialPoint pattLeftPosBy6_method6_num3[5];

	int		sizepattBy12;
	int		sizepattBy6;
	int		sizepattBy3W, sizepattBy3H;

	bool	wehaveTempX;

	bool teachingMiddle;

    __int64 nDiff;				
	double timeMicroSecondC1I0;	LARGE_INTEGER m_lnC1StartI0,	m_lnC1EndI0,	m_lnC1FreqI0;
	double timeMicroSecondC2I0;	LARGE_INTEGER m_lnC2StartI0,	m_lnC2EndI0,	m_lnC2FreqI0;
	double timeMicroSecondC3I0;	LARGE_INTEGER m_lnC3StartI0,	m_lnC3EndI0,	m_lnC3FreqI0;
	double timeMicroSecondC4I0;	LARGE_INTEGER m_lnC4StartI0,	m_lnC4EndI0,	m_lnC4FreqI0;
	double timeMicroSecondC5I0;	LARGE_INTEGER m_lnC5StartI0,	m_lnC5EndI0,	m_lnC5FreqI0;

	double dMicroSecondC1I1;	LARGE_INTEGER m_lnC1StartI1,	m_lnC1EndI1,	m_lnC1FreqI1;
	double dMicroSecondC1I2;	LARGE_INTEGER m_lnC1StartI2,	m_lnC1EndI2,	m_lnC1FreqI2;
	double dMicroSecondC1I3;	LARGE_INTEGER m_lnC1StartI3,	m_lnC1EndI3,	m_lnC1FreqI3;
	double dMicroSecondC1I4;	LARGE_INTEGER m_lnC1StartI4,	m_lnC1EndI4,	m_lnC1FreqI4;
	double dMicroSecondC1I5;	LARGE_INTEGER m_lnC1StartI5,	m_lnC1EndI5,	m_lnC1FreqI5;
	double dMicroSecondC1I6;	LARGE_INTEGER m_lnC1StartI6,	m_lnC1EndI6,	m_lnC1FreqI6;
	double dMicroSecondC1I7;	LARGE_INTEGER m_lnC1StartI7,	m_lnC1EndI7,	m_lnC1FreqI7;
	double dMicroSecondC1I8;	LARGE_INTEGER m_lnC1StartI8,	m_lnC1EndI8,	m_lnC1FreqI8;
	double dMicroSecondC1I9;	LARGE_INTEGER m_lnC1StartI9,	m_lnC1EndI9,	m_lnC1FreqI9;
	double dMicroSecondC1I10;	LARGE_INTEGER m_lnC1StartI10,	m_lnC1EndI10,	m_lnC1FreqI10;
	double dMicroSecondC1I11;	LARGE_INTEGER m_lnC1StartI11,	m_lnC1EndI11,	m_lnC1FreqI11;	//Waist inspection

	double spanElapsed;			LARGE_INTEGER  timeStart,		timeEnd,		Frequency;
	double spanElapsedCam1DupAndCrop;		//Camera1 dulpication and cropping, done in do grab loop of camera 1.

	//*** Picture Log
	BYTE*	imgBuf3RGB_cam1;
	BYTE*	imgBuf3RGB_cam2;
	BYTE*	imgBuf3RGB_cam3;
	BYTE*	imgBuf3RGB_cam4;
	BYTE*	imgBuf3RGB_cam5;

	BYTE*	arr1[7];

	////////////////

	int		tim_stamp[1000][6];
	int		imge_no;

	int		typofault;
	int		showostore;

	int w[8];
	int h[8];

	//////////////
	int			xMidd[8];

	float		m[8];
	int			barX[8];
	int			barY[8];

	TwoPoint	middleRef[8];
	int			xmiddles[8][60];


	int			countTeachingPics[8];

	////////////////////////

	int			sumWhitePixInRow[1000];

	////////////////////////

	int			avgPixInRow[1000];
	int			bottleXL[6];
	int			bottleYL[6];
	int			bottleXR[6];
	int			bottleYR[6];
	int			bottleXspliceIni[6];
	int			bottleYspliceIni[6];
	int			yposSplice[6];
	int			minSpliceAvg[6];
	
	////////////////////////
	
	int			pic1;
	int			xdistPatt;
	SinglePoint	ledgePoint[50];
	char		currentpath[60];
	char		buf[10];

	//to save rectangle pattern
	int pattNum;


	int camForSeam;

	int C1LengthBWDiv3;
	int C1HeightBWDiv3;
	int C1LengthBWDiv6;
	int C1HeightBWDiv6;
	int C1LengthBWDiv12;
	int C1HeightBWDiv12;

/////////////

	void		OnLoadCap(int Job, bool left);

	bool		LearnDone;
	bool		StartLockOut;
	int			TimesThru;
	bool		RejectAll;
	
	TwoPoint	labelPos[7];
	TwoPoint	Ledge;
	
	TwoPoint	NoLbl[8];

	bool		reteachColor;
	bool		reteachColorCap;

	void	createBinMethod5(	BYTE* imgR, BYTE* imgG, BYTE* imgB, BYTE* byteBinOut,
							int fact, int nCam, 
							int tR, int tG, int tB, float tH, 
							int bR, int bG, int bB, float bH,
							int Intensity);

	BYTE* Reduce(BYTE* Image);
	BYTE* Reduce2(BYTE* Image);
	BYTE* Reduce3(BYTE* Image);
	BYTE* Reduce4(BYTE* Image);
	BYTE* Reduce5(BYTE* Image);

	CFile ImageFile;

	bool lockOut;
	bool first;
	SinglePoint MidCap;

	SinglePoint		saved[800];
	SinglePoint		seamPoint[100];

	float			CapW;
	float			CapLHeight;
	float			CapRHeight;
	float			LHArray[24];
	int				LHInc;
	float			RHArray[24];
	int				RHInc;
	float			LArray[24];
	int				LInc;
	float			DArray[24];
	int				DInc;
	float			TBArray[24];
	int				TBInc;
	float			TB2Array[24];
	int				TB2Inc;
	float			FArray[24];
	int				FInc;
	float			PArray[24];
	int				PInc;
	float			NoArray[24];
	int				NoInc;
	float			LCArray[24];
	int				LCInc;

	int				TLInc;
	float			TLArray[24];

	int				WsInc;
	float			WsArray[24];

	int				oldR[12];
	int				oldG[12];
	int				oldB[12];

	bool			inspResultOK[12];
	bool			inspResultOK_A[12];
	bool			inspResultOK_B[12];
	bool			fault1, fault2, fault3, fault4, fault5, fault6, fault7, fault8, fault9,fault10, fault11;

	SinglePointF	MidBot;
	SinglePointF	MidTop;
	SinglePoint		CapLSide;
	SinglePoint		CapRSide;
	SinglePoint		CapLSide3;
	SinglePoint		CapRSide3;
	SinglePoint		CapLTop;
	SinglePoint		CapRTop;
	SinglePoint		CapMTop;
	SinglePoint		RTopBar;
	SinglePoint		LTopBar;

	SinglePointF	MidTop2;
	SinglePoint		CapLSide2;
	SinglePoint		CapRSide2;
	SinglePoint		CapLTop2;
	SinglePoint		CapRTop2;
	SinglePoint		CapMTop2;
	SinglePoint		RTopBar2;
	SinglePoint		LTopBar2;

	SinglePoint		NeckLSide;
	SinglePoint		NeckRSide;
	SpecialPoint	NeckLLip;
	SpecialPoint	NeckRLip;
	DoublePoint		NeckInspR;
	DoublePoint		NeckInspL;
	SinglePoint		capTopMid;

	int			e1Cnt, e2Cnt, e3Cnt, e4Cnt;
	int			e5Cnt, e6Cnt, e7Cnt, e8Cnt;
	int			e9Cnt, e10Cnt, e11Cnt, e12Cnt;
 
	int			E1Cnt, E2Cnt, E3Cnt, E4Cnt;
	int			E5Cnt, E6Cnt, E7Cnt, E8Cnt;
	int			E9Cnt, E10Cnt, E11Cnt, E12Cnt;

	int			E1Cntx, E2Cntx, E3Cntx, E4Cntx;
	int			E5Cntx, E6Cntx, E7Cntx, E8Cntx;
	int			E9Cntx, E10Cntx, E11Cntx, E12Cntx;

	int			E1Cnt24, E2Cnt24, E3Cnt24, E4Cnt24;
	int			E5Cnt24, E6Cnt24, E7Cnt24, E8Cnt24;
	int			E9Cnt24, E10Cnt24, E11Cnt24, E12Cnt24 ;

	float		Psd;
	int			LCsd;
	float		Lsd;
	float		LHsd;
	float		RHsd;
	float		TBsd;
	float		TB2sd;
	float		NLsd;
	float		Fsd;
	float		Dsd;
	float		Usd;
	float		Topsd;
	float		Nosd;
	float		TLsd;
	float		Wssd;

	float		LCt;
	float		Pt;
	float		RHt;
	float		LHt;
	float		Lt;
	float		TLt;
	float		Wst;


	float		LHtf;
	float		RHtf;
	float		TBtf;
	float		TB2tf;
	float		Dtf;
	float		LCtf;
	float		Ft;
	float		Ftf;
	float		Ltf;
	float		TLtf;
	float		Wstf;

	float		Dt;
	float		Ut;
	float		Ptf;
	float		Utf;
	float		U2t;
	float		TBt;
	float		TB2t;
	float		Nlt;
	float		Not;
	float		Notf;

	float		Result1;
	float		Result1l;
	float		Result1r;
	float		Result2;
	float		Result3;
	float		Result4;
	float		Result5;
	float		Result6;
	float		Result7;
	float		Result8;
	float		Result9;
	float		Result10;
	float		Result11Cam1;
	float		Result11Cam2;
	float		Result11Cam3;
	float		Result11Cam4;
	float		Result11;
	float		Result12;



	bool		CapLFOK;
	bool		CapLROK;
	bool		CapTopOK;
	bool		CapLHOK;
	bool		CapRHOK;
	bool		CapDHOK;
	bool		CapFOK;
	bool		CapPLOK;
	bool		CapPTOK;

	bool		CapLFOKLt;
	bool		CapLROKLt;
	bool		CapTopOKLt;
	bool		CapLHOKLt;
	bool		CapRHOKLt;
	bool		CapDHOKLt;
	bool		CapFOKLt;
	bool		CapPLOKLt;
	bool		CapPTOKLt;
	bool		BadCap;
	bool		Modify;
	bool		BadCap2;
		
	int			labelPosY;
	int			zeroLine;
	int			piccount;

	SinglePoint MidBar;
	int			cam;
	
	float		z1, z2, z3, z4, z5;
	float		z6, z7, z8, z10;

	float		pct1, pct2, pct3, pct4;
	float		pct5, pct6, pct7, pct8;

	int			r1, r2, r3, r4, r5, r6, r7, r8;
	int			z1Sub, z2Sub, z3Sub, z4Sub, z5Sub, z6Sub, z7Sub, z8Sub, z9Sub, z10Sub;
	float		z1Avg, z2Avg, z3Avg, z4Avg, z5Avg, z6Avg, z7Avg, z8Avg, z9Avg, z10Avg;

	//@@@@@@@@@@@@@@@@@@@@@@@@@@

	bool		mFlgWaist[5];

public:
	void OnLoadPatternX_method6_num3(int CurrentJobNum, int cam);
	void SaveTemplateX_method6_num3(BYTE *im_sr2, BYTE *im_sr3, int ncam, int pattsel);

//-----------------------------------------------------------------------------------------------------
// Cocked label (alignment)  -- copy from 51R84 new inspection 6 --- Andrew Gawlik 2017-07-14
	SinglePoint cockedLabel1[100],cockedLabel2[100],cockedLabel3[100],cockedLabel4[100]; //##VK for drawing the result 

	int FindCockedLabel1(BYTE* image,int ncam,int factor);
	int FindCockedLabel2(BYTE* image,int ncam,int factor);
	int FindCockedLabel3(BYTE* image,int ncam,int factor);
	int FindCockedLabel4(BYTE* image,int ncam,int factor);

//-----------------------------------------------------------------------------------------------------
// Label Mating

	std::vector<SinglePoint> vector_points1,vector_points2,vector_points3,vector_points4;
	SinglePoint label_mating_results[5];


	void FindLabelTransition(BYTE* image, int imageWidth, int imageHeight,
							 int searchXOffSet, int searchYOffSet, 
							 int searchWidth, int searchHeight,
							 int taughtR, int taughtG, int taughtB,
							 int RTolerance, int GTolerance, int BTolerance,
							 int numberOfPixelsToCheck,
							 std::vector<SinglePoint> &foundPoints);


	void FindingLabelTransition_Cam4(BYTE* image, int, int );
	void FindingLabelTransition_Cam3(BYTE* image, int, int );
	void FindingLabelTransition_Cam2(BYTE* image, int, int );
	void FindingLabelTransition_Cam1(BYTE* image, int, int );


	void TeachLabelMatingColorCam1();
	void TeachLabelMatingColor(BYTE *image, int imageWidth, int imageHeight,
							   int xOffSet, int yOffSet, int teachWidth, int teachHeight,
							   int &taughtR, int &taughtG, int &taughtB);

	void FindBadLabelMating(std::vector<SinglePoint> &vectorPointsToUse, 
							int windowSize, int deltaBetweenWindows, int heightDeltaThreshold,
							SinglePoint &failingCoordinate, int &heightDeltaFound,int nCam);


	void OnLoadPatternX_method6(int CurrentJobNum, int cam);
	void SaveTemplateX_method6(BYTE *im_sr2, BYTE *im_sr3, int ncam, int pattsel);
	void LearnPattX_method6(BYTE *imgInby6, BYTE *imgInby3, int posPattX1, int posPattY1, int cam, int pattsel);

	//For pattern 3 of Method 6
	void LearnPattX_method6_num3(BYTE *imgInby6, BYTE *imgInby3, int posPattX1, int posPattY1, int cam, int pattsel);


	void DuplicateImage(unsigned char *originalImage, unsigned char *duplicateImage,
						int imageWidth, int imageHeight, int bytesPerPix);

	void CropCameraImage(unsigned char *imageToCrop, unsigned char *croppedImage,
						 int imageWidth, int imageHeight, 
						 int croppedImageWidth, int croppedImageHeight, 
						 int croppedImageXOffset, int croppedImageYOffset, 
						 int bytesPerPix);

	void edgeImgCreationC1(BYTE *imgIn, int fact, int nCam);
	void edgeImgCreationC2(BYTE *imgIn, int fact, int nCam);
	void edgeImgCreationC3(BYTE *imgIn, int fact, int nCam);
	void edgeImgCreationC4(BYTE *imgIn, int fact, int nCam);

	void DisplayImage(CVisImageBase& image, CDC* pDC);

	void edgeImgBinC1(BYTE *imgIn1, BYTE *imgIn2, BYTE *imgIn3, int fact, int nCam);
	void edgeImgBinC2(BYTE *imgIn1, BYTE *imgIn2, BYTE *imgIn3, int fact, int nCam);
	void edgeImgBinC3(BYTE *imgIn1, BYTE *imgIn2, BYTE *imgIn3, int fact, int nCam);
	void edgeImgBinC4(BYTE *imgIn1, BYTE *imgIn2, BYTE *imgIn3, int fact, int nCam);
	
	void calculateHistThreshold(int& thl, int& thh);

	void makeStitchImgSelection();
	void calcNoLabelCoordinates();
	void SaveColCustom1(BYTE *im_sr, int width, int height);
	
	void findShiftedEdges7_C1(int fact, int nCam);
	void findShiftedEdges7_C2(int fact, int nCam);
	void findShiftedEdges7_C3(int fact, int nCam);
	void findShiftedEdges7_C4(int fact, int nCam);

	SpecialPoint FindWhiteBand_C1(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindWhiteBand_C2(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindWhiteBand_C3(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindWhiteBand_C4(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);

	SpecialPoint FindBlackBand_C1(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindBlackBand_C2(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindBlackBand_C3(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindBlackBand_C4(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);
	
	void OnSaveDefSet(BYTE *im_sr1, BYTE *im_sr2, BYTE *im_sr3, BYTE *im_sr4, BYTE *im_sr5, int picnum);

	SpecialPoint FindLabelEdge_C1x(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);

	SpecialPoint FindLabelEdge_C1y6(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C2y6(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C3y6(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C4y6(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);

	void FindLabelEdge_C1z1(BYTE *imgIn, int lookinCam, int factor);
	void FindLabelEdge_C2z1(BYTE *imgIn, int lookinCam, int factor);
	void FindLabelEdge_C3z1(BYTE *imgIn, int lookinCam, int factor);
	void FindLabelEdge_C4z1(BYTE *imgIn, int lookinCam, int factor);

	TwoPoint FindLabelEdge_C1z2(BYTE *imgIn, int nCam, int factor, int thLow, int thHigh);
	TwoPoint FindLabelEdge_C2z2(BYTE *imgIn, int nCam, int factor, int thLow, int thHigh);
	TwoPoint FindLabelEdge_C3z2(BYTE *imgIn, int nCam, int factor, int thLow, int thHigh);
	TwoPoint FindLabelEdge_C4z2(BYTE *imgIn, int nCam, int factor, int thLow, int thHigh);

	SpecialPoint FindLabelEdge_C1yy(BYTE *imgIn, int nCam, int factor);
	SpecialPoint FindLabelEdge_C2yy(BYTE *imgIn, int nCam, int factor);
	SpecialPoint FindLabelEdge_C3yy(BYTE *imgIn, int nCam, int factor);
	SpecialPoint FindLabelEdge_C4yy(BYTE *imgIn, int nCam, int factor);

	SpecialPoint FindLabelEdge_C1y(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C2y(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C3y(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C4y(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);
	
	SpecialPoint FindLabelEdge_C1y2(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);
//	SpecialPoint FindLabelEdge_C2y2(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);
//	SpecialPoint FindLabelEdge_C3y2(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);
//	SpecialPoint FindLabelEdge_C4y2(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);

	SpecialPoint FindLabelEdge_C1y3(BYTE *imgIn, BYTE *imgInEdge, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C2y3(BYTE *imgIn, BYTE *imgInEdge, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C3y3(BYTE *imgIn, BYTE *imgInEdge, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C4y3(BYTE *imgIn, BYTE *imgInEdge, int lookinCam, int factor, SpecialPoint refPt);

	SpecialPoint FindLabelEdge_C1y4(BYTE *imgIn, BYTE *imgInEdge, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C2y4(BYTE *imgIn, BYTE *imgInEdge, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C3y4(BYTE *imgIn, BYTE *imgInEdge, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C4y4(BYTE *imgIn, BYTE *imgInEdge, int lookinCam, int factor, SpecialPoint refPt);

	SpecialPoint FindLabelEdge_C1y5(BYTE *imgIn, BYTE *imgInEdge, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C2y5(BYTE *imgIn, BYTE *imgInEdge, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C3y5(BYTE *imgIn, BYTE *imgInEdge, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C4y5(BYTE *imgIn, BYTE *imgInEdge, int lookinCam, int factor, SpecialPoint refPt);

	SpecialPoint FindLabelEdge_C1(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C2(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C3(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C4(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);	

	SpecialPoint FindLabelEdge_C1b(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C2b(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C3b(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindLabelEdge_C4b(BYTE *imgIn, int lookinCam, int factor, SpecialPoint refPt);	


	TwoPoint FindPatternWithDoubleCheck(BYTE* im_srcIn, int nCam);

	int			picNum;
	bool		integrityOK;
	TwoPoint	labelBox;
	int _tempCtr;

	TwoPoint	LabelSeam;

	bool		neckDefectToShow;
	bool		leftLipDefect;
	bool		tog;
	int			colorCount;
	int			targLabelR;
	int			targLabelG;
	int			targLabelB;
	bool		colorReady;
	
	COLORREF	AvgLabelColor;
	COLORREF	LabelColor1;
	COLORREF	LabelColor2;
	COLORREF	LabelColor3;
	COLORREF	LabelColor4;
	COLORREF	CheckForSplice(BYTE* im_color, int y);

	bool enter_OnDraw;
	int			resBest;
	int			resBest2;
	int			camToCheck;
	
	int			NeckOffsetX;
	int			rdist;
	int			ldist;
	int			XEnd;	
	int			XStart;
	bool		labelTaughtDone;
	TwoPoint	neckEdge;
	TwoPoint	sportBand;
	TwoPoint	Layer1, Layer2, Layer3, Layer4;
	TwoPoint	underLip;

	void		findBottleInPicC1(int nCam, int fact);
	void		findBottleInPicC2(int nCam, int fact);
	void		findBottleInPicC3(int nCam, int fact);
	void		findBottleInPicC4(int nCam, int fact);

	void		calcTearCoordinates();
	SpecialPoint FindAPattern(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor);
	void		OnLoad2X(int Job);
	void		SavePattern2X(BYTE *im_sr);
	void		LearnPattern2X(BYTE *im_srcIn, SinglePoint pos);
	
	SpecialPoint FindPattHigherRes_C1(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindPattHigherRes_C2(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindPattHigherRes_C3(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);
	SpecialPoint FindPattHigherRes_C4(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, SpecialPoint refPt);
	
	SpecialPoint FindPattA1_C1(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, int pattTyp);
	SpecialPoint FindPattA1_C2(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, int pattTyp);
	SpecialPoint FindPattA1_C3(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, int pattTyp);
	SpecialPoint FindPattA1_C4(BYTE *imgIn, BYTE *imgPatt, int lookinCam, int factor, int pattTyp);
	
	void		OnLoadPatternX(int Job, int pattType);

	TwoPoint	FindPatternX1_C1(BYTE* im_srcIn, int nCam);
	TwoPoint	FindPatternX1_C2(BYTE* im_srcIn, int nCam);
	TwoPoint	FindPatternX1_C3(BYTE* im_srcIn, int nCam);
	TwoPoint	FindPatternX1_C4(BYTE* im_srcIn, int nCam);
	
	TwoPoint	FindPatternX(BYTE* im_srcIn, int nCam);

	int			NoLabel(BYTE* im_src);
	int			CheckIntegrity(BYTE* im_src, TwoPoint BotPos);

	TwoPoint	findSideEdge(BYTE *imgIn, int nCam);

	void		findSplice(int nCam, int fact, int xSplice, int ySplice, int wSplice, int hSplice, int hsmSplice);




	void		SaveBWCustom1(BYTE *im_sr, int width, int height);
	void		LoadImagePic(int picNum);
	void		OnSaveSet(BYTE *im_sr1, BYTE *im_sr2,BYTE *im_sr3,BYTE *im_sr4,BYTE *im_sr5, int picnum);

	TwoPoint	CheckSportBand(BYTE* im_srcIn, SinglePoint CapLTop, SinglePoint CapRTop);
	
	TwoPoint	GetLayers(BYTE* im_src);

	BYTE*		LoadAnything(BYTE* image);

	
	void		SavePatternCap(BYTE *im_sr, int left);
	
	TwoPoint	FindLedge(BYTE* image, TwoPoint Bolt,int cam);
	TwoPoint	FindLedge2(BYTE* image, TwoPoint Bolt,int cam);
	TwoPoint	FindLedge3(BYTE* image, TwoPoint Bolt,int cam);
	TwoPoint	FindLedgeNatural(BYTE* image, TwoPoint Bolt,int cam);

	float		FindCapMid(BYTE* im_srcIn, float MidTop);
	void		LearnPatternOld(BYTE *im_srcIn, SinglePoint side, bool left);
	void		LearnPattern2(BYTE *im_srcIn, SinglePoint pos);

	SinglePointF Rgb2HS(ColorRGB rgb);
	bool		FoundLBolt(BYTE *c_image, int x, int y);
	bool		FoundLBolt2(BYTE *c_image, int x, int y, int cam);
	TwoPoint	FindPattern3(BYTE* im_srcIn);

	void		DoHide();

	TwoPoint	CheckLabel(BYTE* im_s, int y);
	TwoPoint	CheckLabelNatural(int nCam, int fact);
	DoublePoint CheckLabel2(BYTE* im_s, int x);

	void		Display(BYTE *image);
	void		Display2(BYTE *image);
	void		Display3(BYTE *image);
	void		Display4(BYTE *image);
	void		Display5(BYTE *image);
	
	SinglePoint FindCapSide(BYTE* im_srcIn, int MidCap, bool left);
	SinglePoint FindCapTop(BYTE* im_srcIn, int SideDist, int side);
	
	SinglePoint	FindNeckSide(BYTE* im_srcIn, int NeckPos, int CapPos,bool left);
	SinglePoint	FindNeckLip(BYTE* im_srcIn, SinglePoint NeckSide, bool left);
	SpecialPoint FindNeckLipPat(BYTE* im_srcIn, SinglePoint side, bool left);
	
	COLORREF	GetLabelColor(BYTE *im_srcC, BYTE *im_srcBW, int x, int y, int width, int thresh);
	TwoPoint	FindLine(BYTE *image, int vertpos);
	
	void		Derivative(BYTE *image, int xpos);
	void		OnReadC(char* pFileName);

	void		InitPic();

	void		OnLoad2(int Job);
	void		OnLoad3(int Job);
	int			FindMode(int *ar, int num);
	void		SavePattern(BYTE *im_sr);
	void		SavePattern2(BYTE *im_sr);
	void		SavePattern3(BYTE *im_sr);
	void		OnSaveBW(BYTE *im_sr, int who);
	void		OnSaveC(BYTE *im_sr, int who);

	TwoPoint	LookUnderLip(BYTE* im_src, SinglePoint left, SinglePoint right);
	DoublePoint LookUnderLip2(BYTE* im_src, int xpos, int ypos, int sen, bool left);
	void		Threshold(BYTE* image, int xpos);
	
	TwoPoint	Inspect1(BYTE* im_color, BYTE* im_src1);
	TwoPoint	Inspect2(BYTE* im_color, BYTE* im_src2);
	TwoPoint	Inspect3(BYTE* im_color, BYTE* im_src3);
	TwoPoint	Inspect4(BYTE* im_color, BYTE *im_src4);
	bool		Inspect5(BYTE* im_color, BYTE *im_src5);


	double		GetMax(int numvals, double a, double b, double c, double d);
	double		GetMin(int numvals, double a, double b, double c, double d);
	void		LearnPattern(BYTE *im_srcIn, SinglePoint side, bool left);
	
	void		DoResults();
	int			GetBestAvg(int p1, int p2, int p3);

	COLORREF	ColorTarget;
	COLORREF	ColorResult;
	COLORREF	DoUser(BYTE* im_srcIn, int nCam, int x, int y, int width, int UserStrength);

	COLORREF	ColorTarget2;
	COLORREF	ColorResult2;
	COLORREF	DoUser2(BYTE* im_srcIn, int nCam, int x, int y, int width, int UserStrength);
	
	///guide lines feature request
	///A. Gawlik
	void DrawGuides(CDC *displayContext,CPen* pen, CPen* pen_center, int separation, int height);
	///guides for cap centering feature request
	///A. Gawlik
	void DrawCapGuides(CDC* displayContext,CPen* pen, CPen* pen_center, int photo_eye_level);

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXAxisView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CXAxisView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CXAxisView)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XAXISVIEW_H__00A332A9_BC77_4C8D_B269_55255414C830__INCLUDED_)
