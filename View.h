#if !defined(AFX_VIEW_H__A0671511_92DF_4B58_8C09_C55B19855EDB__INCLUDED_)
#define AFX_VIEW_H__A0671511_92DF_4B58_8C09_C55B19855EDB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// View.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// View dialog

class View : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
// Construction
public:
	void UpdateDisplay2();

	CBottleApp *theapp;
	CMainFrame* pframe;
	int res;
	bool toggle;
	View(CWnd* pParent = NULL);   // standard constructor
//BOOL Create(DWORD dwstyle, const RECT& rect, CWnd* pParentWnd, UINT nID);
// Dialog Data
	//{{AFX_DATA(View)
	enum { IDD = IDD_VIEW };
	CButton	m_tbsname;
	CString	m_id1;
	CString	m_id2;
	CString	m_id3;
	CString	m_id4;
	CString	m_id5;
	CString	m_id6;
	CString	m_id7;
	CString	m_id8;
	CString	m_id9;
	CString	m_id10;
	CString	m_t10;
	CString	m_t3;
	CString	m_t4;
	CString	m_t5;
	CString	m_t6;
	CString	m_t7;
	CString	m_t8;
	CString	m_t9;
	CString	m_sd1;
	CString	m_sd4;
	CString	m_sd5;
	CString	m_sd6;
	CString	m_sd7;
	CString	m_sd8;
	CString	m_insp1;
	CString	m_insp2;
	CString	m_insp3;
	CString	m_insp4;
	CString	m_insp5;
	CString	m_insp6;
	CString	m_insp7;
	CString	m_insp8;
	CString	m_sd9;
	CString	m_sd10;
	CString	m_insp9;
	CString	m_time1;
	CString	m_time10;
	CString	m_time2;
	CString	m_time3;
	CString	m_time4;
	CString	m_time5;
	CString	m_time6;
	CString	m_time7;
	CString	m_time8;
	CString	m_time9;
	CString	m_picNum;
	CString	m_rate;
	CString	m_eject;
	CString	m_t1;
	CString	m_t1b;
	CString	m_t2;
	CString	m_sd1b;
	CString	m_sd2;
	CString	m_sd3;
	CString	m_insp10;
	CString	m_imgRead1;
	CString	m_imgRead2;
	CString	m_imgRead3;
	CString	m_imgRead4;
	CString	m_probCounter;
	CString	m_time0C1;
	CString	m_time0C2;
	CString	m_time0C3;
	CString	m_time0C4;
	CString	m_time0C5;
	CString	m_LeftCap;
	CString	m_RighCap;
	CString	m_sameImg;
	CString	m_imgRead5;
	CString	m_master;
	CString	m_finished1;
	CString	m_finished2;
	CString	m_finished3;
	CString	m_finished4;
	CString	m_finished5;
	CString	m_timeFinalProc;
	CString	m_timeVisionTotal;
	CString	m_timeInsDelay;
	CString	m_insp11;
	CString	m_id11;
	CString	m_t11;
	CString	m_sd11;
	CString	m_time11;
	CString m_insp12;
	//}}AFX_DATA
	
	CTabCtrl m_tabmenu2;
	void UpdateDisplay();
	//void OnSelchangeTabmenu2(NMHDR* pNMHDR, LRESULT* pResult) ;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(View)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(View)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnPictoshowup();
	afx_msg void OnPictoshowdown();
	afx_msg void OnEnapicstoshow();
	afx_msg void OnMisspic1();
	afx_msg void OnMisspic2();
	afx_msg void OnMisspic3();
	afx_msg void OnMisspic4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VIEW_H__A0671511_92DF_4B58_8C09_C55B19855EDB__INCLUDED_)
