// Job.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "Job.h"
#include "mainfrm.h"
#include "xaxisview.h"
#include "bottleview.h"
#include "serial.h"
#include "insp.h"
#include "modify.h"
#include "lim.h"
#include "saveas.h"
#include "camera.h"
#include "cap.h"
#include "WaistInspection.h"
#include "WaistInspection2.h"

#include "view.h"

#include <vector>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Job dialog


Job::Job(CWnd* pParent /*=NULL*/)
	: CDialog(Job::IDD, pParent)
{
	//{{AFX_DATA_INIT(Job)
	m_renamejob = _T("");
	//}}AFX_DATA_INIT
}


void Job::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Job)
	DDX_Control(pDX, IDC_RENAME, m_rename);
	DDX_Control(pDX, IDC_CHANGENAME, m_changename);
	DDX_Control(pDX, IDC_JOBS, m_joblist);
	DDX_Text(pDX, IDC_RENAME, m_renamejob);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Job, CDialog)
	//{{AFX_MSG_MAP(Job)
	ON_LBN_SELCHANGE(IDC_JOBS, OnSelchangeJobs)
	ON_BN_CLICKED(IDC_CHANGENAME, OnChangename)
	ON_BN_CLICKED(IDC_A, OnA)
	ON_BN_CLICKED(IDC_B, OnB)
	ON_BN_CLICKED(IDC_C, OnC)
	ON_BN_CLICKED(IDC_D, OnD)
	ON_BN_CLICKED(IDC_E, OnE)
	ON_BN_CLICKED(IDC_F, OnF)
	ON_BN_CLICKED(IDC_H, OnH)
	ON_BN_CLICKED(IDC_G, OnG)
	ON_BN_CLICKED(IDC_I, OnI)
	ON_BN_CLICKED(IDC_J, OnJ)
	ON_BN_CLICKED(IDC_K, OnK)
	ON_BN_CLICKED(IDC_L, OnL)
	ON_BN_CLICKED(IDC_M, OnM)
	ON_BN_CLICKED(IDC_N, OnN)
	ON_BN_CLICKED(IDC_O, OnO)
	ON_BN_CLICKED(IDC_P, OnP)
	ON_BN_CLICKED(IDC_Q, OnQ)
	ON_BN_CLICKED(IDC_R, OnR)
	ON_BN_CLICKED(IDC_S, OnS)
	ON_BN_CLICKED(IDC_T, OnT)
	ON_BN_CLICKED(IDC_U, OnU)
	ON_BN_CLICKED(IDC_V, OnV)
	ON_BN_CLICKED(IDC_W, OnW)
	ON_BN_CLICKED(IDC_X, OnX)
	ON_BN_CLICKED(IDC_Y, OnY)
	ON_BN_CLICKED(IDC_Z, OnZ)
	ON_BN_CLICKED(IDC_DEL, OnDel)
	ON_BN_CLICKED(IDC_DASH, OnDash)
	ON_BN_CLICKED(IDC_J0, OnJ0)
	ON_BN_CLICKED(IDC_J1, OnJ1)
	ON_BN_CLICKED(IDC_J2, OnJ2)
	ON_BN_CLICKED(IDC_J3, OnJ3)
	ON_BN_CLICKED(IDC_J4, OnJ4)
	ON_BN_CLICKED(IDC_J5, OnJ5)
	ON_BN_CLICKED(IDC_J6, OnJ6)
	ON_BN_CLICKED(IDC_J7, OnJ7)
	ON_BN_CLICKED(IDC_J8, OnJ8)
	ON_BN_CLICKED(IDC_J9, OnJ9)
	ON_WM_TIMER()
	ON_COMMAND(ID_SAVEAS, OnSaveas)
	ON_COMMAND(ID_DELETE, OnDelete)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Job message handlers

BOOL Job::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*) AfxGetMainWnd();
	pframe->m_pjob=this;

	theapp=(CBottleApp*)AfxGetApp();

	LoadJobs();
	m_joblist.SetCaretIndex(0);
	NameChanged=false;
	nIndex=0;
	original=JobOver=0;
	DoOver=false;
	SavAs=false;
	Value="";
	//UpdateData(true);	
	
	if(theapp->serPresent)	{pframe->m_pserial->SendChar(0,410);}

	pframe->OnLine=false;
	pframe->m_pview->SetTimer(2,500,NULL);//toggle red light
	pframe->m_pview->KillTimer(5);

	//////////////
	//	Based on fixed image size of 1024 x 408 for taught images	
	byteBuf3RGB		=	(BYTE*)GlobalAlloc(GMEM_FIXED, (408/3)*(1024/3 )* 4+100);
	byteTempRGB3	=	(BYTE*)GlobalAlloc(GMEM_FIXED, (408/3)*(1024/3 )* 4+100);

	CVisRGBAByteImage image3(408/3, 1024/3, 1, -1, byteBuf3RGB);
	image3.SetRect(0,0,408/3, 1024/3);
	imageC2Q3Taught	=	image3;				//to help creating the im_TemplateRGB3

	//////////////

	UpdateDisplay();
	UpdateData(true);	

		
	//pframe->UpdateOutput(4, true);
	//pframe->UpdateOutput(5, false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Job::LoadJobs()
{
	theapp=(CBottleApp*)AfxGetApp();

//////////////////////////////////
	//We need to clean the list before loading jobs
	m_joblist.ResetContent();

//////////////////////////////////////

	m_joblist.InsertString(0,"Job List");

	CString tmptxt;

	for(int i=1; i<=theapp->SavedTotalJobs; i++)
	{
		if(theapp->jobinfo[i].jobname=="")
		{theapp->jobinfo[i].jobname="xxx";}//+itoa();
		
		tmptxt.Format("[%02i]", i);
		tmptxt = tmptxt+theapp->jobinfo[i].jobname;

		//m_joblist.InsertString(i,theapp->jobinfo[i].jobname);
		m_joblist.InsertString(i,tmptxt);

		//m_joblist.InsertString(i,theapp->jobinfo[i].jobname);
	}

	UpdateData(false);
}

void Job::OnSelchangeJobs() 
{
/*
	DoOver=false;
	original=nIndex=m_joblist.GetCaretIndex();
	
	int n = m_joblist.GetTextLen( nIndex );

	m_joblist.GetText( nIndex, str.GetBuffer(n) );


	m_renamejob.Format("%s",str);
	pframe->m_pview->m_currentjob=pframe->m_pview->m_status=str;
	pframe->WasSetup=true;
	UpdateData(false);*/

	DoOver=false;

	original=nIndex=m_joblist.GetCaretIndex();
	int n = m_joblist.GetTextLen(nIndex);

	m_joblist.GetText( nIndex, str.GetBuffer(n) );
	
	if(nIndex!=0)
	{
		///////////////
		///cutting the first 4 digits
		CString str0=str.GetBuffer(0);
		int sizetot=str0.GetLength();
		CString str1=str0.Mid(4,sizetot-1);
		str=str1;	
		
		//////////////

		m_renamejob.Format("%s", str);

		LoadJobPic(nIndex);
	}
	
	UpdateDisplay();
	UpdateData(false);
	InvalidateRect(NULL,false);
}

void Job::OnOK() 
{
	if(!SavAs)	{nIndex=m_joblist.GetCaretIndex();}
	else				{nIndex=original;}

	theapp	=	(CBottleApp*)AfxGetApp();
	pframe	=	(CMainFrame*)AfxGetMainWnd();

	pframe->SaveJob(pframe->CurrentJobNum);//save old job
	pframe->CleanUp();
	
	pframe->CurrentJobNum=nIndex;
	pframe->GetJob(pframe->CurrentJobNum);
	DoEnables();

	//pframe->m_pview->m_currentjob=pframe->m_pview->m_status=theapp->jobinfo[pframe->CurrentJobNum].jobname;
	
	CString jobinfo; 
	jobinfo.Format("[%i] %s", nIndex, theapp->jobinfo[pframe->CurrentJobNum].jobname);
	pframe->m_pview->m_currentjob=jobinfo;
	pframe->m_pview->m_status=jobinfo;
	
	if(theapp->jobinfo[nIndex].taught || SavAs)
	{SavAs=false; pframe->SendMessage(WM_RELOAD,nIndex);}
	else
	{AfxMessageBox("This Job has not been Taught");}
	
	pframe->SetTimer(31,1000,NULL);	//Update PLC values	
	
	pframe->SetTimer(30,2000,NULL);	//send new job
	
	if(theapp->camsPresent)
	{
		pframe->m_pcamera->SetLightLevel(
			theapp->jobinfo[pframe->CurrentJobNum].lightLevel1,
			theapp->jobinfo[pframe->CurrentJobNum].lightLevel2, 
			theapp->jobinfo[pframe->CurrentJobNum].lightLevel3, 
			theapp->jobinfo[pframe->CurrentJobNum].lightLevel4,
			theapp->jobinfo[pframe->CurrentJobNum].lightLevel5);
	}	

	if(DoOver)	{pframe->CurrentJobNum=JobOver;}

	pframe->m_pxaxis->InvalidateRect(NULL,false);
	theapp->DidTriggerOnce=false;
	pframe->JobLoaded=true;
	theapp->jobinfo[nIndex].taught=true;

	theapp->teachLabel=true;
	pframe->m_pxaxis->labelTaughtDone=false;
	pframe->ResetTotals();

	pframe->m_pmod->UpdateDisplay();
	pframe->m_plimit->UpdateDisplay();
	pframe->m_pview->UpdateDisplay();
	pframe->m_pview->UpdateMinor();
	pframe->m_pview2->UpdateDisplay();
	pframe->m_pWaistInspection->UpdateDisplay();
	pframe->m_pWaistInspection2->UpdateDisplay();
	pframe->m_pview->SetTimer(2,500,NULL);
	pframe->m_pcap->UpdateDisplay();

	//AfxMessageBox("Please Run Samples Before Putting System Online!");

	CDialog::OnOK();
}



void Job::OnDatabase() 
{
	OnCancel();
}

void Job::DoEnables()
{
	/*
	CString temp;
	char buf[10];

	pframe->ProfileCode=itoa(theapp->jobinfo[pframe->CurrentJobNum].profile,buf,10);

	theapp->SavedEnable1=atoi(pframe->ProfileCode.Right(1));

	temp=pframe->ProfileCode.Right(2);
	theapp->SavedEnable2=atoi(temp.Left(1));

	temp=pframe->ProfileCode.Right(3);
	//theapp->SavedEnable3=atoi(temp.Left(1));

	temp=pframe->ProfileCode.Right(4);
	theapp->SavedEnable4=atoi(temp.Left(1));

	temp=pframe->ProfileCode.Right(5);
	theapp->SavedEnable5=atoi(temp.Left(1));

	temp=pframe->ProfileCode.Right(6);
	theapp->SavedEnable6=atoi(temp.Left(1));

	temp=pframe->ProfileCode.Right(7);
	theapp->SavedEnable7=atoi(temp.Left(1));

	temp=pframe->ProfileCode.Right(8);
	//theapp->SavedEnable8=atoi(temp.Left(1));

	temp=pframe->ProfileCode.Right(9);
	theapp->SavedEnable9=atoi(temp.Left(1));

		pframe->m_pxaxis->LHMin=theapp->inspctn[1].min;
		pframe->m_pxaxis->LHMax=theapp->inspctn[1].max;
		pframe->m_pxaxis->RHMin=theapp->inspctn[2].min;
		pframe->m_pxaxis->RHMax=theapp->inspctn[2].max;
		pframe->m_pxaxis->LFMin=theapp->inspctn[4].min;
		pframe->m_pxaxis->LFMax=theapp->inspctn[4].max;
		pframe->m_pxaxis->DMin=theapp->inspctn[3].min;
		pframe->m_pxaxis->DMax=theapp->inspctn[3].max;
		pframe->m_pxaxis->LRMin=theapp->inspctn[5].min;
		pframe->m_pxaxis->LRMax=theapp->inspctn[5].max;
		pframe->m_pxaxis->PLMin=theapp->inspctn[6].min;
		pframe->m_pxaxis->PLMax=theapp->inspctn[6].max;
		pframe->m_pxaxis->FillMin=theapp->inspctn[8].min;
		pframe->m_pxaxis->FillMax=theapp->inspctn[8].max;
		pframe->m_pxaxis->PTMin=theapp->inspctn[7].min;
		pframe->m_pxaxis->PTMax=theapp->inspctn[7].max;
		pframe->m_pxaxis->TopMin=theapp->inspctn[9].min;
		pframe->m_pxaxis->TopMax=theapp->inspctn[9].max;
/*/
}

void Job::OnChangename() 
{
	
	if(!DoOver) {nIndex=m_joblist.GetCaretIndex();}

	m_rename.GetWindowTextA(jname);
//	m_joblist.DeleteString(nIndex);
//	m_joblist.InsertString(nIndex,jname);
	m_renamejob.Format("%s","");
	Value = "";

	pframe->m_pview->m_currentjob=pframe->m_pview->m_status=theapp->jobinfo[nIndex].jobname=jname;
	//save job name to registry

	char folderName[255];

	sprintf(folderName, "JobArray\\Job%03d", nIndex);
	theapp->WriteProfileString(folderName, "jobname", theapp->jobinfo[nIndex].jobname);

	
	LoadJobs();
	m_joblist.SetCaretIndex(nIndex);
	NameChanged=true;
	UpdateData(false); 
}

void Job::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{
		CString str2;

		KillTimer(1);
		nIndex=JobOver;
		DoOver=true;
 		int n = m_joblist.GetTextLen(original);

		m_joblist.GetText( original, str.GetBuffer(n) );
		str.ReleaseBuffer();	
		str2.Format("%s",str.GetBuffer(0));
		str2.TrimRight(); str2 = str2+"Cpy";

		//load the original first
		pframe->GetJob(original);

		m_joblist.DeleteString(nIndex);
		m_joblist.InsertString(nIndex,str2);
		m_renamejob.Format("%s",str2);
		NameChanged=true;

		pframe->m_pview->m_currentjob=CString(str2);
		pframe->m_pview->m_status=CString(str2);
		//theapp->jobinfo[pframe->CurrentJobNum].jobname=CString(str2);

		char folderName[255];

		sprintf(folderName, "JobArray\\Job%03d", nIndex);

		theapp->jobinfo[nIndex].jobname=CString(str2);
		theapp->WriteProfileString(folderName, "jobname", theapp->jobinfo[nIndex].jobname);

		m_joblist.SetCurSel(nIndex);
		CopyFile(original, nIndex);

		UpdateData(false);
	}	
	
	CDialog::OnTimer(nIDEvent);
}


void Job::CopyFile(int Old, int New)
{
	char buffer[20];
	CString a=" copy ";
	CString Space=" ";
	std::vector<CString> OldFiles,NewFiles;
	
	CString currentpath = "c:\\silgandata\\pat\\";
	CString OldJobNum	=	itoa(Old,buffer,10);
	CString NewJobNum   =   itoa(New,buffer,10);
	CString strCmd;

	//bmp files
	OldFiles.push_back(OldJobNum+".bmp");
	OldFiles.push_back("patBy6L.bmp");
	OldFiles.push_back("patBy3L.bmp");
	OldFiles.push_back("patBy6M.bmp");
	OldFiles.push_back("patBy3M.bmp");
	OldFiles.push_back("patBy6R.bmp");
	OldFiles.push_back("patBy3R.bmp");
	OldFiles.push_back("TaughtPicR3.bmp");
	OldFiles.push_back("M6patBy6L.bmp");
	OldFiles.push_back("M6patBy3L.bmp");
	OldFiles.push_back("M6patBy6M.bmp");
	OldFiles.push_back("M6patBy3M.bmp");
	OldFiles.push_back("M6patBy6R.bmp");
	OldFiles.push_back("M6patBy3R.bmp");
	OldFiles.push_back("M6patBy6L_num3.bmp");
	OldFiles.push_back("M6patBy3L_num3.bmp");
	OldFiles.push_back("M6patBy6M_num3.bmp");
	OldFiles.push_back("M6patBy3M_num3.bmp");
	OldFiles.push_back("M6patBy6R_num3.bmp");
	OldFiles.push_back("M6patBy3R_num3.bmp");
	OldFiles.push_back("capimagel.bmp");
	OldFiles.push_back("capimager.bmp");
	
	//txt files
	OldFiles.push_back("imgBy6L.txt");
	OldFiles.push_back("imgBy3L.txt");
	OldFiles.push_back("imgBy6M.txt");
	OldFiles.push_back("imgBy3M.txt");
	OldFiles.push_back("imgBy6R.txt");
	OldFiles.push_back("imgBy3R.txt");
	OldFiles.push_back("M6imgBy6L.txt");
	OldFiles.push_back("M6imgBy3L.txt");
	OldFiles.push_back("M6imgBy6M.txt");
	OldFiles.push_back("M6imgBy3M.txt");
	OldFiles.push_back("M6imgBy6R.txt");
	OldFiles.push_back("M6imgBy3R.txt");
	OldFiles.push_back("M6imgBy6L_num3.txt");
	OldFiles.push_back("M6imgBy3L_num3.txt");
	OldFiles.push_back("M6imgBy6M_num3.txt");
	OldFiles.push_back("M6imgBy3M_num3.txt");
	OldFiles.push_back("M6imgBy6R_num3.txt");
	OldFiles.push_back("M6imgBy3R_num3.txt");
	OldFiles.push_back("image.txt");
	OldFiles.push_back("image3.txt");
	OldFiles.push_back("capimagel.txt");
	OldFiles.push_back("capimager.txt");

	NewFiles = OldFiles; //new filename and old filename will be identical for all cases except the first bmp

	NewFiles[0] = NewJobNum+".bmp";


	for(int index = 0; index < OldFiles.size(); index++)
	{
		strCmd = "copy "+currentpath+OldJobNum+"\\"+OldFiles[index]+" "+currentpath+NewJobNum+"\\"+NewFiles[index] + " /Y";

		system(strCmd);
	}

	/*
	CString OldFile4	=	OldJobNum+"\\"+OldJobNum+".bmp";
	CString OldFile7	=	OldJobNum+"\\"+"image.txt";
	CString OldFile5	=	OldJobNum+"\\"+"capimagel.txt";
	CString OldFile6	=	OldJobNum+"\\"+"capimager.txt";
	CString OldFile8	=	OldJobNum+"\\"+"image3.txt";
	CString OldFile9	=	OldJobNum+"\\"+"imgBy6L.txt";
	CString OldFile10	=	OldJobNum+"\\"+"imgBy3L.txt";

	CString NewJobNum	=	itoa(New,buffer,10);
	CString NewFile4	=	NewJobNum+"\\"+NewJobNum+".bmp";
	CString NewFile7	=	NewJobNum+"\\"+"image.txt";
	CString NewFile5	=	NewJobNum+"\\"+"capimagel.txt";
	CString NewFile6	=	NewJobNum+"\\"+"capimager.txt";
	CString NewFile8	=	NewJobNum+"\\"+"image3.txt";
	CString NewFile9	=	NewJobNum+"\\"+"imgBy6L.txt";
	CString NewFile10	=	NewJobNum+"\\"+"imgBy3L.txt";

	CString e=a+currentpath+OldFile4 +Space+currentpath+NewFile4;
	system(e);

	CString f=a+currentpath+OldFile5 +Space+currentpath+NewFile5;
	system(f);

	CString g=a+currentpath+OldFile6 +Space+currentpath+NewFile6;
	system(g);

	CString h=a+currentpath+OldFile7 +Space+currentpath+NewFile7;
	system(h);

	CString i=a+currentpath+OldFile8 +Space+currentpath+NewFile8;
	system(i);

	CString j=a+currentpath+OldFile9 +Space+currentpath+NewFile9;
	system(j);

	CString k=a+currentpath+OldFile10 +Space+currentpath+NewFile10;
	system(k);
*/
	/////////////////////////////////
	theapp->jobinfo[New].useRing					=theapp->jobinfo[Old].useRing;

	theapp->jobinfo[New].totinsp					=theapp->jobinfo[Old].totinsp;
	theapp->jobinfo[New].taught						=theapp->jobinfo[Old].taught;

	theapp->jobinfo[New].tgYpattern					=theapp->jobinfo[Old].tgYpattern;

	theapp->jobinfo[New].labelIntegrityUseMethod1	=theapp->jobinfo[Old].labelIntegrityUseMethod1;
	theapp->jobinfo[New].labelIntegrityUseMethod2	=theapp->jobinfo[Old].labelIntegrityUseMethod2;
	theapp->jobinfo[New].labelIntegrityUseMethod3	=theapp->jobinfo[Old].labelIntegrityUseMethod3;
	theapp->jobinfo[New].labelIntegrityUseMethod4	=theapp->jobinfo[Old].labelIntegrityUseMethod4;
	theapp->jobinfo[New].labelIntegrityUseMethod5	=theapp->jobinfo[Old].labelIntegrityUseMethod5;
	theapp->jobinfo[New].labelIntegrityUseMethod6	=theapp->jobinfo[Old].labelIntegrityUseMethod6;
	
	theapp->jobinfo[New].avoidOffX					=theapp->jobinfo[Old].avoidOffX;

	theapp->jobinfo[New].showStLigh					=theapp->jobinfo[Old].showStLigh;
	theapp->jobinfo[New].showStDark					=theapp->jobinfo[Old].showStDark;
	theapp->jobinfo[New].showStEdges				=theapp->jobinfo[Old].showStEdges;
	theapp->jobinfo[New].showStFinal				=theapp->jobinfo[Old].showStFinal;
	theapp->jobinfo[New].showStbw					=theapp->jobinfo[Old].showStbw;

	theapp->jobinfo[New].seamW						=theapp->jobinfo[Old].seamW;
	theapp->jobinfo[New].seamH						=theapp->jobinfo[Old].seamH;
	theapp->jobinfo[New].seamOffX					=theapp->jobinfo[Old].seamOffX;
	theapp->jobinfo[New].seamOffY					=theapp->jobinfo[Old].seamOffY;
	theapp->jobinfo[New].stitchY[1]					=theapp->jobinfo[Old].stitchY[1];
	theapp->jobinfo[New].stitchY[2]					=theapp->jobinfo[Old].stitchY[2];
	theapp->jobinfo[New].stitchY[3]					=theapp->jobinfo[Old].stitchY[3];
	theapp->jobinfo[New].stitchY[4]					=theapp->jobinfo[Old].stitchY[4];
	theapp->jobinfo[New].stitchW					=theapp->jobinfo[Old].stitchW;
	theapp->jobinfo[New].stitchH					=theapp->jobinfo[Old].stitchH;
	theapp->jobinfo[New].senEdges					=theapp->jobinfo[Old].senEdges;
	theapp->jobinfo[New].senMidd					=theapp->jobinfo[Old].senMidd;
	theapp->jobinfo[New].MiddImg					=theapp->jobinfo[Old].MiddImg;
	
	theapp->jobinfo[New].stDarkSen					=theapp->jobinfo[Old].stDarkSen;
	theapp->jobinfo[New].stLighSen					=theapp->jobinfo[Old].stLighSen;
	theapp->jobinfo[New].stEdgeSen					=theapp->jobinfo[Old].stEdgeSen;

	theapp->jobinfo[New].stDarkImg					=theapp->jobinfo[Old].stDarkImg;
	theapp->jobinfo[New].stLighImg					=theapp->jobinfo[Old].stLighImg;
	theapp->jobinfo[New].stEdgeImg					=theapp->jobinfo[Old].stEdgeImg;
	
	theapp->jobinfo[New].filterSelNolabel			=theapp->jobinfo[Old].filterSelNolabel;
	theapp->jobinfo[New].filterSelSplice			=theapp->jobinfo[Old].filterSelSplice;
	theapp->jobinfo[New].EdgeTransition				=theapp->jobinfo[Old].EdgeTransition;

	theapp->jobinfo[New].SpliceLookFor				=theapp->jobinfo[Old].SpliceLookFor;



	theapp->jobinfo[New].method5_Rt					=theapp->jobinfo[Old].method5_Rt;
	theapp->jobinfo[New].method5_Gt					=theapp->jobinfo[Old].method5_Gt;
	theapp->jobinfo[New].method5_Bt					=theapp->jobinfo[Old].method5_Bt;
	theapp->jobinfo[New].method5_Ht					=theapp->jobinfo[Old].method5_Ht;

	theapp->jobinfo[New].method5_Rb					=theapp->jobinfo[Old].method5_Rb;
	theapp->jobinfo[New].method5_Gb					=theapp->jobinfo[Old].method5_Gb;
	theapp->jobinfo[New].method5_Bb					=theapp->jobinfo[Old].method5_Bb;
	theapp->jobinfo[New].method5_Hb					=theapp->jobinfo[Old].method5_Hb;


	theapp->jobinfo[New].filterSelPatt				=theapp->jobinfo[Old].filterSelPatt;
	theapp->jobinfo[New].filterSelPattCap			=theapp->jobinfo[Old].filterSelPattCap;


	theapp->jobinfo[New].filterSelShifted			=theapp->jobinfo[Old].filterSelShifted;
	theapp->jobinfo[New].lightLevel1				=theapp->jobinfo[Old].lightLevel1;
	theapp->jobinfo[New].lightLevel2				=theapp->jobinfo[Old].lightLevel2;
	theapp->jobinfo[New].lightLevel3				=theapp->jobinfo[Old].lightLevel3;
	theapp->jobinfo[New].lightLevel4				=theapp->jobinfo[Old].lightLevel4;
	theapp->jobinfo[New].lightLevel5				=theapp->jobinfo[Old].lightLevel5;
	theapp->jobinfo[New].vertBarX					=theapp->jobinfo[Old].vertBarX;
	theapp->jobinfo[New].midBarY					=theapp->jobinfo[Old].midBarY;
	theapp->jobinfo[New].neckBarY					=theapp->jobinfo[Old].neckBarY;
	theapp->jobinfo[New].lDist						=theapp->jobinfo[Old].lDist;
	theapp->jobinfo[New].labelThresh				=theapp->jobinfo[Old].labelThresh;
	theapp->jobinfo[New].profile					=theapp->jobinfo[Old].profile;
	theapp->jobinfo[New].motPos						=theapp->jobinfo[Old].motPos;
	theapp->jobinfo[New].decCutEnable				=theapp->jobinfo[Old].decCutEnable;
	theapp->jobinfo[New].intThresh					=theapp->jobinfo[Old].intThresh;
	theapp->jobinfo[New].notchSen					=theapp->jobinfo[Old].notchSen;
	theapp->jobinfo[New].labelSen					=theapp->jobinfo[Old].labelSen;
	theapp->jobinfo[New].bottleStyle				=theapp->jobinfo[Old].bottleStyle;
	theapp->jobinfo[New].type						=theapp->jobinfo[Old].type;
	theapp->jobinfo[New].colorOffset2				=theapp->jobinfo[Old].colorOffset2;
	theapp->jobinfo[New].lizardFilter				=theapp->jobinfo[Old].lizardFilter;
	theapp->jobinfo[New].colorTargR					=theapp->jobinfo[Old].colorTargR;
	theapp->jobinfo[New].colorTargG					=theapp->jobinfo[Old].colorTargG;
	theapp->jobinfo[New].colorTargB					=theapp->jobinfo[Old].colorTargB;
	theapp->jobinfo[New].colorTargR2				=theapp->jobinfo[Old].colorTargR2;
	theapp->jobinfo[New].colorTargG2				=theapp->jobinfo[Old].colorTargG2;
	theapp->jobinfo[New].colorTargB2				=theapp->jobinfo[Old].colorTargB2;
	theapp->jobinfo[New].colorFilter				=theapp->jobinfo[Old].colorFilter;
	theapp->jobinfo[New].colorFilter2				=theapp->jobinfo[Old].colorFilter2;
	theapp->jobinfo[New].sensMet3					=theapp->jobinfo[Old].sensMet3;
	theapp->jobinfo[New].prodName					=theapp->jobinfo[Old].prodName;
	theapp->jobinfo[New].noLabelSen					=theapp->jobinfo[Old].noLabelSen;
	theapp->jobinfo[New].noLabelOffset				=theapp->jobinfo[Old].noLabelOffset;


	theapp->jobinfo[New].heigMet3					=theapp->jobinfo[Old].heigMet3;
	theapp->jobinfo[New].widtMet3					=theapp->jobinfo[Old].widtMet3;

	theapp->jobinfo[New].heigMet5					=theapp->jobinfo[Old].heigMet5;
	theapp->jobinfo[New].widtMet5					=theapp->jobinfo[Old].widtMet5;

	theapp->jobinfo[New].noLabelOffsetX[1]			=theapp->jobinfo[Old].noLabelOffsetX[1];
	theapp->jobinfo[New].noLabelOffsetX[2]			=theapp->jobinfo[Old].noLabelOffsetX[2];
	theapp->jobinfo[New].noLabelOffsetX[3]			=theapp->jobinfo[Old].noLabelOffsetX[3];
	theapp->jobinfo[New].noLabelOffsetX[4]			=theapp->jobinfo[Old].noLabelOffsetX[4];
	theapp->jobinfo[New].noLabelOffsetW				=theapp->jobinfo[Old].noLabelOffsetW;
	theapp->jobinfo[New].noLabelOffsetH				=theapp->jobinfo[Old].noLabelOffsetH;

	theapp->jobinfo[New].shiftedLabSen				=theapp->jobinfo[Old].shiftedLabSen;
	theapp->jobinfo[New].EdgeLabSen					=theapp->jobinfo[Old].EdgeLabSen;
	
	theapp->jobinfo[New].rotatePos					=theapp->jobinfo[Old].rotatePos;
	theapp->jobinfo[New].insideOut					=theapp->jobinfo[Old].insideOut;


	theapp->jobinfo[New].neckStrength				=theapp->jobinfo[Old].neckStrength;

	theapp->jobinfo[New].capStrength				=theapp->jobinfo[Old].capStrength;
	theapp->jobinfo[New].capStrength2				=theapp->jobinfo[Old].capStrength2;
	theapp->jobinfo[New].mainBodySen				=theapp->jobinfo[Old].mainBodySen;
	theapp->jobinfo[New].orienArea					=theapp->jobinfo[Old].orienArea;
	theapp->jobinfo[New].neckSen					=theapp->jobinfo[Old].neckSen;
	theapp->jobinfo[New].neckPos					=theapp->jobinfo[Old].neckPos;
	theapp->jobinfo[New].underLipEnable				=theapp->jobinfo[Old].underLipEnable;
	theapp->jobinfo[New].neckInOut					=theapp->jobinfo[Old].neckInOut;
	theapp->jobinfo[New].neckInOut2					=theapp->jobinfo[Old].neckInOut2;
	theapp->jobinfo[New].greenFilter				=theapp->jobinfo[Old].greenFilter;
	theapp->jobinfo[New].redFilter					=theapp->jobinfo[Old].redFilter;
	theapp->jobinfo[New].blueFilter					=theapp->jobinfo[Old].blueFilter;
	theapp->jobinfo[New].findWhite					=theapp->jobinfo[Old].findWhite;
	theapp->jobinfo[New].fillOffset					=theapp->jobinfo[Old].fillOffset;
	theapp->jobinfo[New].fillSize					=theapp->jobinfo[Old].fillSize;
	theapp->jobinfo[New].capColorSize				=theapp->jobinfo[Old].capColorSize;
	theapp->jobinfo[New].capColorOffset				=theapp->jobinfo[Old].capColorOffset;
	theapp->jobinfo[New].labelColorRectX			=theapp->jobinfo[Old].labelColorRectX;
	theapp->jobinfo[New].labelColorRectY			=theapp->jobinfo[Old].labelColorRectY;
	theapp->jobinfo[New].ellipseSen					=theapp->jobinfo[Old].ellipseSen;
	theapp->jobinfo[New].rotOffset					=theapp->jobinfo[Old].rotOffset;

	theapp->jobinfo[New].sportPos					=theapp->jobinfo[Old].sportPos;
	theapp->jobinfo[New].sportSen					=theapp->jobinfo[Old].sportSen;
	theapp->jobinfo[New].sport						=theapp->jobinfo[Old].sport;
	theapp->jobinfo[New].taughtMiddle				=theapp->jobinfo[Old].taughtMiddle;
	theapp->jobinfo[New].RefForMiddleIsLeft[1]		=theapp->jobinfo[Old].RefForMiddleIsLeft[1];
	theapp->jobinfo[New].RefForMiddleIsLeft[2]		=theapp->jobinfo[Old].RefForMiddleIsLeft[2];
	theapp->jobinfo[New].RefForMiddleIsLeft[3]		=theapp->jobinfo[Old].RefForMiddleIsLeft[3];
	theapp->jobinfo[New].RefForMiddleIsLeft[4]		=theapp->jobinfo[Old].RefForMiddleIsLeft[4];
	theapp->jobinfo[New].taughtMiddleRefW[1]		=theapp->jobinfo[Old].taughtMiddleRefW[1];
	theapp->jobinfo[New].taughtMiddleRefW[2]		=theapp->jobinfo[Old].taughtMiddleRefW[2];
	theapp->jobinfo[New].taughtMiddleRefW[3]		=theapp->jobinfo[Old].taughtMiddleRefW[3];
	theapp->jobinfo[New].taughtMiddleRefW[4]		=theapp->jobinfo[Old].taughtMiddleRefW[4];
	theapp->jobinfo[New].taughtMiddleRefY[1]		=theapp->jobinfo[Old].taughtMiddleRefY[1];
	theapp->jobinfo[New].taughtMiddleRefY[2]		=theapp->jobinfo[Old].taughtMiddleRefY[2];
	theapp->jobinfo[New].taughtMiddleRefY[3]		=theapp->jobinfo[Old].taughtMiddleRefY[3];
	theapp->jobinfo[New].taughtMiddleRefY[4]		=theapp->jobinfo[Old].taughtMiddleRefY[4];
	theapp->jobinfo[New].middlesM[1]				=theapp->jobinfo[Old].middlesM[1];
	theapp->jobinfo[New].middlesM[2]				=theapp->jobinfo[Old].middlesM[2];
	theapp->jobinfo[New].middlesM[3]				=theapp->jobinfo[Old].middlesM[3];
	theapp->jobinfo[New].middlesM[4]				=theapp->jobinfo[Old].middlesM[4];
	theapp->jobinfo[New].middlesBarX[1]				=theapp->jobinfo[Old].middlesBarX[1];
	theapp->jobinfo[New].middlesBarX[2]				=theapp->jobinfo[Old].middlesBarX[2];
	theapp->jobinfo[New].middlesBarX[3]				=theapp->jobinfo[Old].middlesBarX[3];
	theapp->jobinfo[New].middlesBarX[4]				=theapp->jobinfo[Old].middlesBarX[4];
	theapp->jobinfo[New].middlesBarY[1]				=theapp->jobinfo[Old].middlesBarY[1];
	theapp->jobinfo[New].middlesBarY[2]				=theapp->jobinfo[Old].middlesBarY[2];
	theapp->jobinfo[New].middlesBarY[3]				=theapp->jobinfo[Old].middlesBarY[3];
	theapp->jobinfo[New].middlesBarY[4]				=theapp->jobinfo[Old].middlesBarY[4];
	theapp->jobinfo[New].tgXpatt[1]					=theapp->jobinfo[Old].tgXpatt[1];
	theapp->jobinfo[New].tgYpatt[1]					=theapp->jobinfo[Old].tgYpatt[1];
	theapp->jobinfo[New].tgXpatt[2]					=theapp->jobinfo[Old].tgXpatt[2];
	theapp->jobinfo[New].tgYpatt[2]					=theapp->jobinfo[Old].tgYpatt[2];
	theapp->jobinfo[New].tgXpatt[3]					=theapp->jobinfo[Old].tgXpatt[3];
	theapp->jobinfo[New].tgYpatt[3]					=theapp->jobinfo[Old].tgYpatt[3];
	theapp->jobinfo[New].showMiddles				=theapp->jobinfo[Old].showMiddles;
	theapp->jobinfo[New].findMiddles				=theapp->jobinfo[Old].findMiddles;
	theapp->jobinfo[New].showEdges					=theapp->jobinfo[Old].showEdges;

	theapp->jobinfo[New].ytopLimPatt				=theapp->jobinfo[Old].ytopLimPatt;
	theapp->jobinfo[New].ybotLimPatt				=theapp->jobinfo[Old].ybotLimPatt;
	theapp->jobinfo[New].shiftTopLim[1]				=theapp->jobinfo[Old].shiftTopLim[1];
	theapp->jobinfo[New].shiftBotLim[1]				=theapp->jobinfo[Old].shiftBotLim[1];
	theapp->jobinfo[New].shiftTopLim[2]				=theapp->jobinfo[Old].shiftTopLim[2];
	theapp->jobinfo[New].shiftBotLim[2]				=theapp->jobinfo[Old].shiftBotLim[2];
	theapp->jobinfo[New].shiftTopLim[3]				=theapp->jobinfo[Old].shiftTopLim[3];
	theapp->jobinfo[New].shiftBotLim[3]				=theapp->jobinfo[Old].shiftBotLim[3];
	theapp->jobinfo[New].shiftTopLim[4]				=theapp->jobinfo[Old].shiftTopLim[4];
	theapp->jobinfo[New].shiftBotLim[4]				=theapp->jobinfo[Old].shiftBotLim[4];

	theapp->jobinfo[New].shiftHorIniLim				=theapp->jobinfo[Old].shiftHorIniLim;
	theapp->jobinfo[New].shiftHorEndLim				=theapp->jobinfo[Old].shiftHorEndLim;

	theapp->jobinfo[New].shiftWidth					=theapp->jobinfo[Old].shiftWidth;
	

	theapp->jobinfo[New].totinsp					=theapp->jobinfo[Old].totinsp;

	theapp->jobinfo[New].showC5inPictures=theapp->jobinfo[Old].showC5inPictures;
	theapp->jobinfo[New].showSetupI5=theapp->jobinfo[Old].showSetupI5;
	theapp->jobinfo[New].showSetupI6=theapp->jobinfo[Old].showSetupI6;
	theapp->jobinfo[New].showSetupI7=theapp->jobinfo[Old].showSetupI7;
	theapp->jobinfo[New].showSetupI8=theapp->jobinfo[Old].showSetupI8;
	theapp->jobinfo[New].showSetupI9=theapp->jobinfo[Old].showSetupI9;
	theapp->jobinfo[New].showSetupI10=theapp->jobinfo[Old].showSetupI10;
	theapp->jobinfo[New].showLabelMating=theapp->jobinfo[Old].showLabelMating;;

	theapp->jobinfo[New].ySplice[1]=theapp->jobinfo[Old].ySplice[1];
	theapp->jobinfo[New].ySplice[2]=theapp->jobinfo[Old].ySplice[2];
	theapp->jobinfo[New].ySplice[3]=theapp->jobinfo[Old].ySplice[3];
	theapp->jobinfo[New].ySplice[4]=theapp->jobinfo[Old].ySplice[4];
	theapp->jobinfo[New].wSplice=theapp->jobinfo[Old].wSplice;
	theapp->jobinfo[New].hSplice=theapp->jobinfo[Old].hSplice;
	theapp->jobinfo[New].hSpliceSmBx=theapp->jobinfo[Old].hSpliceSmBx;

	theapp->jobinfo[New].spliceVar=theapp->jobinfo[Old].spliceVar;

	///start waist inspection variable copy
	theapp->jobinfo[New].cam1WaistAvgWindowSize=theapp->jobinfo[Old].cam1WaistAvgWindowSize;
	theapp->jobinfo[New].cam1WaistEdgeCoordinateDeviation=theapp->jobinfo[Old].cam1WaistEdgeCoordinateDeviation;
	theapp->jobinfo[New].cam1WaistEdgeThreshold=theapp->jobinfo[Old].cam1WaistEdgeThreshold;
	theapp->jobinfo[New].cam1WaistfillTopToBottom=theapp->jobinfo[Old].cam1WaistfillTopToBottom;
	theapp->jobinfo[New].cam1WaistHorizontalSearchLength=theapp->jobinfo[Old].cam1WaistHorizontalSearchLength;
	theapp->jobinfo[New].cam1WaistMeasurementBottomBound=theapp->jobinfo[Old].cam1WaistMeasurementBottomBound;
	theapp->jobinfo[New].cam1WaistMeasurementLeftBound=theapp->jobinfo[Old].cam1WaistMeasurementLeftBound;
	theapp->jobinfo[New].cam1WaistMeasurementRightBound=theapp->jobinfo[Old].cam1WaistMeasurementRightBound;
	theapp->jobinfo[New].cam1WaistMeasurementTopBound=theapp->jobinfo[Old].cam1WaistMeasurementTopBound;
	theapp->jobinfo[New].cam1WaistMeasurementUpperBottomBound=theapp->jobinfo[Old].cam1WaistMeasurementUpperBottomBound;
	theapp->jobinfo[New].cam1WaistMeasurementUpperLeftBound=theapp->jobinfo[Old].cam1WaistMeasurementUpperLeftBound;
	theapp->jobinfo[New].cam1WaistMeasurementUpperRightBound=theapp->jobinfo[Old].cam1WaistMeasurementUpperRightBound;
	theapp->jobinfo[New].cam1WaistMeasurementUpperTopBound=theapp->jobinfo[Old].cam1WaistMeasurementUpperTopBound;
	theapp->jobinfo[New].cam1WaistVerticalSearchLength=theapp->jobinfo[Old].cam1WaistVerticalSearchLength;
	theapp->jobinfo[New].cam1WaistWidthOverlayEndX=theapp->jobinfo[Old].cam1WaistWidthOverlayEndX;
	theapp->jobinfo[New].cam1WaistWidthOverlayStartX=theapp->jobinfo[Old].cam1WaistWidthOverlayStartX;
	theapp->jobinfo[New].cam1WaistWidthOverlayY=theapp->jobinfo[Old].cam1WaistWidthOverlayY;

	theapp->jobinfo[New].cam2WaistAvgWindowSize=theapp->jobinfo[Old].cam2WaistAvgWindowSize;
	theapp->jobinfo[New].cam2WaistEdgeCoordinateDeviation=theapp->jobinfo[Old].cam2WaistEdgeCoordinateDeviation;
	theapp->jobinfo[New].cam2WaistEdgeThreshold=theapp->jobinfo[Old].cam2WaistEdgeThreshold;
	theapp->jobinfo[New].cam2WaistfillTopToBottom=theapp->jobinfo[Old].cam2WaistfillTopToBottom;
	theapp->jobinfo[New].cam2WaistHorizontalSearchLength=theapp->jobinfo[Old].cam2WaistHorizontalSearchLength;
	theapp->jobinfo[New].cam2WaistMeasurementBottomBound=theapp->jobinfo[Old].cam2WaistMeasurementBottomBound;
	theapp->jobinfo[New].cam2WaistMeasurementLeftBound=theapp->jobinfo[Old].cam2WaistMeasurementLeftBound;
	theapp->jobinfo[New].cam2WaistMeasurementRightBound=theapp->jobinfo[Old].cam2WaistMeasurementRightBound;
	theapp->jobinfo[New].cam2WaistMeasurementTopBound=theapp->jobinfo[Old].cam2WaistMeasurementTopBound;
	theapp->jobinfo[New].cam2WaistMeasurementUpperBottomBound=theapp->jobinfo[Old].cam2WaistMeasurementUpperBottomBound;
	theapp->jobinfo[New].cam2WaistMeasurementUpperLeftBound=theapp->jobinfo[Old].cam2WaistMeasurementUpperLeftBound;
	theapp->jobinfo[New].cam2WaistMeasurementUpperRightBound=theapp->jobinfo[Old].cam2WaistMeasurementUpperRightBound;
	theapp->jobinfo[New].cam2WaistMeasurementUpperTopBound=theapp->jobinfo[Old].cam2WaistMeasurementUpperTopBound;
	theapp->jobinfo[New].cam2WaistVerticalSearchLength=theapp->jobinfo[Old].cam2WaistVerticalSearchLength;
	theapp->jobinfo[New].cam2WaistWidthOverlayEndX=theapp->jobinfo[Old].cam2WaistWidthOverlayEndX;
	theapp->jobinfo[New].cam2WaistWidthOverlayStartX=theapp->jobinfo[Old].cam2WaistWidthOverlayStartX;
	theapp->jobinfo[New].cam2WaistWidthOverlayY=theapp->jobinfo[Old].cam2WaistWidthOverlayY;
	
	theapp->jobinfo[New].cam3WaistAvgWindowSize=theapp->jobinfo[Old].cam3WaistAvgWindowSize;
	theapp->jobinfo[New].cam3WaistEdgeCoordinateDeviation=theapp->jobinfo[Old].cam3WaistEdgeCoordinateDeviation;
	theapp->jobinfo[New].cam3WaistEdgeThreshold=theapp->jobinfo[Old].cam3WaistEdgeThreshold;
	theapp->jobinfo[New].cam3WaistfillTopToBottom=theapp->jobinfo[Old].cam3WaistfillTopToBottom;
	theapp->jobinfo[New].cam3WaistHorizontalSearchLength=theapp->jobinfo[Old].cam3WaistHorizontalSearchLength;
	theapp->jobinfo[New].cam3WaistMeasurementBottomBound=theapp->jobinfo[Old].cam3WaistMeasurementBottomBound;
	theapp->jobinfo[New].cam3WaistMeasurementLeftBound=theapp->jobinfo[Old].cam3WaistMeasurementLeftBound;
	theapp->jobinfo[New].cam3WaistMeasurementRightBound=theapp->jobinfo[Old].cam3WaistMeasurementRightBound;
	theapp->jobinfo[New].cam3WaistMeasurementTopBound=theapp->jobinfo[Old].cam3WaistMeasurementTopBound;
	theapp->jobinfo[New].cam3WaistMeasurementUpperBottomBound=theapp->jobinfo[Old].cam3WaistMeasurementUpperBottomBound;
	theapp->jobinfo[New].cam3WaistMeasurementUpperLeftBound=theapp->jobinfo[Old].cam3WaistMeasurementUpperLeftBound;
	theapp->jobinfo[New].cam3WaistMeasurementUpperRightBound=theapp->jobinfo[Old].cam3WaistMeasurementUpperRightBound;
	theapp->jobinfo[New].cam3WaistMeasurementUpperTopBound=theapp->jobinfo[Old].cam3WaistMeasurementUpperTopBound;
	theapp->jobinfo[New].cam3WaistVerticalSearchLength=theapp->jobinfo[Old].cam3WaistVerticalSearchLength;
	theapp->jobinfo[New].cam3WaistWidthOverlayEndX=theapp->jobinfo[Old].cam3WaistWidthOverlayEndX;
	theapp->jobinfo[New].cam3WaistWidthOverlayStartX=theapp->jobinfo[Old].cam3WaistWidthOverlayStartX;
	theapp->jobinfo[New].cam3WaistWidthOverlayY=theapp->jobinfo[Old].cam3WaistWidthOverlayY;
	
	theapp->jobinfo[New].cam4WaistAvgWindowSize=theapp->jobinfo[Old].cam4WaistAvgWindowSize;
	theapp->jobinfo[New].cam4WaistEdgeCoordinateDeviation=theapp->jobinfo[Old].cam4WaistEdgeCoordinateDeviation;
	theapp->jobinfo[New].cam4WaistEdgeThreshold=theapp->jobinfo[Old].cam4WaistEdgeThreshold;
	theapp->jobinfo[New].cam4WaistfillTopToBottom=theapp->jobinfo[Old].cam4WaistfillTopToBottom;
	theapp->jobinfo[New].cam4WaistHorizontalSearchLength=theapp->jobinfo[Old].cam4WaistHorizontalSearchLength;
	theapp->jobinfo[New].cam4WaistMeasurementBottomBound=theapp->jobinfo[Old].cam4WaistMeasurementBottomBound;
	theapp->jobinfo[New].cam4WaistMeasurementLeftBound=theapp->jobinfo[Old].cam4WaistMeasurementLeftBound;
	theapp->jobinfo[New].cam4WaistMeasurementRightBound=theapp->jobinfo[Old].cam4WaistMeasurementRightBound;
	theapp->jobinfo[New].cam4WaistMeasurementTopBound=theapp->jobinfo[Old].cam4WaistMeasurementTopBound;
	theapp->jobinfo[New].cam4WaistMeasurementUpperBottomBound=theapp->jobinfo[Old].cam4WaistMeasurementUpperBottomBound;
	theapp->jobinfo[New].cam4WaistMeasurementUpperLeftBound=theapp->jobinfo[Old].cam4WaistMeasurementUpperLeftBound;
	theapp->jobinfo[New].cam4WaistMeasurementUpperRightBound=theapp->jobinfo[Old].cam4WaistMeasurementUpperRightBound;
	theapp->jobinfo[New].cam4WaistMeasurementUpperTopBound=theapp->jobinfo[Old].cam4WaistMeasurementUpperTopBound;
	theapp->jobinfo[New].cam4WaistVerticalSearchLength=theapp->jobinfo[Old].cam4WaistVerticalSearchLength;
	theapp->jobinfo[New].cam4WaistWidthOverlayEndX=theapp->jobinfo[Old].cam4WaistWidthOverlayEndX;
	theapp->jobinfo[New].cam4WaistWidthOverlayStartX=theapp->jobinfo[Old].cam4WaistWidthOverlayStartX;
	theapp->jobinfo[New].cam4WaistWidthOverlayY=theapp->jobinfo[Old].cam4WaistWidthOverlayY;
	///end waist inspection variable copy

	///start label mating variable copy
	theapp->jobinfo[New].labelMatingBottomBound=theapp->jobinfo[Old].labelMatingBottomBound;
	theapp->jobinfo[New].labelMatingLeftBound=theapp->jobinfo[Old].labelMatingLeftBound;
	theapp->jobinfo[New].labelMatingRightBound=theapp->jobinfo[Old].labelMatingRightBound;
	theapp->jobinfo[New].labelMatingTaughtB=theapp->jobinfo[Old].labelMatingTaughtB;
	theapp->jobinfo[New].labelMatingTaughtBTolerance=theapp->jobinfo[Old].labelMatingTaughtBTolerance;
	theapp->jobinfo[New].labelMatingTaughtG=theapp->jobinfo[Old].labelMatingTaughtG;
	theapp->jobinfo[New].labelMatingTaughtGTolerance=theapp->jobinfo[Old].labelMatingTaughtGTolerance;
	theapp->jobinfo[New].labelMatingTaughtR=theapp->jobinfo[Old].labelMatingTaughtR;
	theapp->jobinfo[New].labelMatingTaughtRTolerance=theapp->jobinfo[Old].labelMatingTaughtRTolerance;
	theapp->jobinfo[New].labelMatingTeachColorBottomBound=theapp->jobinfo[Old].labelMatingTeachColorBottomBound;
	theapp->jobinfo[New].labelMatingTeachColorLeftBound=theapp->jobinfo[Old].labelMatingTeachColorLeftBound;
	theapp->jobinfo[New].labelMatingTeachColorRightBound=theapp->jobinfo[Old].labelMatingTeachColorRightBound;
	theapp->jobinfo[New].labelMatingTeachColorTopBound=theapp->jobinfo[Old].labelMatingTeachColorTopBound;
	theapp->jobinfo[New].labelMatingTopBound=theapp->jobinfo[Old].labelMatingTopBound;
	///end label mating variable copy

	///start method 6 variable copy
	theapp->jobinfo[New].meth6_patt1_x=theapp->jobinfo[Old].meth6_patt1_x;
	theapp->jobinfo[New].meth6_patt1_y=theapp->jobinfo[Old].meth6_patt1_y;
	theapp->jobinfo[New].meth6_patt2_x=theapp->jobinfo[Old].meth6_patt2_x;
	theapp->jobinfo[New].meth6_patt2_y=theapp->jobinfo[Old].meth6_patt2_y;
	theapp->jobinfo[New].meth6_patt3_x=theapp->jobinfo[Old].meth6_patt3_x;
	theapp->jobinfo[New].meth6_patt3_y=theapp->jobinfo[Old].meth6_patt3_y;
	///end method 6 variable copy

	///start method 5 box variable copy
	theapp->jobinfo[New].boxHb=theapp->jobinfo[Old].boxHb;
	theapp->jobinfo[New].boxHt=theapp->jobinfo[Old].boxHt;
	theapp->jobinfo[New].boxWb=theapp->jobinfo[Old].boxWb;
	theapp->jobinfo[New].boxWt=theapp->jobinfo[Old].boxWt;
	theapp->jobinfo[New].boxYb=theapp->jobinfo[Old].boxYb;
	theapp->jobinfo[New].boxYt=theapp->jobinfo[Old].boxYt;
	///end method 5 box variable copy

	///start stitch variable copy
	theapp->jobinfo[New].useStDark=theapp->jobinfo[Old].useStDark;
	theapp->jobinfo[New].useStEdge=theapp->jobinfo[Old].useStEdge;
	theapp->jobinfo[New].useStLigh=theapp->jobinfo[Old].useStLigh;
	///end stitch variable copy
	
	///start remaining job variable copy
	theapp->jobinfo[New].sensEdge=theapp->jobinfo[Old].sensEdge;
	theapp->jobinfo[New].bottleColor=theapp->jobinfo[Old].bottleColor;
	///end remaining job variable copy


	theapp->inspctn[1].name=theapp->inspctn[1].name;
	theapp->inspctn[2].name=theapp->inspctn[2].name;
	theapp->inspctn[3].name=theapp->inspctn[3].name;
	theapp->inspctn[4].name=theapp->inspctn[4].name;
	theapp->inspctn[5].name=theapp->inspctn[5].name;
	theapp->inspctn[6].name=theapp->inspctn[6].name;
	theapp->inspctn[7].name=theapp->inspctn[7].name;
	theapp->inspctn[8].name=theapp->inspctn[8].name;
	theapp->inspctn[9].name=theapp->inspctn[9].name;
	theapp->inspctn[10].name=theapp->inspctn[10].name;
	theapp->inspctn[11].name=theapp->inspctn[11].name;

	for (int CurInspctn=1; CurInspctn<=11; CurInspctn++)
	{
		theapp->inspctn[CurInspctn].enable	= theapp->inspctn[CurInspctn].enable;
		theapp->inspctn[CurInspctn].lmin	= theapp->inspctn[CurInspctn].lmin;
		theapp->inspctn[CurInspctn].lmax	= theapp->inspctn[CurInspctn].lmax;
		theapp->inspctn[CurInspctn].lminB	= theapp->inspctn[CurInspctn].lminB;
		theapp->inspctn[CurInspctn].lmaxB	= theapp->inspctn[CurInspctn].lmaxB;
		theapp->inspctn[CurInspctn].lminC	= theapp->inspctn[CurInspctn].lminC;
		theapp->inspctn[CurInspctn].lmaxC	= theapp->inspctn[CurInspctn].lmaxC;
	}

	pframe->SaveJob(New);
}


void Job::OnA() 
{
	Value+="A";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnB() 
{
	Value+="B";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnC() 
{
	Value+="C";
	m_renamejob.Format("%s",Value);
	UpdateData(false);	
}

void Job::OnD() 
{
	Value+="D";
	m_renamejob.Format("%s",Value);
	UpdateData(false);	
}

void Job::OnE() 
{
	Value+="E";
	m_renamejob.Format("%s",Value);
	UpdateData(false);	
}

void Job::OnF() 
{
	Value+="F";
	m_renamejob.Format("%s",Value);
	UpdateData(false);	
}

void Job::OnG() 
{
	Value+="G";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnH() 
{
	Value+="H";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnI() 
{
	Value+="I";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnJ() 
{
	Value+="J";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnK() 
{
	Value+="K";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnL() 
{
	Value+="L";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnM() 
{
	Value+="M";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnN() 
{
	Value+="N";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnO() 
{
	Value+="O";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnP() 
{
	Value+="P";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnQ() 
{
	Value+="Q";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnR() 
{
	Value+="R";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnS() 
{
	Value+="S";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnT() 
{
	Value+="T";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnU() 
{
	Value+="U";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnV() 
{
	Value+="V";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnW() 
{
	Value+="W";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnX() 
{
	Value+="X";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnY() 
{
	Value+="Y";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnZ() 
{
	Value+="Z";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnDel() 
{
	int d=Value.GetLength();
	Value=Value.Left(d-1);
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnDash() 
{
	Value+="-";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

void Job::OnJ0() 
{
	Value+="0";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnJ1() 
{
	Value+="1";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnJ2() 
{
	Value+="2";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnJ3() 
{
	Value+="3";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnJ4() 
{
	Value+="4";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnJ5() 
{
	Value+="5";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnJ6() 
{
	Value+="6";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnJ7() 
{
	Value+="7";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnJ8() 
{
	Value+="8";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnJ9() 
{
	Value+="9";
	m_renamejob.Format("%s",Value);
	UpdateData(false);
}

void Job::OnSaveas() 
{
	SaveAs saveas;
	saveas.DoModal();
}

void Job::OnDelete() 
{
	JobOver=m_joblist.GetCaretIndex();

	nIndex=JobOver;

	int n = m_joblist.GetTextLen( original );

	m_joblist.GetText( original, str.GetBuffer(n) );

	m_joblist.DeleteString(nIndex);

//	m_joblist.InsertString(nIndex,"xxx");



///////////////

	CString tmptxt;
	tmptxt.Format("[%02i]xxx", nIndex);
	m_joblist.InsertString(nIndex,tmptxt);

///////////////




	char folderName[255];
	sprintf(folderName, "JobArray\\Job%03d", nIndex);
	theapp->jobinfo[nIndex].jobname="xxx";
	theapp->WriteProfileString(folderName, "jobname", theapp->jobinfo[nIndex].jobname);
	theapp->WriteProfileInt(folderName,"taught",0);
}


int Job::LoadJobPic(int jobN)
{
	bool messedup=false;

	char buf[10];
	char currentpath1[60];
	char* pFileName1 = "c:\\SilganData\\Pat\\";//drive
	CString c=itoa(jobN,buf,10);
	strcpy(currentpath1,pFileName1);
	strcat(currentpath1,c);
	strcat(currentpath1,"\\");
	strcat(currentpath1,"TaughtPicR3");
	strcat(currentpath1,".bmp");

	//Using Pattern 256x192
	CVisRGBAByteImage image3RGB;

	try
	{
		//RGB
		image3RGB.ReadFile(currentpath1);
		imageFile3.Allocate(image3RGB.Rect());
		image3RGB.CopyPixelsTo(imageFile3);
		byteTempRGB3=(unsigned char*)imageFile3.PixelAddress(0,0,1);

		int camHeight = 408;
		int camWidth = 1024;
		CVisRGBAByteImage imageQ3(camHeight/3, camWidth/3,  1, -1,byteTempRGB3);
		imageC2Q3Taught=imageQ3; //paints image on screen
		
		//pframe->m_pxaxis->SaveColorCust(byteTempRGB3, theapp->camWidth[1]/4, theapp->camHeight[1]/4);

		//InvalidateRect(NULL,false);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Give up trying to read an image from this file.
		// Warn the user.

		CString errMsg;
		errMsg.Format("The file %s could not be opened. You will need to teach the system", currentpath1);
		AfxMessageBox(errMsg);

		int width	= 408/3;
		int height	= 1024/3;

		for(int y=0; y<height; y++)
		{
			for(int x=0; x<4*width; x+=4) 
			{ 
				*(byteTempRGB3 + x + 0 + 4*width*y) = 0; 
				*(byteTempRGB3 + x + 1 + 4*width*y) = 0; 
				*(byteTempRGB3 + x + 2 + 4*width*y) = 0; 
			}
		}

		CVisRGBAByteImage imageQ3(width, height, 1, -1,byteTempRGB3);
		imageC2Q3Taught=imageQ3; //paints image on screen

		messedup=true;
		return 1;
	}

	//////////////////////
	return 0;
}

void Job::UpdateDisplay()
{
	CClientDC ColorRect(this);
	ColorRect.SetWindowOrg(0,0);

	ColorRect.SetWindowOrg(-258,-10);

	CVisImageBase &refimage=imageC2Q3Taught;	//128x96
	assert(refimage.IsValid());
	if(refimage.IsValid())
	{refimage.DisplayInHdc(ColorRect.m_hDC);}

	UpdateData(false);
	InvalidateRect(NULL,false);
}
