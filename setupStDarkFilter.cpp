// setupStDarkFilter.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "setupStDarkFilter.h"
#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// setupStDarkFilter dialog


setupStDarkFilter::setupStDarkFilter(CWnd* pParent /*=NULL*/)
	: CDialog(setupStDarkFilter::IDD, pParent)
{
	//{{AFX_DATA_INIT(setupStDarkFilter)
	m_senFilterDark = _T("");
	//}}AFX_DATA_INIT
}


void setupStDarkFilter::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(setupStDarkFilter)
	DDX_Control(pDX, ID_STEP, m_step);
	DDX_Text(pDX, IDC_SENDARK, m_senFilterDark);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(setupStDarkFilter, CDialog)
	//{{AFX_MSG_MAP(setupStDarkFilter)
	ON_BN_CLICKED(IDC_USEIMGST1, OnUseimgst1)
	ON_BN_CLICKED(IDC_USEIMGST2, OnUseimgst2)
	ON_BN_CLICKED(IDC_USEIMGST3, OnUseimgst3)
	ON_BN_CLICKED(IDC_USEIMGST4, OnUseimgst4)
	ON_BN_CLICKED(IDC_USEIMGST5, OnUseimgst5)
	ON_BN_CLICKED(IDC_USEIMGST6, OnUseimgst6)
	ON_BN_CLICKED(IDC_SENDARKUP, OnSendarkup)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_SENDARKDW, OnSendarkdw)
	ON_BN_CLICKED(ID_STEP, OnStep)
	ON_BN_CLICKED(IDC_USEIMGST7, OnUseimgst7)
	ON_BN_CLICKED(IDC_USEIMGST8, OnUseimgst8)
	ON_BN_CLICKED(IDC_USEIMGST9, OnUseimgst9)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// setupStDarkFilter message handlers

void setupStDarkFilter::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

void setupStDarkFilter::OnUseimgst1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stDarkImg = 1;
	UpdateValues();			
}

void setupStDarkFilter::OnUseimgst2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stDarkImg = 2;
	UpdateValues();			
}

void setupStDarkFilter::OnUseimgst3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stDarkImg = 3;
	UpdateValues();			
}

void setupStDarkFilter::OnUseimgst4() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stDarkImg = 4;
	UpdateValues();			
}

void setupStDarkFilter::OnUseimgst5() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stDarkImg = 5;
	UpdateValues();			
}

void setupStDarkFilter::OnUseimgst6() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stDarkImg = 6;
	UpdateValues();			
}

void setupStDarkFilter::OnUseimgst7() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stDarkImg = 7;
	UpdateValues();			
}

void setupStDarkFilter::OnUseimgst8() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stDarkImg = 8;
	UpdateValues();			
}


BOOL setupStDarkFilter::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_psetDarkF=this;
	theapp=(CBottleApp*)AfxGetApp();

	stepSlow=true;
	resSen=2;


	UpdateDisplay();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void setupStDarkFilter::UpdateValues()
{
	SetTimer(1,300,NULL);
	UpdateDisplay();

	pframe->m_pxaxis->InvalidateRect(NULL,false);
	pframe->m_pxaxis->UpdateData(false);
}

void setupStDarkFilter::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{KillTimer(1); pframe->SaveJob(pframe->CurrentJobNum);}

	CDialog::OnTimer(nIDEvent);
}

void setupStDarkFilter::UpdateDisplay()
{
	m_senFilterDark.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].stDarkSen);

	CheckDlgButton(IDC_USEIMGST1,0);
	CheckDlgButton(IDC_USEIMGST2,0);
	CheckDlgButton(IDC_USEIMGST3,0);
	CheckDlgButton(IDC_USEIMGST4,0);
	CheckDlgButton(IDC_USEIMGST5,0);
	CheckDlgButton(IDC_USEIMGST6,0);
	CheckDlgButton(IDC_USEIMGST7,0);
	CheckDlgButton(IDC_USEIMGST8,0);
	CheckDlgButton(IDC_USEIMGST9,0);

	if(theapp->jobinfo[pframe->CurrentJobNum].stDarkImg==1)	{CheckDlgButton(IDC_USEIMGST1,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stDarkImg==2)	{CheckDlgButton(IDC_USEIMGST2,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stDarkImg==3)	{CheckDlgButton(IDC_USEIMGST3,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stDarkImg==4)	{CheckDlgButton(IDC_USEIMGST4,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stDarkImg==5)	{CheckDlgButton(IDC_USEIMGST5,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stDarkImg==6)	{CheckDlgButton(IDC_USEIMGST6,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stDarkImg==7)	{CheckDlgButton(IDC_USEIMGST7,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stDarkImg==8)	{CheckDlgButton(IDC_USEIMGST8,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stDarkImg==9)	{CheckDlgButton(IDC_USEIMGST9,1);}
	

	UpdateData(false);
}

void setupStDarkFilter::OnSendarkup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stDarkSen+=resSen;

	if(theapp->jobinfo[pframe->CurrentJobNum].stDarkSen>=255)
	{theapp->jobinfo[pframe->CurrentJobNum].stDarkSen=255;}

	UpdateValues();		
}

void setupStDarkFilter::OnSendarkdw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stDarkSen-=resSen;

	if(theapp->jobinfo[pframe->CurrentJobNum].stDarkSen<=1)
	{theapp->jobinfo[pframe->CurrentJobNum].stDarkSen=1;}

	UpdateValues();	
}

void setupStDarkFilter::OnStep() 
{
	if(stepSlow)	
	{
		stepSlow=false;m_step.SetWindowText("Fast"); 
		CheckDlgButton(ID_STEP,1);
		resSen=10;
	}
	else		
	{
		stepSlow=true;	m_step.SetWindowText("Slow"); 
		CheckDlgButton(ID_STEP,0);
		resSen=2;
	}
	
	UpdateData(false);			
}


void setupStDarkFilter::OnUseimgst9() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stDarkImg = 9;
	UpdateValues();			

	
}
