// WaistInspection.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "WaistInspection.h"
#include "mainfrm.h"
#include "xaxisview.h"
//#include "Camera.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// WaistInspection dialog


WaistInspection::WaistInspection(CWnd* pParent /*=NULL*/)
	: CDialog(WaistInspection::IDD, pParent)
{
	//{{AFX_DATA_INIT(WaistInspection)
	m_threshold1 = _T("");
	m_verticalWindowLength1 = _T("");
	m_horizontalWindowLength1 = _T("");
	m_AvgWindowSize1 = _T("");
	m_deviation1 = _T("");
	//}}AFX_DATA_INIT
}


void WaistInspection::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(WaistInspection)
	DDX_Control(pDX, IDC_CAM1CAM4, m_cam1cam2);
	DDX_Control(pDX, IDC_STEP_SIZE, m_stepSize);
	DDX_Text(pDX, IDC_CAM1THRESHOLD1, m_threshold1);
	DDX_Text(pDX, IDC_CAM1VLENGTH1, m_verticalWindowLength1);
	DDX_Text(pDX, IDC_CAM1HLENGTH1, m_horizontalWindowLength1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(WaistInspection, CDialog)
	//{{AFX_MSG_MAP(WaistInspection)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CAM1THRESHOLD1_UP, OnCam1threshold1Up)
	ON_BN_CLICKED(IDC_CAM1THRESHOLD1_DOWN, OnCam1threshold1Down)
	ON_BN_CLICKED(IDC_CAM1HLENGTH1_UP, OnCam1hlength1Up)
	ON_BN_CLICKED(IDC_CAM1VLENGTH1_UP, OnCam1vlength1Up)
	ON_BN_CLICKED(IDC_CAM1VLENGTH1_DOWN, OnCam1vlength1Down)
	ON_BN_CLICKED(IDC_CAM1HLENGTH1_DOWN, OnCam1hlength1Down)
	ON_BN_CLICKED(IDC_STEP_SIZE, OnStepSize)
	ON_BN_CLICKED(IDC_CAM1_UPPER_X_LEFT, OnCam1UpperXLeft)
	ON_BN_CLICKED(IDC_CAM1_UPPER_X_RIGHT, OnCam1UpperXRight)
	ON_BN_CLICKED(IDC_CAM1_UPPER_Y_UP, OnCam1UpperYUp)
	ON_BN_CLICKED(IDC_CAM1_UPPER_Y_DOWN, OnCam1UpperYDown)
	ON_BN_CLICKED(IDC_CAM1_WIDTH_UPPER_LESS, OnCam1WidthUpperLess)
	ON_BN_CLICKED(IDC_CAM1_WIDTH_UPPER_MORE, OnCam1WidthUpperMore)
	ON_BN_CLICKED(IDC_CAM1_HEIGHT_UPPER_MORE, OnCam1HeightUpperMore)
	ON_BN_CLICKED(IDC_CAM1_HEIGHT_UPPER_LESS, OnCam1HeightUpperLess)
	ON_BN_CLICKED(IDC_CAM1_X_LEFT, OnCam1XLeft)
	ON_BN_CLICKED(IDC_CAM1_X_RIGHT, OnCam1XRight)
	ON_BN_CLICKED(IDC_CAM1_WIDTH_LESS, OnCam1WidthLess)
	ON_BN_CLICKED(IDC_CAM1_WIDTH_MORE, OnCam1WidthMore)
	ON_BN_CLICKED(IDC_CAM1_Y_UP, OnCam1YUp)
	ON_BN_CLICKED(IDC_CAM1_Y_DOWN, OnCam1YDown)
	ON_BN_CLICKED(IDC_CAM1_HEIGHT_MORE, OnCam1HeightMore)
	ON_BN_CLICKED(IDC_CAM1_HEIGHT_LESS, OnCam1HeightLess)
	ON_BN_CLICKED(IDC_CAM1CAM4, OnCam1cam4)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// WaistInspection message handlers

void WaistInspection::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

BOOL WaistInspection::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pWaistInspection=this;
	theapp=(CBottleApp*)AfxGetApp();

	isSmallStepSize = false;
	isCam1Settings = true;
	theapp->Cam1SelectedForWaist = true;

	UpdateDisplay();
	OnStepSize();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


//Upper
//X
void WaistInspection::OnCam1UpperXLeft() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperLeftBound-pixelStepSize > FullImageLeftEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperLeftBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperLeftBound-pixelStepSize > FullImageLeftEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperLeftBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection::OnCam1UpperXRight() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperLeftBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperLeftBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

//Y
void WaistInspection::OnCam1UpperYUp() 
{

	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperTopBound-pixelStepSize > FullImageTopEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperTopBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperTopBound-pixelStepSize > FullImageTopEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperTopBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}	
}

void WaistInspection::OnCam1UpperYDown() 
{

	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperTopBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperTopBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}

}

//Width
void WaistInspection::OnCam1WidthUpperLess() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperRightBound-pixelStepSize > 
	       theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperLeftBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperRightBound-pixelStepSize > 
	       theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperLeftBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}

}

void WaistInspection::OnCam1WidthUpperMore() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	
}

//Height
void WaistInspection::OnCam1HeightUpperMore() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperBottomBound-pixelStepSize >
		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperTopBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperBottomBound-pixelStepSize >
		theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperTopBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}

}



void WaistInspection::OnCam1HeightUpperLess() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementUpperBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementUpperBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}

}


//Waist
//X
void WaistInspection::OnCam1XLeft() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementLeftBound-pixelStepSize > FullImageLeftEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementLeftBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementLeftBound-pixelStepSize > FullImageLeftEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementLeftBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection::OnCam1XRight() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementLeftBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementLeftBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}	
}

//Width
void WaistInspection::OnCam1WidthLess() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementRightBound-pixelStepSize > 
	       theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementLeftBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementRightBound-pixelStepSize > 
	       theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementLeftBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection::OnCam1WidthMore() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}	
}

//Y
void WaistInspection::OnCam1YUp() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementTopBound-pixelStepSize > FullImageTopEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementTopBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementTopBound-pixelStepSize > FullImageTopEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementTopBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection::OnCam1YDown() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementTopBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementTopBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

//Height
void WaistInspection::OnCam1HeightMore() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementBottomBound-pixelStepSize >
		   theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementTopBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementBottomBound-pixelStepSize >
		   theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementTopBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection::OnCam1HeightLess() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistMeasurementBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}



void WaistInspection::OnCam1threshold1Up() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeThreshold+1 < thresholdMax)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeThreshold+=1;
			m_threshold1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeThreshold);
			UpdateAndSaveSetupValues();
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeThreshold+1 < thresholdMax)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeThreshold+=1;
			m_threshold1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeThreshold);
			UpdateAndSaveSetupValues();
		}
	}
}

void WaistInspection::OnCam1threshold1Down() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeThreshold-smallPixelIntensityStepSize > thresholdMin)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeThreshold-=smallPixelIntensityStepSize;
			m_threshold1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeThreshold);
			UpdateAndSaveSetupValues();	
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeThreshold-smallPixelIntensityStepSize > thresholdMin)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeThreshold-=smallPixelIntensityStepSize;
			m_threshold1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeThreshold);
			UpdateAndSaveSetupValues();	
		}
	}
}

void WaistInspection::OnCam1vlength1Up() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistVerticalSearchLength+smallPixelIntensityStepSize < verticalSearchLengthMax)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistVerticalSearchLength+=smallPixelIntensityStepSize;
			m_verticalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam1WaistVerticalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistVerticalSearchLength+smallPixelIntensityStepSize < verticalSearchLengthMax)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistVerticalSearchLength+=smallPixelIntensityStepSize;
			m_verticalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam4WaistVerticalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
}

void WaistInspection::OnCam1vlength1Down() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistVerticalSearchLength-1 > verticalSearchLengthMin)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistVerticalSearchLength-=1;
			m_verticalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam1WaistVerticalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistVerticalSearchLength-1 > verticalSearchLengthMin)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistVerticalSearchLength-=1;
			m_verticalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam4WaistVerticalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
}

void WaistInspection::OnCam1hlength1Up() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistHorizontalSearchLength+1 < horizontalSearchLengthMax)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistHorizontalSearchLength+=1;
			m_horizontalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam1WaistHorizontalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistHorizontalSearchLength+1 < horizontalSearchLengthMax)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistHorizontalSearchLength+=1;
			m_horizontalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam4WaistHorizontalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
}

void WaistInspection::OnCam1hlength1Down() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistHorizontalSearchLength-1 > horizontalSearchLengthMin)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistHorizontalSearchLength-=1;
			m_horizontalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam1WaistHorizontalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistHorizontalSearchLength-1 > horizontalSearchLengthMin)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistHorizontalSearchLength-=1;
			m_horizontalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam4WaistHorizontalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
}

/*
void WaistInspection::OnCam1avglength1Up() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistAvgWindowSize+1 < avgWindowSize1Max)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistAvgWindowSize+=1;
			m_AvgWindowSize1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam1WaistAvgWindowSize);
			UpdateAndSaveSetupValues();
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistAvgWindowSize+1 < avgWindowSize1Max)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistAvgWindowSize+=1;
			m_AvgWindowSize1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam4WaistAvgWindowSize);
			UpdateAndSaveSetupValues();
		}
	}
}


void WaistInspection::OnCam1avglength1Down() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistAvgWindowSize-1 > avgWindowSize1Min)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistAvgWindowSize-=1;
			m_AvgWindowSize1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam1WaistAvgWindowSize);
			UpdateAndSaveSetupValues();
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistAvgWindowSize-1 > avgWindowSize1Min)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistAvgWindowSize-=1;
			m_AvgWindowSize1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam4WaistAvgWindowSize);
			UpdateAndSaveSetupValues();
		}
	}
}



void WaistInspection::OnCam1deviation1Up() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeCoordinateDeviation+1 < deviationMax)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeCoordinateDeviation+=1;
			m_deviation1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeCoordinateDeviation);
			UpdateAndSaveSetupValues();
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeCoordinateDeviation+1 < deviationMax)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeCoordinateDeviation+=1;
			m_deviation1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeCoordinateDeviation);
			UpdateAndSaveSetupValues();
		}
	}
}

void WaistInspection::OnCam1deviation1Down() 
{
	if(isCam1Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeCoordinateDeviation-1 > deviationMin)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeCoordinateDeviation-=1;
			m_deviation1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeCoordinateDeviation);
			UpdateAndSaveSetupValues();
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeCoordinateDeviation-1 > deviationMin)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeCoordinateDeviation-=1;
			m_deviation1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeCoordinateDeviation);
			UpdateAndSaveSetupValues();
		}
	}
}
*/


void WaistInspection::OnStepSize() 
{
	if(isSmallStepSize)
	{
		isSmallStepSize = false;
		pixelStepSize = largePixelStepSize;
		pixelIntensityStepSize = largePixelIntensityStepSize;
		m_stepSize.SetWindowText("Fast");
		CheckDlgButton(IDC_STEP_SIZE,1); 
	}
	else
	{
		isSmallStepSize = true;
		pixelStepSize = smallPixelStepSize;
		pixelIntensityStepSize = smallPixelIntensityStepSize;
		m_stepSize.SetWindowText("Slow");
		CheckDlgButton(IDC_STEP_SIZE,1); 
	}
	
	UpdateData(false);	
}

void WaistInspection::OnCam1cam4() 
{
	if(isCam1Settings)
	{
		isCam1Settings = false;
		m_cam1cam2.SetWindowText("CAM4");

		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionLeftBound= 320; 
		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionRightBound= 320+259; 
		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionTopBound= 344;
		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionBottomBound= 0;
		theapp->Cam1SelectedForWaist = false;
	}
	else
	{
		isCam1Settings = true;
		m_cam1cam2.SetWindowText("CAM1");

		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionLeftBound= 0; 
		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionRightBound= 259; 
		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionTopBound= 344;
		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionBottomBound= 0;
		theapp->Cam1SelectedForWaist = true;
	}
	UpdateAndSaveSetupValues();
	UpdateDisplay();
}


void WaistInspection::UpdateWaistWidthOverlayValues()
{
//	theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayStartX = theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementLeftBound;
		
		//theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementLeftBound+
		//((theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementRightBound-theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementLeftBound)
		//  - theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthThreshold)/2;

//	theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayEndX = theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayStartX + 
//		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthThreshold;

//	theapp->jobinfo[pframe->CurrentJobNum].cam1WaistWidthOverlayY = theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementTopBound+
//		(theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementBottomBound-theapp->jobinfo[pframe->CurrentJobNum].cam1WaistMeasurementTopBound)/2;
}


void WaistInspection::UpdateDisplay()
{
	if(isCam1Settings)
	{
		m_threshold1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeThreshold);
		m_verticalWindowLength1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam1WaistVerticalSearchLength);
		m_horizontalWindowLength1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam1WaistHorizontalSearchLength);
		
		//m_AvgWindowSize1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam1WaistAvgWindowSize);
		//m_deviation1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeCoordinateDeviation);

		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionLeftBound= 0; 
		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionRightBound= 259; 
		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionTopBound= 344;
		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionBottomBound= 0;
	}
	else
	{
		m_threshold1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeThreshold);
		m_verticalWindowLength1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam4WaistVerticalSearchLength);
		m_horizontalWindowLength1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam4WaistHorizontalSearchLength);
		//m_AvgWindowSize1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam4WaistAvgWindowSize);
		//m_deviation1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam4WaistEdgeCoordinateDeviation);

		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionLeftBound= 320; 
		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionRightBound= 320+259; 
		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionTopBound= 344;
		theapp->jobinfo[pframe->CurrentJobNum].cam1WaistSelectionBottomBound= 0;
	}

	UpdateData(false);
}

void WaistInspection::UpdateAndSaveSetupValues() 
{
	SetTimer(1,3000,NULL);

	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pxaxis->InvalidateRect(NULL, false); 	
}

void WaistInspection::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{
		KillTimer(1);
		pframe->SaveJob(pframe->CurrentJobNum);
	}

	CDialog::OnTimer(nIDEvent);
}


