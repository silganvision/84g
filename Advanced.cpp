// Advanced.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "Advanced.h"
#include "mainfrm.h"
#include "serial.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// Advanced dialog


Advanced::Advanced(CWnd* pParent /*=NULL*/)
	: CDialog(Advanced::IDD, pParent)
{
	//{{AFX_DATA_INIT(Advanced)
	m_editheight = _T("");
	m_edittemp = _T("");
	m_editcurrent = _T("");
	m_editconv = _T("");
	//}}AFX_DATA_INIT
}


void Advanced::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Advanced)
	DDX_Text(pDX, IDC_EDITHEIGHT, m_editheight);
	DDX_Text(pDX, IDC_EDITTEMP, m_edittemp);
	DDX_Text(pDX, IDC_EDITTEMP2, m_editcurrent);
		DDX_Text(pDX, IDC_ENCSIM, m_enable);
	DDX_Text(pDX, IDC_EDITCONV, m_editconv);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Advanced, CDialog)
	//{{AFX_MSG_MAP(Advanced)
	ON_BN_CLICKED(IDC_ENCSIM, OnEncsim)
	ON_BN_CLICKED(IDC_PLCPOS, OnPlcpos)
	ON_BN_CLICKED(IDC_MORE, OnMore)
	ON_BN_CLICKED(IDC_LESS, OnLess)
	ON_BN_CLICKED(IDC_MORE2, OnMore2)
	ON_BN_CLICKED(IDC_LESS2, OnLess2)
	ON_BN_CLICKED(IDC_THIGHER, OnThigher)
	ON_BN_CLICKED(IDC_TLOWER, OnTlower)
	ON_BN_CLICKED(IDC_OHIGHER, OnOhigher)
	ON_BN_CLICKED(IDC_OLOWER, OnOlower)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_SENDRESETTOPLC, OnSendresettoplc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Advanced message handlers

void Advanced::OnEncsim() 
{
	pframe=(CMainFrame*)AfxGetMainWnd();
	if(theapp->SavedEncSim)
	{
		theapp->SavedEncSim=false;
		CheckDlgButton(IDC_ENCSIM,1); 
		m_enable.Format("%s","A timer is used in place of the encoder");

		//pframe->m_pserial->QueMessage(0,190);
		pframe->m_pserial->SendChar(0,190);
	}
	else
	{
		theapp->SavedEncSim=true;
		CheckDlgButton(IDC_ENCSIM,0);
		m_enable.Format("%s","A Normal Encoder is Used");

		//pframe->m_pserial->QueMessage(0,200);
		pframe->m_pserial->SendChar(0,200);
	}

	UpdateData(false);	
}

BOOL Advanced::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_padvanced=this;
	theapp=(CBottleApp*)AfxGetApp();
	
	if(!theapp->SavedEncSim)
	{
		CheckDlgButton(IDC_ENCSIM,1); 
		m_enable.Format("%s","A timer is used in place of the encoder");
	}
	else
	{
		CheckDlgButton(IDC_ENCSIM,0);
		m_enable.Format("%s","A Normal Encoder is Used");
	}

	localTemp = theapp->Temperature;
	m_editcurrent.Format("%3.2f",localTemp);
	SetTimer(2,2500,NULL);

	UpdateDisplay();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Advanced::OnPlcpos() 
{
	//pframe->m_pserial->QueMessage(0,210);
	pframe->m_pserial->SendChar(0,210);
}

void Advanced::OnMore() 
{
	motorHeight+=1;
	m_editheight.Format("%3i",motorHeight);

	pframe->m_pserial->SendChar(motorHeight,160);

	UpdateData(false);
}

void Advanced::OnLess() 
{
	motorHeight-=1;
	m_editheight.Format("%3i",motorHeight);

	pframe->m_pserial->SendChar(motorHeight,160);

	UpdateData(false);
}

void Advanced::OnMore2() 
{
	motorHeight+=10;
	m_editheight.Format("%3i",motorHeight);

	pframe->m_pserial->SendChar(motorHeight,160);

	UpdateData(false);
}

void Advanced::OnLess2() 
{
	motorHeight-=10;
	m_editheight.Format("%3i",motorHeight);

	pframe->m_pserial->SendChar(motorHeight,160);

	UpdateData(false);
}

void Advanced::OnThigher() 
{
	theapp->tempLimit+=1;
	m_edittemp.Format("%3i",theapp->tempLimit);

	UpdateData(false);
}

void Advanced::OnTlower() 
{
	theapp->tempLimit-=1;
	m_edittemp.Format("%3i",theapp->tempLimit);

	UpdateData(false);
}

void Advanced::OnOhigher() 
{
	theapp->convRatio+=1;
	m_editconv.Format("%3i",theapp->convRatio);
	pframe->m_pserial->SendChar(theapp->convRatio,290);
	UpdateData(false);
}

void Advanced::OnOlower() 
{
	theapp->convRatio-=1;
	m_editconv.Format("%3i",theapp->convRatio);
	pframe->m_pserial->SendChar(theapp->convRatio,290);
	UpdateData(false);
}


void Advanced::UpdateDisplay()
{
	motorHeight=200;
	m_editheight.Format("%3i",motorHeight);
	m_edittemp.Format("%3i",theapp->tempLimit);

	m_editconv.Format("%3i",theapp->convRatio);

	
	UpdateData(false);	
}

void Advanced::OnOK() 
{
	KillTimer(2);
	pframe->SaveJob(pframe->CurrentJobNum);
	
	CDialog::OnOK();
}

void Advanced::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,0,330);
		pframe->m_pserial->SendChar(0,330);
		KillTimer(1);
	}

	if (nIDEvent==2) 
	{
		//Update temperature only if temp changes
		if(theapp->Temperature!=localTemp)
		{
			localTemp = theapp->Temperature;
			m_editcurrent.Format("%3.2f",localTemp);
		}
	
		UpdateData(false);	
	}
	
	CDialog::OnTimer(nIDEvent);
}


void Advanced::OnSendresettoplc() 
{
	SetTimer(1,200,NULL);
	
	pframe->m_pserial->SendChar(0,330);

	SetTimer(1,400,NULL);

	UpdateData(false);	
}
