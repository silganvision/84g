#if !defined(AFX_WHITEBALANCE_H__A71B5723_A9A2_472B_AAA7_05A853E79B63__INCLUDED_)
#define AFX_WHITEBALANCE_H__A71B5723_A9A2_472B_AAA7_05A853E79B63__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WhiteBalance.h : header file
//
#include <FlyCapture2.h>
#include "Camera.h"

/////////////////////////////////////////////////////////////////////////////
// CWhiteBalance dialog


class CWhiteBalance : public CDialog
{
	// Construction
public:

	CMainFrame* pframe;
	CBottleApp* theapp;

	CWhiteBalance(CWnd* pParent = NULL);   // standard constructor
// Dialog Data
	//{{AFX_DATA(CCamera)
	enum { IDD = IDD_WHTBAL };
	enum { 
		NoCamSelected = 0, 
		Cam1Selected = 1, 
		Cam2Selected = 2, 
		Cam3Selected = 4, 
		Cam4Selected = 5, 
		Cam5Selected = 6
	};

	char SelectedCamera;
	char LinkedCameras;

	CSliderCtrl	m_redSlider;		//Slider control interface for red component.
	CSliderCtrl	m_blueSlider;		//Slider control interface for blue component.
	CButton m_auto;					//Button for Auto WB

	int   iPolarity;
	int   iSource;
	int   iRawValue;
	int   iMode;
	FlyCapture2::Error error;
	bool mOnePush, mOnOff, mAuto;

	bool m_whitebalanceAuto = false;
	int m_whitebalanceRed;	//GUI variable for displaying camera 1 whitebalance.
	int m_whitebalanceBlue;	//GUI variable for displaying camera 2 whitebalance.
	CString m_whitebalanceRedText;
	CString m_whitebalanceBlueText;
	CString m_sCam;
	CString m_sLinkCam1;
	CString m_sLinkCam2;
	CString m_sLinkCam3;
	CString m_sLinkCam4;
	int whtbalinc;
	//}}AFX_DATA

	void DoAutoWB(int cameraToUpdate);
	void SetAutoOff(int index);

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CCamera)
public:
	virtual BOOL DestroyWindow();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
//protected:
public:
	// Generated message map functions
	//{{AFX_MSG(CCamera)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnClose();
	afx_msg void Cam1RadioSelect();
	afx_msg void Cam2RadioSelect();
	afx_msg void Cam3RadioSelect();
	afx_msg void Cam4RadioSelect();
	afx_msg void Cam5RadioSelect();
	afx_msg void ClearLinks();
	afx_msg void LoadCamera1Values();
	afx_msg void LoadCamera2Values();
	afx_msg void LoadCamera3Values();
	afx_msg void LoadCamera4Values();
	afx_msg void LoadCamera5Values();
	afx_msg void RefreshWB();
	afx_msg void OnBlueDown();
	afx_msg void OnBlueUp();
	afx_msg void OnRedDown();
	afx_msg void OnRedUp();
	afx_msg void OnLinkCam1();
	afx_msg void OnUpdateLinked();
	afx_msg void OnAutoWB();
	afx_msg void OnLinkCam2();
	afx_msg void UpdateWBParameters(int wbred, int wbblue);
	afx_msg void UpdateWhiteBalanceRed(int whitebalanceRed);
	afx_msg void UpdateWhiteBalanceBlue(int whitebalanceBlue);
	afx_msg void OnReleasedcaptureBlueSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedcaptureRedSlider(NMHDR* pNMHDR, LRESULT* pResult);

	bool IsCamera1Selected();
	bool IsCamera2Selected();
	bool IsCamera3Selected();
	bool IsCamera4Selected();
	bool IsCamera5Selected();

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WHITEBALANCE_H__A71B5723_A9A2_472B_AAA7_05A853E79B63__INCLUDED_)
