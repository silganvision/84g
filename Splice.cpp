// Splice.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "Splice.h"
#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Splice dialog


Splice::Splice(CWnd* pParent /*=NULL*/)
	: CDialog(Splice::IDD, pParent)
{
	//{{AFX_DATA_INIT(Splice)
	m_resSplice1 = _T("");
	m_resSplice2 = _T("");
	m_resSplice3 = _T("");
	m_resSplice4 = _T("");
	m_ysplice1 = _T("");
	m_ysplice2 = _T("");
	m_ysplice3 = _T("");
	m_ysplice4 = _T("");
	m_wsplice = _T("");
	m_hsplice = _T("");
	m_hsplicesmBx = _T("");
	m_picNum = _T("");
	m_spVariance1 = _T("");
	m_spVariance2 = _T("");
	m_spVariance3 = _T("");
	m_spVariance4 = _T("");
	m_spVariance = _T("");
	//}}AFX_DATA_INIT
}


void Splice::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Splice)
	DDX_Control(pDX, ID_STEP, m_step);
	DDX_Text(pDX, IDC_SPLIC1, m_resSplice1);
	DDX_Text(pDX, IDC_SPLIC2, m_resSplice2);
	DDX_Text(pDX, IDC_SPLIC3, m_resSplice3);
	DDX_Text(pDX, IDC_SPLIC4, m_resSplice4);
	DDX_Text(pDX, IDC_YSPLICE1, m_ysplice1);
	DDX_Text(pDX, IDC_YSPLICE2, m_ysplice2);
	DDX_Text(pDX, IDC_YSPLICE3, m_ysplice3);
	DDX_Text(pDX, IDC_YSPLICE4, m_ysplice4);
	DDX_Text(pDX, IDC_WSPLICE, m_wsplice);
	DDX_Text(pDX, IDC_HSPLICE, m_hsplice);
	DDX_Text(pDX, IDC_HSMALLBOX, m_hsplicesmBx);
	DDX_Text(pDX, IDC_PICTOSHOW, m_picNum);
	DDX_Text(pDX, IDC_SPLICVAR1, m_spVariance1);
	DDX_Text(pDX, IDC_SPLICVAR2, m_spVariance2);
	DDX_Text(pDX, IDC_SPLICVAR3, m_spVariance3);
	DDX_Text(pDX, IDC_SPLICVAR4, m_spVariance4);
	DDX_Text(pDX, IDC_VARIANCE, m_spVariance);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Splice, CDialog)
	//{{AFX_MSG_MAP(Splice)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_YSPLICEUP1, OnYspliceup1)
	ON_BN_CLICKED(IDC_YSPLICEDW1, OnYsplicedw1)
	ON_BN_CLICKED(IDC_YSPLICEUP2, OnYspliceup2)
	ON_BN_CLICKED(IDC_YSPLICEDW2, OnYsplicedw2)
	ON_BN_CLICKED(IDC_YSPLICEUP3, OnYspliceup3)
	ON_BN_CLICKED(IDC_YSPLICEDW3, OnYsplicedw3)
	ON_BN_CLICKED(IDC_YSPLICEUP4, OnYspliceup4)
	ON_BN_CLICKED(IDC_YSPLICEDW4, OnYsplicedw4)
	ON_BN_CLICKED(IDC_WSPLICEUP, OnWspliceup)
	ON_BN_CLICKED(IDC_WSPLICEDW, OnWsplicedw)
	ON_BN_CLICKED(IDC_HSPLICEUP, OnHspliceup)
	ON_BN_CLICKED(IDC_HSPLICEDW, OnHsplicedw)
	ON_BN_CLICKED(IDC_HSMALLBOXUP, OnHsmallboxup)
	ON_BN_CLICKED(IDC_HSMALLBOXDW, OnHsmallboxdw)
	ON_BN_CLICKED(IDC_PICTOSHOWUP, OnPictoshowup)
	ON_BN_CLICKED(IDC_PICTOSHOWDN, OnPictoshowdn)
	ON_BN_CLICKED(IDC_ENAPICSTOSHOW, OnEnapicstoshow)
	ON_BN_CLICKED(IDC_USEIMGSPLICE1, OnUseimgsplice1)
	ON_BN_CLICKED(IDC_USEIMGSPLICE2, OnUseimgsplice2)
	ON_BN_CLICKED(IDC_USEIMGSPLICE3, OnUseimgsplice3)
	ON_BN_CLICKED(IDC_USEIMGSPLICE4, OnUseimgsplice4)
	ON_BN_CLICKED(IDC_USEIMGSPLICE5, OnUseimgsplice5)
	ON_BN_CLICKED(IDC_USEIMGSPLICE6, OnUseimgsplice6)
	ON_BN_CLICKED(IDC_USEIMGSPLICE7, OnUseimgsplice7)
	ON_BN_CLICKED(IDC_USEIMGSPLICE8, OnUseimgsplice8)
	ON_BN_CLICKED(IDC_VARIANCUP, OnVariancup)
	ON_BN_CLICKED(IDC_VARIANCDW, OnVariancdw)
	ON_BN_CLICKED(ID_STEP, OnStep)
	ON_BN_CLICKED(IDC_USEIMGSPLICE9, OnUseimgsplice9)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Splice message handlers
BOOL Splice::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_psplice=this;
	theapp=(CBottleApp*)AfxGetApp();

	lengthCam = MainDisplayImageWidth;
	heightCam = MainDisplayImageHeight;

	resBox=4;
	stepSlow=true;

	resVariance = 1;

	//UpdateDisplay(); //we dont have access to the xAxisView
	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Splice::UpdateDisplay()
{
	m_spVariance.Format("%4i", theapp->jobinfo[pframe->CurrentJobNum].spliceVar);

	m_spVariance1.Format("%04i",pframe->m_pxaxis->varianceSplice[1]);
	m_spVariance2.Format("%04i",pframe->m_pxaxis->varianceSplice[2]);
	m_spVariance3.Format("%04i",pframe->m_pxaxis->varianceSplice[3]);
	m_spVariance4.Format("%04i",pframe->m_pxaxis->varianceSplice[4]);

	m_resSplice1.Format("%03i",pframe->m_pxaxis->minSpliceAvg[1]);
	m_resSplice2.Format("%03i",pframe->m_pxaxis->minSpliceAvg[2]);
	m_resSplice3.Format("%03i",pframe->m_pxaxis->minSpliceAvg[3]);
	m_resSplice4.Format("%03i",pframe->m_pxaxis->minSpliceAvg[4]);

	m_ysplice1.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].ySplice[1]);
	m_ysplice2.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].ySplice[2]);
	m_ysplice3.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].ySplice[3]);
	m_ysplice4.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].ySplice[4]);

	m_wsplice.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].wSplice);
	m_hsplice.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].hSplice);
	m_hsplicesmBx.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].hSpliceSmBx);

	m_picNum.Format("%i", pframe->pictoShow);
	if(theapp->enabPicsToShow)		{CheckDlgButton(IDC_ENAPICSTOSHOW,1);}
	else							{CheckDlgButton(IDC_ENAPICSTOSHOW,0);}

	int filterSelSplice = theapp->jobinfo[pframe->CurrentJobNum].filterSelSplice;

	if(filterSelSplice==1){CheckDlgButton(IDC_USEIMGSPLICE1,1);}
	else{CheckDlgButton(IDC_USEIMGSPLICE1,0);}

	if(filterSelSplice==2)	{CheckDlgButton(IDC_USEIMGSPLICE2,1);}
	else{CheckDlgButton(IDC_USEIMGSPLICE2,0);}

	if(filterSelSplice==3)	{CheckDlgButton(IDC_USEIMGSPLICE3,1);}
	else{CheckDlgButton(IDC_USEIMGSPLICE3,0);}

	if(filterSelSplice==4)	{CheckDlgButton(IDC_USEIMGSPLICE4,1);}
	else{CheckDlgButton(IDC_USEIMGSPLICE4,0);}

	if(filterSelSplice==5)	{CheckDlgButton(IDC_USEIMGSPLICE5,1);}
	else{CheckDlgButton(IDC_USEIMGSPLICE5,0);}

	if(filterSelSplice==6)	{CheckDlgButton(IDC_USEIMGSPLICE6,1);}
	else{CheckDlgButton(IDC_USEIMGSPLICE6,0);}

	if(filterSelSplice==7)	{CheckDlgButton(IDC_USEIMGSPLICE7,1);}
	else{CheckDlgButton(IDC_USEIMGSPLICE7,0);}

	if(filterSelSplice==8)	{CheckDlgButton(IDC_USEIMGSPLICE8,1);}
	else{CheckDlgButton(IDC_USEIMGSPLICE8,0);}

	if(filterSelSplice==9)	{CheckDlgButton(IDC_USEIMGSPLICE9,1);}
	else{CheckDlgButton(IDC_USEIMGSPLICE9,0);}

	UpdateData(false);
	pframe->m_pxaxis->InvalidateRect(NULL,false);
}

void Splice::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{KillTimer(1); pframe->SaveJob(pframe->CurrentJobNum);}	

	CDialog::OnTimer(nIDEvent);
}

void Splice::OnYspliceup1() 
{
	int icam=1;

	theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]-=resBox;

	if(theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]=4;}

	UpdateValues();			
}

void Splice::OnYsplicedw1() 
{
	int icam=1;

	theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]+=resBox;

	if( (theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]+
		 theapp->jobinfo[pframe->CurrentJobNum].hSplice)>heightCam)
	{theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]=heightCam-32;}

	UpdateValues();				
}

void Splice::OnYspliceup2() 
{
	int icam=2;

	theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]-=resBox;

	if(theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]=4;}

	UpdateValues();		
}

void Splice::OnYsplicedw2() 
{
	int icam=2;

	theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]+=resBox;

	if( (theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]+
		 theapp->jobinfo[pframe->CurrentJobNum].hSplice)>heightCam)
	{theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]=heightCam-32;}

	UpdateValues();			
}

void Splice::OnYspliceup3() 
{
	int icam=3;

	theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]-=resBox;

	if(theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]=4;}

	UpdateValues();			
}

void Splice::OnYsplicedw3() 
{
	int icam=3;

	theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]+=resBox;

	if( (theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]+
		 theapp->jobinfo[pframe->CurrentJobNum].hSplice)>heightCam)
	{theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]=heightCam-32;}

	UpdateValues();			
}

void Splice::OnYspliceup4() 
{
	int icam=4;

	theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]-=resBox;

	if(theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]=4;}

	UpdateValues();			
}

void Splice::OnYsplicedw4() 
{
	int icam=4;

	theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]+=resBox;

	if( (theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]+
		 theapp->jobinfo[pframe->CurrentJobNum].hSplice)>heightCam)
	{theapp->jobinfo[pframe->CurrentJobNum].ySplice[icam]=heightCam-32;}

	UpdateValues();				
}

void Splice::OnWspliceup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].wSplice+=resBox;

	if(theapp->jobinfo[pframe->CurrentJobNum].wSplice> lengthCam )
	{theapp->jobinfo[pframe->CurrentJobNum].wSplice-=resBox;}

	UpdateValues();			
}

void Splice::OnWsplicedw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].wSplice-=resBox;
	if(theapp->jobinfo[pframe->CurrentJobNum].wSplice < 4 )
	{theapp->jobinfo[pframe->CurrentJobNum].wSplice=4;}

	UpdateValues();	
}

void Splice::OnHspliceup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].hSplice+=resBox;

	int height1 = theapp->jobinfo[pframe->CurrentJobNum].ySplice[1]+theapp->jobinfo[pframe->CurrentJobNum].hSplice;
	int height2 = theapp->jobinfo[pframe->CurrentJobNum].ySplice[2]+theapp->jobinfo[pframe->CurrentJobNum].hSplice;
	int height3 = theapp->jobinfo[pframe->CurrentJobNum].ySplice[3]+theapp->jobinfo[pframe->CurrentJobNum].hSplice;
	int height4 = theapp->jobinfo[pframe->CurrentJobNum].ySplice[4]+theapp->jobinfo[pframe->CurrentJobNum].hSplice;

	if(height1>heightCam || height2>heightCam || height3>heightCam || height4>heightCam)
	{theapp->jobinfo[pframe->CurrentJobNum].hSplice-=resBox;}

	UpdateValues();			
}

void Splice::OnHsplicedw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].hSplice-=resBox;

	if(theapp->jobinfo[pframe->CurrentJobNum].hSplice<4)
	{theapp->jobinfo[pframe->CurrentJobNum].hSplice=4;}

	UpdateValues();			
}

void Splice::OnHsmallboxup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].hSpliceSmBx+=resBox;
	UpdateValues();		
}

void Splice::OnHsmallboxdw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].hSpliceSmBx-=resBox;
	UpdateValues();		
}

void Splice::UpdateValues()
{
	SetTimer(1,300,NULL);
	UpdateDisplay();
}

void Splice::UpdateDisplay2()
{
	m_spVariance1.Format("%4i",pframe->m_pxaxis->varianceSplice[1]);
	m_spVariance2.Format("%4i",pframe->m_pxaxis->varianceSplice[2]);
	m_spVariance3.Format("%4i",pframe->m_pxaxis->varianceSplice[3]);
	m_spVariance4.Format("%4i",pframe->m_pxaxis->varianceSplice[4]);

	m_resSplice1.Format("%03i",pframe->m_pxaxis->minSpliceAvg[1]);
	m_resSplice2.Format("%03i",pframe->m_pxaxis->minSpliceAvg[2]);
	m_resSplice3.Format("%03i",pframe->m_pxaxis->minSpliceAvg[3]);
	m_resSplice4.Format("%03i",pframe->m_pxaxis->minSpliceAvg[4]);

	UpdateData(false);
}

void Splice::OnPictoshowup() 
{
	pframe->pictoShow++;	

	if(pframe->pictoShow>=49)	{pframe->pictoShow=49;}

	UpdateDisplay();
}

void Splice::OnPictoshowdn() 
{
	pframe->pictoShow--;	

	if(pframe->pictoShow<=1)	{pframe->pictoShow=1;}
	
	UpdateDisplay();
}

void Splice::OnEnapicstoshow() 
{
	if( theapp->enabPicsToShow)
	{CheckDlgButton(IDC_ENAPICSTOSHOW,0);	theapp->enabPicsToShow=false;}
	else
	{CheckDlgButton(IDC_ENAPICSTOSHOW,1);	theapp->enabPicsToShow=true;}
		
	UpdateData(false);		
}

void Splice::OnUseimgsplice1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelSplice = 1;
	UpdateValues();		
}

void Splice::OnUseimgsplice2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelSplice = 2;
	UpdateValues();		
}

void Splice::OnUseimgsplice3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelSplice = 3;
	UpdateValues();		
}

void Splice::OnUseimgsplice4() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelSplice = 4;
	UpdateValues();		
}

void Splice::OnUseimgsplice5() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelSplice = 5;
	UpdateValues();		
}

void Splice::OnUseimgsplice6() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelSplice = 6;
	UpdateValues();		
}

void Splice::OnUseimgsplice7() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelSplice = 7;
	UpdateValues();		
	
}

void Splice::OnUseimgsplice8() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelSplice = 8;
	UpdateValues();		
}

void Splice::OnUseimgsplice9() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelSplice = 9;
	UpdateValues();		
}

void Splice::OnVariancup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].spliceVar+=resVariance;
	UpdateValues();			
}

void Splice::OnVariancdw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].spliceVar-=resVariance;
	UpdateValues();			
	
}

void Splice::OnStep() 
{
	if(stepSlow)	
	{
		stepSlow=false;m_step.SetWindowText("Fast"); 
		CheckDlgButton(ID_STEP,1); resBox=12; resVariance = 10;

	}
	else		
	{
		stepSlow=true;	m_step.SetWindowText("Slow"); 
		CheckDlgButton(ID_STEP,0); resBox=4; resVariance = 1;
	} 
	
	UpdateData(false);	
}

