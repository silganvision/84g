// BottleDoc.h : interface of the CBottleDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_BOTTLEDOC_H__A249BB9A_96CF_48EE_9485_D5ED5FB7DEBB__INCLUDED_)
#define AFX_BOTTLEDOC_H__A249BB9A_96CF_48EE_9485_D5ED5FB7DEBB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CBottleDoc : public CDocument
{
protected: // create from serialization only
	CBottleDoc();
	DECLARE_DYNCREATE(CBottleDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBottleDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBottleDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CBottleDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BOTTLEDOC_H__A249BB9A_96CF_48EE_9485_D5ED5FB7DEBB__INCLUDED_)
