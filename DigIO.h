#if !defined(AFX_DIGIO_H__87E6943A_3701_4318_B639_00D5E42350EA__INCLUDED_)
#define AFX_DIGIO_H__87E6943A_3701_4318_B639_00D5E42350EA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DigIO.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DigIO dialog

class DigIO : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
	// Construction
public:
	bool tog;
	DigIO(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DigIO)
	enum { IDD = IDD_DIGIO };
	CString	m_editiorejects;
	CString	m_editconrejects;
	CString	m_editconrejects2;
	//}}AFX_DATA

CBottleApp* theapp;
CMainFrame* pframe;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DigIO)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DigIO)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBmore();
	afx_msg void OnBless();
	afx_msg void OnBmore2();
	afx_msg void OnBless2();
	afx_msg void OnHeartbeat();
	afx_msg void OnTestconej();
	afx_msg void OnTestejall();
	afx_msg void OnBmore3();
	afx_msg void OnBless3();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIGIO_H__87E6943A_3701_4318_B639_00D5E42350EA__INCLUDED_)
