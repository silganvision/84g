#if !defined(AFX_CAPSETUP1_H__6273AEFC_2D47_45B8_9CC7_54828D2771B1__INCLUDED_)
#define AFX_CAPSETUP1_H__6273AEFC_2D47_45B8_9CC7_54828D2771B1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CapSetup1.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCapSetup1 dialog

class CCapSetup1 : public CDialog
{
	friend class CMainFrame;
	friend class CBottleApp;
	friend class CCapSetup1;

// Construction
public:
	void UpdateDisplay2();
	void UpdateDisplay();
	void UpdateValues();
	CMainFrame* pframe;
	CBottleApp* theapp;

	CCapSetup1(CWnd* pParent = NULL);   // standard constructor
	int res;

// Dialog Data
	//{{AFX_DATA(CCapSetup1)
	enum { IDD = IDD_CAPSETUP1 };
	CString	m_editcsen;
	CString	m_editcsen2;
	CString	m_editfsize;
	CString	m_editfsize2;
	CString	m_currEdgeL;
	CString	m_currEdgeR;
	CString	m_currEdgeM;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCapSetup1)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCapSetup1)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnUseimg1();
	afx_msg void OnUseimg2();
	afx_msg void OnUseimg3();
	afx_msg void OnUseimg4();
	afx_msg void OnSenmore1();
	afx_msg void OnSenless1();
	afx_msg void OnSenmore2();
	afx_msg void OnSenless2();
	afx_msg void OnFilllarge();
	afx_msg void OnFillsmaller();
	afx_msg void OnFillup();
	afx_msg void OnFilldn();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CAPSETUP1_H__6273AEFC_2D47_45B8_9CC7_54828D2771B1__INCLUDED_)
