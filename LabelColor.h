#if !defined(AFX_LABELCOLOR_H__A283744B_5CFB_4D3B_AA8B_5DFEA4F4AFD4__INCLUDED_)
#define AFX_LABELCOLOR_H__A283744B_5CFB_4D3B_AA8B_5DFEA4F4AFD4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LabelColor.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// LabelColor dialog

class LabelColor : public CDialog
{
	friend class CMainFrame;
	friend class CBottleApp;
	// Construction
public:
	LabelColor(CWnd* pParent = NULL);   // standard constructor
	
	CMainFrame* pframe;
	CBottleApp* theapp;

	CBitmap bitmapl;
	CBitmap bitmapC;
	bool once;
// Dialog Data
	//{{AFX_DATA(LabelColor)
	enum { IDD = IDD_COLORTARG };
	CString	m_filter;
	CString	m_filter2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(LabelColor)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(LabelColor)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnMore();
	afx_msg void OnLess();
	afx_msg void OnTrigger();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnMore4();
	afx_msg void OnLess2();
	afx_msg void OnCapmore2();
	afx_msg void OnCapmore();
	afx_msg void OnCapless();
	afx_msg void OnCapless2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LABELCOLOR_H__A283744B_5CFB_4D3B_AA8B_5DFEA4F4AFD4__INCLUDED_)
