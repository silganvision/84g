// SleeverHeadData.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "SleeverHeadData.h"

#include "MainFrm.h"
#include "Serial.h"
#include "XAxisView.h"
#include <iostream>
#include <sstream>
#include <iomanip>


using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SleeverHeadData dialog


SleeverHeadData::SleeverHeadData(CWnd* pParent /*=NULL*/)
	: CDialog(SleeverHeadData::IDD, pParent)
{
	//{{AFX_DATA_INIT(SleeverHeadData)
	//}}AFX_DATA_INIT
}


void SleeverHeadData::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SleeverHeadData)
	DDX_Control(pDX, IDC_ENABLE, m_enable);
	DDX_Control(pDX, IDC_LIST1, m_list1control);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SleeverHeadData, CDialog)
	//{{AFX_MSG_MAP(SleeverHeadData)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_Save, OnSave)
	ON_BN_CLICKED(IDC_RESET, OnReset)
	ON_BN_CLICKED(IDC_ENABLE, OnEnable)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SleeverHeadData message handlers

void SleeverHeadData::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

BOOL SleeverHeadData::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pSleeverHeadData=this;
	theapp=(CBottleApp*)AfxGetApp();
	
	repaint=false;
	memset(tempArr,0,sizeof(tempArr));

//	memcpy(tempArr,pframe->arrayA,sizeof(int));

	std::copy(&pframe->arrayA[0][0],&pframe->arrayA[0][0]+(41*11),&tempArr[0][0]);

	m_list1control.SetExtendedStyle(LVS_EX_FULLROWSELECT);
 
	if(theapp->sleeverEnable)
	{
		m_enable.SetWindowText(" Tracker is\n Enabled");}
	else
	{m_enable.SetWindowText(" Tracker is\n Disabled");}

	PopulateListCtrl();

	InvalidateRect(NULL,false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void SleeverHeadData::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

/*
	int digit2=0;
	CString temp;


	CRect rect;
	m_list1control.GetClientRect(&rect);
	int nColInterval = rect.Width()/12;
	m_list1control.InsertColumn(0,"    ",LVCFMT_LEFT,2*nColInterval);

	for(int a=1;a<11;a++)
	{
		temp.Format("%s %d","I",a);
		m_list1control.InsertColumn(a,temp,LVCFMT_LEFT,nColInterval);		  
	 
	}
 
 
	for( int i=1;i<41;i++)	
	{
			temp.Format("%s %d","Head",i);
			int nitem=m_list1control.InsertItem(i,temp);;
			  
	 
			for(int j=1;j<11;j++)
			{	
				digit2=tempArr[i][j];
				temp.Format("%5i ",digit2);
			m_list1control.SetItemText(nitem,j,temp);
			
			}
		 
			
	}
*/
//	int temp1=m_list1control.GetItemCount();
	UpdateData(false);
	
	 
}

void SleeverHeadData::OnSave() 
{
		std::fstream myFile;

	 	myFile.open("C:\\SleeverData.txt", ios::in| ios::out|ios::trunc);

		if(myFile.is_open())
		{
		 	myFile.flush();
			CString	szDigit2;
			int digit2;

			myFile<<"\t  ";
				 

			for(int a=1;a<11;a++)
			{
				myFile<<setw(5);
				szDigit2.Format("%s%d","I",a);
				string str(szDigit2);	  
				myFile<<str;
			}
			 myFile<<"\n\n";

	 		for(int i=1;i<41;i++)	
			{
		 
				myFile<<"Head";
				myFile<<setw(3);
				szDigit2.Format("%d", i);
			    string str(szDigit2);	  
				myFile<<str;

				for(int j=1;j<11;j++)
				{	
					digit2=pframe->arrayA[i][j];
					myFile<<setw(5);
					szDigit2.Format("%i",digit2);
					string str2(szDigit2);	  
					myFile<<str2;

				}
				 	  
				myFile<<"\n";
			}
		
 			myFile.flush();
			myFile.close();
		}

	
}

void SleeverHeadData::OnReset() 
{
 
	pframe->sleeverHeadUnlocked=false;

	memset(tempArr,0,sizeof(tempArr));
	memset(pframe->arrayA,0,sizeof(pframe->arrayA));

	pframe->sleeverHeadUnlocked=true;


  
	m_list1control.DeleteAllItems();
/*
	UpdateWindow();

	PopulateListCtrl();
 */

	

	InvalidateRect(NULL,false);

	OnOK();
}

void SleeverHeadData::OnEnable() 
{
	// TODO: Add your control notification handler code here
	if(theapp->sleeverEnable	)
	{
		m_enable.SetWindowText("Tracker is\n  Disabled");
		theapp->sleeverEnable=false;

		if(theapp->serPresent)	{	pframe->m_pserial->SendChar(0,460);	}
	}
	else
	{
		m_enable.SetWindowText("Tracker is\n  Enabled");
		theapp->sleeverEnable=true;

		if(theapp->serPresent)	{	pframe->m_pserial->SendChar(0,450);	}
	}

	
}

void SleeverHeadData::PopulateListCtrl()
{

	int digit2=0;
	CString temp;


	CRect rect;
	m_list1control.GetClientRect(&rect);
	int nColInterval = rect.Width()/12;
	m_list1control.InsertColumn(0,"    ",LVCFMT_LEFT,2*nColInterval);

	for(int a=1;a<11;a++)
	{
		temp.Format("%s %d","I",a);
		m_list1control.InsertColumn(a,temp,LVCFMT_LEFT,nColInterval);		  
	 
	}
 
 
	for( int i=1;i<41;i++)	
	{
			temp.Format("%s %d","Head",i);
			int nitem=m_list1control.InsertItem(i,temp);;
			  
	 
			for(int j=1;j<11;j++)
			{	
				digit2=tempArr[i][j];
				temp.Format("%5i ",digit2);
			m_list1control.SetItemText(nitem,j,temp);
			
			}
		 
			
	}



}
