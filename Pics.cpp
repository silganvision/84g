// Pics.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "Pics.h"
#include "mainfrm.h"
#include "xaxisview.h"

#include <iostream>
#include <sstream>
#include <string>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Pics dialog


Pics::Pics(CWnd* pParent /*=NULL*/)
	: CDialog(Pics::IDD, pParent)
{
	//{{AFX_DATA_INIT(Pics)
	//}}AFX_DATA_INIT
}


void Pics::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Pics)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Pics, CDialog)
	//{{AFX_MSG_MAP(Pics)
	ON_BN_CLICKED(IDC_SAVEONESETCOL, OnSaveonesetcol)
	ON_BN_CLICKED(IDC_SAVEMANYSETS, OnSavemanysets)
	ON_BN_CLICKED(IDC_SAVEREGFILE, OnSaveregfile)
	ON_BN_CLICKED(IDC_SAVEDEFECTS, OnSavedefects)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Pics message handlers

BOOL Pics::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_ppics=this;
	theapp=(CBottleApp*)AfxGetApp();

	pframe->PicsOpen=true;
		
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Pics::OnCancel()		{pframe->PicsOpen=false; CDialog::OnCancel();}

void Pics::OnSaveonesetcol(){theapp->saveOneSetCol=true; pframe->picNum=1;}

void Pics::OnSavemanysets() {theapp->saveManySetCol=true; pframe->picNum=1;}

void Pics::OnSaveregfile() 
{
	system("reg export \"hkcu\\software\\Silgan Closures\\Bottle\" C:\\SilganData\\savedReg.reg");
}

void Pics::OnSavedefects() 
{
	pframe->picDefNum=1;
	theapp->saveDefects=true;
}
