// Motor.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "Motor.h"
#include "Mainfrm.h"
#include "serial.h"
#include "xaxisview.h"
#include "bottleview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Motor dialog


Motor::Motor(CWnd* pParent /*=NULL*/)
	: CDialog(Motor::IDD, pParent)
{
	//{{AFX_DATA_INIT(Motor)
	m_mpot = _T("");
	//}}AFX_DATA_INIT
}


void Motor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Motor)
	DDX_Text(pDX, IDC_MPOT, m_mpot);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Motor, CDialog)
	//{{AFX_MSG_MAP(Motor)
	ON_BN_CLICKED(IDC_HIGH, OnHigh)
	ON_BN_CLICKED(IDC_LOWER, OnLower)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_HIGH2, OnHigh2)
	ON_BN_CLICKED(IDC_LOWER2, OnLower2)
	ON_BN_CLICKED(IDC_TRIGGER, OnTrigger)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Motor message handlers

BOOL Motor::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pmotor=this;
	theapp=(CBottleApp*)AfxGetApp();
	pframe->OnLine=false;

	pframe->m_pview->SetTimer(2,500,NULL);
	//pframe->SetTimer(8,100,NULL);//com  //Does nothing
	
	if(theapp->serPresent)
	{pframe->m_pserial->SendChar(0,410);}//red light
	
	Value=0;
	Function=0;
	CntrBits=0;

	if(theapp->serPresent)
	{MotorPos=pframe->m_pserial->MotorPos;}
		
	m_mpot.Format("%i",MotorPos);
	SetTimer(5,10,NULL);

	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Motor::OnHigh() 
{
	if(theapp->serPresent)	
	{pframe->m_pserial->SendChar(0,130);}
	
	SetTimer(4,1000,NULL);	
	//UpdateData(false);
}


void Motor::OnLower() 
{
	if(theapp->serPresent)	
	{pframe->m_pserial->SendChar(0,140);}
	
	SetTimer(4,1000,NULL);
	//UpdateData(false);
}

void Motor::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1)	{KillTimer(1); UpdateData(false);}
	
	if (nIDEvent==2)	{KillTimer(2); UpdateData(false);}

	if(nIDEvent==3)
	{
		KillTimer(3);

		Value=0; Function=0; CntrBits=0;
		
		if(theapp->serPresent)
		{pframe->m_pserial->ControlChars=Value+Function+CntrBits;}
		
		SetTimer(4,500,NULL);
		pframe->m_pxaxis->InvalidateRect(NULL,false);
	}

	if(nIDEvent==4)
	{
		KillTimer(4);
		
		if(theapp->serPresent)
		{
			pframe->m_pserial->GetChar(0);			//Query PLC for motor position.
			MotorPos=pframe->m_pserial->MotorPos;
		}
		
		//if(MotorPos==0)		{SetTimer(4,5,NULL);}

		m_mpot.Format("%i",MotorPos);
		OnTrigger();
		UpdateData(false);
	}

	if (nIDEvent==5)	{UpdateDisplay();}
		
	CDialog::OnTimer(nIDEvent);
}

void Motor::OnHigh2() 
{
	if(theapp->serPresent) 	{pframe->m_pserial->SendChar(0,240);}

	SetTimer(4,200,NULL);	
	//UpdateData(false);
}

void Motor::OnLower2() 
{
	if(theapp->serPresent) 	{pframe->m_pserial->SendChar(0,250);}

	SetTimer(4,200,NULL);
	//UpdateData(false);
}

void Motor::OnOK() 
{
	KillTimer(5);

	if(theapp->serPresent)	{theapp->jobinfo[pframe->CurrentJobNum].motPos=pframe->MotPos=MotorPos=pframe->m_pserial->MotorPos;}

	pframe->SaveJob(pframe->CurrentJobNum);
	CDialog::OnOK();
}

void Motor::OnTrigger() 
{ 
	pframe->SendMessage(WM_TRIGGER,NULL,NULL); 
}

void Motor::UpdateDisplay()
{
	m_mpot.Format("%i",pframe->m_pserial->MotorPos);
	UpdateData(false);
}
