// Photoeye.cpp : implementation file
//

#include "stdafx.h"
#include "Bottle.h"
#include "Photoeye.h"
#include "mainfrm.h"
#include "serial.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Photoeye dialog


Photoeye::Photoeye(CWnd* pParent /*=NULL*/)
	: CDialog(Photoeye::IDD, pParent)
{
	//{{AFX_DATA_INIT(Photoeye)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void Photoeye::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Photoeye)
		DDX_Text(pDX, IDC_RADIO1A, m_enable);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Photoeye, CDialog)
	//{{AFX_MSG_MAP(Photoeye)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_RADIO1A, OnRadio1a)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Photoeye message handlers

BOOL Photoeye::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pphoto=this;
	theapp=(CBottleApp*)AfxGetApp();
	
	if(!theapp->SavedPhotoeye)
	{CheckDlgButton(IDC_RADIO1A,1); m_enable.Format("%s","Bottle flow from the right");}
	else
	{CheckDlgButton(IDC_RADIO1A,0);	m_enable.Format("%s","Bottle flow from the left");}
	
	UpdateData(false);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Photoeye::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{
		KillTimer(1);

		Value=0; Function=0; CntrBits=0;

		pframe=(CMainFrame*)AfxGetMainWnd();
		
		if(theapp->serPresent)	{pframe->m_pserial->ControlChars=Value+Function+CntrBits;}
	}	
	
	CDialog::OnTimer(nIDEvent);
}

void Photoeye::OnRadio1a() 
{
	pframe=(CMainFrame*)AfxGetMainWnd();

	if(theapp->SavedPhotoeye)
	{
		theapp->SavedPhotoeye=false;
		CheckDlgButton(IDC_RADIO1A,1); 
		m_enable.Format("%s","Bottle flow is from the right");
		
		if(theapp->serPresent)	{pframe->m_pserial->SendChar(0,220);}
	}
	else
	{
		theapp->SavedPhotoeye=true;
		CheckDlgButton(IDC_RADIO1A,0);
		m_enable.Format("%s","Bottle flow is from the left");

		if(theapp->serPresent)	{pframe->m_pserial->SendChar(0,230);}
	}

	pframe->SaveJob(pframe->CurrentJobNum); //save job

	UpdateData(false);	
}
