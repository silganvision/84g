#if !defined(AFX_DATABASE_H__3D33E4C1_7485_467E_B784_83E955C98D50__INCLUDED_)
#define AFX_DATABASE_H__3D33E4C1_7485_467E_B784_83E955C98D50__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Database.h : header file
#include "VisCore.h"
//
friend class CMainFrame;
friend class CCapApp; 
/////////////////////////////////////////////////////////////////////////////
// Database dialog

class Database : public CDialog
{
// Construction
public:
	void EraseCurrent();
	bool LoadOK;
	void MovePic(int index);
	void LoadPic(int index);
	char month[10];
	char dayofweek[10];
	char year[10];
	char index[10];
	char cp[60];

	char cp2[60];
	char currentpath2[60];
	bool GotIndex;
	int nIndex;
	CFile DataFile;
	CFile DataFile2;
	void ShowData();
	CString Value;
	CString CurrentSpot;
	Database(CWnd* pParent = NULL);   // standard constructor
	CVisByteImage imageMem;
	CCapApp *theapp;
	CMainFrame* pframe;
// Dialog Data
	//{{AFX_DATA(Database)
	enum { IDD = IDD_DATABASE };
	CListBox	m_savelist;
	CListBox	m_loadlist;
	//}}AFX_DATA

	CString D1;
	CString D2;
	afx_msg void OnLoad();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Database)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Database)
	virtual BOOL OnInitDialog();
	afx_msg void OnB10();
	afx_msg void OnAccept();
	virtual void OnOK();
	afx_msg void OnOndel();
	afx_msg void OnLoad2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATABASE_H__3D33E4C1_7485_467E_B784_83E955C98D50__INCLUDED_)
