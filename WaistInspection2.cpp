// WaistInspection2.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "WaistInspection2.h"
#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WaistInspection2 dialog


WaistInspection2::WaistInspection2(CWnd* pParent /*=NULL*/)
	: CDialog(WaistInspection2::IDD, pParent)
{
	//{{AFX_DATA_INIT(WaistInspection2)
	m_horizontalWindowLength1 = _T("");
	m_threshold1 = _T("");
	m_verticalWindowLength1 = _T("");
	//}}AFX_DATA_INIT
}


void WaistInspection2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(WaistInspection2)
	DDX_Control(pDX, IDC_STEP_SIZE, m_stepSize);
	DDX_Control(pDX, IDC_CAM1CAM4, m_cam1cam2);
	DDX_Text(pDX, IDC_CAM1HLENGTH1, m_horizontalWindowLength1);
	DDX_Text(pDX, IDC_CAM1THRESHOLD1, m_threshold1);
	DDX_Text(pDX, IDC_CAM1VLENGTH1, m_verticalWindowLength1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(WaistInspection2, CDialog)
	//{{AFX_MSG_MAP(WaistInspection2)
	ON_BN_CLICKED(IDC_STEP_SIZE, OnStepSize)
	ON_BN_CLICKED(IDC_CAM1_UPPER_X_LEFT, OnCam1UpperXLeft)
	ON_BN_CLICKED(IDC_CAM1_UPPER_X_RIGHT, OnCam1UpperXRight)
	ON_BN_CLICKED(IDC_CAM1_UPPER_Y_UP, OnCam1UpperYUp)
	ON_BN_CLICKED(IDC_CAM1_UPPER_Y_DOWN, OnCam1UpperYDown)
	ON_BN_CLICKED(IDC_CAM1_WIDTH_UPPER_LESS, OnCam1WidthUpperLess)
	ON_BN_CLICKED(IDC_CAM1_WIDTH_UPPER_MORE, OnCam1WidthUpperMore)
	ON_BN_CLICKED(IDC_CAM1_HEIGHT_UPPER_MORE, OnCam1HeightUpperMore)
	ON_BN_CLICKED(IDC_CAM1_HEIGHT_UPPER_LESS, OnCam1HeightUpperLess)
	ON_BN_CLICKED(IDC_CAM1_X_LEFT, OnCam1XLeft)
	ON_BN_CLICKED(IDC_CAM1_X_RIGHT, OnCam1XRight)
	ON_BN_CLICKED(IDC_CAM1_Y_UP, OnCam1YUp)
	ON_BN_CLICKED(IDC_CAM1_Y_DOWN, OnCam1YDown)
	ON_BN_CLICKED(IDC_CAM1_WIDTH_LESS, OnCam1WidthLess)
	ON_BN_CLICKED(IDC_CAM1_WIDTH_MORE, OnCam1WidthMore)
	ON_BN_CLICKED(IDC_CAM1_HEIGHT_MORE, OnCam1HeightMore)
	ON_BN_CLICKED(IDC_CAM1_HEIGHT_LESS, OnCam1HeightLess)
	ON_BN_CLICKED(IDC_CAM1THRESHOLD1_DOWN, OnCam1threshold1Down)
	ON_BN_CLICKED(IDC_CAM1THRESHOLD1_UP, OnCam1threshold1Up)
	ON_BN_CLICKED(IDC_CAM1VLENGTH1_DOWN, OnCam1vlength1Down)
	ON_BN_CLICKED(IDC_CAM1VLENGTH1_UP, OnCam1vlength1Up)
	ON_BN_CLICKED(IDC_CAM1HLENGTH1_DOWN, OnCam1hlength1Down)
	ON_BN_CLICKED(IDC_CAM1HLENGTH1_UP, OnCam1hlength1Up)
	ON_BN_CLICKED(IDC_CAM1CAM4, OnCam1cam4)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// WaistInspection2 message handlers

BOOL WaistInspection2::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pWaistInspection2=this;
	theapp=(CBottleApp*)AfxGetApp();

	isSmallStepSize = false;
	isCam2Settings = true;
	theapp->Cam2SelectedForWaist = true;

	UpdateDisplay();
	OnStepSize();

	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void WaistInspection2::OnCam1UpperXLeft() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperLeftBound-pixelStepSize > FullImageLeftEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperLeftBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperLeftBound-pixelStepSize > FullImageLeftEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperLeftBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1UpperXRight() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperLeftBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperLeftBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1UpperYUp() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperTopBound-pixelStepSize > FullImageTopEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperTopBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperTopBound-pixelStepSize > FullImageTopEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperTopBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1UpperYDown() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperTopBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperTopBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1WidthUpperLess() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperRightBound-pixelStepSize > 
	       theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperLeftBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperRightBound-pixelStepSize > 
	       theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperLeftBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1WidthUpperMore() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1HeightUpperMore() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperBottomBound-pixelStepSize >
		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperTopBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperBottomBound-pixelStepSize >
		theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperTopBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1HeightUpperLess() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementUpperBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementUpperBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1XLeft() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementLeftBound-pixelStepSize > FullImageLeftEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementLeftBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementLeftBound-pixelStepSize > FullImageLeftEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementLeftBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1XRight() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementLeftBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementLeftBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1WidthLess() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementRightBound-pixelStepSize > 
	       theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementLeftBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementRightBound-pixelStepSize > 
	       theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementLeftBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementRightBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1WidthMore() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementRightBound+pixelStepSize < FullImageRightEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementRightBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1YUp() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementTopBound-pixelStepSize > FullImageTopEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementTopBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementTopBound-pixelStepSize > FullImageTopEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementTopBound-=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1YDown() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementTopBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementTopBound+=pixelStepSize;
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1HeightMore() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementBottomBound-pixelStepSize >
		   theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementTopBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementBottomBound-pixelStepSize >
		   theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementTopBound)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementBottomBound-=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1HeightLess() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistMeasurementBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementBottomBound+pixelStepSize < FullImageBottomEdge)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistMeasurementBottomBound+=pixelStepSize;
			//UpdateWaistWidthOverlayValues();
			UpdateAndSaveSetupValues(); 
		}
	}
}

void WaistInspection2::OnCam1threshold1Down() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistEdgeThreshold-smallPixelIntensityStepSize > thresholdMin)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistEdgeThreshold-=smallPixelIntensityStepSize;
			m_threshold1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam2WaistEdgeThreshold);
			UpdateAndSaveSetupValues();	
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistEdgeThreshold-smallPixelIntensityStepSize > thresholdMin)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistEdgeThreshold-=smallPixelIntensityStepSize;
			m_threshold1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam3WaistEdgeThreshold);
			UpdateAndSaveSetupValues();	
		}
	}
}

void WaistInspection2::OnCam1threshold1Up() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistEdgeThreshold+1 < thresholdMax)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistEdgeThreshold+=1;
			m_threshold1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam2WaistEdgeThreshold);
			UpdateAndSaveSetupValues();
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistEdgeThreshold+1 < thresholdMax)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistEdgeThreshold+=1;
			m_threshold1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam3WaistEdgeThreshold);
			UpdateAndSaveSetupValues();
		}
	}
}

void WaistInspection2::OnCam1vlength1Down() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistVerticalSearchLength-1 > verticalSearchLengthMin)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistVerticalSearchLength-=1;
			m_verticalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam2WaistVerticalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistVerticalSearchLength-1 > verticalSearchLengthMin)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistVerticalSearchLength-=1;
			m_verticalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam3WaistVerticalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
}

void WaistInspection2::OnCam1vlength1Up() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistVerticalSearchLength+smallPixelIntensityStepSize < verticalSearchLengthMax)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistVerticalSearchLength+=smallPixelIntensityStepSize;
			m_verticalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam2WaistVerticalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistVerticalSearchLength+smallPixelIntensityStepSize < verticalSearchLengthMax)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistVerticalSearchLength+=smallPixelIntensityStepSize;
			m_verticalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam3WaistVerticalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
}

void WaistInspection2::OnCam1hlength1Up() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistHorizontalSearchLength+1 < horizontalSearchLengthMax)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistHorizontalSearchLength+=1;
			m_horizontalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam2WaistHorizontalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistHorizontalSearchLength+1 < horizontalSearchLengthMax)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistHorizontalSearchLength+=1;
			m_horizontalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam3WaistHorizontalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
}

void WaistInspection2::OnCam1hlength1Down() 
{
	if(isCam2Settings)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam2WaistHorizontalSearchLength-1 > horizontalSearchLengthMin)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam2WaistHorizontalSearchLength-=1;
			m_horizontalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam2WaistHorizontalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
	else
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].cam3WaistHorizontalSearchLength-1 > horizontalSearchLengthMin)
		{
			theapp->jobinfo[pframe->CurrentJobNum].cam3WaistHorizontalSearchLength-=1;
			m_horizontalWindowLength1.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].cam3WaistHorizontalSearchLength);
			UpdateAndSaveSetupValues();
		}
	}
}



void WaistInspection2::OnStepSize() 
{
	if(isSmallStepSize)
	{
		isSmallStepSize = false;
		pixelStepSize = largePixelStepSize;
		pixelIntensityStepSize = largePixelIntensityStepSize;
		m_stepSize.SetWindowText("Fast");
		CheckDlgButton(IDC_STEP_SIZE, 1); 
	}
	else
	{
		isSmallStepSize = true;
		pixelStepSize = smallPixelStepSize;
		pixelIntensityStepSize = smallPixelIntensityStepSize;
		m_stepSize.SetWindowText("Slow");
		CheckDlgButton(IDC_STEP_SIZE, 1); 
	}
	
	UpdateData(false);
}

void WaistInspection2::OnCam1cam4() 
{
	if(isCam2Settings)
	{
		isCam2Settings = false;
		m_cam1cam2.SetWindowText("CAM3");

		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionLeftBound= 320; 
		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionRightBound= 320+259; 
		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionTopBound= 344;
		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionBottomBound= 0;
		theapp->Cam2SelectedForWaist = false;
	}
	else
	{
		isCam2Settings = true;
		m_cam1cam2.SetWindowText("CAM2");

		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionLeftBound= 0; 
		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionRightBound= 259; 
		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionTopBound= 344;
		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionBottomBound= 0;
		theapp->Cam2SelectedForWaist = true;
	}
	UpdateAndSaveSetupValues();
	UpdateDisplay();
}

void WaistInspection2::UpdateDisplay()
{
	if(isCam2Settings)
	{
		m_threshold1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam2WaistEdgeThreshold);
		m_verticalWindowLength1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam2WaistVerticalSearchLength);
		m_horizontalWindowLength1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam2WaistHorizontalSearchLength);
		
		//m_AvgWindowSize1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam2WaistAvgWindowSize);
		//m_deviation1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam2WaistEdgeCoordinateDeviation);

		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionLeftBound= 0; 
		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionRightBound= 259; 
		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionTopBound= 344;
		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionBottomBound= 0;
	}
	else
	{
		m_threshold1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam3WaistEdgeThreshold);
		m_verticalWindowLength1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam3WaistVerticalSearchLength);
		m_horizontalWindowLength1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam3WaistHorizontalSearchLength);
		//m_AvgWindowSize1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam3WaistAvgWindowSize);
		//m_deviation1.Format("%4i",theapp->jobinfo[pframe->CurrentJobNum].cam3WaistEdgeCoordinateDeviation);

		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionLeftBound= 320; 
		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionRightBound= 320+259; 
		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionTopBound= 344;
		theapp->jobinfo[pframe->CurrentJobNum].cam2WaistSelectionBottomBound= 0;
	}

	UpdateData(false);
}

void WaistInspection2::UpdateAndSaveSetupValues() 
{
	SetTimer(1,3000,NULL);

	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pxaxis->InvalidateRect(NULL, false); 	
}

void WaistInspection2::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{
		KillTimer(1);
		pframe->SaveJob(pframe->CurrentJobNum);
	}

	CDialog::OnTimer(nIDEvent);
}
