// LabelAlignment.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "LabelAlignment.h"
#include "xaxisview.h"
#include "mainfrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// LabelAlignment dialog


LabelAlignment::LabelAlignment(CWnd* pParent /*=NULL*/)
	: CDialog(LabelAlignment::IDD, pParent)
{
	//{{AFX_DATA_INIT(LabelAlignment)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void LabelAlignment::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(LabelAlignment)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(LabelAlignment, CDialog)
	//{{AFX_MSG_MAP(LabelAlignment)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// LabelAlignment message handlers

void LabelAlignment::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

BOOL LabelAlignment::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void LabelAlignment::UpdateDisplay()
{

}
