// Tech.cpp : implementation file
//

#include "stdafx.h"
#include "Bottle.h"
#include "Tech.h"
#include "mainfrm.h"
#include "xaxisview.h"
#include "insp.h"
#include "serial.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Tech dialog


Tech::Tech(CWnd* pParent /*=NULL*/)
	: CDialog(Tech::IDD, pParent)
{
	//{{AFX_DATA_INIT(Tech)

	//}}AFX_DATA_INIT
}


void Tech::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Tech)
	
	DDX_Text(pDX, IDC_ENCSIM, m_enable);
	
	
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Tech, CDialog)
	//{{AFX_MSG_MAP(Tech)
	
	ON_BN_CLICKED(IDC_ENCSIM, OnEncsim)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_PLCPOS, OnPlcpos)
	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Tech message handlers

BOOL Tech::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_ptech=this;
	theapp=(CBottleApp*)AfxGetApp();

	if(!theapp->SavedEncSim)
	{
		CheckDlgButton(IDC_ENCSIM,1); 
		m_enable.Format("%s","A timer is used in place of the encoder");
	}
	else
	{
		CheckDlgButton(IDC_ENCSIM,0);
		m_enable.Format("%s","A Normal Encoder is Used");
	}
	
	UpdateData(false);

	Value=0; Function=0; CntrBits=0;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void Tech::OnEncsim() 
{
	pframe=(CMainFrame*)AfxGetMainWnd();

	if(theapp->SavedEncSim)
	{
		theapp->SavedEncSim=false;
		CheckDlgButton(IDC_ENCSIM,1); 
		m_enable.Format("%s","A timer is used in place of the encoder");
		
		if(theapp->serPresent) {pframe->m_pserial->SendChar(0,190);}
	}
	else
	{
		theapp->SavedEncSim=true;
		CheckDlgButton(IDC_ENCSIM,0);
		m_enable.Format("%s","A Normal Encoder is Used");
	
		if(theapp->serPresent)	{pframe->m_pserial->SendChar(0,200);}
	}

	UpdateData(false);	
}

void Tech::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{
		KillTimer(1);

		Value=0; Function=0; CntrBits=0;

		pframe=(CMainFrame*)AfxGetMainWnd();
		
		if(theapp->serPresent)	{pframe->m_pserial->ControlChars=Value+Function+CntrBits;}
	}	

	CDialog::OnTimer(nIDEvent);
}

void Tech::OnPlcpos() 
{
	if(theapp->serPresent) 	{pframe->m_pserial->SendChar(0,210);}
}


void Tech::OnOK() 
{
	pframe->SaveJob(pframe->CurrentJobNum);	
	
	CDialog::OnOK();
}


