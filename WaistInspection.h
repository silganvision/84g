#if !defined(AFX_WAISTINSPECTION_H__44AD3E14_F213_4EBE_9308_662F5FDCEB96__INCLUDED_)
#define AFX_WAISTINSPECTION_H__44AD3E14_F213_4EBE_9308_662F5FDCEB96__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WaistInspection.h : header file
//

#include "xaxisview.h"




/////////////////////////////////////////////////////////////////////////////
// WaistInspection dialog



#define FullImageLeftEdge 0
#define FullImageRightEdge FullImageScaledRotatedWidth-1
#define FullImageTopEdge 0
#define FullImageBottomEdge FullImageScaledRotatedHeight-1

#define thresholdMin 1
#define thresholdMax 255

#define verticalSearchLengthMin 1
#define verticalSearchLengthMax 30

#define horizontalSearchLengthMin 1
#define horizontalSearchLengthMax 30

#define avgWindowSize1Min 1
#define avgWindowSize1Max 30

#define deviationMin 2
#define deviationMax 50

#define waistWidthThresholdMin 1
#define waistWidthThresholdMax FullImageScaledRotatedWidth

#define smallPixelStepSize 1
#define largePixelStepSize 10

#define smallPixelIntensityStepSize 1
#define largePixelIntensityStepSize 5

class WaistInspection : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
// Construction
public:
	WaistInspection(CWnd* pParent = NULL);   // standard constructor

	CBottleApp *theapp;
	CMainFrame* pframe;

	int pixelStepSize;
	int pixelIntensityStepSize;
	bool isSmallStepSize;
	bool isCam1Settings;


	void UpdateWaistWidthOverlayValues();
	void UpdateAndSaveSetupValues();
	void UpdateDisplay();

// Dialog Data
	//{{AFX_DATA(WaistInspection)
	enum { IDD = IDD_WAIST_INSPECTION };
	CButton	m_cam1cam2;
	CButton	m_stepSize;
	CString	m_threshold1;
	CString	m_verticalWindowLength1;
	CString	m_horizontalWindowLength1;
	CString	m_AvgWindowSize1;
	CString	m_deviation1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(WaistInspection)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(WaistInspection)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnCam1threshold1Up();
	afx_msg void OnCam1threshold1Down();
	afx_msg void OnCam1hlength1Up();
	afx_msg void OnCam1vlength1Up();
	afx_msg void OnCam1vlength1Down();
	afx_msg void OnCam1hlength1Down();
	afx_msg void OnCam1waistthresholdUp();
	afx_msg void OnCam1waistthresholdDown();
	afx_msg void OnStepSize();
	afx_msg void OnCam1UpperXLeft();
	afx_msg void OnCam1UpperXRight();
	afx_msg void OnCam1UpperYUp();
	afx_msg void OnCam1UpperYDown();
	afx_msg void OnCam1WidthUpperLess();
	afx_msg void OnCam1WidthUpperMore();
	afx_msg void OnCam1HeightUpperMore();
	afx_msg void OnCam1HeightUpperLess();
	afx_msg void OnCam1XLeft();
	afx_msg void OnCam1XRight();
	afx_msg void OnCam1WidthLess();
	afx_msg void OnCam1WidthMore();
	afx_msg void OnCam1YUp();
	afx_msg void OnCam1YDown();
	afx_msg void OnCam1HeightMore();
	afx_msg void OnCam1HeightLess();
	afx_msg void OnCam1cam4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WAISTINSPECTION_H__44AD3E14_F213_4EBE_9308_662F5FDCEB96__INCLUDED_)
