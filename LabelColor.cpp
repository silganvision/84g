// LabelColor.cpp : implementation file
//

#include "stdafx.h"
#include "Bottle.h"
#include "LabelColor.h"
#include "xaxisview.h"
#include "mainfrm.h"
#include "modify.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// LabelColor dialog


LabelColor::LabelColor(CWnd* pParent /*=NULL*/)
	: CDialog(LabelColor::IDD, pParent)
{
	//{{AFX_DATA_INIT(LabelColor)
	m_filter = _T("");
	m_filter2 = _T("");
	//}}AFX_DATA_INIT
}


void LabelColor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(LabelColor)
	DDX_Text(pDX, IDC_FILTER, m_filter);
	DDX_Text(pDX, IDC_FILTER2, m_filter2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(LabelColor, CDialog)
	//{{AFX_MSG_MAP(LabelColor)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_MORE, OnMore)
	ON_BN_CLICKED(IDC_LESS, OnLess)
	ON_BN_CLICKED(IDC_TRIGGER, OnTrigger)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_MORE4, OnMore4)
	ON_BN_CLICKED(IDC_LESS2, OnLess2)
	ON_BN_CLICKED(IDC_CAPMORE2, OnCapmore2)
	ON_BN_CLICKED(IDC_CAPMORE, OnCapmore)
	ON_BN_CLICKED(IDC_CAPLESS, OnCapless)
	ON_BN_CLICKED(IDC_CAPLESS2, OnCapless2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// LabelColor message handlers

BOOL LabelColor::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*) AfxGetMainWnd();
	pframe->m_plabelcolor=this;
	theapp=(CBottleApp*)AfxGetApp();

	once=true;

	m_filter.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].colorFilter);
	m_filter2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].colorFilter2);
	
	
	UpdateData(false);
	InvalidateRect(NULL,false);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void LabelColor::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	if(once)
	{
		bitmapl.CreateCompatibleBitmap(&dc, 100,32); 
		bitmapC.CreateCompatibleBitmap(&dc, 20,12); 
		once=false; 
	}

	bitmapl.SetBitmapBits(100*32*4 , pframe->m_pxaxis->im_trouble);
	bitmapC.SetBitmapBits(20*12*4 , pframe->m_pxaxis->im_color2);
	
	CDC temp; temp.CreateCompatibleDC(0);
	CDC temp2; temp2.CreateCompatibleDC(0);

	BITMAP bmp;
	BITMAP bmp2;

	CBitmap *old2 =temp2.SelectObject(&bitmapC);
	::GetObject(bitmapC, sizeof(BITMAP), &bmp2);
	dc.BitBlt(35,64,35+20,64+12, &temp2,0,0,SRCCOPY);

	CBitmap *old =temp.SelectObject(&bitmapl);
	::GetObject(bitmapl, sizeof(BITMAP), &bmp);
	dc.BitBlt(173,60,173+100,60+32, &temp,0,0,SRCCOPY);

	// Do not call CDialog::OnPaint() for painting messages
}

void LabelColor::OnMore() 
{
	theapp->jobinfo[pframe->CurrentJobNum].colorFilter-=1;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].colorFilter <=0) 
	{theapp->jobinfo[pframe->CurrentJobNum].colorFilter=0;}

	m_filter.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].colorFilter);
	SetTimer(2,1000,NULL);
	
	UpdateData(false);
	InvalidateRect(NULL,false);
}

void LabelColor::OnLess() 
{
	theapp->jobinfo[pframe->CurrentJobNum].colorFilter+=1;
	m_filter.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].colorFilter);
	
	if(theapp->jobinfo[pframe->CurrentJobNum].colorFilter >255) 
	{theapp->jobinfo[pframe->CurrentJobNum].colorFilter=255;}

	SetTimer(2,1000,NULL);

	UpdateData(false);
	InvalidateRect(NULL,false);
}

void LabelColor::OnTrigger() 
{
	pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	SetTimer(1,500,NULL);
}

void LabelColor::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{
		KillTimer(1);
		UpdateData(false);
		InvalidateRect(NULL,false);

		pframe->m_pmod->UpdateData(false);
		pframe->m_pmod->InvalidateRect(NULL,false);
	}
	
	if (nIDEvent==2) 
	{
		KillTimer(2);
		pframe->SaveJob(pframe->CurrentJobNum);
	}	

	CDialog::OnTimer(nIDEvent);
}

void LabelColor::OnMore4() 
{
	theapp->jobinfo[pframe->CurrentJobNum].colorFilter-=10;
	m_filter.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].colorFilter);
	
	if(theapp->jobinfo[pframe->CurrentJobNum].colorFilter<=0) 
	{theapp->jobinfo[pframe->CurrentJobNum].colorFilter=0;}
	
	SetTimer(2,1000,NULL);
	
	UpdateData(false);
	InvalidateRect(NULL,false);
}

void LabelColor::OnLess2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].colorFilter+=10;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].colorFilter>255) 
	{theapp->jobinfo[pframe->CurrentJobNum].colorFilter=255;}

	m_filter.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].colorFilter);
	SetTimer(2,1000,NULL);

	UpdateData(false);
	InvalidateRect(NULL,false);
}

void LabelColor::OnCapmore2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].colorFilter2-=10;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].colorFilter2<=0) 
	{theapp->jobinfo[pframe->CurrentJobNum].colorFilter2=0;}

	m_filter2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].colorFilter2);
	SetTimer(2,1000,NULL);
	
	UpdateData(false);
	InvalidateRect(NULL,false);
}

void LabelColor::OnCapmore() 
{
	theapp->jobinfo[pframe->CurrentJobNum].colorFilter2-=1;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].colorFilter2<=0) 
	{theapp->jobinfo[pframe->CurrentJobNum].colorFilter2=0;}

	m_filter2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].colorFilter2);
	SetTimer(2,1000,NULL);
	
	UpdateData(false);
	InvalidateRect(NULL,false);
}

void LabelColor::OnCapless() 
{
	theapp->jobinfo[pframe->CurrentJobNum].colorFilter2+=1;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].colorFilter2>255) 
	{theapp->jobinfo[pframe->CurrentJobNum].colorFilter2=255;}

	m_filter2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].colorFilter2);
	SetTimer(2,1000,NULL);
	
	UpdateData(false);
	InvalidateRect(NULL,false);
}

void LabelColor::OnCapless2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].colorFilter2+=10;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].colorFilter2>255) 
	{theapp->jobinfo[pframe->CurrentJobNum].colorFilter2=255;}

	m_filter2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].colorFilter2);
	SetTimer(2,1000,NULL);
	
	UpdateData(false);
	InvalidateRect(NULL,false);
}
