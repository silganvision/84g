// LabIntegSetup.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "LabIntegSetup.h"

#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// LabIntegSetup dialog


LabIntegSetup::LabIntegSetup(CWnd* pParent /*=NULL*/)
	: CDialog(LabIntegSetup::IDD, pParent)
{
	//{{AFX_DATA_INIT(LabIntegSetup)
	m_topLimPatt = _T("");
	m_botLimPatt = _T("");
	m_sensMet3 = _T("");
	m_heightMethod3 = _T("");
	m_widthMethod3 = _T("");
	m_sensEdgeMet3 = _T("");
	m_Method5_Wt = _T("");
	m_Method5_Ht = _T("");
	m_Method5_Hb = _T("");
	m_Method5_Wb = _T("");
	m_Method5_Yt = _T("");
	m_Method5_Yb = _T("");
	m_heighMethod5 = _T("");
	m_widthMethod5 = _T("");
	//}}AFX_DATA_INIT
}


void LabIntegSetup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(LabIntegSetup)
	DDX_Control(pDX, IDC_FINEMOD, m_fine);
	DDX_Text(pDX, IDC_TOPLIMPATT, m_topLimPatt);
	DDX_Text(pDX, IDC_BOTLIMPATT, m_botLimPatt);
	DDX_Text(pDX, IDC_SENSMET3, m_sensMet3);
	DDX_Text(pDX, IDC_HEIGHTMET3, m_heightMethod3);
	DDX_Text(pDX, IDC_WIDTHMET3, m_widthMethod3);
	DDX_Text(pDX, IDC_SENSEDGMET3, m_sensEdgeMet3);
	DDX_Text(pDX, IDC_TOPWIDTH1, m_Method5_Wt);
	DDX_Text(pDX, IDC_TOPHEIGHT1, m_Method5_Ht);
	DDX_Text(pDX, IDC_BOTHEIGHT1, m_Method5_Hb);
	DDX_Text(pDX, IDC_BOTWIDTH1, m_Method5_Wb);
	DDX_Text(pDX, IDC_TOPYPOS1, m_Method5_Yt);
	DDX_Text(pDX, IDC_BOTYPOS1, m_Method5_Yb);
	DDX_Text(pDX, IDC_HEIGHTMET5_1, m_heighMethod5);
	DDX_Text(pDX, IDC_WIDTHMET5_1, m_widthMethod5);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(LabIntegSetup, CDialog)
	//{{AFX_MSG_MAP(LabIntegSetup)
	ON_BN_CLICKED(IDC_SENSMET3UP, OnSensmet3up)
	ON_BN_CLICKED(IDC_SENSMET3DW, OnSensmet3dw)
	ON_BN_CLICKED(IDC_TOPLIMPATTUP, OnToplimpattup)
	ON_BN_CLICKED(IDC_TOPLIMPATTDW, OnToplimpattdw)
	ON_BN_CLICKED(IDC_BOTLIMPATTUP, OnBotlimpattup)
	ON_BN_CLICKED(IDC_BOTLIMPATTDW, OnBotlimpattdw)
	ON_BN_CLICKED(IDC_FINEMOD, OnFinemod)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_HEIGHTMET3UP, OnHeightmet3up)
	ON_BN_CLICKED(IDC_HEIGHTMET3DW, OnHeightmet3dw)
	ON_BN_CLICKED(IDC_WIDTHMET3UP, OnWidthmet3up)
	ON_BN_CLICKED(IDC_WIDTHMET3DW, OnWidthmet3dw)
	ON_BN_CLICKED(IDC_SENSEDGMET3UP, OnSensedgmet3up)
	ON_BN_CLICKED(IDC_SENSEDGMET3DW, OnSensedgmet3dw)
	ON_BN_CLICKED(IDC_TOPHEIGHTUP1, OnTopheightup1)
	ON_BN_CLICKED(IDC_TOPHEIGHTDW1, OnTopheightdw1)
	ON_BN_CLICKED(IDC_TOPWIDTHUP1, OnTopwidthup1)
	ON_BN_CLICKED(IDC_TOPWIDTHDW1, OnTopwidthdw1)
	ON_BN_CLICKED(IDC_BOTHEIGHTUP1, OnBotheightup1)
	ON_BN_CLICKED(IDC_BOTHEIGHTDW1, OnBotheightdw1)
	ON_BN_CLICKED(IDC_BOTWIDTHUP1, OnBotwidthup1)
	ON_BN_CLICKED(IDC_BOTWIDTHDW1, OnBotwidthdw1)
	ON_BN_CLICKED(IDC_TOPYPOSUP1, OnTopyposup1)
	ON_BN_CLICKED(IDC_TOPYPOSDW1, OnTopyposdw1)
	ON_BN_CLICKED(IDC_BOTYPOSUP1, OnBotyposup1)
	ON_BN_CLICKED(IDC_BOTYPOSDW1, OnBotyposdw1)
	ON_BN_CLICKED(IDC_HEIGHTMET5UP1, OnHeightmet5up1)
	ON_BN_CLICKED(IDC_HEIGHTMET5DW1, OnHeightmet5dw1)
	ON_BN_CLICKED(IDC_WIDTHMET5UP1, OnWidthmet5up1)
	ON_BN_CLICKED(IDC_WIDTHMET5DW1, OnWidthmet5dw1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// LabIntegSetup message handlers

void LabIntegSetup::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

void LabIntegSetup::OnSensmet3up() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sensMet3+=2;
	if(theapp->jobinfo[pframe->CurrentJobNum].sensMet3>=250)
	{theapp->jobinfo[pframe->CurrentJobNum].sensMet3=250;}

	UpdateValues();	
}

void LabIntegSetup::OnSensmet3dw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sensMet3-=2;

	if(theapp->jobinfo[pframe->CurrentJobNum].sensMet3<10)
	{theapp->jobinfo[pframe->CurrentJobNum].sensMet3=10;}

	UpdateValues();		
}

void LabIntegSetup::OnToplimpattup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt<5)
	{theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt=5;}

	UpdateValues();
}

void LabIntegSetup::OnToplimpattdw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt>=300)
	{theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt=300;}

	UpdateValues();
}

void LabIntegSetup::OnBotlimpattup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt<5)
	{theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt=5;}

	UpdateValues();	
}

void LabIntegSetup::OnBotlimpattdw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt>=320)
	{theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt=320;}

	UpdateValues();	
}

void LabIntegSetup::OnFinemod() 
{
	if(res==1)	{m_fine.SetWindowText("Fast"); CheckDlgButton(IDC_FINE,1); res=10;}
	else		{m_fine.SetWindowText("Slow"); CheckDlgButton(IDC_FINE,0); res=1;}
	
	UpdateData(false);		
}

void LabIntegSetup::UpdateDisplay() 
{
	m_botLimPatt.Format		("%3i",theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt);
	m_topLimPatt.Format		("%3i",theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt);
	
	m_sensMet3.Format		("%3i",theapp->jobinfo[pframe->CurrentJobNum].sensMet3);
	m_sensEdgeMet3.Format	("%3i",theapp->jobinfo[pframe->CurrentJobNum].sensEdge);
	
	m_heightMethod3.Format	("%3i",theapp->jobinfo[pframe->CurrentJobNum].heigMet3);
	m_widthMethod3.Format	("%3i",theapp->jobinfo[pframe->CurrentJobNum].widtMet3);

	//////////////////

	m_Method5_Yt.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].boxYt);	
	m_Method5_Wt.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].boxWt);
	m_Method5_Ht.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].boxHt);

	m_Method5_Yb.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].boxYb);	
	m_Method5_Wb.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].boxWb);
	m_Method5_Hb.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].boxHb);

	m_heighMethod5.Format	("%3i",theapp->jobinfo[pframe->CurrentJobNum].heigMet5);
	m_widthMethod5.Format	("%3i",theapp->jobinfo[pframe->CurrentJobNum].widtMet5);
	
	//////////////////
	
	UpdateData(false);
}

BOOL LabIntegSetup::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_plabIntegSetup=this;
	theapp=(CBottleApp*)AfxGetApp();

	m_fine.SetWindowText("Slow"); 
	res=1;

	UpdateDisplay();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void LabIntegSetup::UpdateValues()
{
	KillTimer(1);
	SetTimer(1,300,NULL);
	UpdateDisplay();

	pframe->m_pxaxis->InvalidateRect(NULL,false);
	pframe->m_pxaxis->UpdateData(false);
}

void LabIntegSetup::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{KillTimer(1); pframe->SaveJob(pframe->CurrentJobNum);}

	CDialog::OnTimer(nIDEvent);
}

void LabIntegSetup::OnHeightmet3up() 
{
	theapp->jobinfo[pframe->CurrentJobNum].heigMet3+=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].heigMet3>=700)
	{theapp->jobinfo[pframe->CurrentJobNum].heigMet3=700;}

	UpdateValues();		
}

void LabIntegSetup::OnHeightmet3dw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].heigMet3-=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].heigMet3<15)
	{theapp->jobinfo[pframe->CurrentJobNum].heigMet3=15;}

	UpdateValues();			
}

void LabIntegSetup::OnWidthmet3up() 
{
	theapp->jobinfo[pframe->CurrentJobNum].widtMet3+=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].widtMet3>=700)
	{theapp->jobinfo[pframe->CurrentJobNum].widtMet3=700;}

	UpdateValues();		
}

void LabIntegSetup::OnWidthmet3dw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].widtMet3-=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].widtMet3<15)
	{theapp->jobinfo[pframe->CurrentJobNum].widtMet3=15;}

	UpdateValues();	
}

void LabIntegSetup::OnSensedgmet3up() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sensEdge+=2;
	if(theapp->jobinfo[pframe->CurrentJobNum].sensEdge>=250)
	{theapp->jobinfo[pframe->CurrentJobNum].sensEdge=250;}

	UpdateValues();
}

void LabIntegSetup::OnSensedgmet3dw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sensEdge-=2;

	if(theapp->jobinfo[pframe->CurrentJobNum].sensEdge<10)
	{theapp->jobinfo[pframe->CurrentJobNum].sensEdge=10;}

	UpdateValues();
}

void LabIntegSetup::OnTopheightup1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].boxHt+=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].boxHt>=700)
	{theapp->jobinfo[pframe->CurrentJobNum].boxHt=700;}

	UpdateValues2();		
}

void LabIntegSetup::OnTopheightdw1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].boxHt-=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].boxHt<15)
	{theapp->jobinfo[pframe->CurrentJobNum].boxHt=15;}

	UpdateValues2();		
}




void LabIntegSetup::OnTopyposup1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].boxYt-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].boxYt<5)
	{theapp->jobinfo[pframe->CurrentJobNum].boxYt=5;}

	UpdateValues2();	
}

void LabIntegSetup::OnTopyposdw1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].boxYt+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].boxYt>=300)
	{theapp->jobinfo[pframe->CurrentJobNum].boxYt=300;}

	UpdateValues2();
}

void LabIntegSetup::OnTopwidthup1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].boxWt+=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].boxWt>=700)
	{theapp->jobinfo[pframe->CurrentJobNum].boxWt=700;}

	UpdateValues2();			
}

void LabIntegSetup::OnTopwidthdw1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].boxWt-=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].boxWt<15)
	{theapp->jobinfo[pframe->CurrentJobNum].boxWt=15;}

	UpdateValues2();		
}

void LabIntegSetup::OnBotyposup1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].boxYb-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].boxYb<5)
	{theapp->jobinfo[pframe->CurrentJobNum].boxYb=5;}

	UpdateValues2();	
}

void LabIntegSetup::OnBotyposdw1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].boxYb+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].boxYb>=900)
	{theapp->jobinfo[pframe->CurrentJobNum].boxYb=900;}

	UpdateValues2();	
}

void LabIntegSetup::OnBotheightup1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].boxHb+=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].boxHb>=700)
	{theapp->jobinfo[pframe->CurrentJobNum].boxHb=700;}

	UpdateValues2();		
}

void LabIntegSetup::OnBotheightdw1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].boxHb-=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].boxHb<15)
	{theapp->jobinfo[pframe->CurrentJobNum].boxHb=15;}

	UpdateValues2();		
}

void LabIntegSetup::OnBotwidthup1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].boxWb+=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].boxWb>=700)
	{theapp->jobinfo[pframe->CurrentJobNum].boxWb=700;}

	UpdateValues2();			
}

void LabIntegSetup::OnBotwidthdw1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].boxWb-=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].boxWb<15)
	{theapp->jobinfo[pframe->CurrentJobNum].boxWb=15;}

	UpdateValues2();		
}

void LabIntegSetup::UpdateValues2()
{
	////////////////////////

/*	int tmpWt= theapp->jobinfo[pframe->CurrentJobNum].boxWt;
	int tmpHt= theapp->jobinfo[pframe->CurrentJobNum].boxHt;
	
	int tmpWb= theapp->jobinfo[pframe->CurrentJobNum].boxWb;
	int tmpHb= theapp->jobinfo[pframe->CurrentJobNum].boxHb;

	for(int i=1; i<=4; i++)
	{
		pframe->m_pxaxis->bxMethod5_Xt[i] = pframe->m_pxaxis->xMidd[i] - tmpWt/2;
		pframe->m_pxaxis->bxMethod5_Xb[i] = pframe->m_pxaxis->xMidd[i] - tmpWt/2;
	}
*/
	////////////////////////

	KillTimer(1);
	SetTimer(1,300,NULL);
	UpdateDisplay();

	pframe->m_pxaxis->InvalidateRect(NULL,false);
	pframe->m_pxaxis->UpdateData(false);

}



void LabIntegSetup::OnHeightmet5up1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].heigMet5+=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].heigMet5>=700)
	{theapp->jobinfo[pframe->CurrentJobNum].heigMet5=700;}

	UpdateValues();		
}

void LabIntegSetup::OnHeightmet5dw1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].heigMet5-=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].heigMet5<15)
	{theapp->jobinfo[pframe->CurrentJobNum].heigMet5=15;}

	UpdateValues();				
}

void LabIntegSetup::OnWidthmet5up1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].widtMet5+=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].widtMet5>=700)
	{theapp->jobinfo[pframe->CurrentJobNum].widtMet5=700;}

	UpdateValues();			
}

void LabIntegSetup::OnWidthmet5dw1() 
{

	theapp->jobinfo[pframe->CurrentJobNum].widtMet5-=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].widtMet5<15)
	{theapp->jobinfo[pframe->CurrentJobNum].widtMet5=15;}

	UpdateValues();		
}
