// CapSetup1.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "CapSetup1.h"
#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCapSetup1 dialog


CCapSetup1::CCapSetup1(CWnd* pParent /*=NULL*/)
	: CDialog(CCapSetup1::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCapSetup1)
	m_editcsen = _T("");
	m_editcsen2 = _T("");
	m_editfsize = _T("");
	m_editfsize2 = _T("");
	m_currEdgeL = _T("");
	m_currEdgeR = _T("");
	m_currEdgeM = _T("");
	//}}AFX_DATA_INIT
}


void CCapSetup1::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCapSetup1)
	DDX_Text(pDX, IDC_EDITCSEN1, m_editcsen);
	DDX_Text(pDX, IDC_EDITCSEN2, m_editcsen2);
	DDX_Text(pDX, IDC_EDITFSIZE, m_editfsize);
	DDX_Text(pDX, IDC_EDITFSIZE2, m_editfsize2);
	DDX_Text(pDX, IDC_EDGE_L, m_currEdgeL);
	DDX_Text(pDX, IDC_EDGE_R, m_currEdgeR);
	DDX_Text(pDX, IDC_EDGE_M, m_currEdgeM);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCapSetup1, CDialog)
	//{{AFX_MSG_MAP(CCapSetup1)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_USEIMG1, OnUseimg1)
	ON_BN_CLICKED(IDC_USEIMG2, OnUseimg2)
	ON_BN_CLICKED(IDC_USEIMG3, OnUseimg3)
	ON_BN_CLICKED(IDC_USEIMG4, OnUseimg4)
	ON_BN_CLICKED(IDC_SENMORE1, OnSenmore1)
	ON_BN_CLICKED(IDC_SENLESS1, OnSenless1)
	ON_BN_CLICKED(IDC_SENMORE2, OnSenmore2)
	ON_BN_CLICKED(IDC_SENLESS2, OnSenless2)
	ON_BN_CLICKED(IDC_FILLLARGE, OnFilllarge)
	ON_BN_CLICKED(IDC_FILLSMALLER, OnFillsmaller)
	ON_BN_CLICKED(IDC_FILLUP, OnFillup)
	ON_BN_CLICKED(IDC_FILLDN, OnFilldn)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCapSetup1 message handlers

void CCapSetup1::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

BOOL CCapSetup1::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pcapSetup1=this;
	theapp=(CBottleApp*)AfxGetApp();
	res=2;	

	//SetWindowPos(&wndTop,10,400,780,250,SWP_SHOWWINDOW | SWP_NOSIZE );

	//UpdateDisplay();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCapSetup1::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{KillTimer(1); pframe->SaveJob(pframe->CurrentJobNum);}
	
	CDialog::OnTimer(nIDEvent);
}

void CCapSetup1::OnUseimg1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelPattCap = 1;
	UpdateValues();			
}

void CCapSetup1::OnUseimg2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelPattCap = 2;
	UpdateValues();			
}

void CCapSetup1::OnUseimg3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelPattCap = 3;
	UpdateValues();			
}	

void CCapSetup1::OnUseimg4() 
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelPattCap = 4;
	UpdateValues();			
}

void CCapSetup1::UpdateValues()
{
	SetTimer(1,500,NULL);
	UpdateDisplay();
//	pframe->m_pxaxis->InvalidateRect(NULL,false);
}

void CCapSetup1::UpdateDisplay()
{
	int filterSelPattCap = theapp->jobinfo[pframe->CurrentJobNum].filterSelPattCap;

	if(filterSelPattCap==1){CheckDlgButton(IDC_USEIMG1,1);}else{CheckDlgButton(IDC_USEIMG1,0);}
	if(filterSelPattCap==2){CheckDlgButton(IDC_USEIMG2,1);}else{CheckDlgButton(IDC_USEIMG2,0);}
	if(filterSelPattCap==3){CheckDlgButton(IDC_USEIMG3,1);}else{CheckDlgButton(IDC_USEIMG3,0);}
	if(filterSelPattCap==4){CheckDlgButton(IDC_USEIMG4,1);}else{CheckDlgButton(IDC_USEIMG4,0);}
	
	m_editcsen.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].capStrength);
	m_editcsen2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].capStrength2);
	m_editfsize.Format("%3i",100-theapp->jobinfo[pframe->CurrentJobNum].sportSen);
	m_editfsize2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].sportPos);

	InvalidateRect(NULL,false);	
	UpdateData(false);
}

void CCapSetup1::OnSenmore1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].capStrength+=res;

	UpdateValues();
}

void CCapSetup1::OnSenless1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].capStrength-=res;

	UpdateValues();
}

void CCapSetup1::OnSenmore2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].capStrength2+=res;

	UpdateValues();
}

void CCapSetup1::OnSenless2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].capStrength2-=res;
	
	UpdateValues();
}

void CCapSetup1::OnFilllarge() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sportSen-=res;

	UpdateValues();
}

void CCapSetup1::OnFillsmaller() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sportSen+=res;
	
	UpdateValues();
}

void CCapSetup1::OnFillup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sportPos-=1;
	
	UpdateValues();
}

void CCapSetup1::OnFilldn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sportPos+=1;

	UpdateValues();
}

void CCapSetup1::UpdateDisplay2()
{

	m_currEdgeL.Format("%03i",pframe->m_pxaxis->currEdgeL);
	m_currEdgeR.Format("%03i",pframe->m_pxaxis->currEdgeR);
	m_currEdgeM.Format("%03i",pframe->m_pxaxis->currEdgeM);

	UpdateData(false);
}

void CCapSetup1::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	SetWindowPos(&wndTop,10,400,780,250,SWP_SHOWWINDOW | SWP_NOSIZE );
	
}
