// setupStEdgeFilter.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "setupStEdgeFilter.h"
#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// setupStEdgeFilter dialog


setupStEdgeFilter::setupStEdgeFilter(CWnd* pParent /*=NULL*/)
	: CDialog(setupStEdgeFilter::IDD, pParent)
{
	//{{AFX_DATA_INIT(setupStEdgeFilter)
	m_senFilterEdge = _T("");
	//}}AFX_DATA_INIT
}


void setupStEdgeFilter::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(setupStEdgeFilter)
	DDX_Control(pDX, ID_STEP, m_step);
	DDX_Text(pDX, IDC_SENEDGE, m_senFilterEdge);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(setupStEdgeFilter, CDialog)
	//{{AFX_MSG_MAP(setupStEdgeFilter)
	ON_BN_CLICKED(IDC_USEIMGST1, OnUseimgst1)
	ON_BN_CLICKED(IDC_USEIMGST2, OnUseimgst2)
	ON_BN_CLICKED(IDC_USEIMGST3, OnUseimgst3)
	ON_BN_CLICKED(IDC_USEIMGST4, OnUseimgst4)
	ON_BN_CLICKED(IDC_USEIMGST5, OnUseimgst5)
	ON_BN_CLICKED(IDC_USEIMGST6, OnUseimgst6)
	ON_WM_TIMER()
	ON_BN_CLICKED(ID_STEP, OnStep)
	ON_BN_CLICKED(IDC_SENEDGEUP, OnSenedgeup)
	ON_BN_CLICKED(IDC_SENEDGEDW, OnSenedgedw)
	ON_BN_CLICKED(IDC_USEIMGST7, OnUseimgst7)
	ON_BN_CLICKED(IDC_USEIMGST8, OnUseimgst8)
	ON_BN_CLICKED(IDC_USEIMGST9, OnUseimgst9)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// setupStEdgeFilter message handlers

void setupStEdgeFilter::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

void setupStEdgeFilter::OnUseimgst1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg = 1;
	UpdateValues();			
	
}

void setupStEdgeFilter::OnUseimgst2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg = 2;
	UpdateValues();			
	
}

void setupStEdgeFilter::OnUseimgst3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg = 3;
	UpdateValues();			
	
}

void setupStEdgeFilter::OnUseimgst4() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg = 4;
	UpdateValues();			
	
}

void setupStEdgeFilter::OnUseimgst5() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg = 5;
	UpdateValues();			
	
}

void setupStEdgeFilter::OnUseimgst6() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg = 6;
	UpdateValues();			
}


void setupStEdgeFilter::OnUseimgst7() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg = 7;
	UpdateValues();			
}

void setupStEdgeFilter::OnUseimgst8() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg = 8;
	UpdateValues();			
}



//////

BOOL setupStEdgeFilter::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_psetEdgeF=this;
	theapp=(CBottleApp*)AfxGetApp();
	
	stepSlow=true;
	resSen=2;

	UpdateDisplay();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void setupStEdgeFilter::UpdateValues()
{
	SetTimer(1,300,NULL);
	UpdateDisplay();

	pframe->m_pxaxis->InvalidateRect(NULL,false);
	pframe->m_pxaxis->UpdateData(false);
}

void setupStEdgeFilter::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{KillTimer(1); pframe->SaveJob(pframe->CurrentJobNum);}


	CDialog::OnTimer(nIDEvent);
}

void setupStEdgeFilter::UpdateDisplay()
{
	m_senFilterEdge.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].stEdgeSen);

	CheckDlgButton(IDC_USEIMGST1,0);
	CheckDlgButton(IDC_USEIMGST2,0);
	CheckDlgButton(IDC_USEIMGST3,0);
	CheckDlgButton(IDC_USEIMGST4,0);
	CheckDlgButton(IDC_USEIMGST5,0);
	CheckDlgButton(IDC_USEIMGST6,0);
	CheckDlgButton(IDC_USEIMGST7,0);
	CheckDlgButton(IDC_USEIMGST8,0);
	CheckDlgButton(IDC_USEIMGST9,0);

	if(theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg==1)	{CheckDlgButton(IDC_USEIMGST1,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg==2)	{CheckDlgButton(IDC_USEIMGST2,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg==3)	{CheckDlgButton(IDC_USEIMGST3,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg==4)	{CheckDlgButton(IDC_USEIMGST4,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg==5)	{CheckDlgButton(IDC_USEIMGST5,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg==6)	{CheckDlgButton(IDC_USEIMGST6,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg==7)	{CheckDlgButton(IDC_USEIMGST7,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg==8)	{CheckDlgButton(IDC_USEIMGST8,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg==9)	{CheckDlgButton(IDC_USEIMGST9,1);}

	UpdateData(false);
}

void setupStEdgeFilter::OnStep() 
{
	if(stepSlow)	
	{
		stepSlow=false;m_step.SetWindowText("Fast"); 
		CheckDlgButton(ID_STEP,1);
		resSen=10;
	}
	else		
	{
		stepSlow=true;	m_step.SetWindowText("Slow"); 
		CheckDlgButton(ID_STEP,0);
		resSen=2;
	}
	
	UpdateData(false);			
}	


void setupStEdgeFilter::OnSenedgeup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stEdgeSen+=resSen;

	if(theapp->jobinfo[pframe->CurrentJobNum].stEdgeSen>=255)
	{theapp->jobinfo[pframe->CurrentJobNum].stEdgeSen=255;}

	UpdateValues();		
	
}

void setupStEdgeFilter::OnSenedgedw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stEdgeSen-=resSen;

	if(theapp->jobinfo[pframe->CurrentJobNum].stEdgeSen<=1)
	{theapp->jobinfo[pframe->CurrentJobNum].stEdgeSen=1;}

	UpdateValues();		
}

void setupStEdgeFilter::OnUseimgst9() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stEdgeImg = 9;
	UpdateValues();			
	
}
