// Status.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "Status.h"
#include "mainfrm.h"
#include "camera.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Status dialog


Status::Status(CWnd* pParent /*=NULL*/)
	: CDialog(Status::IDD, pParent)
{
	//{{AFX_DATA_INIT(Status)
	m_inittry = _T("");
	m_cam = _T("");
	//}}AFX_DATA_INIT
}


void Status::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Status)
	DDX_Text(pDX, IDC_TIMES, m_inittry);
	DDX_Text(pDX, IDC_WHO, m_cam);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Status, CDialog)
	//{{AFX_MSG_MAP(Status)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_STOP, OnStop)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Status message handlers

void Status::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)	{ KillTimer(1); OnOK();}
	
	CDialog::OnTimer(nIDEvent);
}

BOOL Status::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pstatus=this;
	theapp=(CBottleApp*)AfxGetApp();

	m_cam.Format("%2i",pframe->m_pcamera->cam);
	m_inittry.Format("%2i",pframe->m_pcamera->initTry);

	SetTimer(1,250,NULL);
	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Status::OnOK() 
{
	//pframe->m_pcamera->initTry=10;
	//KillTimer(1);
	
	CDialog::OnOK();
}

void Status::OnStop() 
{
	pframe->m_pcamera->initTry=10;
	SetTimer(1,1,NULL);
}
