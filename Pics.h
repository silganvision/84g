#if !defined(AFX_PICS_H__799493F3_E137_49ED_AF36_F2C1F190E33C__INCLUDED_)
#define AFX_PICS_H__799493F3_E137_49ED_AF36_F2C1F190E33C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Pics.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Pics dialog

class Pics : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
	// Construction
public:
	Pics(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Pics)
	enum { IDD = IDD_PICS };
	//}}AFX_DATA
	CBottleApp *theapp;
	CMainFrame* pframe;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Pics)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Pics)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnSaveonesetcol();
	afx_msg void OnSavemanysets();
	afx_msg void OnSaveregfile();
	afx_msg void OnSavedefects();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PICS_H__799493F3_E137_49ED_AF36_F2C1F190E33C__INCLUDED_)
