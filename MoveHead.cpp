// MoveHead.cpp : implementation file
//

#include "stdafx.h"
#include "Bottle.h"
#include "MoveHead.h"
#include "Serial.h"
#include "mainfrm.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// MoveHead dialog


MoveHead::MoveHead(CWnd* pParent /*=NULL*/)
	: CDialog(MoveHead::IDD, pParent)
{
	//{{AFX_DATA_INIT(MoveHead)
	m_mpos = _T("");
	m_mpos2 = _T("");
	//}}AFX_DATA_INIT
}


void MoveHead::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(MoveHead)
	DDX_Text(pDX, IDC_EDITPOS, m_mpos);
	DDX_Text(pDX, IDC_EDITPOS2, m_mpos2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(MoveHead, CDialog)
	//{{AFX_MSG_MAP(MoveHead)
	ON_BN_CLICKED(IDC_START, OnStart)
	ON_BN_CLICKED(IDC_STOP, OnStop)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// MoveHead message handlers

BOOL MoveHead::OnInitDialog() 
{
	CDialog::OnInitDialog();
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pmovehead=this;
	theapp=(CBottleApp*)AfxGetApp();
	
	//target 
	pframe->MotPos=theapp->jobinfo[pframe->CurrentJobNum].motPos;
	
	MotorPos = 0;

	if(theapp->serPresent)
	{
		//current position coming from serial
		MotorPos=pframe->m_pserial->MotorPos;

		if(MotorPos<=0)	
		{MotorPos=pframe->m_pserial->MotorPos=0;}
	}
	
	m_mpos.Format("%i",MotorPos);
	m_mpos2.Format("%i",pframe->MotPos);
	
	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void MoveHead::OnStart() 
{
	count=0;
	SetTimer(1,1500,NULL);
	
	if(theapp->serPresent)	{MotorPos=pframe->m_pserial->MotorPos;}

	if(MotorPos==0)					{OnHighSlow();}
	if(MotorPos>pframe->MotPos)		{OnHighSlow();}
	if(MotorPos<pframe->MotPos)		{OnLowSlow();}
	if(MotorPos==pframe->MotPos)	{KillTimer(1);}

	m_mpos.Format("%i",MotorPos);
	m_mpos2.Format("%i",pframe->MotPos);

	UpdateData(false);
}

void MoveHead::CheckMotorPos()
{
	if(theapp->serPresent)	{MotorPos=pframe->m_pserial->MotorPos;}

	if(MotorPos==0)								{OnHighSlow();}
	else
	{
		if(MotorPos > pframe->MotPos)
		{ 
			if(MotorPos-10 > pframe->MotPos)	{OnHigh();}
			else								{OnHighSlow();} 
		}

		if(MotorPos < pframe->MotPos)
		{ 
			if(MotorPos+10 < pframe->MotPos)	{OnLow();}
			else								{OnLowSlow();} 
		}
	}

	if(MotorPos  == pframe->MotPos)				{KillTimer(1);}
	if(count>=60)								{KillTimer(1);}
	
	count+=1;
	m_mpos.Format("%i",MotorPos );
	m_mpos2.Format("%i",pframe->MotPos);
	
	UpdateData(false);
}

void MoveHead::OnStop() 
{ 
	KillTimer(1); 
}

void MoveHead::OnHigh() 
{
	if(theapp->serPresent)	{pframe->m_pserial->SendChar(0,240);}

	SetTimer(3,200,NULL);
	UpdateData(false);
}

void MoveHead::OnLow() 
{
	if(theapp->serPresent)	{pframe->m_pserial->SendChar(0,250);}

	SetTimer(3,200,NULL);
	UpdateData(false);
}

void MoveHead::OnHighSlow() 
{
	if(theapp->serPresent)	{pframe->m_pserial->SendChar(0,130);}
	
	SetTimer(3,200,NULL);//going up
	UpdateData(false);
}

void MoveHead::OnLowSlow() 
{
	if(theapp->serPresent)	{pframe->m_pserial->SendChar(0,140);}
	
	SetTimer(3,200,NULL);
	UpdateData(false);
}

void MoveHead::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)			{CheckMotorPos();}
	if(nIDEvent==3)			{KillTimer(3);	SetTimer(5,300,NULL);}
	if(nIDEvent==5)
	{
		KillTimer(5);
		if(theapp->serPresent)	{MotorPos=pframe->m_pserial->MotorPos;}
		if(MotorPos==0)				{SetTimer(5,5,NULL);}

		m_mpos.Format("%i",MotorPos);
		pframe->SendMessage(WM_TRIGGER,NULL,NULL);
		UpdateData(false);
	}

	CDialog::OnTimer(nIDEvent);
}

void MoveHead::OnOK() 
{
	if(theapp->serPresent)	{theapp->jobinfo[pframe->CurrentJobNum].motPos = pframe->m_pserial->MotorPos;}

	pframe->SaveJob(pframe->CurrentJobNum);

	CDialog::OnOK();
}
