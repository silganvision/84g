#if !defined(AFX_XVIEW_H__B53C1148_EA35_48BE_B74F_2F722C22A6CB__INCLUDED_)
#define AFX_XVIEW_H__B53C1148_EA35_48BE_B74F_2F722C22A6CB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// xview.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// xview view

class xview : public CView
{
protected:
	xview();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(xview)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(xview)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~xview();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(xview)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XVIEW_H__B53C1148_EA35_48BE_B74F_2F722C22A6CB__INCLUDED_)
