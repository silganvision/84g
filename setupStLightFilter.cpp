// setupStLightFilter.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "setupStLightFilter.h"
#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// setupStLightFilter dialog


setupStLightFilter::setupStLightFilter(CWnd* pParent /*=NULL*/)
	: CDialog(setupStLightFilter::IDD, pParent)
{
	//{{AFX_DATA_INIT(setupStLightFilter)
	m_senFilterLigh = _T("");
	//}}AFX_DATA_INIT
}

void setupStLightFilter::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(setupStLightFilter)
	DDX_Control(pDX, ID_STEP, m_step);
	DDX_Text(pDX, IDC_SENLIGHT, m_senFilterLigh);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(setupStLightFilter, CDialog)
	//{{AFX_MSG_MAP(setupStLightFilter)
	ON_BN_CLICKED(IDC_USEIMGST1, OnUseimgst1)
	ON_BN_CLICKED(IDC_USEIMGST2, OnUseimgst2)
	ON_BN_CLICKED(IDC_USEIMGST3, OnUseimgst3)
	ON_BN_CLICKED(IDC_USEIMGST4, OnUseimgst4)
	ON_BN_CLICKED(IDC_USEIMGST5, OnUseimgst5)
	ON_BN_CLICKED(IDC_USEIMGST6, OnUseimgst6)
	ON_WM_TIMER()
	ON_BN_CLICKED(ID_STEP, OnStep)
	ON_BN_CLICKED(IDC_SENLIGHTUP, OnSenlightup)
	ON_BN_CLICKED(IDC_SENLIGHTDW, OnSenlightdw)
	ON_BN_CLICKED(IDC_USEIMGST7, OnUseimgst7)
	ON_BN_CLICKED(IDC_USEIMGST8, OnUseimgst8)
	ON_BN_CLICKED(IDC_USEIMGST9, OnUseimgst9)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// setupStLightFilter message handlers

void setupStLightFilter::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

void setupStLightFilter::OnUseimgst1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stLighImg = 1;
	UpdateValues();			
}

void setupStLightFilter::OnUseimgst2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stLighImg = 2;
	UpdateValues();			
}

void setupStLightFilter::OnUseimgst3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stLighImg = 3;
	UpdateValues();			
}

void setupStLightFilter::OnUseimgst4() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stLighImg = 4;
	UpdateValues();			
}

void setupStLightFilter::OnUseimgst5() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stLighImg = 5;
	UpdateValues();			
}

void setupStLightFilter::OnUseimgst6() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stLighImg = 6;
	UpdateValues();			
}

void setupStLightFilter::OnUseimgst7() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stLighImg = 7;
	UpdateValues();			
}

void setupStLightFilter::OnUseimgst8() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stLighImg = 8;
	UpdateValues();			
}


BOOL setupStLightFilter::OnInitDialog() 
{
	CDialog::OnInitDialog();
		
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_psetLighF=this;
	theapp=(CBottleApp*)AfxGetApp();

	stepSlow=true;
	resSen=2;
	UpdateDisplay();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void setupStLightFilter::UpdateValues()
{
	SetTimer(1,300,NULL);
	UpdateDisplay();

	pframe->m_pxaxis->InvalidateRect(NULL,false);
	pframe->m_pxaxis->UpdateData(false);
}

void setupStLightFilter::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{KillTimer(1); pframe->SaveJob(pframe->CurrentJobNum);}

	CDialog::OnTimer(nIDEvent);
}

void setupStLightFilter::UpdateDisplay()
{
	m_senFilterLigh.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].stLighSen);

	CheckDlgButton(IDC_USEIMGST1,0);
	CheckDlgButton(IDC_USEIMGST2,0);
	CheckDlgButton(IDC_USEIMGST3,0);
	CheckDlgButton(IDC_USEIMGST4,0);
	CheckDlgButton(IDC_USEIMGST5,0);
	CheckDlgButton(IDC_USEIMGST6,0);
	CheckDlgButton(IDC_USEIMGST7,0);
	CheckDlgButton(IDC_USEIMGST8,0);
	CheckDlgButton(IDC_USEIMGST9,0);
	
	if(theapp->jobinfo[pframe->CurrentJobNum].stLighImg==1)	{CheckDlgButton(IDC_USEIMGST1,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stLighImg==2)	{CheckDlgButton(IDC_USEIMGST2,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stLighImg==3)	{CheckDlgButton(IDC_USEIMGST3,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stLighImg==4)	{CheckDlgButton(IDC_USEIMGST4,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stLighImg==5)	{CheckDlgButton(IDC_USEIMGST5,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stLighImg==6)	{CheckDlgButton(IDC_USEIMGST6,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stLighImg==7)	{CheckDlgButton(IDC_USEIMGST7,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stLighImg==8)	{CheckDlgButton(IDC_USEIMGST8,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].stLighImg==9)	{CheckDlgButton(IDC_USEIMGST9,1);}

	UpdateData(false);
}

void setupStLightFilter::OnStep() 
{
	if(stepSlow)	
	{
		stepSlow=false;m_step.SetWindowText("Fast"); 
		CheckDlgButton(ID_STEP,1);
		resSen=10;
	}
	else		
	{
		stepSlow=true;	m_step.SetWindowText("Slow"); 
		CheckDlgButton(ID_STEP,0);
		resSen=2;
	}
	
	UpdateData(false);			
}	


void setupStLightFilter::OnSenlightup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stLighSen+=resSen;

	if(theapp->jobinfo[pframe->CurrentJobNum].stLighSen>=255)
	{theapp->jobinfo[pframe->CurrentJobNum].stLighSen=255;}

	UpdateValues();			
}

void setupStLightFilter::OnSenlightdw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stLighSen-=resSen;

	if(theapp->jobinfo[pframe->CurrentJobNum].stLighSen<=1)
	{theapp->jobinfo[pframe->CurrentJobNum].stLighSen=1;}

	UpdateValues();		
}


void setupStLightFilter::OnUseimgst9() 
{
	theapp->jobinfo[pframe->CurrentJobNum].stLighImg = 9;
	UpdateValues();			
	
}
