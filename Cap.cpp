// Label.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "cap.h"
#include "mainfrm.h"
#include "xaxisview.h"
#include "labelcolor.h"
#include "bottleview.h"
#include "camera.h"
#include "MiddleSetup.h"
#include "CapSetup1.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CCap dialog


CCap::CCap(CWnd* pParent /*=NULL*/)
	: CDialog(CCap::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCap)
	m_editcorner = _T("");
	m_editneck = _T("");
	m_editfsize = _T("");
	m_editfsize2 = _T("");
	m_editcsize = _T("");
	m_editcoffset = _T("");
	m_editcsen = _T("");
	m_editcsen2 = _T("");
	m_picNum = _T("");
	m_neckSen = _T("");
	//}}AFX_DATA_INIT
}


void CCap::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCap)
	DDX_Text(pDX, IDC_EDITCORNER, m_editcorner);
	DDX_Text(pDX, IDC_EDITNECK, m_editneck);
	DDX_Text(pDX, IDC_EDITFSIZE, m_editfsize);
	DDX_Text(pDX, IDC_EDITFSIZE2, m_editfsize2);
	DDX_Text(pDX, IDC_EDITCSIZE, m_editcsize);
	DDX_Text(pDX, IDC_EDITCOFFSET, m_editcoffset);
	DDX_Text(pDX, IDC_EDITCSEN, m_editcsen);
	DDX_Text(pDX, IDC_EDITCSEN2, m_editcsen2);
	DDX_Text(pDX, IDC_PICTOSHOW, m_picNum);
	DDX_Text(pDX, IDC_SENNECK, m_neckSen);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCap, CDialog)
	//{{AFX_MSG_MAP(CCap)
	ON_BN_CLICKED(IDC_BUTTONRETEACH, OnButtonreteach)
	ON_BN_CLICKED(IDC_COLORTARG2, OnColortarg2)
	ON_BN_CLICKED(IDC_COLORUP, OnColorup)
	ON_BN_CLICKED(IDC_COLORDN, OnColordn)
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_TEACH, OnTeach)
	ON_BN_CLICKED(IDC_CORNERLEFT, OnCornerleft)
	ON_BN_CLICKED(IDC_CORNERRIGHT, OnCornerright)
	ON_BN_CLICKED(IDC_NECKUP, OnNeckup)
	ON_BN_CLICKED(IDC_NECKDN, OnNeckdn)
	ON_BN_CLICKED(IDC_FILLLARGE, OnFilllarge)
	ON_BN_CLICKED(IDC_FILLSMALLER, OnFillsmaller)
	ON_BN_CLICKED(IDC_FILLUP, OnFillup)
	ON_BN_CLICKED(IDC_FILLDN, OnFilldn)
	ON_BN_CLICKED(IDC_TEACHFILL, OnTeachfill)
	ON_BN_CLICKED(IDC_CAPSMALLER, OnCapsmaller)
	ON_BN_CLICKED(IDC_CAPLARGER, OnCaplarger)
	ON_BN_CLICKED(IDC_SENMORE, OnSenmore)
	ON_BN_CLICKED(IDC_SENLESS, OnSenless)
	ON_BN_CLICKED(IDC_SENMORE2, OnSenmore2)
	ON_BN_CLICKED(IDC_SENLESS2, OnSenless2)
	ON_BN_CLICKED(IDC_MIDDLESETUP, OnMiddlesetup)
	ON_BN_CLICKED(IDC_PICTOSHOWUP, OnPictoshowup)
	ON_BN_CLICKED(IDC_PICTOSHOWDN, OnPictoshowdn)
	ON_BN_CLICKED(IDC_ENAPICSTOSHOW, OnEnapicstoshow)
	ON_BN_CLICKED(IDC_SENNECKDW, OnSenneckdw)
	ON_BN_CLICKED(IDC_SENNECKUP, OnSenneckup)
	ON_BN_CLICKED(IDC_SHOWCAP_FIL1, OnShowcapFil1)
	ON_BN_CLICKED(IDC_SHOWCAP_FIL2, OnShowcapFil2)
	ON_BN_CLICKED(IDC_SHOWCAP_FIL3, OnShowcapFil3)
	ON_BN_CLICKED(IDC_SHOWCAP_FIL4, OnShowcapFil4)
	ON_BN_CLICKED(IDC_SETUPCAP1, OnSetupcap1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCap message handlers


void CCap::OnButtonreteach() 
{
	pframe->m_pxaxis->reteachColorCap=true;
	theapp->jobinfo[pframe->CurrentJobNum].taught=true;
	//newTargetCap=true;
	InvalidateRect(NULL,false);	
}

void CCap::OnColortarg2() 
{
	LabelColor labelcolor;
	labelcolor.DoModal();
}

void CCap::OnColorup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].capColorOffset+=1;
	m_editcoffset.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].capColorOffset);
	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pxaxis->InvalidateRect(NULL,false); 	
}

void CCap::OnColordn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].capColorOffset-=1;
	m_editcoffset.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].capColorOffset);
	
	SetTimer(1,3000,NULL);
	
	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pxaxis->InvalidateRect(NULL,false); 
}

BOOL CCap::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pcap=this;
	theapp=(CBottleApp*)AfxGetApp();
	res=2;	
	newTarget=false;
	once=true;	

	


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCap::UpdateDisplay()
{
	if(newTarget)	{newTarget=false;}

	m_editcsize.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].capColorSize);
	m_editcoffset.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].capColorOffset);
	m_editcorner.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].vertBarX);
	m_editneck.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckBarY);
	m_editfsize2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].sportPos);
	m_editfsize.Format("%3i",100-theapp->jobinfo[pframe->CurrentJobNum].sportSen);
	m_editcsen.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].capStrength);
	m_editcsen2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].capStrength2);

	m_neckSen.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckStrength);

	m_picNum.Format("%i", pframe->pictoShow);

	CheckDlgButton(IDC_ENAPICSTOSHOW,theapp->enabPicsToShow ? 1 : 0);
	///////////////////////
	CheckDlgButton(IDC_SHOWCAP_FIL1,pframe->showCapBWImg ? 1 : 0);
	CheckDlgButton(IDC_SHOWCAP_FIL2,pframe->showCapBWRedImg ? 1 : 0);
	CheckDlgButton(IDC_SHOWCAP_FIL3,pframe->showCapBWGreenImg ? 1 : 0);
	CheckDlgButton(IDC_SHOWCAP_FIL4,pframe->showCapBWBlueImg ? 1 : 0);

	///////////////////////

	UpdateData(false);
	InvalidateRect(NULL,false);
}

void CCap::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{
		KillTimer(1);
		pframe->SaveJob(pframe->CurrentJobNum);
	}
	
	CDialog::OnTimer(nIDEvent);
}

void CCap::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	COLORREF capcolor;
	CBrush SavedCap;

	capcolor= pframe->m_pxaxis->ColorTarget2=theapp->savedColor2;
	SavedCap.CreateSolidBrush(capcolor);

	CRect caprect;
	caprect.top=27;//45
	caprect.left=400;//493
	caprect.bottom=caprect.top+20;
	caprect.right=caprect.left+40;

	dc.FillRect(caprect,&SavedCap);
	
	if(once)
	{
		bitmapl.CreateCompatibleBitmap(&dc, 28,28); 
		bitmapr.CreateCompatibleBitmap(&dc, 28,28);
		once=false; 
	}

	bitmapl.SetBitmapBits(28 * 28 *4 , pframe->m_pxaxis->im_patl2);
	bitmapr.SetBitmapBits(28 * 28 *4 , pframe->m_pxaxis->im_patr2x);

	CDC templ; templ.CreateCompatibleDC(0);
	CDC tempr; tempr.CreateCompatibleDC(0);

	BITMAP bmpl;
	BITMAP bmpr;

	CBitmap *old =templ.SelectObject(&bitmapl);
	::GetObject(bitmapl, sizeof(BITMAP), &bmpl);
	dc.StretchBlt(140,10,36,36, &templ,0,0,28,28,SRCCOPY);

	CBitmap *oldr =tempr.SelectObject(&bitmapr);
	::GetObject(bitmapr, sizeof(BITMAP), &bmpr);
	dc.StretchBlt(180,10,36,36, &tempr,0,0,28,28,SRCCOPY);
}

void CCap::OnTeach() 
{
	pframe=(CMainFrame*)AfxGetMainWnd();

	///////////////////////////

	int filterSelPattCap =	theapp->jobinfo[pframe->CurrentJobNum].filterSelPattCap;

	if(filterSelPattCap==2)			{byte5ForPattMatchDiv3 = pframe->m_pxaxis->im_cam5Rbw;}
	else if(filterSelPattCap==3)	{byte5ForPattMatchDiv3 = pframe->m_pxaxis->im_cam5Gbw;}
	else if(filterSelPattCap==4)	{byte5ForPattMatchDiv3 = pframe->m_pxaxis->im_cam5Bbw;}
	else							{byte5ForPattMatchDiv3 = pframe->m_pxaxis->im_cambw5;}

	///////////////////////////

	SinglePoint CapLSide=pframe->m_pxaxis->FindCapSide(byte5ForPattMatchDiv3,theapp->jobinfo[pframe->CurrentJobNum].neckBarY,true);
	SinglePoint CapRSide=pframe->m_pxaxis->FindCapSide(byte5ForPattMatchDiv3,theapp->jobinfo[pframe->CurrentJobNum].neckBarY,false);

	theapp->jobinfo[pframe->CurrentJobNum].lDist=theapp->jobinfo[pframe->CurrentJobNum].vertBarX-CapLSide.x;

	if(theapp->jobinfo[pframe->CurrentJobNum].lDist>=40)
	{theapp->jobinfo[pframe->CurrentJobNum].lDist=40;}
	
	if(theapp->jobinfo[pframe->CurrentJobNum].lDist<=0)
	{theapp->jobinfo[pframe->CurrentJobNum].lDist=0;}

	pframe->m_pxaxis->LearnDone=false;
		
	pframe->m_pxaxis->LearnPattern(byte5ForPattMatchDiv3, CapLSide, true);
	pframe->m_pxaxis->LearnPattern(byte5ForPattMatchDiv3, CapRSide, false);

	theapp->jobinfo[pframe->CurrentJobNum].taught=true;

	pframe->m_pview->m_status="Taught Success!";
	pframe->m_pview->UpdateDisplay();
	pframe->WasSetup=true;
	pframe->CountEnable=true;

	InvalidateRect(NULL,false);	
}

void CCap::OnCornerleft() 
{
	theapp->jobinfo[pframe->CurrentJobNum].vertBarX-=res;
	m_editcorner.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].vertBarX);

	theapp->jobinfo[pframe->CurrentJobNum].lDist-=res;

	SetTimer(1,3000,NULL);

	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pxaxis->InvalidateRect(NULL,false); 
}

void CCap::OnCornerright() 
{
	theapp->jobinfo[pframe->CurrentJobNum].vertBarX+=res;
	m_editcorner.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].vertBarX);

	theapp->jobinfo[pframe->CurrentJobNum].lDist+=res;

	SetTimer(1,3000,NULL);

	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pxaxis->InvalidateRect(NULL,false); 
}

void CCap::OnNeckup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckBarY-=res;
	m_editneck.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckBarY);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pxaxis->InvalidateRect(NULL,false); 
}

void CCap::OnNeckdn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckBarY+=res;
	m_editneck.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckBarY);
	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pxaxis->InvalidateRect(NULL,false); 
}

void CCap::OnFilllarge() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sportSen-=res;
	m_editfsize.Format("%3i",100-theapp->jobinfo[pframe->CurrentJobNum].sportSen);
	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pxaxis->InvalidateRect(NULL,false); 
}

void CCap::OnFillsmaller() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sportSen+=res;
	m_editfsize.Format("%3i",100-theapp->jobinfo[pframe->CurrentJobNum].sportSen);
	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pxaxis->InvalidateRect(NULL,false); 
}

void CCap::OnFillup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sportPos-=1;
	m_editfsize2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].sportPos);
	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pxaxis->InvalidateRect(NULL,false); 
}

void CCap::OnFilldn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sportPos+=1;
	m_editfsize2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].sportPos);
	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pxaxis->InvalidateRect(NULL,false); 	
}

void CCap::OnTeachfill() 
{
	pframe->m_pxaxis->reteachColor=true;
	newTarget=true;
	theapp->jobinfo[pframe->CurrentJobNum].taught=true;
	InvalidateRect(NULL,false);
}

void CCap::OnCapsmaller() 
{
	theapp->jobinfo[pframe->CurrentJobNum].capColorSize-=res;
	m_editcsize.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].capColorSize);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pxaxis->InvalidateRect(NULL,false); 
}

void CCap::OnCaplarger() 
{
	theapp->jobinfo[pframe->CurrentJobNum].capColorSize+=res;
	m_editcsize.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].capColorSize);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pxaxis->InvalidateRect(NULL,false); 
}

void CCap::OnSenmore() 
{
	theapp->jobinfo[pframe->CurrentJobNum].capStrength+=res;
	m_editcsen.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].capStrength);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
}

void CCap::OnSenless() 
{
	theapp->jobinfo[pframe->CurrentJobNum].capStrength-=res;
	m_editcsen.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].capStrength);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
}

void CCap::OnSenmore2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].capStrength2+=res;
	m_editcsen2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].capStrength2);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
}

void CCap::OnSenless2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].capStrength2-=res;
	m_editcsen2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].capStrength2);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
}



void CCap::OnMiddlesetup() 
{
	MiddleSetup middleSetup;
	middleSetup.DoModal();	
}

void CCap::OnPictoshowup() 
{
	pframe->pictoShow++;	

	if(pframe->pictoShow>=49)	{pframe->pictoShow=49;}

	UpdateDisplay();	
}

void CCap::OnPictoshowdn() 
{
	pframe->pictoShow--;	

	if(pframe->pictoShow<=1) pframe->pictoShow=1;
	
	UpdateDisplay();	
}

void CCap::OnEnapicstoshow() 
{
	CheckDlgButton(IDC_ENAPICSTOSHOW,theapp->enabPicsToShow ? 0 : 1); 
	theapp->enabPicsToShow = !theapp->enabPicsToShow;
		
	UpdateData(false);		
}



void CCap::OnSenneckdw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckStrength-=res;
	m_neckSen.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckStrength);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

void CCap::OnSenneckup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckStrength+=res;
	m_neckSen.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckStrength);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
}

void CCap::setAllToFalse()
{
	pframe->showCapBWImg=false;
	pframe->showCapBWRedImg=false;
	pframe->showCapBWGreenImg=false;
	pframe->showCapBWBlueImg=false;
}

void CCap::OnShowcapFil1() 
{
	bool showSta = pframe->showCapBWImg;

	if(!showSta)	{setAllToFalse();}
	showSta = !showSta;

	pframe->showCapBWImg = showSta;
		
	UpdateValues();		
}

void CCap::OnShowcapFil2() 
{
	bool showSta = pframe->showCapBWRedImg;

	if(!showSta)	{setAllToFalse();}
	showSta = !showSta;

	pframe->showCapBWRedImg = showSta;
		
	UpdateValues();			
}

void CCap::OnShowcapFil3() 
{
	bool showSta = pframe->showCapBWGreenImg;

	if(!showSta)	{setAllToFalse();}
	showSta = !showSta;

	pframe->showCapBWGreenImg = showSta;
		
	UpdateValues();				
}

void CCap::OnShowcapFil4() 
{
	bool showSta = pframe->showCapBWBlueImg;

	if(!showSta)	{setAllToFalse();}
	showSta = !showSta;

	pframe->showCapBWBlueImg = showSta;
		
	UpdateValues();			
}

void CCap::UpdateValues()
{
	SetTimer(1,300,NULL);
	UpdateDisplay();

	pframe->m_pxaxis->InvalidateRect(NULL,false);
	pframe->m_pxaxis->UpdateData(false);
}

void CCap::OnSetupcap1() 
{
	//CCapSetup1 capSetup1;
	//capSetup1.DoModal();
	
	pframe->m_pcapSetup1->ShowWindow(SW_SHOW);
		//SetWindowPos(&wndTop,10,400,780,250,SWP_SHOWWINDOW | SWP_NOSIZE );

	pframe->m_pcapSetup1->UpdateDisplay();

	
}
