// MiddleSetup.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "MiddleSetup.h"
#include "mainfrm.h"
#include "xaxisview.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// MiddleSetup dialog


MiddleSetup::MiddleSetup(CWnd* pParent /*=NULL*/)
	: CDialog(MiddleSetup::IDD, pParent)
{
	//{{AFX_DATA_INIT(MiddleSetup)
	m_middleW1 = _T("");
	m_middleW2 = _T("");
	m_middleW3 = _T("");
	m_middleW4 = _T("");
	m_middleY1 = _T("");
	m_middleY2 = _T("");
	m_middleY3 = _T("");
	m_middleY4 = _T("");
	m_senMidd = _T("");
	//}}AFX_DATA_INIT
}


void MiddleSetup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(MiddleSetup)
	DDX_Control(pDX, IDC_FINEMOD, m_fineMode);
	DDX_Text(pDX, IDC_WIDTH1, m_middleW1);
	DDX_Text(pDX, IDC_WIDTH2, m_middleW2);
	DDX_Text(pDX, IDC_WIDTH3, m_middleW3);
	DDX_Text(pDX, IDC_WIDTH4, m_middleW4);
	DDX_Text(pDX, IDC_YREF1, m_middleY1);
	DDX_Text(pDX, IDC_YREF2, m_middleY2);
	DDX_Text(pDX, IDC_YREF3, m_middleY3);
	DDX_Text(pDX, IDC_YREF4, m_middleY4);
	DDX_Text(pDX, IDC_SENMIDD, m_senMidd);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(MiddleSetup, CDialog)
	//{{AFX_MSG_MAP(MiddleSetup)
	ON_BN_CLICKED(IDC_REFISLEFTC1, OnRefisleftc1)
	ON_BN_CLICKED(IDC_REFISLEFTC2, OnRefisleftc2)
	ON_BN_CLICKED(IDC_REFISLEFTC3, OnRefisleftc3)
	ON_BN_CLICKED(IDC_REFISLEFTC4, OnRefisleftc4)
	ON_BN_CLICKED(IDC_YREFUP1, OnYrefup1)
	ON_BN_CLICKED(IDC_YREFDW1, OnYrefdw1)
	ON_BN_CLICKED(IDC_YREFUP2, OnYrefup2)
	ON_BN_CLICKED(IDC_YREFUP3, OnYrefup3)
	ON_BN_CLICKED(IDC_YREFUP4, OnYrefup4)
	ON_BN_CLICKED(IDC_YREFDW2, OnYrefdw2)
	ON_BN_CLICKED(IDC_YREFDW3, OnYrefdw3)
	ON_BN_CLICKED(IDC_YREFDW4, OnYrefdw4)
	ON_BN_CLICKED(IDC_WIDTHUP1, OnWidthup1)
	ON_BN_CLICKED(IDC_WIDTHDW1, OnWidthdw1)
	ON_BN_CLICKED(IDC_WIDTHUP2, OnWidthup2)
	ON_BN_CLICKED(IDC_WIDTHUP3, OnWidthup3)
	ON_BN_CLICKED(IDC_WIDTHUP4, OnWidthup4)
	ON_BN_CLICKED(IDC_WIDTHDW2, OnWidthdw2)
	ON_BN_CLICKED(IDC_WIDTHDW3, OnWidthdw3)
	ON_BN_CLICKED(IDC_WIDTHDW4, OnWidthdw4)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_TEACHMIDDLES, OnTeachmiddles)
	ON_BN_CLICKED(IDC_SENMIDDUP, OnSenmiddup)
	ON_BN_CLICKED(IDC_SENMIDDDW, OnSenmidddw)
	ON_BN_CLICKED(IDC_FINEMOD, OnFinemod)
	ON_BN_CLICKED(IDC_USEIMG1, OnUseimg1)
	ON_BN_CLICKED(IDC_USEIMG2, OnUseimg2)
	ON_BN_CLICKED(IDC_USEIMG3, OnUseimg3)
	ON_BN_CLICKED(IDC_USEIMG4, OnUseimg4)
	ON_BN_CLICKED(IDC_USEIMG5, OnUseimg5)
	ON_BN_CLICKED(IDC_USEIMG6, OnUseimg6)
	ON_BN_CLICKED(IDC_USEIMG7, OnUseimg7)
	ON_BN_CLICKED(IDC_USEIMG8, OnUseimg8)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// MiddleSetup message handlers

void MiddleSetup::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}




void MiddleSetup::UpdateDisplay()
{
	if(theapp->jobinfo[pframe->CurrentJobNum].MiddImg==1)	{CheckDlgButton(IDC_USEIMG1,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].MiddImg==2)	{CheckDlgButton(IDC_USEIMG2,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].MiddImg==3)	{CheckDlgButton(IDC_USEIMG3,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].MiddImg==4)	{CheckDlgButton(IDC_USEIMG4,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].MiddImg==5)	{CheckDlgButton(IDC_USEIMG5,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].MiddImg==6)	{CheckDlgButton(IDC_USEIMG6,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].MiddImg==7)	{CheckDlgButton(IDC_USEIMG7,1);}
	if(theapp->jobinfo[pframe->CurrentJobNum].MiddImg==8)	{CheckDlgButton(IDC_USEIMG8,1);}

	////////////////////////////

	if(theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[1])
									{CheckDlgButton(IDC_REFISLEFTC1,1); }
	else							{CheckDlgButton(IDC_REFISLEFTC1,0); }

	if(theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[2])
									{CheckDlgButton(IDC_REFISLEFTC2,1); }
	else							{CheckDlgButton(IDC_REFISLEFTC2,0); }

	if(theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[3])
									{CheckDlgButton(IDC_REFISLEFTC3,1); }
	else							{CheckDlgButton(IDC_REFISLEFTC3,0); }

	if(theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[4])
									{CheckDlgButton(IDC_REFISLEFTC4,1); }
	else							{CheckDlgButton(IDC_REFISLEFTC4,0); }

	////////////////////////////

	m_middleW1.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[1]);
	m_middleW2.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[2]);
	m_middleW3.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[3]);
	m_middleW4.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[4]);

	m_middleY1.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[1]);
	m_middleY2.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[2]);
	m_middleY3.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[3]);
	m_middleY4.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[4]);

	m_senMidd.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].senMidd);

	InvalidateRect(NULL,false);	
	UpdateData(false);
}

BOOL MiddleSetup::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pmiddleSetup=this;
	theapp=(CBottleApp*)AfxGetApp();


	camHeight	=	theapp->camWidth[1]/3;
	camWidth	=	theapp->camHeight[1]/3;

	res			=	3;
	resSen		=	1;

	UpdateDisplay();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void MiddleSetup::OnRefisleftc1() 
{
	int ncam = 1;

	if(theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[ncam])
	{
		CheckDlgButton(IDC_REFISLEFTC1,0); 
		theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[ncam]=false;
	}
	else
	{
		CheckDlgButton(IDC_REFISLEFTC1,1);
		theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[ncam]=true;
	}

	UpdateDisplay();	
}

void MiddleSetup::OnRefisleftc2() 
{
	int ncam = 2;

	if(theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[ncam])
	{
		CheckDlgButton(IDC_REFISLEFTC2,0); 
		theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[ncam]=false;
	}
	else
	{
		CheckDlgButton(IDC_REFISLEFTC2,1);
		theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[ncam]=true;
	}

	UpdateDisplay();		
}

void MiddleSetup::OnRefisleftc3() 
{
	int ncam = 3;

	if(theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[ncam])
	{
		CheckDlgButton(IDC_REFISLEFTC3,0); 
		theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[ncam]=false;
	}
	else
	{
		CheckDlgButton(IDC_REFISLEFTC3,1);
		theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[ncam]=true;
	}

	UpdateDisplay();		
}

void MiddleSetup::OnRefisleftc4() 
{
	int ncam = 4;

	if(theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[ncam])
	{
		CheckDlgButton(IDC_REFISLEFTC4,0); 
		theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[ncam]=false;
	}
	else
	{
		CheckDlgButton(IDC_REFISLEFTC4,1);
		theapp->jobinfo[pframe->CurrentJobNum].RefForMiddleIsLeft[ncam]=true;
	}

	UpdateDisplay();		
}

void MiddleSetup::UpdateValues()
{
	SetTimer(1,500,NULL);
	UpdateDisplay();
	pframe->m_pxaxis->InvalidateRect(NULL,false);
}


void MiddleSetup::OnYrefup1() 
{
	int ncam = 1;
	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]=4;}

	UpdateValues();
}


void MiddleSetup::OnYrefup2() 
{
	int ncam = 2;
	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]=4;}

	UpdateValues();
}

void MiddleSetup::OnYrefup3() 
{
	int ncam = 3;
	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]=4;}

	UpdateValues();
}

void MiddleSetup::OnYrefup4() 
{
	int ncam = 4;
	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]<4)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]=4;}

	UpdateValues();	
}

void MiddleSetup::OnYrefdw1() 
{
	int ncam = 1;
	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]+=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]>camHeight)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]=camHeight-8;}

	UpdateValues();	
}

void MiddleSetup::OnYrefdw2() 
{
	int ncam = 2;
	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]+=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]>camHeight)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]=camHeight-8;}

	UpdateValues();	
}

void MiddleSetup::OnYrefdw3() 
{
	int ncam = 3;
	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]+=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]>camHeight)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]=camHeight-8;}

	UpdateValues();	
}

void MiddleSetup::OnYrefdw4() 
{
	int ncam = 4;
	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]+=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]>camHeight)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefY[ncam]=camHeight-8;}

	UpdateValues();	
}

void MiddleSetup::OnWidthup1() 
{
	int ncam = 1;

	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]+=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]>camWidth-20)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]=camWidth-20;}

	UpdateValues();	
}

void MiddleSetup::OnWidthup2() 
{
	int ncam = 2;

	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]+=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]>camWidth-20)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]=camWidth-20;}

	UpdateValues();		
}

void MiddleSetup::OnWidthup3() 
{
	int ncam = 3;

	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]+=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]>camWidth-20)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]=camWidth-20;}

	UpdateValues();		
}

void MiddleSetup::OnWidthup4() 
{
	int ncam = 4;

	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]+=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]>camWidth-20)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]=camWidth-20;}

	UpdateValues();		
}


void MiddleSetup::OnWidthdw1() 
{
	int ncam = 1;

	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]<camWidth/4)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]=camWidth/4;}

	UpdateValues();		
}


void MiddleSetup::OnWidthdw2() 
{
	int ncam = 2;

	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]<camWidth/4)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]=camWidth/4;}

	UpdateValues();			
}

void MiddleSetup::OnWidthdw3() 
{
	int ncam = 3;

	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]<camWidth/4)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]=camWidth/4;}

	UpdateValues();			
}

void MiddleSetup::OnWidthdw4() 
{
	int ncam = 4;

	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]-=res;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]<camWidth/4)
	{theapp->jobinfo[pframe->CurrentJobNum].taughtMiddleRefW[ncam]=camWidth/4;}

	UpdateValues();			
}


void MiddleSetup::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{KillTimer(1); pframe->SaveJob(pframe->CurrentJobNum);}

	CDialog::OnTimer(nIDEvent);
}


void MiddleSetup::OnTeachmiddles() 
{
	pframe->m_pxaxis->teachingMiddle=true;
	theapp->jobinfo[pframe->CurrentJobNum].taughtMiddle=false;

	for(int i=1; i<=5; i++)
	{pframe->m_pxaxis->countTeachingPics[i] = 0;}

	theapp->jobinfo[pframe->CurrentJobNum].taught=true;
}


void MiddleSetup::OnSenmiddup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].senMidd+=resSen;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].senMidd>250)
	{theapp->jobinfo[pframe->CurrentJobNum].senMidd=250;}

	UpdateValues();		
}

void MiddleSetup::OnSenmidddw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].senMidd-=resSen;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].senMidd<10)
	{theapp->jobinfo[pframe->CurrentJobNum].senMidd=10;}

	UpdateValues();			
}

void MiddleSetup::OnFinemod() 
{
	if(res==1)	{m_fineMode.SetWindowText("Fast"); CheckDlgButton(IDC_FINEMOD,1); res=10;	resSen=5;}
	else		{m_fineMode.SetWindowText("Slow"); CheckDlgButton(IDC_FINEMOD,0); res=1;	resSen=1;}
	
	UpdateData(false);		
}

void MiddleSetup::OnUseimg1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].MiddImg = 1;
	UpdateValues();			
}

void MiddleSetup::OnUseimg2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].MiddImg = 2;
	UpdateValues();			
}

void MiddleSetup::OnUseimg3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].MiddImg = 3;
	UpdateValues();			
}

void MiddleSetup::OnUseimg4() 
{
	theapp->jobinfo[pframe->CurrentJobNum].MiddImg = 4;
	UpdateValues();				
}

void MiddleSetup::OnUseimg5() 
{
	theapp->jobinfo[pframe->CurrentJobNum].MiddImg = 5;
	UpdateValues();				
}

void MiddleSetup::OnUseimg6() 
{
	theapp->jobinfo[pframe->CurrentJobNum].MiddImg = 6;
	UpdateValues();				
}

void MiddleSetup::OnUseimg7() 
{
	theapp->jobinfo[pframe->CurrentJobNum].MiddImg = 7;
	UpdateValues();		
}

void MiddleSetup::OnUseimg8() 
{
	theapp->jobinfo[pframe->CurrentJobNum].MiddImg = 8;
	UpdateValues();		
}
