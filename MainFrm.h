// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__FD711536_2C82_4F70_BF4C_2541D0DCE267__INCLUDED_)
#define AFX_MAINFRM_H__FD711536_2C82_4F70_BF4C_2541D0DCE267__INCLUDED_


//        #define _CRTDBG_MAP_ALLOC
  //      #include <stdlib.h>
    //    #include <crtdbg.h>


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxmt.h>
#include "resource.h"
#include "SystemEnums.h"
#include "Globals.h"

#include <atomic>

class CMainFrame : public CMDIFrameWnd
{

	friend class Serial;
	friend class CBottleView;
	friend class CBottleApp;
	friend class Job;
	friend class Setup;
	friend class CXAxisView;
	friend class CYAxisView;
	friend class Pics;
	friend class Insp;
	friend class Blowoff;
	friend class SDelay;
	friend class Data;
	friend class View;
	friend class CCap;
	friend class Security;
	friend class Light;
	friend class Modify;
	friend class Splice;
	friend class Motor;
	friend class Tech;
	friend class Lim;
	friend class SaveAs;
	friend class MoveHead;
	friend class Photoeye;
	friend class DigIO;
	friend class Database;
	friend class capCircle;
	friend class CCamera;
	friend class LabelColor;
	friend class Prompt;
	friend class C3WaySplitterFrame;
	friend class Neck;
	friend class Status;
	friend class Advanced;
	friend class MiddleSetup;
	friend class PicRecal;
	friend class BottleDown;
	friend class StitchSetup;
	friend class ShiftedLabel;
	friend class LabIntegSetup;
	friend class NoLabelSetup;
	friend class WaistInspection;
	friend class WaistInspection2;
	friend class LabelMatingSetup;
	friend class CWhiteBalance;
	friend class CCockedLabel; // copy from 51R84 -- Andrew Gawlik 2017-07-14

	friend class setupStDarkFilter;
	friend class setupStLightFilter;
	friend class setupStEdgeFilter;
	friend class Usb;
	friend class CCapSetup1;
	friend class SleeverHeadData;
	friend class CSysConfig;

	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// Attributes
public:
	int		imgShown;
	bool TeachPicC1;

	bool	gotPicC1;
	bool	gotPicC2;
	bool	gotPicC3;
	bool	gotPicC4;
	bool	gotPicC5;

	std::atomic<int> countC0;
	std::atomic<int> countC1;
	std::atomic<int> countC2;
	std::atomic<int> countC3;
	std::atomic<int> countC4;
	std::atomic<int> countC5;

	//////////////////////////////////

	int		screenSaverTimeOut;
	bool	flagScreenSaver;

	bool GetScreenSaverRunning();
	void ScreenSaverCheckAndRefresh();
	void StopScreenSaver();
	int	GetScreenSaverTimeout();

	//////////////////////////////////
	/////////

	int arrayA[42][11]; ///2 D array holding 41 sleever heads and 11 inspections

	int _arrayLocal[42][11]; ///Temporary 2 D array holding 41 sleever heads and 11 inspections

    int _sleeverHeadNumber,_inspectionNumber;
	int _counterSleever;

	bool sleeverHeadUnlocked;

	bool isValidated;

	int temp_sleever;
	int _PLCdecision;
int _counterTemp, TEMP_COUNTER;


	 CCriticalSection _criticalSection;
//	static UINT TestThreading( void* pparam );
	//////////

	int		seqComp;

	int		notFinished;
	int		sameImg;

	bool	missPic1;
	bool	missPic2;
	bool	missPic3;
	bool	missPic4;

	int timesThru;
	bool conRejectLatch;
	bool reloading;
	int gotNext;
	bool JobLoaded;

	bool	showMagentaImg;
	bool	showCyanImg;
	bool	showYellowImg;
	bool	showSaturatImg;
	bool	showSatura2Img;
	bool	showBWImg;
	bool	showBWRedImg;
	bool	showBWGreenImg;
	bool	showBWBlueImg;
	bool	showEdgeHImg;
	bool	showEdgeVImg;
	bool	showBinImg;
	bool	showEdgFinalImg;
	bool	showBinImgMetho5;

	bool	SSecretMenu;

	bool	showCapBWImg;
	bool	showCapBWRedImg;
	bool	showCapBWGreenImg;
	bool	showCapBWBlueImg;

	CComboBox	CameraListCombo;
	CComboBox	*m_pcameralist;
	CComboBox	FormatListCombo;
	CComboBox	ModeListCombo;
	CComboBox	FrateListCombo;

	int *m_pCrntFormat;
	int *m_pCrntMode;
	int *m_pCrntFrate;
	int *m_pTrigger;
	int CurrentLip;
	int CurrentU2Thresh2;
	CString ProfileCode;
	int got1;
	int got2;
	int got3;
	int got4;
	bool toggle;
	bool everyOther;
	int CurrentJoba5;
	int CurrentJoba4;
	int savedtotal;
	int conreject;
	int ConRejectLimit;
	int ConRejectLimit2;
	int IORejectLimit;
	bool IODebounce;
	bool IODone;
	int IORejects;
	bool IOReject;
	bool PicsOpen;
	bool ToggleCheckStatus;
	int LockUp;
	int Condition2;
	int Condition1;
	int NewTriggerCount;
	int OldTriggerCount;
	int TriggerCount;
	
	bool OnLine;
	
	bool SJob;
	bool SAdvanced;
	bool SDownBottle;

	bool FreezeReset;
	int MotPos;

	bool STech;
	bool Stop;
	bool JobNoGood;
	bool Freeze;
	bool SModify;
	bool SEject;
	bool SLimits;
	bool SSetup;
	bool SCam;
	bool Safe;
	bool WaitNext;
	
	float EjectRate;
	float BottleRate;
	int OldBottleCount;
	bool InspOpen;
	bool WasSetup;
	bool CountEnable;
	bool Faulted;
	bool debounce;
	int	DigitalData;
	int Rejects;
	int CurrentJobNum;
	int CurrentJob;

	void ResetTotals();
	void GotFault();
	void Init();
	void UpdatePLC();
	void CheckStatus();
	void PhotoEyeEnable(bool enabled);
	void GenerateFormatList(BOOL bRedisp);
	void GenerateModeList(BOOL bRedisp);
	void GenerateFrateList(BOOL bRedisp);
	bool DeleteCamList();
	void UpdatePLCData();
	void ResetTotals2();
	void DisableEHS();
	void SaveJob(int jobNum);
	bool MakeCamList();
	void GetJob(int JobNum);	
	void SetTrigger();
	void CleanUp();	

	bool SetWelchsEnable(bool welchsEnabled);
	bool SetTrackerEnable(bool trackerEnabled);
	bool SetWaistEnable(bool waistEnabled);
	bool SetAlignmentEnable(bool alignmentEnabled);
	bool SetMachineType(MachineType machineType);

	BottleDown  *m_pbotdn;
	PicRecal	*m_picrecall;
	CCap		*m_pcap;
	CCapSetup1	*m_pcapSetup1;
	Setup		*m_psetup;
	Job			*m_pjob;
	CBottleView	*m_pview;
	CXAxisView	*m_pxaxis;
	CYAxisView	*m_pyaxis;
	CMainFrame	*m_pform;
	CSysConfig	*m_sysconfigform;
	Serial		*m_pserial;
	SDelay		*m_psdelay;
	CBottleApp	*theapp;
	Pics		*m_ppics;
	Prompt		*m_pprompt;
	Insp		*m_pinsp;
	Blowoff		*m_pblowoff;
	Data		*m_pdata;
	View		*m_pview2;
	CWnd		*pParent;
	Advanced	*m_padvanced;
	Security	*m_psec;
	Light		*m_plight;
	Modify		*m_pmod;
	Splice		*m_psplice;
	Motor		*m_pmotor;
	Tech		*m_ptech;
	Lim			*m_plimit;
	SaveAs		*m_psaveas;
	MoveHead	*m_pmovehead;
	CCamera		*m_pcamera;
	capCircle	*m_pcircle;
	Photoeye	*m_pphoto;
	DigIO		*m_pdigio;
	Database	*m_pdatabase;
	LabelColor	*m_plabelcolor;
	Neck		*m_pneck;
	Status		*m_pstatus;
	MiddleSetup *m_pmiddleSetup;
	C3WaySplitterFrame* m_psplit;
	StitchSetup	*m_pstitchSetup;
	ShiftedLabel	*m_shiftedLabel;
	LabIntegSetup	*m_plabIntegSetup;
	NoLabelSetup	*m_pNoLabelSetup;
	WaistInspection *m_pWaistInspection;
	WaistInspection2 *m_pWaistInspection2;
	LabelMatingSetup *m_pLabelMatingSetup;
	CWhiteBalance* m_pwhtbal;


	setupStDarkFilter	*m_psetDarkF;
	setupStLightFilter	*m_psetLighF;
	setupStEdgeFilter	*m_psetEdgeF;
	Usb					*m_pusb;
	SleeverHeadData *m_pSleeverHeadData;

	CCockedLabel	*m_pCockedLabel; //copied from 51R85 Andrew Gawlik 2017-07-14
public:

	bool CopyDone;

	afx_msg LONG Inspect(UINT, LONG);
	afx_msg LONG Trigger(UINT, LONG);
	afx_msg LONG NewJob(UINT, LONG);
	afx_msg LONG ReLoad(UINT mp, LONG);
	afx_msg LONG SetLightLevel(UINT level, LONG);

	CString m_r1, m_r2, m_r3, m_r4, m_r5, m_r6, m_r7, m_r8, m_r11, m_r10, m_r9, m_r15;
	
	CFile DataFile;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	void PopulateArray();
	void Demo();
	void setSecurityFlagsOFF(bool status);
	void KillContinuousTrigger();
	int pictoShow;
	int picNum;
	int picDefNum;

	bool dataReady;
	int oldTotal;
	bool testData;
	int Rejectsx;
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSerial();
	afx_msg void OnJobchange();
	afx_msg void OnClose();
	afx_msg void OnSetup1();
	afx_msg void OnSetup2();
	afx_msg void OnPics();
	afx_msg void OnHist();
	afx_msg void OnEject();
	afx_msg void OnCapwidth();
	afx_msg void OnSecurity();
	afx_msg void OnLight();
	afx_msg void OnMotor();
	afx_msg void OnPhotoeye();
	afx_msg void OnDigio();
	afx_msg void OnData();
	afx_msg void OnSetup4();
	afx_msg void OnLogout();
	afx_msg void OnShowguide();
	afx_msg void OnAdvanced();
	afx_msg void OnRecalldefective();
	afx_msg void OnDownbottle();
	afx_msg void OnSystemConfig();
	afx_msg void OnCamerasettings();
	afx_msg void OnUsb();
	afx_msg void OnDataHeadtrackerdata();
	afx_msg void OnAdjustWhtBal();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__FD711536_2C82_4F70_BF4C_2541D0DCE267__INCLUDED_)
