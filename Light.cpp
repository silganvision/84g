// Light.cpp : implementation file
//

#include "stdafx.h"
#include "Bottle.h"
#include "Light.h"
#include "mainfrm.h"
#include "serial.h"
#include "camera.h"
#include "math.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Light dialog


Light::Light(CWnd* pParent /*=NULL*/)
	: CDialog(Light::IDD, pParent)
{
	//{{AFX_DATA_INIT(Light)
	m_lightval = _T("");
	m_lightDuration = _T("");
	m_ringDuration = _T("");
	//}}AFX_DATA_INIT
}


void Light::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Light)
	DDX_Control(pDX, IDC_SLIDER5, m_slide5);
	DDX_Control(pDX, IDC_SLIDER4, m_slide4);
	DDX_Control(pDX, IDC_SLIDER3, m_slide3);
	DDX_Control(pDX, IDC_SLIDER2, m_slide2);
	DDX_Control(pDX, IDC_SLIDER1, m_slide);
	DDX_Text(pDX, IDC_LIGHTVAL, m_lightval);
	DDX_Text(pDX, IDC_LIGHTDURATION, m_lightDuration);
	DDX_Text(pDX, IDC_RINGDURATION, m_ringDuration);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Light, CDialog)
	//{{AFX_MSG_MAP(Light)
	ON_BN_CLICKED(IDC_LIGHT, OnLight)
	ON_BN_CLICKED(IDC_DARK, OnDark)
	ON_BN_CLICKED(IDC_ALL, OnAll)
	ON_BN_CLICKED(IDC_FRONT, OnFront)
	ON_BN_CLICKED(IDC_LEFT, OnLeft)
	ON_BN_CLICKED(IDC_RIGHT, OnRight)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER1, OnReleasedcaptureSlider1)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER2, OnReleasedcaptureSlider2)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER3, OnReleasedcaptureSlider3)
	ON_BN_CLICKED(IDC_TOP, OnTop)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER4, OnReleasedcaptureSlider4)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER5, OnReleasedcaptureSlider5)
	ON_BN_CLICKED(IDC_CAM5, OnCam5)
	ON_BN_CLICKED(IDC_LIGHTDURATIONUP, OnLightdurationup)
	ON_BN_CLICKED(IDC_LIGHTDURATIONDW, OnLightdurationdw)
	ON_BN_CLICKED(IDC_RINGDURATIONUP, OnRingdurationup)
	ON_BN_CLICKED(IDC_RINGDURATIONDW, OnRingdurationdw)
	ON_BN_CLICKED(IDC_EXTRALIGHT, OnExtralight)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Light message handlers

BOOL Light::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*) AfxGetMainWnd();
	pframe->m_plight=this;
	theapp=(CBottleApp*)AfxGetApp();

	LightLevel1=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1)*4;
	LightLevel2=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel2)*4;
	LightLevel3=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel3)*4;
	LightLevel4=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel4)*4;
	LightLevel5=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel5)*4;
	
	m_lightval.Format("%3i",LightLevel1);

	m_slide.SetRange(0,100);
	m_slide.SetTicFreq(10);
	m_slide.SetPos(100-LightLevel1);

	m_slide2.SetRange(0,100);
	m_slide2.SetTicFreq(10);
	m_slide2.SetPos(100-LightLevel2);

	m_slide3.SetRange(0,100);
	m_slide3.SetTicFreq(10);
	m_slide3.SetPos(100-LightLevel3);

	m_slide4.SetRange(0,100);
	m_slide4.SetTicFreq(10);
	m_slide4.SetPos(100-LightLevel4);

	if(theapp->cam5Enable)
	{
		m_slide5.SetRange(0,100);
		m_slide5.SetTicFreq(10);
		m_slide5.SetPos(100-LightLevel5);
	}
	else
	{
		GetDlgItem(IDC_SLIDER5)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CAM5)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CAM5LABEL)->ShowWindow(SW_HIDE);
	}

	CheckDlgButton(IDC_ALL,1);
	CheckDlgButton(IDC_FRONT,0);
	CheckDlgButton(IDC_LEFT,0);
	CheckDlgButton(IDC_RIGHT,0);

	if(theapp->cam5Enable)
	{
		CheckDlgButton(IDC_CAM5,0);
	}

	Cams=AllCameras;

	m_lightDuration.Format("%3i",theapp->lightDuration);
	m_ringDuration.Format("%3i",theapp->ringDuration);

	if(theapp->XLightOn)
	{CheckDlgButton(IDC_EXTRALIGHT,1); m_lenable.Format("%s","Side Light On");}
	else
	{CheckDlgButton(IDC_EXTRALIGHT,0); m_lenable.Format("%s","Side Light Off");}

	bool useRing = theapp->jobinfo[pframe->CurrentJobNum].useRing;

	CheckDlgButton(IDC_EXTRALIGHT, useRing ? 1 : 0);

	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Light::OnLight() 
{
	switch(Cams)
	{
		case FrontCamera:
			pframe->m_pcamera->OnGmore();
			LightLevel1=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1)*4;
			m_lightval.Format("%3i",LightLevel1);
			m_slide.SetPos(100-LightLevel1);
			break;
		
		case LeftCamera:
			pframe->m_pcamera->OnGmore2();
			LightLevel2=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel2)*4;
			m_lightval.Format("%3i",LightLevel2);
			m_slide2.SetPos(100-LightLevel2);
			break;
		
		case RightCamera:
			pframe->m_pcamera->OnGmore3();
			LightLevel3=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel3)*4;
			m_lightval.Format("%3i",LightLevel3);
			m_slide3.SetPos(100-LightLevel3);
			break;
		
		case TopCamera:
			pframe->m_pcamera->OnGmore4();
			LightLevel4=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel4)*4;
			m_lightval.Format("%3i",LightLevel4);
			m_slide4.SetPos(100-LightLevel4);
			break;
		
		case AllCameras:
			pframe->m_pcamera->OnGmore();
			pframe->m_pcamera->OnGmore2();
			pframe->m_pcamera->OnGmore3();
			pframe->m_pcamera->OnGmore4();
			
			if(theapp->cam5Enable)
			{
				pframe->m_pcamera->OnGmore5();
			}

			LightLevel1=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1)*4;
			m_lightval.Format("%3i",LightLevel1);
			m_slide.SetPos(100-LightLevel1);

			LightLevel2=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel2)*4;
			m_slide2.SetPos(100-LightLevel2);

			LightLevel3=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel3)*4;
			m_slide3.SetPos(100-LightLevel3);

			LightLevel4=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel4)*4;
			m_slide4.SetPos(100-LightLevel4);

			if(theapp->cam5Enable)
			{
				LightLevel5=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel5)*4;
				m_slide5.SetPos(100-LightLevel5);
			}

			break;

		case FifthCamera:
			
			if(theapp->cam5Enable)
			{
				pframe->m_pcamera->OnGmore5();
				LightLevel5=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel5)*4;
				m_lightval.Format("%3i",LightLevel5);
				m_slide5.SetPos(100-LightLevel5);
			}

			break;
	}

	pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	UpdateData(false);
}

void Light::OnDark() 
{
	switch(Cams)
	{
		case FrontCamera:
			pframe->m_pcamera->OnGless();
			LightLevel1=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1)*4;
			m_lightval.Format("%3i",LightLevel1);
			m_slide.SetPos(100-LightLevel1);
			break;

		case LeftCamera:
			pframe->m_pcamera->OnGless2();
			LightLevel2=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel2)*4;
			m_lightval.Format("%3i",LightLevel2);
			m_slide2.SetPos(100-LightLevel2);
			break;
		
		case RightCamera:
			pframe->m_pcamera->OnGless3();
			LightLevel3=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel3)*4;
			m_lightval.Format("%3i",LightLevel3);
			m_slide3.SetPos(100-LightLevel3);
			break;

		case TopCamera:
			pframe->m_pcamera->OnGless4();
			LightLevel4=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel4)*4;
			m_lightval.Format("%3i",LightLevel4);
			m_slide4.SetPos(100-LightLevel4);
			break;

		case FifthCamera:
			if(theapp->cam5Enable)
			{
				pframe->m_pcamera->OnGless5();
				LightLevel5=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel5)*4;
				m_lightval.Format("%3i",LightLevel5);
				m_slide5.SetPos(100-LightLevel5);
			}

			break;

		case AllCameras:
			pframe->m_pcamera->OnGless();
			pframe->m_pcamera->OnGless2();
			pframe->m_pcamera->OnGless3();
			pframe->m_pcamera->OnGless4();

			if(theapp->cam5Enable)
			{
				pframe->m_pcamera->OnGless5();
			}

			LightLevel1=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1)*4;
			m_lightval.Format("%3i",LightLevel1);
			m_slide.SetPos(100-LightLevel1);

			LightLevel2=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel2)*4;
			m_slide2.SetPos(100-LightLevel2);

			LightLevel3=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel3)*4;
			m_slide3.SetPos(100-LightLevel3);

			LightLevel4=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel4)*4;
			m_slide4.SetPos(100-LightLevel4);

			if(theapp->cam5Enable)
			{
				LightLevel5=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel5)*4;
				m_slide5.SetPos(100-LightLevel5);
			}

			break;
	}

	pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	UpdateData(false);
}

void Light::OnOK() 
{
	pframe->SaveJob(pframe->CurrentJobNum);
	CDialog::OnOK();
}

void Light::OnCancel() 
{
	CDialog::OnCancel();
}


void Light::UpdatePLC()
{
	int Value=LightLevel1;
	int Function=0;
	int CntrBits=0;
	int ControlChars;
		
	Function=1;
	Function=Function *10;
	CntrBits=0;
	ControlChars=Value+Function+CntrBits;

	if(theapp->serPresent)	{pframe->m_pserial->SendChar(Value,Function);}
}

void Light::OnAll()		{Cams=AllCameras;}
void Light::OnFront()	{Cams=FrontCamera;}
void Light::OnLeft()	{Cams=LeftCamera;}
void Light::OnRight()	{Cams=RightCamera;}
void Light::OnTop()		{Cams=TopCamera;}
void Light::OnCam5()	{Cams=FifthCamera;}

void Light::OnReleasedcaptureSlider1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	double res=100-double(m_slide.GetPos());
	double dif=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1)*4-res;

	if(dif<0) 
	{
		for(double dist=fabs(dif); dist>0; dist-=1)
		{pframe->m_pcamera->OnGmore();}
	}
	else
	{
		for(double dist=fabs(dif); dist>0; dist-=1)
		{pframe->m_pcamera->OnGless();}
	}

	LightLevel1=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1)*4;
	m_lightval.Format("%3i",LightLevel1);
	
	pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	UpdateData(false);
	*pResult = 0;
}

void Light::OnReleasedcaptureSlider2(NMHDR* pNMHDR, LRESULT* pResult) 
{
	double res=100-double(m_slide2.GetPos());
	double dif=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel2)*4-res;

	if(dif<0) 
	{
		for(double dist=fabs(dif); dist>0; dist-=1)
		{pframe->m_pcamera->OnGmore2();}
	}
	else
	{
		for(double dist=fabs(dif); dist>0; dist-=1)
		{pframe->m_pcamera->OnGless2();}
	}

	LightLevel2=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel2)*4;
	m_lightval.Format("%3i",LightLevel2);
	
	pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	UpdateData(false);
	
	*pResult = 0;
}

void Light::OnReleasedcaptureSlider3(NMHDR* pNMHDR, LRESULT* pResult) 
{
	double res=100 - double(m_slide3.GetPos());
	double dif=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel3)*4-res;

	if(dif<0) 
	{
		for(double dist=fabs(dif); dist>0; dist-=1)
		{pframe->m_pcamera->OnGmore3();}
	}
	else
	{
		for(double dist=fabs(dif); dist>0; dist-=1)
		{pframe->m_pcamera->OnGless3();}
	}

	LightLevel3=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel3)*4;
	m_lightval.Format("%3i",LightLevel3);
	pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	UpdateData(false);
	
	*pResult = 0;
}

void Light::OnReleasedcaptureSlider4(NMHDR* pNMHDR, LRESULT* pResult) 
{
	double res=100 - double(m_slide4.GetPos());
	double dif=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel4)*4-res;

	if(dif<0) 
	{
		for(double dist=fabs(dif); dist>0; dist-=1)
		{pframe->m_pcamera->OnGmore4();}
	}
	else
	{
		for(double dist=fabs(dif); dist>0; dist-=1)
		{pframe->m_pcamera->OnGless4();}
	}

	LightLevel4=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel4)*4;
	m_lightval.Format("%3i",LightLevel4);
	pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	UpdateData(false);
	
	*pResult = 0;
}

void Light::OnReleasedcaptureSlider5(NMHDR* pNMHDR, LRESULT* pResult) 
{
	double res=100 - double(m_slide5.GetPos());
	double dif=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel5)*4-res;

	if(dif<0) 
	{
		for(double dist=fabs(dif); dist>0; dist-=1)
		{pframe->m_pcamera->OnGmore5();}
	}
	else
	{
		for(double dist=fabs(dif); dist>0; dist-=1)
		{pframe->m_pcamera->OnGless5();}
	}

	LightLevel5=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel5)*4;
	m_lightval.Format("%3i",LightLevel5);
	pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	UpdateData(false);
	
	*pResult = 0;
}



void Light::OnLightdurationup() 
{
	Value=theapp->lightDuration+=1;
	m_lightDuration.Format("%3i",theapp->lightDuration);

	Function=380;

	if(theapp->serPresent)	{pframe->m_pserial->SendChar(Value,Function);}

	UpdateData(false);	
}



void Light::OnLightdurationdw() 
{
	Value=theapp->lightDuration-=1;
	m_lightDuration.Format("%3i",theapp->lightDuration);

	Function=380;

	if(theapp->serPresent)	{pframe->m_pserial->SendChar(Value,Function);}

	UpdateData(false);		
}

void Light::OnRingdurationup() 
{
	Value=theapp->ringDuration+=1;
	m_ringDuration.Format("%3i",theapp->ringDuration);

	Function=370;

	if(theapp->serPresent)	{pframe->m_pserial->SendChar(Value,Function);}

	UpdateData(false);		
}

void Light::OnRingdurationdw() 
{
	Value=theapp->ringDuration-=1;
	m_ringDuration.Format("%3i",theapp->ringDuration);

	Function=370;

	if(theapp->serPresent)	{pframe->m_pserial->SendChar(Value,Function);}

	UpdateData(false);			
}

void Light::OnExtralight() 
{
	bool useRing = theapp->jobinfo[pframe->CurrentJobNum].useRing;

	useRing = !useRing;
	theapp->jobinfo[pframe->CurrentJobNum].useRing = useRing;

	/////////////
	//Send the signal

	if(theapp->serPresent)	{	pframe->m_pserial->SendChar(0,useRing ? 270 : 260); }
	UpdateDisplay();	
}

void Light::UpdateDisplay()
{
	bool useRing = theapp->jobinfo[pframe->CurrentJobNum].useRing;
	CheckDlgButton(IDC_EXTRALIGHT, useRing ? 1 : 0);

	UpdateData(false);		
}


