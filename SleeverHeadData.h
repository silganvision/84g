#if !defined(AFX_SLEEVERHEADDATA_H__1A738E57_19A1_49D0_847C_0F9061DE3409__INCLUDED_)
#define AFX_SLEEVERHEADDATA_H__1A738E57_19A1_49D0_847C_0F9061DE3409__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SleeverHeadData.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// SleeverHeadData dialog

class SleeverHeadData : public CDialog
{
	friend class CMainFrame;
	friend class CBottleApp;
	friend class XAxisView;
	// Construction
public:
	void PopulateListCtrl();
	SleeverHeadData(CWnd* pParent = NULL);   // standard constructor


	CMainFrame	*pframe;
	CBottleApp	*theapp;
	XAxisView	*m_pxaxis;

	int tempArr[41][11];
	bool repaint;

// Dialog Data
	//{{AFX_DATA(SleeverHeadData)
	enum { IDD = IDD_SleeverHeadData };
	CButton	m_enable;
	CListCtrl	m_list1control;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SleeverHeadData)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(SleeverHeadData)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnSave();
	afx_msg void OnReset();
	afx_msg void OnEnable();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SLEEVERHEADDATA_H__1A738E57_19A1_49D0_847C_0F9061DE3409__INCLUDED_)
