// Blowoff.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "Blowoff.h"
#include "mainfrm.h"
#include "serial.h"
#include "bottleview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Blowoff dialog


Blowoff::Blowoff(CWnd* pParent /*=NULL*/)
	: CDialog(Blowoff::IDD, pParent)
{
	//{{AFX_DATA_INIT(Blowoff)
	m_dist = _T("");
	m_dur = _T("");
	m_edithold = _T("");
	//}}AFX_DATA_INIT
}


void Blowoff::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Blowoff)
	DDX_Text(pDX, IDC_DIST, m_dist);
	DDX_Text(pDX, IDC_DUR, m_dur);
	DDX_Text(pDX, IDC_ENABLE, m_enable);
	DDX_Text(pDX, IDC_EDITHOLD, m_edithold);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Blowoff, CDialog)
	//{{AFX_MSG_MAP(Blowoff)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_MORE, OnMore)
	ON_BN_CLICKED(IDC_LESS, OnLess)
	ON_BN_CLICKED(IDC_M2, OnM2)
	ON_BN_CLICKED(IDC_L2, OnL2)
	ON_BN_CLICKED(IDC_MORE2, OnMore2)
	ON_BN_CLICKED(IDC_LESS2, OnLess2)
	ON_BN_CLICKED(IDC_ENABLE, OnEnable)
	ON_BN_CLICKED(IDC_RAISFINGDELAYMORE, OnRaisfingdelaymore)
	ON_BN_CLICKED(IDC_RAISFINGDELAYLESS, OnRaisfingdelayless)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Blowoff message handlers

BOOL Blowoff::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pblowoff=this;
	theapp=(CBottleApp*)AfxGetApp();

	//pframe->SetTimer(8,100,NULL);//com
	//pframe->OnLine=false;
	//pframe->m_pview->SetTimer(2,500,NULL);

	Value=0;
	Function=0;
	m_dist.Format("%3i",theapp->SavedEncDist);
	m_dur.Format("%3i",theapp->SavedEncDur);
	m_edithold.Format("%3i",theapp->holdTime1);

	//if(theapp->SavedEjectDisable){CheckDlgButton(IDC_ENABLE,1); m_enable.Format("%s","Eject is disabled");}else{CheckDlgButton(IDC_ENABLE,0);m_enable.Format("%s","Eject is enabled");}
	UpdateData(false);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Blowoff::OnMore() 
{
	Value=theapp->SavedEncDist+=1;
	m_dist.Format("%3i",theapp->SavedEncDist);

	Function=80;

	if(theapp->serPresent)	{pframe->m_pserial->SendChar(Value,Function);}

	UpdateData(false);
}

void Blowoff::OnM2() 
{
	Value=theapp->SavedEncDist+=20;
	m_dist.Format("%3i",theapp->SavedEncDist);

	Function=80;

	if(theapp->serPresent)	{pframe->m_pserial->SendChar(Value,Function);}
	
	UpdateData(false);
}

void Blowoff::OnLess() 
{
	Value=theapp->SavedEncDist-=1;
	m_dist.Format("%3i",theapp->SavedEncDist);

	if(Value<=0) Value=theapp->SavedEncDist=0;

	Function=80;
	if(theapp->serPresent)	{pframe->m_pserial->SendChar(Value,Function);}
	
	UpdateData(false);
}

void Blowoff::OnL2() 
{
	Value=theapp->SavedEncDist-=20;
	m_dist.Format("%3i",theapp->SavedEncDist);

	if(Value<=0) Value=theapp->SavedEncDist=0;

	Function=80;
	if(theapp->serPresent)	{pframe->m_pserial->SendChar(Value,Function);}
	
	UpdateData(false);
}

void Blowoff::OnMore2() 
{
	Value=theapp->SavedEncDur+=1;
	m_dur.Format("%3i",theapp->SavedEncDur);

	Function=90;

	if(theapp->serPresent)	{pframe->m_pserial->SendChar(Value,Function);}
	
	UpdateData(false);
}

void Blowoff::OnLess2() 
{
	Value=theapp->SavedEncDur-=1;
	m_dur.Format("%3i",theapp->SavedEncDur);
	
	Function=90;

	if(theapp->serPresent)	{pframe->m_pserial->SendChar(Value,Function);}
	
	UpdateData(false);
}

void Blowoff::OnEnable() 
{
	if( theapp->SavedEjectDisable )
	{
		CheckDlgButton(IDC_ENABLE,0); 
		Enable(false); 
		theapp->SavedEjectDisable=false; 
		m_enable.Format("%s","Eject is enabled");}
	else
	{ 
		CheckDlgButton(IDC_ENABLE,1);
		Enable(true); 
		theapp->SavedEjectDisable=true; 
		m_enable.Format("%s","Eject is disabled");
	}

	UpdateData(false);
}

void Blowoff::OnOK() 
{
	pframe->Stop=false;
	pframe->SaveJob(pframe->CurrentJobNum);
	
	CDialog::OnOK();
}

void Blowoff::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{
		KillTimer(1);

		Value=0; Function=0; CntrBits=0;
	
		if(theapp->serPresent)		{pframe->m_pserial->ControlChars = (Value + Function + CntrBits);}
		
		SetTimer(2,200,NULL);
	}

	if(nIDEvent==2)
	{
		KillTimer(2);
		pframe=(CMainFrame*)AfxGetMainWnd();
		pframe->Stop=false;
	}

	CDialog::OnTimer(nIDEvent);
}

void Blowoff::Enable(bool off)
{
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->Stop=true;
	/*
	if(theapp->serPresent)
	{
		if(off)
		{pframe->m_pserial->SendChar(0,100);}
		else
		{pframe->m_pserial->SendChar(0,110);}
	}
	*/
}

void Blowoff::OnRaisfingdelaymore() 
{
	Value=theapp->holdTime1+=1;
	m_edithold.Format("%3i",theapp->holdTime1);

	Function=800;

	if(theapp->serPresent)	{pframe->m_pserial->SendChar(Value,Function);}

	UpdateData(false);	
}

void Blowoff::OnRaisfingdelayless() 
{
	Value=theapp->holdTime1-=1;
	m_edithold.Format("%3i",theapp->holdTime1);

	Function=800;

	if(theapp->serPresent)	{pframe->m_pserial->SendChar(Value,Function);}

	UpdateData(false);
}
