// PicRecal.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "PicRecal.h"

#include "MainFrm.h"
#include "XAxisView.h"
#include <iostream>
#include <sstream>

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PicRecal dialog


PicRecal::PicRecal(CWnd* pParent /*=NULL*/)
	: CDialog(PicRecal::IDD, pParent)
{
	//{{AFX_DATA_INIT(PicRecal)
	m_countpics = _T("");
	m_edit1 = _T("");
	//}}AFX_DATA_INIT
}


void PicRecal::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PicRecal)
	DDX_Text(pDX, IDC_COUNT_PICS, m_countpics);
	DDX_Text(pDX, IDC_EDIT1, m_edit1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PicRecal, CDialog)
	//{{AFX_MSG_MAP(PicRecal)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_RESET, OnReset)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_CBN_SELENDOK(0x1448,OnSelection)
	ON_BN_CLICKED(IDC_SHOWC5, OnShowC5)

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PicRecal message handlers

BOOL PicRecal::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_picrecall=this;
	theapp=(CBottleApp*)AfxGetApp();

	pframe->m_pxaxis->showostore=1;

	m_pcombo = new CComboBox;

    m_pcombo->Create(WS_CHILD | WS_VISIBLE | WS_VSCROLL,
		   CRect(650, 70, 790, 400), this, 0x1448);

	for(int c=1; c<=4; c++)
	{w[c] = CroppedCameraHeight/3;		h[c] = CroppedCameraWidth/3;}

	w[5] = theapp->camWidth[5]/2;		h[5] = theapp->camHeight[5]/2;	

	//char abc1[100];

   /* for(int i=0;i<MAXNUMDEFPIC;i++)
	{
		stringstream ss (stringstream::in | stringstream::out);

		if(pframe->m_pxaxis->tim_stamp[i][0] !=0 )
		{
			ss << pframe->m_pxaxis->tim_stamp[i][1];
			ss<<":d";
			ss << pframe->m_pxaxis->tim_stamp[i][2];
			ss<<":";
			ss << pframe->m_pxaxis->tim_stamp[i][3];
			
			CString abc(ss.str().c_str());

			m_pcombo->AddString(abc);
		}	
	}*/

	for(int i=MAXNUMDEFPIC-1;i>=0;i--)
	{
		stringstream ss (stringstream::in | stringstream::out);

		if(pframe->m_pxaxis->tim_stamp[i][0] !=0 )
		{
			ss << pframe->m_pxaxis->tim_stamp[i][1];
			ss<<":";
			ss << pframe->m_pxaxis->tim_stamp[i][2];
			ss<<":";
			ss << pframe->m_pxaxis->tim_stamp[i][3];
			
			CString abc(ss.str().c_str());

			m_pcombo->AddString(abc);
		}	
	}

	count=0;reasnofailur=0;

	arr_strng_addrs[0]=pframe->m_pxaxis->imgBuf3RGB_cam1; //Keeping track of the last memory address retained by these pointers
	arr_strng_addrs[1]=pframe->m_pxaxis->imgBuf3RGB_cam2;
	arr_strng_addrs[2]=pframe->m_pxaxis->imgBuf3RGB_cam3;
	arr_strng_addrs[3]=pframe->m_pxaxis->imgBuf3RGB_cam4;
	arr_strng_addrs[4]=pframe->m_pxaxis->imgBuf3RGB_cam5;

	m_pcombo->SetCurSel(0); // Used to Initialise the Dialog to displaying the 1st Picture

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void PicRecal::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	CRect rect;
  
  	if(pframe->CurrentJobNum >=1)
	{
		count++;
		int nIndex = m_pcombo->GetCurSel();

		nIndex=pframe->m_pxaxis->imge_no-nIndex-1; //this is the revised INdex Number
		int nCount = m_pcombo->GetCount();

		if ((nIndex != CB_ERR) &&(nIndex >= 0) )
		{
			pframe->m_pxaxis->imgBuf3RGB_cam1=pframe->m_pxaxis->arr1[0]+(nIndex*(w[1]*h[1]*4));
			pframe->m_pxaxis->imgBuf3RGB_cam2=pframe->m_pxaxis->arr1[1]+(nIndex*(w[2]*h[2]*4));
			pframe->m_pxaxis->imgBuf3RGB_cam3=pframe->m_pxaxis->arr1[2]+(nIndex*(w[3]*h[3]*4));
			pframe->m_pxaxis->imgBuf3RGB_cam4=pframe->m_pxaxis->arr1[3]+(nIndex*(w[4]*h[4]*4));
			pframe->m_pxaxis->imgBuf3RGB_cam5=pframe->m_pxaxis->arr1[4]+(nIndex*(w[5]*h[5]*4));

			CVisRGBAByteImage im_FileBW3_cam1(w[1], h[1], 1, -1, pframe->m_pxaxis->imgBuf3RGB_cam1);
			CVisRGBAByteImage im_FileBW3_cam2(w[2], h[2], 1, -1, pframe->m_pxaxis->imgBuf3RGB_cam2);
			CVisRGBAByteImage im_FileBW3_cam3(w[3], h[3], 1, -1, pframe->m_pxaxis->imgBuf3RGB_cam3);
			CVisRGBAByteImage im_FileBW3_cam4(w[4], h[4], 1, -1, pframe->m_pxaxis->imgBuf3RGB_cam4);
			CVisRGBAByteImage im_FileBW3_cam5(w[5], h[5], 1, -1, pframe->m_pxaxis->imgBuf3RGB_cam5);

			reasnofailur=*(pframe->m_pxaxis->imgBuf3RGB_cam1);
			
//			*(pframe->m_pxaxis->imgBuf3RGB_cam1=255;
//			*(pframe->m_pxaxis->imgBuf3RGB_cam1=255;

			dc.SetWindowOrg(-5,-10);	//dc.SetWindowOrg(-60,0);
			CVisImageBase& refimage1=im_FileBW3_cam1;
			assert(refimage1.IsValid());	refimage1.DisplayInHdc(dc); 

			dc.SetWindowOrg(-w[1]-10,-10);
			CVisImageBase& refimage2=im_FileBW3_cam2;
			assert(refimage2.IsValid());	refimage2.DisplayInHdc(dc); 

			dc.SetWindowOrg(-2*w[1]-15,-10);
			CVisImageBase& refimage3=im_FileBW3_cam3;
			assert(refimage3.IsValid());	refimage3.DisplayInHdc(dc); 

			dc.SetWindowOrg(-3*w[1]-20,-10);
			CVisImageBase& refimage4=im_FileBW3_cam4;
			assert(refimage4.IsValid());	refimage4.DisplayInHdc(dc); 

			if(	theapp->jobinfo[pframe->CurrentJobNum].showC5inPictures)
			{
				dc.SetWindowOrg(-w[1]/8-10,-150);
				CVisImageBase& refimage5=im_FileBW3_cam5;
				assert(refimage5.IsValid());refimage5.DisplayInHdc(dc); 
			}
		}

		if(count==MAXNUMDEFPIC)	{count =0;}
	}
	
	UpdateDisplay();
}

void PicRecal::UpdateDisplay()
{
	m_countpics.Format("%2i",count);

	switch(reasnofailur)
	{
		case 1:m_edit1.Format("Cap L-R Height");break;
		case 2:
			if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)	{m_edit1.Format("Cap Sport");}
			else												{m_edit1.Format("Cap Mid Height");}			
			break;
		case 3:	m_edit1.Format("Cap Color");	break;
		case 4:	m_edit1.Format("LblNeck");		break;
		case 5:	m_edit1.Format("LblHeight");	break;
		case 6:	m_edit1.Format("LblRotate");	break;
		case 7:	m_edit1.Format("LblInteg");break;
		case 8:	m_edit1.Format("NoLabel");		break;
		case 9:	m_edit1.Format("Splice");		break;
		case 10:m_edit1.Format("TearLbl");		break;
		default:m_edit1.Format("----");			break;
	}

//	m_edit2.Format("%d : %d : %d ",*(pframe->m_pxaxis->imgBuf3RGB_cam1+2),*(pframe->m_pxaxis->imgBuf3RGB_cam1+4),*(pframe->m_pxaxis->imgBuf3RGB_cam1+6));

	if(theapp->jobinfo[pframe->CurrentJobNum].showC5inPictures)
	{CheckDlgButton(IDC_SHOWC5,1);}
	else {CheckDlgButton(IDC_SHOWC5,0);}

	UpdateData(false);
}

void PicRecal::OnOK() 
{
	OnClose();
	CDialog::OnOK();
}

void PicRecal::OnReset() 
{
	if(m_pcombo->GetCurSel() != CB_ERR)  //To Reset, 1st check if user selected something from the Drop Down
	{
	   	pframe->m_pxaxis->imgBuf3RGB_cam1=pframe->m_pxaxis->arr1[0];//storing the Default address in an array
		pframe->m_pxaxis->imgBuf3RGB_cam2=pframe->m_pxaxis->arr1[1];
		pframe->m_pxaxis->imgBuf3RGB_cam3=pframe->m_pxaxis->arr1[2];
		pframe->m_pxaxis->imgBuf3RGB_cam4=pframe->m_pxaxis->arr1[3];
		pframe->m_pxaxis->imgBuf3RGB_cam5=pframe->m_pxaxis->arr1[4];

		if(	(GlobalFree(pframe->m_pxaxis->imgBuf3RGB_cam1) == NULL) &&
			(GlobalFree(pframe->m_pxaxis->imgBuf3RGB_cam2) == NULL) && 
			(GlobalFree(pframe->m_pxaxis->imgBuf3RGB_cam3) == NULL) &&
			(GlobalFree(pframe->m_pxaxis->imgBuf3RGB_cam4) == NULL) && 
			(GlobalFree(pframe->m_pxaxis->imgBuf3RGB_cam5) == NULL)	) //Free Up the memory already occupied
		{
			pframe->m_pxaxis->imgBuf3RGB_cam1      =   (BYTE*)GlobalAlloc(GMEM_FIXED| GMEM_ZEROINIT,w[1]*h[1]*4*MAXNUMDEFPIC);    //Re allocate and Reinitialise the Pointers
			pframe->m_pxaxis->imgBuf3RGB_cam2      =   (BYTE*)GlobalAlloc(GPTR,w[2]*h[2]*4*MAXNUMDEFPIC); 
			pframe->m_pxaxis->imgBuf3RGB_cam3      =   (BYTE*)GlobalAlloc(GPTR,w[3]*h[3]*4*MAXNUMDEFPIC); 
			pframe->m_pxaxis->imgBuf3RGB_cam4      =   (BYTE*)GlobalAlloc(GPTR,w[4]*h[4]*4*MAXNUMDEFPIC); 
			pframe->m_pxaxis->imgBuf3RGB_cam5      =   (BYTE*)GlobalAlloc(GPTR,w[5]*h[5]*4*MAXNUMDEFPIC); 
			
			pframe->m_pxaxis->arr1[0]=pframe->m_pxaxis->imgBuf3RGB_cam1; //storing the Default address in an array
			pframe->m_pxaxis->arr1[1]=pframe->m_pxaxis->imgBuf3RGB_cam2;
			pframe->m_pxaxis->arr1[2]=pframe->m_pxaxis->imgBuf3RGB_cam3;
			pframe->m_pxaxis->arr1[3]=pframe->m_pxaxis->imgBuf3RGB_cam4;
			pframe->m_pxaxis->arr1[4]=pframe->m_pxaxis->imgBuf3RGB_cam5;

			arr_strng_addrs[0]=pframe->m_pxaxis->imgBuf3RGB_cam1;
			arr_strng_addrs[1]=pframe->m_pxaxis->imgBuf3RGB_cam2;
			arr_strng_addrs[2]=pframe->m_pxaxis->imgBuf3RGB_cam3;
			arr_strng_addrs[3]=pframe->m_pxaxis->imgBuf3RGB_cam4;
			arr_strng_addrs[4]=pframe->m_pxaxis->imgBuf3RGB_cam5;

			for(int i=0;i<MAXNUMDEFPIC;i++)
			{
				for(int j=0;j<=3;j++)
				{pframe->m_pxaxis->tim_stamp[i][j]=0;}
			}

			for(int k = m_pcombo->GetCount()-1; k >= 0; k--)
			{m_pcombo->DeleteString(k);}

			m_pcombo->SetCurSel(-1);
			pframe->m_pxaxis->imge_no=0;
		}
	}

	else{ 
		
		MessageBox(" Need to Select Something from the Drop Down Before you can Reset ");
	}	
}

void PicRecal::OnDestroy() 
{
	CDialog::OnDestroy();
}

void PicRecal::OnClose() 
{
	count=0;
	pframe->m_pxaxis->imgBuf3RGB_cam1=arr_strng_addrs[0];   //Putting the stored address back
	pframe->m_pxaxis->imgBuf3RGB_cam2=arr_strng_addrs[1];
	pframe->m_pxaxis->imgBuf3RGB_cam3=arr_strng_addrs[2];
	pframe->m_pxaxis->imgBuf3RGB_cam4=arr_strng_addrs[3];
	pframe->m_pxaxis->imgBuf3RGB_cam5=arr_strng_addrs[4];

	pframe->m_pxaxis->showostore=0;

	OnDestroy();

	CDialog::OnClose();
}


void PicRecal::OnSelection()
{
	Invalidate(true);	
}

void PicRecal::OnShowC5() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].showC5inPictures)
	{
		CheckDlgButton(IDC_SHOWC5,0); 
		theapp->jobinfo[pframe->CurrentJobNum].showC5inPictures=false;
	}
	else
	{
		CheckDlgButton(IDC_SHOWC5,1);
		theapp->jobinfo[pframe->CurrentJobNum].showC5inPictures=true;
	}
		
	UpdateValues();	
}

void PicRecal::UpdateValues()
{
	SetTimer(1,300,NULL);
	Invalidate(true);	
}


void PicRecal::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{
		KillTimer(1);
		pframe->SaveJob(pframe->CurrentJobNum);
	}

	CDialog::OnTimer(nIDEvent);
}

