// Lim.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "Lim.h"
#include "mainfrm.h"
#include "xaxisview.h"
#include "bottleview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Lim dialog


Lim::Lim(CWnd* pParent /*=NULL*/)
	: CDialog(Lim::IDD, pParent)
{
	//{{AFX_DATA_INIT(Lim)
	m_1 = _T("");
	m_2 = _T("");
	m_5 = _T("");
	m_4 = _T("");
	m_3 = _T("");
	m_6 = _T("");
	m_8 = _T("");
	m_7 = _T("");
	m_m1 = _T("");
	m_m2 = _T("");
	m_m3 = _T("");
	m_m4 = _T("");
	m_m5 = _T("");
	m_m6 = _T("");
	m_m7 = _T("");
	m_m8 = _T("");
	m_l2 = _T("");
	m_l1 = _T("");
	m_l3 = _T("");
	m_l4 = _T("");
	m_l5 = _T("");
	m_l6 = _T("");
	m_l7 = _T("");
	m_l8 = _T("");
	m_1max = _T("");
	m_2max = _T("");
	m_3max = _T("");
	m_4max = _T("");
	m_5max = _T("");
	m_6max = _T("");
	m_7max = _T("");
	m_8max = _T("");
	m_1min = _T("");
	m_2min = _T("");
	m_3min = _T("");
	m_4min = _T("");
	m_5min = _T("");
	m_6min = _T("");
	m_7min = _T("");
	m_8min = _T("");
	m_10 = _T("");
	m_l10 = _T("");
	m_m10 = _T("");
	m_10max = _T("");
	m_10min = _T("");
	m_9 = _T("");
	m_l9 = _T("");
	m_m9 = _T("");
	m_9max = _T("");
	m_9min = _T("");
	m_11 = _T("");
	m_l11 = _T("");
	m_m11 = _T("");
	m_11min = _T("");
	m_11max = _T("");
	//}}AFX_DATA_INIT
}


void Lim::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Lim)
	DDX_Control(pDX, IDC_E12, mbutt_enabl12);
	DDX_Control(pDX, IDC_E11, mbutt_enab11);
	DDX_Control(pDX, IDC_FINE, m_fine);
	DDX_Control(pDX, IDC_E9, mbutt_enab9);
	DDX_Control(pDX, IDC_E7, mbutt_enab7);
	DDX_Control(pDX, IDC_E6, mbutt_enab6);
	DDX_Control(pDX, IDC_E5, mbutt_enab5);
	DDX_Control(pDX, IDC_E4, mbutt_enab4);
	DDX_Control(pDX, IDC_E3, mbutt_enab3);
	DDX_Control(pDX, IDC_E2, mbutt_enab2);
	DDX_Control(pDX, IDC_E10, mbutt_enab10);
	DDX_Control(pDX, IDC_E1, mbutt_enab1);
	DDX_Control(pDX, IDC_E8, mbutt_enab8);
	DDX_Control(pDX, IDC_10UP, m_10up);
	DDX_Control(pDX, IDC_10DN, m_10dn);
	DDX_Control(pDX, IDC_NAME3, m_name3);
	DDX_Control(pDX, IDC_6UP, m_6upc);
	DDX_Control(pDX, IDC_6DN, m_6dnc);
	DDX_Control(pDX, IDC_6, m_6c);
	DDX_Control(pDX, IDC_1UP, m_1up);
	DDX_Control(pDX, IDC_1DN, m_1dn);
	DDX_Text(pDX, IDC_1, m_1);
	DDX_Text(pDX, IDC_2, m_2);
	DDX_Text(pDX, IDC_5, m_5);
	DDX_Text(pDX, IDC_4, m_4);
	DDX_Text(pDX, IDC_3, m_3);
	DDX_Text(pDX, IDC_6, m_6);
	DDX_Text(pDX, IDC_8, m_8);
	DDX_Text(pDX, IDC_7, m_7);
	DDX_Text(pDX, IDC_M1, m_m1);
	DDX_Text(pDX, IDC_M2, m_m2);
	DDX_Text(pDX, IDC_M3, m_m3);
	DDX_Text(pDX, IDC_M4, m_m4);
	DDX_Text(pDX, IDC_M5, m_m5);
	DDX_Text(pDX, IDC_M6, m_m6);
	DDX_Text(pDX, IDC_M7, m_m7);
	DDX_Text(pDX, IDC_M8, m_m8);
	DDX_Text(pDX, IDC_L2, m_l2);
	DDX_Text(pDX, IDC_L1, m_l1);
	DDX_Text(pDX, IDC_L3, m_l3);
	DDX_Text(pDX, IDC_L4, m_l4);
	DDX_Text(pDX, IDC_L5, m_l5);
	DDX_Text(pDX, IDC_L6, m_l6);
	DDX_Text(pDX, IDC_L7, m_l7);
	DDX_Text(pDX, IDC_L8, m_l8);
	DDX_Text(pDX, IDC_MAX1, m_1max);
	DDX_Text(pDX, IDC_MAX2, m_2max);
	DDX_Text(pDX, IDC_MAX3, m_3max);
	DDX_Text(pDX, IDC_MAX4, m_4max);
	DDX_Text(pDX, IDC_MAX5, m_5max);
	DDX_Text(pDX, IDC_MAX6, m_6max);
	DDX_Text(pDX, IDC_MAX7, m_7max);
	DDX_Text(pDX, IDC_MAX8, m_8max);
	DDX_Text(pDX, IDC_MIN1, m_1min);
	DDX_Text(pDX, IDC_MIN2, m_2min);
	DDX_Text(pDX, IDC_MIN3, m_3min);
	DDX_Text(pDX, IDC_MIN4, m_4min);
	DDX_Text(pDX, IDC_MIN5, m_5min);
	DDX_Text(pDX, IDC_MIN6, m_6min);
	DDX_Text(pDX, IDC_MIN7, m_7min);
	DDX_Text(pDX, IDC_MIN8, m_8min);
	DDX_Text(pDX, IDC_10, m_10);
	DDX_Text(pDX, IDC_L10, m_l10);
	DDX_Text(pDX, IDC_M10, m_m10);
	DDX_Text(pDX, IDC_MAX10, m_10max);
	DDX_Text(pDX, IDC_MIN10, m_10min);
	DDX_Text(pDX, IDC_9, m_9);
	DDX_Text(pDX, IDC_L9, m_l9);
	DDX_Text(pDX, IDC_M9, m_m9);
	DDX_Text(pDX, IDC_MAX9, m_9max);
	DDX_Text(pDX, IDC_MIN9, m_9min);
	DDX_Text(pDX, IDC_11, m_11);
	DDX_Text(pDX, IDC_L11, m_l11);
	DDX_Text(pDX, IDC_M11, m_m11);
	DDX_Text(pDX, IDC_MIN11, m_11min);
	DDX_Text(pDX, IDC_MAX11, m_11max);
	DDX_Text(pDX, IDC_12, m_12);
	DDX_Text(pDX, IDC_L12, m_l12);
	DDX_Text(pDX, IDC_M12, m_m12);
	DDX_Text(pDX, IDC_MIN12, m_12min);
	DDX_Text(pDX, IDC_MAX12, m_12max);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Lim, CDialog)
	//{{AFX_MSG_MAP(Lim)
	ON_BN_CLICKED(IDC_1UP, On1up)
	ON_BN_CLICKED(IDC_1DN, On1dn)
	ON_BN_CLICKED(IDC_2UP, On2up)
	ON_BN_CLICKED(IDC_2DN, On2dn)
	ON_BN_CLICKED(IDC_3UP, On3up)
	ON_BN_CLICKED(IDC_3DN, On3dn)
	ON_BN_CLICKED(IDC_4UP, On4up)
	ON_BN_CLICKED(IDC_4DN, On4dn)
	ON_BN_CLICKED(IDC_5UP, On5up)
	ON_BN_CLICKED(IDC_5DN, On5dn)
	ON_BN_CLICKED(IDC_6UP, On6up)
	ON_BN_CLICKED(IDC_6DN, On6dn)
	ON_BN_CLICKED(IDC_7UP, On7up)
	ON_BN_CLICKED(IDC_7DN, On7dn)
	ON_BN_CLICKED(IDC_8UP, On8up)
	ON_BN_CLICKED(IDC_8DN, On8dn)
	ON_BN_CLICKED(IDC_MAX, OnMax)
	ON_BN_CLICKED(IDC_MIN, OnMin)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_7UP2, On7up2)
	ON_BN_CLICKED(IDC_7DN2, On7dn2)
	ON_BN_CLICKED(IDC_8UP2, On8up2)
	ON_BN_CLICKED(IDC_8DN2, On8dn2)
	ON_BN_CLICKED(IDC_10UP, On10up)
	ON_BN_CLICKED(IDC_10DN, On10dn)
	ON_BN_CLICKED(IDC_10UP2, On10up2)
	ON_BN_CLICKED(IDC_10DN2, On10dn2)
	ON_BN_CLICKED(IDC_9UP, On9up)
	ON_BN_CLICKED(IDC_9DN, On9dn)
	ON_BN_CLICKED(IDC_E8, OnE8)
	ON_BN_CLICKED(IDC_E1, OnE1)
	ON_BN_CLICKED(IDC_E2, OnE2)
	ON_BN_CLICKED(IDC_E3, OnE3)
	ON_BN_CLICKED(IDC_E4, OnE4)
	ON_BN_CLICKED(IDC_E5, OnE5)
	ON_BN_CLICKED(IDC_E6, OnE6)
	ON_BN_CLICKED(IDC_E7, OnE7)
	ON_BN_CLICKED(IDC_E9, OnE9)
	ON_BN_CLICKED(IDC_E10, OnE10)
	ON_BN_CLICKED(IDC_FINE, OnFine)
	ON_BN_CLICKED(IDC_E11, OnE11)
	ON_BN_CLICKED(IDC_11UP, On11up)
	ON_BN_CLICKED(IDC_11DN, On11dn)
	ON_BN_CLICKED(IDC_E12, OnE12)
	ON_BN_CLICKED(IDC_12UP, On12up)
	ON_BN_CLICKED(IDC_12DN, On12dn)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Lim message handlers

BOOL Lim::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_plimit=this;
	theapp=(CBottleApp*)AfxGetApp();


	if(!theapp->waistAvailable)
	{
		GetDlgItem(IDC_WAIST_GROUP_BOX)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_L11)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_M11)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MAX11)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_11)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MIN11)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_E11)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_11UP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_11DN)->ShowWindow(SW_HIDE);
	}

	// Added by Andrew Gawlik 2017-07-10 to hide cap inspection configuration when in 84 mode (when cam5Enable is false)
	if(!theapp->cam5Enable)
	{
		GetDlgItem(IDC_I1_GROUP_BOX)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_L1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_M1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MAX1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MIN1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_E1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_1UP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_1DN)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_I2_GROUP_BOX)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_L2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_M2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MAX2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MIN2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_E2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_2UP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_2DN)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_I3_GROUP_BOX)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_L3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_M3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MAX3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MIN3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_E3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_3UP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_3DN)->ShowWindow(SW_HIDE);
	}

	UpdateVisibilityInspection12();

	Max=true;
	
	//CheckDlgButton(IDC_FINE,0);
	m_fine.SetWindowText("Slow");
	res=1;
	fine=true;
	OnMax();

	AbsoluteMax		= 500;
	AbsoluteMax2	= 1000;


	////////////////
/*
	static_m_1="                                                                      ";
	static_m_2="                                                                      ";
	static_m_3="                                                                      ";
	static_m_4="                                                                      ";
	static_m_5="                                                                      ";
	static_m_6="                                                                      ";
	static_m_7="                                                                      ";
	static_m_8="                                                                      ";
	static_m_9="                                                                      ";
	static_m_10="                                                                      ";

	static_m_m1="                                                                      ";
	static_m_m2="                                                                      ";
	static_m_m3="                                                                      ";
	static_m_m4="                                                                      ";
	static_m_m5="                                                                      ";
	static_m_m6="                                                                      ";
	static_m_m7="                                                                      ";
	static_m_m8="                                                                      ";
	static_m_m9="                                                                      ";
	static_m_m10="                                                                      ";

	static_m_l1="                                                                      ";
	static_m_l2="                                                                      ";
	static_m_l3="                                                                      ";
	static_m_l4="                                                                      ";
	static_m_l5="                                                                      ";
	static_m_l6="                                                                      ";
	static_m_l7="                                                                      ";
	static_m_l8="                                                                      ";
	static_m_l9="                                                                      ";
	static_m_l10="                                                                      ";

	static_m_1max="                                                                      ";
	static_m_2max="                                                                      ";
	static_m_3max="                                                                      ";
	static_m_4max="                                                                      ";
	static_m_5max="                                                                      ";
	static_m_6max="                                                                      ";
	static_m_7max="                                                                      ";
	static_m_8max="                                                                      ";
	static_m_9max="                                                                      ";
	static_m_10max="                                                                      ";

	static_m_1min="                                                                      ";
	static_m_2min="                                                                      ";
	static_m_3min="                                                                      ";
	static_m_4min="                                                                      ";
	static_m_5min="                                                                      ";
	static_m_6min="                                                                      ";
	static_m_7min="                                                                      ";
	static_m_8min="                                                                      ";
	static_m_9min="                                                                      ";
	static_m_10min="                                                                      ";


	m_1=static_m_1;
	m_2=static_m_2;
	m_3=static_m_3;
	m_4=static_m_4;
	m_5=static_m_5;
	m_6=static_m_6;
	m_7=static_m_7;
	m_8=static_m_8;
	m_9=static_m_9;
	m_10=static_m_10;

	m_m1=static_m_m1;
	m_m2=static_m_m2;
	m_m3=static_m_m3;
	m_m4=static_m_m4;
	m_m5=static_m_m5;
	m_m6=static_m_m6;
	m_m7=static_m_m7;
	m_m8=static_m_m8;
	m_m9=static_m_m9;
	m_m10=static_m_m10;

	m_l1=static_m_l1;
	m_l2=static_m_l2;
	m_l3=static_m_l3;
	m_l4=static_m_l4;
	m_l5=static_m_l5;
	m_l6=static_m_l6;
	m_l7=static_m_l7;
	m_l8=static_m_l8;
	m_l9=static_m_l9;
	m_l10=static_m_l10;

	m_1max=static_m_1max;
	m_2max=static_m_2max;
	m_3max=static_m_3max;
	m_4max=static_m_4max;
	m_5max=static_m_5max;
	m_6max=static_m_6max;
	m_7max=static_m_7max;
	m_8max=static_m_8max;
	m_9max=static_m_9max;
	m_10max=static_m_10max;

	m_1min=static_m_1min;
	m_2min=static_m_2min;
	m_3min=static_m_3min;
	m_4min=static_m_4min;
	m_5min=static_m_5min;
	m_6min=static_m_6min;
	m_7min=static_m_7min;
	m_8min=static_m_8min;
	m_9min=static_m_9min;
	m_10min=static_m_10min;
*/

	////////////////

	m_1.Format("%3i",0);
	m_2.Format("%3i",0);
	m_3.Format("%3i",0);
	m_4.Format("%3i",0);
	m_5.Format("%3i",0);
	m_6.Format("%3i",0);
	m_7.Format("%3i",0);
	m_8.Format("%3i",0);
	m_9.Format("%3i",0);
	m_11.Format("%3i",0);
	m_12.Format("%3i",0);


	m_l1.Format("%s",theapp->inspctn[1].name);
	m_l2.Format("%s",theapp->inspctn[2].name);
	m_l3.Format("%s",theapp->inspctn[3].name);
	m_l4.Format("%s",theapp->inspctn[4].name);
	m_l5.Format("%s",theapp->inspctn[5].name);
	m_l6.Format("%s",theapp->inspctn[6].name);
	m_l7.Format("%s",theapp->inspctn[7].name);
	m_l8.Format("%s",theapp->inspctn[8].name);
	m_l9.Format("%s",theapp->inspctn[9].name);
	m_l11.Format("%s",theapp->inspctn[11].name);
	m_l12.Format("%s",theapp->inspctn[12].name);


	UpdateData(false);	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Lim::UpdateDisplay()
{
	int bottleStyle = theapp->jobinfo[pframe->CurrentJobNum].bottleStyle;

	if(Max)
	{
		m_1.Format("%3i",theapp->inspctn[1].lmax);
		m_2.Format("%3i",theapp->inspctn[2].lmax);
		m_3.Format("%3i",theapp->inspctn[3].lmax);
		m_4.Format("%3i",theapp->inspctn[4].lmax);
		m_5.Format("%3i",theapp->inspctn[5].lmax);

		if(bottleStyle==3)			m_6.Format("%3i",theapp->inspctn[6].lmax);
		else if(bottleStyle==10)	m_6.Format("%3i",theapp->inspctn[6].lmaxC);
		else						m_6.Format("%3i",theapp->inspctn[6].lmaxB);
		
		m_7.Format("%3i",theapp->inspctn[7].lmax);
		m_8.Format("%3i",theapp->inspctn[8].lmax);
		m_9.Format("%3i",theapp->inspctn[9].lmax);
		m_10.Format("%3i",theapp->inspctn[10].lmax);
		m_11.Format("%3i",theapp->inspctn[11].lmax);
		m_12.Format("%3i",theapp->inspctn[12].lmax);
	}
	else
	{
		m_1.Format("%3i",theapp->inspctn[1].lmin);
		m_2.Format("%3i",theapp->inspctn[2].lmin);
		m_3.Format("%3i",theapp->inspctn[3].lmin);
		m_4.Format("%3i",theapp->inspctn[4].lmin);
		m_5.Format("%3i",theapp->inspctn[5].lmin);

		if(bottleStyle==3)			m_6.Format("%3i",theapp->inspctn[6].lmin);
		else if(bottleStyle==10)	m_6.Format("%3i",theapp->inspctn[6].lminC);
		else						m_6.Format("%3i",theapp->inspctn[6].lminB);

		m_7.Format("%3i",theapp->inspctn[7].lmin);
		m_8.Format("%3i",theapp->inspctn[8].lmin);
		m_9.Format("%3i",theapp->inspctn[9].lmin);
		m_10.Format("%3i",theapp->inspctn[10].lmin);
		m_11.Format("%3i",theapp->inspctn[11].lmin);
		m_12.Format("%3i",theapp->inspctn[12].lmin);
	}

	m_l1.Format("%s",theapp->inspctn[1].name);
	m_l2.Format("%s",theapp->inspctn[2].name);
	m_l3.Format("%s",theapp->inspctn[3].name);
	m_l4.Format("%s",theapp->inspctn[4].name);
	m_l5.Format("%s",theapp->inspctn[5].name);
	m_l6.Format("%s",theapp->inspctn[6].name);
	m_l7.Format("%s",theapp->inspctn[7].name);
	m_l8.Format("%s",theapp->inspctn[8].name);
	m_l9.Format("%s",theapp->inspctn[9].name);
	m_l10.Format("%s",theapp->inspctn[10].name);
	m_l11.Format("%s",theapp->inspctn[11].name);
	m_l12.Format("%s",theapp->inspctn[12].name);

	if(theapp->cam5Enable)
	{
		if( theapp->inspctn[1].enable )
		{
			CheckDlgButton(IDC_E1,0);
			mbutt_enab1.SetWindowText("ON");
		}
		else								
		{
			CheckDlgButton(IDC_E1,1); mbutt_enab1.SetWindowText("OFF");
		}

		if( theapp->inspctn[2].enable )	{CheckDlgButton(IDC_E2,0); mbutt_enab2.SetWindowText("ON");}
		else									{CheckDlgButton(IDC_E2,1); mbutt_enab2.SetWindowText("OFF");}

		if( theapp->inspctn[3].enable )	{CheckDlgButton(IDC_E3,0); mbutt_enab3.SetWindowText("ON");}
		else									{CheckDlgButton(IDC_E3,1); mbutt_enab3.SetWindowText("OFF");}

		if( theapp->inspctn[4].enable )	{CheckDlgButton(IDC_E4,0); mbutt_enab4.SetWindowText("ON");}
		else									{CheckDlgButton(IDC_E4,1); mbutt_enab4.SetWindowText("OFF");}
	}
	
	if( theapp->inspctn[5].enable )	{CheckDlgButton(IDC_E5,0); mbutt_enab5.SetWindowText("ON");}
	else									{CheckDlgButton(IDC_E5,1); mbutt_enab5.SetWindowText("OFF");}
	
	if( theapp->inspctn[6].enable )	{CheckDlgButton(IDC_E6,0); mbutt_enab6.SetWindowText("ON");}
	else									{CheckDlgButton(IDC_E6,1); mbutt_enab6.SetWindowText("OFF");}
	
	if( theapp->inspctn[7].enable )	{CheckDlgButton(IDC_E7,0); mbutt_enab7.SetWindowText("ON");}
	else									{CheckDlgButton(IDC_E7,1); mbutt_enab7.SetWindowText("OFF");}
	
	if( theapp->inspctn[8].enable )	{CheckDlgButton(IDC_E8,0); mbutt_enab8.SetWindowText("ON");}
	else									{CheckDlgButton(IDC_E8,1); mbutt_enab8.SetWindowText("OFF");}
	
	if( theapp->inspctn[9].enable )	{CheckDlgButton(IDC_E9,0); mbutt_enab9.SetWindowText("ON");}
	else									{CheckDlgButton(IDC_E9,1); mbutt_enab9.SetWindowText("OFF");}
	
	if( theapp->inspctn[10].enable )	{CheckDlgButton(IDC_E10,0);mbutt_enab10.SetWindowText("ON");}
	else									{CheckDlgButton(IDC_E10,1);mbutt_enab10.SetWindowText("OFF");}
	
	if(theapp->waistAvailable)
	{
		if( theapp->inspctn[11].enable )	{CheckDlgButton(IDC_E11,0);mbutt_enab11.SetWindowText("ON");}
		else									{CheckDlgButton(IDC_E11,1);mbutt_enab11.SetWindowText("OFF");}
	}

	//make sure the I12 shows when it should given the job
	UpdateVisibilityInspection12();

	//align available or job for square bottle
	if(theapp->alignAvailable || theapp->jobinfo[pframe->CurrentJobNum].bottleStyle == 10)
	{
		if( theapp->inspctn[12].enable )	{CheckDlgButton(IDC_E12,0);mbutt_enabl12.SetWindowText("ON");}
		else									{CheckDlgButton(IDC_E12,1);mbutt_enabl12.SetWindowText("OFF");}
	}
	//////////////////////	
	
	if(theapp->inspctn[1].type==1)	
	{
		m_1min.Format("%s", "0");
		m_1max.Format("%s", "255");
	}
	if(theapp->inspctn[2].type==1)	{m_2min.Format("%s", "0"); m_2max.Format("%s", "255");}
	if(theapp->inspctn[3].type==1)	{m_3min.Format("%s", "0"); m_3max.Format("%s", "255");}
	if(theapp->inspctn[4].type==1)	{m_4min.Format("%s", "0"); m_4max.Format("%s", "255");}
	if(theapp->inspctn[5].type==1)	{m_5min.Format("%s", "0"); m_5max.Format("%s", "255");}
	if(theapp->inspctn[6].type==1)	{m_6min.Format("%s", "0"); m_6max.Format("%s", "255");}
	if(theapp->inspctn[7].type==1)	{m_7min.Format("%s", "0"); m_7max.Format("%s", "255");}
	if(theapp->inspctn[8].type==1)	{m_8min.Format("%s", "0"); m_8max.Format("%s", "255");}
	if(theapp->inspctn[9].type==1)	{m_9min.Format("%s", "0"); m_9max.Format("%s", "255");}
	if(theapp->inspctn[10].type==1)	{m_10min.Format("%s", "0");m_10max.Format("%s", "255");}

	//////////////////////

	if(theapp->inspctn[1].type==2)	{m_1min.Format("%s", "pixel");	m_1max.Format("%s", "");}
	if(theapp->inspctn[2].type==2)	{m_2min.Format("%s", "pixel");  m_2max.Format("%s", "");}
	if(theapp->inspctn[3].type==2)	{m_3min.Format("%s", "RGB");	m_3max.Format("%s", "");}
	if(theapp->inspctn[4].type==2)	{m_4min.Format("%s", "%");		m_4max.Format("%s", "");}
	if(theapp->inspctn[5].type==2)	{m_5min.Format("%s", "mm");		m_5max.Format("%s", "");}

	if(theapp->inspctn[6].type==2)
	{
		if(bottleStyle==3)			{m_6min.Format("%s", "mm");		m_6max.Format("%s", "");}
		else if(bottleStyle==10)	{m_6min.Format("%s", "pixel");	m_6max.Format("%s", "");}
		else						{m_6min.Format("%s", "");		m_6max.Format("%s", "");}
	}

	if(theapp->inspctn[7].type==2)	{m_7min.Format("%s", "%");		m_7max.Format("%s", "");}
	if(theapp->inspctn[8].type==2)	{m_8min.Format("%s", "pixel");	m_8max.Format("%s", "");}
	if(theapp->inspctn[9].type==2)	{m_9min.Format("%s", "RGB");	m_9max.Format("%s", "");}
	if(theapp->inspctn[10].type==2)	{m_10min.Format("%s", "pixel");	m_10max.Format("%s", "");}
	
	//////////////////////

	if(theapp->inspctn[1].type==3)	{m_1min.Format("%s", "0"); m_1max.Format("%s", "2.00");}
	if(theapp->inspctn[2].type==3)	{m_2min.Format("%s", "0"); m_2max.Format("%s", "2.00");}
	if(theapp->inspctn[3].type==3)	{m_3min.Format("%s", "0"); m_3max.Format("%s", "2.00");}
	if(theapp->inspctn[4].type==3)	{m_4min.Format("%s", "0"); m_4max.Format("%s", "2.00");}
	if(theapp->inspctn[5].type==3)	{m_5min.Format("%s", "0"); m_5max.Format("%s", "2.00");}
	if(theapp->inspctn[6].type==3)	{m_6min.Format("%s", "0"); m_6max.Format("%s", "2.00");}
	if(theapp->inspctn[7].type==3)	{m_7min.Format("%s", "0"); m_7max.Format("%s", "2.00");}
	if(theapp->inspctn[8].type==3)	{m_8min.Format("%s", "0"); m_8max.Format("%s", "2.00");}
	if(theapp->inspctn[9].type==3)	{m_9min.Format("%s", "0"); m_9max.Format("%s", "2.00");}
	if(theapp->inspctn[10].type==3)	{m_10min.Format("%s", "0");m_10max.Format("%s", "2.00");}



	m_11min.Format("%s", "count");	
	m_11max.Format("%s", "");

	UpdateData(false);
}

void Lim::On1up() 
{
	if(Max)
	{
		theapp->inspctn[1].lmax+=res;
		if(theapp->inspctn[1].lmax >=AbsoluteMax)	{theapp->inspctn[1].lmax=AbsoluteMax;}
	}
	else
	{
		theapp->inspctn[1].lmin+=res;
		if(theapp->inspctn[1].lmin >=AbsoluteMax)	{theapp->inspctn[1].lmin=AbsoluteMax;}
	}

	UpdateValues();
}

void Lim::On1dn() 
{
	if(Max)
	{
		theapp->inspctn[1].lmax-=res;
		if(theapp->inspctn[1].lmax <=0)				{theapp->inspctn[1].lmax=0;}
	}
	else
	{
		theapp->inspctn[1].lmin-=res;
		if(theapp->inspctn[1].lmin <=0)				{theapp->inspctn[1].lmin=0;}
	}

	UpdateValues();
}

void Lim::On2up() 
{
	if(Max)
	{
		theapp->inspctn[2].lmax+=res;
		if(theapp->inspctn[2].lmax >=AbsoluteMax)	{theapp->inspctn[2].lmax=AbsoluteMax;}
	}
	else
	{
		theapp->inspctn[2].lmin+=res;
		if(theapp->inspctn[2].lmin >=AbsoluteMax)	{theapp->inspctn[2].lmin=AbsoluteMax;}
	}

	UpdateValues();
}

void Lim::On2dn() 
{
	if(Max)
	{
		theapp->inspctn[2].lmax-=res;
		if(theapp->inspctn[2].lmax <=0)				{theapp->inspctn[2].lmax=0;}
	}
	else
	{
		theapp->inspctn[2].lmin-=res;
		if(theapp->inspctn[2].lmin <=0)				{theapp->inspctn[2].lmin=0;}
	}

	UpdateValues();
}

void Lim::On3up() 
{
	if(Max)
	{
		theapp->inspctn[3].lmax+=res;
		if(theapp->inspctn[3].lmax >=AbsoluteMax)	{theapp->inspctn[3].lmax=AbsoluteMax;}
	}
	else
	{
		theapp->inspctn[3].lmin+=res;
		if(theapp->inspctn[3].lmin >=AbsoluteMax)	{theapp->inspctn[3].lmin=AbsoluteMax;}
	}

	UpdateValues();
}

void Lim::On3dn() 
{
	if(Max)
	{
		theapp->inspctn[3].lmax-=res;
		if(theapp->inspctn[3].lmax <=0)				{theapp->inspctn[3].lmax=0;}
	}
	else
	{
		theapp->inspctn[3].lmin-=res;
		if(theapp->inspctn[3].lmin <=0)				{theapp->inspctn[3].lmin=0;}
	}

	UpdateValues();
}

void Lim::On4up() 
{
	if(Max)
	{
		theapp->inspctn[4].lmax+=res;
		if(theapp->inspctn[4].lmax >=100)			{theapp->inspctn[4].lmax=100;}
	}
	else
	{
		theapp->inspctn[4].lmin+=res;
		if(theapp->inspctn[4].lmin >=AbsoluteMax)	{theapp->inspctn[4].lmin=AbsoluteMax;}
	}

	UpdateValues();
}

void Lim::On4dn() 
{
	if(Max)
	{
		theapp->inspctn[4].lmax-=res;
		if(theapp->inspctn[4].lmax <=0)				{theapp->inspctn[4].lmax=0;}
	}
	else
	{
		theapp->inspctn[4].lmin-=res;
		if(theapp->inspctn[4].lmin <=0)				{theapp->inspctn[4].lmin=0;}
	}

	UpdateValues();
}

void Lim::On5up() 
{
	if(Max)
	{
		theapp->inspctn[5].lmax+=res;
		if(theapp->inspctn[5].lmax >=AbsoluteMax)	{theapp->inspctn[5].lmax=AbsoluteMax;}
	}
	else
	{
		theapp->inspctn[5].lmin+=res;
		if(theapp->inspctn[5].lmin >=AbsoluteMax)	{theapp->inspctn[5].lmin=AbsoluteMax;}
	}

	UpdateValues();
}

void Lim::On5dn() 
{
	if(Max)
	{
		theapp->inspctn[5].lmax-=res;
		if(theapp->inspctn[5].lmax <=0)				{theapp->inspctn[5].lmax=0;}
//		m_5.Format("%3i",theapp->inspctn[5].lmax);
	}
	else
	{
		theapp->inspctn[5].lmin-=res;
		//if(theapp->inspctn[5].lmin <=0) theapp->inspctn[5].lmin=0;
	}

	UpdateValues();
}

void Lim::On6up() 
{
	int bottleStyle = theapp->jobinfo[pframe->CurrentJobNum].bottleStyle;

	if(bottleStyle==3)
	{
		if(Max)
		{
			theapp->inspctn[6].lmax+=res;
			if(theapp->inspctn[6].lmax >=AbsoluteMax)	{theapp->inspctn[6].lmax=AbsoluteMax;}
		}
		else
		{
			theapp->inspctn[6].lmin+=res;
			if(theapp->inspctn[6].lmin >=AbsoluteMax)	{theapp->inspctn[6].lmin=AbsoluteMax;}
		}
	}
	else if(bottleStyle==10)
	{
		if(Max)
		{
			theapp->inspctn[6].lmaxC+=res;
			if(theapp->inspctn[6].lmaxC >=AbsoluteMax2)	{theapp->inspctn[6].lmaxC=AbsoluteMax2;}
		}
		else
		{
			theapp->inspctn[6].lminC+=res;
			if(theapp->inspctn[6].lminC >=AbsoluteMax2)	{theapp->inspctn[6].lminC=AbsoluteMax2;}
		}
	}
	else
	{
		if(Max)
		{
			theapp->inspctn[6].lmaxB+=res;
			if(theapp->inspctn[6].lmaxB >=AbsoluteMax2)	{theapp->inspctn[6].lmaxB=AbsoluteMax2;}
		}
		else
		{
			theapp->inspctn[6].lminB+=res;
			if(theapp->inspctn[6].lminB >=AbsoluteMax2)	{theapp->inspctn[6].lminB=AbsoluteMax2;}
		}
	}

	UpdateValues();
}

void Lim::On6dn() 
{
	int bottleStyle = theapp->jobinfo[pframe->CurrentJobNum].bottleStyle;

	if(bottleStyle==3)
	{
		if(Max)
		{theapp->inspctn[6].lmax-=res; if(theapp->inspctn[6].lmax <=0) {theapp->inspctn[6].lmax=0;} }
		else
		{theapp->inspctn[6].lmin-=res;}
	}
	else if(bottleStyle==10)
	{
		if(Max)
		{theapp->inspctn[6].lmaxC-=res; if(theapp->inspctn[6].lmaxC <=0) {theapp->inspctn[6].lmaxC=0;} }
		else
		{theapp->inspctn[6].lminC-=res;}
	}
	else
	{
		if(Max)
		{theapp->inspctn[6].lmaxB-=res; if(theapp->inspctn[6].lmaxB <=0) {theapp->inspctn[6].lmaxB=0;} }
		else
		{theapp->inspctn[6].lminB-=res;}
	}


	UpdateValues();
}

void Lim::On7up2() 
{
	if(Max)
	{
		theapp->inspctn[7].lmax+=100;
		if(theapp->inspctn[7].lmax >=4500)			{theapp->inspctn[7].lmax=4500;}
	}
	else
	{
		theapp->inspctn[7].lmin+=100;
		if(theapp->inspctn[7].lmin >=4500)			{theapp->inspctn[7].lmin=4500;}
	}

	UpdateValues();
}

void Lim::On7dn2() 
{
	if(Max)
	{
		theapp->inspctn[7].lmax-=100;
		if(theapp->inspctn[7].lmax <=0)				{theapp->inspctn[7].lmax=0;}
	}
	else
	{
		theapp->inspctn[7].lmin-=100;
		if(theapp->inspctn[7].lmin <=0)				{theapp->inspctn[7].lmin=0;}
	}

	UpdateValues();
}

void Lim::On7up() 
{
	if(Max)
	{
		theapp->inspctn[7].lmax+=res;
		if(theapp->inspctn[7].lmax >=100)			{theapp->inspctn[7].lmax=100;}
	}
	else
	{
		theapp->inspctn[7].lmin+=res;
		if(theapp->inspctn[7].lmin >=100)			{theapp->inspctn[7].lmin=100;}
	}

	UpdateValues();
}

void Lim::On7dn() 
{
	if(Max)
	{
		theapp->inspctn[7].lmax-=res;
		if(theapp->inspctn[7].lmax <=0)				{theapp->inspctn[7].lmax=0;}
	}
	else
	{
		theapp->inspctn[7].lmin-=res;
		if(theapp->inspctn[7].lmin <=0)				{theapp->inspctn[7].lmin=0;}
	}

	UpdateValues();
}

void Lim::On8up() 
{
	if(Max)
	{
		theapp->inspctn[8].lmax+=res;
		if(theapp->inspctn[8].lmax >=5000)			{theapp->inspctn[8].lmax=5000;}
	}
	else
	{
		theapp->inspctn[8].lmin+=res;
		if(theapp->inspctn[8].lmin >=5000)			{theapp->inspctn[8].lmin=5000;}
	}

	UpdateValues();
}

void Lim::On8dn() 
{
	if(Max)
	{
		theapp->inspctn[8].lmax-=res;
		if(theapp->inspctn[8].lmax <=0)				{theapp->inspctn[8].lmax=0;}

	}
	else
	{
		theapp->inspctn[8].lmin-=res;
		if(theapp->inspctn[8].lmin <=0)				{theapp->inspctn[8].lmin=0;}
	}

	UpdateValues();
}


void Lim::On8up2() 
{
	if(Max)
	{theapp->inspctn[8].lmax+=100;}
	else
	{theapp->inspctn[8].lmin+=100;}

	UpdateValues();
}

void Lim::On8dn2() 
{
	if(Max)
	{
		theapp->inspctn[8].lmax-=100;
		if(theapp->inspctn[8].lmax<=0)	{theapp->inspctn[8].lmax=0;}
	}
	else
	{
		theapp->inspctn[8].lmin-=100;
		if(theapp->inspctn[8].lmin<=0)	{theapp->inspctn[8].lmin=0;}
	}

	UpdateValues();
}

void Lim::On9up() 
{
	if(Max)
	{
		theapp->inspctn[9].lmax+=res;
		if(theapp->inspctn[9].lmax >=255)	{theapp->inspctn[9].lmax=255;}
	}
	else
	{
		theapp->inspctn[9].lmin+=res;
		if(theapp->inspctn[9].lmin >=255)	{theapp->inspctn[9].lmin=255;}
	}

	UpdateValues();
}

void Lim::On9dn() 
{
	if(Max)
	{
		theapp->inspctn[9].lmax-=res;
		if(theapp->inspctn[9].lmax <=0)		{theapp->inspctn[9].lmax=0;}
	}
	else
	{
		theapp->inspctn[9].lmin-=res;
		if(theapp->inspctn[9].lmax <=0)		{theapp->inspctn[9].lmax=0;}
	}

	UpdateValues();
}

void Lim::On10up() 
{
	if(Max)		{theapp->inspctn[10].lmax+=res;}
	else				{theapp->inspctn[10].lmin+=res;}

	UpdateValues();
}

void Lim::On10dn() 
{
	if(Max)
	{
		theapp->inspctn[10].lmax-=res;
		if(theapp->inspctn[10].lmax <=0)	{theapp->inspctn[10].lmax=0;}
	}
	else
	{
		theapp->inspctn[10].lmin-=res;
		if(theapp->inspctn[10].lmin <=0)	{theapp->inspctn[10].lmin=0;}
	}

	UpdateValues();
}

void Lim::On10up2() 
{
	if(Max)	{theapp->inspctn[10].lmax+=100;}
	else			{theapp->inspctn[10].lmin+=100;}

	UpdateValues();
}

void Lim::On10dn2() 
{
	if(Max)
	{
		theapp->inspctn[10].lmax-=100;
		if(theapp->inspctn[10].lmax <=0)
		{
			theapp->inspctn[10].lmax=0;
		}
	}
	else
	{
		theapp->inspctn[10].lmin-=100;
		if(theapp->inspctn[10].lmin <=0)
		{
			theapp->inspctn[10].lmin=0;
		}
	}

	UpdateValues();
}


void Lim::OnMax() 
{
	Max=true;
	m_m1.Format("%s","Max");
	m_m2.Format("%s","Max");
	m_m3.Format("%s","Max");
	m_m4.Format("%s","Max");
	m_m5.Format("%s","Max");
	m_m6.Format("%s","Max");
	m_m7.Format("%s","Max");
	m_m8.Format("%s","Max");
	m_m9.Format("%s","Max");
	m_m11.Format("%s","Max");
	m_m12.Format("%s","Max");

	UpdateDisplay();
}

void Lim::OnMin() 
{
	Max=false;
	m_m1.Format("%s","Min");
	m_m2.Format("%s","Min");
	m_m3.Format("%s","Min");
	m_m4.Format("%s","Min");
	m_m5.Format("%s","Min");
	m_m6.Format("%s","Min");
	m_m7.Format("%s","Min");
	m_m8.Format("%s","Min");
	m_m9.Format("%s","Min");
	m_m11.Format("%s","Min");
	m_m12.Format("%s","Min");

	UpdateDisplay();
}

void Lim::OnFast() 
{
	
	
}

void Lim::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{
		KillTimer(1);
		pframe->SaveJob(pframe->CurrentJobNum);
		pframe->m_pview->UpdateDisplay();
		pframe->m_pview->UpdateMinor();
	}
	
	CDialog::OnTimer(nIDEvent);
}

void Lim::OnE1() 
{
	if( theapp->inspctn[1].enable )
	{CheckDlgButton(IDC_E1,1); theapp->inspctn[1].enable=false;}
	else
	{CheckDlgButton(IDC_E1,0); theapp->inspctn[1].enable=true;}

	UpdateValues();
}

void Lim::OnE2() 
{
	if( theapp->inspctn[2].enable )
	{CheckDlgButton(IDC_E2,1); theapp->inspctn[2].enable=false;}
	else
	{CheckDlgButton(IDC_E2,0); theapp->inspctn[2].enable=true;}

	UpdateValues();
}

void Lim::OnE3() 
{
	if( theapp->inspctn[3].enable )
	{CheckDlgButton(IDC_E3,1); theapp->inspctn[3].enable=false;}
	else
	{CheckDlgButton(IDC_E3,0); theapp->inspctn[3].enable=true;}

	UpdateValues();
}

void Lim::OnE4() 
{
	if( theapp->inspctn[4].enable )
	{CheckDlgButton(IDC_E4,1); theapp->inspctn[4].enable=false;}
	else
	{CheckDlgButton(IDC_E4,0); theapp->inspctn[4].enable=true;}

	UpdateValues();
}

void Lim::OnE5() 
{
	if( theapp->inspctn[5].enable )
	{CheckDlgButton(IDC_E5,1); theapp->inspctn[5].enable=false;}
	else
	{CheckDlgButton(IDC_E5,0); theapp->inspctn[5].enable=true;}

	UpdateValues();
}

void Lim::OnE6() 
{
	if( theapp->inspctn[6].enable )
	{CheckDlgButton(IDC_E6,1); theapp->inspctn[6].enable=false;}
	else
	{CheckDlgButton(IDC_E6,0); theapp->inspctn[6].enable=true;}
	
	UpdateValues();
}

void Lim::OnE7() 
{
	if( theapp->inspctn[7].enable )
	{CheckDlgButton(IDC_E7,1); theapp->inspctn[7].enable=false;}
	else
	{CheckDlgButton(IDC_E7,0); theapp->inspctn[7].enable=true;}
	
	UpdateValues();
}

void Lim::OnE8() 
{
	if( theapp->inspctn[8].enable )
	{CheckDlgButton(IDC_E8,1); theapp->inspctn[8].enable=false;}
	else
	{CheckDlgButton(IDC_E8,0); theapp->inspctn[8].enable=true;}
	
	UpdateValues();
}


void Lim::OnE9() 
{
	if( theapp->inspctn[9].enable )
	{CheckDlgButton(IDC_E9,1); theapp->inspctn[9].enable=false;}
	else
	{CheckDlgButton(IDC_E9,0); theapp->inspctn[9].enable=true;}
	
	UpdateValues();
}


void Lim::OnE10() 
{
	if( theapp->inspctn[10].enable )
	{CheckDlgButton(IDC_E10,1); theapp->inspctn[10].enable=false;}
	else
	{CheckDlgButton(IDC_E10,0); theapp->inspctn[10].enable=true;}

	UpdateValues();
}

void Lim::OnE11() 
{
	if( theapp->inspctn[11].enable )
	{
		CheckDlgButton(IDC_E11,1); 
		theapp->inspctn[11].enable=false;
	}
	else
	{
		CheckDlgButton(IDC_E11,0); 
		theapp->inspctn[11].enable=true;
	}

	UpdateValues();
}


void Lim::On11up() 
{
	if(Max)
	{
		theapp->inspctn[11].lmax+=res;
		if(theapp->inspctn[11].lmax >=AbsoluteMax2)
		{
			theapp->inspctn[11].lmax=AbsoluteMax;
		}
	}
	else
	{
		theapp->inspctn[11].lmin+=res;
		if(theapp->inspctn[11].lmin >=AbsoluteMax2)
		{
			theapp->inspctn[11].lmin=AbsoluteMax;
		}
	}

	UpdateValues();
}

void Lim::On11dn() 
{
	if(Max)
	{
		theapp->inspctn[11].lmax-=res;
		if(theapp->inspctn[11].lmax <=0)				
		{
			theapp->inspctn[11].lmax=0;
		}
	}
	else
	{
		theapp->inspctn[11].lmin-=res;
		if(theapp->inspctn[11].lmin <=0)			
		{
			theapp->inspctn[11].lmin=0;
		}
	}

	UpdateValues();
}


void Lim::UpdateValues()
{
	SetTimer(1,300,NULL);
	UpdateDisplay();
}

void Lim::UpdateVisibilityInspection12()
{
	//align unavailable
	if(!theapp->alignAvailable)
	{
		int Insp12Visibility = (theapp->jobinfo[pframe->CurrentJobNum].bottleStyle == 10)?SW_SHOW:SW_HIDE; // show I12 only if job is a square bottle
		GetDlgItem(IDC_ALIGNMENT_GROUP_BOX)->ShowWindow(Insp12Visibility);
		GetDlgItem(IDC_L12)->ShowWindow(Insp12Visibility);
		GetDlgItem(IDC_M12)->ShowWindow(Insp12Visibility);
		GetDlgItem(IDC_MAX12)->ShowWindow(Insp12Visibility);
		GetDlgItem(IDC_12)->ShowWindow(Insp12Visibility);
		GetDlgItem(IDC_MIN12)->ShowWindow(Insp12Visibility);
		GetDlgItem(IDC_E12)->ShowWindow(Insp12Visibility);
		GetDlgItem(IDC_12UP)->ShowWindow(Insp12Visibility);
		GetDlgItem(IDC_12DN)->ShowWindow(Insp12Visibility);
	}
}

void Lim::OnFine() 
{
	if(fine)
	{
		fine=false;
		m_fine.SetWindowText("Fast");
		CheckDlgButton(IDC_FINE,1); 
		res=10;
	}
	else
	{
		fine=true; 
		m_fine.SetWindowText("Slow");
		CheckDlgButton(IDC_FINE,0); 
		res=1;
	}
	
	UpdateData(false);	
}

BOOL Lim::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
//LPTSTR tempBuf = _tgetcwd(NULL,_MAX_PATH);
//tempStr = tempBuf;
//free(tempBuf);

////////////////////	
	
/*	free(static_m_1);
	free(static_m_2);
	free(static_m_3);
	free(static_m_4);
	free(static_m_5);
	free(static_m_6);
	free(static_m_7);
	free(static_m_8);
	free(static_m_9);
	free(static_m_10);

	free(static_m_m1);
	free(static_m_m2);
	free(static_m_m3);
	free(static_m_m4);
	free(static_m_m5);
	free(static_m_m6);
	free(static_m_m7);
	free(static_m_m8);
	free(static_m_m9);
	free(static_m_m10);

	free(static_m_l1);
	free(static_m_l2);
	free(static_m_l3);
	free(static_m_l4);
	free(static_m_l5);
	free(static_m_l6);
	free(static_m_l7);
	free(static_m_l8);
	free(static_m_l9);
	free(static_m_l10);

	free(static_m_1max);
	free(static_m_2max);
	free(static_m_3max);
	free(static_m_4max);
	free(static_m_5max);
	free(static_m_6max);
	free(static_m_7max);
	free(static_m_8max);
	free(static_m_9max);
	free(static_m_10max);

	free(static_m_1min);
	free(static_m_2min);
	free(static_m_3min);
	free(static_m_4min);
	free(static_m_5min);
	free(static_m_6min);
	free(static_m_7min);
	free(static_m_8min);
	free(static_m_9min);
	free(static_m_10min);
*/



/*	m_1.Empty();	m_1.FreeExtra();
	m_2.Empty();	m_2.FreeExtra();
	m_3.Empty();	m_3.FreeExtra();
	m_4.Empty();	m_4.FreeExtra();
	m_5.Empty();	m_5.FreeExtra();
	m_6.Empty();	m_6.FreeExtra();
	m_7.Empty();	m_7.FreeExtra();
	m_8.Empty();	m_8.FreeExtra();
	m_9.Empty();	m_9.FreeExtra();
	m_10.Empty();	m_10.FreeExtra();

	m_m1.Empty();	m_m1.FreeExtra();
	m_m2.Empty();	m_m2.FreeExtra();
	m_m3.Empty();	m_m3.FreeExtra();
	m_m4.Empty();	m_m4.FreeExtra();
	m_m5.Empty();	m_m5.FreeExtra();
	m_m6.Empty();	m_m6.FreeExtra();
	m_m7.Empty();	m_m7.FreeExtra();
	m_m8.Empty();	m_m8.FreeExtra();
	m_m9.Empty();	m_m9.FreeExtra();
	m_m10.Empty();	m_m10.FreeExtra();

	m_l1.Empty();	m_l1.FreeExtra();
	m_l2.Empty();	m_l2.FreeExtra();
	m_l3.Empty();	m_l3.FreeExtra();
	m_l4.Empty();	m_l4.FreeExtra();
	m_l5.Empty();	m_l5.FreeExtra();
	m_l6.Empty();	m_l6.FreeExtra();
	m_l7.Empty();	m_l7.FreeExtra();
	m_l8.Empty();	m_l8.FreeExtra();
	m_l9.Empty();	m_l9.FreeExtra();
	m_l10.Empty();	m_l10.FreeExtra();

	m_1max.Empty();	m_1max.FreeExtra();
	m_2max.Empty();	m_2max.FreeExtra();
	m_3max.Empty();	m_3max.FreeExtra();
	m_4max.Empty();	m_4max.FreeExtra();
	m_5max.Empty();	m_5max.FreeExtra();
	m_6max.Empty();	m_6max.FreeExtra();
	m_7max.Empty();	m_7max.FreeExtra();
	m_8max.Empty();	m_8max.FreeExtra();
	m_9max.Empty();	m_9max.FreeExtra();
	m_10max.Empty();m_10max.FreeExtra();

	m_1min.Empty();	m_1min.FreeExtra();
	m_2min.Empty();	m_2min.FreeExtra();
	m_3min.Empty();	m_3min.FreeExtra();
	m_4min.Empty();	m_4min.FreeExtra();
	m_5min.Empty();	m_5min.FreeExtra();
	m_6min.Empty();	m_6min.FreeExtra();
	m_7min.Empty();	m_7min.FreeExtra();
	m_8min.Empty();	m_8min.FreeExtra();
	m_9min.Empty();	m_9min.FreeExtra();
	m_10min.Empty();m_10min.FreeExtra();*/


	return CDialog::DestroyWindow();
}

void Lim::OnE12() 
{
	if( theapp->inspctn[12].enable )
	{
		CheckDlgButton(IDC_E12,1); 
		theapp->inspctn[12].enable=false;
	}
	else
	{
		CheckDlgButton(IDC_E12,0); 
		theapp->inspctn[12].enable=true;
	}

	UpdateValues();
	
}

void Lim::On12up() 
{
	if(Max)
	{
		theapp->inspctn[12].lmax+=res;
		if(theapp->inspctn[12].lmax >=AbsoluteMax2)
		{
			theapp->inspctn[12].lmax=AbsoluteMax;
		}
	}
	else
	{
		theapp->inspctn[12].lmin+=res;
		if(theapp->inspctn[12].lmin >=AbsoluteMax2)
		{
			theapp->inspctn[12].lmin=AbsoluteMax;
		}
	}

	UpdateValues();	
}

void Lim::On12dn() 
{
	if(Max)
	{
		theapp->inspctn[12].lmax-=res;
		if(theapp->inspctn[12].lmax <=0)				
		{
			theapp->inspctn[12].lmax=0;
		}
	}
	else
	{
		theapp->inspctn[12].lmin-=res;
		if(theapp->inspctn[12].lmin <=0)			
		{
			theapp->inspctn[12].lmin=0;
		}
	}

	UpdateValues();	
}
