// BottleView.h : interface of the CBottleView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_BOTTLEVIEW_H__20EE6241_3924_421E_BB9F_B57BE3250F9D__INCLUDED_)
#define AFX_BOTTLEVIEW_H__20EE6241_3924_421E_BB9F_B57BE3250F9D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "BottleDoc.h"

class CBottleView : public CFormView
{
	friend class CMainFrame;
	friend class CBottleApp;

protected: // create from serialization only
	CBottleView();
	DECLARE_DYNCREATE(CBottleView)

public:
	CMainFrame* pframe;
	CBottleApp* theapp;
	
	CString m_currentjob;
	int BoxColor;
	bool ToggleColor;
	bool EjectAll;
	bool toggle;
	
	void UpdateMinor();
	void OnlineBox(int BoxColor);
	void UpDateEject();
	void UpdateDisplay();
	void UpdateDisplay2();

	CString m_enable1;
	CString m_enable2;
	CString m_enable5;

	//{{AFX_DATA(CBottleView)
	enum { IDD = IDD_BOTTLE_FORM };
	CStatic	m_bxRes12;
	CStatic	m_bxRes11;
	CStatic	m_bxRes10;
	CStatic	m_bxRes8;
	CStatic	m_resultBx;
	CStatic	m_bxRes9;
	CStatic	m_bxRes7;
	CStatic	m_bxRes6;
	CStatic	m_bxRes5;
	CStatic	m_bxRes4;
	CStatic	m_bxRes3;
	CStatic	m_bxRes2;
	CStatic	m_bxRes1;
	CEdit	m_statuscontrol;
	CEdit	m_camser5;
	CEdit	m_camser3;
	CEdit	m_camser4;
	CEdit	m_camser2;
	CEdit	m_camser1;
	CString	m_total;
	CString	m_rejects;
	CString	m_status;
	CString	m_result2;
	CString	m_result3;
	CString	m_result4;
	CString	m_result5;
	CString	m_result6;
	CString	m_result7;
	CString	m_result8;
	CString	m_time;
	CString	m_h1;
	CString	m_h2;
	CString	m_h3;
	CString	m_h4;
	CString	m_h5;
	CString	m_h6;
	CString	m_h7;
	CString	m_h8;
	CString	m_insp1;
	CString	m_insp2;
	CString	m_insp3;
	CString	m_insp4;
	CString	m_insp5;
	CString	m_insp6;
	CString	m_insp7;
	CString	m_insp8;
	CString	m_l1;
	CString	m_l2;
	CString	m_l3;
	CString	m_l4;
	CString	m_l5;
	CString	m_l6;
	CString	m_l7;
	CString	m_l8;
	CString	m_editcam1;
	CString	m_editcam2;
	CString	m_editcam3;
	CString	m_editcam4;
	CString	m_editcam5;
	CString	m_time1;
	CString	m_time2;
	CString	m_time3;
	CString	m_time4;
	CString	m_time5;
	CString	m_shutter;
	CString	m_result1r;
	CString	m_inspdelay;
	CString	m_result1l;
	CString	m_b1;
	CString	m_b2;
	CString	m_b3;
	CString	m_b4;
	CString	m_h9;
	CString	m_insp9;
	CString	m_result9;
	CString	m_l9;
	CString	m_shutter2;
	CString	m_label;
	CString	m_bottle;
	CString	m_camrestart;
	CString	m_result10;
	CString	m_l10;
	CString	m_h10;
	CString	m_insp10;
	CString	m_result11;
	CString	m_l11;
	CString	m_h11;
	CString	m_insp11;
	CString	m_result12;
	CString	m_l12;
	CString	m_h12;
	CString	m_insp12;
	CString	m_deltaC1;
	CString	m_deltaC2;
	CString	m_deltaC3;
	CString	m_deltaC4;
	//}}AFX_DATA

// Attributes
public:
	CBottleDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBottleView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	//}}AFX_VIRTUAL

// Implementation
public:
	void UpdateValues();
	virtual ~CBottleView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CBottleView)
	afx_msg void OnCont();
	afx_msg void OnTrig1();
	afx_msg void OnRtot();
	afx_msg void OnRejectall();
	afx_msg void OnFreeze();
	afx_msg void OnFreeze2();
	afx_msg void OnOnline();
	afx_msg void OnOffline();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnTesteject();
	afx_msg void OnMore3();
	afx_msg void OnLess3();
	afx_msg void OnCambutton();
	afx_msg void OnShowjob();
	afx_msg void OnResetcams();
	afx_msg void OnMore5();
	afx_msg void OnLess4();
	afx_msg void OnTest24();
	afx_msg void OnNomot();
	afx_msg void OnMore6();
	afx_msg void OnLess5();
	afx_msg void OnEject2();
	afx_msg void OnEnablesec();
	afx_msg void OnPassnotfinished();
	afx_msg void OnTempPlc();
	afx_msg void OnTempUsb();
	afx_msg void OnTempNone();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in BottleView.cpp
inline CBottleDoc* CBottleView::GetDocument()
   { return (CBottleDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BOTTLEVIEW_H__20EE6241_3924_421E_BB9F_B57BE3250F9D__INCLUDED_)
