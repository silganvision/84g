#if !defined(AFX_LABELMATINGSETUP_H__CDF68C5C_54F4_4EBD_A61C_6A941BAEDB20__INCLUDED_)
#define AFX_LABELMATINGSETUP_H__CDF68C5C_54F4_4EBD_A61C_6A941BAEDB20__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LabelMatingSetup.h : header file
//


#include "XAxisView.h"




/////////////////////////////////////////////////////////////////////////////
// LabelMatingSetup dialog

#define smallPixelStepSize 1
#define largePixelStepSize 10

#define smallPixelIntensityStepSize 1
#define largePixelIntensityStepSize 5

class LabelMatingSetup : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
// Construction
public:
	LabelMatingSetup(CWnd* pParent = NULL);   // standard constructor


	CBottleApp *theapp;
	CMainFrame *pframe;

	int pixelStepSize;
	int pixelIntensityStepSize;
	bool isSmallStepSize;

	void UpdateAndSaveSetupValues();
	void UpdateDisplay();



// Dialog Data
	//{{AFX_DATA(LabelMatingSetup)
	enum { IDD = IDD_LABEL_MATING_SETUP };
	CButton	m_stepSize;
	CString	m_taughtR;
	CString	m_taughtG;
	CString	m_taughtB;
	CString	m_RTol;
	CString	m_GTol;
	CString	m_BTol;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(LabelMatingSetup)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(LabelMatingSetup)
	virtual BOOL OnInitDialog();
	afx_msg void OnStepSize();
	afx_msg void OnXLeft();
	afx_msg void OnXRight();
	afx_msg void OnYUp();
	afx_msg void OnYDown();
	afx_msg void OnWidthLess();
	afx_msg void OnWidthMore();
	afx_msg void OnHeightMore();
	afx_msg void OnHeightLess();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnXLeft2();
	afx_msg void OnXRight2();
	afx_msg void OnYUp2();
	afx_msg void OnYDown2();
	afx_msg void OnWidthLess2();
	afx_msg void OnWidthMore2();
	afx_msg void OnHeightMore2();
	afx_msg void OnHeightLess2();
	afx_msg void OnTeachColor();
	afx_msg void OnTolRUp();
	afx_msg void OnTolRDown();
	afx_msg void OnTolGUp();
	afx_msg void OnTolGDown();
	afx_msg void OnTolBUp();
	afx_msg void OnTolBDown();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LABELMATINGSETUP_H__CDF68C5C_54F4_4EBD_A61C_6A941BAEDB20__INCLUDED_)
