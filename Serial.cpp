// Serial.cpp : implementation file
//
#include "stdafx.h"
#include "Bottle.h"
#include "Serial.h"
#include "mainfrm.h"
#include "bottleview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Serial dialog

Serial::Serial(CWnd* pParent /*=NULL*/)
	: CDialog(Serial::IDD, pParent)
{
	//{{AFX_DATA_INIT(Serial)
	m_chars = _T("");
	m_chars2 = _T("");
	m_commcount = _T("");
	m_filler = _T("");
	m_motor = _T("");
	m_capper = _T("");
	m_length = _T("");
	m_length2 = _T("");
	m_length3 = _T("");
	m_length4 = _T("");
	m_chars1 = _T("");
	m_chars3 = _T("");
	m_chars4 = _T("");
	m_chars5 = _T("");
	m_editinchar = _T("");
	m_ch = _T("");
	m_temp = _T("");
	//}}AFX_DATA_INIT
}

void Serial::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Serial)
	DDX_Control(pDX, IDC_MSCOMM1, m_serial);
	DDX_Text(pDX, IDC_CHARS, m_chars);
	DDX_Text(pDX, IDC_CHARS2, m_chars2);
	DDX_Text(pDX, IDC_COMMCOUNT, m_commcount);
	DDX_Text(pDX, IDC_FILLER, m_filler);
	DDX_Text(pDX, IDC_MOTOR, m_motor);
	DDX_Text(pDX, IDC_CAPPER, m_capper);
	DDX_Text(pDX, IDC_LENGTH, m_length);
	DDX_Text(pDX, IDC_LENGTH2, m_length2);
	DDX_Text(pDX, IDC_LENGTH3, m_length3);
	DDX_Text(pDX, IDC_LENGTH4, m_length4);
	DDX_Text(pDX, IDC_CHARS1, m_chars1);
	DDX_Text(pDX, IDC_CHARS3, m_chars3);
	DDX_Text(pDX, IDC_CHARS4, m_chars4);
	DDX_Text(pDX, IDC_CHARS5, m_chars5);
	DDX_Text(pDX, IDC_EDITINCHAR, m_editinchar);
	DDX_Text(pDX, IDC_CH, m_ch);
	DDX_Text(pDX, IDC_TEMP, m_temp);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(Serial, CDialog)
	//{{AFX_MSG_MAP(Serial)
	ON_WM_CLOSE()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Serial message handlers

//////////////////////////////////////////////////////////////////////////////////
//To change from 12 to 16 bits serial communication or visceversa
//Go to this functions and uncomment what is necessary
//
//1. OnInitDialog()
//2. SendChar(int data, int function)
//3. GetChar(int FromPLC)
//4. OnTimer will not be necessary to change, because Timer3 is called only for
//16BITS and Timer1 is only called for 12BITS

//////////////////////////////////////////////////////////////////////////////////

BOOL Serial::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pframe		=	(CMainFrame*)AfxGetMainWnd();
	pframe->m_pserial=this;

	theapp		=	(CBottleApp*)AfxGetApp();
	returnedInt	=	0;
	noCommCount	=	0;
	didComOnce	=	false;

	m_serial.SetOutBufferSize(128);//128
	m_serial.SetInBufferSize(128);//128	
	m_serial.SetCommPort(1);
	m_serial.SetSettings("38400,n,8,1");//19200  38400
	
	CommProblem=false;
	count=0;
	inQue=0;
	lockOut=false;
	inSendChar=0;
	currentFillerPosition=FillPos=currentCapperPosition=CapPos=0;
	
//	//this is for 16BITS communication, comment it if working with 12 bits
	SetTimer(3,4,NULL);
	
	try
	{
		m_serial.SetPortOpen(true);
		theapp->commProblem=false;
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Warn the user.
		CommProblem=true;
		theapp->commProblem=true;
		AfxMessageBox("The serial port could not be opened.");
	}

	long commID=m_serial.GetCommID();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Serial::ShutDown()
{
	m_serial.SetPortOpen(false);
	KillTimer(1);
	KillTimer(2);
	KillTimer(3);
}

void Serial::OnClose()
{
	m_serial.SetPortOpen(false);
	KillTimer(1);
	KillTimer(2);
	CDialog::OnClose();
}

void Serial::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}


void Serial::OnTimer(UINT nIDEvent) 
{
	//Timer 1 only works for 12BITS communication
	if(nIDEvent==1)
	{
		noCommCount+=1;
		GetChar(1);
		
		if(returnedInt==sentInt)
		{
			KillTimer(1);
			SendChar(0,0);
			m_commcount.Format("%i",noCommCount);
			noCommCount=0;
			UpdateData(false);
		}

		if(noCommCount>=50)
		{
			KillTimer(1); 
			noCommCount=0;

			if(didComOnce)
			{pframe->m_pview->m_status="Comm Problem";}

			didComOnce=true;
		}
	}

	if(nIDEvent==2)
	{

	}

	//Timer 3 only works for 16BITS communication
	if(nIDEvent==3)
	{
		GetChar(1);

		if(noCommCount>=50)
		{
			noCommCount=0;
			//SendChar(0,0);//not sure if this should be commented
			comString=itoa(noCommCount,bufz,10);
			pframe->m_pview->m_status="Comm" + comString;
			ShowResults(false);
			didComOnce=true;
		}

		//SendChar(0,0);//not sure if this should be commented
	}

	if(nIDEvent==4)
	{

	}

	CDialog::OnTimer(nIDEvent);
}

void Serial::ShowResults(bool ok)
{
	if(ok)
	{
		b=CharsFromPLC.Right(14);
		Temperature=atoi(b.Left(3));

		b=CharsFromPLC.Right(11);
		MotorPos=atoi(b.Left(3));
		
		b=CharsFromPLC.Right(8);
		currentFillerPosition=FillPos=atoi(b.Left(3));

		if(currentFillerPosition>=120)	{currentFillerPosition=120;}
		if(currentFillerPosition<=0)	{currentFillerPosition=0;}

		b=CharsFromPLC.Right(5);
		currentCapperPosition=CapPos=atoi(b.Left(2));

		if(currentCapperPosition>=32)	{currentCapperPosition=32;}
		if(currentCapperPosition<=0)	{currentCapperPosition=0;}
		
		m_temp.Format("%i",Temperature );
		m_motor.Format("%i",MotorPos );
		m_filler.Format("%i",FillPos );
		m_capper.Format("%i",CapPos );

		if(MotorPos==666)
		{
			if(pframe->OnLine)
			{
				pframe->IOReject=true;
				theapp->rejectAllKrones=true;
			}
		}
		else
		{
			theapp->rejectAllKrones=false;
		}

		theapp->resetFromPLC = (777 == MotorPos);
		
		////////////////////////////////////////////////////////
		//Temperature Update
		if(theapp->tempSelect==1) //Serial Temp Sensor
		{
			theapp->Temperature = Temperature;
			
			if(theapp->Temperature>=theapp->tempLimit)
			{
				pframe->m_pview->m_status="Cabinet Temp Exceeded!!!";
				pframe->m_pview->UpdateDisplay();
			}
		}
		////////////////////////////////////////////////////////

	}

	CharsFromPLC=CharsFromPLC.Right(16);
	m_chars.Format("%s",CharsFromPLC );
	m_chars2.Format("%s",CharsToPLC );	
	m_commcount.Format("%i",noCommCount2);
		
	UpdateData(false);
}

bool Serial::SendChar(int data, int function)
{
	bool result=false;

	//Sendchar for 16BITS communication
	L1=function + 10000;//add check code
	itoa(L1,st1,10);
	f1=st1;
	f1=f1.Right(4);
	
	L2=data+10000000;
	itoa(L2,st2,10);
	d1=st2;
	d1=d1.Right(7);

	strcpy(st,d1);
	strcat(st,f1);
	ch=st;
	ch="%0000"+ch;

	sentChar=ch.Right(3);
	buf.vt=VT_BSTR;
	CharsToPLC=buf.bstrVal=ch.AllocSysString();

	m_serial.SetOutput(buf);
	
	VariantClear(&buf);
	ch.FreeExtra();

	noCommCount+=1;

	m_chars2.Format("%s",CharsToPLC );		//Load send string to GUI variable for display
	UpdateData(false);

	/*
	/////////////////////////////////////////
	//Sendchar for 12BITS communication
	L1=function + 10000;//add check code
	itoa(L1,st1,10);
	f1=st1;
	f1=f1.Right(4);

	L2=data+10000000;
	itoa(L2,st2,10);
	d1=st2;
	d1=d1.Right(7);

	strcpy(st,d1);
	strcat(st,f1);
	ch=st;
	ch="%"+ch;

	sentInt=atoi(ch.Right(3));
	buf.vt=VT_BSTR;
	CharsToPLC=buf.bstrVal=ch.AllocSysString();

	m_chars2.Format("%s",CharsToPLC );
	m_serial.SetOutput(buf);

	VariantClear(&buf);
	ch.FreeExtra();

	if(function!=0)
	{SetTimer(1,1,NULL);}//low 17
	

	/////////////////////////////////////////
	
	*/

	return result;
}

void Serial::GetChar(int FromPLC)
{

	/////////////////////////////////////////
	//GetChar for 16BITS communication
	m_serial.SetRThreshold(1);
	
	buf2=m_serial.GetInput();
	short event=m_serial.GetCommEvent();
	
	CharsFromPLC1=buf2.bstrVal;
	VariantClear(&buf2);
	
	returnedChar="";

	for(int i=1; i<=16; i++)
	{
		b=CharsFromPLC1.Left(32);
		b=b.Right(16+i);
		a=b.Right(16+i);
		b=b.Left(1);

		if(b=="+")
		{	
			CharsFromPLC=a.Left(16);
			returnedChar=CharsFromPLC.Right(3);
			i=16;

			int temp_motor_pos;

			temp_motor_pos=atoi(CharsFromPLC.Mid(5,3));		//parse out the current motors position.

			if(temp_motor_pos != 666)						//If motor position is not 666 then eject from 
															//user is not enabled and this the actual position.
			{

				MotorPos=atoi(CharsFromPLC.Mid(5,3));		//parse out the current motors position.
			//	theapp->rejectFromUser=false;				//Set flag indicating that eject signal from user was not received.
			//	pframe->is666received=false;				//Set flag indicating that eject signal from user was not received.
			}
			else if(temp_motor_pos ==666)					//If eject signal from user was received. 
			{
					if(pframe->OnLine==true)				//If ejector is on-line
					{
						pframe->IOReject=true;				//Set flag indicating that eject signal from user was received.
					//	theapp->rejectFromUser=true;		//Set flag indicating that eject signal from user was received.
					}
			//		pframe->is666received=true; //For consecutive Reject signal from External I/O (client input onPLC)

			}
		}
	}

	if(returnedChar=="789")//sentChar)
	{
		noCommCount2=noCommCount;
		ShowResults(true);
		//SendChar(0,0);
		noCommCount=0;
	}

	/*
	/////////////////////////////////////////

	//GetChar for 12BITS communication
	m_serial.SetRTSEnable(true);
	
	buf2=m_serial.GetInput();
	short event=m_serial.GetCommEvent();
	
	CharsFromPLC=buf2.bstrVal;
	VariantClear(&buf2);

	returnedInt=atoi(CharsFromPLC.Right(3));

	b=CharsFromPLC.Right(3);
	StatusByte=atoi(b.Left(1));

	b=CharsFromPLC.Right(5);

	b=CharsFromPLC.Right(6);
	MotorPos=atoi(b.Left(3));
	if(MotorPos==666) theapp->rejectAllKrones=true;
	if(MotorPos!=666) theapp->rejectAllKrones=false;
	if(MotorPos==777) theapp->resetFromPLC=true;
	if(MotorPos!=777) theapp->resetFromPLC=false;

	CharsFromPLC=CharsFromPLC.Right(9);
	m_chars.Format("%s",CharsFromPLC );
	UpdateData(false);

	/////////////////////////////////////////
	*/
}


BOOL Serial::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class

	return CDialog::DestroyWindow();
}
