#if !defined(AFX_MODIFY_H__AB86F5B7_6566_4789_BB90_091B16BBB445__INCLUDED_)
#define AFX_MODIFY_H__AB86F5B7_6566_4789_BB90_091B16BBB445__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Modify.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Modify dialog

class Modify : public CDialog
{
	friend class CBottleApp;
	friend class CMainFrame;
// Construction
public:
	void setAllToFalse();
	void UpdateDisplay2();
	void UpdateValues();
	bool newTarget;
	bool toggle;
	void UpdateDisplay();
	int res;
	Modify(CWnd* pParent = NULL);   // standard constructor

	CBottleApp *theapp;
	CMainFrame* pframe;

	CString m_fine;
	CString m_enable;

// Dialog Data
	//{{AFX_DATA(Modify)
	enum { IDD = IDD_MODIFY };
	CEdit	m_editec;
	CStatic	m_name2;
	CString	m_capneck;
	CString	m_capmiddle;
	CString	m_captop;
	CString	m_edite;
	CString	m_capradius;
	CString	m_editdec;
	CString	m_editcode;
	CString	m_editsize2;
	CString	m_editbody;
	CString	m_editopos;
	CString	m_editosen;
	CString	m_pos1;
	CString	m_pos2;
	CString	m_pos3;
	CString	m_pos4;
	CString	m_pos5;
	CString	m_pos6;
	CString	m_pos7;
	CString	m_pos8;
	CString	m_editint;
	CString	m_pos10;
	CString	m_pos11;
	CString	m_pos9;
	CString	m_pos13;
	CString	m_picNum;
	CString	m_corrLeftC1;
	CString	m_corrLeftC2;
	CString	m_corrLeftC3;
	CString	m_corrLeftC4;
	CString	m_corrLef2C1;
	CString	m_corrLef2C2;
	CString	m_corrLef2C3;
	CString	m_corrLef2C4;
	CString	m_modl6;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Modify)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Modify)
	afx_msg void OnMup();
	afx_msg void OnMdn();
	afx_msg void OnNup();
	afx_msg void OnNdn();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnPaint();
	afx_msg void OnLon();
	afx_msg void OnLoff();
	afx_msg void OnOsenmore();
	afx_msg void OnOsenless();
	afx_msg void OnOposup();
	afx_msg void OnOposdn();
	afx_msg void OnM1();
	afx_msg void OnM2();
	afx_msg void OnM3();
	afx_msg void OnM4();
	afx_msg void OnM5();
	afx_msg void Ons1();
	afx_msg void Ons2();
	afx_msg void Ons3();
	afx_msg void Ons4();
	afx_msg void Ons5();
	afx_msg void OnFinemod();
	afx_msg void OnIntup();
	afx_msg void OnIntdn();
	afx_msg void OnToggle();
	afx_msg void OnM9();
	afx_msg void OnS9();
	afx_msg void OnM10();
	afx_msg void OnS10();
	afx_msg void OnM11();
	afx_msg void Ons11();
	afx_msg void OnS13();
	afx_msg void OnM13();
	afx_msg void OnEnapicstoshow();
	afx_msg void OnPictoshowup();
	afx_msg void OnPictoshowdn();
	afx_msg void OnShowsetupi5();
	afx_msg void OnShowsetupi8();
	afx_msg void OnFindmiddles();
	afx_msg void OnShowmiddles();
	afx_msg void OnMiddlesetup();
	afx_msg void OnShowmagenta();
	afx_msg void OnShowcyan();
	afx_msg void OnShowyellow();
	afx_msg void OnShowsaturation();
	afx_msg void OnShowbw();
	afx_msg void OnShowred();
	afx_msg void OnShiftedsetup();
	afx_msg void OnShowedges();
	afx_msg void OnShowsetupi7();
	afx_msg void OnLabelintset();
	afx_msg void OnShowgreen();
	afx_msg void OnShowblue();
	afx_msg void OnNolabelsetup();
	afx_msg void OnShowedgeh();
	afx_msg void OnShowedgev();
	afx_msg void OnShowbin();
	afx_msg void OnShowedgefinal();
	afx_msg void OnTeach();
	afx_msg void OnShowsatur2();
	afx_msg void OnShowbin1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODIFY_H__AB86F5B7_6566_4789_BB90_091B16BBB445__INCLUDED_)
