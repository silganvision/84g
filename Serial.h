//{{AFX_INCLUDES()
#include "mscomm.h"
//}}AFX_INCLUDES
#if !defined(AFX_SERIAL_H__F5F83A22_CAB3_41D2_908A_29AA2FA09B2F__INCLUDED_)
#define AFX_SERIAL_H__F5F83A22_CAB3_41D2_908A_29AA2FA09B2F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Serial.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Serial dialog
typedef struct  {
	int data; 
	int function; } mA;

class Serial : public CDialog
{
	friend class CMainFrame;
	friend class CBottleApp;
	// Construction
public:

	CString comString;
	CString CharsToPLC;
	CString CharsFromPLC;
	CString CharsFromPLC1;
	CString ch;
	CString ch2;
	CString a;
	CString CharBuf1;
	CString CharBuf2;
	CString CharBuf3;
	CString CharBuf4;
	CString CharBuf5;
	CString returnedChar;
	CString sentChar;
	CString b;
	CString f1;
	CString d1;
	CString d2;

	char st[20];
	char st1[20];
	char st2[20];



	bool lockOut;

	int inSendChar;

	int currentCapperPosition;
	int currentFillerPosition;
	int fillerPos;
	int datax;
	int functionx;
	mA messArray[20];
	int len1;
	int len2;
	int len3;
	int len4;
	void ShowResults(bool ok);
	char bufz[10];
	bool didComOnce;
	int count;
	int count2;
	void ShutDown();
	int noCommCount;
	int noCommCount2;
	int sentInt;
	int returnedInt;
	int MotorPos;
	int Temperature;
	int StatusByte;
	bool CommProblem;
	int CapPos;
	int FillPos;
	int inQue;
	long ControlChars;
	long ControlChars2;
	Serial(CWnd* pParent = NULL);   // standard constructor
	void GetChar(int FromPLC);

	bool SendChar(int data, int function);


	VARIANT buf;
	VARIANT buf2;
	long L1;
	long L2;
// Dialog Data
	//{{AFX_DATA(Serial)
	enum { IDD = IDD_SERIAL };
	CMSComm	m_serial;
	CString	m_chars;
	CString	m_chars2;
	CString	m_commcount;
	CString	m_endchar;
	CString	m_filler;
	CString	m_motor;
	CString	m_capper;
	CString	m_length;
	CString	m_length2;
	CString	m_length3;
	CString	m_length4;
	CString	m_chars1;
	CString	m_chars3;
	CString	m_chars4;
	CString	m_chars5;
	CString	m_editinchar;
	CString	m_ch;
	CString	m_temp;
	//}}AFX_DATA

	CMainFrame* pframe;
	CBottleApp* theapp;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Serial)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	
// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Serial)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnClose();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIAL_H__F5F83A22_CAB3_41D2_908A_29AA2FA09B2F__INCLUDED_)
