// Setup.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "Setup.h"
#include "mainfrm.h"
#include "serial.h"
#include "xaxisview.h"
#include "bottleview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Setup dialog


Setup::Setup(CWnd* pParent /*=NULL*/)
	: CDialog(Setup::IDD, pParent)
{
	//{{AFX_DATA_INIT(Setup)
	m_zvalue = _T("");
	m_fvalue = _T("");
	//}}AFX_DATA_INIT
}


void Setup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Setup)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Setup, CDialog)
	//{{AFX_MSG_MAP(Setup)
	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Setup message handlers

BOOL Setup::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_psetup=this;
	theapp=(CBottleApp*)AfxGetApp();

	UpdateData(false);
		
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Setup::OnZin() 
{

}

void Setup::CalculateChars()
{
	
}



void Setup::OnZout() 
{
	
}


void Setup::OnOK() 
{
	
}


void Setup::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{
	
		
		
	}

	if(nIDEvent==2)
	{
		
	}

	if(nIDEvent==3)
	{
		
	}
	
	CDialog::OnTimer(nIDEvent);
}

void Setup::OnAop() 
{
	
	
}

void Setup::OnAcl() 
{
	
	
	
}

void Setup::OnFin() 
{
	
	
}

void Setup::OnFfar() 
{
	
	
}
/////////////////////////////////////////////////////////////////////////////
// SDelay dialog


SDelay::SDelay(CWnd* pParent /*=NULL*/)
	: CDialog(SDelay::IDD, pParent)
{
	//{{AFX_DATA_INIT(SDelay)
	m_sdelay = _T("");
	m_editdb = _T("");
	//}}AFX_DATA_INIT
}


void SDelay::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SDelay)
	DDX_Text(pDX, IDC_SDELAY, m_sdelay);
	DDX_Text(pDX, IDC_EDITDB, m_editdb);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SDelay, CDialog)
	//{{AFX_MSG_MAP(SDelay)
	ON_BN_CLICKED(IDC_MORE, OnMore)
	ON_BN_CLICKED(IDC_LESS, OnLess)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_MORE3, OnMore3)
	ON_BN_CLICKED(IDC_LESS3, OnLess3)
	ON_BN_CLICKED(IDC_MORE5, OnMore5)
	ON_BN_CLICKED(IDC_LESS4, OnLess4)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SDelay message handlers

void SDelay::OnMore() 
{
	theapp->SavedDelay+=1;
	m_sdelay.Format("%3i",theapp->SavedDelay);
	UpdatePLC();

	UpdateData(false);
}

void SDelay::OnLess() 
{
	theapp->SavedDelay-=1;
	m_sdelay.Format("%3i",theapp->SavedDelay);
	UpdatePLC();

	UpdateData(false);
}

void SDelay::UpdatePLC()
{
	int Value		=	theapp->SavedDelay;
	int Function	=	0;
	int CntrBits	=	0;
	//int ControlChars;
		
	Function=70;
	CntrBits=0;

	if(theapp->serPresent) 	{pframe->m_pserial->SendChar(Value,Function);}
}

BOOL SDelay::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_psdelay=this;
	theapp=(CBottleApp*)AfxGetApp();

	m_sdelay.Format("%3i",theapp->SavedDelay);
	m_editdb.Format("%3i",theapp->SavedDeadBand);
	
	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void SDelay::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{
		KillTimer(1);

		Value=0; Function=0; CntrBits=0;

		if(theapp->serPresent)
		{pframe->m_pserial->ControlChars=Value+Function+CntrBits;}
	}
	
	CDialog::OnTimer(nIDEvent);
}

void SDelay::OnOK() 
{
	// TODO: Add extra validation here
	pframe->SaveJob(pframe->CurrentJobNum);
	
	CDialog::OnOK();
}

void SDelay::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}


void SDelay::OnMore3() 
{
	theapp->SavedDelay+=20;
	m_sdelay.Format("%3i",theapp->SavedDelay);
	UpdatePLC();
	
	UpdateData(false);
}

void SDelay::OnLess3() 
{
	theapp->SavedDelay-=20;
	m_sdelay.Format("%3i",theapp->SavedDelay);
	UpdatePLC();
	
	UpdateData(false);
}

void SDelay::OnMore5() 
{
	theapp->SavedDeadBand+=1;
	m_editdb.Format("%3i",theapp->SavedDeadBand);

	int Value=theapp->SavedDeadBand;
	int Function=0;
	int CntrBits=0;
	//int ControlChars;
		
	Function=120;
	CntrBits=0;

	if(theapp->serPresent) {pframe->m_pserial->SendChar(Value,Function);}
	
	UpdateData(false);
}

void SDelay::OnLess4() 
{
	theapp->SavedDeadBand-=1;
	
	if(theapp->SavedDeadBand<=0) theapp->SavedDeadBand=0;
	m_editdb.Format("%3i",theapp->SavedDeadBand);
	
	int Value=theapp->SavedDeadBand;
	int Function=0;
	int CntrBits=0;
	int ControlChars;
		
	Function=120;
	CntrBits=0;
	ControlChars=Value+Function+CntrBits;
	
	if(theapp->serPresent) {pframe->m_pserial->SendChar(Value,Function);}
	
	UpdateData(false);
}
