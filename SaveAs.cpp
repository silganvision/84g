// SaveAs.cpp : implementation file
//

#include "stdafx.h"
#include "Bottle.h"
#include "SaveAs.h"
#include "mainfrm.h"
#include "job.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SaveAs dialog


SaveAs::SaveAs(CWnd* pParent /*=NULL*/)
	: CDialog(SaveAs::IDD, pParent)
{
	//{{AFX_DATA_INIT(SaveAs)
	m_jobcopy = _T("");
	//}}AFX_DATA_INIT
}


void SaveAs::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SaveAs)
	DDX_Control(pDX, IDC_LISTJOBS, m_listjobs);
	DDX_Text(pDX, IDC_JOBCOPY, m_jobcopy);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SaveAs, CDialog)
	//{{AFX_MSG_MAP(SaveAs)
	ON_LBN_SELCHANGE(IDC_LISTJOBS, OnSelchangeListjobs)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SaveAs message handlers

BOOL SaveAs::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*) AfxGetMainWnd();
	pframe->m_psaveas=this;
	
	theapp=(CBottleApp*)AfxGetApp();
	pframe->m_pjob->SavAs=true;

	LoadJobs();

	CurrentJob=pframe->m_pjob->nIndex;
	m_jobcopy.Format("%s",pframe->m_pjob->m_renamejob);
	
	if(CurrentJob==0) 
	{AfxMessageBox("You need to choose a job before you click <Save As>! "); CDialog::OnOK();}
	
	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void SaveAs::OnSelchangeListjobs() 
{
	pframe->m_pjob->JobOver=m_listjobs.GetCaretIndex();
}

void SaveAs::OnOK() 
{
	pframe->m_pjob->SetTimer(1,100,NULL);
	CDialog::OnOK();
}

void SaveAs::LoadJobs()
{
	theapp=(CBottleApp*)AfxGetApp();
	m_listjobs.ResetContent();
	
	m_listjobs.InsertString(0,"Job List");
	
	for(int i=1; i<=theapp->SavedTotalJobs; i++)
	{
		if(theapp->jobinfo[i].jobname=="")
		{theapp->jobinfo[i].jobname="xxx";}//+itoa();
		
		m_listjobs.InsertString(i,theapp->jobinfo[i].jobname);
	}

	UpdateData(false);
}