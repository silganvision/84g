#if !defined(AFX_CAMERA_H__959BAD3F_CD37_460F_A2FE_6411CB179F87__INCLUDED_)
#define AFX_CAMERA_H__959BAD3F_CD37_460F_A2FE_6411CB179F87__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Camera.h : header file
//
//#include <PGRFlyCapture.h>
//#include <PGRFlyCapturePlus.h>
#include "Globals.h"
#include <thread>
#include <ppl.h>
#include <fstream>

/////////////////////////////////////////////////////////////////////////////
// CCamera dialog

#define bytesPerPixel 4

#define FullImageWidth 1288
#define FullImageHeight 964
#define FullImageXOffset 0
#define FullImageYOffset 0

#define CroppedImageWidth 1024
#define CroppedImageHeight 408
#define CroppedImageXOffset 132
#define CroppedImageYOffset 278
#define CroppedImageNoYOffset 0
using namespace FlyCapture2;

class CCamera : public CDialog
{
	friend class CMainFrame;
	friend class CBottleApp;

// Construction
public:

//-------------- Bottle waist inspection ------------------------------------
	//Variables
	unsigned char cam1ImageCropped[CroppedImageWidth*CroppedImageHeight*bytesPerPixel];
	unsigned char cam1ImageFull[FullImageWidth*FullImageHeight*bytesPerPixel];

//-------------------------------------------------------------------------
	//Variables
	unsigned char cam4ImageCropped[CroppedImageWidth*CroppedImageHeight*bytesPerPixel];
	unsigned char cam4ImageFull[FullImageWidth*FullImageHeight*bytesPerPixel];

//-------------------------------------------------------------------------
	//Variables
	unsigned char cam2ImageCropped[CroppedImageWidth*CroppedImageHeight*bytesPerPixel];
	unsigned char cam2ImageFull[FullImageWidth*FullImageHeight*bytesPerPixel];

//-------------------------------------------------------------------------
	//Variables
	unsigned char cam3ImageCropped[CroppedImageWidth*CroppedImageHeight*bytesPerPixel];
	unsigned char cam3ImageFull[FullImageWidth*FullImageHeight*bytesPerPixel];

//-------------------------------------------------------------------------

	//Functions
	void CropCameraImage(FlyCapture2::Image &image, BYTE *croppedImageBuffer,							  
						  int croppedImageWidth, int croppedImageHeight,
						  int croppedImageXOffset, int croppedImageYOffset);

	void DuplicateFlyCaptureBGRUImage(FlyCapture2::Image &image, BYTE *duplicatedImage);

//---------------------------------------------------------------------------

public:
	__int64 nDiff0;
	double spanElapsed0;		//Used to hold time in seconds of how long XAxisView.Inspect() takes.
	LARGE_INTEGER  timeStart0,	//Used to hold time at which XAxisView.Inspect() starts.
				   timeEnd0,	//Used to hold time at which XAxisView.Inspect() ends.
				   Frequency0;	//Used to convert processor cycles to seconds time measurements.

	int resWidth;
	int resHeigh;

	int m_cams1to4Width;
	int m_cams1to4Height;
	int m_cams1to4XOffset;
	int m_cams1to4YOffset;

	int m_cam5Width;
	int m_cam5Height;
	int m_cam5XOffset;
	int m_cam5YOffset;

	CWinThread *thread1;
	CWinThread *thread2;
	CWinThread *thread3;
	CWinThread *thread4;
	CWinThread *thread5;


	FlyCapture2::Image* pimage1;
	FlyCapture2::Image* pimage2;
	FlyCapture2::Image* pimage3;
	FlyCapture2::Image* pimage4;
	FlyCapture2::Image* pimage5;

	void InitializeCameras();
	double gain[EXPECTED_CAMS];			//Gain for camera
	double shutterSpeed[EXPECTED_CAMS];	//Shutter speed for camera

	bool cam1OK;
	bool cam2OK;
	bool cam3OK;
	bool cam4OK;
	bool cam5OK;

	double frameRate;
	
	std::atomic<int> m_bContinueGrabThread;
	std::atomic<int> m_bRestart;

	void ModifyCams();
	int StartCams();
	int ReStartCams();

	int _count_1;
	
	CCamera(CWnd* pParent = NULL);   // standard constructor
	FlyCapture2::BusManager* busMgr;
	FlyCapture2::IPAddress ipaddresses[EXPECTED_CAMS];

	// Processed image for displaying and saving
	FlyCapture2::Image   m_imageProcessed1;
	FlyCapture2::Image   m_imageProcessed2;
	FlyCapture2::Image   m_imageProcessed3;
	FlyCapture2::Image   m_imageProcessed4;
	FlyCapture2::Image   m_imageProcessed5;
	FlyCapture2::Image*   m_imageProcessed[EXPECTED_CAMS][2];
	std::atomic<unsigned char> readNext[EXPECTED_CAMS];
	std::atomic<unsigned char> writeNext[EXPECTED_CAMS];
	static void busRemovalCallback(void* pparam, int serial);

	std::ofstream filestream;

	// Event indicating that the thread is about to exit.
	//HANDLE   m_heventThreadDone;
	//HANDLE   m_heventThreadDone2;
	//HANDLE   m_heventThreadDone3;
	//HANDLE   m_heventThreadDone4;
	//HANDLE   m_heventThreadDone5;
	//
	CameraInfo    m_cameraInfo;
	CameraInfo    m_cameraInfo2;
	CameraInfo    m_cameraInfo3;
	CameraInfo    m_cameraInfo4;
	CameraInfo    m_cameraInfo5;
	CameraInfo	  m_camInfo[5];

	void StopCams();

	// The image grab thread.
	static UINT threadGrabImage( void* pparam );
	static UINT threadGrabImage2( void* pparam );
	static UINT threadGrabImage3( void* pparam );
	static UINT threadGrabImage4( void* pparam );
	static UINT threadGrabImage5( void* pparam );
	static UINT threadInspect1(void* pparam);
	static UINT threadInspect2(void* pparam);
	static UINT threadInspect3(void* pparam);
	static UINT threadInspect4(void* pparam);
	static UINT threadInspect5(void* pparam);
	void doInspect1();
	void doInspect2();
	void doInspect3();
	void doInspect4();
	void doInspect5();

	static UINT threadCheckGPIOcontrolReg( void* pparam );

	UINT   CheckGPIOControlReg(   );

	// The object grab image loop.  Only executed from within the grab thread.
	UINT doGrabLoop();
	UINT doGrabLoop2();
	UINT doGrabLoop3();
	UINT doGrabLoop4();
	UINT doGrabLoop5();

	void initBitmapStruct( int iCols, int iRows );
	void resizeProcessedImage(int rows, int cols, FlyCapture2::Image* im, int& bufferSize);

	// Structure used to draw to the screen.
	BITMAPINFO        m_bitmapInfo;  
	
	// Current size of the processed image buffer
	int m_iProcessedBufferSize[EXPECTED_CAMS];
	HANDLE signal1;
	HANDLE signal2;
	HANDLE signal3;
	HANDLE signal4;
	HANDLE signal5;

//	CameraGUIContext m_guicontext;

// Dialog Data
	//{{AFX_DATA(CCamera)
	enum { IDD = IDD_CAMERA };
	CString	m_camTop1;
	CString	m_camTop2;
	CString	m_camTop3;
	CString	m_camTop4;
	CString	m_bandwidth;
	CString	m_camWidth1;
	CString	m_camHeigh1;
	CString	m_camTop5;
	CString	m_camWidth5;
	CString	m_camHeigh5;
	CString	m_camLeft1;
	CString	m_camLeft2;
	CString	m_camLeft3;
	CString	m_camLeft4;
	CString	m_camLeft5;
	CString	m_bandwidth5;
	//}}AFX_DATA
	static void busCallback( void* pparam, int iMessage, unsigned long ulParam );
	static void manbusCallback( void* pparam, int iMessage, unsigned long ulParam );
	FlyCapture2::Error CameraSetup(int index, FlyCapture2::BusManager* busMgr, int& camOK);
	FlyCapture2::GigECamera cam_e1;
	FlyCapture2::GigECamera cam_e2;
	FlyCapture2::GigECamera cam_e3;
	FlyCapture2::GigECamera cam_e4;
	FlyCapture2::GigECamera cam_e5;
	FlyCapture2::GigECamera* cam_e[EXPECTED_CAMS];
	FlyCapture2::Error SetImageSettings(int index);
	FlyCapture2::Error SetTriggerMode(FlyCapture2::GigECamera* cam, bool value = true);
	FlyCapture2::Error SetCameraPropertyAuto(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, bool autoOn);
	FlyCapture2::Error SetCameraProperty(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, float value);
	FlyCapture2::Error SetCameraPropertyNoAbs(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, float value);
	FlyCapture2::Error SetCameraProperty(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, unsigned int valueA, unsigned int valueB);
	FlyCapture2::Error GetCameraProperty(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, int &valueA, int &valueB);
	CMainFrame* pframe;
	CBottleApp* theapp;
	std::mutex imageSettings;

	HANDLE handles[EXPECTED_CAMS];
	std::thread* grabThread1;
	std::thread* grabThread2;
	std::thread* grabThread3;
	std::thread* grabThread4;
	std::thread* grabThread5;
	std::thread* inspectThread1;
	std::thread* inspectThread2;
	std::thread* inspectThread3;
	std::thread* inspectThread4;
	std::thread* inspectThread5;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCamera)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	FlyCapture2::Error doContinousGrab(FlyCapture2::GigECamera* cam, bool &gotPic, std::atomic<int>& count, int& bufferSize, int index);

// Implementation

	enum LoopMode
   {
      // None, the grab thread doesn't exist. The camera is stopped.
      NONE,
      // Lock latest.  The normal flycap mode.
      FREE_RUNNING,
      // Record mode.  Lock next.  Every image is displayed.
      RECORD,
      // Storing images for .avi saving.
      RECORD_STORING,
      // Selected to record streaming images, waiting for user to begin.
      RECORD_PRE_STREAMING,
      // Record streaming images.  Lock next.
      RECORD_STREAMING,
   };

	LoopMode m_loopmode;
//protected:
public:
	void UpdateDisplay();
	void UpdateValues();
	bool LockOut;
	bool LockOut2;

	bool stop;
	int cam;
	int initTry;
	//void ChangeBlue();
	//void LessRed();
	void MoreRed();
//	void Restart();
	void OnGless();
	void OnGless2();
	void OnGless3();
	void OnGless4();
	void OnGless5();
	void OnGmore5();
	void OnGmore4();
	void OnGmore3();
	void OnGmore2();
	void OnGmore();
	void OnSsless1();
	void OnSsless2();
	void OnSsless3();
	void OnSsless4();
	void OnSsless5();
	void OnSsmore5();
	void OnSsmore4();
	void OnSsmore3();
	void OnSsmore2();
	void OnSsmore1();
	void SetLightLevel(double cam1Gain, double cam2Gain, double cam3Gain, double cam4Gain, double cam5Gain);
	void StartCams1to5(bool restart);
	void SetOffsets();
	void DisableAdjustableHtWd();
	void DisableCam5Controls();
	void CamTopUp(int camNo);
	void CamTopDown(int camNo);
	void CamLeftUp(int camNo);
	void CamLeftDown(int camNo);;
	void UpdateCameraPosition(int camNo);
	void StartImageGrabThreads();

	// Generated message map functions
	//{{AFX_MSG(CCamera)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnOK();
	afx_msg void OnCamtopup1();
	afx_msg void OnCamtopdw1();
	afx_msg void OnCamtopup2();
	afx_msg void OnCamtopup3();
	afx_msg void OnCamtopup4();
	afx_msg void OnCamtopdw2();
	afx_msg void OnCamtopdw3();
	afx_msg void OnCamtopdw4();
	afx_msg void OnRestart();
	afx_msg void OnBandwidthup1();
	afx_msg void OnCamwidthup1();
	afx_msg void OnCamwidthdw1();
	afx_msg void OnCamheightup1();
	afx_msg void OnCamheightdw1();
	afx_msg void OnBandwidthdw1();
	afx_msg void OnCamtopup5();
	afx_msg void OnCamtopdw5();
	afx_msg void OnCamwidthup5();
	afx_msg void OnCamwidthdw5();
	afx_msg void OnCamheightup5();
	afx_msg void OnCamheightdw5();
	afx_msg void OnCamleftup1();
	afx_msg void OnCamleftdw1();
	afx_msg void OnCamleftup2();
	afx_msg void OnCamleftup3();
	afx_msg void OnCamleftup4();
	afx_msg void OnCamleftdw2();
	afx_msg void OnCamleftdw3();
	afx_msg void OnCamleftdw4();
	afx_msg void OnCamleftup5();
	afx_msg void OnCamleftdw5();
	afx_msg void OnBandwidthup5();
	afx_msg void OnBandwidthdw5();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CAMERA_H__959BAD3F_CD37_460F_A2FE_6411CB179F87__INCLUDED_)
