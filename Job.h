#if !defined(AFX_JOB_H__AA5A9589_E81E_493B_9F9C_991DE651A4B4__INCLUDED_)
#define AFX_JOB_H__AA5A9589_E81E_493B_9F9C_991DE651A4B4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Job.h : header file
//
#include "VisCore.h"


/////////////////////////////////////////////////////////////////////////////
// Job dialog

class Job : public CDialog
{
	friend class CMainFrame;
	friend class CBottleApp;
	friend class KBoard;
	// Construction
public:
	void UpdateDisplay();
	int LoadJobPic(int jobN);

	void DoEnables();
	bool SavAs;
	int original;
	bool DoOver;
	int JobOver;
	CString Value;
	bool NameChanged;
	void LoadJobs();
	Job(CWnd* pParent = NULL);   // standard constructor

	void SaveOld();
	void CopyFile(int Old,int New);
	int nIndex;
	CString jname;
	CMainFrame* pframe;
	CBottleApp *theapp;
	KBoard* pkboard;

	CVisRGBAByteImage imageFile3;

	BYTE *byteBuf3RGB;
	BYTE *byteTempRGB3;
	CVisRGBAByteImage imageC2Q3Taught;



// Dialog Data
	//{{AFX_DATA(Job)
	enum { IDD = IDD_JOB };
	CEdit	m_rename;
	CButton	m_changename;
	CListBox	m_joblist;
	CString	m_renamejob;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Job)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	
	//CString jobname;
	CString str;
	CString Job1;
	CString Job2;
	CString Job3;
	CString Job4;
	CString Job5;
	CString Job6;
	CString Job7;
	CString Job8;
	CString Job9;
	CString Job10;
	CString Job11;
	CString Job12;
	CString Job13;
	CString Job14;
	CString Job15;
	CString Job16;
	CString Job17;
	CString Job18;
	CString Job19;
	CString Job20;
	
	// Generated message map functions
	//{{AFX_MSG(Job)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeJobs();
	afx_msg void OnChangename();
	afx_msg void OnA();
	afx_msg void OnB();
	afx_msg void OnC();
	afx_msg void OnD();
	afx_msg void OnE();
	afx_msg void OnF();
	afx_msg void OnH();
	afx_msg void OnG();
	afx_msg void OnI();
	afx_msg void OnJ();
	afx_msg void OnK();
	afx_msg void OnL();
	afx_msg void OnM();
	afx_msg void OnN();
	afx_msg void OnO();
	afx_msg void OnP();
	afx_msg void OnQ();
	afx_msg void OnR();
	afx_msg void OnS();
	afx_msg void OnT();
	afx_msg void OnU();
	afx_msg void OnV();
	afx_msg void OnW();
	afx_msg void OnX();
	afx_msg void OnY();
	afx_msg void OnZ();
	afx_msg void OnDel();
	afx_msg void OnDash();
	virtual void OnCancel();
	afx_msg void OnJ0();
	afx_msg void OnJ1();
	afx_msg void OnJ2();
	afx_msg void OnJ3();
	afx_msg void OnJ4();
	afx_msg void OnJ5();
	afx_msg void OnJ6();
	afx_msg void OnJ7();
	afx_msg void OnJ8();
	afx_msg void OnJ9();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDatabase();
	afx_msg void OnSaveas();
	afx_msg void OnDelete();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_JOB_H__AA5A9589_E81E_493B_9F9C_991DE651A4B4__INCLUDED_)
