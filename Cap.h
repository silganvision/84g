#if !defined(AFX_LABEL_H__00958869_1728_4460_B91A_4F698657EBE8__INCLUDED_)
#define AFX_LABEL_H__00958869_1728_4460_B91A_4F698657EBE8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Label.h : header file

#include "resource.h"       // main symbols
//

//#define IDD_CAP                         134
/////////////////////////////////////////////////////////////////////////////
// CCap dialog

class CCap : public CDialog
{
	friend class CMainFrame;
	friend class CBottleApp;
// Construction
public:
	void UpdateValues();
	void setAllToFalse();
	CCap(CWnd* pParent = NULL);   // standard constructor
	CMainFrame* pframe;
	CBottleApp* theapp;

	BYTE*	byte5ForPattMatchDiv3;

	int res;
	bool once;
	void UpdateDisplay();

// Dialog Data
	//{{AFX_DATA(CCap)
	enum { IDD = IDD_CAP };
	CString	m_editcorner;
	CString	m_editneck;
	CString	m_editfsize;
	CString	m_editfsize2;
	CString	m_editcsize;
	CString	m_editcoffset;
	CString	m_editcsen;
	CString	m_editcsen2;
	CString	m_picNum;
	CString	m_neckSen;
	//}}AFX_DATA

	CBitmap bitmapr;
	CBitmap bitmapl;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCap)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	bool newTarget;
// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCap)
	afx_msg void OnButtonreteach();
	afx_msg void OnColortarg2();
	afx_msg void OnColorup();
	afx_msg void OnColordn();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnPaint();
	afx_msg void OnTeach();
	afx_msg void OnCornerleft();
	afx_msg void OnCornerright();
	afx_msg void OnNeckup();
	afx_msg void OnNeckdn();
	afx_msg void OnFilllarge();
	afx_msg void OnFillsmaller();
	afx_msg void OnFillup();
	afx_msg void OnFilldn();
	afx_msg void OnTeachfill();
	afx_msg void OnCapsmaller();
	afx_msg void OnCaplarger();
	afx_msg void OnSenmore();
	afx_msg void OnSenless();
	afx_msg void OnSenmore2();
	afx_msg void OnSenless2();
	afx_msg void OnMiddlesetup();
	afx_msg void OnPictoshowup();
	afx_msg void OnPictoshowdn();
	afx_msg void OnEnapicstoshow();
	afx_msg void OnSenneckdw();
	afx_msg void OnSenneckup();
	afx_msg void OnShowcapFil1();
	afx_msg void OnShowcapFil2();
	afx_msg void OnShowcapFil3();
	afx_msg void OnShowcapFil4();
	afx_msg void OnSetupcap1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LABEL_H__00958869_1728_4460_B91A_4F698657EBE8__INCLUDED_)
