#if !defined(AFX_STATUS_H__77C0E7F7_528A_457D_9504_77291D7C8D19__INCLUDED_)
#define AFX_STATUS_H__77C0E7F7_528A_457D_9504_77291D7C8D19__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Status.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Status dialog

class Status : public CDialog
{
// Construction
	friend class CBottleApp;
	friend class CMainFrame;
public:
	Status(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Status)
	enum { IDD = IDD_CAMSTART };
	CString	m_inittry;
	CString	m_cam;
	//}}AFX_DATA

	CBottleApp *theapp;
	CMainFrame* pframe;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Status)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Status)
	afx_msg void OnTimer(UINT nIDEvent);
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnStop();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STATUS_H__77C0E7F7_528A_457D_9504_77291D7C8D19__INCLUDED_)
