// Usb.cpp : implementation file
//

#include "stdafx.h"
#include "Bottle.h"
#include "Usb.h"

#include "MainFrm.h"
#include "stdafx.h"
#include "Bottleview.h"

#include <wtypes.h>
#include <initguid.h>
#define MAX_LOADSTRING 256

#define MAX_NUM_MEASUREMENTS 100
#define GOIO_MAX_SIZE_DEVICE_NAME 100

char *deviceDesc[8] = {"?", "?", "Go! Temp", "Go! Link", "Go! Motion", "?", "?", "Mini GC"};



extern "C" {

// This file is in the Windows DDK available from Microsoft.
#include "hidsdi.h"

#include "dbt.h"
#include <setupapi.h>
//#include "winuser.h"
}

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//function prototypes

//Application global variables 

	HIDP_CAPS							Capabilities;
	
	PSP_DEVICE_INTERFACE_DETAIL_DATA	detailData;
	HANDLE								ReadHandle;
	HANDLE								WriteHandle;
	HANDLE								DeviceHandle;	
	HANDLE								hEventObject;
	HANDLE								hDevInfo;
	GUID								HidGuid;
	OVERLAPPED							HIDOverlapped;
	LPOVERLAPPED						lpOverLap;
	DWORD								ReportType;

/////////////////////////////////////////////////////////////////////////////
// Usb dialog

Usb::Usb(CWnd* pParent /*=NULL*/)
	: CDialog(Usb::IDD, pParent)
{
	//{{AFX_DATA_INIT(Usb)
	m_ResultsString = _T("");
	m_strBytesReceived = _T("");
	m_ProductIDString = _T("0002");
	m_VendorIDString = _T("08F7");
	m_probe1 = _T("");
	m_probe2 = _T("");
	//}}AFX_DATA_INIT
}

//These are the vendor and product IDs to look for.
//Uses Lakeview Research's Vendor ID.
int VendorID	= 0x08F7;
int ProductID	= 0x0002;



void Usb::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Usb)
	DDX_Control(pDX, IDC_txtVendorID, m_VendorID);
	DDX_Control(pDX, IDC_txtProductID, m_ProductID);
	DDX_Control(pDX, IDC_lstBytesReceived, m_BytesReceived);
	DDX_Control(pDX, IDC_LIST2, m_ResultsList);
	DDX_Control(pDX, IDC_Once, m_Once);
//	DDX_LBString(pDX, IDC_LIST2, m_ResultsString);
//	DDX_LBString(pDX, IDC_lstBytesReceived, m_strBytesReceived);
	DDX_Text(pDX, IDC_txtProductID, m_ProductIDString);
	DDX_Text(pDX, IDC_txtVendorID, m_VendorIDString);
	DDX_Text(pDX, IDC_PROBE1, m_probe1);
	DDX_Text(pDX, IDC_PROBE2, m_probe2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Usb, CDialog)
	//{{AFX_MSG_MAP(Usb)
	ON_BN_CLICKED(IDC_Once, OnOnce)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_cmdFindMyDevice, On_cmdFindMyDevice)
	ON_EN_CHANGE(IDC_txtVendorID, OnChange_txtVendorID)
	ON_EN_CHANGE(IDC_txtProductID, OnChange_txtProductID)
	ON_BN_CLICKED(IDC_HIDE, OnHide)
	//}}AFX_MSG_MAP
ON_MESSAGE(WM_DEVICECHANGE, Main_OnDeviceChange)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Usb message handlers

BOOL Usb::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pusb=this;
	theapp=(CBottleApp*)AfxGetApp();

	MyDeviceDetected = FALSE;

	int		ByteToSend = 0;
	CString	strByteToSend = "";
	CString	strComboBoxText="";
	LensTrigger=0;
	LensTrigger2=0;
	LensTrigger3=0;
	LensTrigger4=0;
//	theapp->WhichCam=1;
	//End my declares

	//Populate the combo boxes with values from 00 to FF.
	MyDeviceDetected=FALSE;

	for (ByteToSend=0; ByteToSend < 256; ByteToSend++)
	{
		//Display the value as a 2-digit Hex value.
		strByteToSend.Format("%.2X",ByteToSend);
	}

	//Set the caption for the Continous button.

	UpdateData(false);

	Usb::FindTheHID();
	ReadAndWriteToDevice();
	SetTimer(5,5000,NULL);
	
	//end my code
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void Usb::CloseHandles()
{
	//Close open handles.
	if(DeviceHandle != INVALID_HANDLE_VALUE)	{CloseHandle(DeviceHandle);}
	if(ReadHandle != INVALID_HANDLE_VALUE)		{CloseHandle(ReadHandle);}
	if(WriteHandle != INVALID_HANDLE_VALUE)		{CloseHandle(WriteHandle);}
}


BOOL Usb::DeviceNameMatch(LPARAM lParam)
{
	// Compare the device path name of a device recently attached or removed 
	// with the device path name of the device we want to communicate with.
	PDEV_BROADCAST_HDR lpdb = (PDEV_BROADCAST_HDR)lParam;

	DisplayData("MyDevicePathName = " + MyDevicePathName);

	if (lpdb->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE) 
	{
		PDEV_BROADCAST_DEVICEINTERFACE lpdbi = (PDEV_BROADCAST_DEVICEINTERFACE)lParam;
		
		CString DeviceNameString;
		//The dbch_devicetype parameter indicates that the event applies to a device interface.
		//So the structure in LParam is actually a DEV_BROADCAST_INTERFACE structure, 
		//which begins with a DEV_BROADCAST_HDR.

		//The dbcc_name parameter of DevBroadcastDeviceInterface contains the device name. 
 
		//Compare the name of the newly attached device with the name of the device 
		//the application is accessing (myDevicePathName).

		DeviceNameString = lpdbi->dbcc_name;

		DisplayData("DeviceNameString = " + DeviceNameString);


		if ((DeviceNameString.CompareNoCase(MyDevicePathName)) == 0)
		{return true;}//The name matches.
		else
		{return false;}//It's a different device.
	}
	else	{return false;}	
}


void Usb::DisplayCurrentTime()
{
	//Get the current time and date and display them in the log List Box.
	CTime curTime = CTime::GetCurrentTime();
	CString CurrentTime = curTime.Format( "%H:%M:%S, %B %d, %Y" );
	DisplayData(CurrentTime);
}


void Usb::DisplayData(CString cstrDataToDisplay)
{
	//Display data in the log List Box
	USHORT	Index;
	Index=m_ResultsList.InsertString(-1, (LPCTSTR)cstrDataToDisplay);
	ScrollToBottomOfListBox(Index);
}


void Usb::DisplayFeatureReport()
{
	USHORT	ByteNumber;
	CHAR	ReceivedByte;
	
	//Display the received data in the log and the Bytes Received List boxes.
	//Start at the top of the List Box.
	m_BytesReceived.ResetContent();
	
	//Step through the received bytes and display each.
	for (ByteNumber=0; ByteNumber < Capabilities.FeatureReportByteLength; ByteNumber++)
	{
		ReceivedByte = FeatureReport[ByteNumber];		//Get a byte.
		DisplayReceivedData(ByteNumber, ReceivedByte);	//Display it.
	}
}


void Usb::DisplayInputReport()
{
	USHORT	ByteNumber;
	CHAR	ReceivedByte;
	
	//Display the received data in the log and the Bytes Received List boxes.
	//Start at the top of the List Box.
	m_BytesReceived.ResetContent();

	if(theapp->tempSelect==2) //USB Temp Sensor
	{
		//Step through the received bytes and display each.
		for (ByteNumber=0; ByteNumber < Capabilities.InputReportByteLength; ByteNumber++)
		{
			ReceivedByte = InputReport[ByteNumber];			//Get a byte.
			DisplayReceivedData(ByteNumber, ReceivedByte);	//Display it.
		}

	/////////////
		
		double vernier		= double(int(InputReport[4])*16*16+int(InputReport[3]));
		double farenheit	= (vernier/128.0)*9.0/5.0 + 32.0;


		CString m_probeX;
		m_probeX.Format("%0.2f %cF", farenheit, 0x00B0);

		m_probe1.Format("%0.2f = ByteReceived[4] * 16 * 16 + ByteReceived[3]", farenheit);
		m_probe2.Format("%0.2f = %i * 16 * 16 + %i", farenheit, InputReport[4], InputReport[3]);

	//////////////////////

		theapp->Temperature = farenheit;

		if(theapp->Temperature>=theapp->tempLimit)
		{pframe->m_pview->m_status="Over Temperature Limit!!!";}

	//////////////////////

		COLORREF iniCol	=	RGB(230,230,230);

		CFont font;
		font.CreateFont(	22,					//int nHeight
							16,					//int nWidth,
							0,					//int nEscapement
							0,					//int nOrientation 
							FW_BOLD,			//int nWeight, FW_NORMAL
							FALSE,				//BYTE bItalic,
							FALSE,				//BYTE bUnderline,
							FALSE,				//BYTE cStrikeOut
							0,					//BYTE nCharSet
							OUT_DEFAULT_PRECIS,	//BYTE nOutPrecision, 
							CLIP_DEFAULT_PRECIS,//BYTE nClipPrecision,   
							DEFAULT_QUALITY,	//BYTE nQuality,
							DEFAULT_PITCH | FF_ROMAN, //BYTE nPitchAndFamily,
							"Arial"				//LPCTSTR lpszFacename
						);

		CClientDC ColorRect(this);
		ColorRect.SetBkColor(iniCol);
		CFont *pfont = ColorRect.SelectObject(&font);
		ColorRect.SetWindowOrg(-80,-100);
		ColorRect.TextOut(20,20, m_probeX);

		ColorRect.SelectObject(&pfont);
		font.DeleteObject();

	//////////////////////
	}


	UpdateData(false);
}


void Usb::DisplayLastError(CString Operation)
{
	//Display a message and the last error in the log List Box.
	LPVOID lpMsgBuf;
	USHORT Index = 0;
	CString	strLastError = "";

	FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		GetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR) &lpMsgBuf,
		0,
		NULL 
	);

	//Display the last error.
	strLastError = Operation + (LPCTSTR)lpMsgBuf; 

	//Trim CR/LF from the error message.
	strLastError.TrimRight(); 

	Index = m_ResultsList.InsertString(-1, strLastError);
	ScrollToBottomOfListBox(Index);
	LocalFree(lpMsgBuf); 
}


void Usb::DisplayReceivedData(USHORT seqNum, unsigned char ReceivedByte)
{
	//Display data received from the device.
	CString strToShow;
	strToShow.Format("[%i]  %03u   0x%02X", seqNum, ReceivedByte, ReceivedByte);

	//Display the value in the Bytes Received List Box.
	m_BytesReceived.InsertString(-1, strToShow);

	//Display the value in the log List Box (optional).
	//MessageToDisplay.Format("%s%s", "Byte 0: ", strByteRead); 
	//DisplayData(MessageToDisplay);	
	//UpdateData(false);
}


bool Usb::FindTheHID()
{
	//Use a series of API calls to find a HID with a specified Vendor IF and Product ID.
	HIDD_ATTRIBUTES						Attributes;
	DWORD								DeviceUsage;
	SP_DEVICE_INTERFACE_DATA			devInfoData;
	bool								LastDevice = FALSE;
	int									MemberIndex = 0;
	LONG								Result;	
	CString								UsageDescription;

	Length = 0;
	detailData = NULL;
	DeviceHandle=NULL;

	/*
	API function: HidD_GetHidGuid
	Get the GUID for all system HIDs.
	Returns: the GUID in HidGuid.
	*/

	HidD_GetHidGuid(&HidGuid);	
	
	/*
	API function: SetupDiGetClassDevs
	Returns: a handle to a device information set for all installed devices.
	Requires: the GUID returned by GetHidGuid.
	*/
	
	hDevInfo=SetupDiGetClassDevs 
		(&HidGuid, 
		NULL, 
		NULL, 
		DIGCF_PRESENT|DIGCF_INTERFACEDEVICE);
		
	devInfoData.cbSize = sizeof(devInfoData);

	//Step through the available devices looking for the one we want. 
	//Quit on detecting the desired device or checking all available devices without success.

	MemberIndex = 0;
	LastDevice = FALSE;

	do
	{
		/*
		API function: SetupDiEnumDeviceInterfaces
		On return, MyDeviceInterfaceData contains the handle to a
		SP_DEVICE_INTERFACE_DATA structure for a detected device.
		Requires:
		The DeviceInfoSet returned in SetupDiGetClassDevs.
		The HidGuid returned in GetHidGuid.
		An index to specify a device.
		*/

		Result=SetupDiEnumDeviceInterfaces 
			(hDevInfo, 
			0, 
			&HidGuid, 
			MemberIndex, 
			&devInfoData);

		if (Result != 0)
		{
			//A device has been detected, so get more information about it.

			/*
			API function: SetupDiGetDeviceInterfaceDetail
			Returns: an SP_DEVICE_INTERFACE_DETAIL_DATA structure
			containing information about a device.
			To retrieve the information, call this function twice.
			The first time returns the size of the structure in Length.
			The second time returns a pointer to the data in DeviceInfoSet.
			Requires:
			A DeviceInfoSet returned by SetupDiGetClassDevs
			The SP_DEVICE_INTERFACE_DATA structure returned by SetupDiEnumDeviceInterfaces.
			
			The final parameter is an optional pointer to an SP_DEV_INFO_DATA structure.
			This application doesn't retrieve or use the structure.			
			If retrieving the structure, set 
			MyDeviceInfoData.cbSize = length of MyDeviceInfoData.
			and pass the structure's address.
			*/
			
			//Get the Length value.
			//The call will return with a "buffer too small" error which can be ignored.

			Result = SetupDiGetDeviceInterfaceDetail 
				(hDevInfo, 
				&devInfoData, 
				NULL, 
				0, 
				&Length, 
				NULL);

			//Allocate memory for the hDevInfo structure, using the returned Length.

			detailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc(Length);
			
			//Set cbSize in the detailData structure.

			detailData -> cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

			//Call the function again, this time passing it the returned buffer size.

			Result = SetupDiGetDeviceInterfaceDetail 
				(hDevInfo, 
				&devInfoData, 
				detailData, 
				Length, 
				&Required, 
				NULL);

			// Open a handle to the device.
			// To enable retrieving information about a system mouse or keyboard,
			// don't request Read or Write access for this handle.

			/*
			API function: CreateFile
			Returns: a handle that enables reading and writing to the device.
			Requires:
			The DevicePath in the detailData structure
			returned by SetupDiGetDeviceInterfaceDetail.
			*/

			DeviceHandle=CreateFile 
				(detailData->DevicePath, 
				0, 
				FILE_SHARE_READ|FILE_SHARE_WRITE, 
				(LPSECURITY_ATTRIBUTES)NULL,
				OPEN_EXISTING, 
				0, 
				NULL);

			DisplayLastError("CreateFile: ");

			/*
			API function: HidD_GetAttributes
			Requests information from the device.
			Requires: the handle returned by CreateFile.
			Returns: a HIDD_ATTRIBUTES structure containing
			the Vendor ID, Product ID, and Product Version Number.
			Use this information to decide if the detected device is
			the one we're looking for.
			*/

			//Set the Size to the number of bytes in the structure.

			Attributes.Size = sizeof(Attributes);

			Result = HidD_GetAttributes 
				(DeviceHandle, 
				&Attributes);
			
			DisplayLastError("HidD_GetAttributes: ");
			
			//Is it the desired device?

			MyDeviceDetected = FALSE;
//		*pVendorId = VERNIER_DEFAULT_VENDOR_ID;
//		*pProductId = SKIP_DEFAULT_PRODUCT_ID;
	//These are the vendor and product IDs to look for.
	//Uses Lakeview Research's Vendor ID.


			if (Attributes.VendorID == VendorID)
			{
				if (Attributes.ProductID == ProductID)
				{
					//Both the Vendor ID and Product ID match.

					MyDeviceDetected = TRUE;
					MyDevicePathName = detailData->DevicePath;
					DisplayData("Device detected");

					//Register to receive device notifications.
					RegisterForDeviceNotifications();

					//Get the device's capablities.
					GetDeviceCapabilities();

					// Find out if the device is a system mouse or keyboard.
					DeviceUsage = (Capabilities.UsagePage * 256) + Capabilities.Usage;

					if (DeviceUsage == 0x102)
						{
						UsageDescription = "mouse";
						}
				
					if (DeviceUsage == 0x106)
						{
						UsageDescription = "keyboard";
						}

					if ((DeviceUsage == 0x102) | (DeviceUsage == 0x106)) 
						{
						DisplayData("");
						DisplayData("*************************");
						DisplayData("The device is a system " + UsageDescription + ".");
						DisplayData("Windows 2000 and Windows XP don't allow applications");
						DisplayData("to directly request Input reports from or "); 
						DisplayData("write Output reports to these devices.");
						DisplayData("*************************");
						DisplayData("");
						}

					// Get a handle for writing Output reports.

					WriteHandle=CreateFile 
						(detailData->DevicePath, 
						GENERIC_WRITE, 
						FILE_SHARE_READ|FILE_SHARE_WRITE, 
						(LPSECURITY_ATTRIBUTES)NULL,
						OPEN_EXISTING, 
						0, 
						NULL);

					DisplayLastError("CreateFile: ");

					// Prepare to read reports using Overlapped I/O.

					PrepareForOverlappedTransfer();

				} //if (Attributes.ProductID == ProductID)

				else
					//The Product ID doesn't match.

					CloseHandle(DeviceHandle);

			} //if (Attributes.VendorID == VendorID)

			else
				//The Vendor ID doesn't match.

				CloseHandle(DeviceHandle);

		//Free the memory used by the detailData structure (no longer needed).
		free(detailData);

		}  //if (Result != 0)

		else
			//SetupDiEnumDeviceInterfaces returned 0, so there are no more devices to check.
			LastDevice=TRUE;

		//If we haven't found the device yet, and haven't tried every available device,
		//try the next one.
		MemberIndex = MemberIndex + 1;

	} //do

	while ((LastDevice == FALSE) && (MyDeviceDetected == FALSE));

	if (MyDeviceDetected == FALSE)
		DisplayData("Device not detected");
	else
		DisplayData("Device detected");

	//Free the memory reserved for hDevInfo by SetupDiClassDevs.
	SetupDiDestroyDeviceInfoList(hDevInfo);
	DisplayLastError("SetupDiDestroyDeviceInfoList");

	return MyDeviceDetected;
}


void Usb::GetDeviceCapabilities()
{
	//Get the Capabilities structure for the device.

	PHIDP_PREPARSED_DATA	PreparsedData;

	/*
	API function: HidD_GetPreparsedData
	Returns: a pointer to a buffer containing the information about the device's capabilities.
	Requires: A handle returned by CreateFile.
	There's no need to access the buffer directly,
	but HidP_GetCaps and other API functions require a pointer to the buffer.
	*/

	HidD_GetPreparsedData 
		(DeviceHandle, 
		&PreparsedData);
	DisplayLastError("HidD_GetPreparsedData: ");

	/*
	API function: HidP_GetCaps
	Learn the device's capabilities.
	For standard devices such as joysticks, you can find out the specific
	capabilities of the device.
	For a custom device, the software will probably know what the device is capable of,
	and the call only verifies the information.
	Requires: the pointer to the buffer returned by HidD_GetPreparsedData.
	Returns: a Capabilities structure containing the information.
	*/
	
	HidP_GetCaps 
		(PreparsedData, 
		&Capabilities);
	DisplayLastError("HidP_GetCaps: ");

	//Display the capabilities

	ValueToDisplay.Format("%s%X", "Usage Page: ", Capabilities.UsagePage);
	DisplayData(ValueToDisplay);
	ValueToDisplay.Format("%s%d", "Input Report Byte Length: ", Capabilities.InputReportByteLength);
	DisplayData(ValueToDisplay);
	ValueToDisplay.Format("%s%d", "Output Report Byte Length: ", Capabilities.OutputReportByteLength);
	DisplayData(ValueToDisplay);
	ValueToDisplay.Format("%s%d", "Feature Report Byte Length: ", Capabilities.FeatureReportByteLength);
	DisplayData(ValueToDisplay);
	ValueToDisplay.Format("%s%d", "Number of Link Collection Nodes: ", Capabilities.NumberLinkCollectionNodes);
	DisplayData(ValueToDisplay);
	ValueToDisplay.Format("%s%d", "Number of Input Button Caps: ", Capabilities.NumberInputButtonCaps);
	DisplayData(ValueToDisplay);
	ValueToDisplay.Format("%s%d", "Number of InputValue Caps: ", Capabilities.NumberInputValueCaps);
	DisplayData(ValueToDisplay);
	ValueToDisplay.Format("%s%d", "Number of InputData Indices: ", Capabilities.NumberInputDataIndices);
	DisplayData(ValueToDisplay);
	ValueToDisplay.Format("%s%d", "Number of Output Button Caps: ", Capabilities.NumberOutputButtonCaps);
	DisplayData(ValueToDisplay);
	ValueToDisplay.Format("%s%d", "Number of Output Value Caps: ", Capabilities.NumberOutputValueCaps);
	DisplayData(ValueToDisplay);
	ValueToDisplay.Format("%s%d", "Number of Output Data Indices: ", Capabilities.NumberOutputDataIndices);
	DisplayData(ValueToDisplay);
	ValueToDisplay.Format("%s%d", "Number of Feature Button Caps: ", Capabilities.NumberFeatureButtonCaps);
	DisplayData(ValueToDisplay);
	ValueToDisplay.Format("%s%d", "Number of Feature Value Caps: ", Capabilities.NumberFeatureValueCaps);
	DisplayData(ValueToDisplay);
	ValueToDisplay.Format("%s%d", "Number of Feature Data Indices: ", Capabilities.NumberFeatureDataIndices);
	DisplayData(ValueToDisplay);

	//No need for PreparsedData any more, so free the memory it's using.

	HidD_FreePreparsedData(PreparsedData);
	DisplayLastError("HidD_FreePreparsedData: ") ;
}


LRESULT Usb::Main_OnDeviceChange(WPARAM wParam, LPARAM lParam)  
	{
  
	//DisplayData("Device change detected.");

	PDEV_BROADCAST_HDR lpdb = (PDEV_BROADCAST_HDR)lParam;

	switch(wParam) 
		{
		// Find out if a device has been attached or removed.
		// If yes, see if the name matches the device path name of the device we want to access.

		case DBT_DEVICEARRIVAL:
			DisplayData("A device has been attached.");

			if (DeviceNameMatch(lParam))
			{
				DisplayData("My device has been attached.");
			}
		
			return TRUE; 
	
		case DBT_DEVICEREMOVECOMPLETE:
			DisplayData("A device has been removed.");

			if (DeviceNameMatch(lParam))
			{
				DisplayData("My device has been removed.");

				// Look for the device on the next transfer attempt.

				MyDeviceDetected = false;
			}
			return TRUE; 
	
		default:
			return TRUE; 
		} 
  }



void Usb::OnChangeResults() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
}




void Usb::OnChange_txtProductID() 
{
	/*
	CString ProductIDtext;

	// Get the text in the edit box.
	CEdit* m_ProductID = (CEdit*) GetDlgItem(IDC_txtProductID);
	m_ProductID->GetWindowText(ProductIDtext); 

	// Convert the hex string in the edit box to an integer.
	ProductID = strtoul("0x" + ProductIDtext, 0, 16); 
	
	MyDeviceDetected=false;*/
}


void Usb::OnChange_txtVendorID() 
{
/*	
	CString VendorIDtext;

	// Get the text in the edit box.
	CEdit* m_VendorID = (CEdit*) GetDlgItem(IDC_txtVendorID);
	m_VendorID->GetWindowText(VendorIDtext); 

	// Convert the hex string in the edit box to an integer.
	VendorID = strtoul("0x" + VendorIDtext, 0, 16); 
	
	MyDeviceDetected=false;*/
}


void Usb::OnClose() 
{
	//Anything that needs to occur on closing the application goes here.
	//Free any resources used by previous API calls and still allocated.

	CDialog::OnClose();
}


void Usb::On_cmdFindMyDevice() 
{
	Usb::FindTheHID();
	//SetTimer(1,100,NULL);
}



void Usb::OnOK() 
{
	CDialog::OnOK();
}


void Usb::OnOnce() 
{
	//Click the Once button to read and write one pair of reports.
	ReadAndWriteToDevice();
}



void Usb::OnTimer(UINT nIDEvent) 
{
	//The timer event.
	//Read and Write one pair of reports.
	if(nIDEvent==1)
	{ReadAndWriteToDevice();}

	if(nIDEvent==2)
	{
		KillTimer(2);
		LensTrigger=0;
		LensTrigger2=0;
		ReadAndWriteToDevice();
	}

	if(nIDEvent==3)
	{
		KillTimer(3);
		LensTrigger=0;
		LensTrigger2=0;
		ReadAndWriteToDevice();
	}

	if(nIDEvent==4)
	{
		KillTimer(4);
		//LensTrigger3=0;
		//ReadAndWriteToDevice();
	}
	
	if(nIDEvent==5)
	{
		ReadAndWriteToDevice();
	}
	CDialog::OnTimer(nIDEvent);
}


void Usb::PrepareForOverlappedTransfer()
{
	//Get a handle to the device for the overlapped ReadFiles.
	ReadHandle=CreateFile 
		(detailData->DevicePath, 
		GENERIC_READ, 
		FILE_SHARE_READ|FILE_SHARE_WRITE,
		(LPSECURITY_ATTRIBUTES)NULL, 
		OPEN_EXISTING, 
		FILE_FLAG_OVERLAPPED, 
		NULL);

	DisplayLastError("CreateFile (ReadHandle): ");

	//Get an event object for the overlapped structure.

	/*API function: CreateEvent
	Requires:
	  Security attributes or Null
	  Manual reset (true). Use ResetEvent to set the event object's state to non-signaled.
	  Initial state (true = signaled) 
	  Event object name (optional)
	Returns: a handle to the event object
	*/

	if (hEventObject == 0)
	{
		hEventObject = CreateEvent(NULL, TRUE, TRUE, "");
		DisplayLastError("CreateEvent: ") ;

		//Set the members of the overlapped structure.
		HIDOverlapped.hEvent = hEventObject;
		HIDOverlapped.Offset = 0;
		HIDOverlapped.OffsetHigh = 0;
	}
}


void Usb::ReadAndWriteToDevice()
{
	//If necessary, find the device and learn its capabilities.
	//Then send a report and request a report.

	//Clear the List Box (optional).
	//m_ResultsList.ResetContent();

	DisplayData("***HID Test Report***");
	DisplayCurrentTime();

	//If the device hasn't been detected already, look for it.

	if (MyDeviceDetected==FALSE)
	{MyDeviceDetected=FindTheHID();}
	
	// Do nothing if the device isn't detected.

	if (MyDeviceDetected==TRUE)
	{
		//Just clearing any previous message if needed
		if(pframe->m_pview->m_status=="No USBTemp Detected")
		{pframe->m_pview->m_status = "";}

		switch (ReportType)
		{
			case 0:// Output and Input Reports
			{
				////Write a report to the device.
				WriteOutputReport();

				//Read a report from the device.
				ReadInputReport();

				break;
			}

			case 1:// Feature reports
			{
				//Write a report to the device.
				WriteFeatureReport();

				//Read a report from the device.
				ReadFeatureReport();

				break;
			}

			default:
			{break;}
		}
	} 
	else
	{
		m_probe1 = "No USB Temperature detected";
		m_probe2 = "No USB Temperature detected";
		//If not physical USB sensor
		//pframe->m_pview->m_status="No USBTemp Detected";
		
		UpdateData(false);
	}
}


void Usb::ReadFeatureReport()
{
	// Read a Feature report from the device.
	CString	ByteToDisplay = "";
	BOOLEAN	Result;
	
	//The first byte is the report number.
	FeatureReport[0]=0;

	//Read a report from the device.

	/*
	HidD_GetFeature
	Returns:
	True on success
	Requires: 
	A device handle returned by CreateFile.
	A buffer to hold the report.
	The report length returned by HidP_GetCaps in Capabilities.InputReportByteLength.
	*/
	
	if (DeviceHandle != INVALID_HANDLE_VALUE)
	{
		Result = HidD_GetFeature(DeviceHandle, FeatureReport, Capabilities.FeatureReportByteLength);
		DisplayLastError("HidD_GetFeature: ");
	}
		
	if (!Result)
	{
		//The read attempt failed, so close the handles, display a message,
		//and set MyDeviceDetected to FALSE so the next attempt will look for the device.

		CloseHandles();
		DisplayData("Can't read from device");
		MyDeviceDetected = FALSE;
	}
	else
	{
		DisplayData("Received Feature report: ");
		DisplayFeatureReport();
	}
}


void Usb::ReadInputReport()
{
	// Retrieve an Input report from the device.
	CString	ByteToDisplay = "";

	DWORD	Result;
	
	//The first byte is the report number.
	InputReport[0]=0;

	// Find out if the "Use Control Transfers Only" check box is checked.
	UpdateData(true);
 	
	/*API call:ReadFile
	'Returns: the report in InputReport.
	'Requires: a device handle returned by CreateFile
	'(for overlapped I/O, CreateFile must be called with FILE_FLAG_OVERLAPPED),
	'the Input report length in bytes returned by HidP_GetCaps,
	'and an overlapped structure whose hEvent member is set to an event object.
	*/
	if (ReadHandle != INVALID_HANDLE_VALUE)
	{
		Result = ReadFile 
		(ReadHandle, InputReport, Capabilities.InputReportByteLength, 
		&NumberOfBytesRead, (LPOVERLAPPED) &HIDOverlapped); 
	}
 
	DisplayLastError("ReadFile: ") ;

	/*API call:WaitForSingleObject
	'Used with overlapped ReadFile.
	'Returns when ReadFile has received the requested amount of data or on timeout.
	'Requires an event object created with CreateEvent
	'and a timeout value in milliseconds.
	*/
	Result = WaitForSingleObject(hEventObject, 6000);
	DisplayLastError("WaitForSingleObject: ") ;
 
	switch (Result)
	{
		case WAIT_OBJECT_0:
		{	DisplayData("Received Input report,");
				break;	}
		case WAIT_TIMEOUT:
		{
			ValueToDisplay.Format("%s", "ReadFile timeout");
			DisplayData(ValueToDisplay);
			//Cancel the Read operation.

			/*API call: CancelIo
			Cancels the ReadFile
			Requires the device handle.
			Returns non-zero on success.
			*/
			Result = CancelIo(ReadHandle);
			
			//A timeout may mean that the device has been removed. 
			//Close the device handles and set MyDeviceDetected = False 
			//so the next access attempt will search for the device.
			CloseHandles();
			DisplayData("Can't read from device");
			MyDeviceDetected = FALSE;
			break;
		}
		default:
		{	
			ValueToDisplay.Format("%s", "Undefined error");
			//Close the device handles and set MyDeviceDetected = False 
			//so the next access attempt will search for the device.

			CloseHandles();
			DisplayData("Can't read from device");
			MyDeviceDetected = FALSE;
			break;
		}
	}

	/*
	API call: ResetEvent
	Sets the event object to non-signaled.
	Requires a handle to the event object.
	Returns non-zero on success.
	*/
	ResetEvent(hEventObject);

	//Display the report data.
	DisplayInputReport();

}


void Usb::RegisterForDeviceNotifications()
{

	// Request to receive messages when a device is attached or removed.
	// Also see WM_DEVICECHANGE in BEGIN_MESSAGE_MAP(Usb, CDialog).

	DEV_BROADCAST_DEVICEINTERFACE DevBroadcastDeviceInterface;
	HDEVNOTIFY DeviceNotificationHandle;

	DevBroadcastDeviceInterface.dbcc_size = sizeof(DevBroadcastDeviceInterface);
	DevBroadcastDeviceInterface.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
	DevBroadcastDeviceInterface.dbcc_classguid = HidGuid;

	DeviceNotificationHandle = RegisterDeviceNotification(m_hWnd, 
		&DevBroadcastDeviceInterface, DEVICE_NOTIFY_WINDOW_HANDLE);

}


void Usb::ScrollToBottomOfListBox(USHORT Index)
{
	/* 
	Scroll to the bottom of the list box. 
	To do so, add a line and set it as the current selection,
	possibly scrolling the window.
	Then deselect the line, 
	leaving the list box scrolled to the bottom with nothing selected.
	*/

	m_ResultsList.SetCurSel( Index );
	m_ResultsList.SetCurSel( -1 );
}


void Usb::WriteFeatureReport()
{


}


void Usb::WriteOutputReport()
{
	//Send a report to the device.

	DWORD	BytesWritten = 0;
	INT		Index =0;
	ULONG	Result;
	CString	strBytesWritten = "";

	UpdateData(true);


	//Can set the other report values here, or get them from the combo boxes.
	//OutputReport[1]=33;
	//OutputReport[2]=6;

	//Get the bytes to send from the combo boxes.
	
	//If Autoincrement is checked, increment the selection.


	//if (m_cbutAutoIncrement.GetCheck()>0)
		//{
		//Index=m_cboByteToSend0.GetCurSel();
		//Index=Index+1;
		//m_cboByteToSend0.SetCurSel(Index);
		//}

	//if (m_cbutAutoIncrement.GetCheck()>0)
		//{
		//Index=m_cboByteToSend1.GetCurSel();
		//Index=Index+1;
		//m_cboByteToSend1.SetCurSel(Index);
		//}	

	//Get the values from the combo boxes.
//The first byte is the report number.

	OutputReport[0]=0;

	OutputReport[2]=LensTrigger;
	OutputReport[3]=LensTrigger2;
	OutputReport[4]=LensTrigger3;
	OutputReport[5]=LensTrigger4;


		/*
		API Function: WriteFile
		Sends a report to the device.
		Returns: success or failure.
		Requires:
		A device handle returned by CreateFile.
		A buffer that holds the report.
		The Output Report length returned by HidP_GetCaps,
		A variable to hold the number of bytes written.
		*/

		if (WriteHandle != INVALID_HANDLE_VALUE)
		{
			Result = WriteFile(	WriteHandle, 
								OutputReport, 
								Capabilities.OutputReportByteLength, 
								&BytesWritten, 
								NULL);
		}

		//Display the result of the API call and the report bytes.
		DisplayLastError("WriteFile: ");

		if (!Result)
		{
			//The WriteFile failed, so close the handles, display a message,
			//and set MyDeviceDetected to FALSE so the next attempt will look for the device.
			CloseHandles();
			DisplayData("Can't write to device");
			MyDeviceDetected = FALSE;
		}
		else
		{
			DisplayData("An Output report was written to the device.");
			strBytesWritten.Format("%s%d", "Bytes Written: ", BytesWritten); 
			DisplayData(strBytesWritten);
		}
}


void Usb::OnRb2() 
{
	//LensTrigger3=1;
	//ReadAndWriteToDevice();
	//SetTimer(4,100,NULL);
	
}

void Usb::OnHide() 
{
	ShowWindow(SW_HIDE);	
}


