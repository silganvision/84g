//{{AFX_INCLUDES()
//}}AFX_INCLUDES
#if !defined(AFX_INSP_H__07A48C30_EFB2_422F_8E42_F0DA1F59B386__INCLUDED_)
#define AFX_INSP_H__07A48C30_EFB2_422F_8E42_F0DA1F59B386__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Insp.h : header file
//
#include "VisCore.h"
#include "XAxisView.h"	// Added by ClassView
/////////////////////////////////////////////////////////////////////////////
// Insp dialog

typedef struct  {
	int x; 
	int y; } SPoint;


class Insp : public CDialog
{
	friend class CMainFrame;
	friend class CBottleApp;
	// Construction
public:
	void setAllCapCheckBoxesToFalse();
	void setAllMethodsToFalse();
	void setAllCheckBoxesToFalse();
	void SaveTemplateX(BYTE *im_sr2, BYTE *im_sr3, int ncam, int pattsel);
	//void LearnPattX(BYTE *imgInby6, BYTE *imgInby3,int posPattX1, int posPattY1, int cam, int pattsel);
	void DisplayImage(CVisImageBase& image, CDC* pDC);

	enum { Method1 = 1, Method2 = 2, Method3 = 3, Method4 = 4, Method5 = 5, Method6 = 6 };
	enum { NoPattern = 0, Pattern1 = 1, Pattern2 = 2, Pattern3 = 3 };

	int chosenMethod;

	void UpdateValues();
	void UpdateDisplay();
	Insp(CWnd* pParent = NULL);   // standard constructor

	CMainFrame	*pframe;
	CBottleApp	*theapp;

	int		EdgeTransition;
	int		filterSelPatt;
	int		filterSel;

	int		resRes12;
	int		resRes6;
	int		pattSel;

	int		sizepattBy12;
	int		sizepattBy6;
	int		sizepattBy3W;
	int		sizepattBy3H;

	bool	once;
	int		fac;
/*
	int		width[8]; 
	int		height[8];

	int		widthby3[8];
	int		heighby3[8];	
	
	int		widthby6[8];
	int		heighby6[8];	
	
	int		widthby12[8];	
	int		heighby12[8];	
*/	
	int		WIDTH; 
	int		HEIGHT;

	CFile			ImageFile;
	CVisByteImage	imagepat1;
	CVisByteImage	imagepat2;
	CVisByteImage	imagepat3;

	char	buf[10];
	char	currentpath1[60];
	char	currentpath2[60];

	char	currentpathTxt[60];	char	currentpathBmp[60];
	char	currentpathTxt2[60];char	currentpathBmp2[60];
	char	currentpathTxt3[60];char	currentpathBmp3[60];

	CString				m_fine;
	SinglePoint			boltPos;

	CBitmap				bitmapl;
	CBitmap				bitmapl2;

	BYTE	*static_byteImg1BWDiv3;

	BYTE	*byteImgCoDiv3[8];

	BYTE	*byteImg1BWDiv3;	BYTE	*byteImg2BWDiv3;	BYTE	*byteImg3BWDiv3;	BYTE	*byteImg4BWDiv3;
	BYTE	*byteImg1BWDiv6;	BYTE	*byteImg2BWDiv6;	BYTE	*byteImg3BWDiv6;	BYTE	*byteImg4BWDiv6;

	BYTE	*byteImg1Fil2Div3;	BYTE	*byteImg2Fil2Div3;	BYTE	*byteImg3Fil2Div3;	BYTE	*byteImg4Fil2Div3;
	BYTE	*byteImg1Fil2Div6;	BYTE	*byteImg2Fil2Div6;	BYTE	*byteImg3Fil2Div6;	BYTE	*byteImg4Fil2Div6;

	BYTE	*byteImg1Fil3Div3;	BYTE	*byteImg2Fil3Div3;	BYTE	*byteImg3Fil3Div3;	BYTE	*byteImg4Fil3Div3;
	BYTE	*byteImg1Fil3Div6;	BYTE	*byteImg2Fil3Div6;	BYTE	*byteImg3Fil3Div6;	BYTE	*byteImg4Fil3Div6;

	BYTE	*byteImg1Fil4Div3;	BYTE	*byteImg2Fil4Div3;	BYTE	*byteImg3Fil4Div3;	BYTE	*byteImg4Fil4Div3;
	BYTE	*byteImg1Fil4Div6;	BYTE	*byteImg2Fil4Div6;	BYTE	*byteImg3Fil4Div6;	BYTE	*byteImg4Fil4Div6;

	BYTE	*byteImg1Fil5Div3;	BYTE	*byteImg2Fil5Div3;	BYTE	*byteImg3Fil5Div3;	BYTE	*byteImg4Fil5Div3;
	BYTE	*byteImg1Fil5Div6;	BYTE	*byteImg2Fil5Div6;	BYTE	*byteImg3Fil5Div6;	BYTE	*byteImg4Fil5Div6;

	BYTE	*byteImg1Fil6Div3;	BYTE	*byteImg2Fil6Div3;	BYTE	*byteImg3Fil6Div3;	BYTE	*byteImg4Fil6Div3;
	BYTE	*byteImg1Fil6Div6;	BYTE	*byteImg2Fil6Div6;	BYTE	*byteImg3Fil6Div6;	BYTE	*byteImg4Fil6Div6;

	BYTE	*byteImg1Fil7Div3;	BYTE	*byteImg2Fil7Div3;	BYTE	*byteImg3Fil7Div3;	BYTE	*byteImg4Fil7Div3;
	BYTE	*byteImg1Fil7Div6;	BYTE	*byteImg2Fil7Div6;	BYTE	*byteImg3Fil7Div6;	BYTE	*byteImg4Fil7Div6;

	BYTE	*byteImg1Fil8Div3;	BYTE	*byteImg2Fil8Div3;	BYTE	*byteImg3Fil8Div3;	BYTE	*byteImg4Fil8Div3;
	BYTE	*byteImg1Fil8Div6;	BYTE	*byteImg2Fil8Div6;	BYTE	*byteImg3Fil8Div6;	BYTE	*byteImg4Fil8Div6;
	

	
	CVisRGBAByteImage	imageCoDiv3[8];
	CVisByteImage	image1BWDiv3;	CVisByteImage	image2BWDiv3;	CVisByteImage	image3BWDiv3;	CVisByteImage	image4BWDiv3;
	CVisByteImage	image1BWDiv6;	CVisByteImage	image2BWDiv6;	CVisByteImage	image3BWDiv6;	CVisByteImage	image4BWDiv6;	


	CVisByteImage	image1Fil2Div3;	CVisByteImage	image2Fil2Div3;	CVisByteImage	image3Fil2Div3;	CVisByteImage	image4Fil2Div3;
	CVisByteImage	image1Fil2Div6;	CVisByteImage	image2Fil2Div6;	CVisByteImage	image3Fil2Div6;	CVisByteImage	image4Fil2Div6;

	CVisByteImage	image1Fil3Div3;	CVisByteImage	image2Fil3Div3;	CVisByteImage	image3Fil3Div3;	CVisByteImage	image4Fil3Div3;
	CVisByteImage	image1Fil3Div6;	CVisByteImage	image2Fil3Div6;	CVisByteImage	image3Fil3Div6;	CVisByteImage	image4Fil3Div6;

	CVisByteImage	image1Fil4Div3;	CVisByteImage	image2Fil4Div3;	CVisByteImage	image3Fil4Div3;	CVisByteImage	image4Fil4Div3;
	CVisByteImage	image1Fil4Div6;	CVisByteImage	image2Fil4Div6;	CVisByteImage	image3Fil4Div6;	CVisByteImage	image4Fil4Div6;

	CVisByteImage	image1Fil5Div3;	CVisByteImage	image2Fil5Div3;	CVisByteImage	image3Fil5Div3;	CVisByteImage	image4Fil5Div3;
	CVisByteImage	image1Fil5Div6;	CVisByteImage	image2Fil5Div6;	CVisByteImage	image3Fil5Div6;	CVisByteImage	image4Fil5Div6;

	CVisByteImage	image1Fil6Div3;	CVisByteImage	image2Fil6Div3;	CVisByteImage	image3Fil6Div3;	CVisByteImage	image4Fil6Div3;
	CVisByteImage	image1Fil6Div6;	CVisByteImage	image2Fil6Div6;	CVisByteImage	image3Fil6Div6;	CVisByteImage	image4Fil6Div6;

	CVisByteImage	image1Fil7Div3;	CVisByteImage	image2Fil7Div3;	CVisByteImage	image3Fil7Div3;	CVisByteImage	image4Fil7Div3;
	CVisByteImage	image1Fil7Div6;	CVisByteImage	image2Fil7Div6;	CVisByteImage	image3Fil7Div6;	CVisByteImage	image4Fil7Div6;

	CVisByteImage	image1Fil8Div3;	CVisByteImage	image2Fil8Div3;	CVisByteImage	image3Fil8Div3;	CVisByteImage	image4Fil8Div3;
	CVisByteImage	image1Fil8Div6;	CVisByteImage	image2Fil8Div6;	CVisByteImage	image3Fil8Div6;	CVisByteImage	image4Fil8Div6;

	CVisByteImage		m_image;

	CVisByteImage		imagePattBy6[8];
	CVisByteImage		imagePattBy3[8];

	CVisByteImage		imagePattBy6_M6_1;
	CVisByteImage		imagePattBy3_M6_1; //M6 : method 6

	CVisByteImage		imagePattBy6_M6;
	CVisByteImage		imagePattBy3_M6; //M6 : method 6

	CVisByteImage		imagePattBy6_M6_Num3;
	CVisByteImage		imagePattBy3_M6_Num3; //M6 : method 6 //Num3: Pattern Number 3
	
	int		cam;
	int		res;
	int heightBy3;
	int widthBy3;
	int heightBy6;
	int widthBy6;

	void InitialXLocations();

// Dialog Data
	//{{AFX_DATA(Insp)
	enum { IDD = IDD_INSP };
	CButton	m_tgLeft;
	CButton	m_align;
	CButton	m_b2;
	CButton	m_b1;
	CEdit	m_editperfc;
	CButton	m_perfsmaller;
	CButton	m_perflarger;
	CButton	m_tbupsp;
	CButton	m_tbsmallersp;
	CButton	m_tb2upsp;
	CButton	m_tb2mupsp;
	CButton	m_tb2mdnsp;
	CButton	m_tb2dnsp;
	CButton	m_fdnsp;
	CButton	m_fleftsp;
	CButton	m_fupsp;
	CButton	m_frightsp;
	CButton	m_nupsp;
	CButton	m_ndnsp;
	CButton	m_mupsp;
	CButton	m_mdnsp;
	CButton	m_tbdnsp;
	CButton	m_tblargersp;
	CButton	m_tbleftsp;
	CButton	m_tbrightsp;
	CButton	m_udnsp;
	CButton	m_ulargesp;
	CButton	m_uleftsp;
	CButton	m_urightsp;
	CButton	m_usmallsp;
	CButton	m_uupsp;
	CString	m_capmiddle;
	CString	m_tbl;
	CString	m_tbr;
	CString	m_fillh;
	CString	m_fillv;
	CString	m_edittb2s;
	CString	m_edittb2v;
	CString	m_edittb2h;
	CString	m_edituh;
	CString	m_editus;
	CString	m_edituv;
	CString	m_editsize;
	CString	m_edittb2;
	CString	m_editb2v;
	CString	m_edittbseam;
	CString	m_tb2vert;
	CString	m_editperf;
	CString	m_tgXYpatt;
	CString	m_tgXYpatt2;
	CString	m_sensMet3;
	CString	m_topLimPatt;
	CString	m_botLimPatt;
	CString	m_offset1;
	CString	m_offset2;
	CString	m_offset3;
	CString	m_offset4;
	CString	m_sensEdge;
	//}}AFX_DATA

	const CVisImageBase& Image() const
		{ return m_image; }
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Insp)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Insp)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnClose();
	afx_msg void OnMup();
	afx_msg void OnMdn();
	virtual void OnOK();
	afx_msg void OnNup();
	afx_msg void OnNdn();
	afx_msg void OnPaint();
	afx_msg void OnTbup();
	afx_msg void OnTbdn();
	afx_msg void OnTbleft();
	afx_msg void OnTbright();
	afx_msg void OnFup();
	afx_msg void OnFdn();
	afx_msg void OnFleft();
	afx_msg void OnFright();
	afx_msg void OnTrigger();
	afx_msg void OnTrigger3();
	afx_msg void OnInspectPushed();
	afx_msg void OnTech();
	afx_msg void OnTb2up();
	afx_msg void OnTb2dn();
	afx_msg void OnTb2left();
	afx_msg void OnTb2right();
	afx_msg void OnTb2mup();
	afx_msg void OnTb2mdn();
	afx_msg void OnUup();
	afx_msg void OnUdn();
	afx_msg void OnUleft();
	afx_msg void OnUright();
	afx_msg void OnUsup();
	afx_msg void OnUsdn();
	afx_msg void OnUsmall();
	afx_msg void OnUlarge();
	afx_msg void OnTbsmaller();
	afx_msg void OnTblarger();
	afx_msg void OnManual();
	afx_msg void OnCircle();
	afx_msg void OnPerflarger();
	afx_msg void OnPerfsmaller();
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnRadio3();
	afx_msg void OnRadio4();
	afx_msg void OnCameras();
	afx_msg void OnButton5();
	afx_msg void OnButton6();
	afx_msg void OnChk1();
	afx_msg void OnChk2();
	afx_msg void OnChk3();
	afx_msg void OnFineins();
	afx_msg void OnChk4();
	afx_msg void OnChk5();
	afx_msg void OnChk6();
	afx_msg void OnChk7();
	afx_msg void OnTargleft();
	afx_msg void OnTargmiddle();
	afx_msg void OnTargright();
	afx_msg void OnTargpattup();
	afx_msg void OnTargpattdw();
	afx_msg void OnTargpattright();
	afx_msg void OnTargpattleft();
	afx_msg void OnTeachtargpatt();
	afx_msg void OnLabintusemethod2();
	afx_msg void OnFilter1();
	afx_msg void OnFilter2();
	afx_msg void OnFilter3();
	afx_msg void OnFilter4();
	afx_msg void OnFilter5();
	afx_msg void OnTargpattup2();
	afx_msg void OnTargpattdw2();
	afx_msg void OnTargpattright2();
	afx_msg void OnTargpattleft2();
	afx_msg void OnFilter6();
	afx_msg void OnChk8();
	afx_msg void OnChk9();
	afx_msg void OnLabintusemethod3();
	afx_msg void OnSensmet3up();
	afx_msg void OnSensmet3dw();
	afx_msg void OnChk10();
	afx_msg void OnChk11();
	afx_msg void OnToplimpattup();
	afx_msg void OnToplimpattdw();
	afx_msg void OnBotlimpattup();
	afx_msg void OnBotlimpattdw();
	afx_msg void OnLabintusemethod4();
	afx_msg void OnFilter7();
	afx_msg void OnFilter8();
	afx_msg void OnLabintusemethod1();
	afx_msg void OnLighttodark();
	afx_msg void OnDarktolight();
	afx_msg void OnSensedgeup();
	afx_msg void OnSensedgedw();
	afx_msg void OnLabintusemethod5();
	afx_msg void OnSelbody1();
	afx_msg void OnSelbody2();
	afx_msg void OnSelbody3();
	afx_msg void OnSelcap0();
	afx_msg void OnSelcap1();
	afx_msg void OnLabintusemethod6();
	afx_msg void OnMethod6Pattern1();
	afx_msg void OnMethod6Pattern2();
	afx_msg void OnMethod6Pattern3();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

		/***************************
		 *  Private Helper Methods *
		 ***************************/
private:
	void LoadCameraImages();

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INSP_H__07A48C30_EFB2_422F_8E42_F0DA1F59B386__INCLUDED_)
