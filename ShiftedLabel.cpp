// ShiftedLabel.cpp : implementation file
//

#include "stdafx.h"
#include "bottle.h"
#include "ShiftedLabel.h"

#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ShiftedLabel dialog


ShiftedLabel::ShiftedLabel(CWnd* pParent /*=NULL*/)
	: CDialog(ShiftedLabel::IDD, pParent)
{
	//{{AFX_DATA_INIT(ShiftedLabel)
	m_shiftedLabTh = _T("");
	m_shiftBotLim1 = _T("");
	m_shiftTopLim1 = _T("");
	m_shiftBotLim2 = _T("");
	m_shiftBotLim3 = _T("");
	m_shiftBotLim4 = _T("");
	m_shiftTopLim2 = _T("");
	m_shiftTopLim3 = _T("");
	m_shiftTopLim4 = _T("");
	m_edgeSen = _T("");
	m_shiftWidth = _T("");
	m_horIni = _T("");
	m_horEnd = _T("");
	//}}AFX_DATA_INIT
}


void ShiftedLabel::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ShiftedLabel)
	DDX_Control(pDX, IDC_FINEMOD, m_fine);
	DDX_Text(pDX, IDC_SHIFTBOTTH, m_shiftedLabTh);
	DDX_Text(pDX, IDC_SHIFTBOTLIM, m_shiftBotLim1);
	DDX_Text(pDX, IDC_SHIFTTOPLIM, m_shiftTopLim1);
	DDX_Text(pDX, IDC_SHIFTBOTLIM2, m_shiftBotLim2);
	DDX_Text(pDX, IDC_SHIFTBOTLIM3, m_shiftBotLim3);
	DDX_Text(pDX, IDC_SHIFTBOTLIM4, m_shiftBotLim4);
	DDX_Text(pDX, IDC_SHIFTTOPLIM2, m_shiftTopLim2);
	DDX_Text(pDX, IDC_SHIFTTOPLIM3, m_shiftTopLim3);
	DDX_Text(pDX, IDC_SHIFTTOPLIM4, m_shiftTopLim4);
	DDX_Text(pDX, IDC_SHIFTBOTTH2, m_edgeSen);
	DDX_Text(pDX, IDC_SHIFTWIDTH, m_shiftWidth);
	DDX_Text(pDX, IDC_SHIFTINILIM, m_horIni);
	DDX_Text(pDX, IDC_SHIFTENDLIM, m_horEnd);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ShiftedLabel, CDialog)
	//{{AFX_MSG_MAP(ShiftedLabel)
	ON_BN_CLICKED(IDC_FILTER1, OnFilter1)
	ON_BN_CLICKED(IDC_FILTER2, OnFilter2)
	ON_BN_CLICKED(IDC_FILTER3, OnFilter3)
	ON_BN_CLICKED(IDC_FILTER4, OnFilter4)
	ON_BN_CLICKED(IDC_FILTER5, OnFilter5)
	ON_BN_CLICKED(IDC_FILTER6, OnFilter6)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_SHIFTTOPLIMUP, OnShifttoplimup)
	ON_BN_CLICKED(IDC_SHIFTTOPLIMDW, OnShifttoplimdw)
	ON_BN_CLICKED(IDC_SHIFTBOTLIMUP, OnShiftbotlimup)
	ON_BN_CLICKED(IDC_SHIFTBOTLIMDW, OnShiftbotlimdw)
	ON_BN_CLICKED(IDC_FINEMOD, OnFinemod)
	ON_BN_CLICKED(IDC_SHIFTBOTTHUP, OnShiftbotthup)
	ON_BN_CLICKED(IDC_SHIFTBOTTHDW, OnShiftbotthdw)
	ON_BN_CLICKED(IDC_SHIFTTOPLIMUP2, OnShifttoplimup2)
	ON_BN_CLICKED(IDC_SHIFTTOPLIMUP3, OnShifttoplimup3)
	ON_BN_CLICKED(IDC_SHIFTTOPLIMUP4, OnShifttoplimup4)
	ON_BN_CLICKED(IDC_SHIFTTOPLIMDW2, OnShifttoplimdw2)
	ON_BN_CLICKED(IDC_SHIFTTOPLIMDW3, OnShifttoplimdw3)
	ON_BN_CLICKED(IDC_SHIFTTOPLIMDW4, OnShifttoplimdw4)
	ON_BN_CLICKED(IDC_SHIFTBOTLIMUP2, OnShiftbotlimup2)
	ON_BN_CLICKED(IDC_SHIFTBOTLIMUP3, OnShiftbotlimup3)
	ON_BN_CLICKED(IDC_SHIFTBOTLIMUP4, OnShiftbotlimup4)
	ON_BN_CLICKED(IDC_SHIFTBOTLIMDW2, OnShiftbotlimdw2)
	ON_BN_CLICKED(IDC_SHIFTBOTLIMDW3, OnShiftbotlimdw3)
	ON_BN_CLICKED(IDC_SHIFTBOTLIMDW4, OnShiftbotlimdw4)
	ON_BN_CLICKED(IDC_SHIFTBOTTHUP2, OnShiftbotthup2)
	ON_BN_CLICKED(IDC_SHIFTBOTTHDW2, OnShiftbotthdw2)
	ON_BN_CLICKED(IDC_SHIFTWIDTHUP, OnShiftwidthup)
	ON_BN_CLICKED(IDC_SHIFTWIDTHDW, OnShiftwidthdw)
	ON_BN_CLICKED(IDC_SHIFTINILIMRIGH, OnShiftinilimrigh)
	ON_BN_CLICKED(IDC_SHIFTINILIMLEFT, OnShiftinilimleft)
	ON_BN_CLICKED(IDC_SHIFTENDLIMRIGH, OnShiftendlimrigh)
	ON_BN_CLICKED(IDC_SHIFTENDLIMLEFT, OnShiftendlimleft)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ShiftedLabel message handlers

void ShiftedLabel::OnFilter1() 
{filterSelShifted = 1;	UpdateValues();}	

void ShiftedLabel::OnFilter2() 
{filterSelShifted = 2;	UpdateValues();}	

void ShiftedLabel::OnFilter3() 
{filterSelShifted = 3;	UpdateValues();}	


void ShiftedLabel::OnFilter4() 
{filterSelShifted = 4;	UpdateValues();}	

void ShiftedLabel::OnFilter5() 
{filterSelShifted = 5;	UpdateValues();}	

void ShiftedLabel::OnFilter6() 
{filterSelShifted = 6;	UpdateValues();}	

void ShiftedLabel::UpdateValues()
{
	theapp->jobinfo[pframe->CurrentJobNum].filterSelShifted = filterSelShifted;

	KillTimer(5);
	SetTimer(5,500,NULL);

	pframe->m_pxaxis->InvalidateRect(NULL,false);
	UpdateDisplay();
}

void ShiftedLabel::UpdateDisplay()
{
	if(filterSelShifted==1)	{CheckDlgButton(IDC_FILTER1,1);}
	if(filterSelShifted==2)	{CheckDlgButton(IDC_FILTER2,1);}
	if(filterSelShifted==3)	{CheckDlgButton(IDC_FILTER3,1);}
	if(filterSelShifted==4)	{CheckDlgButton(IDC_FILTER4,1);}
	if(filterSelShifted==5)	{CheckDlgButton(IDC_FILTER5,1);}
	if(filterSelShifted==6)	{CheckDlgButton(IDC_FILTER6,1);}


	m_horIni.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].shiftHorIniLim);
	m_horEnd.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].shiftHorEndLim);

	m_edgeSen.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].EdgeLabSen);

	m_shiftWidth.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].shiftWidth);

	m_shiftBotLim1.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[1]);
	m_shiftTopLim1.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[1]);

	m_shiftBotLim2.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[2]);
	m_shiftTopLim2.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[2]);

	m_shiftBotLim3.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[3]);
	m_shiftTopLim3.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[3]);

	m_shiftBotLim4.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[4]);
	m_shiftTopLim4.Format("%i", theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[4]);


	m_shiftedLabTh.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].shiftedLabSen);

	InvalidateRect(NULL,false);
	UpdateData(false);	
}

void ShiftedLabel::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==5) 
	{
		KillTimer(5);
		pframe->SaveJob(pframe->CurrentJobNum);
	}

	CDialog::OnTimer(nIDEvent);
}

BOOL ShiftedLabel::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_shiftedLabel=this;
	theapp=(CBottleApp*)AfxGetApp();

	res = 1;
	resTh = 1;

	filterSelShifted = theapp->jobinfo[pframe->CurrentJobNum].filterSelShifted;

	UpdateDisplay();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ShiftedLabel::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

void ShiftedLabel::OnShifttoplimup() 
{
	int ncam = 1;

	theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]<5)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]=5;}

	UpdateValues();
}

void ShiftedLabel::OnShifttoplimdw() 
{
	int ncam = 1;

	theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]>=1000)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]=1000;}

	UpdateValues();
}

void ShiftedLabel::OnShiftbotlimup() 
{
	int ncam = 1;

	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]<5)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]=5;}

	UpdateValues();
}

void ShiftedLabel::OnShiftbotlimdw() 
{
	int ncam = 1;

	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]>=1000)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]=1000;}

	UpdateValues();	
}

void ShiftedLabel::OnFinemod() 
{
	if(res==1)
	{
		m_fine.SetWindowText("Fast"); CheckDlgButton(IDC_FINEMOD,1); 
		res=10; resTh = 5;
	}
	else
	{
		m_fine.SetWindowText("Slow"); CheckDlgButton(IDC_FINEMOD,0);
		res=1;	resTh = 1;
	}
	
	UpdateData(false);		
}

void ShiftedLabel::OnShiftbotthup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].shiftedLabSen+=resTh;
	UpdateValues();
}

void ShiftedLabel::OnShiftbotthdw() 
{
	theapp->jobinfo[pframe->CurrentJobNum].shiftedLabSen-=resTh;
	UpdateValues();
}

void ShiftedLabel::OnShifttoplimup2() 
{
	int ncam = 2;

	theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]<5)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]=5;}

	UpdateValues();	
}

void ShiftedLabel::OnShifttoplimup3() 
{
	int ncam = 3;

	theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]<5)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]=5;}

	UpdateValues();	
}

void ShiftedLabel::OnShifttoplimup4() 
{
	int ncam = 4;

	theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]<5)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]=5;}

	UpdateValues();	
}

void ShiftedLabel::OnShifttoplimdw2() 
{
	int ncam = 2;

	theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]>=1000)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]=1000;}

	UpdateValues();	
}

void ShiftedLabel::OnShifttoplimdw3() 
{
	int ncam = 3;

	theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]>=1000)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]=1000;}

	UpdateValues();	
}

void ShiftedLabel::OnShifttoplimdw4() 
{
	int ncam = 4;

	theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]>=1000)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftTopLim[ncam]=1000;}

	UpdateValues();	
}

void ShiftedLabel::OnShiftbotlimup2() 
{
	int ncam = 2;

	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]<5)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]=5;}

	UpdateValues();	
}

void ShiftedLabel::OnShiftbotlimup3() 
{
	int ncam = 3;

	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]<5)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]=5;}

	UpdateValues();	
}

void ShiftedLabel::OnShiftbotlimup4() 
{
	int ncam = 4;

	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]<5)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]=5;}

	UpdateValues();	
}

void ShiftedLabel::OnShiftbotlimdw2() 
{
	int ncam = 2;

	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]>=1000)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]=1000;}

	UpdateValues();		
}

void ShiftedLabel::OnShiftbotlimdw3() 
{
	int ncam = 3;

	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]>=1000)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]=1000;}

	UpdateValues();		
}

void ShiftedLabel::OnShiftbotlimdw4() 
{
	int ncam = 4;

	theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]>=1000)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftBotLim[ncam]=1000;}

	UpdateValues();		
}

void ShiftedLabel::OnShiftbotthup2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].EdgeLabSen+=resTh;
	UpdateValues();
}

void ShiftedLabel::OnShiftbotthdw2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].EdgeLabSen-=resTh;
	UpdateValues();
}



void ShiftedLabel::OnShiftwidthup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].shiftWidth+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftWidth>=100)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftWidth=100;}

	UpdateValues();	

}

void ShiftedLabel::OnShiftwidthdw() 
{

	theapp->jobinfo[pframe->CurrentJobNum].shiftWidth-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftWidth<12)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftWidth=12;}

	UpdateValues();		
}

void ShiftedLabel::OnShiftinilimrigh() 
{
	theapp->jobinfo[pframe->CurrentJobNum].shiftHorIniLim+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftHorIniLim>=300)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftHorIniLim=300;}

	UpdateValues();		
}

void ShiftedLabel::OnShiftinilimleft() 
{
	theapp->jobinfo[pframe->CurrentJobNum].shiftHorIniLim-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftHorIniLim<=20)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftHorIniLim=20;}

	UpdateValues();			
}

void ShiftedLabel::OnShiftendlimrigh() 
{
	theapp->jobinfo[pframe->CurrentJobNum].shiftHorEndLim+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftHorEndLim>=400)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftHorEndLim=400;}

	UpdateValues();		
}

void ShiftedLabel::OnShiftendlimleft() 
{
	theapp->jobinfo[pframe->CurrentJobNum].shiftHorEndLim-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].shiftHorEndLim<=100)
	{theapp->jobinfo[pframe->CurrentJobNum].shiftHorEndLim=100;}

	UpdateValues();			
}
